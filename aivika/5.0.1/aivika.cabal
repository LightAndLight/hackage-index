name:            aivika
version:         5.0.1
synopsis:        A multi-method simulation library
description:
    Aivika is a multi-method simulation library focused on 
    discrete event simulation (DES) with partial support of 
    system dynamics and agent-based modeling.
    .
    The library has the following features:
    .
    * allows defining recursive stochastic differential equations of 
      system dynamics (unordered as in maths via the recursive do-notation);
    .
    * supports the event-driven paradigm of DES as a basic core for 
      implementing other paradigms;
    .
    * supports extensively the process-oriented paradigm of DES
      with an ability to resume, suspend and cancel 
      the discontinuous processes;
    .
    * allows working with the resources based on specified queue strategies 
      (FCFS\/FIFO, LCFS\/LIFO, SIRO, static priorities and so on);
    .
    * allows customizing the infinite and finite queues based on strategies too;
    .
    * supports the resource preemption;
    .
    * allows defining a queue network based on streams of data (transacts) 
      and their processors;
    .
    * allows simulating circuits with recursive links and delays;
    .
    * supports the activity-oriented paradigm of DES;
    .
    * supports basic constructs for the agent-based modeling such as 
      agents, states, timeout and timer handlers;
    .
    * allows creating combined discrete-continuous models as all parts
      of the library are well integrated and this is reflected directly 
      in the type system;
    .
    * the arrays of simulation variables are inherently supported;
    .
    * supports the Monte-Carlo simulation;
    .
    * the simulation model can depend on external parameters;
    .
    * uses extensively signals for notification;
    .
    * allows gathering statistics in time points;
    .
    * hides technical details in high-level simulation computations
      (monads, streams and arrows).
    .
    Aivika itself is a light-weight engine with minimal dependencies. 
    However, it has additional packages [1, 2] that offer the following 
    features:
    .
    * automating simulation experiments;
    .
    * saving the results in CSV files;
    .
    * plotting the deviation chart by rule 3-sigma, histogram, 
      time series, XY chart;
    .
    * collecting the summary of statistical data;
    .
    * parallel execution of the Monte-Carlo simulation;
    .
    * has an extensible architecture.
    .
    The charting package has two interchangeable back-ends [3, 4],
    where one of them uses Cairo and it is more preferable.
    .
    The PDF documentation and installation instructions are 
    available on the Aivika Wiki website [5]. 
    .
    Moreover, the method was generalized [6] and applied to 
    nested simulation [7] and parallel distributed simulation [8]. 
    .
    The libraries were tested on Linux, Windows and OS X.
    .
    A more full information about Aivika is available on the project website [9].
    .
    \[1] <http://hackage.haskell.org/package/aivika-experiment>
    .
    \[2] <http://hackage.haskell.org/package/aivika-experiment-chart>
    .
    \[3] <http://hackage.haskell.org/package/aivika-experiment-cairo>
    .
    \[4] <http://hackage.haskell.org/package/aivika-experiment-diagrams>
    .
    \[5] <https://github.com/dsorokin/aivika/wiki>
    .
    \[6] <http://hackage.haskell.org/package/aivika-transformers>
    .
    \[7] <http://hackage.haskell.org/package/aivika-branches>
    .
    \[8] <http://hackage.haskell.org/package/aivika-distributed>
    .
    \[9] <http://www.aivikasoft.com/en/products/aivika.html>
    .
    P.S. Aivika is actually a genuine female Mari name which is pronounced 
    with stress on the last syllable.
    .
category:        Simulation
license:         BSD3
license-file:    LICENSE
copyright:       (c) 2009-2016. David Sorokin <david.sorokin@gmail.com>
author:          David Sorokin
maintainer:      David Sorokin <david.sorokin@gmail.com>
homepage:        http://www.aivikasoft.com/en/products/aivika.html
cabal-version:   >= 1.10
build-type:      Simple
tested-with:     GHC == 8.0.1

extra-source-files:  examples/BassDiffusion.hs
                     examples/BouncingBall.hs
                     examples/ChemicalReaction.hs
                     examples/ChemicalReactionCircuit.hs
                     examples/FishBank.hs
                     examples/MachRep1.hs
                     examples/MachRep1EventDriven.hs
                     examples/MachRep1TimeDriven.hs
                     examples/MachRep2.hs
                     examples/MachRep3.hs
                     examples/MachineBreakdowns.hs
                     examples/Furnace.hs
                     examples/InspectionAdjustmentStations.hs
                     examples/InventorySystem.hs
                     examples/WorkStationsInSeries.hs
                     examples/QuarryOperations.hs
                     examples/TimeOut.hs
                     examples/TimeOutInt.hs
                     examples/TimeOutWait.hs
                     examples/PERT.hs
                     examples/PingPong.hs
                     examples/PortOperations.hs
                     examples/SingleLaneTraffic.hs
                     examples/RenegingFromQueue.hs
                     examples/TruckHaulingSituation.hs
                     CHANGELOG.md

library

    exposed-modules: Simulation.Aivika
                     Simulation.Aivika.Activity
                     Simulation.Aivika.Activity.Random
                     Simulation.Aivika.Agent
                     Simulation.Aivika.Arrival
                     Simulation.Aivika.Channel
                     Simulation.Aivika.Circuit
                     Simulation.Aivika.Composite
                     Simulation.Aivika.Cont
                     Simulation.Aivika.DoubleLinkedList
                     Simulation.Aivika.Dynamics
                     Simulation.Aivika.Dynamics.Extra
                     Simulation.Aivika.Dynamics.Memo
                     Simulation.Aivika.Dynamics.Memo.Unboxed
                     Simulation.Aivika.Dynamics.Random
                     Simulation.Aivika.Event
                     Simulation.Aivika.Gate
                     Simulation.Aivika.Generator
                     Simulation.Aivika.Net
                     Simulation.Aivika.Net.Random
                     Simulation.Aivika.Operation
                     Simulation.Aivika.Operation.Random
                     Simulation.Aivika.Parameter
                     Simulation.Aivika.Parameter.Random
                     Simulation.Aivika.PriorityQueue
                     Simulation.Aivika.PriorityQueue.Pure
                     Simulation.Aivika.Process
                     Simulation.Aivika.Process.Random
                     Simulation.Aivika.Processor
                     Simulation.Aivika.Processor.Random
                     Simulation.Aivika.Processor.RoundRobbin
                     Simulation.Aivika.Queue
                     Simulation.Aivika.Queue.Base
                     Simulation.Aivika.Queue.Infinite
                     Simulation.Aivika.Queue.Infinite.Base
                     Simulation.Aivika.QueueStrategy
                     Simulation.Aivika.Ref
                     Simulation.Aivika.Ref.Base
                     Simulation.Aivika.Resource
                     Simulation.Aivika.Resource.Base
                     Simulation.Aivika.Resource.Preemption
                     Simulation.Aivika.Resource.Preemption.Base
                     Simulation.Aivika.Results.Locale
                     Simulation.Aivika.Results.Transform
                     Simulation.Aivika.Results
                     Simulation.Aivika.Results.IO
                     Simulation.Aivika.Server
                     Simulation.Aivika.Server.Random
                     Simulation.Aivika.Signal
                     Simulation.Aivika.Signal.Random
                     Simulation.Aivika.Simulation
                     Simulation.Aivika.Specs
                     Simulation.Aivika.Statistics
                     Simulation.Aivika.Statistics.Accumulator
                     Simulation.Aivika.Stream
                     Simulation.Aivika.Stream.Random
                     Simulation.Aivika.SystemDynamics
                     Simulation.Aivika.Table
                     Simulation.Aivika.Task
                     Simulation.Aivika.Transform
                     Simulation.Aivika.Transform.Extra
                     Simulation.Aivika.Transform.Memo
                     Simulation.Aivika.Transform.Memo.Unboxed
                     Simulation.Aivika.Unboxed
                     Simulation.Aivika.Var
                     Simulation.Aivika.Var.Unboxed
                     Simulation.Aivika.Vector
                     Simulation.Aivika.Vector.Unboxed

    other-modules:   Simulation.Aivika.Internal.Cont
                     Simulation.Aivika.Internal.Dynamics
                     Simulation.Aivika.Internal.Event
                     Simulation.Aivika.Internal.Parameter
                     Simulation.Aivika.Internal.Process
                     Simulation.Aivika.Internal.Simulation
                     Simulation.Aivika.Internal.Specs
                     Simulation.Aivika.Internal.Arrival
                     
    build-depends:   base >= 4.5.0.0 && < 6,
                     mtl >= 2.1.1,
                     array >= 0.3.0.0,
                     containers >= 0.4.0.0,
                     random >= 1.0.0.3,
                     vector >= 0.10.0.1

    other-extensions:   FlexibleContexts,
                        FlexibleInstances,
                        UndecidableInstances,
                        BangPatterns,
                        RecursiveDo,
                        Arrows,
                        MultiParamTypeClasses,
                        FunctionalDependencies,
                        ExistentialQuantification,
                        TypeFamilies,
                        DeriveDataTypeable,
                        TypeSynonymInstances,
                        RankNTypes
                     
    ghc-options:     -O2

    default-language:   Haskell2010

source-repository head

    type:     git
    location: https://github.com/dsorokin/aivika
