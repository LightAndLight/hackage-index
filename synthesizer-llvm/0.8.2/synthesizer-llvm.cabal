Name:           synthesizer-llvm
Version:        0.8.2
License:        GPL
License-File:   LICENSE
Author:         Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:     Henning Thielemann <haskell@henning-thielemann.de>
Homepage:       http://www.haskell.org/haskellwiki/Synthesizer
Package-URL:    http://code.haskell.org/synthesizer/llvm/
Category:       Sound, Music
Synopsis:       Efficient signal processing using runtime compilation
Description:
  Efficient signal processing
  using runtime compilation and vector instructions.
  It uses LLVM library, thus it is not bound to a specific CPU.
  There are some example executables that you can build
  with Cabal flag @buildExamples@:
  .
  * @synthi-llvm-render@:
    Render a MIDI file into an audio file
    using some arbitrary instruments.
  .
  * @synthi-llvm-alsa@:
    A realtime software synthesizer
    that receives MIDI events via ALSA
    and in response plays tones via ALSA.
    If you have no ALSA (or Linux at all),
    then you can disable this example with @-f-alsa@.
  .
  * @synthi-llvm-jack@:
    The same realtime software synthesizer using JACK.
    If you have no JACK,
    then you can disable this example with @-f-jack@.
  .
  * @synthi-llvm-example@:
    Not very useful as an executable.
    You should better load the according module into GHCi
    and play around with it.
    The module Synthesizer.LLVM.LAC2011
    should be especially useful for an introduction.
Stability:      Experimental
Tested-With:    GHC==7.4.2, GHC==7.6.3, GHC==7.8.4, GHC==7.10.1
Cabal-Version:  1.14
Build-Type:     Simple
Extra-Source-Files:
  Changes.md

Flag buildExamples
  description: Build example executables
  default:     False

Flag alsa
  description: Build ALSA synthesizer if examples are built
  default:     True

Flag jack
  description: Build JACK synthesizer if examples are built
  default:     False

Source-Repository this
  Tag:         0.8.2
  Type:        darcs
  Location:    http://code.haskell.org/synthesizer/llvm/

Source-Repository head
  Type:        darcs
  Location:    http://code.haskell.org/synthesizer/llvm/


Library
  Build-Depends:
    llvm-extra >=0.8 && <0.9,
    llvm-tf >=3.1 && <3.2,
    tfp >=1.0 && <1.1,
    vault >=0.3 && <0.4,
    synthesizer-core >=0.8 && <0.9,
    synthesizer-midi >=0.6 && <0.7,
    midi >=0.2.1 && <0.3,
    storable-record >=0.0.3 && <0.1,
    storable-tuple >=0.0.2 && <0.1,
    sox >=0.2 && <0.3,
    storablevector >=0.2.6 && <0.3,
    unsafe >=0.0 && <0.1,
    numeric-prelude >=0.3 && <0.5,
    non-negative >=0.1 && <0.2,
    non-empty >=0.2.1 && <0.4,
    event-list >=0.1 && <0.2,
    pathtype >=0.8 && <0.9,
    random >=1.0 && <1.2,
    containers >=0.1 && <0.7,
    transformers >=0.2 && <0.6,
    semigroups >=0.1 && <1.0,
    utility-ht >=0.0.14 && <0.1

  Build-Depends:
    -- base-4 needed for Control.Category
    base >=4 && <5

  Default-Language: Haskell98
  GHC-Options:    -Wall
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
    If impl(ghc<8.0)
      GHC-Options: -fcontext-stack=1000
    Else
      GHC-Options: -freduction-depth=1000

  Hs-source-dirs: src
  Exposed-Modules:
    Synthesizer.LLVM.Simple.Signal
    Synthesizer.LLVM.Simple.SignalPacked
    Synthesizer.LLVM.Simple.Value
    Synthesizer.LLVM.Parameterized.Signal
    Synthesizer.LLVM.Parameterized.SignalPacked
    Synthesizer.LLVM.Parameter
    Synthesizer.LLVM.Storable.Signal
    Synthesizer.LLVM.Storable.Process
    Synthesizer.LLVM.Causal.Process
    Synthesizer.LLVM.Causal.ProcessValue
    Synthesizer.LLVM.Causal.ProcessPacked
    Synthesizer.LLVM.Causal.Controlled
    Synthesizer.LLVM.Causal.ControlledPacked
    Synthesizer.LLVM.CausalParameterized.Process
    Synthesizer.LLVM.CausalParameterized.ProcessValue
    Synthesizer.LLVM.CausalParameterized.ProcessPacked
    Synthesizer.LLVM.CausalParameterized.Controlled
    Synthesizer.LLVM.CausalParameterized.ControlledPacked
    Synthesizer.LLVM.CausalParameterized.Functional
    Synthesizer.LLVM.CausalParameterized.FunctionalPlug
    Synthesizer.LLVM.CausalParameterized.RingBuffer
    Synthesizer.LLVM.CausalParameterized.RingBufferForward
    Synthesizer.LLVM.CausalParameterized.Helix
    Synthesizer.LLVM.Fold
    Synthesizer.LLVM.Plug.Input
    Synthesizer.LLVM.Plug.Output
    Synthesizer.LLVM.Filter.Allpass
    Synthesizer.LLVM.Filter.Butterworth
    Synthesizer.LLVM.Filter.Chebyshev
    Synthesizer.LLVM.Filter.ComplexFirstOrder
    Synthesizer.LLVM.Filter.ComplexFirstOrderPacked
    Synthesizer.LLVM.Filter.FirstOrder
    Synthesizer.LLVM.Filter.SecondOrder
    Synthesizer.LLVM.Filter.SecondOrderPacked
    Synthesizer.LLVM.Filter.SecondOrderCascade
    Synthesizer.LLVM.Filter.Moog
    Synthesizer.LLVM.Filter.Universal
    Synthesizer.LLVM.Filter.NonRecursive
    Synthesizer.LLVM.Generator.Exponential2
    Synthesizer.LLVM.Interpolation
    Synthesizer.LLVM.Frame.SerialVector
    Synthesizer.LLVM.Frame
    Synthesizer.LLVM.Frame.Stereo
    Synthesizer.LLVM.Frame.StereoInterleaved
    Synthesizer.LLVM.Frame.Binary
    Synthesizer.LLVM.Complex
    Synthesizer.LLVM.Wave
    Synthesizer.LLVM.MIDI
    Synthesizer.LLVM.MIDI.BendModulation
    Synthesizer.LLVM.Server.Packed.Instrument
    Synthesizer.LLVM.Server.Scalar.Instrument
    Synthesizer.LLVM.Server.CausalPacked.Instrument
    Synthesizer.LLVM.Server.CausalPacked.InstrumentPlug
    Synthesizer.LLVM.Server.CausalPacked.Speech
    Synthesizer.LLVM.Server.SampledSound
    Synthesizer.LLVM.Server.Common
    Synthesizer.LLVM.Server.CommonPacked
    Synthesizer.LLVM.Server.Parameter

  Other-Modules:
    Synthesizer.LLVM.ConstantPiece
    Synthesizer.LLVM.ForeignPtr
    Synthesizer.LLVM.Random
    Synthesizer.LLVM.EventIterator
    Synthesizer.LLVM.Storable.Vector
    Synthesizer.LLVM.Storable.ChunkIterator
    Synthesizer.LLVM.Storable.LazySizeIterator
    Synthesizer.LLVM.RingBuffer
    Synthesizer.LLVM.Simple.SignalPrivate
    Synthesizer.LLVM.Parameterized.SignalPrivate
    Synthesizer.LLVM.Causal.ProcessPrivate
    Synthesizer.LLVM.CausalParameterized.ProcessPrivate
    Synthesizer.LLVM.Debug.Counter
    Synthesizer.LLVM.Debug.StablePtr
    Synthesizer.LLVM.Debug.Storable
    -- may be moved to llvm-extra
    Synthesizer.LLVM.Execution
    -- shall be removed when Foreign.Marshal.Alloc is fixed
    Synthesizer.LLVM.Alloc
    -- experimental
    Synthesizer.LLVM.Simple.Vanilla
    -- Synthesizer.LLVM.Parameterized.Value

Executable synthi-llvm-example
  If flag(buildExamples)
    Build-Depends:
      synthesizer-llvm,

      llvm-extra,
      llvm-tf,
      tfp,
      synthesizer-core,
      synthesizer-midi >=0.6 && <0.7,
      midi >=0.2.1 && <0.3,
      storable-record >=0.0.2 && <0.1,
      storable-tuple >=0.0.2 && <0.1,
      sox >=0.2 && <0.3,
      storablevector >=0.2.6 && <0.3,
      numeric-prelude >=0.3 && <0.5,
      non-negative >=0.1 && <0.2,
      event-list >=0.1 && <0.2,
      random,
      containers >=0.1 && <0.7,
      transformers,
      non-empty,
      utility-ht,
      pathtype,
      base >=4 && <5
  Else
    Buildable: False
  Default-Language: Haskell98
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  GHC-Prof-Options: -fprof-auto-exported
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
    If impl(ghc<8.0)
      GHC-Options: -fcontext-stack=1000
    Else
      GHC-Options: -freduction-depth=1000
  Hs-Source-Dirs: example, server
  Main-Is:     Synthesizer/LLVM/Test.hs
  Other-Modules:
    Synthesizer.LLVM.LAC2011
    Synthesizer.LLVM.Server.Default

Executable synthi-llvm-lndw
  If flag(buildExamples) && flag(alsa)
    Build-Depends:
      synthesizer-llvm,

      llvm-extra,
      llvm-tf,
      tfp,
      synthesizer-core,
      synthesizer-midi >=0.6 && <0.7,
      midi >=0.2.1 && <0.3,
      storable-record >=0.0.2 && <0.1,
      storable-tuple >=0.0.2 && <0.1,
      sox >=0.2 && <0.3,
      storablevector >=0.2.6 && <0.3,
      numeric-prelude >=0.3 && <0.5,
      non-negative >=0.1 && <0.2,
      event-list >=0.1 && <0.2,
      random,
      containers >=0.1 && <0.7,
      transformers,
      non-empty,
      utility-ht,
      pathtype,

      synthesizer-alsa >=0.5 && <0.6,
      alsa-pcm >=0.6 && <0.7,
      base >=4 && <5
  Else
    Buildable: False
  Default-Language: Haskell98
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  GHC-Prof-Options: -fprof-auto-exported
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
    If impl(ghc<8.0)
      GHC-Options: -fcontext-stack=1000
    Else
      GHC-Options: -freduction-depth=1000
  Hs-Source-Dirs: example
  Main-Is:     Synthesizer/LLVM/TestALSA.hs
  Other-Modules:
    Synthesizer.LLVM.LNdW2011

Executable synthi-llvm-alsa
  If flag(buildExamples) && flag(alsa)
    Build-Depends:
      synthesizer-llvm,

      llvm-extra,
      llvm-tf,
      tfp,
      synthesizer-core,
      synthesizer-midi >=0.6 && <0.7,
      midi >=0.2.1 && <0.3,
      storable-record >=0.0.2 && <0.1,
      storable-tuple >=0.0.2 && <0.1,
      storablevector >=0.2.6 && <0.3,
      numeric-prelude >=0.3 && <0.5,
      non-negative >=0.1 && <0.2,
      event-list >=0.1 && <0.2,
      shell-utility >=0.0 && <0.1,
      pathtype >=0.8 && <0.9,
      optparse-applicative >=0.11 && <0.15,
      containers >=0.1 && <0.7,
      transformers,
      utility-ht,

      synthesizer-alsa >=0.5 && <0.6,
      midi-alsa >=0.2.1 && <0.3,
      alsa-seq >=0.6 && <0.7,
      alsa-pcm >=0.6 && <0.7,
      base >=4 && <5
  Else
    Buildable: False
  Default-Language: Haskell98
  --  -threaded -debug
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  GHC-Options:      -rtsopts
  GHC-Prof-Options: -fprof-auto-exported
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
    If impl(ghc<8.0)
      GHC-Options: -fcontext-stack=1000
    Else
      GHC-Options: -freduction-depth=1000
  Hs-Source-Dirs: alsa, server
  Main-Is:        Synthesizer/LLVM/Server.hs
  Other-Modules:
    Synthesizer.LLVM.Server.Packed.Test
    Synthesizer.LLVM.Server.Packed.Run
    Synthesizer.LLVM.Server.Scalar.Test
    Synthesizer.LLVM.Server.Scalar.Run
    Synthesizer.LLVM.Server.CausalPacked.Run
    Synthesizer.LLVM.Server.CausalPacked.Test
    Synthesizer.LLVM.Server.CausalPacked.Arrange
    Synthesizer.LLVM.Server.ALSA
    Synthesizer.LLVM.Server.Option
    Synthesizer.LLVM.Server.OptionCommon
    Synthesizer.LLVM.Server.Default

Executable synthi-llvm-jack
  If flag(buildExamples) && flag(jack)
    Build-Depends:
      synthesizer-llvm,

      jack >=0.7 && <0.8,

      llvm-extra,
      llvm-tf,
      tfp,
      synthesizer-core,
      synthesizer-midi >=0.6 && <0.7,
      midi >=0.2.1 && <0.3,
      storable-record >=0.0.2 && <0.1,
      storable-tuple >=0.0.2 && <0.1,
      storablevector >=0.2.6 && <0.3,
      numeric-prelude >=0.3 && <0.5,
      non-negative >=0.1 && <0.2,
      random,
      explicit-exception >=0.1.7 && <0.2,
      event-list >=0.1 && <0.2,
      shell-utility >=0.0 && <0.1,
      pathtype >=0.8 && <0.9,
      optparse-applicative >=0.11 && <0.15,
      containers >=0.1 && <0.7,
      transformers,
      utility-ht,

      base >=4 && <5

  Else
    Buildable: False
  Default-Language: Haskell98
  --  -threaded -debug
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  GHC-Options:      -rtsopts
  GHC-Prof-Options: -fprof-auto-exported
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
  Hs-Source-Dirs: jack, server
  Main-Is:        Synthesizer/LLVM/Server/JACK.hs
  Other-Modules:
    Synthesizer.LLVM.Server.CausalPacked.Arrange
    Synthesizer.LLVM.Server.Option
    Synthesizer.LLVM.Server.OptionCommon
    Synthesizer.LLVM.Server.Default

Executable synthi-llvm-render
  If flag(buildExamples)
    Build-Depends:
      synthesizer-llvm,

      llvm-extra,
      llvm-tf,
      tfp,
      sox >=0.2.1 && <0.3,
      synthesizer-core,
      synthesizer-midi >=0.6 && <0.7,
      midi >=0.2.1 && <0.3,
      storable-record >=0.0.2 && <0.1,
      storable-tuple >=0.0.2 && <0.1,
      storablevector >=0.2.6 && <0.3,
      numeric-prelude >=0.3 && <0.5,
      non-negative >=0.1 && <0.2,
      explicit-exception >=0.1.7 && <0.2,
      event-list >=0.1 && <0.2,
      shell-utility >=0.0 && <0.1,
      pathtype >=0.8 && <0.9,
      optparse-applicative >=0.11 && <0.15,
      containers >=0.1 && <0.7,
      transformers,
      utility-ht,

      base >=4 && <5

  Else
    Buildable: False
  Default-Language: Haskell98
  --  -threaded -debug
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  GHC-Options:      -rtsopts
  GHC-Prof-Options: -fprof-auto-exported
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
  Hs-Source-Dirs: render, server
  Main-Is:        Synthesizer/LLVM/Server/Render.hs
  Other-Modules:
    Synthesizer.LLVM.Server.CausalPacked.Arrange
    Synthesizer.LLVM.Server.Option
    Synthesizer.LLVM.Server.OptionCommon
    Synthesizer.LLVM.Server.Default

Executable synthi-llvm-sample
  If flag(buildExamples)
    Build-Depends:
      gnuplot >=0.5 && <0.6,
      synthesizer-llvm,
      synthesizer-core,
      midi,
      numeric-prelude,
      storablevector,
      pathtype,
      utility-ht,
      base >=4 && <5
  Else
    Buildable: False
  Default-Language: Haskell98
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
    If impl(ghc<8.0)
      GHC-Options: -fcontext-stack=1000
    Else
      GHC-Options: -freduction-depth=1000
  Hs-Source-Dirs: ., server
  Main-Is:        src/Synthesizer/LLVM/Server/SampledSoundAnalysis.hs

Executable synthi-llvm-speech
  If flag(buildExamples)
    Build-Depends:
      gnuplot >=0.5 && <0.6,
      pathtype,
      sox,
      synthesizer-llvm,
      synthesizer-core,
      numeric-prelude,
      storablevector,
      utility-ht,
      base >=4 && <5
  Else
    Buildable: False
  Default-Language: Haskell98
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
    If impl(ghc<8.0)
      GHC-Options: -fcontext-stack=1000
    Else
      GHC-Options: -freduction-depth=1000
  Main-Is:        src/Synthesizer/LLVM/Server/CausalPacked/SpeechExplore.hs

Test-Suite synthi-llvm-test
  Type: exitcode-stdio-1.0
  Build-Depends:
    synthesizer-llvm,

    llvm-extra,
    llvm-tf,
    tfp,
    synthesizer-core,
    storablevector >=0.2.6 && <0.3,
    numeric-prelude >=0.3 && <0.5,
    random,
    utility-ht,

    QuickCheck >=1 && <3,
    base >=4 && <5
  Default-Language: Haskell98
  GHC-Options:      -Wall
  GHC-Options:      -pgmlg++
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Default-Extensions: CPP
    If impl(ghc<8.0)
      GHC-Options: -fcontext-stack=1000
    Else
      GHC-Options: -freduction-depth=1000
  Hs-Source-Dirs: testsuite
  Main-Is:     Test/Main.hs
  Other-Modules:
    Test.Synthesizer.LLVM.RingBufferForward
    Test.Synthesizer.LLVM.Helix
    Test.Synthesizer.LLVM.Filter
    Test.Synthesizer.LLVM.Packed
    Test.Synthesizer.LLVM.Utility
    Test.Synthesizer.LLVM.Generator
