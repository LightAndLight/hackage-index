name:                sexp-grammar
version:             1.1.1
x-revision: 1
license:             BSD3
license-file:        LICENSE
author:              Eugene Smolanka, Sergey Vinokurov
maintainer:          Eugene Smolanka <esmolanka@gmail.com>, Sergey Vinokurov <serg.foo@gmail.com>
homepage:            https://github.com/esmolanka/sexp-grammar
category:            Language
build-type:          Simple
extra-source-files:  README.md
                     examples/Expr.hs
                     examples/Misc.hs
cabal-version:       >=1.10
synopsis:
  Invertible parsers for S-expressions
description:
  Invertible grammar combinators for serializing and deserializing from S-expessions
tested-with:   GHC == 7.8.3, GHC == 7.10.3

source-repository head
  type: git
  location: https://github.com/esmolanka/sexp-grammar

flag dev
  description: whether to build library in development mode with strict checks
  default:     False
  manual:      True

library
  hs-source-dirs:      src
  default-language:    Haskell2010
  ghc-options:         -Wall -fno-warn-name-shadowing -fno-warn-unused-do-bind
  if flag(dev)
    ghc-options:       -Werror
  exposed-modules:
    Language.Sexp
    Language.Sexp.Utils
    Language.SexpGrammar

  other-modules:
    Data.InvertibleGrammar
    Data.InvertibleGrammar.TH
    Data.StackPrism.ReverseTH
    Language.Sexp.Token
    Language.Sexp.Lexer
    Language.Sexp.Types
    Language.Sexp.Parser
    Language.Sexp.Pretty
    Language.SexpGrammar.Base
    Language.SexpGrammar.Class
    Language.SexpGrammar.Combinators

  build-depends:
      array
    , base >=4.7 && <5
    , containers
    , mtl >=2.1
    , scientific
    , semigroups
    , stack-prism
    , split
    , template-haskell < 2.11
    , text
    , wl-pprint-text < 1.2

  build-tools: alex, happy

test-suite sexp-grammar-test
  type:              exitcode-stdio-1.0
  build-depends:
      QuickCheck
    , base
    , scientific
    , semigroups
    , sexp-grammar
    , stack-prism
    , tasty
    , tasty-hunit
    , tasty-quickcheck
  main-is:           Main.hs
  hs-source-dirs:    test
  default-language:  Haskell2010
