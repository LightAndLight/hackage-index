cabal-version:      2.4
name:               purescript-cst
version:            0.2.0.0
license:            BSD-3-Clause
license-file:       LICENSE
copyright:
    (c) 2013-17 Phil Freeman, (c) 2014-19 Gary Burgess, (c) other contributors (see CONTRIBUTORS.md)

maintainer:
    Gary Burgess <gary.burgess@gmail.com>, Hardy Jones <jones3.hardy@gmail.com>, Harry Garrood <harry@garrood.me>, Christoph Hegemann <christoph.hegemann1337@gmail.com>, Liam Goodacre <goodacre.liam@gmail.com>, Nathan Faubion <nathan@n-son.com>

author:             Phil Freeman <paf31@cantab.net>
stability:          experimental
homepage:           http://www.purescript.org/
bug-reports:        https://github.com/purescript/purescript/issues
synopsis:           PureScript Programming Language Concrete Syntax Tree
description:        The parser for the PureScript programming language.
category:           Language
build-type:         Simple
extra-source-files: README.md

source-repository head
    type:     git
    location: https://github.com/purescript/purescript

library
    exposed-modules:
        Control.Monad.Supply
        Control.Monad.Supply.Class
        Language.PureScript.AST
        Language.PureScript.AST.Binders
        Language.PureScript.AST.Declarations
        Language.PureScript.AST.Declarations.ChainId
        Language.PureScript.AST.Exported
        Language.PureScript.AST.Literals
        Language.PureScript.AST.Operators
        Language.PureScript.AST.SourcePos
        Language.PureScript.AST.Traversals
        Language.PureScript.CST.Convert
        Language.PureScript.CST.Errors
        Language.PureScript.CST.Flatten
        Language.PureScript.CST.Layout
        Language.PureScript.CST.Lexer
        Language.PureScript.CST.Monad
        Language.PureScript.CST.Parser
        Language.PureScript.CST.Positions
        Language.PureScript.CST.Print
        Language.PureScript.CST.Traversals
        Language.PureScript.CST.Traversals.Type
        Language.PureScript.CST.Types
        Language.PureScript.CST.Utils
        Language.PureScript.Comments
        Language.PureScript.Constants.Prim
        Language.PureScript.Crash
        Language.PureScript.Environment
        Language.PureScript.Label
        Language.PureScript.Names
        Language.PureScript.PSString
        Language.PureScript.Roles
        Language.PureScript.Traversals
        Language.PureScript.TypeClassDictionaries
        Language.PureScript.Types

    build-tool-depends: happy:happy ==1.20.0
    hs-source-dirs:     src
    other-modules:
        Data.Text.PureScript
        Paths_purescript_cst

    autogen-modules:    Paths_purescript_cst
    default-language:   Haskell2010
    default-extensions:
        BangPatterns ConstraintKinds DataKinds DefaultSignatures
        DeriveFunctor DeriveFoldable DeriveTraversable DeriveGeneric
        DerivingStrategies EmptyDataDecls FlexibleContexts
        FlexibleInstances GeneralizedNewtypeDeriving KindSignatures
        LambdaCase MultiParamTypeClasses NamedFieldPuns NoImplicitPrelude
        PatternGuards PatternSynonyms RankNTypes RecordWildCards
        OverloadedStrings ScopedTypeVariables TupleSections TypeFamilies
        ViewPatterns

    ghc-options:        -Wall
    build-depends:
        aeson >=1.5.6.0 && <1.6,
        array >=0.5.4.0 && <0.6,
        base >=4.14.1.0 && <4.15,
        base-compat >=0.11.2 && <0.12,
        bytestring >=0.10.12.0 && <0.11,
        containers >=0.6.2.1 && <0.7,
        deepseq >=1.4.4.0 && <1.5,
        dlist >=0.8.0.8 && <0.9,
        filepath >=1.4.2.1 && <1.5,
        microlens >=0.4.11.2 && <0.5,
        mtl >=2.2.2 && <2.3,
        protolude >=0.3.0 && <0.4,
        scientific >=0.3.6.2 && <0.4,
        semigroups >=0.19.1 && <0.20,
        text >=1.2.4.1 && <1.3,
        serialise >=0.2.3.0 && <0.3,
        vector >=0.12.1.2 && <0.13
