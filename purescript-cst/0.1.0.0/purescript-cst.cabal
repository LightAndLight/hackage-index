cabal-version:      1.12
name:               purescript-cst
version:            0.1.0.0
license:            BSD3
license-file:       LICENSE
copyright:
    (c) 2013-17 Phil Freeman, (c) 2014-19 Gary Burgess, (c) other contributors (see CONTRIBUTORS.md)

maintainer:
    Gary Burgess <gary.burgess@gmail.com>, Hardy Jones <jones3.hardy@gmail.com>, Harry Garrood <harry@garrood.me>, Christoph Hegemann <christoph.hegemann1337@gmail.com>, Liam Goodacre <goodacre.liam@gmail.com>, Nathan Faubion <nathan@n-son.com>

author:             Phil Freeman <paf31@cantab.net>
stability:          experimental
homepage:           http://www.purescript.org/
bug-reports:        https://github.com/purescript/purescript/issues
synopsis:           PureScript Programming Language Concrete Syntax Tree
description:        The surface syntax of the PureScript Programming Language.
category:           Language
build-type:         Simple
extra-source-files:
    tests/purs/layout/AdoIn.out
    tests/purs/layout/CaseGuards.out
    tests/purs/layout/CaseWhere.out
    tests/purs/layout/ClassHead.out
    tests/purs/layout/Commas.out
    tests/purs/layout/Delimiter.out
    tests/purs/layout/DoLet.out
    tests/purs/layout/DoOperator.out
    tests/purs/layout/DoWhere.out
    tests/purs/layout/IfThenElseDo.out
    tests/purs/layout/InstanceChainElse.out
    tests/purs/layout/LetGuards.out
    tests/purs/layout/AdoIn.purs
    tests/purs/layout/CaseGuards.purs
    tests/purs/layout/CaseWhere.purs
    tests/purs/layout/ClassHead.purs
    tests/purs/layout/Commas.purs
    tests/purs/layout/Delimiter.purs
    tests/purs/layout/DoLet.purs
    tests/purs/layout/DoOperator.purs
    tests/purs/layout/DoWhere.purs
    tests/purs/layout/IfThenElseDo.purs
    tests/purs/layout/InstanceChainElse.purs
    tests/purs/layout/LetGuards.purs
    README.md

source-repository head
    type:     git
    location: https://github.com/purescript/purescript

library
    exposed-modules:
        Language.PureScript.CST.Convert
        Language.PureScript.CST.Errors
        Language.PureScript.CST.Flatten
        Language.PureScript.CST.Layout
        Language.PureScript.CST.Lexer
        Language.PureScript.CST.Monad
        Language.PureScript.CST.Parser
        Language.PureScript.CST.Positions
        Language.PureScript.CST.Print
        Language.PureScript.CST.Traversals
        Language.PureScript.CST.Traversals.Type
        Language.PureScript.CST.Types
        Language.PureScript.CST.Utils

    build-tools:        happy ==1.19.9
    hs-source-dirs:     src
    other-modules:      Data.Text.PureScript
    default-language:   Haskell2010
    default-extensions:
        BangPatterns ConstraintKinds DataKinds DefaultSignatures
        DeriveFunctor DeriveFoldable DeriveTraversable DeriveGeneric
        DerivingStrategies EmptyDataDecls FlexibleContexts
        FlexibleInstances GeneralizedNewtypeDeriving KindSignatures
        LambdaCase MultiParamTypeClasses NamedFieldPuns NoImplicitPrelude
        PatternGuards PatternSynonyms RankNTypes RecordWildCards
        OverloadedStrings ScopedTypeVariables TupleSections TypeFamilies
        ViewPatterns

    ghc-options:        -Wall -O2
    build-depends:
        array <0.6,
        base >=4.11 && <4.13,
        containers <0.7,
        dlist <0.9,
        purescript-ast <0.2,
        scientific >=0.3.4.9 && <0.4,
        semigroups >=0.16.2 && <0.19,
        text <1.3

test-suite tests
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    build-tools:        happy ==1.19.9
    hs-source-dirs:     tests
    other-modules:
        TestCst
        Paths_purescript_cst

    default-language:   Haskell2010
    default-extensions: NoImplicitPrelude LambdaCase OverloadedStrings
    ghc-options:        -Wall
    build-depends:
        array <0.6,
        base >=4.11 && <4.13,
        base-compat >=0.6.0 && <0.11,
        bytestring <0.11,
        containers <0.7,
        dlist <0.9,
        filepath <1.5,
        purescript-ast <0.2,
        purescript-cst -any,
        scientific >=0.3.4.9 && <0.4,
        semigroups >=0.16.2 && <0.19,
        tasty <1.3,
        tasty-golden <2.4,
        tasty-quickcheck <0.11,
        text <1.3
