Name:                prelude-compat
Version:             0.0.0.1
Synopsis:            Provide Prelude and Data.List with fixed content across GHC versions
Description:
  This package allows you to write warning-free code
  that compiles with versions of 'base' before and after AMP and FTP,
  that is, 'base' before and beginning with 4.8, respectively,
  and GHC before and beginning with 7.10, respectively.
  It serves three purposes:
  .
  * Prevent you from name clashes of FTP-Prelude
    with locally defined functions having names like '<*>', 'join', 'foldMap'.
  .
  * Prevent you from redundant import warnings
    if you manually import "Data.Monoid" or "Control.Applicative".
  .
  * Fix list functions to the list type, contrarily to the aim of the FTP.
    This way you are saved from @length (2,1) == 1@ and @maximum (2,1) == 1@,
    until you import @Data.Foldable@.
  .
  You should add
  .
  > import Prelude2010
  > import Prelude ()
  .
  to your modules.
  This way, you must change all affected modules.
  If you want to avoid this you may try the 'prelude2010' package
  or if you already import Prelude explicitly, you may try to add
  .
  > Default-Extensions: CPP, NoImplicitPrelude
  > CPP-Options: -DPrelude=Prelude2010
  .
  to your Cabal file.
  .
  In my opinion, this is the wrong way round.
  The presented Prelude2010 module should have been the one for GHC-7.10
  and the Prelude with added and generalized list functions
  should have been an additionally PreludeFTP,
  preferably exported by a separate package
  like all other alternate Prelude projects.
  But the purpose of the FTP was to save some import statements
  at the expense of blowing up the 'Foldable' class
  and prevent simple ways to write code that works
  with GHC version before and starting with GHC-7.10
  and that does not provoke warnings.
  .
  Related packages:
  .
  * 'base-compat': The opposite approach -
    Make future function definitions available in older GHC versions.
  .
  * 'haskell2010': Defines the Prelude for Haskell 2010.
    Unfortunately, 'haskell2010' is not available anymore since GHC-7.10,
    because of the AMP.
  .
  * 'numeric-prelude':
    It is intended to provide a refined numeric class hierarchy
    but it also provides a non-numeric subset of the Prelude
    that is more stable than the one of 'base'.
License:             BSD3
License-File:        LICENSE
Author:              Henning Thielemann
Maintainer:          haskell@henning-thielemann.de
Category:            Prelude, Haskell2010
Build-Type:          Simple
Cabal-Version:       >=1.10
Tested-With:         GHC==6.12.3, GHC==7.0.4, GHC==7.2.2
Tested-With:         GHC==7.4.2, GHC==7.6.3, GHC==7.8.4, GHC==7.10.3

Source-Repository this
  Tag:         0.0.0.1
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/prelude-compat

Source-Repository head
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/prelude-compat

Library
  Exposed-Modules:     Prelude2010, Data.List2010
  Other-Modules:       Catch
  Build-Depends:       base >=3 && <5
  Hs-Source-Dirs:      src
  If impl(ghc>=7.2.2)
    Hs-Source-Dirs: catch/new
  Else
    Hs-Source-Dirs: catch/old
  Default-Language:    Haskell98
  GHC-Options:         -Wall
