name:                argon
version:             0.2.0.0
synopsis:            Measure your code's complexity
homepage:            http://github.com/rubik/argon
bug-reports:         http://github.com/rubik/argon/issues
license:             ISC
license-file:        LICENSE
author:              Michele Lacchia
maintainer:          michelelacchia@gmail.com
copyright:           2015 Michele Lacchia
category:            Web
build-type:          Simple
cabal-version:       >=1.10
description:
    Argon performs static analysis on your code in order to compute cyclomatic
    complexity. It is a quantitative measure of the number of linearly
    indipendent paths through the code.
    .
    The intended usage is through Argon's executable, which accepts a list of
    file paths to analyze. The data can be optionally exported to JSON.
extra-source-files:
    stack.yaml
    README.md
    CHANGELOG.md
    USAGE.txt

library
  hs-source-dirs:      src
  exposed-modules:     Argon
  other-modules:       Argon.Parser
                       Argon.Visitor
                       Argon.Results
                       Argon.Formatters
                       Argon.Types
  build-depends:       base             >=4.7    && <5
                     , haskell-src-exts ==1.16.*
                     , cpphs            ==1.19.*
                     , uniplate         ==1.6.*
                     , ansi-terminal    ==0.6.*
                     , docopt           ==0.7.*
                     , aeson            ==0.8.*
                     , bytestring       ==0.10.*
  default-language:    Haskell2010
  ghc-options:         -Wall

executable argon
  hs-source-dirs:      app
  main-is:             Main.hs
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N
  build-depends:       base             >=4.7    && <5
                     , haskell-src-exts ==1.16.*
                     , cpphs            ==1.19.*
                     , uniplate         ==1.6.*
                     , ansi-terminal    ==0.6.*
                     , docopt           ==0.7.*
                     , aeson            ==0.8.*
                     , bytestring       ==0.10.*
                     , argon            -any
  default-language:    Haskell2010
  ghc-options:         -Wall

test-suite argon-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test src
  main-is:             Spec.hs
  build-depends:       base             >=4.7    && <5
                     , argon            -any
                     , hspec            ==2.1.*
                     , QuickCheck       -any
                     , haskell-src-exts ==1.16.*
                     , cpphs            ==1.19.*
                     , uniplate         ==1.6.*
                     , ansi-terminal    ==0.6.*
                     , aeson            ==0.8.*
                     , bytestring       ==0.10.*
  ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N
  default-language:    Haskell2010
  other-modules:       Argon
                       ArgonSpec
                       Argon.Parser
                       Argon.Visitor
                       Argon.Results
                       Argon.Formatters
                       Argon.Types

test-suite style
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             HLint.hs
  build-depends:       base  ==4.*
                     , hlint ==1.*
  default-language:    Haskell2010
  ghc-options:         -Wall

source-repository head
  type:     git
  location: https://github.com/rubik/argon
