name:                argon
version:             0.3.0.0
x-revision: 1
synopsis:            Measure your code's complexity
homepage:            http://github.com/rubik/argon
bug-reports:         http://github.com/rubik/argon/issues
license:             ISC
license-file:        LICENSE
author:              Michele Lacchia
maintainer:          michelelacchia@gmail.com
copyright:           2015 Michele Lacchia
category:            Web
build-type:          Simple
cabal-version:       >=1.10
description:
    Argon performs static analysis on your code in order to compute cyclomatic
    complexity. It is a quantitative measure of the number of linearly
    indipendent paths through the code.
    .
    The intended usage is through Argon's executable, which accepts a list of
    file paths to analyze. The data can be optionally exported to JSON.
extra-source-files:
    stack.yaml
    README.md
    CHANGELOG.md
    USAGE.txt

library
  hs-source-dirs:      src
  exposed-modules:     Argon
  other-modules:       Argon.Parser
                       Argon.Visitor
                       Argon.Results
                       Argon.Formatters
                       Argon.Types
                       Argon.Preprocess
                       Argon.Loc
  build-depends:       base             >=4.7    && <5
                     , ansi-terminal    >=0.6
                     , aeson            >=0.8 && <0.11
                     , bytestring       >=0.10
                     , ghc              >=7.10.2 && <8
                     , ghc-paths        >=0.1
                     , syb              >=0.5
  default-language:    Haskell2010
  ghc-options:         -Wall
  if impl(ghc < 7.10.2)
    buildable: False

executable argon
  hs-source-dirs:      app
  main-is:             Main.hs
  ghc-options:         -Wall
  build-depends:       base             >=4.7    && <5
                     , docopt           >=0.7
                     , pathwalk         >=0.3
                     , filepath         >=1.4
                     , directory        >=1.2
                     , containers       >=0.5
                     , argon            -any
  default-language:    Haskell2010
  if impl(ghc < 7.10.2)
    buildable: False

test-suite argon-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test src
  main-is:             Spec.hs
  build-depends:       base             >=4.7    && <5
                     , ansi-terminal    >=0.6
                     , aeson            >=0.8
                     , bytestring       >=0.10
                     , ghc              >=7.10.2 && <8
                     , ghc-paths        >=0.1
                     , syb              >=0.5
                     , hspec            >=2.1
                     , QuickCheck       -any
                     , filepath         >=1.4
                     , argon            -any
  ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N
  if impl(ghc < 7.10.2)
    buildable: False
  default-language:    Haskell2010
  other-modules:       Argon
                       ArgonSpec
                       Argon.Parser
                       Argon.Visitor
                       Argon.Results
                       Argon.Formatters
                       Argon.Types
                       Argon.Preprocess
                       Argon.Loc

test-suite style
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             HLint.hs
  build-depends:       base  ==4.*
                     , hlint ==1.*
  default-language:    Haskell2010
  ghc-options:         -Wall

source-repository head
  type:     git
  location: https://github.com/rubik/argon
