name:                argon
version:             0.4.0.0
x-revision: 1
synopsis:            Measure your code's complexity
homepage:            http://github.com/rubik/argon
bug-reports:         http://github.com/rubik/argon/issues
license:             ISC
license-file:        LICENSE
author:              Michele Lacchia
maintainer:          michelelacchia@gmail.com
copyright:           2015 Michele Lacchia
category:            Development, Static Analysis
build-type:          Simple
cabal-version:       >=1.10
description:
    Argon performs static analysis on your code in order to compute cyclomatic
    complexity. It is a quantitative measure of the number of linearly
    indipendent paths through the code.
    .
    The intended usage is through Argon's executable, which accepts a list of
    file paths to analyze. The data can be optionally exported to JSON.
extra-source-files:
    stack.yaml
    stack-7.8.yaml
    README.md
    CHANGELOG.md
    USAGE.txt
    test/data/*.hs
    test/tree/*.hs
    test/tree/*.txt
    test/tree/sub/*.hs
    test/tree/sub2/*.hs
tested-with: GHC >= 7.8 && < 8

library
  hs-source-dirs:      src
  exposed-modules:     Argon
  other-modules:       Argon.Parser
                       Argon.Visitor
                       Argon.Results
                       Argon.Formatters
                       Argon.Types
                       Argon.Preprocess
                       Argon.Loc
                       Argon.Cabal
                       Argon.SYB.Utils
                       Argon.Walker
  build-depends:       base             >=4.7    && <5
                     , ansi-terminal    >=0.6
                     , aeson            >=0.8 && <0.11
                     , bytestring       >=0.10
                     , pipes            >=4.1
                     , pipes-group      >=1.0
                     , lens-simple      >=0.1
                     , ghc              >=7.8    && <8
                     , ghc-paths        >=0.1
                     , ghc-syb-utils    >=0.2
                     , syb              >=0.4
                     , Cabal            >=1.18
                     , containers       >=0.5
                     , pathwalk         >=0.3
                     , filepath         >=1.3
                     , directory        >=1.2
  default-language:    Haskell2010
  ghc-options:         -Wall
  if impl(ghc < 7.8)
    buildable: False

executable argon
  hs-source-dirs:      app
  main-is:             Main.hs
  ghc-options:         -Wall
  build-depends:       base             >=4.7    && <5
                     , docopt           >=0.7
                     , pipes            >=4.1
                     , argon            -any
  if impl(ghc < 7.10)
    build-depends:     pathwalk         >=0.3.1.2
  default-language:    Haskell2010
  if impl(ghc < 7.8)
    buildable: False

test-suite argon-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test src
  main-is:             Spec.hs
  build-depends:       base             >=4.7    && <5
                     , ansi-terminal    >=0.6
                     , aeson            >=0.8
                     , bytestring       >=0.10
                     , pipes            >=4.1
                     , pipes-group      >=1.0
                     , lens-simple      >=0.1
                     , ghc              >=7.8    && <8
                     , ghc-paths        >=0.1
                     , ghc-syb-utils    >=0.2
                     , syb              >=0.4
                     , Cabal            >=1.18
                     , containers       >=0.5
                     , hspec            >=2.1
                     , QuickCheck       -any
                     , filepath         >=1.3
                     , docopt           >=0.7
                     , pathwalk         >=0.3
                     , directory        >=1.2
  ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N
  if impl(ghc < 7.8)
    buildable: False
  default-language:    Haskell2010
  other-modules:       Argon
                       ArgonSpec
                       Argon.Parser
                       Argon.Visitor
                       Argon.Results
                       Argon.Formatters
                       Argon.Types
                       Argon.Preprocess
                       Argon.Loc
                       Argon.Cabal
                       Argon.SYB.Utils
                       Argon.Walker

test-suite style
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             HLint.hs
  build-depends:       base  ==4.*
                     , hlint ==1.*
  default-language:    Haskell2010
  ghc-options:         -Wall

source-repository head
  type:     git
  location: https://github.com/rubik/argon
