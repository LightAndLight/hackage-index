cabal-version:            2.2
name:                     capnp
version:                  0.1.0.0
x-revision: 1
stability:                Experimental
category:                 Data, Serialization
copyright:                2016-2018 haskell-capnp contributors (see CONTRIBUTORS file).
author:                   Ian Denhardt
maintainer:               ian@zenhack.net
license:                  MIT
license-file:             LICENSE.md
homepage:                 https://github.com/zenhack/haskell-capnp
bug-reports:              https://github.com/zenhack/haskell-capnp/issues
synopsis:                 Cap'n Proto for Haskell
description:
  A native Haskell implementation of the Cap'N Proto cerialization format.
  .
  This library is currently serialization-only. RPC support is planned but not
  yet implemented. It works, but bear in mind that the API is considered
  unstable, and is likely to change to accomodate RPC, facilitate improved
  performance, etc.
  .
  The 'Data.Capnp.Tutorial' module is the best place to start reading; the
  reference documentation can seem bewildering without that context.
build-type:               Simple
extra-source-files:
    README.md
  , CHANGELOG.md
  , CONTRIBUTING.md
  , ci/README.md
  , ci/Dockerfile
  , core-schema/README.md
  , core-schema/capnp/c++.capnp
  , core-schema/capnp/json.capnp
  , core-schema/capnp/persistent.capnp
  , core-schema/capnp/rpc.capnp
  , core-schema/capnp/rpc-twoparty.capnp
  , core-schema/capnp/schema.capnp
  , scripts/format.sh
  , scripts/gen-basic-instances.hs
  , scripts/hlint.sh
  , scripts/README.md
  , scripts/regen.sh
  , .stylish-haskell.yaml
  , .gitattributes
  , .gitignore
  , .gitlab-ci.yml
  -- FIXME: add a CHANGELOG.md and add it to this list
tested-with:
  -- Our Gitlab CI uses this version:
    GHC == 8.2.1
  -- @zenhack currently uses this version:
  , GHC == 8.4.3

--------------------------------------------------------------------------------

source-repository head
    type:                 git
    branch:               master
    location:             https://github.com/zenhack/haskell-capnp.git

--------------------------------------------------------------------------------

common shared-opts
  build-depends:
        base                              >= 4.10  && < 5.0
      , array                             >= 0.5  && < 0.6
      , bytes                             >= 0.15.4 && < 0.16
      , bytestring                        >= 0.10 && < 0.11
      , exceptions                        >= 0.10.0 && < 0.11
      , mtl                               >= 2.2.2 && < 2.3
      , primitive                         >= 0.6.3 && < 0.7
      , reinterpret-cast                  >= 0.1.0 && < 0.2
      , text                              >= 1.2  && < 2.0
      , transformers                      >= 0.5.2 && < 0.6
      , vector                            >= 0.12.0 && < 0.13
  ghc-options:
    -Wnoncanonical-monad-instances
    -Wincomplete-patterns
    -Wunused-imports
  default-language:     Haskell2010

--------------------------------------------------------------------------------

library
    import: shared-opts
    hs-source-dirs:       lib
    exposed-modules:
        Data.Capnp
      , Data.Capnp.Pure
      , Data.Capnp.Address
      , Data.Capnp.Basics
      , Data.Capnp.Basics.Pure
      , Data.Capnp.Bits
      , Data.Capnp.Classes
      , Data.Capnp.Errors
      , Data.Capnp.GenHelpers
      , Data.Capnp.GenHelpers.Pure
      , Data.Capnp.IO
      , Data.Capnp.Message
      , Data.Capnp.Pointer
      , Data.Capnp.Untyped
      , Data.Capnp.Untyped.Pure
      , Data.Capnp.TraversalLimit
      , Data.Capnp.Tutorial
      -- BEGIN GENERATED SCHEMA MODULES
      , Capnp.Capnp.Cxx.Pure
      , Capnp.Capnp.Json.Pure
      , Capnp.Capnp.Persistent.Pure
      , Capnp.Capnp.Rpc.Pure
      , Capnp.Capnp.RpcTwoparty.Pure
      , Capnp.Capnp.Schema.Pure
      , Capnp.Capnp.Cxx
      , Capnp.Capnp.Json
      , Capnp.Capnp.Persistent
      , Capnp.Capnp.Rpc
      , Capnp.Capnp.RpcTwoparty
      , Capnp.Capnp.Schema
      , Capnp.ById.Xbdf87d7bb8304e81.Pure
      , Capnp.ById.X8ef99297a43a5e34.Pure
      , Capnp.ById.Xb8630836983feed7.Pure
      , Capnp.ById.Xb312981b2552a250.Pure
      , Capnp.ById.Xa184c7885cdaf2a1.Pure
      , Capnp.ById.Xa93fc509624c72d9.Pure
      , Capnp.ById.Xbdf87d7bb8304e81
      , Capnp.ById.X8ef99297a43a5e34
      , Capnp.ById.Xb8630836983feed7
      , Capnp.ById.Xb312981b2552a250
      , Capnp.ById.Xa184c7885cdaf2a1
      , Capnp.ById.Xa93fc509624c72d9
      -- END GENERATED SCHEMA MODULES
    other-modules:
        Internal.Util
      , Internal.Gen.Instances
      , Codec.Capnp
    -- other-extensions:
    build-depends:
        cpu                               >= 0.1.2 && < 0.2
      , data-default                      >= 0.7.1 && < 0.8
      , data-default-instances-vector     >= 0.0.1 && < 0.1

--------------------------------------------------------------------------------

executable capnpc-haskell
    import: shared-opts
    main-is:              Main.hs
    other-modules:
        FrontEnd
      , IR
      , Backends.Common
      , Backends.Pure
      , Backends.Raw
      , Util
    hs-source-dirs:       exe/capnpc-haskell
    build-depends:
        capnp
      , binary                            >= 0.8.5 && < 0.9
      , cereal                            >= 0.5.5 && < 0.6
      , containers                        >= 0.5.10 && < 0.6
      , directory                         >= 1.3.0 && < 1.4
      , dlist                             >= 0.8.0 && < 0.9
      , filepath                          >= 1.4.1 && < 1.5
      , utf8-string                       >= 1.0.1 && < 1.1
      , wl-pprint-text                    >= 1.2 && <1.3

--------------------------------------------------------------------------------

test-suite simple-tests
    import: shared-opts
    type:                 exitcode-stdio-1.0
    main-is:              Main.hs
    hs-source-dirs:       tests/simple-tests
    other-modules:
        Tests.Module.Capnp.Capnp.Schema
      , Tests.Module.Capnp.Capnp.Schema.Pure
      , Tests.Module.Data.Capnp.Untyped
      , Tests.Module.Data.Capnp.Untyped.Pure
      , Tests.Module.Data.Capnp.Pointer
      , Tests.Module.Data.Capnp.Bits
      , Tests.WalkSchemaCodeGenRequest
      , Tests.SchemaGeneration
      , Tests.SchemaQuickCheck
      , Tests.Util
    build-depends:
        capnp
      , data-default
      , process
      , process-extras
      , QuickCheck
      , quickcheck-io
      , quickcheck-instances
      , HUnit
      , test-framework
      , test-framework-hunit
      , test-framework-quickcheck2
      , binary
      , directory
      , resourcet
      , heredoc
      , deepseq
      , pretty-show
