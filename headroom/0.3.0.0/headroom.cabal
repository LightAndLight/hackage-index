cabal-version:      2.2
name:               headroom
version:            0.3.0.0
license:            BSD-3-Clause
license-file:       LICENSE
copyright:          Copyright (c) 2019-2020 Vaclav Svejcar
maintainer:         vaclav.svejcar@gmail.com
author:             Vaclav Svejcar
homepage:           https://github.com/vaclavsvejcar/headroom
bug-reports:        https://github.com/vaclavsvejcar/headroom/issues
synopsis:           License Header Manager
description:
    Would you like to have nice, up-to-date license/copyright headers in your source code files but hate to manage them by hand? Then Headroom is the right tool for you! Now you can define your license header as Mustache template, put all the variables (such as author's name, year, etc.) into the YAML config file and Headroom will take care to add such license headers to all your source code files.

category:           Utils
build-type:         Simple
extra-source-files:
    CHANGELOG.md
    LICENSE
    README.md
    embedded/config-file.yaml
    embedded/default-config.yaml
    embedded/license/apache2/c.mustache
    embedded/license/apache2/cpp.mustache
    embedded/license/apache2/css.mustache
    embedded/license/apache2/haskell.mustache
    embedded/license/apache2/html.mustache
    embedded/license/apache2/java.mustache
    embedded/license/apache2/js.mustache
    embedded/license/apache2/rust.mustache
    embedded/license/apache2/scala.mustache
    embedded/license/apache2/shell.mustache
    embedded/license/bsd3/c.mustache
    embedded/license/bsd3/cpp.mustache
    embedded/license/bsd3/css.mustache
    embedded/license/bsd3/haskell.mustache
    embedded/license/bsd3/html.mustache
    embedded/license/bsd3/java.mustache
    embedded/license/bsd3/js.mustache
    embedded/license/bsd3/rust.mustache
    embedded/license/bsd3/scala.mustache
    embedded/license/bsd3/shell.mustache
    embedded/license/gpl2/c.mustache
    embedded/license/gpl2/cpp.mustache
    embedded/license/gpl2/css.mustache
    embedded/license/gpl2/haskell.mustache
    embedded/license/gpl2/html.mustache
    embedded/license/gpl2/java.mustache
    embedded/license/gpl2/js.mustache
    embedded/license/gpl2/rust.mustache
    embedded/license/gpl2/scala.mustache
    embedded/license/gpl2/shell.mustache
    embedded/license/gpl3/c.mustache
    embedded/license/gpl3/cpp.mustache
    embedded/license/gpl3/css.mustache
    embedded/license/gpl3/haskell.mustache
    embedded/license/gpl3/html.mustache
    embedded/license/gpl3/java.mustache
    embedded/license/gpl3/js.mustache
    embedded/license/gpl3/rust.mustache
    embedded/license/gpl3/scala.mustache
    embedded/license/gpl3/shell.mustache
    embedded/license/mit/c.mustache
    embedded/license/mit/cpp.mustache
    embedded/license/mit/css.mustache
    embedded/license/mit/haskell.mustache
    embedded/license/mit/html.mustache
    embedded/license/mit/java.mustache
    embedded/license/mit/js.mustache
    embedded/license/mit/rust.mustache
    embedded/license/mit/scala.mustache
    embedded/license/mit/shell.mustache
    embedded/license/mpl2/c.mustache
    embedded/license/mpl2/cpp.mustache
    embedded/license/mpl2/css.mustache
    embedded/license/mpl2/haskell.mustache
    embedded/license/mpl2/html.mustache
    embedded/license/mpl2/java.mustache
    embedded/license/mpl2/js.mustache
    embedded/license/mpl2/rust.mustache
    embedded/license/mpl2/scala.mustache
    embedded/license/mpl2/shell.mustache
    test-data/test-template.mustache
    test-data/code-samples/c/sample1.c
    test-data/code-samples/c/sample2.c
    test-data/code-samples/cpp/sample1.cpp
    test-data/code-samples/cpp/sample2.cpp
    test-data/code-samples/css/sample1.css
    test-data/code-samples/css/sample2.css
    test-data/code-samples/haskell/full.hs
    test-data/code-samples/haskell/header.hs
    test-data/code-samples/haskell/sample1.hs
    test-data/code-samples/haskell/sample2.hs
    test-data/code-samples/html/sample1.html
    test-data/code-samples/html/sample2.html
    test-data/code-samples/java/sample1.java
    test-data/code-samples/java/sample2.java
    test-data/code-samples/js/sample1.js
    test-data/code-samples/js/sample2.js
    test-data/code-samples/rust/sample1.rs
    test-data/code-samples/scala/sample1.scala
    test-data/code-samples/scala/sample2.scala
    test-data/code-samples/shell/sample1.sh
    test-data/configs/full.yaml
    test-data/templates/haskell.mustache
    test-data/test-traverse/a.html
    test-data/test-traverse/foo/b.html
    test-data/test-traverse/foo/test.xml
    test-data/test-traverse/foo/bar/c.html

source-repository head
    type:     git
    location: https://github.com/vaclavsvejcar/headroom

library
    exposed-modules:
        Headroom.Command
        Headroom.Command.Gen
        Headroom.Command.Init
        Headroom.Command.Readers
        Headroom.Command.Run
        Headroom.Command.Types
        Headroom.Command.Utils
        Headroom.Configuration
        Headroom.Configuration.Types
        Headroom.Data.EnumExtra
        Headroom.Data.Has
        Headroom.Data.Lens
        Headroom.Data.Regex
        Headroom.Data.TextExtra
        Headroom.Embedded
        Headroom.Ext
        Headroom.Ext.Haskell
        Headroom.Ext.Haskell.Haddock
        Headroom.FileSupport
        Headroom.FileSupport.Types
        Headroom.FileSystem
        Headroom.FileType
        Headroom.FileType.Types
        Headroom.HeaderFn
        Headroom.HeaderFn.Types
        Headroom.HeaderFn.UpdateCopyright
        Headroom.Meta
        Headroom.Serialization
        Headroom.Template
        Headroom.Template.Mustache
        Headroom.Types
        Headroom.UI
        Headroom.UI.Progress
        Headroom.Variables
        Headroom.Variables.Types

    hs-source-dirs:   src
    other-modules:    Paths_headroom
    autogen-modules:  Paths_headroom
    default-language: Haskell2010
    ghc-options:
        -optP-Wno-nonportable-include-path -Wall -Wcompat -Widentities
        -Wincomplete-record-updates -Wincomplete-uni-patterns
        -Wpartial-fields -Wredundant-constraints
        -Werror=incomplete-patterns

    build-depends:
        aeson >=1.4.7.1,
        base >=4.7 && <5,
        data-default-class >=0.1.2.0,
        either >=5.0.1.1,
        file-embed >=0.0.11.2,
        microlens >=0.4.11.2,
        microlens-th >=0.4.3.5,
        mustache >=2.3.1,
        optparse-applicative >=0.15.1.0,
        pcre-heavy >=1.0.0.2,
        pcre-light >=0.4.1.0,
        rio >=0.1.17.0,
        template-haskell >=2.15.0.0,
        time >=1.9.3,
        yaml >=0.11.4.0

executable headroom
    main-is:          Main.hs
    hs-source-dirs:   app
    other-modules:    Paths_headroom
    default-language: Haskell2010
    ghc-options:
        -optP-Wno-nonportable-include-path -Wall -Wcompat -Widentities
        -Wincomplete-record-updates -Wincomplete-uni-patterns
        -Wpartial-fields -Wredundant-constraints
        -Werror=incomplete-patterns -threaded -rtsopts -with-rtsopts=-N

    build-depends:
        base >=4.7 && <5,
        headroom -any,
        optparse-applicative >=0.15.1.0,
        rio >=0.1.17.0

test-suite doctest
    type:             exitcode-stdio-1.0
    main-is:          Main.hs
    hs-source-dirs:   doctest
    other-modules:    Paths_headroom
    default-language: Haskell2010
    ghc-options:
        -optP-Wno-nonportable-include-path -Wall -Wcompat -Widentities
        -Wincomplete-record-updates -Wincomplete-uni-patterns
        -Wpartial-fields -Wredundant-constraints
        -Werror=incomplete-patterns

    build-depends:
        base >=4.7 && <5,
        doctest >=0.16.3,
        optparse-applicative >=0.15.1.0,
        rio >=0.1.17.0

test-suite spec
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   test
    other-modules:
        Headroom.Command.InitSpec
        Headroom.Command.ReadersSpec
        Headroom.Command.RunSpec
        Headroom.ConfigurationSpec
        Headroom.Data.EnumExtraSpec
        Headroom.Data.RegexSpec
        Headroom.Data.TextExtraSpec
        Headroom.Ext.Haskell.HaddockSpec
        Headroom.Ext.HaskellSpec
        Headroom.ExtSpec
        Headroom.FileSupportSpec
        Headroom.FileSystemSpec
        Headroom.FileTypeSpec
        Headroom.HeaderFn.TypesSpec
        Headroom.HeaderFn.UpdateCopyrightSpec
        Headroom.HeaderFnSpec
        Headroom.SerializationSpec
        Headroom.Template.MustacheSpec
        Headroom.TypesSpec
        Headroom.UI.ProgressSpec
        Headroom.VariablesSpec
        Test.Utils
        Paths_headroom

    default-language: Haskell2010
    ghc-options:
        -optP-Wno-nonportable-include-path -Wall -Wcompat -Widentities
        -Wincomplete-record-updates -Wincomplete-uni-patterns
        -Wpartial-fields -Wredundant-constraints
        -Werror=incomplete-patterns

    build-depends:
        QuickCheck >=2.13.2,
        aeson >=1.4.7.1,
        base >=4.7 && <5,
        headroom -any,
        hspec >=2.7.1,
        optparse-applicative >=0.15.1.0,
        pcre-light >=0.4.1.0,
        rio >=0.1.17.0,
        time >=1.9.3
