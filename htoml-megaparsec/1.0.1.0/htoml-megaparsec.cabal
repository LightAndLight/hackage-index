name:                     htoml-megaparsec
version:                  1.0.1.0
x-revision: 1
synopsis:                 Parser for TOML files
description:              TOML is an obvious and minimal format for config files.
                          This package provides a TOML parser
                          built with the Megaparsec. It exposes a JSON
                          interface using Aeson.
homepage:                 https://github.com/vmchale/htoml-megaparsec
bug-reports:              https://github.com/vmchale/htoml-megaparsec/issues
license:                  BSD3
license-file:             LICENSE
copyright:                (c) 2013-2016 Cies Breijs, 2017 Vanessa McHale
author:                   Cies Breijs, Vanessa McHale
maintainer:               Vanessa McHale <vanessa.mchale@reconfigure.io>
category:                 Data, Text, Configuration, Language, TOML
build-type:               Simple
cabal-version:            >= 1.18
extra-source-files:       test/BurntSushi/fetch-toml-tests.sh
                        , test/BurntSushi/valid/*.toml
                        , test/BurntSushi/valid/*.json
                        , test/BurntSushi/invalid/*.toml
                        , benchmarks/example.toml
                        , benchmarks/repeated.toml

Extra-doc-files:          README.md
                        , CHANGES.md

source-repository head
  type:                   git
  location:               https://github.com/cies/htoml.git

library
  exposed-modules:        Text.Toml
                        , Text.Toml.Parser
                        , Text.Toml.Types
  hs-source-dirs:         src
  default-language:       Haskell2010
  build-depends:          base                   >= 4.8    && < 5
                        , containers             >= 0.5
                        , megaparsec             >= 6.0.0  && < 6.4.0
                        , unordered-containers   >= 0.2
                        , vector                 >= 0.10
                        , aeson                  >= 0.8
                        , text                   >= 1.0    && < 2
                        , mtl                    >= 2.2
                        , composition-prelude    >= 0.1.1.0
                        , deepseq
                        , time                   -any
                        , old-locale             -any
  if impl(ghc >= 8.0)
    ghc-options:          -Wincomplete-uni-patterns -Wincomplete-record-updates
  ghc-options:            -Wall

test-suite htoml-test
  hs-source-dirs:         test
  ghc-options:            -Wall -threaded -rtsopts -with-rtsopts=-N
  main-is:                Test.hs
  other-modules:          BurntSushi
                        , JSON
                        , Text.Toml.Parser.Spec
  type:                   exitcode-stdio-1.0
  default-language:       Haskell2010
  build-depends:          base
                        , megaparsec
                        , containers
                        , unordered-containers
                        , vector
                        , aeson
                        , text
                        , time
                        , htoml-megaparsec
                        , bytestring
                        , file-embed
                        , tasty
                        , tasty-hspec
                        , tasty-hunit

benchmark benchmarks
  hs-source-dirs:         benchmarks .
  ghc-options:            -O2 -Wall -threaded -rtsopts -with-rtsopts=-N
  main-is:                Benchmarks.hs
  type:                   exitcode-stdio-1.0
  default-language:       Haskell2010
  build-depends:          base
                        , parsec
                        , containers
                        , unordered-containers
                        , vector
                        , aeson
                        , text
                        , time
			  -- from here non-lib deps
                        , htoml-megaparsec
                        , criterion
