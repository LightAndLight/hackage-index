name:           Allure
-- The package version. See the Haskell package versioning policy (PVP)
-- for standards guiding when and how versions should be incremented.
-- http://www.haskell.org/haskellwiki/Package_versioning_policy
-- PVP summary:+-+------- breaking API changes
--             | | +----- minor or non-breaking API additions
--             | | | +--- code changes with no API change
version:       0.6.1.0
synopsis:      Near-future Sci-Fi roguelike and tactical squad game
description:   Allure of the Stars
               is a near-future Sci-Fi roguelike and tactical squad game.
               Try out the browser version of Allure of the Stars at
               <http://allureofthestars.com/play>
               (It runs fastest on Chrome. Keyboard commands and savefiles
               are supported only on recent enough versions of browsers.
               Mouse should work everywhere.)
               .
               <<https://raw.githubusercontent.com/AllureOfTheStars/media/master/screenshot/crawl-0.6.0.0-8x8xb.png>>
               .
               Please see the changelog file for recent improvements
               and the issue tracker for short-term plans. Long term goals
               are high replayability and auto-balancing through procedural
               content generation and persistent content modification
               based on player behaviour. Contributions are welcome.
               .
               The game is written using the LambdaHack roguelike game engine
               available at <http://hackage.haskell.org/package/LambdaHack>.
homepage:      http://allureofthestars.com
bug-reports:   http://github.com/AllureOfTheStars/Allure/issues
license:       AGPL-3
license-file:  LICENSE
tested-with:   GHC >= 7.10 && <= 8.2
data-files:    GameDefinition/config.ui.default,
               GameDefinition/fonts/16x16x.fon,
               GameDefinition/fonts/8x8xb.fon,
               GameDefinition/fonts/8x8x.fon,
               GameDefinition/fonts/LICENSE.16x16x,
               GameDefinition/fonts/Fix15Mono-Bold.woff,
               GameDefinition/fonts/LICENSE.Fix15Mono-Bold,
               GameDefinition/PLAYING.md,
               GameDefinition/InGameHelp.txt,
               README.md,
               CHANGELOG.md,
               LICENSE,
               CREDITS
extra-source-files: GameDefinition/MainMenu.ascii, Makefile
author:        Andres Loeh, Mikolaj Konarski and others
maintainer:    Mikolaj Konarski <mikolaj.konarski@funktory.com>
category:      Game
build-type:    Simple
cabal-version: >= 1.10

source-repository head
  type:               git
  location:           git://github.com/AllureOfTheStars/Allure.git

executable Allure
  hs-source-dirs:     GameDefinition
  main-is:            Main.hs
  other-modules:      Client.UI.Content.KeyKind,
                      Content.CaveKind,
                      Content.ItemKind,
                      Content.ItemKindEmbed,
                      Content.ItemKindActor,
                      Content.ItemKindOrgan,
                      Content.ItemKindBlast,
                      Content.ItemKindTemporary,
                      Content.ModeKind,
                      Content.ModeKindPlayer,
                      Content.PlaceKind,
                      Content.RuleKind,
                      Content.TileKind,
                      TieKnot,
                      Paths_Allure
  build-depends:      LambdaHack >= 0.6.1.0 && < 0.6.2.0,
                      template-haskell >= 2.6,

                      async      >= 2,
                      base       >= 4 && < 99,
                      containers >= 0.5.3.0,
                      enummapset-th >= 0.6.0.0,
                      filepath   >= 1.2.0.1,
                      random     >= 1.1,
                      text       >= 0.11.2.3

  default-language:   Haskell2010
  default-extensions: MonoLocalBinds, ScopedTypeVariables, OverloadedStrings
                      BangPatterns, RecordWildCards, NamedFieldPuns, MultiWayIf
  other-extensions:   TemplateHaskell
  ghc-options:        -Wall -fwarn-orphans -fwarn-tabs -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates -fwarn-unrecognised-pragmas
  ghc-options:        -fno-warn-implicit-prelude -fno-ignore-asserts -fexpose-all-unfoldings -fspecialise-aggressively
  ghc-options:        -threaded -rtsopts

  if impl(ghcjs) {
-- This is the largest GHCJS_BUSY_YIELD value that does not cause dropped frames
-- on my machine with default --maxFps.
    cpp-options:      -DGHCJS_BUSY_YIELD=50
-- Minimize median lag at the cost of occasional huge lag when GC kicks in
-- (and some of the GCs fit into idle time, while the player ponders
-- or game is being saved):
    ghc-options:      "-with-rtsopts=-A99m"
  } else {
    build-depends:    zlib >= 0.5.3.1
-- The -A options makes it slightly faster, especially with short sessions:
    ghc-options:      "-with-rtsopts=-A99m -K1000K"
-- TODO: get back to -K1K when I can use pretty-1.1.3.4 (TH depends on an old one)
  }

test-suite test
  type:               exitcode-stdio-1.0
  hs-source-dirs:     GameDefinition, test
  main-is:            test.hs
  other-modules:      Client.UI.Content.KeyKind,
                      Content.CaveKind,
                      Content.ItemKind,
                      Content.ItemKindEmbed,
                      Content.ItemKindActor,
                      Content.ItemKindOrgan,
                      Content.ItemKindBlast,
                      Content.ItemKindTemporary,
                      Content.ModeKind,
                      Content.ModeKindPlayer,
                      Content.PlaceKind,
                      Content.RuleKind,
                      Content.TileKind,
                      TieKnot,
                      Paths_Allure
  build-depends:      LambdaHack,
                      template-haskell >= 2.6,

                      base       >= 4 && < 99,
                      containers >= 0.5.3.0,
                      enummapset-th >= 0.6.0.0,
                      filepath   >= 1.2.0.1,
                      random     >= 1.1,
                      text       >= 0.11.2.3

  default-language:   Haskell2010
  default-extensions: MonoLocalBinds, ScopedTypeVariables, OverloadedStrings
                      BangPatterns, RecordWildCards, NamedFieldPuns, MultiWayIf
  other-extensions:   TemplateHaskell
  ghc-options:        -Wall -fwarn-orphans -fwarn-tabs -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates -fwarn-unrecognised-pragmas
  ghc-options:        -fno-warn-implicit-prelude -fno-ignore-asserts -fexpose-all-unfoldings -fspecialise-aggressively
  ghc-options:        -threaded -rtsopts

  if impl(ghcjs) {
-- This is the largest GHCJS_BUSY_YIELD value that does not cause dropped frames
-- on my machine with default --maxFps.
    cpp-options:      -DGHCJS_BUSY_YIELD=50
    ghc-options:      "-with-rtsopts=-A99m"
  } else {
    build-depends:    zlib >= 0.5.3.1
    ghc-options:      "-with-rtsopts=-A99m -K1000K"
-- get back to -K1K when I can use pretty-1.1.3.4 (TH depends on an old one)
  }
