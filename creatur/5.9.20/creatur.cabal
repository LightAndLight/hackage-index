name: creatur
version: 5.9.20
cabal-version: >=1.8
build-type: Simple
license: BSD3
license-file: LICENSE
copyright: (c) Amy de Buitléir 2010-2016
maintainer: amy@nualeargais.ie
stability: experimental
homepage: https://github.com/mhwombat/creatur
bug-reports: https://github.com/mhwombat/creatur/issues
synopsis: Framework for artificial life experiments.
description:
    A software framework for automating experiments
    with artificial life. It provides a daemon which
    maintains its own "clock", schedules events,
    provides logging, and ensures that each agent gets
    its turn to use the CPU. You can use other
    applications on the computer at the same time
    without fear of interfering with experiments; they
    will run normally, just more slowly. See the
    tutorial at
    <https://github.com/mhwombat/creatur-examples/raw/master/Tutorial.pdf>
    for examples on how to use the
    Créatúr framework.
    .
    About the name: \"Créatúr\" (pronounced kray-toor)
    is an Irish word meaning animal, creature, or an
    unfortunate person.
category: AI
author: Amy de Buitléir

library
    exposed-modules:
        ALife.Creatur
        ALife.Creatur.Checklist
        ALife.Creatur.Clock
        ALife.Creatur.Counter
        ALife.Creatur.Daemon
        ALife.Creatur.Database
        ALife.Creatur.Database.CachedFileSystem
        ALife.Creatur.Database.CachedFileSystemInternal
        ALife.Creatur.Database.FileSystem
        ALife.Creatur.Genetics.Analysis
        ALife.Creatur.Genetics.BRGCBool
        ALife.Creatur.Genetics.BRGCWord8
        ALife.Creatur.Genetics.BRGCWord16
        ALife.Creatur.Genetics.Diploid
        ALife.Creatur.Genetics.Recombination
        ALife.Creatur.Genetics.Reproduction.Sexual
        ALife.Creatur.Genetics.Reproduction.SimplifiedSexual
        ALife.Creatur.Logger
        ALife.Creatur.Logger.SimpleLogger
        ALife.Creatur.Logger.SimpleRotatingLogger
        ALife.Creatur.Namer
        ALife.Creatur.Persistent
        ALife.Creatur.Universe
        ALife.Creatur.Task
        ALife.Creatur.Util
    build-depends:
        array >=0.5.2.0 && <0.6,
        base >=4.10.0.0 && <4.11,
        binary >=0.8.5.1 && <0.9,
        bytestring >=0.10.8.2 && <0.11,
        cond >=0.4.1.1 && <0.5,
        cereal >=0.5.4.0 && <0.6,
        directory >=1.3.0.2 && <1.4,
        exceptions >=0.8.3 && <0.9,
        filepath >=1.4.1.2 && <1.5,
        gray-extended >=1.5.2 && <1.6,
        hdaemonize >=0.5.4 && <0.6,
        hsyslog >=5.0.1 && <5.1,
        MonadRandom >=0.5.1 && <0.6,
        mtl >=2.2.1 && <2.3,
        old-locale >=1.0.0.7 && <1.1,
        process >=1.6.1.0 && <1.7,
        random ==1.1.*,
        split >=0.2.3.2 && <0.3,
        time >=1.8.0.2 && <1.9,
        transformers >=0.5.2.0 && <0.6,
        unix >=2.7.2.2 && <2.8,
        zlib >=0.6.1.2 && <0.7
    hs-source-dirs: src
    other-modules:
        Paths_creatur
    ghc-options: -Wall -fno-warn-orphans

test-suite creatur-tests
    type: exitcode-stdio-1.0
    main-is: TestAll.hs
    build-depends:
        array >=0.5.2.0 && <0.6,
        base >=4.10.0.0 && <4.11,
        binary >=0.8.5.1 && <0.9,
        cereal >=0.5.4.0 && <0.6,
        creatur,
        directory >=1.3.0.2 && <1.4,
        filepath >=1.4.1.2 && <1.5,
        hsyslog >=5.0.1 && <5.1,
        HUnit >=1.6.0.0 && <1.7,
        MonadRandom >=0.5.1 && <0.6,
        mtl >=2.2.1 && <2.3,
        temporary >=1.2.1.1 && <1.3,
        test-framework >=0.8.1.1 && <0.9,
        test-framework-hunit >=0.3.0.2 && <0.4,
        test-framework-quickcheck2 >=0.3.0.4 && <0.4,
        QuickCheck >=2.10.0.1 && <2.11
    hs-source-dirs: test
    other-modules:
        ALife.Creatur.ChecklistQC
        ALife.Creatur.CounterQC
        ALife.Creatur.Database.CachedFileSystemQC
        ALife.Creatur.Database.FileSystemQC
        ALife.Creatur.Genetics.BRGCBoolQC
        ALife.Creatur.Genetics.BRGCWord16QC
        ALife.Creatur.Genetics.BRGCWord8QC
        ALife.Creatur.Genetics.DiploidQC
        ALife.Creatur.Genetics.RecombinationQC
        ALife.Creatur.PersistentQC
        ALife.Creatur.UniverseQC
        ALife.Creatur.UtilQC
    ghc-options: -Wall
