cabal-version: 3.0

name: loc
version: 0.1.3.14

synopsis: Types representing line and column positions and ranges in text files.

description:
    The package name /loc/ stands for “location” and is
    also an allusion to the acronym for “lines of code”.

    The @Loc@ type represents a caret position in a text file,
    the @Span@ type is a nonempty range between two @Loc@s,
    and the @Area@ type is a set of non-touching @Span@s.

category: Data Structures

homepage: https://github.com/chris-martin/loc

author:     Chris Martin <ch.martin@gmail.com>
maintainer: Chris Martin <ch.martin@gmail.com>

license: Apache-2.0
license-file: license.txt

build-type: Simple

extra-source-files: example.png example.svg README.md changelog.md

common base
    default-language: Haskell2010
    default-extensions: NoImplicitPrelude
    ghc-options: -Wall
    build-depends: base >= 4.11 && < 4.17
    build-depends: containers >= 0.5.7.1 && < 0.7

common test
    import: base
    hs-source-dirs: test
    ghc-options: -threaded
    build-depends: loc

library
    import: base
    hs-source-dirs: src
    exposed-modules:
        Data.Loc
        Data.Loc.Area
        Data.Loc.Exception
        Data.Loc.Internal.Map
        Data.Loc.Internal.Prelude
        Data.Loc.List.OneToTwo
        Data.Loc.List.ZeroToTwo
        Data.Loc.Loc
        Data.Loc.Pos
        Data.Loc.Span
        Data.Loc.Types

test-suite doctest
    import: test
    type: exitcode-stdio-1.0
    main-is: doctest.hs
    build-depends: doctest >= 0.16 && < 0.21

test-suite hedgehog
    import: test
    type: exitcode-stdio-1.0
    main-is: hedgehog.hs
    hs-source-dirs: loc-test-src
    build-depends: hedgehog ^>= 1.0
    other-modules: Test.Loc.Hedgehog.Gen
