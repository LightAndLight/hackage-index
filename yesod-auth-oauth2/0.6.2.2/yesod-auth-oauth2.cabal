cabal-version:      1.12
name:               yesod-auth-oauth2
version:            0.6.2.2
license:            MIT
license-file:       LICENSE
maintainer:         Pat Brisbin <pbrisbin@gmail.com>
author:             Tom Streller
homepage:           http://github.com/thoughtbot/yesod-auth-oauth2
bug-reports:        https://github.com/thoughtbot/yesod-auth-oauth2/issues
synopsis:           OAuth 2.0 authentication plugins
description:
    Library to authenticate with OAuth 2.0 for Yesod web applications.

category:           Web
build-type:         Simple
extra-source-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/thoughtbot/yesod-auth-oauth2

flag example
    description: Build the example application
    default:     False

library
    exposed-modules:
        URI.ByteString.Extension
        Yesod.Auth.OAuth2
        Yesod.Auth.OAuth2.AzureAD
        Yesod.Auth.OAuth2.BattleNet
        Yesod.Auth.OAuth2.Bitbucket
        Yesod.Auth.OAuth2.ClassLink
        Yesod.Auth.OAuth2.Dispatch
        Yesod.Auth.OAuth2.ErrorResponse
        Yesod.Auth.OAuth2.EveOnline
        Yesod.Auth.OAuth2.Exception
        Yesod.Auth.OAuth2.GitHub
        Yesod.Auth.OAuth2.GitLab
        Yesod.Auth.OAuth2.Google
        Yesod.Auth.OAuth2.Nylas
        Yesod.Auth.OAuth2.Prelude
        Yesod.Auth.OAuth2.Salesforce
        Yesod.Auth.OAuth2.Slack
        Yesod.Auth.OAuth2.Spotify
        Yesod.Auth.OAuth2.Upcase
        Yesod.Auth.OAuth2.WordPressDotCom

    hs-source-dirs:   src
    other-modules:    Paths_yesod_auth_oauth2
    default-language: Haskell2010
    ghc-options:      -Wall
    build-depends:
        aeson >=0.6 && <1.6,
        base >=4.9.0.0 && <5,
        bytestring >=0.9.1.4 && <0.11,
        cryptonite >=0.25 && <0.28,
        errors >=2.3.0 && <2.4,
        hoauth2 >=1.11.0 && <1.17,
        http-client >=0.4.0 && <0.8,
        http-conduit >=2.0 && <3.0,
        http-types >=0.8 && <0.13,
        memory >=0.15.0 && <0.16,
        microlens >=0.4.11.2 && <0.5,
        safe-exceptions >=0.1.7.1 && <0.2,
        text >=0.7 && <2.0,
        uri-bytestring >=0.3.3.0 && <0.4,
        yesod-auth >=1.6.0 && <1.7,
        yesod-core >=1.6.0 && <1.7

executable yesod-auth-oauth2-example
    main-is:          Main.hs
    hs-source-dirs:   example
    other-modules:    Paths_yesod_auth_oauth2
    default-language: Haskell2010
    ghc-options:      -Wall -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        aeson >=0.6 && <1.6,
        aeson-pretty >=0.8.8 && <0.9,
        base >=4.9.0.0 && <5,
        bytestring >=0.9.1.4 && <0.11,
        containers >=0.6.2.1 && <0.7,
        http-conduit >=2.0 && <3.0,
        load-env >=0.2.1.0 && <0.3,
        text >=0.7 && <2.0,
        warp >=3.3.13 && <3.4,
        yesod >=1.6.1.0 && <1.7,
        yesod-auth >=1.6.0 && <1.7,
        yesod-auth-oauth2 -any

    if !flag(example)
        buildable: False

test-suite test
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   test
    other-modules:
        URI.ByteString.ExtensionSpec
        Paths_yesod_auth_oauth2

    default-language: Haskell2010
    ghc-options:      -Wall
    build-depends:
        base >=4.9.0.0 && <5,
        hspec >=2.7.4 && <2.8,
        uri-bytestring >=0.3.3.0 && <0.4,
        yesod-auth-oauth2 -any
