-- Initial hanabi-dealer.cabal generated by cabal init.  For further
-- documentation, see http://haskell.org/cabal/users-guide/

-- The name of the package.
name:                hanabi-dealer

-- The package version.  See the Haskell package versioning policy (PVP)
-- for standards guiding when and how versions should be incremented.
-- https://wiki.haskell.org/Package_versioning_policy
-- PVP summary:      +-+------- breaking API changes
--                   | | +----- non-breaking API additions
--                   | | | +--- code changes with no API change
version:             0.15.1.1

-- A short (one-line) description of the package.
synopsis:            Hanabi card game

-- A longer description of the package.
-- description:

-- URL for the project homepage or repository.
homepage:            https://nautilus.cs.miyazaki-u.ac.jp/~skata/Sontakki/

-- The license under which the package is released.
license:             BSD3

-- The file containing the license text.
license-file:        LICENSE

-- The package author(s).
author:              Susumu Katayama

-- An email address to which users can send suggestions, bug reports, and
-- patches.
maintainer:          Susumu Katayama <skata@cs.miyazaki-u.ac.jp>

-- A copyright notice.
-- copyright:

category:            Game

build-type:          Simple

-- Extra files to be distributed with the package, such as examples or a
-- README.
extra-source-files:  ChangeLog.md, examples/exampleStrategy.lhs

-- Constraint on the version of Cabal needed to build this package.
cabal-version:       >=1.10

source-repository head
  type:     darcs
  location: https://hub.darcs.net/susumu/hanabi-dealer

Flag server
  Description: Build the server program and library in addition to the minimal library.
  Default:     False
Flag SNAP
  Description: Use Snap instead of runServer.
  Default:     False
Flag WARP
  Description: Use Warp instead of runServer.
  Default:     True

-- -- Temporarily commented out because this is misleading. Use of random >=1.2.0 is recommended.
-- FLAG TFRANDOM
--   Description: Use tf-random instead of random.
--   Default:     True

FLAG official
  Description: The client connects to the official server URI instead of localhost.
  Default:     False
FLAG TH
  Description: Use template-haskell just for obtaining the compilation time.
  Default:     True
Flag jsaddle
--   Description: Build the client with JSaddle even when compiling with GHC.
--                (Package miso must have been built with --flags="jsaddle".)
--   Default:     False
-- Flag all
  Description: Quickbuild all-in-one server, using JSaddle.
               (Package miso must have been built with --flags="jsaddle".)
  Default:     False
Flag examples
     Description: Include example strategies in the library.
     Default:     True
Flag mhlmc
     Description: Include strategies using MagicHaskeller. (Patched version of MagicHaskeller is recommended.)
     Default:     False
Flag miso1710
     Description: Avoid using miso>=1.8.* when building with GHCJS, because those versions do not work with Konqueror or Falkon.
     Default:     True
Flag debug
     Description: debug flag
     Default: False

library
  -- Modules exported by the library.
  exposed-modules:     Game.Hanabi, Game.Hanabi.Strategies.EndGameLite, Game.Hanabi.Strategies.EndGameOld

  -- Other library packages from which modules are imported.
  build-depends:       base >=4.8 && <4.15, containers >=0.5, random >=1.1

  -- Modules included in this library but not exported.
  -- other-modules:

  -- LANGUAGE extensions used by modules in this package.
  other-extensions:    MultiParamTypeClasses, FlexibleInstances, Safe


  -- Directories containing source files.
  -- hs-source-dirs:

  -- Base language which the package is written in.
  default-language:    Haskell2010

  if flag(debug)
    cpp-options: -DDEBUG
    build-depends: QuickCheck
  if flag(examples) || flag(mhlmc) || impl(ghcjs)
    exposed-modules:     Game.Hanabi.Strategies, Game.Hanabi.Strategies.SimpleStrategy, Game.Hanabi.Strategies.StatefulStrategy, Game.Hanabi.Strategies.Stateless, Game.Hanabi.Strategies.EndGameSearch, Game.Hanabi.Strategies.MCSearch, Game.Hanabi.Strategies.LazyMC
    build-depends: tf-random, array
  if flag(mhlmc)
     cpp-options: -DMHLMC
     exposed-modules: Game.Hanabi.Strategies.AppendDSL, Game.Hanabi.Strategies.AdaptiveLMC
     build-depends: MagicHaskeller >=0.9.7.0, parallel
  if impl(ghcjs) || flag(server) || flag(jsaddle)
    exposed-modules:     Game.Hanabi.VersionInfo
    other-modules:       Game.Hanabi.Msg
    -- cpp-options:      -DCABAL
    -- -DCABAL is used for detecting the version of hanabi-dealer, which is impossible when building as the library.
    if flag(TH)
      build-depends: template-haskell

  if !impl(ghcjs) && flag(server)
    ghc-options:      -O2
    -- Not sure if -O2 is worthy, but this should be OK because the server is not built by default.
    exposed-modules:     Game.Hanabi.Backend
    build-depends:    websockets >=0.12 && <0.13, network >=2.6 && <3.2, hashable >=1.3, time >=1.6, text >=1.2, utf8-string
--    if flag(TFRANDOM)
--      build-depends: tf-random
--      cpp-options: -DTFRANDOM
    if flag(SNAP)
      build-depends:       unix >=2.7 && <2.8, websockets-snap >=0.10 && <0.11, snap-server >=1.1 && <1.2, abstract-par >=0.3 && <0.4, monad-par >=0.3 && <0.4
      cpp-options:         -DSNAP
    else
      if flag(WARP)
        build-depends:       wai-websockets, warp, wai, http-types
        cpp-options:         -DWARP
  if impl(ghcjs) || flag(jsaddle)
      exposed-modules:     Game.Hanabi.Client
      build-depends:       servant >=0.12
      -- miso-1.7.1.0 fails to build without servant
      if impl(ghcjs)
          build-depends:      base >=4.12 && <4.15, aeson, time, network-uri >=2.6
          other-modules:       Game.Hanabi.FFI
          ghcjs-options:      -dedupe -O
          if flag(miso1710)
             build-depends:  miso <1.8
          else
             build-depends:  miso
      else
          build-depends:      base  <4.15, aeson, miso, time, network-uri >=2.6
          build-depends:   wai, warp, websockets, jsaddle, jsaddle-warp
      if flag(official)
        cpp-options:  -DWSURI="//nautilus.cs.miyazaki-u.ac.jp/wshanabi"
--  if flag(jsaddle)
--      build-depends:      warp, wai, websockets

executable hanabiq
  main-is: all.hs
  other-modules: Game.Hanabi, Game.Hanabi.Strategies, Game.Hanabi.Strategies.EndGameLite, Game.Hanabi.Strategies.EndGameOld, Game.Hanabi.Msg, Game.Hanabi.Backend, Game.Hanabi.Client, Game.Hanabi.Strategies.SimpleStrategy, Game.Hanabi.Strategies.StatefulStrategy, Game.Hanabi.Strategies.Stateless, Game.Hanabi.Strategies.EndGameSearch, Game.Hanabi.Strategies.MCSearch, Game.Hanabi.Strategies.LazyMC
  build-depends: tf-random, array
--  if !flag(all)
  if !flag(jsaddle)
    buildable: False
  else
    ghc-options: -O0 -rtsopts
    cpp-options: -DWSURI="//localhost:8080/ws/" -DWARP -DALL
    build-depends: base >=4.9 && <4.15, containers >=0.5, random >=1.1, jsaddle, jsaddle-warp >=0.9, aeson, miso >=1.4, servant >=0.12, websockets >=0.12 && <0.13, network >=2.6 && <3.2, hashable >=1.3, time >=1.6, text >=1.2, utf8-string, wai-websockets, warp, wai, http-types, network-uri >=2.6
    -- I am not sure but seemingly miso fails to build without servant.
--    if flag(TFRANDOM)
--      build-depends: tf-random
--      cpp-options: -DTFRANDOM
    default-language: Haskell2010

executable hanabib
  main-is:       server.hs
  other-modules: Game.Hanabi.Strategies, Game.Hanabi.Strategies.SimpleStrategy, Game.Hanabi.Strategies.StatefulStrategy, Game.Hanabi.Strategies.Stateless, Game.Hanabi.Strategies.EndGameSearch, Game.Hanabi.Strategies.MCSearch, Game.Hanabi.Strategies.LazyMC
  build-depends: tf-random, array
  if impl(ghcjs) || !flag(server)
    buildable: False
  else
    ghc-options:      -O2 -threaded -Wall -rtsopts
    -- Not sure if -O2 is worthy, but this should be OK because the server is not built by default.
    cpp-options:      -DCABAL
    build-depends:    base >=4.8 && <4.15, containers >=0.5, random >=1.1, websockets >=0.12 && <0.13, network >=2.6 && <3.2, hashable >=1.3, time >=1.6, text >=1.2, utf8-string, array, hanabi-dealer
    default-language: Haskell2010
    if flag(mhlmc)
      cpp-options: -DMHLMC
      other-modules: Game.Hanabi.Strategies.AppendDSL, Game.Hanabi.Strategies.AdaptiveLMC
      build-depends: MagicHaskeller >=0.9.7.0, parallel
    if flag(debug)
      cpp-options: -DDEBUG
      build-depends: QuickCheck
    if flag(TH)
      build-depends: template-haskell
--    if flag(TFRANDOM)
--      build-depends: tf-random
--      cpp-options: -DTFRANDOM
    if flag(SNAP)
      build-depends:       unix >=2.7 && <2.8, websockets-snap >=0.10 && <0.11, snap-server >=1.1 && <1.2, abstract-par >=0.3 && <0.4, monad-par >=0.3 && <0.4
      cpp-options:         -DSNAP
    else
      if flag(WARP)
        build-depends:       wai-websockets, warp, wai, http-types
        cpp-options:         -DWARP

executable hanabif
  main-is:       client.hs
  other-modules: Game.Hanabi.Strategies, Game.Hanabi.Strategies.SimpleStrategy, Game.Hanabi.Strategies.StatefulStrategy, Game.Hanabi.Strategies.Stateless, Game.Hanabi.Strategies.EndGameSearch, Game.Hanabi.Strategies.MCSearch, Game.Hanabi.Strategies.LazyMC
  build-depends: tf-random, array
  if !impl(ghcjs)  && !flag(jsaddle)
    buildable: False
  else
    ghcjs-options:      -dedupe -O
    cpp-options:        -DCABAL
    if flag(debug)
      cpp-options: -DDEBUG
      build-depends: QuickCheck
    if !impl(ghcjs)
      build-depends:      base >=4.9 && <4.15, containers >=0.5, random >=1.1, jsaddle, jsaddle-warp >=0.9, aeson, miso >=1.4, servant >=0.12, websockets >=0.12 && <0.13, network >=2.6 && <3.2, hashable >=1.3, time >=1.6, text >=1.2, utf8-string, wai-websockets, warp, wai, http-types, network-uri >=2.6
    else
      build-depends:      base >=4.12 && <4.15, containers >=0.5, random >=1.1, jsaddle-warp >=0.9, aeson, time >=1.6, hanabi-dealer, network-uri >=2.6
      if flag(miso1710)
        build-depends: miso <1.8
      else
        build-depends: miso
    default-language:   Haskell2010
    if flag(mhlmc)
      cpp-options: -DMHLMC
      other-modules: Game.Hanabi.Strategies.AppendDSL, Game.Hanabi.Strategies.AdaptiveLMC
      build-depends: MagicHaskeller >=0.9.7.0, parallel
    if flag(official)
--      cpp-options:  -DWSURI="//133.54.228.39:8720"
      cpp-options:  -DWSURI="//nautilus.cs.miyazaki-u.ac.jp/wshanabi"
    if flag(TH)
      build-depends: template-haskell
--    if flag(jsaddle)
--      build-depends:      warp, wai, websockets

-- hanabibat executes batch experiments and prints the resulting statistics
executable hanabibat
  main-is: batch.hs
  other-modules: Game.Hanabi.Strategies, Game.Hanabi.Strategies.SimpleStrategy, Game.Hanabi.Strategies.StatefulStrategy, Game.Hanabi.Strategies.Stateless, Game.Hanabi.Strategies.EndGameSearch, Game.Hanabi.Strategies.MCSearch, Game.Hanabi.Strategies.LazyMC,
                 Game.Hanabi.VersionInfo,
                 Game.Hanabi.Strategies.AppendDSL, Game.Hanabi.Strategies.AdaptiveLMC
  build-depends:    base >=4.8 && <4.15, containers >=0.5, random >=1.2, splitmix, websockets >=0.12 && <0.13, network >=2.6 && <3.2, hashable >=1.3, time >=1.6, text >=1.2, utf8-string, array, hanabi-dealer, parallel, transformers, MagicHaskeller >=0.9.7.0
  if !flag(mhlmc)
    buildable: False
  else
    default-language: Haskell2010
    ghc-options:   -threaded -Wall -rtsopts
    -- Maybe -O2 is desired?
    cpp-options:      -DCABAL -DMHLMC
    if flag(debug)
      cpp-options: -DDEBUG
      build-depends: QuickCheck
    if flag(TH)
      build-depends: template-haskell
