cabal-version: >=1.8
name: dbus
version: 1.2.6
license: Apache-2.0
license-file: license.txt
maintainer: Andrey Sverdlichenko <blaze@ruddy.ru>
author: John Millikin <john@john-millikin.com>
stability: experimental
homepage: https://github.com/rblaze/haskell-dbus#readme
synopsis: A client library for the D-Bus IPC system.
description:
    D-Bus is a simple, message-based protocol for inter-process
    communication, which allows applications to interact with other parts of
    the machine and the user's session using remote procedure calls.
    .
    D-Bus is a essential part of the modern Linux desktop, where it replaces
    earlier protocols such as CORBA and DCOP.
    .
    This library is an implementation of the D-Bus protocol in Haskell. It
    can be used to add D-Bus support to Haskell applications, without the
    awkward interfaces common to foreign bindings.
    .
    Example: connect to the session bus, and get a list of active names.
    .
    @
    &#x7b;-\# LANGUAGE OverloadedStrings \#-&#x7d;
    .
    import Data.List (sort)
    import DBus
    import DBus.Client
    .
    main = do
    &#x20;   client <- connectSession
    &#x20;   //
    &#x20;   \-- Request a list of connected clients from the bus
    &#x20;   reply <- call_ client (methodCall \"\/org\/freedesktop\/DBus\" \"org.freedesktop.DBus\" \"ListNames\")
    &#x20;       &#x7b; methodCallDestination = Just \"org.freedesktop.DBus\"
    &#x20;       &#x7d;
    &#x20;   //
    &#x20;   \-- org.freedesktop.DBus.ListNames() returns a single value, which is
    &#x20;   \-- a list of names (here represented as [String])
    &#x20;   let Just names = fromVariant (methodReturnBody reply !! 0)
    &#x20;   //
    &#x20;   \-- Print each name on a line, sorted so reserved names are below
    &#x20;   \-- temporary names.
    &#x20;   mapM_ putStrLn (sort names)
    @
    .
    >$ ghc --make list-names.hs
    >$ ./list-names
    >:1.0
    >:1.1
    >:1.10
    >:1.106
    >:1.109
    >:1.110
    >ca.desrt.dconf
    >org.freedesktop.DBus
    >org.freedesktop.Notifications
    >org.freedesktop.secrets
    >org.gnome.ScreenSaver
category: Network, Desktop
build-type: Simple
extra-source-files:
    examples/dbus-monitor.hs
    examples/export.hs
    examples/introspect.hs
    examples/list-names.hs
    idlxml/dbus.xml

source-repository head
    type: git
    location: https://github.com/rblaze/haskell-dbus

library
    exposed-modules:
        DBus
        DBus.Client
        DBus.Generation
        DBus.Internal.Address
        DBus.Internal.Message
        DBus.Internal.Types
        DBus.Internal.Wire
        DBus.Introspection
        DBus.Introspection.Parse
        DBus.Introspection.Render
        DBus.Introspection.Types
        DBus.Socket
        DBus.TH
        DBus.Transport
    hs-source-dirs: lib
    ghc-options: -W -Wall
    build-depends:
        base ==4.*,
        bytestring <0.11,
        cereal <0.6,
        conduit >=1.3.0 && <1.4,
        containers <0.7,
        deepseq <1.5,
        exceptions <0.11,
        filepath <1.5,
        lens <4.18,
        network >=2.8.0.0 && <2.9 || >=3.0.1.0 && <3.1,
        parsec <3.2,
        random <1.2,
        split <0.3,
        template-haskell <2.15,
        text <1.3,
        th-lift <0.9,
        transformers <0.6,
        unix <2.8,
        vector <0.13,
        xml-conduit <1.9,
        xml-types <0.4

test-suite dbus_tests
    type: exitcode-stdio-1.0
    main-is: DBusTests.hs
    hs-source-dirs: tests
    other-modules:
        DBusTests.Address
        DBusTests.BusName
        DBusTests.Client
        DBusTests.ErrorName
        DBusTests.Generation
        DBusTests.Integration
        DBusTests.InterfaceName
        DBusTests.Introspection
        DBusTests.MemberName
        DBusTests.Message
        DBusTests.ObjectPath
        DBusTests.Serialization
        DBusTests.Signature
        DBusTests.Socket
        DBusTests.TH
        DBusTests.Transport
        DBusTests.Util
        DBusTests.Variant
        DBusTests.Wire
    ghc-options: -W -Wall -fno-warn-orphans
    build-depends:
        dbus -any,
        base <4.13,
        bytestring <0.11,
        cereal <0.6,
        containers <0.7,
        directory <1.4,
        extra <1.7,
        filepath <1.5,
        network <2.9,
        parsec <3.2,
        process <1.7,
        QuickCheck <2.14,
        random <1.2,
        resourcet <1.3,
        tasty <1.3,
        tasty-hunit <0.11,
        tasty-quickcheck <0.11,
        text <1.3,
        transformers <0.6,
        unix <2.8,
        vector <0.13

benchmark dbus_benchmarks
    type: exitcode-stdio-1.0
    main-is: DBusBenchmarks.hs
    hs-source-dirs: benchmarks
    ghc-options: -Wall -fno-warn-orphans
    build-depends:
        dbus -any,
        base <4.13,
        criterion <1.6
