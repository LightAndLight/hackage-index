cabal-version: 2.4

author: Mitchell Rosen
bug-reports: https://github.com/termbox/termbox-haskell/issues
build-type: Simple
category: User Interfaces
copyright: (c) 2018-2023, Mitchell Rosen
description:
  This package provides an Elm Architecture interface to @termbox@ programs.
  .
  See also:
  .
  * @<https://hackage.haskell.org/package/termbox-banana termbox-banana>@ for a @reactive-banana@ FRP interface.
homepage: https://github.com/termbox/termbox-haskell
license: BSD-3-Clause
license-file: LICENSE
maintainer: Mitchell Rosen <mitchellwrosen@gmail.com>
name: termbox-tea
synopsis: termbox + The Elm Architecture
tested-with: GHC == 9.2.7, GHC == 9.4.4, GHC == 9.6.1
version: 0.1.0
x-revision: 1

extra-source-files:
  CHANGELOG.md

source-repository head
  type: git
  location: git://github.com/termbox/termbox-haskell.git

flag build-examples
  default: False
  manual: True

common component
  default-extensions:
    BlockArguments
    CApiFFI
    DeriveAnyClass
    DeriveFunctor
    DeriveGeneric
    DerivingStrategies
    DerivingVia
    DuplicateRecordFields
    ExistentialQuantification
    FlexibleInstances
    GeneralizedNewtypeDeriving
    InstanceSigs
    LambdaCase
    NamedFieldPuns
    NumericUnderscores
    OverloadedStrings
    PatternSynonyms
    TypeApplications
  default-language: Haskell2010
  ghc-options:
    -Weverything
    -Wno-all-missed-specialisations
    -Wno-implicit-prelude
    -Wno-missing-import-lists
    -Wno-missing-local-signatures
    -Wno-monomorphism-restriction
    -Wno-safe
    -Wno-unsafe
  if impl(ghc >= 8.10)
    ghc-options:
      -Wno-missing-safe-haskell-mode
      -Wno-prepositive-qualified-module
  if impl(ghc >= 9.2)
    ghc-options:
      -Wno-missing-kind-signatures

library
  import: component
  build-depends:
    base ^>= 4.13 || ^>= 4.14 || ^>= 4.15 || ^>= 4.16 || ^>= 4.17 || ^>= 4.18,
    termbox ^>= 1.1.0,
    ki ^>= 1.0,
  exposed-modules: Termbox.Tea
  hs-source-dirs: src

executable termbox-example-demo
  import: component
  if !flag(build-examples)
    buildable: False
  build-depends:
    base,
    ki ^>= 1.0,
    termbox-tea,
  ghc-options:
    -rtsopts
    -threaded
  hs-source-dirs: examples
  main-is: Demo.hs
