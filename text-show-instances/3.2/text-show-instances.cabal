name:                text-show-instances
version:             3.2
x-revision: 3
synopsis:            Additional instances for text-show
description:         @text-show-instances@ is a supplemental library to @text-show@
                     that provides additional @Show@ instances for data types in
                     common Haskell libraries and GHC dependencies that are not
                     encompassed by @text-show@. Currently, @text-show-instances@
                     covers these libraries:
                     .
                     * @<http://hackage.haskell.org/package/bifunctors           bifunctors>@
                     .
                     * @<http://hackage.haskell.org/package/binary               binary>@
                     .
                     * @<http://hackage.haskell.org/package/containers           containers>@
                     .
                     * @<http://hackage.haskell.org/package/directory            directory>@
                     .
                     * @<http://hackage.haskell.org/package/haskeline            haskeline>@
                     .
                     * @<http://hackage.haskell.org/package/hoopl                hoopl>@
                     .
                     * @<http://hackage.haskell.org/package/hpc                  hpc>@
                     .
                     * @<http://hackage.haskell.org/package/old-locale           old-locale>@
                     .
                     * @<http://hackage.haskell.org/package/old-time             old-time>@
                     .
                     * @<http://hackage.haskell.org/package/pretty               pretty>@
                     .
                     * @<http://hackage.haskell.org/package/random               random>@
                     .
                     * @<http://hackage.haskell.org/package/semigroups           semigroups>@
                     .
                     * @<http://hackage.haskell.org/package/tagged               tagged>@
                     .
                     * @<http://hackage.haskell.org/package/template-haskell     template-haskell>@
                     .
                     * @<http://hackage.haskell.org/package/terminfo             terminfo>@
                     .
                     * @<http://hackage.haskell.org/package/time                 time>@
                     .
                     * @<http://hackage.haskell.org/package/transformers         transformers>@
                     .
                     * @<http://hackage.haskell.org/package/unix                 unix>@
                     .
                     * @<http://hackage.haskell.org/package/unordered-containers unordered-containers>@
                     .
                     * @<http://hackage.haskell.org/package/vector               vector>@
                     .
                     * @<http://hackage.haskell.org/package/Win32                Win32>@
                     .
                     * @<http://hackage.haskell.org/package/xhtml                xhtml>@
                     .
                     One can use these instances by importing
                     "TextShow.Instances". Alternatively, there are monomorphic
                     versions of the @showb@ function available in the other submodules
                     of "TextShow".
homepage:            https://github.com/RyanGlScott/text-show-instances
bug-reports:         https://github.com/RyanGlScott/text-show-instances/issues
license:             BSD3
license-file:        LICENSE
author:              Ryan Scott
maintainer:          Ryan Scott <ryan.gl.scott@gmail.com>
stability:           Experimental
copyright:           (C) 2014-2016 Ryan Scott
category:            Text
build-type:          Simple
tested-with:         GHC == 7.0.4
                   , GHC == 7.2.2
                   , GHC == 7.4.2
                   , GHC == 7.6.3
                   , GHC == 7.8.4
                   , GHC == 7.10.3
                   , GHC == 8.0.1
extra-source-files:  CHANGELOG.md, README.md, include/inline.h
cabal-version:       >=1.10

source-repository head
  type:                git
  location:            https://github.com/RyanGlScott/text-show-instances

flag developer
  description:         Operate in developer mode (allows for faster recompilation of tests)
  default:             False
  manual:              True

library
  exposed-modules:     TextShow.Instances

                       TextShow.Compiler.Hoopl
                       TextShow.Control.Applicative.Trans
                       TextShow.Control.Monad.Trans
                       TextShow.Data.Bifunctor
                       TextShow.Data.Binary
                       TextShow.Data.Containers
                       TextShow.Data.Functor.Trans
                       TextShow.Data.Tagged
                       TextShow.Data.Time
                       TextShow.Data.UnorderedContainers
                       TextShow.Data.Vector
                       TextShow.Language.Haskell.TH
                       TextShow.System.Console.Haskeline
                       TextShow.System.Directory
                       TextShow.System.Locale
                       TextShow.System.Random
                       TextShow.System.Time
                       TextShow.Text.PrettyPrint
                       TextShow.Text.XHtml
                       TextShow.Trace.Hpc

                       -- Only exports functions if using Windows
                       TextShow.System.Win32

                       -- Only exports functions if not using Windows
                       TextShow.System.Console.Terminfo
                       TextShow.System.Posix
  other-modules:       TextShow.Utils
  build-depends:       base                 >= 4.3    && < 5
                     , base-compat          >= 0.8.1  && < 1
                     , bifunctors           >= 5.1    && < 6
                     , binary               >= 0.6    && < 0.9
                     , bytestring           >= 0.9    && < 0.11
                     , containers           >= 0.1    && < 0.6
                     , directory            >= 1      && < 1.3
                     , haskeline            >= 0.7    && < 0.8
                     , hoopl                >= 3.8.7  && < 3.11
                     , hpc                  >= 0.5    && < 0.7
                     , old-locale           >= 1      && < 1.1
                     , old-time             >= 1      && < 1.2
                     , pretty               >= 1      && < 1.2
                     , random               >= 1.0.1  && < 1.2
                     , semigroups           >= 0.16.2 && < 1
                     , tagged               >= 0.4.4  && < 1
                     , template-haskell     >= 2.5    && < 2.12
                     , text                 >= 0.11.1 && < 1.3
                     , text-show            >= 3      && < 3.3
                     , time                 >= 0.1    && < 1.7
                     , transformers         >= 0.2.1  && < 0.6
                     , transformers-compat  >= 0.5    && < 1
                     , unordered-containers >= 0.2    && < 0.3
                     , vector               >= 0.9    && < 0.12
                     , xhtml                >= 3000.2 && < 3000.3
  hs-source-dirs:      src
  default-language:    Haskell2010
  ghc-options:         -Wall
  include-dirs:        include
  includes:            inline.h
  install-includes:    inline.h

  if os(windows)
    build-depends:     Win32                >= 2.1    && < 2.4
  else
    build-depends:     terminfo             >= 0.3.2  && < 0.5
                     , unix                 >= 2      && < 2.8

test-suite spec
  type:                exitcode-stdio-1.0
  main-is:             Spec.hs
  other-modules:       Instances.Compiler.Hoopl
                       Instances.Control.Applicative.Trans
                       Instances.Control.Monad.Trans
                       Instances.Data.Bifunctor
                       Instances.Data.Binary
                       Instances.Data.Containers
                       Instances.Data.Functor.Trans
                       Instances.Data.Tagged
                       Instances.Language.Haskell.TH
                       Instances.Miscellaneous
                       Instances.System.Console.Haskeline
                       Instances.System.Directory
                       Instances.System.Locale
                       Instances.System.Random
                       Instances.Text.PrettyPrint
                       Instances.Text.XHtml
                       Instances.Trace.Hpc
                       Instances.Utils

                       -- Only exports functions if using Windows
                       Instances.System.Win32

                       -- Only exports functions if not using Windows
                       Instances.System.Console.Terminfo
                       Instances.System.Posix

                       Spec.Compiler.HooplSpec
                       Spec.Control.Applicative.TransSpec
                       Spec.Control.Monad.TransSpec
                       Spec.Data.BifunctorSpec
                       Spec.Data.BinarySpec
                       Spec.Data.ContainersSpec
                       Spec.Data.Functor.TransSpec
                       Spec.Data.TaggedSpec
                       Spec.Data.TimeSpec
                       Spec.Data.UnorderedContainersSpec
                       Spec.Language.Haskell.THSpec
                       Spec.System.Console.HaskelineSpec
                       Spec.System.DirectorySpec
                       Spec.System.LocaleSpec
                       Spec.System.RandomSpec
                       Spec.System.TimeSpec
                       Spec.Text.PrettyPrintSpec
                       Spec.Text.XHtmlSpec
                       Spec.Trace.HpcSpec
                       Spec.Utils

                       -- Only exports functions if using Windows
                       Spec.System.Win32Spec

                       -- Only exports functions if not using Windows
                       Spec.System.Console.TerminfoSpec
                       Spec.System.PosixSpec
  build-depends:       base                 >= 4.3    && < 5
                     , base-compat          >= 0.8.1  && < 1
                     , bifunctors           >= 5.1    && < 6
                     , binary               >= 0.6    && < 0.9
                     , bytestring           >= 0.9    && < 0.11
                     , containers           >= 0.1    && < 0.6
                     , directory            >= 1      && < 1.3
                     , generic-deriving     >= 1.9    && < 2
                     , ghc-prim
                     , haskeline            >= 0.7    && < 0.8
                     , hoopl                >= 3.8.7  && < 3.11
                     , hpc                  >= 0.5    && < 0.7
                     , hspec                >= 2      && < 3
                     , old-locale           >= 1      && < 1.1
                     , old-time             >= 1      && < 1.2
                     , pretty               >= 1      && < 1.2
                     , QuickCheck           >= 2.5    && < 3
                     , quickcheck-instances >= 0.3.12 && < 0.4
                     , random               >= 1.0.1  && < 1.2
                     , semigroups           >= 0.17   && < 1
                     , tagged               >= 0.4.4  && < 1
                     , template-haskell     >= 2.5    && < 2.12
                     , text                 >= 0.11.1 && < 1.3
                     , text-show            >= 3.2    && < 3.3
                     , th-orphans           >= 0.12   && < 1
                     , time                 >= 0.1    && < 1.7
                     , transformers         >= 0.2.1  && < 0.6
                     , transformers-compat  >= 0.5    && < 1
                     , unordered-containers >= 0.2    && < 0.3
                     , vector               >= 0.9    && < 0.12
                     , xhtml                >= 3000.2 && < 3000.3
  if !flag(developer)
    build-depends:     text-show-instances  == 3.2

  hs-source-dirs:      tests
  if flag(developer)
    hs-source-dirs:    src

  default-language:    Haskell2010
  ghc-options:         -Wall -threaded -rtsopts
  include-dirs:        include
  includes:            inline.h
  install-includes:    inline.h

  if os(windows)
    build-depends:     Win32                >= 2.1    && < 2.4
  else
    build-depends:     terminfo             >= 0.3.2  && < 0.5
                     , unix                 >= 2      && < 2.8
