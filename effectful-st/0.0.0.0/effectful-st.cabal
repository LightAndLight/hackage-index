cabal-version:      2.4
name:               effectful-st
version:            0.0.0.0
license:            CC0-1.0
license-file:       CC0-1.0.txt
author:             Russell Hernandez Ruiz
maintainer:         qrpnxz@hyperlife.xyz
stability:          stable
homepage:           https://sr.ht/~qrpnxz/effectful-st/
bug-reports:        https://todo.sr.ht/~qrpnxz/effectful-st
synopsis:           `ST`-style mutation for `effectful`.
description:
  An `ST`-style alternative to the `Prim` effect in `effectful` that
  allows safely running scoped state transformations without `IOE`.
  .
  The `Prim` effect allows the use of `PrimMonad` on `Eff` without
  allowing arbitrary `IO`. However, there is no safe way to run the
  effect without introducing the `IOE` effect because `Prim` has no
  way of delimiting the scope of the mutation to `runPrim`. In other
  words, `Prim` is effectively acting merely as a restricted form of
  `IOE` that only allows `PrimMonad`.
  .
  `STE` from this package on the other hand borrows the quantification
  trick from `Control.Monad.ST` to provide a safe way to run the effect
  purely.
category:           effectful
extra-source-files: README.md
extra-source-files: CHANGELOG.md

source-repository head
  type:     git
  location: https://git.sr.ht/~qrpnxz/effectful-st

source-repository this
  type:     git
  location: https://git.sr.ht/~qrpnxz/effectful-st
  tag:      0.0.0.0

library
  hs-source-dirs:   src
  exposed-modules:
      Effectful.ST
  build-depends:
      base >=4.13 && <4.18
    , effectful-core >1 && <2.2
    , primitive >=0.7 && <0.8
  default-language: Haskell2010
  ghc-options:
    -Wall
    -Wcompat
    -Widentities
    -Wincomplete-record-updates
    -Wincomplete-uni-patterns
    -Wpartial-fields
    -Wredundant-constraints
  ghc-options:
    -Wno-redundant-constraints
    -Wno-unticked-promoted-constructors
