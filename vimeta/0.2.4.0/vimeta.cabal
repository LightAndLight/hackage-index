--------------------------------------------------------------------------------
name:          vimeta
version:       0.2.4.0
synopsis:      Frontend for video metadata tagging tools
homepage:      http://github.com/pjones/vimeta
bug-reports:   http://github.com/pjones/vimeta/issues
license:       BSD2
license-file:  LICENSE
author:        Peter Jones <pjones@devalot.com>
maintainer:    Peter Jones <pjones@devalot.com>
copyright:     Copyright (c) 2013-2018 Peter Jones
category:      Video
stability:     experimental
tested-with:   GHC == 7.10.3, GHC == 8.0.2, GHC == 8.2.2
build-type:    Simple
cabal-version: 1.18
description:
  Vimeta is a tool to fetch video metadata from the interwebs and
  update video files using a tagging tool.

--------------------------------------------------------------------------------
extra-source-files:
  CHANGES.md
  README.md
  TODO.org

--------------------------------------------------------------------------------
source-repository head
  type: git
  location: git://github.com/pjones/vimeta.git

--------------------------------------------------------------------------------
flag maintainer
  description: Enable settings for the package maintainer.
  manual: True
  default: False

--------------------------------------------------------------------------------
library
  exposed-modules:
    Vimeta.Core
    Vimeta.Core.Cache
    Vimeta.Core.Config
    Vimeta.Core.Download
    Vimeta.Core.Format
    Vimeta.Core.MappingFile
    Vimeta.Core.Process
    Vimeta.Core.Tagger
    Vimeta.Core.Vimeta
    Vimeta.UI.CommandLine
    Vimeta.UI.CommandLine.Common
    Vimeta.UI.CommandLine.Config
    Vimeta.UI.CommandLine.Movie
    Vimeta.UI.CommandLine.TV
    Vimeta.UI.Common.Movie
    Vimeta.UI.Common.TV
    Vimeta.UI.Common.Util
    Vimeta.UI.Term.Common
    Vimeta.UI.Term.Movie
    Vimeta.UI.Term.TV

  other-modules:
    Paths_vimeta

  hs-source-dirs: src
  default-language: Haskell2010
  ghc-options: -Wall -fwarn-incomplete-uni-patterns

  if flag(maintainer)
    ghc-options: -Werror

  build-depends: base                 >= 4.6    && < 5.0
               , aeson                >= 0.8    && < 1.4
               , byline               >= 0.1    && < 1.0
               , bytestring           >= 0.10   && < 0.11
               , containers           >= 0.5    && < 0.6
               , directory            >= 1.2    && < 1.4
               , either               >= 4.3    && < 6
               , filepath             >= 1.3    && < 1.5
               , http-client          >= 0.4.30 && < 0.6
               , http-client-tls      >= 0.2.2  && < 0.4
               , http-types           >= 0.8    && < 0.13
               , mtl                  >= 2.1    && < 2.3
               , old-locale           >= 1.0    && < 1.1
               , optparse-applicative >= 0.11   && < 0.15
               , parsec               >= 3.1    && < 3.2
               , process              >= 1.1    && < 1.7
               , temporary            >= 1.1    && < 1.3
               , text                 >= 0.11   && < 1.3
               , themoviedb           >= 1.1    && < 1.2
               , time                 >= 1.2    && < 1.10
               , time-locale-compat   >= 0.1    && < 0.2
               , transformers         >= 0.3    && < 0.6
               , xdg-basedir          >= 0.2    && < 0.3
               , yaml                 >= 0.8    && < 0.9

--------------------------------------------------------------------------------
executable vimeta
  default-language: Haskell2010
  main-is: src/Main.hs
  build-depends: base, vimeta
  ghc-options: -Wall -fwarn-incomplete-uni-patterns

  if flag(maintainer)
    ghc-options: -Werror
