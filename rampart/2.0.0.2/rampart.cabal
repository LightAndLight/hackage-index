cabal-version: 2.2

name: rampart
version: 2.0.0.2
synopsis: Determine how intervals relate to each other.
description: Rampart determines how intervals relate to each other.

build-type: Simple
category: Math
extra-doc-files: docs/interval-relations.svg
extra-source-files: CHANGELOG.markdown README.markdown
license-file: LICENSE.markdown
license: MIT
maintainer: Taylor Fausak

source-repository head
  location: https://github.com/tfausak/rampart
  type: git

flag pedantic
  default: False
  description: Enables @-Werror@, which turns warnings into errors.
  manual: True

common library
  build-depends:
    , base >= 4.13.0 && < 4.17
  default-language: Haskell2010
  ghc-options:
    -Wall-missed-specialisations
    -Weverything
    -Wno-implicit-prelude
    -Wno-missing-deriving-strategies
    -Wno-missing-exported-signatures
    -Wno-safe

  if flag(pedantic)
    ghc-options: -Werror

  if impl(ghc >= 8.10)
    ghc-options:
      -Wno-missing-safe-haskell-mode
      -Wno-prepositive-qualified-module

  if impl(ghc >= 9.2)
    ghc-options:
      -Wno-missing-kind-signatures

common executable
  import: library

  build-depends: rampart
  ghc-options:
    -rtsopts
    -threaded
    -Wno-unused-packages

library
  import: library

  exposed-modules: Rampart
  hs-source-dirs: source/library

test-suite test
  import: executable

  build-depends:
    , hspec >= 2.7.6 && < 2.11
  hs-source-dirs: source/test-suite
  main-is: Main.hs
  type: exitcode-stdio-1.0

benchmark bench
  import: executable

  build-depends:
    , criterion >= 1.5.7 && < 1.7
  hs-source-dirs: source/benchmark
  main-is: Main.hs
  type: exitcode-stdio-1.0
