Name:               dfinity-radix-tree
Version:            0.1.1
Synopsis:           A generic data integrity layer.
Description:
   This library allows you to construct a Merkle tree on top of any underlying
   key–value database. It works by organizing your key–value pairs into a binary
   radix tree, which is well suited for storing large dictionaries of fairly
   random keys, and is optimized for storing keys of the same length.

License:            GPL-3
License-File:       LICENSE
Category:           Blockchain, DFINITY, Database
Copyright:          2018 DFINITY Stiftung
Author:             Enzo Haussecker <enzo@dfinity.org>
Maintainer:         DFINITY USA Research <team@dfinity.org>
Homepage:           https://github.com/dfinity-lab/hs-radix-tree
Bug-Reports:        https://github.com/dfinity-lab/hs-radix-tree/issues
Tested-With:        GHC==8.4.3, GHC==8.2.2
Build-Type:         Simple
Cabal-Version:      >= 1.10
Extra-Source-Files: CHANGELOG.md README.md

Library
   Build-Depends:
      BoundedChan,
      base >=4.10 && <5,
      base16-bytestring,
      bloomfilter,
      bytestring,
      conduit,
      containers,
      cryptohash-sha256,
      data-default-class,
      deepseq,
      ghc-prim,
      lens-simple,
      leveldb-haskell,
      lrucaching,
      mtl,
      resourcet,
      semigroups,
      serialise
   Default-Language:
      Haskell2010
   Exposed-Modules:
      Network.DFINITY.RadixTree
   GHC-Options:
      -O2
      -Wall
      -fno-warn-missing-signatures
   HS-Source-Dirs:
      src
   Other-Modules:
      Network.DFINITY.RadixTree.Bits
      Network.DFINITY.RadixTree.Bloom
      Network.DFINITY.RadixTree.Lenses
      Network.DFINITY.RadixTree.Memory
      Network.DFINITY.RadixTree.Serialise
      Network.DFINITY.RadixTree.Types
      Network.DFINITY.RadixTree.Utilities

Executable dfinity-radix-tree-unit-tests
   Build-Depends:
      aeson,
      base >=4.10 && <5,
      base16-bytestring,
      bytestring,
      cmdargs,
      containers,
      data-default-class,
      dfinity-radix-tree,
      mtl,
      text,
      unordered-containers
   Default-Language:
      Haskell2010
   GHC-Options:
      -O2
      -Wall
   HS-Source-Dirs:
      test
   Main-Is:
      Main.hs

Executable dfinity-radix-tree-benchmarks
   Build-Depends:
      base >=4.10 && <5,
      bytestring,
      cmdargs,
      cryptohash-sha256,
      data-default-class,
      dfinity-radix-tree,
      leveldb-haskell,
      resourcet
   Default-Language:
      Haskell2010
   GHC-Options:
      -O2
      -Wall
      -rtsopts
   HS-Source-Dirs:
      benchmark
   Main-Is:
      Main.hs
