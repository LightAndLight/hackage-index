cabal-version: 2.2

name:         epub-tools
version:      3.1
synopsis:     Command line utilities for working with epub files
description:  A suite of command-line utilities for creating and manipulating epub book files. Included are: epubmeta, epubname, epubzip
category:     Application, Console
homepage:     https://github.com/dino-/epub-tools.git
author:       Dino Morelli 
maintainer:   Dino Morelli <dino@ui3.info>
copyright:    2008 Dino Morelli
license:      ISC
license-file: LICENSE
build-type:   Simple
tested-with:  GHC >= 9.2.6
extra-source-files:
  .gitignore
  changelog.md
  doc/dev/notes.md
  doc/hcar/epubtoolsCommandlineepubU-De.tex
  doc/INSTALL
  README.md
  stack.yaml
  stack.yaml.lock
  TODO.md
  util/all-books.hs
  util/install.sh
  util/win-dist.sh

source-repository head
   type: git
   location: https://github.com/dino-/epub-tools.git

common lang
  default-language: Haskell2010
  build-depends:
      directory
    , epub-metadata >= 5.1
    , filepath
    , mtl
  ghc-options:
    -fwarn-tabs
    -Wall
    -Wcompat
    -Wincomplete-record-updates
    -Wincomplete-uni-patterns
    -Wredundant-constraints
  autogen-modules:
    Paths_epub_tools

common rts
  ghc-options:
    -rtsopts
    -with-rtsopts=-K32m

executable epubmeta
  import: lang
  import: rts
  hs-source-dirs: src/app
  main-is: epubmeta.hs
  build-depends:
      base >= 3 && < 5
    , bytestring
    , process
    , zip-archive
  other-modules:
    EpubTools.EpubMeta.Display
    EpubTools.EpubMeta.Edit
    EpubTools.EpubMeta.Export
    EpubTools.EpubMeta.Import
    EpubTools.EpubMeta.Opts
    EpubTools.EpubMeta.Util
    Paths_epub_tools

executable epubname
  import: lang
  import: rts
  hs-source-dirs: src/app
  main-is: epubname.hs
  build-depends:
      base >= 3 && < 5
    , containers
    , parsec
    , regex-compat
  other-modules:
    EpubTools.EpubName.Doc.Dsl
    EpubTools.EpubName.Doc.Rules
    EpubTools.EpubName.Format.Author
    EpubTools.EpubName.Format.Compile
    EpubTools.EpubName.Format.Format
    EpubTools.EpubName.Format.PubYear
    EpubTools.EpubName.Format.Util
    EpubTools.EpubName.Main
    EpubTools.EpubName.Opts
    EpubTools.EpubName.Prompt
    EpubTools.EpubName.Util
    Paths_epub_tools

test-suite epubname-tests
  import: lang
  import: rts
  type: exitcode-stdio-1.0
  hs-source-dirs: src/app src/tests
  main-is: test-epubname.hs
  build-depends:
      base >= 3 && < 5
    , containers
    , HUnit
    , parsec
    , regex-compat
  other-modules:
    EpubTools.EpubName.Doc.Rules
    EpubTools.EpubName.Format.Author
    EpubTools.EpubName.Format.Compile
    EpubTools.EpubName.Format.Format
    EpubTools.EpubName.Format.PubYear
    EpubTools.EpubName.Format.Util
    EpubTools.EpubName.Main
    EpubTools.EpubName.Opts
    EpubTools.EpubName.Util
    EpubTools.Test.EpubName.Format
    EpubTools.Test.EpubName.PubYear
    Paths_epub_tools

executable epubzip
  import: lang
  hs-source-dirs: src/app
  main-is: epubzip.hs
  build-depends:
      base >= 3 && < 5
    , containers
    , parsec
    , regex-compat
  other-modules:
    EpubTools.EpubName.Doc.Rules
    EpubTools.EpubName.Format.Author
    EpubTools.EpubName.Format.Compile
    EpubTools.EpubName.Format.Format
    EpubTools.EpubName.Format.PubYear
    EpubTools.EpubName.Format.Util
    EpubTools.EpubName.Main
    EpubTools.EpubName.Opts
    EpubTools.EpubName.Util
    EpubTools.EpubZip.Opts
    Paths_epub_tools
