cabal-version: 1.12
name: ribosome
version: 0.3.0.0
license: OtherLicense
license-file: LICENSE
copyright: 2019 Torsten Schmits
maintainer: tek@tryp.io
author: Torsten Schmits
homepage: https://github.com/tek/ribosome-hs#readme
bug-reports: https://github.com/tek/ribosome-hs/issues
synopsis: api extensions for nvim-hs
description:
    Please see the README on GitHub at <https://github.com/tek/ribosome-hs>
category: Neovim
build-type: Simple

source-repository head
    type: git
    location: https://github.com/tek/ribosome-hs

library
    exposed-modules:
        Ribosome.Api.Atomic
        Ribosome.Api.Autocmd
        Ribosome.Api.Buffer
        Ribosome.Api.Echo
        Ribosome.Api.Exists
        Ribosome.Api.Function
        Ribosome.Api.Input
        Ribosome.Api.Option
        Ribosome.Api.Path
        Ribosome.Api.Process
        Ribosome.Api.Response
        Ribosome.Api.Sleep
        Ribosome.Api.Syntax
        Ribosome.Api.Tabpage
        Ribosome.Api.Variable
        Ribosome.Api.Window
        Ribosome.Config.Setting
        Ribosome.Config.Settings
        Ribosome.Control.Concurrent.Wait
        Ribosome.Control.Exception
        Ribosome.Control.Lock
        Ribosome.Control.Monad.Error
        Ribosome.Control.Monad.Ribo
        Ribosome.Control.Ribosome
        Ribosome.Control.StrictRibosome
        Ribosome.Data.Conduit
        Ribosome.Data.Conduit.Composition
        Ribosome.Data.ErrorReport
        Ribosome.Data.Errors
        Ribosome.Data.Foldable
        Ribosome.Data.Mapping
        Ribosome.Data.Maybe
        Ribosome.Data.PersistError
        Ribosome.Data.Scratch
        Ribosome.Data.ScratchOptions
        Ribosome.Data.Setting
        Ribosome.Data.SettingError
        Ribosome.Data.String
        Ribosome.Data.Syntax
        Ribosome.Data.Text
        Ribosome.Error.Report
        Ribosome.Error.Report.Class
        Ribosome.File
        Ribosome.Internal.IO
        Ribosome.Internal.NvimObject
        Ribosome.Log
        Ribosome.Mapping
        Ribosome.Menu.Data.Menu
        Ribosome.Menu.Data.MenuAction
        Ribosome.Menu.Data.MenuConfig
        Ribosome.Menu.Data.MenuConsumer
        Ribosome.Menu.Data.MenuConsumerAction
        Ribosome.Menu.Data.MenuConsumerUpdate
        Ribosome.Menu.Data.MenuEvent
        Ribosome.Menu.Data.MenuItem
        Ribosome.Menu.Data.MenuRenderEvent
        Ribosome.Menu.Data.MenuResult
        Ribosome.Menu.Data.MenuUpdate
        Ribosome.Menu.Nvim
        Ribosome.Menu.Prompt.Data.Codes
        Ribosome.Menu.Prompt.Data.CursorUpdate
        Ribosome.Menu.Prompt.Data.InputEvent
        Ribosome.Menu.Prompt.Data.Prompt
        Ribosome.Menu.Prompt.Data.PromptConfig
        Ribosome.Menu.Prompt.Data.PromptConsumed
        Ribosome.Menu.Prompt.Data.PromptConsumerUpdate
        Ribosome.Menu.Prompt.Data.PromptEvent
        Ribosome.Menu.Prompt.Data.PromptRenderer
        Ribosome.Menu.Prompt.Data.PromptState
        Ribosome.Menu.Prompt.Data.PromptUpdate
        Ribosome.Menu.Prompt.Data.TextUpdate
        Ribosome.Menu.Prompt.Nvim
        Ribosome.Menu.Prompt.Run
        Ribosome.Menu.Run
        Ribosome.Menu.Simple
        Ribosome.Msgpack.Decode
        Ribosome.Msgpack.Encode
        Ribosome.Msgpack.Error
        Ribosome.Msgpack.NvimObject
        Ribosome.Msgpack.Util
        Ribosome.Nvim.Api.Data
        Ribosome.Nvim.Api.Generate
        Ribosome.Nvim.Api.GenerateData
        Ribosome.Nvim.Api.GenerateIO
        Ribosome.Nvim.Api.IO
        Ribosome.Nvim.Api.RpcCall
        Ribosome.Orphans
        Ribosome.Persist
        Ribosome.Plugin
        Ribosome.Plugin.Builtin
        Ribosome.Plugin.Mapping
        Ribosome.Plugin.RpcHandler
        Ribosome.Plugin.TH
        Ribosome.Plugin.TH.Command
        Ribosome.Plugin.TH.Handler
        Ribosome.Plugin.Watch
        Ribosome.Prelude
        Ribosome.PreludeExport
        Ribosome.Scratch
        Ribosome.System.Time
        Ribosome.Tmux.Run
        Ribosome.Unsafe
    hs-source-dirs: lib
    other-modules:
        Prelude
    default-language: Haskell2010
    default-extensions: AutoDeriveTypeable BangPatterns BinaryLiterals
                        ConstraintKinds DataKinds DefaultSignatures DeriveDataTypeable
                        DeriveFoldable DeriveFunctor DeriveGeneric DeriveTraversable
                        DoAndIfThenElse EmptyDataDecls ExistentialQuantification
                        FlexibleContexts FlexibleInstances FunctionalDependencies GADTs
                        GeneralizedNewtypeDeriving InstanceSigs KindSignatures LambdaCase
                        MonadFailDesugaring MultiParamTypeClasses MultiWayIf NamedFieldPuns
                        OverloadedStrings PartialTypeSignatures PatternGuards PolyKinds
                        RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving
                        TupleSections TypeApplications TypeFamilies TypeOperators
                        TypeSynonymInstances UnicodeSyntax ViewPatterns
    build-depends:
        MissingH >=1.4.1.0 && <1.5,
        aeson >=1.3.1.1 && <1.4,
        ansi-terminal >=0.8.2 && <0.9,
        base-noprelude >=4.7 && <5,
        bytestring >=0.10.8.2 && <0.11,
        cereal >=0.5.7.0 && <0.6,
        cereal-conduit >=0.8.0 && <0.9,
        chiasma >=0.1.0.0 && <0.2,
        composition >=1.0.2.1 && <1.1,
        composition-extra >=2.0.0 && <2.1,
        conduit >=1.3.1 && <1.4,
        conduit-extra >=1.3.0 && <1.4,
        containers >=0.5.11.0 && <0.6,
        cornea >=0.2.2.0 && <0.3,
        data-default >=0.7.1.1 && <0.8,
        deepseq >=1.4.3.0 && <1.5,
        directory >=1.3.1.5 && <1.4,
        either >=5.0.1 && <5.1,
        exceptions >=0.10.0 && <0.11,
        filepath >=1.4.2 && <1.5,
        free >=5.0.2 && <5.1,
        hslogger >=1.2.12 && <1.3,
        lens >=4.16.1 && <4.17,
        lifted-async >=0.10.0.3 && <0.11,
        lifted-base >=0.2.3.12 && <0.3,
        messagepack >=0.5.4 && <0.6,
        monad-control >=1.0.2.3 && <1.1,
        monad-loops >=0.4.3 && <0.5,
        mtl >=2.2.2 && <2.3,
        nvim-hs >=2.1.0.0 && <2.2,
        path >=0.6.1 && <0.7,
        path-io >=1.3.3 && <1.4,
        pretty-terminal >=0.1.0.0 && <0.2,
        prettyprinter >=1.2.1 && <1.3,
        prettyprinter-ansi-terminal >=1.1.1.2 && <1.2,
        process >=1.6.3.0 && <1.7,
        relude >=0.1.1 && <0.2,
        resourcet >=1.2.2 && <1.3,
        safe >=0.3.17 && <0.4,
        split >=0.2.3.3 && <0.3,
        stm >=2.4.5.1 && <2.5,
        stm-chans >=3.0.0.4 && <3.1,
        stm-conduit >=4.0.1 && <4.1,
        template-haskell >=2.13.0.0 && <2.14,
        text >=1.2.3.1 && <1.3,
        th-abstraction >=0.2.10.0 && <0.3,
        time >=1.8.0.2 && <1.9,
        transformers >=0.5.5.0 && <0.6,
        transformers-base >=0.4.5.2 && <0.5,
        typed-process >=0.2.3.0 && <0.3,
        unix >=2.7.2.2 && <2.8,
        unliftio >=0.2.9.0 && <0.3,
        unliftio-core >=0.1.2.0 && <0.2,
        utf8-string >=1.0.1.1 && <1.1
