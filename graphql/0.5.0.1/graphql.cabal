cabal-version: 1.12
name: graphql
version: 0.5.0.1
license: BSD3
license-file: LICENSE
copyright: (c) 2019 Eugen Wissner,
           (c) 2015-2017 J. Daniel Navarro
maintainer: belka@caraus.de
author: Danny Navarro <j@dannynavarro.net>,
        Matthías Páll Gissurarson <mpg@mpg.is>,
        Sólrún Halla Einarsdóttir <she@mpg.is>
homepage: https://github.com/caraus-ecms/graphql#readme
bug-reports: https://github.com/caraus-ecms/graphql/issues
synopsis: Haskell GraphQL implementation
description:
    This package provides a rudimentary parser for the <https://graphql.github.io/graphql-spec/June2018/ GraphQL> language.
category: Language
build-type: Simple
data-files:
    tests/data/kitchen-sink.graphql
    tests/data/kitchen-sink.min.graphql
extra-source-files:
    CHANGELOG.md
    README.md
    LICENSE
    docs/tutorial/tutorial.lhs

source-repository head
    type: git
    location: https://github.com/caraus-ecms/graphql

library
    exposed-modules:
        Language.GraphQL
        Language.GraphQL.AST
        Language.GraphQL.AST.Core
        Language.GraphQL.AST.Transform
        Language.GraphQL.Encoder
        Language.GraphQL.Error
        Language.GraphQL.Execute
        Language.GraphQL.Lexer
        Language.GraphQL.Parser
        Language.GraphQL.Schema
        Language.GraphQL.Trans
        Language.GraphQL.Type
    hs-source-dirs: src
    other-modules:
        Paths_graphql
    default-language: Haskell2010
    build-depends:
        aeson >=1.4.4.0 && <1.5,
        base >=4.7 && <5,
        megaparsec >=7.0.5 && <7.1,
        text >=1.2.3.1 && <1.3,
        transformers >=0.5.6.2 && <0.6,
        unordered-containers >=0.2.10.0 && <0.3

test-suite tasty
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: tests
    other-modules:
        Language.GraphQL.EncoderSpec
        Language.GraphQL.ErrorSpec
        Language.GraphQL.LexerSpec
        Language.GraphQL.ParserSpec
        Test.KitchenSinkSpec
        Test.StarWars.Data
        Test.StarWars.QuerySpec
        Test.StarWars.Schema
        Paths_graphql
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        aeson >=1.4.4.0 && <1.5,
        base >=4.7 && <5,
        graphql -any,
        hspec >=2.7.1 && <2.8,
        hspec-expectations >=0.8.2 && <0.9,
        megaparsec >=7.0.5 && <7.1,
        raw-strings-qq ==1.1.*,
        text >=1.2.3.1 && <1.3,
        transformers >=0.5.6.2 && <0.6
