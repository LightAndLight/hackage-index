cabal-version:  2.2
name:           memoize
version:        1.1.0
x-revision: 1
license:        BSD-3-Clause
license-file:   LICENSE
stability:      experimental
author:         Jesse A. Tov <jesse.tov@gmail.com>
maintainer:     jesse.tov@gmail.com
category:       Data
synopsis:       A memoization library
build-type:     Simple
tested-with:    GHC == 9.0.1, GHC == 8.10.7, GHC == 8.6.5

description:
        This library provides a type class 'Memoizable' for memoizing
        functions, along with instances for a variety of argument types.
        It includes a Template Haskell function for deriving
        'Memoizable' instances for arbitrary algebraic datatypes.
        .
        The library constructs pure memo caches without the use of
        'unsafePerformIO'.  This technique relies on implementation
        assumptions that, while not guaranteed by the semantics of
        Haskell, appear to be true.

library
  build-depends:        base >=4.8 && <5,
                        template-haskell >=2 && <3
  default-language:     Haskell98

  ghc-options:          -Wall -fno-warn-orphans
  hs-source-dirs:       src
  exposed-modules:
    Data.Function.Memoize
  other-modules:
    Data.Function.Memoize.TH
    Data.Function.Memoize.Class

test-suite memoize-test1
    Default-Language: Haskell98
    Hs-Source-Dirs: test
    Type: exitcode-stdio-1.0
    Main-is: test1.hs
    build-depends: base, memoize

test-suite memoize-test2
    Default-Language: Haskell98
    Hs-Source-Dirs: test
    Type: exitcode-stdio-1.0
    Main-is: test2.hs
    build-depends: base, memoize

test-suite memoize-test3
    Default-Language: Haskell98
    Hs-Source-Dirs: test
    Other-Modules: Test3Helper
    Type: exitcode-stdio-1.0
    Main-is: test3.hs
    build-depends: base, memoize

source-repository head
  type:                 git
  location:             git://github.com/tov/memoize.git
