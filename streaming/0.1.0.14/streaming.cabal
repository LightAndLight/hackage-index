name:                streaming
version:             0.1.0.14
cabal-version:       >=1.10
build-type:          Simple
synopsis:            a free monad transformer optimized for streaming applications

description:         `Stream` can be used wherever `FreeT` is used. The compiler's
                     standard range of optimizations work better for operations 
                     written in terms of `Stream`. @FreeT f m r@ / @Stream f m r@
                     is of course extremely general, and many functor-general combinators
                     are exported by the general module @Streaming@. 
                     . 
                     See the examples in @Streaming.Prelude@ for a sense of how 
                     simple the library is to use and think about. 
                     That module is focused on employment with such base functors 
                     (readings of the @f@ in @Stream f m r@) that express 
                     different forms of effectful sequences. Some of these appear 
                     elsewhere under titles like
                     .
                     > pipes:      Producer a m r, Producer a m (Producer a m r), FreeT (Producer a m) m r
                     > io-streams: InputStream a, Generator a r
                     > conduit:    Source m a, ConduitM () o m r
                     .
                     and the like. @Streaming.Prelude@ closely follows @Pipes.Prelude@, but cleverly /omits the pipes/:
                     .
                     >>> S.stdoutLn $ S.take 2 S.stdinLn
                     let's<Enter>
                     let's
                     stream<Enter>
                     stream
                     .
                     And here we do a little /connect and resume/, as the streaming-io experts call it:
                     .
                     >>> rest <- S.print $ S.splitAt 3 $ S.each [1..10]
                     1
                     2
                     3
                     >>> S.sum rest
                     49
                     .
                     Somehow, we didn't even need a four-character operator for that, nor advice
                     about best practices; just ordinary Haskell common sense. 
                     .
                     The simplest form of interoperation with 
                     <http://hackage.haskell.org/package/pipes pipes>
                     is accomplished with this isomorphism:
                     .  
                     > Pipes.unfoldr Streaming.next        :: Stream (Of a) m r   -> Producer a m r
                     > Streaming.unfoldr Pipes.next        :: Producer a m r      -> Stream (Of a) m r                     
                     .
                     Interoperation with 
                     <http://hackage.haskell.org/package/io-streams io-streams> 
                     is thus:
                     .
                     > Streaming.reread IOStreams.read     :: InputStream a       -> Stream (Of a) IO ()
                     > IOStreams.unfoldM Streaming.uncons  :: Stream (Of a) IO () -> IO (InputStream a)
                     .
                     A simple exit to <http://hackage.haskell.org/package/conduit conduit> would be, e.g.:
                     .
                     > Conduit.unfoldM Streaming.uncons    :: Stream (Of a) m ()  -> Source m a
                     .
                     These conversions should never be more expensive than a single @>->@ or @=$=@.
                     .
                     At a much more general level, we also of course have interoperation with 
                     <http://hackage.haskell.org/package/free free>:
                     .
                     > Free.iterTM  Stream.wrap              :: FreeT f m a -> Stream f m a
                     > Stream.iterTM Free.wrap               :: Stream f m a -> FreeT f m a 
                     .
                     For some simple ghci examples, see the commentary throughout the Prelude module.
                     For slightly more advanced usage see the commentary in the haddocks of <https://hackage.haskell.org/package/streaming-bytestring streaming-bytestring>
                     and e.g. 
                     <https://gist.github.com/michaelt/6c6843e6dd8030e95d58 these replicas> of shell-like programs from
                     the io-streams tutorial.
                     Here's a simple <https://gist.github.com/michaelt/2dcea1ba32562c091357 streaming GET request> with
                     intrinsically streaming byte streams.
                     
                     
license:             BSD3
license-file:        LICENSE
author:              michaelt
maintainer:          what_is_it_to_do_anything@yahoo.com
stability:           Experimental
homepage:            https://github.com/michaelt/streaming
bug-reports:         https://github.com/michaelt/streaming/issues
category:            Data, Pipes
source-repository head
    type: git
    location: https://github.com/michaelt/streaming


library
  exposed-modules:     Streaming, 
                       Streaming.Prelude,
                       Streaming.Internal

    -- other-modules:       
  other-extensions:    RankNTypes, CPP,
                       StandaloneDeriving, FlexibleContexts, 
                       DeriveDataTypeable, DeriveFoldable, 
                       DeriveFunctor, DeriveTraversable, 
                       UndecidableInstances
  
  build-depends:       base >=4.6 && <4.9
                     , mtl >=2.1 && <2.3
                     , mmorph >=1.0 && <1.2
                     , transformers >=0.3 && <0.5

  default-language:  Haskell2010
  


