name:                streaming
version:             0.1.0.8
x-revision: 1
cabal-version:       >=1.10
build-type:          Simple
synopsis:            A free monad transformer optimized for streaming applications.
                     
description:         `Stream` can be used wherever `FreeT` is used. The compiler's
                     standard range of optimizations work better for operations 
                     written in terms of `Stream`. See the examples in @Streaming.Prelude@ 
                     for a sense of how simple the library is to use and think about.
                     . 
                     @Streaming.Prelude@ closely follows 
                     @Pipes.Prelude@, but cleverly /omits the pipes/. It is focused 
                     on employment with base functors which generate
                     effectful sequences. These appear elsewhere under titles like
                     .
                     > pipes:      Producer a m r, Producer a m (Producer a m r), FreeT (Producer a m) m r
                     > io-streams: InputStream a, Generator a r
                     > conduit:    Source m a, ConduitM () o m r
                     .
                     and the like.
                     .
                     Interoperation with 
                     <http://hackage.haskell.org/package/pipes pipes>
                     is accomplished with this isomorphism:
                     .  
                     > Pipes.unfoldr Streaming.next        :: Stream (Of a) m r   -> Producer a m r
                     > Streaming.unfoldr Pipes.next        :: Producer a m r      -> Stream (Of a) m r                     
                     .
                     Interoperation with 
                     <http://hackage.haskell.org/package/io-streams io-streams> 
                     is thus:
                     .
                     > Streaming.reread IOStreams.read     :: InputStream a       -> Stream (Of a) IO ()
                     > IOStreams.unfoldM Streaming.uncons  :: Stream (Of a) IO () -> IO (InputStream a)
                     .
                     A simple exit to <http://hackage.haskell.org/package/conduit conduit> would be, e.g.:
                     .
                     > Conduit.unfoldM Streaming.uncons    :: Stream (Of a) m ()  -> Source m a
                     .
                     These conversions should never be more expensive than a single @>->@ or @=$=@.
                     .

                     
license:             BSD3
license-file:        LICENSE
author:              michaelt
maintainer:          what_is_it_to_do_anything@yahoo.com
stability:           Experimental
homepage:            https://github.com/michaelt/streaming
bug-reports:         https://github.com/michaelt/streaming/issues
category:            Data, Pipes
source-repository head
    type: git
    location: https://github.com/michaelt/streaming


library
  exposed-modules:     Streaming, 
                       Streaming.Prelude,
                       Streaming.Internal

    -- other-modules:       
  other-extensions:    LambdaCase, RankNTypes, EmptyCase, 
                       StandaloneDeriving, FlexibleContexts, 
                       DeriveDataTypeable, DeriveFoldable, 
                       DeriveFunctor, DeriveTraversable, 
                       UndecidableInstances
  
  build-depends:       base >=4.8 && <4.9
                     , mtl >=2.1 && <2.3
                     , mmorph >=1.0 && <1.2
                     , transformers >=0.3 && <0.5

  default-language:  Haskell2010
  


