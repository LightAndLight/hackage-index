name:                streaming
version:             0.1.3.2
cabal-version:       >=1.10
build-type:          Simple
synopsis:            an elementary streaming prelude and a general stream type.

description:         @Streaming.Prelude@ exports an elementary streaming prelude relating to 
                     an elementary source\/generator\/producer type, namely @Stream (Of a) m r@. 
                     The main module, @Streaming@, exports a much more general type, 
                     @Stream f m r@, which
                     can be used to stream successive distinct steps characterized by /any/ 
                     functor @f@, though we are here interested only in a limited range of 
                     cases. 
                     .
                     The streaming-io libraries have various devices for dealing
                     with effectful variants of @[a]@ or @([a],r)@. But it is only with
                     the general type @Stream f m r@, or some equivalent, 
                     that one can hope to stream their sorts of stream, as one 
                     makes lists of lists in the Haskell @Prelude@ and @Data.List@. 
                     One needs some such type if we are to
                     express a properly streaming equivalent of e.g.
                     .
                     > group :: Ord a => [a] -> [[a]]
                     > chunksOf :: Int -> [a] -> [[a]]
                     > lines :: [Char] -> [[Char]] -- but similarly with bytestring, etc.
                     .
                     to mention a few obviously desirable operations. But once one grasps 
                     the concept needed to express those functions - i.e. @Stream f m r@ or
                     some equivalent - then one will also see that, 
                     with it, one is already in possession of a complete
                     elementary streaming library - since one possesses @Stream (Of a) m r@, which
                     is the type of a \'generator\' or \'producer\' or whatever you call an 
                     effectful stream of items. The present @Streaming.Prelude@ is the
                     simplest streaming library that can replicate anything like the 
                     API of the @Prelude@ and @Data.List@. 
                     .
                     The emphasis of the library is on interoperation; for
                     the rest its advantages are: extreme simplicity and re-use of 
                     intuitions the user has gathered from mastery of @Prelude@ and
                     @Data.List@. The two conceptual pre-requisites are some 
                     comprehension of monad transformers and some familiarity 
                     with \'rank 2 types\'.
                     . 
                     See the 
                     <https://hackage.haskell.org/package/streaming#readme readme> below
                     for an explanation, including the examples linked there. Elementary usage can be divined from the ghci examples in 
                     @Streaming.Prelude@ and perhaps from this rough beginning of a 
                     <https://github.com/michaelt/streaming-tutorial/blob/master/tutorial.md tutorial> Note also the 
                     <https://hackage.haskell.org/package/streaming-bytestring streaming bytestring> 
                     and 
                     <https://hackage.haskell.org/package/streaming-utils streaming utils> 
                     packages.
                     .
                     The simplest form of interoperation with <http://hackage.haskell.org/package/pipes pipes>
                     is accomplished with this isomorphism:
                     .
                     > Pipes.unfoldr Streaming.next        :: Stream (Of a) m r   -> Producer a m r
                     > Streaming.unfoldr Pipes.next        :: Producer a m r      -> Stream (Of a) m r                     
                     .
                     Interoperation with <http://hackage.haskell.org/package/io-streams io-streams> is thus:
                     .
                     > Streaming.reread IOStreams.read     :: InputStream a       -> Stream (Of a) IO ()
                     > IOStreams.unfoldM Streaming.uncons  :: Stream (Of a) IO () -> IO (InputStream a)
                     .
                     A simple exit to <http://hackage.haskell.org/package/conduit conduit> would be, e.g.:
                     .
                     > Conduit.unfoldM Streaming.uncons    :: Stream (Of a) m ()  -> Source m a
                     .
                     These conversions should never be more expensive than a single @>->@ or @=$=@. Further
                     points of comparison are discussed in the 
                     <https://hackage.haskell.org/package/streaming#readme readme>
                     below.
                     .
                     Here are the results of some
                     <https://gist.github.com/michaelt/f19bef01423b17f29ffd microbenchmarks> 
                     based on the
                     <https://github.com/ekmett/machines/blob/master/benchmarks/Benchmarks.hs benchmarks> 
                     included in the machines package:
                     .
                     <<http://i.imgur.com/sSG5MvH.png>>
                     .

                     
license:             BSD3
license-file:        LICENSE
author:              michaelt
maintainer:          what_is_it_to_do_anything@yahoo.com
stability:           Experimental
homepage:            https://github.com/michaelt/streaming
bug-reports:         https://github.com/michaelt/streaming/issues
category:            Data, Pipes, Streaming
extra-source-files:  README.md

source-repository head
    type: git
    location: https://github.com/michaelt/streaming


library
  exposed-modules:     Streaming, 
                       Streaming.Prelude,
                       Streaming.Internal

    -- other-modules:       
  other-extensions:    RankNTypes, CPP,
                       StandaloneDeriving, FlexibleContexts, 
                       DeriveDataTypeable, DeriveFoldable, 
                       DeriveFunctor, DeriveTraversable, 
                       UndecidableInstances
  
  build-depends:       base >=4.6 && <5
                     , mtl >=2.1 && <2.3
                     , mmorph >=1.0 && <1.2
                     , transformers >=0.4 && <0.5
                     , transformers-base
                     , bytestring
                     , time
                     , resourcet
                     , exceptions
                     , containers
                     
  default-language:  Haskell2010
  


