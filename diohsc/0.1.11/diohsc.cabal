cabal-version:      1.18
name:               diohsc
version:            0.1.11
license:            GPL-3
license-file:       COPYING
maintainer:         mbays@sdf.org
author:             Martin Bays
homepage:           https://mbays.sdf.org/diohsc
synopsis:           Gemini client
description:
    Line-based command-oriented interactive client for the gemini protocol.

category:           Browser
build-type:         Simple
extra-doc-files:
    README.md
    THANKS
    diohscrc.sample
    Makefile
    diohsc.1.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://repo.or.cz/diohsc.git

flag libiconv
    description:
        On windows: use libiconv for charset conversion
        (iconv is always used on POSIX systems, with no need for this flag)

    default:     False
    manual:      True

flag magic
    description: Use libmagic to determine mimetypes of local files
    default:     False
    manual:      True

flag irilinks
    description:
        Allow IRIs in gemtext links (preparing for likely spec change).
        Punycoding is not currently supported.

    default:     False
    manual:      True

executable diohsc
    main-is:          diohsc.hs
    other-modules:
        ActiveIdentities
        Alias
        ANSIColour
        BoundedBSChan
        BStack
        ClientCert
        ClientSessionManager
        Command
        CommandLine
        Fingerprint
        GeminiProtocol
        Identity
        Marks
        MetaString
        Mundanities
        Opts
        Pager
        Prompt
        Request
        RunExternal
        ServiceCerts
        Slurp
        TextGemini
        URI
        Util
        WCWidth
        Version

    default-language: Haskell2010
    ghc-options:      -threaded -Wall
    build-depends:
        base >=4.3 && <5,
        asn1-encoding <0.10,
        asn1-types >=0.3.4 && <0.4,
        bytestring >=0.10.4.0 && <0.12,
        containers >=0.5.5.1 && <0.7,
        cryptonite >=0.26 && <0.31,
        data-default-class >=0.1.2.0 && <0.2,
        data-hash >=0.2.0.1 && <0.3,
        directory >=1.2.1.0 && <1.4,
        drunken-bishop >=0.1.0.0 && <0.2,
        exceptions >=0.10.4 && <0.11,
        filepath >=1.3.0.2 && <1.5,
        haskeline ==0.8.*,
        hourglass >=0.2.12 && <0.3,
        mime >=0.4.0.2 && <0.5,
        mtl >=2.1.3.1 && <2.4,
        memory >=0.14 && <0.18,
        network >=2.4.2.3 && <3.2,
        network-simple >=0.4.3 && <0.5,
        network-uri >=2.6.3.0 && <2.8,
        parsec >=3.1.5 && <3.2,
        pem >=0.2.4 && <0.3,
        process >=1.2.0.0 && <1.7,
        regex-compat >=0.95.1 && <0.96,
        rset < 1.1,
        safe >=0.3.19 && <0.4,
        temporary ==1.3.*,
        terminal-size >=0.3.2.1 && <0.4,
        text >=1.1.0.0 && <2.1,
        tls >=1.5.4 && <1.6,
        transformers >=0.3.0.0 && <0.7,
        x509 >=1.7.5 && <1.8,
        x509-store >=1.6.7 && <1.7,
        x509-validation >=1.6.11 && <1.7

    if os(windows)
        cpp-options: -DWINDOWS

    else
        build-depends: unix >=2.7.0.1 && <2.8

    if flag(magic)
        cpp-options:   -DMAGIC
        build-depends: magic <1.2

    if flag(irilinks)
        cpp-options: -DIRILinks

    if (!os(windows) || flag(libiconv))
        cpp-options:   -DICONV
        build-depends: iconv >=0.4.1.3 && <0.5
