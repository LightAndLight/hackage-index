Name:                   nested-routes
Version:                7.1.1
Author:                 Athan Clark <athan.clark@gmail.com>
Maintainer:             Athan Clark <athan.clark@gmail.com>
License:                BSD3
License-File:           LICENSE
Synopsis:               Declarative, compositional Wai responses
Category:               Web
Description:
  A method to writing Wai responses
  .
  This library attempts to make it easier to write nice Wai response handlers
  by giving us a Sinatra/
  <https://hackage.haskell.org/package/scotty Scotty>-like syntax for declaring HTTP-verb oriented
  routes, in addition to file-extension handling and rose-tree like composition.
  Not only do we have literal route specification, like
  <https://hackage.haskell.org/package/scotty Scotty> &
  <https://hackage.haskell.org/package/spock Spock>, but we
  can also embed
  <https://hackage.haskell.org/package/attoparsec Attoparsec>
  parsers and <https://hackage.haskell.org/package/regex-compat Regular Expressions>
  /directly/ in our routes, with our handlers
  reflecting their results.
Cabal-Version:          >= 1.10
Build-Type:             Simple


Flag Example
  Description:          Build the trivial example.
  Default:              False

Library
  Default-Language:     Haskell2010
  HS-Source-Dirs:       src
  GHC-Options:          -Wall
  Exposed-Modules:      Web.Routes.Nested
                        Web.Routes.Nested.Match
                        Web.Routes.Nested.Types
  Build-Depends:        base >= 4.8 && < 5
                      , attoparsec
                      , bytestring
                      , composition-extra >= 2.0.0
                      , errors
                      , exceptions
                      , hashable
                      , hashtables
                      , mtl
                      , poly-arity >= 0.0.7
                      , pred-set >= 0.0.1
                      , pred-trie >= 0.5.1
                      , regex-compat
                      , semigroups
                      , text
                      , transformers
                      , tries
                      , unordered-containers
                      , wai-transformers >= 0.0.5
                      , wai-middleware-content-type >= 0.4.0
                      , wai-middleware-verbs >= 0.3.0


Test-Suite test
  Type:                 exitcode-stdio-1.0
  Default-Language:     Haskell2010
  HS-Source-Dirs:       src
                      , test
  GHC-Options:          -Wall -threaded
  Main-Is:              Test.hs
  Other-Modules:        Spec
                        Web.Routes.NestedSpec
                        Web.Routes.NestedSpec.Basic
                        Web.Routes.Nested
                        Web.Routes.Nested.Types
                        Web.Routes.Nested.Match
  Build-Depends:        base
                      , nested-routes
                      , attoparsec
                      , bytestring
                      , composition-extra
                      , errors
                      , exceptions
                      , hashable
                      , hashtables
                      , HSet
                      , http-types
                      , mtl
                      , poly-arity
                      , pred-trie
                      , pred-set
                      , regex-compat
                      , semigroups
                      , text
                      , transformers
                      , tries
                      , unordered-containers
                      , wai-transformers
                      , wai-middleware-content-type
                      , wai-middleware-verbs
                      , hspec
                      , hspec-wai


Executable example
  if flag(Example)
    Buildable: True
  else
    Buildable: False
  Default-Language:     Haskell2010
  HS-Source-Dirs:       src
                      , examples
  GHC-Options:          -Wall -threaded
  Main-Is:              Main.hs
  Other-Modules:        Web.Routes.Nested
                        Web.Routes.Nested.Types
                        Web.Routes.Nested.Match
  Build-Depends:        base
                      , nested-routes >= 7
                      , attoparsec
                      , bytestring
                      , composition-extra
                      , errors
                      , exceptions
                      , hashable
                      , hashtables
                      , HSet
                      , http-types
                      , mtl
                      , poly-arity
                      , pred-set
                      , pred-trie
                      , regex-compat
                      , semigroups
                      , text
                      , transformers
                      , tries
                      , unordered-containers
                      , wai-transformers
                      , wai-middleware-content-type
                      , wai-middleware-verbs
                      , warp


Source-Repository head
  Type:                 git
  Location:             https://github.com/athanclark/nested-routes.git
