Name:                   nested-routes
Version:                4.0.0
Author:                 Athan Clark <athan.clark@gmail.com>
Maintainer:             Athan Clark <athan.clark@gmail.com>
License:                BSD3
License-File:           LICENSE
Synopsis:               Declarative, compositional Wai responses
Category:               Web
Description:
  A method to writing Wai responses
  .
  This library attempts to make it easier to write nice Wai response handlers
  by giving us a Sinatra/
  <https://hackage.haskell.org/package/scotty Scotty>-like syntax for declaring HTTP-verb oriented
  routes, in addition to file-extension handling and rose-tree like composition.
  Not only do we have literal route specification, like
  <https://hackage.haskell.org/package/scotty Scotty> &
  <https://hackage.haskell.org/package/spock Spock>, but we
  can also embed
  <https://hackage.haskell.org/package/attoparsec Attoparsec>
  parsers and <https://hackage.haskell.org/package/regex-compat Regular Expressions>
  /directly/ in our routes, with our handlers
  reflecting their results. You can find more information in the
  <https://www.fpcomplete.com/user/AthanClark/nested-routes demo>.
  and the examples.
Cabal-Version:          >= 1.10
Build-Type:             Simple


Flag Example
  Description:          Build the trivial example.
  Default:              False

Flag Example-STM
  Description:          Build the Sha512 / STM nonce cache example.
  Default:              False


Library
  Default-Language:     Haskell2010
  HS-Source-Dirs:       src
  GHC-Options:          -Wall
  Exposed-Modules:      Web.Routes.Nested
                        Web.Routes.Nested.Types
                        Web.Routes.Nested.Types.UrlChunks
                        Web.Routes.Nested.VerbListener
                        Web.Routes.Nested.FileExtListener
                        Web.Routes.Nested.FileExtListener.Types
                        Web.Routes.Nested.FileExtListener.Text
                        Web.Routes.Nested.FileExtListener.Json
                        Web.Routes.Nested.FileExtListener.Builder
                        Web.Routes.Nested.FileExtListener.Blaze
                        Web.Routes.Nested.FileExtListener.ByteString
                        Web.Routes.Nested.FileExtListener.Lucid
                        Web.Routes.Nested.FileExtListener.Clay
                        Web.Routes.Nested.FileExtListener.Julius
                        Web.Routes.Nested.FileExtListener.Lucius
                        Web.Routes.Nested.FileExtListener.Cassius
  Build-Depends:        base >= 4.6 && < 5
                      , wai
                      , wai-extra
                      , wai-util
                      , http-types
                      , http-media
                      , mtl
                      , transformers
                      , witherable
                      , composition
                      , composition-extra >= 2.0.0
                      , semigroups
                      , constraints
                      , containers
                      , text
                      , aeson
                      , blaze-html
                      , lucid
                      , shakespeare
                      , clay
                      , bytestring
                      , bifunctors
                      , attoparsec
                      , regex-compat
                      , pred-trie >= 0.3
                      , tries
                      , poly-arity >= 0.0.6
                      , sets >= 0.0.5
                      , errors

Test-Suite spec
  Type:                 exitcode-stdio-1.0
  Default-Language:     Haskell2010
  Hs-Source-Dirs:       src
                      , test
  Ghc-Options:          -Wall
  Main-Is:              Spec.hs
  Build-Depends:        base
                      , hspec
                      , hspec-wai
                      , shakespeare
                      , lucid
                      , attoparsec
                      , regex-compat
                      , containers
                      , composition
                      , composition-extra
                      , semigroups
                      , text
                      , aeson
                      , wai-util
                      , blaze-html
                      , bytestring
                      , poly-arity
                      , witherable
                      , constraints
                      , pred-trie
                      , mtl
                      , wai
                      , warp
                      , transformers
                      , http-media
                      , http-types
                      , sets
                      , errors


Executable example
  if flag(Example)
    Buildable: True
  else
    Buildable: False
  Default-Language:     Haskell2010
  HS-Source-Dirs:       src
                      , examples
  GHC-Options:          -Wall
  Main-Is:              Main.hs
  Build-Depends:        base
                      , wai
                      , wai-extra
                      , wai-util
                      , warp
                      , http-types
                      , http-media
                      , mtl
                      , transformers
                      , witherable
                      , composition
                      , composition-extra
                      , semigroups
                      , constraints
                      , containers
                      , text
                      , aeson
                      , blaze-html
                      , lucid
                      , shakespeare
                      , clay
                      , bytestring
                      , attoparsec
                      , regex-compat
                      , pred-trie
                      , tries
                      , poly-arity
                      , sets
                      , errors

Executable example-stm
  if flag(Example-STM)
    Buildable: True
  else
    Buildable: False
  Default-Language:     Haskell2010
  HS-Source-Dirs:       src
                      , examples
  GHC-Options:          -Wall
  Main-Is:              STM.hs
  Other-Modules:        STM.Auth
                        Web.Routes.Nested
                        Web.Routes.Nested.FileExtListener
                        Web.Routes.Nested.FileExtListener.Blaze
                        Web.Routes.Nested.FileExtListener.Builder
                        Web.Routes.Nested.FileExtListener.ByteString
                        Web.Routes.Nested.FileExtListener.Json
                        Web.Routes.Nested.FileExtListener.Lucid
                        Web.Routes.Nested.FileExtListener.Text
                        Web.Routes.Nested.FileExtListener.Types
                        Web.Routes.Nested.Types
                        Web.Routes.Nested.Types.UrlChunks
                        Web.Routes.Nested.VerbListener
  Build-Depends:        base
                      , wai
                      , wai-extra
                      , wai-session
                      , cookie
                      , wai-util
                      , warp
                      , http-types
                      , http-media
                      , mtl
                      , transformers
                      , witherable
                      , composition
                      , composition-extra
                      , semigroups
                      , constraints
                      , containers
                      , text
                      , aeson
                      , blaze-html
                      , lucid
                      , shakespeare
                      , clay
                      , bytestring
                      , attoparsec
                      , regex-compat
                      , pred-trie
                      , tries
                      , poly-arity
                      , sets
                      , errors
                      , cryptonite
                      , memory
                      , time
                      , iso8601-time
                      , utf8-string
                      , stm
                      , errors
                      , cookie
                      , data-default
                      , blaze-builder
                      , base64-bytestring

Source-Repository head
  Type:                 git
  Location:             git://github.com/athanclark/nested-routes.git
