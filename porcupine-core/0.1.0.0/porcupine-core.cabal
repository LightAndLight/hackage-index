cabal-version: 1.12

-- This file has been generated from package.yaml by hpack version 0.32.0.
--
-- see: https://github.com/sol/hpack
--
-- hash: f4672c1d7de06695f07c27b5c5621ceffb7e30abb9469d2c87f60c0dbf8a6fff

name:           porcupine-core
version:        0.1.0.0
synopsis:       Express portable, composable and reusable data tasks and pipelines
description:    porcupine is centered around the PTask datatype, which represents a
                computation that will request access to some resources (both in read and write
                modes) and require some options (parameters, with docstrings and default
                values). PTasks are composable both sequentially and in parallel, into a
                pipeline of tasks. The resources and parameters are organized in a tree which
                will be automatically exposed to the outside world. This makes the pipeline
                self-documented, and makes it so any option or file required at some point by
                any task can be visualized and set/remapped (via a combination of a YAML/JSON
                config file and command-line arguments) before the pipeline will run. That
                means that the PTasks are completely agnostic of their data inputs, and that
                new data sources can be added without having to change any of the tasks' logic
                or even their types. This is done via the LocationAccessor
                typeclass. `porcupine-core` provides only access to local files (via
                resourcet), other location accessors will be in separate packages. See for
                instance the <https://hackage.haskell.org/package/porcupine-http
                `porcupine-http`> package to access HTTP urls. PTasks also provide caching
                thanks to the funflow package. See the README at
                <https://github.com/tweag/porcupine#README.md> and the examples in
                `porcupine-core` package.
category:       Data, Arrows, Combinators, Control, Pipes, Streaming, Numerical, JSON
homepage:       https://github.com/tweag/porcupine#readme
bug-reports:    https://github.com/tweag/porcupine/issues
maintainer:     Yves Parès <yves.pares@tweag.io>
copyright:      2018 EURL Tweag, NovaDiscovery
license:        MIT
license-file:   LICENSE
build-type:     Simple

source-repository head
  type: git
  location: https://github.com/tweag/porcupine

library
  exposed-modules:
      Control.Arrow.FoldA
      Data.Locations
      Data.Locations.Accessors
      Data.Locations.FunflowRemoteCache
      Data.Locations.Loc
      Data.Locations.LocationTree
      Data.Locations.LocVariable
      Data.Locations.LogAndErrors
      Data.Locations.Mappings
      Data.Locations.SerializationMethod
      Data.Locations.VirtualFile
      Data.Representable
      Porcupine
      Porcupine.Foldl
      Porcupine.Run
      Porcupine.Serials
      Porcupine.Tasks
      Porcupine.VFiles
      Streaming.TaskPipelineUtils
      System.ClockHelpers
      System.TaskPipeline
      System.TaskPipeline.Caching
      System.TaskPipeline.CLI
      System.TaskPipeline.ConfigurationReader
      System.TaskPipeline.Logger
      System.TaskPipeline.Options
      System.TaskPipeline.PorcupineTree
      System.TaskPipeline.PTask
      System.TaskPipeline.PTask.Internal
      System.TaskPipeline.Repetition
      System.TaskPipeline.Repetition.Foldl
      System.TaskPipeline.Repetition.Internal
      System.TaskPipeline.Repetition.Streaming
      System.TaskPipeline.Run
      System.TaskPipeline.VirtualFileAccess
  other-modules:
      Paths_porcupine_core
  hs-source-dirs:
      src
  ghc-options: -Wall
  build-depends:
      aeson
    , aeson-pretty
    , attoparsec
    , base >=4.10 && <5
    , binary
    , binary-orphans
    , bytestring
    , cassava
    , clock
    , conduit
    , containers
    , contravariant
    , data-default
    , deepseq
    , directory
    , docrecords ==0.1.*
    , filepath
    , foldl
    , formatting
    , funflow >=1.5.0
    , hashable
    , katip ==0.8.*
    , lens
    , monad-control
    , mtl
    , optparse-applicative
    , path
    , profunctors
    , reader-soup ==0.1.*
    , resourcet
    , safe-exceptions
    , store
    , streaming
    , streaming-bytestring
    , streaming-conduit
    , streaming-utils
    , template-haskell
    , temporary
    , text
    , transformers
    , transformers-base
    , unix
    , unliftio-core
    , unordered-containers
    , url
    , vector
    , vinyl
    , yaml
    , zlib
  default-language: Haskell2010

executable example-radon
  main-is: ExampleRadon.hs
  other-modules:
      Plotting
      Paths_porcupine_core
  hs-source-dirs:
      examples/example-radon
  ghc-options: -Wall
  build-depends:
      aeson
    , aeson-pretty
    , attoparsec
    , base >=4.10 && <5
    , binary
    , binary-orphans
    , bytestring
    , cassava
    , clock
    , conduit
    , containers
    , contravariant
    , data-default
    , deepseq
    , directory
    , docrecords
    , filepath
    , foldl
    , formatting
    , funflow >=1.5.0
    , hashable
    , hvega
    , katip ==0.8.*
    , lens
    , log-domain
    , monad-bayes
    , monad-control
    , mtl
    , optparse-applicative
    , path
    , porcupine-core
    , profunctors
    , reader-soup ==0.1.*
    , resourcet
    , safe-exceptions
    , store
    , streaming
    , streaming-bytestring
    , streaming-conduit
    , streaming-utils
    , template-haskell
    , temporary
    , text
    , transformers
    , transformers-base
    , unix
    , unliftio-core
    , unordered-containers
    , url
    , vector
    , vinyl
    , yaml
    , zlib
  default-language: Haskell2010

executable example0
  main-is: Example0.hs
  other-modules:
      Paths_porcupine_core
  hs-source-dirs:
      examples/example0
  ghc-options: -Wall
  build-depends:
      aeson
    , aeson-pretty
    , attoparsec
    , base >=4.10 && <5
    , binary
    , binary-orphans
    , bytestring
    , cassava
    , clock
    , conduit
    , containers
    , contravariant
    , data-default
    , deepseq
    , directory
    , docrecords ==0.1.*
    , filepath
    , foldl
    , formatting
    , funflow >=1.5.0
    , hashable
    , katip ==0.8.*
    , lens
    , monad-control
    , mtl
    , optparse-applicative
    , path
    , porcupine-core
    , profunctors
    , reader-soup ==0.1.*
    , resourcet
    , safe-exceptions
    , store
    , streaming
    , streaming-bytestring
    , streaming-conduit
    , streaming-utils
    , template-haskell
    , temporary
    , text
    , transformers
    , transformers-base
    , unix
    , unliftio-core
    , unordered-containers
    , url
    , vector
    , vinyl
    , yaml
    , zlib
  default-language: Haskell2010

executable example0.1
  main-is: Example0_1.hs
  other-modules:
      Paths_porcupine_core
  hs-source-dirs:
      examples/example0.1
  ghc-options: -Wall
  build-depends:
      aeson
    , aeson-pretty
    , attoparsec
    , base >=4.10 && <5
    , binary
    , binary-orphans
    , bytestring
    , cassava
    , clock
    , conduit
    , containers
    , contravariant
    , data-default
    , deepseq
    , directory
    , docrecords ==0.1.*
    , filepath
    , foldl
    , formatting
    , funflow >=1.5.0
    , hashable
    , katip ==0.8.*
    , lens
    , monad-control
    , mtl
    , optparse-applicative
    , path
    , porcupine-core
    , profunctors
    , reader-soup ==0.1.*
    , resourcet
    , safe-exceptions
    , store
    , streaming
    , streaming-bytestring
    , streaming-conduit
    , streaming-utils
    , template-haskell
    , temporary
    , text
    , transformers
    , transformers-base
    , unix
    , unliftio-core
    , unordered-containers
    , url
    , vector
    , vinyl
    , yaml
    , zlib
  default-language: Haskell2010

executable example1
  main-is: Example1.hs
  other-modules:
      Paths_porcupine_core
  hs-source-dirs:
      examples/example1
  ghc-options: -Wall
  build-depends:
      aeson
    , aeson-pretty
    , attoparsec
    , base >=4.10 && <5
    , binary
    , binary-orphans
    , bytestring
    , cassava
    , clock
    , conduit
    , containers
    , contravariant
    , data-default
    , deepseq
    , directory
    , docrecords ==0.1.*
    , filepath
    , foldl
    , formatting
    , funflow >=1.5.0
    , hashable
    , katip ==0.8.*
    , lens
    , monad-control
    , mtl
    , optparse-applicative
    , path
    , porcupine-core
    , profunctors
    , reader-soup ==0.1.*
    , resourcet
    , safe-exceptions
    , store
    , streaming
    , streaming-bytestring
    , streaming-conduit
    , streaming-utils
    , template-haskell
    , temporary
    , text
    , transformers
    , transformers-base
    , unix
    , unliftio-core
    , unordered-containers
    , url
    , vector
    , vinyl
    , yaml
    , zlib
  default-language: Haskell2010

executable example2
  main-is: Example2.hs
  other-modules:
      Paths_porcupine_core
  hs-source-dirs:
      examples/example2
  ghc-options: -Wall
  build-depends:
      aeson
    , aeson-pretty
    , attoparsec
    , base >=4.10 && <5
    , binary
    , binary-orphans
    , bytestring
    , cassava
    , clock
    , conduit
    , containers
    , contravariant
    , data-default
    , deepseq
    , directory
    , docrecords ==0.1.*
    , filepath
    , foldl
    , formatting
    , funflow >=1.5.0
    , hashable
    , katip ==0.8.*
    , lens
    , monad-control
    , mtl
    , optparse-applicative
    , path
    , porcupine-core
    , profunctors
    , reader-soup ==0.1.*
    , resourcet
    , safe-exceptions
    , store
    , streaming
    , streaming-bytestring
    , streaming-conduit
    , streaming-utils
    , template-haskell
    , temporary
    , text
    , transformers
    , transformers-base
    , unix
    , unliftio-core
    , unordered-containers
    , url
    , vector
    , vinyl
    , yaml
    , zlib
  default-language: Haskell2010
