name:                termonad
version:             0.1.0.0
synopsis:            Terminal emulator configurable in Haskell
description:         Please see <https://github.com/cdepillabout/termonad#readme README.md>.
homepage:            https://github.com/cdepillabout/termonad
license:             BSD3
license-file:        LICENSE
author:              Dennis Gosnell
maintainer:          cdep.illabout@gmail.com
copyright:           2017 Dennis Gosnell
category:            Text
build-type:          Custom
extra-source-files:  README.md
                   , CHANGELOG.md
cabal-version:       >=1.12

custom-setup
  setup-depends:     base
                   , Cabal
                   , cabal-doctest >=1.0.2 && <1.1

library
  hs-source-dirs:      src
  exposed-modules:     Termonad
                     , Termonad.App
                     , Termonad.Config
                     , Termonad.FocusList
                     , Termonad.Gtk
                     , Termonad.Keys
                     , Termonad.Prelude
                     , Termonad.Term
                     , Termonad.Types
                     , Termonad.XML
  build-depends:       base >= 4.7 && < 5
                     , classy-prelude
                     , colour
                     , constraints
                     , data-default
                     , dyre
                     , gi-gdk
                     , gi-gio
                     , gi-glib
                     , gi-gtk
                     , gi-pango
                     , gi-vte
                     , haskell-gi-base
                     , lens
                     , pretty-simple
                     , QuickCheck
                     , xml-conduit
                     , xml-html-qq
  default-language:    Haskell2010
  ghc-options:         -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  default-extensions:  DataKinds
                     , GADTs
                     , GeneralizedNewtypeDeriving
                     , InstanceSigs
                     , KindSignatures
                     , NamedFieldPuns
                     , NoImplicitPrelude
                     , OverloadedStrings
                     , OverloadedLabels
                     , OverloadedLists
                     , PatternSynonyms
                     , PolyKinds
                     , RankNTypes
                     , RecordWildCards
                     , ScopedTypeVariables
                     , TypeApplications
                     , TypeFamilies
                     , TypeOperators
  other-extensions:    TemplateHaskell

executable termonad
  main-is:             Main.hs
  hs-source-dirs:      app
  build-depends:       base
                     , termonad
  default-language:    Haskell2010
  ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N

test-suite doctests
  type:                exitcode-stdio-1.0
  main-is:             DocTest.hs
  hs-source-dirs:      test
  build-depends:       base
                     , doctest
                     , QuickCheck
                     , template-haskell
  default-language:    Haskell2010
  ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N

test-suite termonad-test
  type:                exitcode-stdio-1.0
  main-is:             Test.hs
  hs-source-dirs:      test
  build-depends:       base
                     , hedgehog
                     , lens
                     , termonad
                     , tasty
                     , tasty-hedgehog
  default-language:    Haskell2010
  ghc-options:         -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates -threaded -rtsopts -with-rtsopts=-N
  default-extensions:  DataKinds
                     , GADTs
                     , GeneralizedNewtypeDeriving
                     , InstanceSigs
                     , KindSignatures
                     , NamedFieldPuns
                     , NoImplicitPrelude
                     , OverloadedStrings
                     , OverloadedLabels
                     , OverloadedLists
                     , PatternSynonyms
                     , PolyKinds
                     , RankNTypes
                     , RecordWildCards
                     , ScopedTypeVariables
                     , TypeApplications
                     , TypeFamilies
                     , TypeOperators
  other-extensions:    TemplateHaskell

-- benchmark termonad-bench
--   type:                exitcode-stdio-1.0
--   main-is:             Bench.hs
--   hs-source-dirs:      bench
--   build-depends:       base
--                      , criterion
--                      , termonad
--   default-language:    Haskell2010
--   ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N

source-repository head
  type:     git
  location: git@github.com:cdepillabout/termonad.git
