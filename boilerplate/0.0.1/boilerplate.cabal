cabal-version: 2.2
name:          boilerplate
version:       0.0.1
synopsis:      Generate Haskell boilerplate.
license:       GPL-3.0-or-later
license-file:  LICENSE
author:        Tseen She
maintainer:    Tseen She
copyright:     2020 Tseen She
tested-with:   GHC ^>=8.8.3
category:      Building
description:
  Generates boilerplate from templates and markers in Haskell source code.

source-repository head
  type:     git
  location: https://gitlab.com/tseenshe/boilerplate.git

-- https://www.haskell.org/cabal/users-guide/cabal-projectindex.html

flag ghcflags
  description: Generate .ghc.flags files during compilation
  manual:      True
  default:     False

common deps
  build-depends:
    , base        >=4.11 && <5
    , containers
    , directory
    , filepath
    , ghc
    , text

  ghc-options:      -Wall -Werror=missing-home-modules -Werror=orphans
  default-language: Haskell2010

  if flag(ghcflags)
    build-tool-depends: hsinspect:hsinspect ==0.0.13
    build-depends:      ghcflags
    ghc-options:        -fplugin GhcFlags.Plugin

executable boilerplate
  import:         deps
  hs-source-dirs: exe
  main-is:        Main.hs
  ghc-options:    -rtsopts -threaded -with-rtsopts=-N8
  build-depends:
    , boilerplate
    , parsec       >3.1 && <3.2

library
  import:          deps
  hs-source-dirs:  library
  build-depends:
    , ghc-paths
    , hsinspect  ^>=0.0.13
    , parsers    >0.12   && <0.13
    , vector     >0.12   && <0.13

  -- cabal-fmt: expand library
  exposed-modules:
    Boilerplate.ConfigParser
    Boilerplate.Doc
    Boilerplate.GhcParser
    Boilerplate.Interpreter
    Boilerplate.RuleFinder
    Boilerplate.RuleParser
    Boilerplate.Types

test-suite test
  import:             deps
  hs-source-dirs:     test
  type:               exitcode-stdio-1.0
  main-is:            Driver.hs

  -- cabal-fmt: expand test -Driver
  other-modules:
    Boilerplate.ConfigParserTests
    Boilerplate.DocTests
    Boilerplate.Example
    Boilerplate.GhcParserTests
    Boilerplate.InterpreterTests
    Boilerplate.RuleParserTests

  build-depends:
    , boilerplate
    , extra        ^>=1.7.8
    , HUnit        ^>=1.6.0
    , parsec
    , tasty        ^>=1.3.1
    , tasty-hspec  ^>=1.1.5

  build-tool-depends: tasty-discover:tasty-discover ^>=4.2.1
  ghc-options:        -threaded
