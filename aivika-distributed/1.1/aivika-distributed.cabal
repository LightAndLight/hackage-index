name:            aivika-distributed
version:         1.1
synopsis:        Parallel distributed discrete event simulation module for the Aivika library
description:
    This package extends the aivika-transformers [1] package and allows running parallel distributed simulations.
    It uses an optimistic strategy known as the Time Warp method. To synchronize the global virtual time, 
    it uses Samadi's algorithm. 
    .
    Moreover, this package uses the author's modification that allows recovering the distributed
    simulation after temporary connection errors whenever possible. For that, you have to enable explicitly 
    the recovering mode and enable monitoring all logical processes including the specialized Time Server process 
    as it is shown in one of the test examples included in the distribution.
    .
    With the recovering mode enabled, you can try to build a distributed simulation using ordinary computers connected
    via the ordinary net. For example, such a distributed model could even consist of computers located in different 
    continents of the Earth, where the computers could be connected through the Internet. Here the most exciting thing 
    is that this is the optimistic distributed simulation with possible rollbacks. It is assumed that optimistic methods 
    tend to better support the parallelism inherited in the models. 
    .
    You can test the distributed simulation using your own laptop, although the package is still destined to be 
    used with a multi-core computer, or computers connected in the distributed cluster.
    .
    There are additional packages that allow you to run the distributed simulation experiments by using
    the Monte-Carlo method. They allow you to save the simulation results in SQL databases and then generate a report 
    or a set of reports consisting of HTML pages with charts, histograms, links to CSV tables, summary statistics etc.
    Please consult the AivikaSoft [3] website for more details.
    .
    Regarding the speed of simulation, the recent rough estimation is as follows. This estimation may change from 
    version to version. For example, in version 1.0 the rollback log was rewritten, which had a significant effect.
    .
    The distributed simulation module is slower up to 8-15 times in comparison with the sequential aivika [2] simulation 
    library using the equivalent sequential models. The lower estimation in 8 times is likely to correspond to complex 
    models. The upper estimation in 15 times will probably correspond to quite simple event-oriented and process-oriented 
    models, where the sequential module can be exceptionally fast. 
    .
    Note that you can run up to 7 parallel logical processes on a single 8-core processor computer and run the Time Server 
    process too. On a 36-core processor, you can launch up to 35 logical processes simultaneously.
    . 
    At the same time, the message passing between the logical processes can dramatically decrease the speed of distributed 
    simulation, especially if they cause rollbacks. Thus, much depends on the distributed model itself.
    .
    Finally, you can use the following test model [4] as an example.
    .
    \[1] <http://hackage.haskell.org/package/aivika-transformers>
    .
    \[2] <http://hackage.haskell.org/package/aivika>
    .
    \[3] <http://www.aivikasoft.com>
    .
    \[4] <https://github.com/dsorokin/aivika-distributed-test>
    .
category:        Simulation
license:         BSD3
license-file:    LICENSE
copyright:       (c) 2015-2017. David Sorokin <david.sorokin@gmail.com>
author:          David Sorokin
maintainer:      David Sorokin <david.sorokin@gmail.com>
homepage:        http://www.aivikasoft.com
cabal-version:   >= 1.6
build-type:      Simple
tested-with:     GHC == 7.10.1

extra-source-files:  tests/Guard1.hs
                     tests/MachRep1.hs
                     tests/MachRep1Simple.hs
                     tests/MachRep1SimpleWithMonitoring.hs
                     tests/MachRep2.hs
                     tests/MachRep2Distributed.hs
                     tests/MachRep2DistributedReproducible.hs
                     tests/MachRep2DistributedReproducibleFaultTolerant.hs
                     tests/MachRep2DistributedWithMonitoring.hs
                     tests/MachRep2Reproducible.hs
                     tests/MachRep2Sync.hs
                     tests/MachRep2SyncIO.hs
                     tests/MachRep2WithMonitoring.hs
                     tests/SimpleLocalnetHelper.hs
                     tests/cluster.conf
                     CHANGELOG.md

library

    exposed-modules: Simulation.Aivika.Distributed
                     Simulation.Aivika.Distributed.Optimistic
                     Simulation.Aivika.Distributed.Optimistic.DIO
                     Simulation.Aivika.Distributed.Optimistic.Guard
                     Simulation.Aivika.Distributed.Optimistic.Generator
                     Simulation.Aivika.Distributed.Optimistic.QueueStrategy
                     Simulation.Aivika.Distributed.Optimistic.Message
                     Simulation.Aivika.Distributed.Optimistic.Priority
                     Simulation.Aivika.Distributed.Optimistic.Ref.Base
                     Simulation.Aivika.Distributed.Optimistic.Ref.Base.Lazy
                     Simulation.Aivika.Distributed.Optimistic.Ref.Base.Strict
                     Simulation.Aivika.Distributed.Optimistic.State
                     Simulation.Aivika.Distributed.Optimistic.TimeServer

    other-modules:   Simulation.Aivika.Distributed.Optimistic.Internal.Channel
                     Simulation.Aivika.Distributed.Optimistic.Internal.DIO
                     Simulation.Aivika.Distributed.Optimistic.Internal.Event
                     Simulation.Aivika.Distributed.Optimistic.Internal.Expect
                     Simulation.Aivika.Distributed.Optimistic.Internal.InputMessageQueue
                     Simulation.Aivika.Distributed.Optimistic.Internal.IO
                     Simulation.Aivika.Distributed.Optimistic.Internal.KeepAliveManager
                     Simulation.Aivika.Distributed.Optimistic.Internal.Message
                     Simulation.Aivika.Distributed.Optimistic.Internal.OutputMessageQueue
                     Simulation.Aivika.Distributed.Optimistic.Internal.Priority
                     Simulation.Aivika.Distributed.Optimistic.Internal.Ref
                     Simulation.Aivika.Distributed.Optimistic.Internal.Ref.Lazy
                     Simulation.Aivika.Distributed.Optimistic.Internal.Ref.Strict
                     Simulation.Aivika.Distributed.Optimistic.Internal.SignalHelper
                     Simulation.Aivika.Distributed.Optimistic.Internal.TimeServer
                     Simulation.Aivika.Distributed.Optimistic.Internal.TimeWarp
                     Simulation.Aivika.Distributed.Optimistic.Internal.TransientMessageQueue
                     Simulation.Aivika.Distributed.Optimistic.Internal.UndoableLog

    build-depends:   base >= 4.6.0.0 && < 6,
                     mtl >= 2.1.1,
                     array >= 0.3.0.0,
                     stm >= 2.4.2,
                     random >= 1.0.0.3,
                     mwc-random >= 0.13.0.0,
                     binary >= 0.6.4.0,
                     time >= 1.5.0.1,
                     containers >= 0.4.0.0,
                     exceptions >= 0.8.0.2,
                     distributed-process >= 0.6.1,
                     aivika >= 5.3.1,
                     aivika-transformers >= 5.3.1

    extensions:      MultiParamTypeClasses,
                     FlexibleInstances,
                     FlexibleContexts,
                     TypeFamilies,
                     RankNTypes,
                     DeriveGeneric,
                     DeriveDataTypeable,
                     OverlappingInstances,
                     MonoLocalBinds

    ghc-options:     -O2

source-repository head

    type:     git
    location: https://github.com/dsorokin/aivika-distributed
