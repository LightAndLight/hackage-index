name:                   bdcs
version:                0.2.1
synopsis:               Tools for managing a content store of software packages
description:            This module provides a library and various tools for managing a content store and
                        metadata database.  These store the contents of software packages that make up a
                        Linux distribution as well as a lot of metadata about those software packages.
                        Tools are inclued to construct those stores from pre-built software and to pull
                        files back out to turn into bootable images.
homepage:               https://github.com/weldr/bdcs
category:               Distribution
author:                 Chris Lumens
maintainer:             clumens@redhat.com
license:                LGPL
license-file:           LICENSE
build-type:             Custom
cabal-version:          >= 1.10

data-dir:               data
data-files:             sysusers-default.conf,
                        tmpfiles-default.conf,
                        tmpfiles-ostree.conf,
                        nsswitch-altfiles.conf

extra-source-files:     ChangeLog.md,
                        Dockerfile,
                        Dockerfile.build,
                        entrypoint-integration-test.sh,
                        entrypoint.sh,
                        cabal.config,
                        Makefile,
                        queries,
                        README.md
                        schema.sql,
                        src/tests/*.hs,
                        src/tests/BDCS/*.hs,
                        src/tests/BDCS/Export/*.hs,
                        src/tests/BDCS/RPM/*.hs,
                        tests/*.sh

custom-setup
    setup-depends:      base,
                        Cabal >= 1.10 && < 2.1,
                        filepath

source-repository       head
    type:               git
    location:           https://github.com/weldr/bdcs

flag scripts
    description:        Enable importing package scripts to the database
    default:            False

library
    hs-source-dirs:     src

    exposed-modules:    BDCS.Build.NPM,
                        BDCS.Builds,
                        BDCS.CS,
                        BDCS.DB,
                        BDCS.Depclose,
                        BDCS.Depsolve,
                        BDCS.Exceptions,
                        BDCS.Export,
                        BDCS.Export.Directory,
                        BDCS.Export.FSTree,
                        BDCS.Export.Qcow2,
                        BDCS.Export.Ostree,
                        BDCS.Export.Tar,
                        BDCS.Export.TmpFiles,
                        BDCS.Export.Utils,
                        BDCS.Files,
                        BDCS.GroupKeyValue,
                        BDCS.Groups,
                        BDCS.Import.Comps,
                        BDCS.Import.Conduit,
                        BDCS.Import.NPM,
                        BDCS.Import.RPM,
                        BDCS.Import.Repodata,
                        BDCS.Import.State,
                        BDCS.Import.URI,
                        BDCS.KeyType,
                        BDCS.KeyValue,
                        BDCS.Label.Docs,
                        BDCS.Label.Fonts,
                        BDCS.Label.FileLabels,
                        BDCS.Label.InfoPage,
                        BDCS.Label.Library,
                        BDCS.Label.License,
                        BDCS.Label.ManPage,
                        BDCS.Label.Service,
                        BDCS.Label.Translation,
                        BDCS.Label.Types,
                        BDCS.Label.Utils,
                        BDCS.NPM.SemVer,
                        BDCS.Packages,
                        BDCS.Projects,
                        BDCS.RPM.Builds,
                        BDCS.RPM.Files,
                        BDCS.RPM.Groups,
                        BDCS.RPM.Projects,
                        BDCS.RPM.Requirements,
                        BDCS.RPM.Scripts,
                        BDCS.RPM.Signatures,
                        BDCS.RPM.Sources,
                        BDCS.RPM.Utils,
                        BDCS.ReqType,
                        BDCS.Requirements,
                        BDCS.Scripts,
                        BDCS.Signatures,
                        BDCS.Sources,
                        BDCS.Utils.Conduit,
                        BDCS.Utils.Either,
                        BDCS.Utils.Error,
                        BDCS.Utils.Filesystem,
                        BDCS.Utils.Mode,
                        BDCS.Utils.Monad
                        BDCS.Version

    build-depends:      aeson >= 1.0.0.0 && < 1.3.0.0,
                        base >= 4.9 && < 5.0,
                        bytestring >= 0.10 && < 0.11,
                        codec-rpm >= 0.2.0 && < 0.3,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        conduit >= 1.2.8 && < 1.3,
                        conduit-combinators >= 1.1.0 && < 1.2,
                        conduit-extra >= 1.1.0 && < 1.3,
                        containers >= 0.5.7.1 && < 0.6,
                        content-store >= 0.2.0 && < 0.3.0,
                        cpio-conduit >= 0.7.0 && < 0.8.0,
                        cryptonite >= 0.22 && < 0.30,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        esqueleto >= 2.5.3 && < 2.6.0,
                        exceptions >= 0.8.0 && < 0.9.0,
                        filepath >= 1.4.1.1 && < 1.5.0.0,
                        gi-gio >= 2.0.14 && < 2.1.0,
                        gi-glib >= 2.0.14 && < 2.1.0,
                        gi-ostree >= 1.0.3 && < 1.1.0,
                        gitrev >= 1.3.1 && < 1.4.0,
                        http-conduit >= 2.2.3 && < 2.3.0,
                        listsafe >= 0.1.0.1 && < 0.2.0,
                        memory >= 0.14.3 && < 0.15.0,
                        monad-control >= 1.0.1.0 && < 1.1.0.0,
                        monad-logger >= 0.3.20.2 && < 0.4.0,
                        mtl >= 2.2.1 && < 2.3,
                        network-uri >= 2.6.0 && < 2.7.0,
                        parsec >= 3.1.10 && < 3.2.0,
                        parsec-numbers >= 0.1.0 && < 0.2.0,
                        persistent >= 2.7.0 && < 2.8.0,
                        persistent-sqlite >= 2.6.0 && < 2.7.0,
                        persistent-template >= 2.5.0 && < 2.6.0,
                        process >= 1.4.3.0 && < 2.0,
                        regex-pcre >= 0.94 && < 0.95,
                        resourcet >= 1.1.9 && < 1.2,
                        split >= 0.2.3 && < 0.3,
                        tar >= 0.5 && < 0.6,
                        tar-conduit >= 0.1.0 && < 0.2.0,
                        temporary >= 1.2.0.4 && < 1.3.0.0,
                        text >= 1.2.2.0 && < 1.3,
                        time >= 1.6.0.1 && < 2.0,
                        unix >= 2.7.2.1 && < 2.8.0.0,
                        unordered-containers >= 0.2.7.2 && < 0.2.9.0,
                        xml-conduit >= 1.4.0.4 && < 1.8.0

    pkgconfig-depends:  ostree-1 >= 2017.8

    other-modules:      Paths_bdcs
    default-language:   Haskell2010

    if flag(scripts)
        cpp-options:    -DSCRIPTS
        cc-options:     "-DSCRIPTS"
        ghc-options:    -Wall
    else
        ghc-options:    -Wall

executable bdcs
    main-is:            bdcs.hs
    hs-source-dirs:     src/tools

    other-modules:      Utils.GetOpt,
                        Utils.Subcommands,
                        Paths_bdcs

    build-depends:      bdcs,
                        base >= 4.7 && < 5.0,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        filepath >= 1.4.1.1 && < 1.5.0.0,
                        monad-loops >= 0.4.0 && < 0.5,
                        process >= 1.4.3.0 && < 2.0,
                        text >= 1.2.2.0 && < 1.3

    default-language:   Haskell2010

    ghc-options:        -Wall

executable bdcs-import
    main-is:            import.hs
    hs-source-dirs:     src/tools

    other-modules:      Utils.GetOpt

    build-depends:      bdcs,
                        base >= 4.7 && < 5.0,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        content-store >= 0.2.0 && < 0.3.0,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        mtl >= 2.2.1,
                        network-uri,
                        text >= 1.2.2.0 && < 1.3

    default-language:   Haskell2010

    ghc-options:        -Wall

executable bdcs-inspect
    main-is:            inspect.hs
    hs-source-dirs:     src/tools/inspect,
                        src/tools

    other-modules:      Utils.GetOpt,
                        Utils.IO,
                        Utils.Subcommands,
                        Paths_bdcs

    build-depends:      bdcs,
                        base >= 4.7 && < 5.0,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        filepath >= 1.4.1.1 && < 1.5.0.0,
                        monad-loops >= 0.4.0 && < 0.5,
                        process >= 1.4.3.0 && < 2.0,
                        text >= 1.2.2.0 && < 1.3

    default-language:   Haskell2010

    ghc-options:        -Wall

executable inspect-groups
    main-is:            groups.hs
    hs-source-dirs:     src/tools/inspect/subcommands,
                        src/tools/inspect,
                        src/tools

    other-modules:      Utils.Exceptions,
                        Utils.GetOpt,
                        Utils.IO,
                        Utils.KeyVal

    build-depends:      aeson >= 1.0.0.0 && < 1.3.0.0,
                        aeson-pretty,
                        base >= 4.7 && < 5.0,
                        bdcs,
                        bytestring >= 0.10 && < 0.11,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        conduit >= 1.2.8 && < 1.3,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        mtl >= 2.2.1 && < 2.3,
                        persistent-sqlite >= 2.6.0 && < 2.7.0,
                        regex-pcre >= 0.94 && < 0.95,
                        text >= 1.2.2.0 && < 1.3

    default-language:   Haskell2010
    ghc-options:        -Wall

executable inspect-ls
    main-is:            ls.hs
    hs-source-dirs:     src/tools/inspect/subcommands,
                        src/tools/inspect,
                        src/tools

    other-modules:      Utils.Exceptions,
                        Utils.GetOpt,
                        Utils.IO,
                        Utils.KeyVal

    build-depends:      aeson >= 1.0.0.0 && < 1.3.0.0,
                        aeson-pretty,
                        base >= 4.7 && < 5.0,
                        bdcs,
                        bytestring >= 0.10 && < 0.11,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        conduit >= 1.2.8 && < 1.3,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        mtl >= 2.2.1 && < 2.3,
                        regex-pcre >= 0.94 && < 0.95,
                        text >= 1.2.2.0 && < 1.3,
                        time >= 1.6.0.1 && < 2.0

    default-language:   Haskell2010
    ghc-options:        -Wall

executable inspect-nevras
    main-is:            nevras.hs
    hs-source-dirs:     src/tools/inspect/subcommands,
                        src/tools/inspect,
                        src/tools

    other-modules:      Utils.Exceptions,
                        Utils.GetOpt,
                        Utils.IO

    build-depends:      bdcs,
                        base >= 4.7 && < 5.0,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        conduit >= 1.2.8 && < 1.3,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        mtl >= 2.2.1 && < 2.3,
                        regex-pcre >= 0.94 && < 0.95,
                        text >= 1.2.2.0 && < 1.3

    default-language:   Haskell2010
    ghc-options:        -Wall

executable bdcs-export
    main-is:            export.hs
    hs-source-dirs:     src/tools

    other-modules:      Utils.GetOpt

    build-depends:      bdcs,
                        base >= 4.9 && < 5.0,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        conduit >= 1.2.8 && < 1.3,
                        content-store >= 0.2.0 && < 0.3.0,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        mtl >= 2.2.1 && < 2.3,
                        text >= 1.2.2.0 && < 1.3

    default-language:   Haskell2010

    ghc-options:        -Wall

executable bdcs-tmpfiles
    main-is:            bdcs-tmpfiles.hs
    hs-source-dirs:     src/tools

    build-depends:      bdcs,
                        base >= 4.9 && < 5.0,
                        directory >= 1.3.0.0 && < 1.4.0.0

    default-language:   Haskell2010

    ghc-options:        -Wall

executable bdcs-depsolve
    main-is:            depsolve.hs
    hs-source-dirs:     src/tools

    other-modules:      Utils.GetOpt

    build-depends:      bdcs,
                        base >= 4.9 && < 5.0,
                        mtl >= 2.2.1 && < 2.3,
                        text >= 1.2.2.0 && < 1.3

    default-language:   Haskell2010

    ghc-options:        -Wall

Test-Suite test-bdcs
    type:               exitcode-stdio-1.0
    hs-source-dirs:     src,
                        src/tests
    main-is:            Spec.hs

    build-depends:      hspec == 2.*,
                        HUnit >= 1.5.0.0 && < 1.7.0.0,
                        aeson >= 1.0.0.0 && < 1.3.0.0,
                        base >= 4.8 && < 5.0,
                        bytestring >= 0.10 && < 0.11,
                        codec-rpm >= 0.2.0 && < 0.3,
                        cond >= 0.4.1.1 && < 0.5.0.0,
                        conduit >= 1.2.8 && < 1.3,
                        conduit-combinators >= 1.1.0 && < 1.2,
                        containers >= 0.5.7.1 && < 0.6,
                        directory >= 1.3.0.0 && < 1.4.0.0,
                        esqueleto >= 2.5.3 && < 2.6.0,
                        filepath >= 1.4.1.1 && < 1.5.0.0,
                        gi-gio >= 2.0.14 && < 2.1.0,
                        gi-glib >= 2.0.14 && < 2.1.0,
                        listsafe >= 0.1.0.1 && < 0.2.0,
                        monad-logger >= 0.3.20.2 && < 0.4.0,
                        mtl >= 2.2.1 && < 2.3,
                        parsec >= 3.1.10 && < 3.2.0,
                        parsec-numbers >= 0.1.0 && < 0.2.0,
                        persistent >= 2.7.0 && < 2.8.0,
                        persistent-sqlite >= 2.6.0 && < 2.7.0,
                        persistent-template >= 2.5.0 && < 2.6.0,
                        resourcet >= 1.1.9 && < 1.2,
                        text >= 1.2.2.0 && < 1.3,
                        time >= 1.6.0.1 && < 2.0,
                        unix >= 2.7.2.1 && < 2.8.0.0

    -- the modules compiled with -DTEST are automatically pulled in from src/
    -- based on what the tests import, and that's how we want it. Don't warn
    -- about unlisted modules.
    ghc-options:        -Wall -Wno-unrecognised-warning-flags -Wno-missing-home-modules
    cpp-options:        -DTEST

    default-language:   Haskell2010
