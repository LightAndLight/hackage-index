cabal-version:  2.2
name:           interval-algebra
version:        2.1.2
synopsis:       An implementation of Allen's interval algebra for temporal logic
description:    Please see the README on GitHub at <https://github.com/novisci/interval-algebra>
category:       Algebra,Time
homepage:       https://github.com/novisci/interval-algebra#readme
bug-reports:    https://github.com/novisci/interval-algebra/issues
author:         Bradley Saul
maintainer:     bsaul@novisci.com
copyright:      2020 NoviSci
license:        BSD-3-Clause
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
  type: git
  location: https://github.com/novisci/interval-algebra

library
  exposed-modules:
      IntervalAlgebra
      IntervalAlgebra.Core
      IntervalAlgebra.IntervalUtilities
      IntervalAlgebra.IntervalDiagram
      IntervalAlgebra.PairedInterval
      IntervalAlgebra.Axioms
      IntervalAlgebra.RelationProperties
      IntervalAlgebra.Arbitrary
  other-modules:
      Paths_interval_algebra
  autogen-modules:
      Paths_interval_algebra
  hs-source-dirs:
      src
  build-depends:
      base >=4.7 && <5
    , binary ^>= 0.8
    , containers ^>= 0.6
    , deepseq >= 1.1 && < 1.5
    , foldl ^>= 1.4
    , prettyprinter ^>= 1.7
    , QuickCheck ^>= 2.14
    , safe ^>= 0.3
    , text ^>= 1.2 || ^>= 2.0
    , time >= 1.9 && < 2
    , witch ^>= 1.0
    , witherable ^>= 0.4
  default-language: Haskell2010

test-suite axioms
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      AxiomsSpec
  hs-source-dirs:
      test-axioms
  ghc-options: -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      base >=4.7 && <5
    , hspec
    , interval-algebra
    , QuickCheck
    , time
  default-language: Haskell2010

test-suite relations
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      RelationPropertiesSpec
  hs-source-dirs:
      test-relation-properties
  ghc-options: -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      base >=4.7 && <5
    , hspec
    , interval-algebra
    , QuickCheck
    , time
  default-language: Haskell2010

test-suite interval-algebra-test
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      IntervalAlgebraSpec
      IntervalAlgebra.IntervalUtilitiesSpec
      IntervalAlgebra.PairedIntervalSpec
      Paths_interval_algebra
  autogen-modules:
      Paths_interval_algebra
  hs-source-dirs:
      test
  ghc-options: -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      base >=4.7 && <5
    , containers
    , hspec
    , interval-algebra
    , QuickCheck
    , safe
    , time
    , witherable
  build-tool-depends:
      hspec-discover:hspec-discover >= 2.9.2
  default-language: Haskell2010

executable tutorial
  main-is: TutorialMain.hs
  hs-source-dirs: tutorial
  build-depends:
      base >=4.7 && <5
    , containers ^>= 0.6
    , interval-algebra
    , prettyprinter ^>= 1.7
    , time >= 1.9 && < 2
    , witch ^>= 1.0
  default-language: Haskell2010
