cabal-version: >=1.10
name: too-many-cells
version: 0.1.2.0
license: GPL-3
license-file: LICENSE
copyright: 2019 Gregory W. Schwartz
maintainer: gsch@pennmedicine.upenn.edu
author: Gregory W. Schwartz
homepage: http://github.com/GregorySchwartz/too-many-cells#readme
synopsis: Cluster single cells and analyze cell clade relationships.
description:
    Different methods to cluster and analyze single cell data with diversity indices and differential expression.
category: Bioinformatics
build-type: Simple

source-repository head
    type: git
    location: https://github.com/GregorySchwartz/too-many-cells

library
    exposed-modules:
        TooManyCells.Diversity.Diversity
        TooManyCells.Differential.Differential
        TooManyCells.Differential.Types
        TooManyCells.Diversity.Load
        TooManyCells.Diversity.Plot
        TooManyCells.Diversity.Types
        TooManyCells.File.Types
        TooManyCells.MakeTree.Adjacency
        TooManyCells.MakeTree.Clumpiness
        TooManyCells.MakeTree.Cluster
        TooManyCells.MakeTree.Load
        TooManyCells.MakeTree.Plot
        TooManyCells.MakeTree.Print
        TooManyCells.MakeTree.Types
        TooManyCells.Matrix.Load
        TooManyCells.Matrix.Preprocess
        TooManyCells.Matrix.Types
        TooManyCells.Matrix.Utility
        TooManyCells.Paths.Distance
        TooManyCells.Paths.Plot
        TooManyCells.Paths.Types
    hs-source-dirs: src
    default-language: Haskell2010
    ghc-options: -O2
    build-depends:
        base >=4.7 && <5,
        aeson >=1.4.0.0,
        birch-beer >=0.1.0.0,
        bytestring >=0.10.8.2,
        cassava >=0.5.1.0,
        colour >=2.3.4,
        containers >=0.5.11.0,
        deepseq >=1.4.3.0,
        diagrams >=1.4,
        diagrams-lib >=1.4.2.3,
        diagrams-cairo >=1.4.1,
        diagrams-graphviz >=1.4.1,
        differential >=0.1.1.0,
        directory >=1.3.1.5,
        diversity >=0.8.1.0,
        find-clumpiness >=0.2.3.1,
        filepath >=1.4.2,
        fgl >=5.6.0.0,
        foldl >=1.4.2,
        graphviz >=2999.20.0.2,
        hierarchical-clustering >=0.4.6,
        hierarchical-spectral-clustering >=0.2.2.0,
        hmatrix >=0.19.0.0,
        inline-r >=0.9.2,
        lens >=4.16.1,
        managed >=1.0.6,
        matrix-market-attoparsec >=0.1.0.8,
        mltool >=0.2.0.1,
        modularity >=0.2.1.0,
        mtl >=2.2.2,
        palette >=0.3.0.1,
        parallel >=3.2.1.1,
        plots >=0.1.0.2,
        safe >=0.3.17,
        scientific >=0.3.6.2,
        sparse-linear-algebra >=0.3.1,
        split >=0.2.3.3,
        statistics >=0.14.0.2,
        streaming >=0.2.1.0,
        streaming-bytestring >=0.1.6,
        streaming-cassava >=0.1.0.1,
        streaming-with >=0.2.2.1,
        SVGFonts >=1.6.0.3,
        text >=1.2.3.0,
        text-show >=3.7.4,
        vector >=0.12.0.1,
        vector-algorithms >=0.7.0.1

executable too-many-cells
    main-is: Main.hs
    hs-source-dirs: app
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -O2
    build-depends:
        base >=4.11.1.0,
        too-many-cells -any,
        aeson >=1.4.0.0,
        birch-beer >=0.1.0.0,
        bytestring >=0.10.8.2,
        cassava >=0.5.1.0,
        colour >=2.3.4,
        containers >=0.5.11.0,
        diagrams-lib >=1.4.2.3,
        diagrams-cairo >=1.4.1,
        directory >=1.3.1.5,
        fgl >=5.6.0.0,
        filepath >=1.4.2,
        find-clumpiness >=0.2.3.1,
        graphviz >=2999.20.0.2,
        hierarchical-spectral-clustering >=0.2.2.0,
        inline-r >=0.9.2,
        lens >=4.16.1,
        matrix-market-attoparsec >=0.1.0.8,
        mtl >=2.2.2,
        optparse-generic >=1.3.0,
        palette >=0.3.0.1,
        plots >=0.1.0.2,
        spectral-clustering >=0.2.2.3,
        streaming >=0.2.1.0,
        streaming-bytestring >=0.1.6,
        streaming-utils >=0.1.4.7,
        terminal-progress-bar >=0.2,
        text >=1.2.3.0,
        text-show >=3.7.4,
        transformers >=0.5.5.0,
        vector >=0.12.0.1
