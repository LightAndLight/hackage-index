Cabal-Version: 2.2
Name:          llvm-ffi
Version:       11.0
License:       BSD-3-Clause
License-File:  LICENSE
Synopsis:      FFI bindings to the LLVM compiler toolkit.
Description:
  FFI bindings to the LLVM compiler toolkit.
  .
  Installation cannot be done fully automatically.
  It would require Cabal code that is bound to certain Cabal versions
  and is prone to fail.
  We give several non-automatic ways
  that also allow you to choose a particular LLVM version.
  .
  First possibility is to point Cabal
  to the LLVM installation directories manually.
  It is recommended to add options to your global @.cabal/config@:
  .
  > extra-include-dirs: /usr/lib/llvm-11/include
  > extra-lib-dirs: /usr/lib/llvm-11/lib
  .
  This works for both @v1-build@ and @v2-build@.
  The shown paths work for Debian and Ubuntu
  using the LLVM repositories at <https://apt.llvm.org/>.
  You can obtain them with
  .
  > llvm-config-11 --includedir --libdir
  .
  You can choose specific LLVM versions per project.
  For @v1-builds@ it works like so:
  .
  > cabal install -fllvm900 --extra-include-dirs=$(llvm-config-9 --includedir) --extra-lib-dirs=$(llvm-config-9 --libdir) yourpackage
  .
  For Nix-style build you must add some options
  to the @cabal.project.local@ file of your LLVM-related project:
  .
  > package llvm-ffi
  >   flags: +llvm900
  >   extra-include-dirs: /usr/lib/llvm-9/include
  >   extra-lib-dirs: /usr/lib/llvm-9/lib
  .
  The second way uses @pkg-config@.
  You can store above paths permanently in a @pkg-config@ file like @llvm.pc@.
  The optimal way would be if LLVM installations or GNU/Linux distributions
  would contain such a file, but they don't.
  Instead, you may generate it using the @llvm-pkg-config@ package
  or write one manually.
  Then you run
  .
  > cabal install -fpkgConfig
  .
  We try to stay up to date with LLVM releases.
  The current version of this package is compatible with LLVM 3.9-11.
  Please understand that the package may or may not work
  against older LLVM releases.
  .
  Warning for inplace builds:
  Re-configuring the package using, say @-fllvm600@,
  and re-buildung it might result in corrupt code.
  You must make sure that the stuff in @cbits@ is re-compiled.
  Cabal or GHC may forget about that.
  You are safe if you run @cabal clean@.
  .
  Caution: Ugly crashes can occur
  if you have configured paths for LLVM version X in @.cabal/config@
  and try to build @llvm-ffi@ for a different LLVM version Y.
  Counterintuitively, global search paths have higher precedence
  than local ones: <https://github.com/haskell/cabal/issues/7782>.
  But that does not simply mean
  that the local configuration is ignored completely.
  Instead the local library file is found,
  because its name libLLVM-Y.so is unique,
  whereas the include file names clash,
  thus the ones from the global include directory are used.
Author:        Henning Thielemann, Bryan O'Sullivan, Lennart Augustsson
Maintainer:    Henning Thielemann <llvm@henning-thielemann.de>
Homepage:      http://haskell.org/haskellwiki/LLVM
Stability:     experimental
Category:      Compilers/Interpreters, Code Generation
Tested-With:   GHC==7.4.2, GHC==7.6.3, GHC==7.8.4
Tested-With:   GHC==8.0.2, GHC==8.2.2, GHC==8.4.4
Tested-With:   GHC==8.6.5, GHC==8.8.4, GHC==8.10.4
Build-Type:    Simple

Extra-Source-Files:
  include/support.h
  tool/ltrace.config
  tool/ltrace.readme
  flatpak/llvm-3.8.1.json
  flatpak/llvm-3.9.1.json

Flag developer
  Description: developer mode - warnings let compilation fail
  Manual: True
  Default: False

Flag buildExamples
  Description: Build example executables
  Manual: True
  Default: False

Flag pkgConfig
  Description: use pkgconfig (llvm.pc) instead of extra-libraries
  Default: False

Flag specificPkgConfig
  Description: use llvm-x.pc instead of llvm.pc
  Default: True

Flag llvm309
  Description: use LLVM-3.9 instead of latest supported LLVM
  Default: False

Flag llvm400
  Description: use LLVM-4.0 instead of latest supported LLVM
  Default: False

Flag llvm500
  Description: use LLVM-5.0 instead of latest supported LLVM
  Default: False

Flag llvm600
  Description: use LLVM-6.0 instead of latest supported LLVM
  Default: False

Flag llvm700
  Description: use LLVM-7 instead of latest supported LLVM
  Default: False

Flag llvm800
  Description: use LLVM-8 instead of latest supported LLVM
  Default: False

Flag llvm900
  Description: use LLVM-9 instead of latest supported LLVM
  Default: False

Flag llvm1000
  Description: use LLVM-10 instead of latest supported LLVM
  Default: False

Source-Repository head
  Type:     darcs
  Location: https://hub.darcs.net/thielema/llvm-ffi/

Source-Repository this
  Tag:      11.0
  Type:     darcs
  Location: https://hub.darcs.net/thielema/llvm-ffi/

Library
  Build-Depends:
    enumset >=0.0.5 && <0.2,
    base >= 3 && < 5

  Hs-Source-Dirs: src
  GHC-Options: -Wall -fwarn-missing-import-lists
  Default-Language: Haskell2010

  If flag(developer)
    GHC-Options: -Werror

  Exposed-Modules:
      LLVM.FFI.Analysis
      LLVM.FFI.BitReader
      LLVM.FFI.BitWriter
      LLVM.FFI.Core
      LLVM.FFI.Core.Attribute
      LLVM.FFI.ExecutionEngine
      LLVM.FFI.Support.Host
      LLVM.FFI.Target
      LLVM.FFI.Transforms.IPO
      LLVM.FFI.Transforms.PassManagerBuilder
      LLVM.FFI.Transforms.Scalar
      LLVM.FFI.Transforms.Vectorize
      LLVM.Target.Native

  Other-modules:
      LLVM.FFI.Base
      LLVM.FFI.Version
      LLVM.Target.ARM
      LLVM.Target.Hexagon
      LLVM.Target.MSP430
      LLVM.Target.Mips
      LLVM.Target.NVPTX
      LLVM.Target.PowerPC
      LLVM.Target.Sparc
      LLVM.Target.X86
      LLVM.Target.XCore
      LLVM.Raw.Core

  If flag(llvm309)
    If flag(pkgConfig)
      If flag(specificPkgConfig)
        PkgConfig-Depends: llvm-3.9
      Else
        PkgConfig-Depends: llvm == 3.9.*
    Else
      Extra-Libraries: LLVM-3.9
    Hs-Source-Dirs: src/3.9
    CC-Options: -DHS_LLVM_VERSION=309
    Cxx-Options: -DHS_LLVM_VERSION=309
    CPP-Options: -DHS_LLVM_VERSION=309
  Else
    If flag(llvm400)
      If flag(pkgConfig)
        If flag(specificPkgConfig)
          PkgConfig-Depends: llvm-4.0
        Else
          PkgConfig-Depends: llvm == 4.0.*
      Else
        Extra-Libraries: LLVM-4.0
      Hs-Source-Dirs: src/4.0
      CC-Options: -DHS_LLVM_VERSION=400
      Cxx-Options: -DHS_LLVM_VERSION=400
      CPP-Options: -DHS_LLVM_VERSION=400
    Else
      If flag(llvm500)
        If flag(pkgConfig)
          If flag(specificPkgConfig)
            PkgConfig-Depends: llvm-5.0
          Else
            PkgConfig-Depends: llvm == 5.0.*
        Else
          Extra-Libraries: LLVM-5.0
        Hs-Source-Dirs: src/5.0
        CC-Options: -DHS_LLVM_VERSION=500
        Cxx-Options: -DHS_LLVM_VERSION=500
        CPP-Options: -DHS_LLVM_VERSION=500
      Else
        If flag(llvm600)
          If flag(pkgConfig)
            If flag(specificPkgConfig)
              PkgConfig-Depends: llvm-6.0
            Else
              PkgConfig-Depends: llvm == 6.0.*
          Else
            Extra-Libraries: LLVM-6.0
          Hs-Source-Dirs: src/6.0
          CC-Options: -DHS_LLVM_VERSION=600
          Cxx-Options: -DHS_LLVM_VERSION=600
          CPP-Options: -DHS_LLVM_VERSION=600
        Else
          If flag(llvm700)
            If flag(pkgConfig)
              If flag(specificPkgConfig)
                PkgConfig-Depends: llvm-7
              Else
                PkgConfig-Depends: llvm == 7.*
            Else
              Extra-Libraries: LLVM-7
            Hs-Source-Dirs: src/7
            CC-Options: -DHS_LLVM_VERSION=700
            Cxx-Options: -DHS_LLVM_VERSION=700
            CPP-Options: -DHS_LLVM_VERSION=700
          Else
            If flag(llvm800)
              If flag(pkgConfig)
                If flag(specificPkgConfig)
                  PkgConfig-Depends: llvm-8
                Else
                  PkgConfig-Depends: llvm == 8.*
              Else
                Extra-Libraries: LLVM-8
              Hs-Source-Dirs: src/8
              CC-Options: -DHS_LLVM_VERSION=800
              Cxx-Options: -DHS_LLVM_VERSION=800
              CPP-Options: -DHS_LLVM_VERSION=800
            Else
              If flag(llvm900)
                If flag(pkgConfig)
                  If flag(specificPkgConfig)
                    PkgConfig-Depends: llvm-9
                  Else
                    PkgConfig-Depends: llvm == 9.*
                Else
                  Extra-Libraries: LLVM-9
                Hs-Source-Dirs: src/9
                CC-Options: -DHS_LLVM_VERSION=900
                Cxx-Options: -DHS_LLVM_VERSION=900
                CPP-Options: -DHS_LLVM_VERSION=900
              Else
                Cxx-Options: -std=c++14
                If flag(llvm1000)
                  If flag(pkgConfig)
                    If flag(specificPkgConfig)
                      PkgConfig-Depends: llvm-10
                    Else
                      PkgConfig-Depends: llvm == 10.*
                  Else
                    Extra-Libraries: LLVM-10
                  Hs-Source-Dirs: src/10
                  CC-Options: -DHS_LLVM_VERSION=1000
                  Cxx-Options: -DHS_LLVM_VERSION=1000
                  CPP-Options: -DHS_LLVM_VERSION=1000
                Else
                  If flag(pkgConfig)
                    If flag(specificPkgConfig)
                      PkgConfig-Depends: llvm-11
                    Else
                      PkgConfig-Depends: llvm == 11.*
                  Else
                    Extra-Libraries: LLVM-11
                  Hs-Source-Dirs: src/11
                  CC-Options: -DHS_LLVM_VERSION=1100
                  Cxx-Options: -DHS_LLVM_VERSION=1100
                  CPP-Options: -DHS_LLVM_VERSION=1100

  CC-Options: -DHAVE_LLVM_SUPPORT_DYNAMICLIBRARY_H=1
  CPP-Options: -D__STDC_CONSTANT_MACROS -D__STDC_FORMAT_MACROS
  Include-Dirs: include
  Extra-Libraries: stdc++
  Cxx-Options: -std=c++0x -std=c++11
  Cxx-Sources:
    cbits/support.cpp

Executable llvm-ffi-host
  If flag(buildExamples)
    Build-Depends:
      llvm-ffi,
      utility-ht >=0.0.9 && <0.1,
      base
  Else
    Buildable: False

  If flag(developer)
    GHC-Options: -Werror

  Hs-Source-Dirs: example
  GHC-Options: -Wall
  Default-Language: Haskell2010
  Main-Is: Host.hs
  Other-Modules: Common

Executable llvm-ffi-jit
  If flag(buildExamples)
    Build-Depends:
      llvm-ffi,
      utility-ht >=0.0.9 && <0.1,
      base
  Else
    Buildable: False

  If flag(developer)
    GHC-Options: -Werror

  Hs-Source-Dirs: example
  GHC-Options: -Wall
  Default-Language: Haskell2010
  Main-Is: JIT.hs
  Other-Modules: Common

Executable llvm-ffi-offset
  If flag(buildExamples)
    Build-Depends:
      llvm-ffi,
      utility-ht >=0.0.9 && <0.1,
      base
  Else
    Buildable: False

  If flag(developer)
    GHC-Options: -Werror

  Hs-Source-Dirs: example
  GHC-Options: -Wall
  Default-Language: Haskell2010
  Main-Is: Offset.hs
  Other-Modules: Common
