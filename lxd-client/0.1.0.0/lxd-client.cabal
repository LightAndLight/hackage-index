name:                lxd-client
version:             0.1.0.0
synopsis:            LXD client written in Haskell.
description:
    Implementation of the LXD client protocol in Haskell.
    .
    This module implements the LXD client protocol in Haskell using servant and
    websockets. It allows you to manage LXD containers and other resources
    directly from Haskell.
    .
    More information and a tutorial is in "Network.LXD.Client.Commands".
    .
    Accompanying blog post:
    <https://deliquus.com/posts/2017-10-02-using-servant-to-orchestrate-lxd-containers.md>
homepage:            https://github.com/hverr/haskell-lxd-client#readme
license:             GPL-3
license-file:        LICENSE
author:              Henri Verroken
maintainer:          henriverroken@gmail.com
copyright:           2017 Henri Verroken
category:            System
build-type:          Simple
cabal-version:       >=1.10

library
  hs-source-dirs:      src
  exposed-modules:     Network.LXD.Client
                     , Network.LXD.Client.API
                     , Network.LXD.Client.Commands
                     , Network.LXD.Client.Events
                     , Network.LXD.Client.Internal.Prelude
                     , Network.LXD.Client.Remotes
                     , Network.LXD.Client.Types
  build-depends:       base >= 4.7 && < 5
                     , aeson
                     , async
                     , bimap
                     , bytestring
                     , connection
                     , containers
                     , data-default
                     , directory
                     , either
                     , exceptions
                     , filepath
                     , http-api-data
                     , http-client
                     , http-client-tls
                     , http-media
                     , http-types
                     , mtl
                     , network
                     , semigroups
                     , servant
                     , servant-client
                     , text
                     , tls
                     , transformers
                     , unix
                     , websockets
                     , x509
                     , x509-store
                     , x509-validation
  ghc-options:         -Wall
  default-extensions:  NoImplicitPrelude OverloadedStrings
  default-language:    Haskell2010

executable lxd-client-example
  hs-source-dirs:      example
  main-is:             Main.hs
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N -Wall
  build-depends:       base
                     , lxd-client
  default-language:    Haskell2010

test-suite lxd-client-integration
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Integration.hs
  other-modules:
  build-depends:       base
                     , lxd-client
                     , exceptions
                     , hspec
                     , hspec-core
                     , random
                     , text
                     , turtle
                     , uuid
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N -Wall
  default-extensions:  NoImplicitPrelude OverloadedStrings
  default-language:    Haskell2010

source-repository head
  type:     git
  location: https://github.com/hverr/haskell-lxd-client
