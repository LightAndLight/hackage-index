cabal-version:      3.0

-- Initial package description 'FiniteCategories.cabal' generated by
-- 'cabal init'. For further documentation, see:
--   http://haskell.org/cabal/users-guide/
-- 
-- The name of the package.
name:               FiniteCategories

-- The package version.
-- See the Haskell package versioning policy (PVP) for standards
-- guiding when and how versions should be incremented.
-- https://pvp.haskell.org
-- PVP summary:      +-+------- breaking API changes
--                   | | +----- non-breaking API additions
--                   | | | +--- code changes with no API change
version:            0.6.0.2

-- A short (one-line) description of the package.
synopsis: Finite categories and usual categorical constructions on them.
    

-- A longer description of the package.
description: This package provides tools to create categories at the value level. This is different from the __Hask__ category where types are objects in a category with an infinite collection of objects and functions, here we construct categories where objects and arrows are arbitrary values so that we can change categories during runtime. Each category implements two functions following the category structure axioms : @ar@ which returns arrows between two objects of the category and @identity@ which returns the identity of an object. A FiniteCategory implements an additional function : @ob@ which returns objects of the category. Thanks to these functions, we can construct automatically all the usual constructions on the categories (limits and colimits, adjunctions, Yoneda embedding, etc.) Functors are different from usual @Functor@ typeclass, we store functors as mapping between objects and morphisms of two categories. This package is also different from the package @data-category@ because we can enumerate objects and arrows in a finite category.  This allows us to construct limit, colimits, adjunctions, etc. automatically for arbitrary finite categories. On the other hand, we loose typecheck at compilation time which ensures that composition is sound in __Hask__, composition in our package might lead to an error raised during runtime. See the Readme file for installation help. See the package FiniteCategoriesGraphViz to visualize categories with graphviz.

-- URL for the project homepage or repository.
homepage: https://gitlab.utc.fr/gsabbagh/FiniteCategories

-- A URL where users can report bugs.
-- bug-reports: 

-- The license under which the package is released.
license:            GPL-3.0-or-later

-- The file containing the license text.
license-file:       LICENSE

-- The package author(s).
author:             Guillaume Sabbagh

-- An email address to which users can send suggestions, bug reports, and patches.
maintainer:         guillaumesabbagh@protonmail.com

-- A copyright notice.
-- copyright:
category:           Maths, Data

-- Extra files to be distributed with the package, such as examples or a README.
extra-source-files:
    CHANGELOG.md
    Readme.md

library
    -- Modules exported by the library.
    exposed-modules:    Math.FiniteCategories.All,
                        Math.Category,
                        Math.FiniteCategory,
                        Math.CompleteCategory,
                        Math.CocompleteCategory,
                        Math.CartesianClosedCategory,
                        Math.FiniteCategoryError,
                        Math.Categories,
                        Math.Categories.TotalOrder,
                        Math.Categories.OrdinalCategory,
                        Math.Categories.Omega,
                        Math.Categories.Galaxy,
                        Math.Categories.FinSet,
                        Math.Categories.FinGrph,
                        Math.Categories.Opposite,
                        Math.Categories.FinCat,
                        Math.Categories.FunctorCategory,
                        Math.Categories.CommaCategory,
                        Math.Categories.ConeCategory,
                        Math.Categories.PresheafCategory,
                        Math.Categories.FinSketch,
                        Math.FiniteCategories,
                        Math.FiniteCategories.FullSubcategory,
                        Math.FiniteCategories.NumberCategory,
                        Math.FiniteCategories.DiscreteCategory,
                        Math.FiniteCategories.Hat,
                        Math.FiniteCategories.V,
                        Math.FiniteCategories.Parallel,
                        Math.FiniteCategories.Square,
                        Math.FiniteCategories.Ens,
                        Math.FiniteCategories.Opposite,
                        Math.FiniteCategories.FunctorCategory,
                        Math.FiniteCategories.CompositionGraph,
                        Math.FiniteCategories.SafeCompositionGraph,
                        Math.FiniteCategories.CommaCategory,
                        Math.FiniteCategories.One,
                        Math.FiniteCategories.ConeCategory,
                        Math.FiniteCategories.Subcategory,
                        Math.FiniteCategories.DiscreteTwo,
                        Math.FiniteCategories.LimitCategory,
                        Math.FiniteCategories.ColimitCategory,
                        Math.FiniteCategories.ExponentialCategory,
                        Math.FiniteCategories.Examples,
                        Math.FiniteCategories.CommaCategory.Examples,
                        Math.FiniteCategories.CompositionGraph.Examples,
                        Math.FiniteCategories.ConeCategory.Examples,
                        Math.FiniteCategories.DiscreteCategory.Examples,
                        Math.FiniteCategories.Ens.Examples,
                        Math.FiniteCategories.FinCat.Examples,
                        Math.FiniteCategories.FinGrph.Examples,
                        Math.FiniteCategories.FinSketch.Examples,
                        Math.FiniteCategories.FunctorCategory.Examples,
                        Math.FiniteCategories.LimitCategory.Examples,
                        Math.FiniteCategories.NumberCategory.Examples,
                        Math.FiniteCategories.Opposite.Examples,
                        Math.FiniteCategories.SafeCompositionGraph.Examples,
                        Math.Functors,
                        Math.Functors.Adjunction,
                        Math.Functors.DiagonalFunctor,
                        Math.Functors.DataMigration,
                        Math.Functors.KanExtension,
                        Math.Functors.SetValued,
                        Math.Functors.Examples,
                        Math.Functors.Adjunction.Examples,
                        Math.Functors.DataMigration.Examples,
                        Math.Functors.DiagonalFunctor.Examples,
                        Math.Functors.KanExtension.Examples,
                        Math.Functors.SetValued.Examples,
                        Math.Functors.YonedaEmbedding.Examples,
                        Math.IO.PrettyPrint

    -- Modules included in this library but not exported.
    -- other-modules:  

    -- LANGUAGE extensions used by modules in this package.
    other-extensions: MultiParamTypeClasses, FunctionalDependencies, UndecidableInstances, FlexibleInstances, MonadComprehensions, DeriveGeneric, DeriveAnyClass, DefaultSignatures, TypeOperators, FlexibleContexts

    -- Other library packages from which modules are imported.
    build-depends:      base             >= 4 && < 5,
                        random           >= 1.2.1 && < 1.3,
                        WeakSets         >= 1.6.1.0 && < 1.7,
                        containers       >= 0.6.6.0 && < 0.7,
                        text             >= 1.0 && < 2.2,
                        filepath         >= 1.4.100.1 && < 1.5,
                        directory        >= 1.3.8.1 && < 1.4,
                        
    -- Directories containing source files.
    hs-source-dirs:   src

    -- Base language which the package is written in.
    default-language: Haskell2010

test-suite FiniteCategories-test
    -- Base language which the package is written in.
    default-language: Haskell2010

    -- The interface type and version of the test suite.
    type:             exitcode-stdio-1.0

    -- Directories containing source files.
    hs-source-dirs:   test

    -- The entrypoint to the test suite.
    main-is:          RunAllExamples.hs
                      

    -- Test dependencies.
    build-depends:      FiniteCategories,
                        base             >= 4 && < 5,
                        random           >= 1.2.1 && < 2,
                        WeakSets         >= 1.6.1 && < 1.7,
                        containers       >= 0.6.6.0 && < 0.7,
                        text             >= 1.0 && < 2.2,
                        filepath         >= 1.4.100.1 && < 1.5,
                        directory        >= 1.3.8.1 && < 1.4,
    other-modules:  CheckAllFiniteCategories,
                    FiniteCategoryExamples.CommaCategory,
                    FiniteCategoryExamples.CompositionGraph,
                    FiniteCategoryExamples.ConeCategory,
                    FiniteCategoryExamples.DiscreteCategory,
                    FiniteCategoryExamples.Ens,
                    FiniteCategoryExamples.FinCat,
                    FiniteCategoryExamples.FinGrph,
                    FiniteCategoryExamples.FunctorCategory,
                    FiniteCategoryExamples.LimitCategory,
                    FiniteCategoryExamples.NumberCategory,
                    FiniteCategoryExamples.Opposite,
                    FiniteCategoryExamples.SafeCompositionGraph,
                    FunctorExamples.Adjunction,
                    FunctorExamples.DataMigration,
                    FunctorExamples.DiagonalFunctor,
                    FunctorExamples.KanExtension,
                    FunctorExamples.SetValued,
                    FunctorExamples.YonedaEmbedding,