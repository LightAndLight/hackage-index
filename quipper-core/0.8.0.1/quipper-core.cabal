name:                quipper-core
version:             0.8.0.1
synopsis:            An embedded, scalable functional programming language for quantum computing.
description:
        Quipper is an embedded, scalable functional programming language for quantum computing. It provides, among other things:
        .
        * A high-level circuit description language. This includes gate-by-gate descriptions of circuit fragments, as well as powerful operators for assembling and manipulating circuits.
        .
        * A monadic semantics, allowing for a mixture of procedural and declarative programming styles.
        .
        * Built-in facilities for automatic synthesis of reversible quantum circuits, including from classical code.
        .
        * Support for hierarchical circuits.
        .
        * Extensible quantum data types.
        .
        * Programmable circuit transformers.
        .
        * Support for three execution phases: compile time, circuit generation time, and circuit execution time. A dynamic lifting operation to allow circuit generation to be parametric on values generated at circuit execution time.
        .
        * Extensive libraries of quantum functions, including: libraries for quantum integer and fixed-point arithmetic; the Quantum Fourier transform; an efficient Qram implementation; libraries for simulation of pseudo-classical circuits, Stabilizer circuits, and arbitrary circuits; libraries for exact and approximate decomposition of circuits into specific gate sets.
        .
        This package contains all of quipper, except for the rendering part, which has been separated into the quipper-rendering package to reduce dependencies.
homepage:            http://www.mathstat.dal.ca/~selinger/quipper/
license:             BSD3
license-file:        COPYRIGHT
author:              Applied Communication Sciences
maintainer:          leonardo.taglialegne@gmail.com
copyright:           Copyright (C) 2012-2013 Applied Communication Sciences.
-- category:            
build-type:          Simple
extra-source-files:  README
cabal-version:       >=1.10

library
  exposed-modules:     Quipper, Quipper.Generic, Quipper.CircLifting, Quipper.Classical, Quipper.QData, Quipper.Monad, Quipper.Control, Quipper.Circuit, Quipper.QClasses, Quipper.Labels, Quipper.Transformer
  other-modules:       Libraries.Template, Libraries.RandomSource, Libraries.CommandLine, Libraries.Auxiliary, Libraries.Typeable, Libraries.Sampling, Libraries.Tuple, Libraries.Template.LiftQ, Libraries.Template.Auxiliary, Libraries.Template.ErrorMsgQ, Libraries.Template.Lifting
  other-extensions:    GADTs, RankNTypes, FlexibleInstances, OverlappingInstances, MultiParamTypeClasses, FunctionalDependencies, UndecidableInstances, CPP, StandaloneDeriving, DeriveDataTypeable, ScopedTypeVariables, TypeSynonymInstances, TemplateHaskell, BangPatterns, FlexibleContexts, TypeFamilies, Rank2Types, ExistentialQuantification
  build-depends:       base >=4.6 && <4.10, random >=1.0 && <1.2, containers >=0.5 && <0.6, template-haskell >=2.8 && <2.12, mtl >=2.1 && <2.3, primes >=0.2 && <0.3
  hs-source-dirs:      src
  default-language:    Haskell2010
