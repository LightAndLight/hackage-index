name:                unicode-transforms
version:             0.1.0.1
x-revision: 1
synopsis:            Unicode transforms (normalization NFC/NFD/NFKC/NFKD)
description:
  This is a lightweight library supporting a limited set of unicode
  transformations (only normalizations as of now) on ByteStrings (UTF-8) and
  Text without requiring any other system libraries. It is based on
  the utf8proc C utility supporting unicode versions upto 5.1.0.
  .
  text-icu is a full featured alternative for all unicode operations but with
  a dependency on the system installed icu libraries. This package aims to
  provide an API similar to text-icu.
  .
  For more details see the README.md file.

homepage:            http://github.com/harendra-kumar/unicode-transforms
license:             BSD3
license-file:        LICENSE
author:              Harendra Kumar
maintainer:          harendra.kumar@gmail.com
copyright:           2016 Harendra Kumar
category:            Data,Text,Unicode
stability:           Experimental

build-type:          Simple
cabal-version:       >=1.10

extra-source-files:
  NOTES.md
  README.md
  benchmark/data/*.txt
  cbits/*.h
  cbits/XQillaExport.hpp
  stack.yaml
  stack-7.8.yaml
  stack-7.10.yaml
  stack-8.0.yaml
  unicode-data/README.md
  unicode-data/ucd/NormalizationTest.txt

flag bench-icu
  description: Use text-icu for benchmark comparison
  manual: True
  default: False

library
  default-language:    Haskell98
  ghc-options:         -Wall
                       -fwarn-identities
                       -fwarn-incomplete-record-updates
                       -fwarn-incomplete-uni-patterns
                       -fwarn-tabs

  cc-options:          -Wall

  exposed-modules:     Data.ByteString.UTF8.Normalize
                       Data.Text.Normalize
                       Data.Unicode.Types
  other-modules:
                       Data.Unicode.UTF8Proc

  build-depends:        base >= 4.7 && < 5
                      , bytestring
                      , text < 1.3

  include-dirs:        cbits
  c-sources:           cbits/utf8proc.c

test-suite test
  default-language:    Haskell98
  ghc-options:         -Wall
                       -fwarn-identities
                       -fwarn-incomplete-record-updates
                       -fwarn-incomplete-uni-patterns
                       -fwarn-tabs
  type:                exitcode-stdio-1.0
  ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N
  hs-source-dirs:      test
  main-is:             NormalizationTest.hs
  build-depends:         base >= 4.7 && < 5
                       , split
                       , text
                       , unicode-transforms

Benchmark bench
  default-language:    Haskell98
  ghc-options:         -Wall
                       -fwarn-identities
                       -fwarn-incomplete-record-updates
                       -fwarn-incomplete-uni-patterns
                       -fwarn-tabs
  type:                exitcode-stdio-1.0
  ghc-options:         -Wall -threaded -rtsopts -with-rtsopts=-N
  hs-source-dirs:      benchmark
  main-is:             Benchmark.hs
  build-depends:         base >= 4.7 && < 5
                       , criterion >= 0.4.0
                       , deepseq >= 1.1.0.0
                       , filepath
                       , path
                       , path-io
                       , text
                       , unicode-transforms
  if flag(bench-icu)
    cpp-options: -DBENCH_ICU
    build-depends:
        text-icu >= 0.6.2.1

source-repository head
  type:     git
  location: https://github.com/harendra-kumar/unicode-transforms
