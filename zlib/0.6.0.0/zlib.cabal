name:            zlib
version:         0.6.0.0
x-revision: 2
copyright:       (c) 2006-2014 Duncan Coutts
license:         BSD3
license-file:    LICENSE
author:          Duncan Coutts <duncan@community.haskell.org>
maintainer:      Duncan Coutts <duncan@community.haskell.org>
category:        Codec
synopsis:        Compression and decompression in the gzip and zlib formats
description:     This package provides a pure interface for compressing and 
                 decompressing streams of data represented as lazy 
                 'ByteString's. It uses the zlib C library so it has high
                 performance. It supports the \"zlib\", \"gzip\" and \"raw\"
                 compression formats.
                 .
                 It provides a convenient high level API suitable for most
                 tasks and for the few cases where more control is needed it
                 provides access to the full zlib feature set.
build-type:      Simple
cabal-version:   >= 1.10
extra-source-files: cbits/crc32.h cbits/inffast.h cbits/inflate.h
                    cbits/trees.h cbits/deflate.h cbits/inffixed.h
                    cbits/inftrees.h cbits/zutil.h
                    -- demo programs:
                    examples/gzip.hs examples/gunzip.hs

source-repository head
  type: darcs
  location: http://code.haskell.org/zlib/

library
  exposed-modules: Codec.Compression.GZip,
                   Codec.Compression.Zlib,
                   Codec.Compression.Zlib.Raw,
                   Codec.Compression.Zlib.Internal
  other-modules:   Codec.Compression.Zlib.Stream
  if impl(ghc < 7)
    default-language: Haskell98
  else
    default-language: Haskell2010
  other-extensions: CPP, ForeignFunctionInterface, RankNTypes, BangPatterns,
                    DeriveDataTypeable, DeriveGeneric
  build-depends:   base >= 4.6 && < 4.13,
                   bytestring >= 0.9 && < 0.11
  includes:        zlib.h
  ghc-options:     -Wall -fwarn-tabs
  if !os(windows)
    -- Normally we use the the standard system zlib:
    extra-libraries: z
  else
    -- However for the benefit of users of Windows (which does not have zlib
    -- by default) we bundle a complete copy of the C sources of zlib-1.2.8
    c-sources:     cbits/adler32.c cbits/compress.c cbits/crc32.c
                   cbits/deflate.c cbits/infback.c
                   cbits/inffast.c cbits/inflate.c cbits/inftrees.c
                   cbits/trees.c cbits/uncompr.c cbits/zutil.c
    include-dirs:  cbits
    install-includes: zlib.h zconf.h

test-suite tests
  type: exitcode-stdio-1.0
  main-is:         Test.hs
  other-modules:   Utils,
                   Test.Codec.Compression.Zlib.Internal,
                   Test.Codec.Compression.Zlib.Stream
  hs-source-dirs:  test
  default-language: Haskell2010
  build-depends:   base, bytestring, zlib,
                   QuickCheck       == 2.*,
                   HUnit            == 1.2.*,
                   tasty            == 0.8.*,
                   tasty-quickcheck == 0.8.*,
                   tasty-hunit      == 0.8.*
  ghc-options:     -Wall
