name: mediabus
version: 0.3.2.1
cabal-version: >=1.10
build-type: Simple
license: BSD3
license-file: LICENSE
copyright: 2016,2017 Sven Heyll, Lindenbaum GmbH
maintainer: sven.heyll@lindenbaum.eu
homepage: https://github.com/lindenbaum/mediabus
synopsis: Multimedia streaming on top of Conduit
description:
    Please see README.md
category: Media
author: Sven Heyll

source-repository head
    type: git
    location: https://github.com/lindenbaum/mediabus

library
    exposed-modules:
        Data.MediaBus
        Data.MediaBus.Basics.Clock
        Data.MediaBus.Basics.LoggingExtra
        Data.MediaBus.Basics.Monotone
        Data.MediaBus.Basics.OrderedBy
        Data.MediaBus.Basics.Sequence
        Data.MediaBus.Basics.Series
        Data.MediaBus.Basics.SourceId
        Data.MediaBus.Basics.Ticks
        Data.MediaBus.Basics.VectorExtra
        Data.MediaBus.Conduit.Async
        Data.MediaBus.Conduit.Audio.Raw.Alaw
        Data.MediaBus.Conduit.Audio.Raw.DebugSink
        Data.MediaBus.Conduit.Audio.Raw.Resample
        Data.MediaBus.Conduit.Discontinous
        Data.MediaBus.Conduit.Logging
        Data.MediaBus.Conduit.Reorder
        Data.MediaBus.Conduit.Segment
        Data.MediaBus.Conduit.Stream
        Data.MediaBus.Conduit.Timing
        Data.MediaBus.Conduit.Trace
        Data.MediaBus.Conduit.TypeAnnotations
        Data.MediaBus.Media.Audio
        Data.MediaBus.Media.Audio.Raw
        Data.MediaBus.Media.Audio.Raw.Alaw
        Data.MediaBus.Media.Audio.Raw.Mono
        Data.MediaBus.Media.Audio.Raw.Signed16bit
        Data.MediaBus.Media.Audio.Raw.Stereo
        Data.MediaBus.Media.Blank
        Data.MediaBus.Media.Buffer
        Data.MediaBus.Media.Channels
        Data.MediaBus.Media.Discontinous
        Data.MediaBus.Media.Media
        Data.MediaBus.Media.Reframe
        Data.MediaBus.Media.Samples
        Data.MediaBus.Media.Segment
        Data.MediaBus.Media.Stream
        Data.MediaBus.Transport.Udp
    build-depends:
        QuickCheck <2.10,
        array <0.6,
        async <2.2,
        base >=4.9 && <5,
        bytestring <0.11,
        cereal <0.6,
        conduit <1.3,
        conduit-combinators <1.2,
        conduit-extra <1.2,
        containers <0.6,
        data-default <0.8,
        deepseq <1.5,
        lens <4.16,
        lifted-async <0.10,
        monad-control <1.1,
        monad-logger <0.4,
        mtl <2.3,
        network <2.7,
        parallel <3.3,
        primitive <0.7,
        process <1.5,
        random <1.2,
        resourcet <1.2,
        spool ==0.1,
        stm <2.5,
        streaming-commons <0.2,
        tagged <0.9,
        text <1.3,
        time <1.7,
        transformers <0.6,
        vector <13
    default-language: Haskell2010
    default-extensions: ApplicativeDo BangPatterns ConstraintKinds CPP
                        DataKinds DefaultSignatures DeriveDataTypeable DeriveFoldable
                        DeriveFunctor DeriveGeneric DeriveLift DeriveTraversable
                        DuplicateRecordFields EmptyDataDecls EmptyCase FlexibleInstances
                        FlexibleContexts FunctionalDependencies GADTs
                        GeneralizedNewtypeDeriving KindSignatures LambdaCase
                        MultiParamTypeClasses MultiWayIf NamedFieldPuns OverloadedStrings
                        QuasiQuotes RecordWildCards RankNTypes ScopedTypeVariables
                        StandaloneDeriving TemplateHaskell TupleSections TypeApplications
                        TypeFamilies TypeInType TypeOperators TypeSynonymInstances
                        UnicodeSyntax
    hs-source-dirs: src
    ghc-options: -O2 -Wall -funbox-strict-fields -fno-warn-unused-do-bind -fprint-explicit-kinds

test-suite tests
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    build-depends:
        QuickCheck <2.10,
        array <0.6,
        async <2.2,
        base >=4.9 && <5,
        binary <0.9,
        bytestring <0.11,
        conduit <1.3,
        conduit-combinators <1.2,
        conduit-extra <1.2,
        mediabus <0.4,
        containers <0.6,
        data-default <0.8,
        deepseq <1.5,
        hspec <2.5,
        lens <4.16,
        monad-control <1.1,
        mtl <2.3,
        singletons <2.3,
        spool ==0.1,
        stm <2.5,
        tagged <0.9,
        template-haskell <2.12,
        text <1.3,
        time <1.7,
        transformers <0.6,
        type-spec <0.4,
        vector <13
    default-language: Haskell2010
    default-extensions: ApplicativeDo Arrows BangPatterns
                        ConstraintKinds CPP DataKinds DefaultSignatures DeriveDataTypeable
                        DeriveFoldable DeriveFunctor DeriveGeneric DeriveLift
                        DeriveTraversable DuplicateRecordFields EmptyDataDecls EmptyCase
                        FlexibleInstances FlexibleContexts FunctionalDependencies GADTs
                        GeneralizedNewtypeDeriving KindSignatures LambdaCase
                        MultiParamTypeClasses MultiWayIf NamedFieldPuns OverloadedStrings
                        QuasiQuotes RankNTypes ScopedTypeVariables StandaloneDeriving
                        TemplateHaskell TupleSections TypeApplications TypeFamilies
                        TypeInType TypeOperators TypeSynonymInstances UnicodeSyntax
    hs-source-dirs: specs
    other-modules:
        Data.MediaBus.Conduit.Audio.Raw.ResampleSpec
        Data.MediaBus.Media.BufferSpec
        Data.MediaBus.Conduit.SegmentSpec
        Data.MediaBus.Conduit.ReorderSpec
        Data.MediaBus.Conduit.StreamSpec
        Data.MediaBus.Basics.TicksSpec
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -j +RTS -A256m -n2m -RTS -Wall -O0 -fno-warn-unused-binds -fno-warn-orphans -fno-warn-unused-do-bind -fno-warn-missing-signatures -fno-warn-type-defaults
