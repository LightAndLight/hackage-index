name:                 stache
version:              1.1.0
cabal-version:        >= 1.18
tested-with:          GHC==7.8.4, GHC==7.10.3, GHC==8.0.2, GHC==8.2.1
license:              BSD3
license-file:         LICENSE.md
author:               Mark Karpov <markkarpov92@gmail.com>
maintainer:           Mark Karpov <markkarpov92@gmail.com>
homepage:             https://github.com/stackbuilders/stache
bug-reports:          https://github.com/stackbuilders/stache/issues
category:             Text
synopsis:             Mustache templates for Haskell
build-type:           Simple
description:          Mustache templates for Haskell.
extra-doc-files:      CHANGELOG.md
                    , README.md
data-files:           bench-data/*.mustache
                    , specification/*.yml
                    , templates/*.mustache

source-repository head
  type:               git
  location:           https://github.com/stackbuilders/stache.git

flag dev
  description:        Turn on development settings.
  manual:             True
  default:            False

library
  build-depends:      aeson            >= 0.11 && < 1.3
                    , base             >= 4.7  && < 5.0
                    , bytestring       >= 0.10 && < 0.11
                    , containers       >= 0.5  && < 0.6
                    , deepseq          >= 1.4  && < 1.5
                    , directory        >= 1.2  && < 1.4
                    , filepath         >= 1.2  && < 1.5
                    , megaparsec       >= 6.0  && < 7.0
                    , mtl              >= 2.1  && < 3.0
                    , template-haskell >= 2.10 && < 2.13
                    , text             >= 1.2  && < 1.3
                    , unordered-containers >= 0.2.5 && < 0.3
                    , vector           >= 0.11 && < 0.13
  if !impl(ghc >= 7.10)
    build-depends:    void             == 0.7.*
  if !impl(ghc >= 8.0)
    build-depends:    semigroups       == 0.18.*
  exposed-modules:    Text.Mustache
                    , Text.Mustache.Compile
                    , Text.Mustache.Compile.TH
                    , Text.Mustache.Parser
                    , Text.Mustache.Render
                    , Text.Mustache.Type
  if flag(dev)
    ghc-options:      -Wall -Werror -fsimpl-tick-factor=150
  else
    ghc-options:      -O2 -Wall -fsimpl-tick-factor=150
  default-language:   Haskell2010

test-suite tests
  main-is:            Spec.hs
  hs-source-dirs:     tests
  type:               exitcode-stdio-1.0
  build-depends:      aeson            >= 0.11 && < 1.3
                    , base             >= 4.7  && < 5.0
                    , containers       >= 0.5  && < 0.6
                    , hspec            >= 2.0  && < 3.0
                    , hspec-megaparsec >= 1.0  && < 2.0
                    , megaparsec       >= 6.0  && < 7.0
                    , stache
                    , text             >= 1.2  && < 1.3
  other-modules:      Text.Mustache.Compile.THSpec
                    , Text.Mustache.ParserSpec
                    , Text.Mustache.RenderSpec
                    , Text.Mustache.TypeSpec
  if !impl(ghc >= 8.0)
    build-depends:    semigroups     == 0.18.*
  if flag(dev)
    ghc-options:      -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010

test-suite mustache-spec
  main-is:            Spec.hs
  hs-source-dirs:     mustache-spec
  type:               exitcode-stdio-1.0
  build-depends:      aeson            >= 0.11 && < 1.3
                    , base             >= 4.7  && < 5.0
                    , bytestring       >= 0.10 && < 0.11
                    , containers       >= 0.5  && < 0.6
                    , file-embed
                    , hspec            >= 2.0  && < 3.0
                    , megaparsec       >= 6.0  && < 7.0
                    , stache
                    , text             >= 1.2  && < 1.3
                    , yaml             >= 0.8  && < 0.9
  if flag(dev)
    ghc-options:      -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010

benchmark bench
  main-is:            Main.hs
  hs-source-dirs:     bench
  type:               exitcode-stdio-1.0
  build-depends:      aeson            >= 0.11 && < 1.3
                    , base             >= 4.7  && < 5.0
                    , criterion        >= 0.6.2.1 && < 1.3
                    , deepseq          >= 1.4  && < 1.5
                    , megaparsec       >= 6.0  && < 7.0
                    , stache
                    , text             >= 1.2  && < 1.3
  if flag(dev)
    ghc-options:      -O2 -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010
