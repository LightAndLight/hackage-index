Name:           sr-extra
Version:        1.64
x-revision: 1
License:        BSD3
License-File:   COPYING
Author:         David Fox
Category:       Unclassified
Synopsis:       Module limbo
Description:
  A hodge-podge of functions, modules, and instances.  These
  generally end up here because
     1. they are needed in two unrelated packages,
     2. they belong in some upstream module,
     3. they can't be moved to an upstream module because
        they would add dependencies, or
     4. they are deprecated but still in use
Maintainer:     David Fox <dsf@seereason.com>
Homepage:       https://github.com/seereason/sr-extra
Build-Type:     Simple
Cabal-Version:  >= 1.4

flag network-uri
  Description: Get Network.URI from the network-uri package rather than the
   full network package.
  Default: True

flag omit-serialize
  Description: Omit all the Serialize instances, on the assumption
   that we will use SafeCopy instances instead.
  Default: False

Library
  GHC-Options: -Wall -Wredundant-constraints
  Build-Depends:
    base >= 4.11 && < 5,
    bytestring,
    bzlib,
    Cabal,
    containers,
    Diff,
    directory,
    exceptions,
    fgl,
    filepath,
    generic-data,
    hslogger,
    lens,
    ListLike < 4.7,
    mmorph,
    mtl >= 2.2.2,
    pretty,
    pureMD5,
    random,
    safecopy >= 0.9.5 && < 0.10.4,
    show-combinators,
    syb,
    template-haskell < 2.16,
    text,
    th-lift,
    th-lift-instances,
    th-orphans,
    time >= 1.1,
    unix,
    Unixutils >= 1.51,
    userid,
    uuid,
    uuid-orphans,
    uuid-types,
    zlib
  C-Sources:         cbits/gwinsz.c
  Include-Dirs:        cbits
  Install-Includes:    gwinsz.h
  Extensions: ConstraintKinds, CPP, DataKinds, DeriveDataTypeable, DeriveFunctor, DeriveGeneric
  Extensions: FlexibleInstances, FlexibleContexts, MultiParamTypeClasses, RankNTypes
  Extensions: ScopedTypeVariables, StandaloneDeriving, TypeApplications, TypeFamilies
  Exposed-modules:
    Extra.Bool,
    Extra.Debug,
    Extra.Debug2,
    Extra.Either,
    Extra.EnvPath,
    Extra.Except,
    Extra.Exit,
    Extra.Files,
    Extra.FP,
    Extra.Generics,
    Extra.Generics.Show,
    Extra.IOThread
    Extra.List,
    Extra.HughesPJ,
    Extra.Log,
    Extra.Misc,
    Extra.Monad.Supply,
    Extra.Net,
    Extra.Orphans,
    Extra.Orphans2,
    Extra.Orphans3,
    Extra.Pretty,
    Extra.SafeCopy,
    Extra.SafeCopyDebug,
    Extra.Serialize,
    Extra.SerializeDebug,
    Extra.Text,
    Extra.TH,
    Extra.Time,
    Extra.Terminal,
    Extra.URI,
    Extra.URIQuery,
    Extra.Verbosity
  if !impl(ghcjs)
    Build-Depends:
      show-please,
      filemanip,
      HUnit,
      process,
      process-extras,
      QuickCheck >= 2 && < 3
    Exposed-Modules:
      Extra.GPGSign,
      Extra.Lock,
      Extra.IO,
      Extra.Process,
      Extra.SSH,
      Test.QUnit,
      Test.QuickCheck.Properties

  if flag(network-uri)
    Build-Depends: network-uri >= 2.6 && < 2.6.2
  else
    Build-Depends: network >= 2.4

  if flag(omit-serialize)
    CPP-Options: -DOMIT_SERIALIZE
    -- We still need cereal to implement instance SafeCopy Proxy
    Build-Depends: cereal
  else
    Build-Depends: cereal
