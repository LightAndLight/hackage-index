cabal-version: 2.2

name: witch
version: 1.0.0.1
synopsis: Convert values from one type into another.
description: Witch converts values from one type into another.

build-type: Simple
category: Data
extra-source-files: CHANGELOG.markdown README.markdown
license-file: LICENSE.markdown
license: MIT
maintainer: Taylor Fausak

source-repository head
  location: https://github.com/tfausak/witch
  type: git

flag pedantic
  default: False
  description: Enables @-Werror@, which turns warnings into errors.
  manual: True

common library
  build-depends:
    , base >= 4.10.0 && < 4.17
    , bytestring >= 0.10.8 && < 0.12
    , containers >= 0.5.10 && < 0.7
    , template-haskell >= 2.12.0 && < 2.19
    , text >= 1.2.3 && < 1.3
    , time >= 1.9.1 && < 1.13
  default-language: Haskell2010
  ghc-options:
    -Weverything
    -Wno-all-missed-specialisations
    -Wno-implicit-prelude
    -Wno-missed-specialisations
    -Wno-missing-exported-signatures
    -Wno-redundant-constraints
    -Wno-safe
    -Wno-unsafe

  if impl(ghc >= 8.4)
    ghc-options:
      -Wno-missing-export-lists

  if impl(ghc >= 8.8)
    ghc-options:
      -Wno-missing-deriving-strategies

  if impl(ghc >= 8.10)
    ghc-options:
      -Wno-missing-safe-haskell-mode
      -Wno-prepositive-qualified-module

  if impl(ghc >= 9.2)
    ghc-options:
      -Wno-missing-kind-signatures

  if flag(pedantic)
    ghc-options:
      -Werror

common executable
  import: library

  build-depends: witch
  ghc-options:
    -rtsopts
    -threaded

  if impl(ghc >= 8.10)
    ghc-options:
      -Wno-unused-packages

library
  import: library

  exposed-modules:
    Witch
    Witch.From
    Witch.Instances
    Witch.Lift
    Witch.TryFrom
    Witch.TryFromException
    Witch.Utility
  hs-source-dirs: source/library

  if impl(ghc >= 9.0)
    hs-source-dirs: source/ghc-9.0
  elif impl(ghc >= 8.10)
    hs-source-dirs: source/ghc-8.10
  else
    hs-source-dirs: source/ghc-8.8

test-suite test
  import: executable

  build-depends:
    , HUnit >= 1.6.1 && < 1.7
  hs-source-dirs: source/test-suite
  main-is: Main.hs
  type: exitcode-stdio-1.0
