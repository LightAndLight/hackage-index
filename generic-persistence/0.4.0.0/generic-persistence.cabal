cabal-version:      1.12
name:               generic-persistence
version:            0.4.0.0
license:            BSD3
license-file:       LICENSE
copyright:          2023 Thomas Mahler
maintainer:         thma@apache.org
author:             Thomas Mahler
tested-with:        ghc ==9.2.5 ghc ==9.4.4
homepage:           https://github.com/thma/generic-persistence#readme
bug-reports:        https://github.com/thma/generic-persistence/issues
synopsis:           Database persistence using generics
description:
    Please see the README on GitHub at <https://github.com/thma/generic-persistence#readme>

category:           Database
build-type:         Simple
extra-source-files: README.md

source-repository head
    type:     git
    location: https://github.com/thma/generic-persistence

library
    exposed-modules:
        Database.GP
        Database.GP.Conn
        Database.GP.Entity
        Database.GP.GenericPersistence
        Database.GP.GenericPersistenceSafe
        Database.GP.Query
        Database.GP.SqlGenerator
        Database.GP.TypeInfo

    hs-source-dirs:   src
    other-modules:    Paths_generic_persistence
    default-language: GHC2021
    ghc-options:
        -Wall -Wcompat -Widentities -Wincomplete-record-updates
        -Wincomplete-uni-patterns -Wmissing-export-lists
        -Wmissing-home-modules -Wpartial-fields -Wredundant-constraints

    build-depends:
        HDBC <2.5,
        base >=4.7 && <5,
        convertible <1.2,
        generic-deriving <1.15,
        resource-pool <0.5

test-suite generic-persistence-test
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    build-tool-depends: hspec-discover:hspec-discover >=2 && <3
    hs-source-dirs:     test
    other-modules:
        DemoSpec
        EmbeddedSpec
        EnumSpec
        ExceptionsSpec
        GenericPersistenceSpec
        OneToManySpec
        ReferenceSpec
        Paths_generic_persistence

    default-language:   GHC2021
    ghc-options:
        -Wall -Wcompat -Widentities -Wincomplete-record-updates
        -Wincomplete-uni-patterns -Wmissing-export-lists
        -Wmissing-home-modules -Wpartial-fields -Wredundant-constraints
        -threaded -rtsopts -with-rtsopts=-N

    build-depends:
        HDBC <2.5,
        HDBC-sqlite3 <2.4,
        QuickCheck <2.15,
        base >=4.7 && <5,
        convertible <1.2,
        generic-deriving <1.15,
        generic-persistence,
        hspec <2.10,
        hspec-discover <2.10,
        resource-pool <0.5
