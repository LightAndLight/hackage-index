cabal-version:      2.4
name:               perf
version:            0.10.2
synopsis:           Low-level run time measurement.
description:
  A set of tools to measure performance of Haskell programs.
  See the Perf module for an example and full API documentation.

category:           project
homepage:           https://github.com/tonyday567/perf#readme
bug-reports:        https://github.com/tonyday567/perf/issues
author:             Tony Day, Marco Zocca
maintainer:         tonyday567@gmail.com
copyright:          Tony Day
license:            BSD-3-Clause
license-file:       LICENSE
build-type:         Simple
tested-with:
  GHC ==8.6.5 || ==8.8.4 || ==8.10.7 || ==9.2.5 || ==9.4.4
extra-source-files: ChangeLog.md

source-repository head
  type:     git
  location: https://github.com/tonyday567/perf

common ghc-options-stanza
  ghc-options:
    -Wall -Wcompat -Wincomplete-record-updates
    -Wincomplete-uni-patterns -Wredundant-constraints

library
  import:             ghc-options-stanza
  exposed-modules:
    Perf
    Perf.Algos
    Perf.BigO
    Perf.Measure
    Perf.Report
    Perf.Space
    Perf.Stats
    Perf.Time
    Perf.Types

  hs-source-dirs:     src
  build-depends:
    , base                  >=4.7    && <5
    , containers            ^>=0.6
    , deepseq               >=1.4.4  && <1.5
    , formatn               ^>=0.2.1
    , mtl                   >=2.2.2 && <2.4
    , numhask-space         ^>=0.10
    , optparse-applicative  ^>=0.17
    , rdtsc                 ^>=1.3
    , recursion-schemes     ^>=5.2.2
    , text                  >=1.2 && <2.1
    , time                  >=1.9 && <1.13
    , vector                >=0.12.3 && <0.14

  default-language:   Haskell2010
  default-extensions:
  ghc-options:        -O2

executable perf-explore
  import:           ghc-options-stanza
  main-is:          explore.hs
  hs-source-dirs:   app
  build-depends:
    , base                  >=4.7   && <5
    , containers            ^>=0.6
    , deepseq               >=1.4.4 && <1.5
    , formatn               >=0.2 && <0.3
    , gauge                 ^>=0.2.5
    , mtl                   >=2.2.2 && <2.4
    , optparse-applicative  ^>=0.17
    , perf
    , text                  >=1.2 && <2.1

  default-language: Haskell2010
  ghc-options:      -O2

test-suite doctests
  type:             exitcode-stdio-1.0
  main-is:          doctests.hs
  hs-source-dirs:   test
  default-language: Haskell2010
  build-depends:
    , base
    , QuickCheck
