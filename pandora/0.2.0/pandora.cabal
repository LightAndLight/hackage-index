name:                pandora
version:             0.2.0
synopsis:            A box of patterns and paradigms
description:         Humble attempt to define a library for problem solving based on math abstractions.
homepage:            https://github.com/iokasimov/pandora
license:             MIT
license-file:        LICENSE
extra-source-files:  CHANGELOG.md, README.md
author:              Murat Kasimov
maintainer:          Murat Kasimov <iokasimov.m@gmail.com>
copyright:           Copyright (c) 2019 Murat Kasimov
category:            Data, Control
build-type:          Simple
cabal-version:       >= 1.10

source-repository head
  type: git
  location: https://github.com/iokasimov/pandora.git

library
  exposed-modules:
    -- Axioms set
    Pandora.Core
    Pandora.Core.Functor
    Pandora.Core.Morphism
    Pandora.Core.Transformation

    Pandora.Paradigm
    -- Basic constructions
    Pandora.Paradigm.Basis
    Pandora.Paradigm.Basis.Conclusion
    Pandora.Paradigm.Basis.Constant
    Pandora.Paradigm.Basis.Continuation
    Pandora.Paradigm.Basis.Edges
    Pandora.Paradigm.Basis.Endo
    Pandora.Paradigm.Basis.Fix
    Pandora.Paradigm.Basis.Free
    Pandora.Paradigm.Basis.Identity
    Pandora.Paradigm.Basis.Jack
    Pandora.Paradigm.Basis.Jet
    Pandora.Paradigm.Basis.Kan
    Pandora.Paradigm.Basis.Maybe
    Pandora.Paradigm.Basis.Predicate
    Pandora.Paradigm.Basis.Product
    Pandora.Paradigm.Basis.Proxy
    Pandora.Paradigm.Basis.Tagged
    Pandora.Paradigm.Basis.Twister
    Pandora.Paradigm.Basis.Validation
    Pandora.Paradigm.Basis.Variation
    Pandora.Paradigm.Basis.Wye
    Pandora.Paradigm.Basis.Yoneda
    -- Control flow primitives
    Pandora.Paradigm.Controlflow
    Pandora.Paradigm.Controlflow.Observable
    Pandora.Paradigm.Controlflow.Pipeline
    -- Tools for datastructures
    Pandora.Paradigm.Inventory
    Pandora.Paradigm.Inventory.Environmental
    Pandora.Paradigm.Inventory.Optics
    Pandora.Paradigm.Inventory.Stateful
    Pandora.Paradigm.Inventory.Storage
    -- Tree-based datastructures
    Pandora.Paradigm.Structure
    Pandora.Paradigm.Structure.Stack
    Pandora.Paradigm.Structure.Graph
    Pandora.Paradigm.Structure.Binary

    Pandora.Pattern
    -- Functor typeclassess
    Pandora.Pattern.Functor
    Pandora.Pattern.Functor.Adjoint
    Pandora.Pattern.Functor.Alternative
    Pandora.Pattern.Functor.Applicative
    Pandora.Pattern.Functor.Bindable
    Pandora.Pattern.Functor.Comonad
    Pandora.Pattern.Functor.Contravariant
    Pandora.Pattern.Functor.Covariant
    Pandora.Pattern.Functor.Determinable
    Pandora.Pattern.Functor.Distributive
    Pandora.Pattern.Functor.Avoidable
    Pandora.Pattern.Functor.Extendable
    Pandora.Pattern.Functor.Extractable
    Pandora.Pattern.Functor.Invariant
    Pandora.Pattern.Functor.Liftable
    Pandora.Pattern.Functor.Lowerable
    Pandora.Pattern.Functor.Monad
    Pandora.Pattern.Functor.Pointable
    Pandora.Pattern.Functor.Representable
    Pandora.Pattern.Functor.Traversable
    Pandora.Pattern.Functor.Divariant
    -- Typeclassess about functor junctions
    Pandora.Pattern.Junction
    Pandora.Pattern.Junction.Composition
    Pandora.Pattern.Junction.Transformer
    Pandora.Pattern.Junction.Schemes
    Pandora.Pattern.Junction.Schemes.TU
    Pandora.Pattern.Junction.Schemes.TUV
    Pandora.Pattern.Junction.Schemes.TUVW
    Pandora.Pattern.Junction.Schemes.UT
    Pandora.Pattern.Junction.Schemes.UTU
    -- Typeclassess about object internals
    Pandora.Pattern.Object
    Pandora.Pattern.Object.Chain
    Pandora.Pattern.Object.Group
    Pandora.Pattern.Object.Lattice
    Pandora.Pattern.Object.Monoid
    Pandora.Pattern.Object.Quasiring
    Pandora.Pattern.Object.Ringoid
    Pandora.Pattern.Object.Semigroup
    Pandora.Pattern.Object.Semilattice
    Pandora.Pattern.Object.Setoid
  default-extensions:
    DataKinds, ConstraintKinds, ExistentialQuantification, GADTs, QuantifiedConstraints
    FlexibleContexts, FlexibleInstances, KindSignatures, LiberalTypeSynonyms
    MultiParamTypeClasses, NoImplicitPrelude, PackageImports, PolyKinds, RankNTypes
    ScopedTypeVariables, TypeApplications, TypeFamilies, TypeFamilyDependencies, TypeOperators
  default-language: Haskell2010
  ghc-options: -Wall -fno-warn-tabs
