Name:           group-by-date
Version:        0.1.0.2
License:        BSD3
License-File:   LICENSE
Author:         Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:     Henning Thielemann <haskell@henning-thielemann.de>
Homepage:       http://hub.darcs.net/thielema/group-by-date/
Category:       Console
Synopsis:       Shell command for grouping files by dates into folders
Description:
  This program is intended for grouping photography images by date
  into a hierarchy of date related folders.
  .
  If you have a folder of photographies, say @photos@,
  you may run
  .
  > group-by-date -r photos
  .
  The program will emit a Bash script like this one:
  .
  > mkdir -p 2017/2017-06/2017-06-28 && mv photos/0001.jpeg 2017/2017-06/2017-06-28
  > mkdir -p 2017/2017-06/2017-06-28 && mv photos/0002.jpeg 2017/2017-06/2017-06-28
  > mkdir -p 2017/2017-06/2017-06-28 && mv photos/0003.jpeg 2017/2017-06/2017-06-28
  .
  You can inspect the script and if you like it, you can run it:
  .
  > group-by-date -r photos | bash
  .
  If you want a different command,
  say copying with preservation of modification time, you can call
  .
  > group-by-date --command='cp -p' -r photos
  .
  Alternatively, you can run the actions immediately,
  that is, without a Bash script:
  .
  > group-by-date --mode=move -r photos
  > group-by-date --mode=copy -r photos
  .
  You can also change the target directory structure
  using the @--format@ option.
  You can list all options and default values using @--help@.
  .
  Attention:
  Media for photographies is often formatted with FAT.
  This may yield trouble with respect to timezones.
Tested-With:    GHC==7.8.4, GHC==8.2.1
Cabal-Version:  >=1.6
Build-Type:     Simple

Source-Repository this
  Tag:         0.1.0.2
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/group-by-date/

Source-Repository head
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/group-by-date/

Executable group-by-date
  Build-Depends:
    hsshellscript >=3.1.0 && <3.5,
    filemanip >=0.3.5 && <0.4,
    pathtype >=0.8 && <0.9,
    time >=1.5 && <1.9,
    unix-compat >=0.3 && <0.6,
    explicit-exception >=0.1 && <0.2,
    transformers >=0.2 && <0.6,
    utility-ht >=0.0.1 && <0.1,
    base >=3 && <5

  GHC-Options:    -Wall
  Hs-source-dirs: src
  Other-Modules:  GroupByDate
  Main-Is:        Main.hs
