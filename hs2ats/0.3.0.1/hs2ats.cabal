cabal-version: 1.18
name: hs2ats
version: 0.3.0.1
x-revision: 1
license: BSD3
license-file: LICENSE
copyright: Copyright: (c) 2018 Vanessa McHale
maintainer: vamchale@gmail.com
author: Vanessa McHale
homepage: https://github.com/vmchale/hs2ats#readme
synopsis: Create ATS types from Haskell types
description:
    This package enables scanning Haskell source files for data types and then generating [ATS](http://www.ats-lang.org/) types from them.
category: Language, Haskell, ATS
build-type: Simple
extra-source-files:
    test/data/*.hs
    test/data/*.out

source-repository head
    type: git
    location: https://github.com/vmchale/hs2ats

flag development
    description:
        Enable `-Werror`
    default: False
    manual: True

flag no-executable
    description:
        Don't generate an executable

library
    exposed-modules:
        Language.ATS.Generate
    hs-source-dirs: src
    other-modules:
        Language.ATS.Generate.Error
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        base >=4.7 && <5,
        haskell-src-exts -any,
        language-ats >=1.0.0.0 && <1.4.0.0,
        casing -any,
        lens -any,
        ansi-wl-pprint -any,
        composition-prelude -any,
        deepseq -any,
        cpphs -any
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates

executable hs2ats
    main-is: Main.hs
    hs-source-dirs: app
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base -any,
        optparse-generic -any,
        hs2ats -any
    
    if flag(no-executable)
        buildable: False
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates

test-suite hs2ats-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base -any,
        hs2ats -any,
        hspec -any,
        hspec-dirstream -any,
        system-filepath -any
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates

benchmark hs2ats-bench
    type: exitcode-stdio-1.0
    main-is: Bench.hs
    hs-source-dirs: bench
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        base -any,
        hs2ats -any,
        criterion -any
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
