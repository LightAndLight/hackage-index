Name: debug-me
Version: 1.20170505
Cabal-Version: >= 1.8
Maintainer: Joey Hess <joey@kitenet.net>
Author: Joey Hess
Stability: Experimental
Copyright: 2017 Joey Hess
License: AGPL-3
Homepage: https://debug-me.branchable.com/
Category: Utility
Build-Type: Custom
Synopsis: secure remote debugging
Description:
 Debugging a problem over email is slow, tedious, and hard. The developer
 needs to see your problem to understand it. Debug-me aims to make debugging
 fast, fun, and easy, by letting the developer access your computer remotely,
 so they can immediately see and interact with the problem. Making your
 problem their problem gets it fixed fast.
 .
 A debug-me session is logged and signed with the developer's GnuPG
 key, producing a chain of evidence of what they saw and what they did.
 So the developer's good reputation is leveraged to make debug-me secure.
 .
 When you start debug-me without any options, it will connect to a debug-me
 server, and print out an url that you can give to the developer to get
 them connected to you. Then debug-me will show you their GnuPG key and who
 has signed it. If the developer has a good reputation, you can proceed
 to let them type into your console in a debug-me session. Once the
 session is done, the debug-me server will email you the signed
 evidence of what the developer did in the session.
 . 
 If the developer did do something bad, you'd have proof that they cannot
 be trusted, which you can share with the world. Knowing that is the case
 will keep most developers honest.
License-File: AGPL
Extra-Source-Files:
  CHANGELOG
  Makefile
  debug-me.1
  debug-me.service
  debug-me.init
  debug-me.default

Executable debug-me
  Main-Is: debug-me.hs
  GHC-Options: -threaded -Wall -fno-warn-tabs -O2
  Build-Depends:
      base (>= 4.9 && < 5.0)
    , network (>= 2.6)
    , bytestring == 0.10.*
    , cryptonite (>= 0.20)
    , unix (>= 2.7)
    , process (>= 1.4)
    , async (>= 2.1)
    , stm (>= 2.4)
    , stm-chans (>= 3.0)
    , posix-pty (>= 0.2.1)
    , terminal-size (>= 0.3)
    , aeson (>= 0.11 && < 1.1)
    , sandi (>= 0.4)
    , text (>= 1.2.2)
    , optparse-applicative (>= 0.12)
    , graphviz (== 2999.18.*)
    , time (>= 1.6)
    , filepath (>= 1.4)
    , directory (>= 1.2)
    , containers (>= 0.5)
    , unordered-containers (>= 0.2)
    , unbounded-delays (>= 0.1)
    , memory (>= 0.13)
    , warp (>= 3.2)
    , wai (>= 3.2)
    , http-types (>= 0.9)
    , http-client (>= 0.4)
    , http-client-tls (>= 0.2)
    , websockets (>= 0.9)
    , wai-websockets (>= 3.0)
    , uuid (>= 1.3)
    , protobuf (>= 0.2)
    , cereal (>= 0.5)
    , utf8-string (>= 1.0)
    , network-uri (>= 2.6)
    , mime-mail (>= 0.4)
  Other-Modules:
    ControlWindow
    ControlSocket
    CmdLine
    Crypto
    DotDir
    Graphviz
    Gpg
    Gpg.Wot
    Hash
    JSON
    Log
    Memory
    Output
    Pty
    PrevActivity
    ProtocolBuffers
    Replay
    Role.Developer
    Role.Downloader
    Role.User
    Role.Watcher
    Session
    Server
    ServerList
    SessionID
    Types
    Val
    VirtualTerminal
    WebSockets

source-repository head
  type: git
  location: git://debug-me.branchable.com/
