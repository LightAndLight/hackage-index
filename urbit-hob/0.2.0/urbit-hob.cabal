name:           urbit-hob
version:        0.2.0
synopsis:       Hoon-style atom manipulation and printing functions
homepage:       https://github.com/urbit/urbit-hob
bug-reports:    https://github.com/urbit/urbit-hob/issues
author:         Jared Tobin
maintainer:     jared@jtobin.io
copyright:      2019 Jared Tobin
category:       Urbit
license:        MIT
license-file:   LICENSE
build-type:     Simple
cabal-version:  >= 1.10
description:
  Here you can primarily find functions for dealing with the \"patp\" phonetic
  base used by Urbit.  The \@p encoding is used for naming ships; it uniquely
  represents a 32-bit number in a memorable and pronounceable fashion.
  .
  The \@p encoding is an /obfuscated/ representation of an underlying 32-bit
  number, in particular, hence the \"ob\" in the library's name.
  .
  The @Urbit.Ob@ module exposes two functions, 'patp' and 'fromPatp', for
  converting between representations.  You can also render a 'Patp' value via
  the 'render' function, or parse one via 'parse'.
  .
  Some quick examples:
  .
  >>> :set -XOverloadedStrings
  >>> import qualified Urbit.Ob as Ob
  >>>
  >>> let nidsut = Ob.patp 15663360
  >>> Ob.render nidsut
  "~nidsut-tomdun"
  >>> Ob.fromPatp nidsut
  15663360
  >>> Ob.parse "~nidsut-tomdun"
  Right ~nidsut-tomdun

source-repository head
  type: git
  location: https://github.com/urbit/urbit-hob

flag release
  Description:  Build for release
  Default:      False
  Manual:       True

library
  default-language: Haskell2010
  hs-source-dirs:   lib

  if flag(release)
    ghc-options:     -Wall
  else
    ghc-options:     -Wall -Werror

  exposed-modules:
      Urbit.Ob
    , Urbit.Ob.Co
    , Urbit.Ob.Muk
    , Urbit.Ob.Ob

  other-modules:
      Data.Serialize.Extended

  build-depends:
      base        >= 4.7  && < 6
    , bytestring  >= 0.10 && < 1
    , murmur3     >= 1.0  && < 2
    , text        >= 1.2  && < 2
    , vector      >= 0.12 && < 1

Test-suite ob
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Ob.hs
  other-modules:
    Ob.Tests.Small
    Ob.Tests.Med
    Ob.Tests.Property
    Ob.Tests.Unit
  default-language:    Haskell2010
  ghc-options:
    -rtsopts
  build-depends:
      base
    , hspec
    , hspec-core
    , QuickCheck
    , urbit-hob

Test-suite co
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Co.hs
  other-modules:
    Co.Tests.Property
    Co.Tests.Unit
  default-language:    Haskell2010
  ghc-options:
    -rtsopts
  build-depends:
      base
    , hspec
    , hspec-core
    , QuickCheck
    , text
    , urbit-hob

