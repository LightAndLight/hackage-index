name:                imj-base
version:             0.1.0.2
Category:            Animation, Game Engine, Graphics, Algorithms, Mathematics,
                     Optimisation, Optimization, User Interface, Terminal
Synopsis: Game engine with geometry, easing, animated text, delta rendering.
Description:         Game engine that is intended to help implementing games
                     for the terminal.
                     .

                     Contains types and classes about discrete and continuous
                     geometry, collision detection, animated UIs,
                     animated colored text and easing functions.
                     .

                     Also contains a renderer (delta-renderer) optimized to avoid
                     screen tearing in the terminal.
homepage:            https://github.com/OlivierSohn/hamazed/blob/master/imj-base/README.md
bug-reports:         https://github.com/OlivierSohn/hamazed/issues/
license:             BSD3
license-file:        LICENSE
author:              Olivier Sohn
maintainer:          olivier.sohn@gmail.com
copyright:           2017 - 2018 Olivier Sohn
build-type:          Simple
extra-source-files:  README.md CHANGELOG.md
cabal-version:       >=1.10

Tested-With: GHC == 8.0.2, GHC == 8.2.2

library
  hs-source-dirs:      src
  other-modules:
  exposed-modules:     Imj.Data.Vector.Unboxed.Mutable.Dynamic
                     , Imj.Example.DeltaRender.FromMonadIO
                     , Imj.Example.DeltaRender.FromMonadReader
                     , Imj.Example.SequentialTextTranslationsAnchored
                     , Imj.GameItem.Weapon.Laser.Types
                     , Imj.GameItem.Weapon.Laser
                     , Imj.Geo.Discrete
                     , Imj.Geo.Discrete.Bresenham
                     , Imj.Geo.Discrete.Bresenham3
                     , Imj.Geo.Discrete.Resample
                     , Imj.Geo.Discrete.Types
                     , Imj.Geo.Continuous
                     , Imj.Geo.Continuous.Conversion
                     , Imj.Geo.Continuous.Types
                     , Imj.Geo.Types
                     , Imj.Graphics.Class
                     , Imj.Graphics.Class.Colorable
                     , Imj.Graphics.Class.DiscreteColorableMorphing
                     , Imj.Graphics.Class.DiscreteInterpolation
                     , Imj.Graphics.Class.DiscreteMorphing
                     , Imj.Graphics.Class.Drawable
                     , Imj.Graphics.Class.Draw
                     , Imj.Graphics.Class.HasLayeredColor
                     , Imj.Graphics.Class.Render
                     , Imj.Graphics.Class.DiscreteDistance
                     , Imj.Graphics.Color
                     , Imj.Graphics.Color.Types
                     , Imj.Graphics.Render
                     , Imj.Graphics.Render.FromMonadReader
                     , Imj.Graphics.Interpolation
                     , Imj.Graphics.Interpolation.SequentiallyInterpolatedList
                     , Imj.Graphics.Interpolation.Evolution
                     , Imj.Graphics.Math.Ease
                     , Imj.Graphics.Render.Delta
                     , Imj.Graphics.Render.Delta.Buffers
                     , Imj.Graphics.Render.Delta.Buffers.Dimensions
                     , Imj.Graphics.Render.Delta.Cell
                     , Imj.Graphics.Render.Delta.Cells
                     , Imj.Graphics.Render.Delta.Clear
                     , Imj.Graphics.Render.Delta.Console
                     , Imj.Graphics.Render.Delta.DefaultPolicies
                     , Imj.Graphics.Render.Delta.Draw
                     , Imj.Graphics.Render.Delta.Env
                     , Imj.Graphics.Render.Delta.Flush
                     , Imj.Graphics.Render.Delta.Internal.Types
                     , Imj.Graphics.Render.Delta.Types
                     , Imj.Graphics.Render.Naive
                     , Imj.Graphics.Text.Alignment
                     , Imj.Graphics.Text.Animation
                     , Imj.Graphics.Text.ColorString
                     , Imj.Graphics.Text.ColorString.Interpolation
                     , Imj.Graphics.UI.Animation
                     , Imj.Graphics.UI.Colored
                     , Imj.Graphics.UI.RectContainer
                     , Imj.Graphics.UI.RectContainer.MorphParallel4
                     , Imj.Input
                     , Imj.Input.Blocking
                     , Imj.Input.NonBlocking
                     , Imj.Input.Types
                     , Imj.Iteration
                     , Imj.Physics.Discrete
                     , Imj.Physics.Discrete.Collision
                     , Imj.Physics.Discrete.Types
                     , Imj.Threading
                     , Imj.Timing
                     , Imj.Util
  build-depends:       base >= 4.8 && < 4.11
                     , ansi-terminal >= 0.6.3.1 && < 0.9
                     , imj-prelude ==0.1.*
                     , mtl >= 2.2.1 && < 2.3
                     , primitive ==0.6.*
                     , random ==1.1.*
                     , terminal-size >= 0.3.2.1 && < 0.3.3
                     , text ==1.2.*
                     , time ==1.8.*
                     , text ==1.2.*
                     , vector >= 0.12.0.1 && < 0.12.1
                     , vector-algorithms >= 0.7.0.1 && < 0.8
  ghc-options:       -Wall -fpedantic-bottoms -Wredundant-constraints
                     -fexcess-precision -optc-ffast-math
  default-language:    Haskell2010

executable imj-base-examples-exe
  hs-source-dirs:      example
  other-modules:
  main-is:             Main.hs
  build-depends:       base >= 4.8 && < 4.11
                     , ansi-terminal >= 0.6.3.1 && < 0.9
                     , mtl >= 2.2.1 && < 2.3
                     , text ==1.2.*
                     , imj-base
                     , imj-prelude ==0.1.*
                     , time ==1.8.*
  ghc-options:       -Wall -fpedantic-bottoms -Wredundant-constraints
                     -fexcess-precision -optc-ffast-math
  default-language:    Haskell2010

test-suite imj-base-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  other-modules:       Test.Imj.Bresenham3
                     , Test.Imj.Ease
                     , Test.Imj.InterpolatedColorString
                     , Test.Imj.Interpolation
                     , Test.Imj.Timing
                     , Test.Imj.Vector
  main-is:             Spec.hs
  build-depends:       base >= 4.8 && < 4.11
                     , ansi-terminal >= 0.6.3.1 && < 0.9
                     , mtl >= 2.2.1 && < 2.3
                     , text ==1.2.*
                     , imj-base
                     , imj-prelude ==0.1.*
                     , time ==1.8.*
  ghc-options:       -Wall -fpedantic-bottoms -Wredundant-constraints
                     -fexcess-precision -optc-ffast-math
  default-language:    Haskell2010

source-repository head
  type:     git
  location: https://github.com/OlivierSohn/hamazed/
  subdir:   imj-base
