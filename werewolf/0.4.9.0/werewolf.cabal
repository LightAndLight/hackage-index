name:           werewolf
version:        0.4.9.0

author:         Henry J. Wylde
maintainer:     public@hjwylde.com
homepage:       https://github.com/hjwylde/werewolf

synopsis:       A game engine for running werewolf in a chat client
description:    This engine is based off of Werewolves of Millers Hollow
                (http://www.games-wiki.org/wiki/Werewolves_of_Millers_Hollow/).
                See https://github.com/hjwylde/werewolf for help on writing chat interfaces.

license:        BSD3
license-file:   LICENSE

cabal-version:  >= 1.10
category:       Game
build-type:     Simple

extra-source-files: CHANGELOG.md README.md

source-repository head
    type:       git
    location:   git@github.com:hjwylde/werewolf

executable werewolf
    main-is:        Main.hs
    hs-source-dirs: app/
    ghc-options:    -threaded -with-rtsopts=-N
    other-modules:
        Werewolf.Command.Choose
        Werewolf.Command.Circle
        Werewolf.Command.End
        Werewolf.Command.Heal
        Werewolf.Command.Help
        Werewolf.Command.Interpret
        Werewolf.Command.Pass
        Werewolf.Command.Ping
        Werewolf.Command.Poison
        Werewolf.Command.Protect
        Werewolf.Command.Quit
        Werewolf.Command.Reveal
        Werewolf.Command.See
        Werewolf.Command.Start
        Werewolf.Command.Status
        Werewolf.Command.Version
        Werewolf.Command.Vote
        Werewolf.Game
        Werewolf.Messages
        Werewolf.Options
        Werewolf.Version
        Paths_werewolf

    default-language: Haskell2010
    other-extensions:
        FlexibleContexts,
        MultiParamTypeClasses,
        OverloadedStrings
    build-depends:
        aeson >= 0.8 && < 0.11,
        base >= 4.8 && < 5,
        directory == 1.2.*,
        extra == 1.4.*,
        filepath == 1.4.*,
        lens >= 4.12 && < 4.14,
        MonadRandom == 0.4.*,
        mtl == 2.2.*,
        optparse-applicative >= 0.11 && < 0.13,
        random-shuffle,
        text == 1.2.*,
        transformers == 0.4.*,
        werewolf

library
    hs-source-dirs: src/
    exposed-modules:
        Game.Werewolf
        Game.Werewolf.Command
        Game.Werewolf.Engine
        Game.Werewolf.Game
        Game.Werewolf.Player
        Game.Werewolf.Response
        Game.Werewolf.Role
    other-modules:
        Game.Werewolf.Messages
        Game.Werewolf.Util

    default-language: Haskell2010
    other-extensions:
        CPP,
        DeriveGeneric,
        FlexibleContexts,
        MultiParamTypeClasses,
        OverloadedStrings,
        Rank2Types,
        TemplateHaskell
    build-depends:
        aeson >= 0.8 && < 0.11,
        base >= 4.8 && < 5,
        containers == 0.5.*,
        directory == 1.2.*,
        extra == 1.4.*,
        filepath == 1.4.*,
        lens >= 4.12 && < 4.14,
        mtl == 2.2.*,
        text == 1.2.*,
        transformers == 0.4.*

test-suite werewolf-test
    type:           exitcode-stdio-1.0
    main-is:        Main.hs
    hs-source-dirs: test/app/, test/src/
    ghc-options:    -threaded -with-rtsopts=-N
    other-modules:
        Game.Werewolf.Test.Arbitrary
        Game.Werewolf.Test.Command
        Game.Werewolf.Test.Command.Choose
        Game.Werewolf.Test.Command.Heal
        Game.Werewolf.Test.Command.Pass
        Game.Werewolf.Test.Command.Poison
        Game.Werewolf.Test.Command.Protect
        Game.Werewolf.Test.Command.Quit
        Game.Werewolf.Test.Command.Reveal
        Game.Werewolf.Test.Command.See
        Game.Werewolf.Test.Command.Vote
        Game.Werewolf.Test.Engine
        Game.Werewolf.Test.Game
        Game.Werewolf.Test.Player
        Game.Werewolf.Test.Util

    default-language: Haskell2010
    other-extensions:
        OverloadedStrings
    build-depends:
        base >= 4.8 && < 5,
        containers == 0.5.*,
        extra == 1.4.*,
        lens >= 4.12 && < 4.14,
        mtl == 2.2.*,
        QuickCheck == 2.8.*,
        tasty >= 0.10 && < 0.12,
        tasty-quickcheck == 0.8.*,
        text == 1.2.*,
        werewolf
