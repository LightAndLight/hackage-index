name:                tmp-postgres
version:             1.7.0.1
synopsis: Start and stop a temporary postgres
description:
 @tmp-postgres@ provides functions creating a temporary @postgres@ instance.
 .
 By default it will create a temporary directory for the data,
 a random port for listening and a temporary directory for a UNIX
 domain socket.
 .
 Here is an example using the expection safe 'with' function:
 .
 >  with $ \db -> bracket (connectPostgreSQL (toConnectionString db)) close $ \conn ->
 >   execute_ conn "CREATE TABLE foo (id int)"
 .
 MacOS and Linux are support. Windows is not.
 .
 Requires PostgreSQL 9.3+
 .
 WARNING!!
 Ubuntu's PostgreSQL installation does not put @initdb@ on the @PATH@. We need to add it manually. The necessary binaries are in the @\/usr\/lib\/postgresql\/VERSION\/bin\/@ directory, and should be added to the @PATH@
 .
 > echo "export PATH=$PATH:/usr/lib/postgresql/VERSION/bin/" >> /home/ubuntu/.bashrc
 .
homepage:            https://github.com/jfischoff/tmp-postgres#readme
license:             BSD3
license-file:        LICENSE
author:              Jonathan Fischoff
maintainer:          jonathangfischoff@gmail.com
copyright:           2017-2019 Jonathan Fischoff
category:            Web
build-type:          Simple
extra-source-files:  README.md
cabal-version:       >=1.10
tested-with: GHC ==8.6.5

library
  hs-source-dirs:      src
  exposed-modules: Database.Postgres.Temp
                 , Database.Postgres.Temp.Internal
                 , Database.Postgres.Temp.Internal.Core
                 , Database.Postgres.Temp.Internal.Partial
  default-extensions: LambdaCase
    , DerivingStrategies
    , DerivingVia
    , ScopedTypeVariables
    , RecordWildCards
    , DeriveGeneric
    , OverloadedStrings
    , RecordWildCards
    , ApplicativeDo
    , DeriveFunctor
    , ViewPatterns
    , GeneralizedNewtypeDeriving
  build-depends: base >= 4.6 && < 5
               , temporary
               , process >= 1.2.0.0
               , unix
               , directory
               , bytestring
               , postgresql-simple
               , postgres-options >= 0.2.0.0
               , port-utils
               , async
               , generic-monoid
               , transformers
               , containers
               , ansi-wl-pprint
  ghc-options: -Wall
  default-language:    Haskell2010


test-suite test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Spec.hs
  build-depends:       base
                     , containers
                     , postgresql-libpq
                     , tmp-postgres
                     , hspec
                     , temporary
                     , directory
                     , process
                     , postgresql-simple
                     , bytestring
                     , mtl
                     , unix
                     , temporary
                     , either
                     , transformers
                     , postgres-options
                     , port-utils

  ghc-options:        -Wall -threaded -rtsopts -with-rtsopts=-N
  default-language:    Haskell2010
  default-extensions: LambdaCase
    , DerivingStrategies
    , DerivingVia
    , ScopedTypeVariables
    , RecordWildCards
    , DeriveGeneric
    , OverloadedStrings
    , RecordWildCards
    , DeriveDataTypeable
    , QuasiQuotes
    , RankNTypes


source-repository head
  type:     git
  location: https://github.com/jfischoff/tmp-postgres
