name:          cacophony
version:       0.3.0
synopsis:      A library implementing the Noise protocol.
license:       PublicDomain
license-file:  LICENSE
author:        John Galt
maintainer:    jgalt@centromere.net
homepage:      https://github.com/centromere/cacophony
bug-reports:   https://github.com/centromere/cacophony/issues
category:      Cryptography
build-type:    Simple
cabal-version: >=1.10
tested-with:   GHC == 7.10.1, GHC == 7.10.2
description:
  This library implements the <https://github.com/trevp/noise/blob/master/noise.md Noise>
  protocol.

extra-source-files:
  .travis.yml
  README.md
  changelog.md
  LICENSE
  tests/.hlint

source-repository head
  type: git
  location: https://github.com/centromere/cacophony.git

--------------------------------------------------------------------------------
-- FLAGS

flag hlint

flag doctest

flag llvm
  default: False
  manual: True

--------------------------------------------------------------------------------
-- LIBRARY

library
  build-depends:
    base >=4.8 && <5,
    bytestring,
    cryptonite,
    lens,
    memory,
    mtl
  hs-source-dirs:   src
  default-language: Haskell2010
  exposed-modules:
    Crypto.Noise.Cipher
    Crypto.Noise.Cipher.ChaChaPoly1305
    Crypto.Noise.Curve
    Crypto.Noise.Curve.Curve25519
    Crypto.Noise.MessagePatterns
    Crypto.Noise.Handshake
    Crypto.Noise.HandshakePatterns
    Crypto.Noise.Hash
    Crypto.Noise.Hash.SHA256
    Crypto.Noise.Internal.CipherState
    Crypto.Noise.Internal.SymmetricState
    Crypto.Noise.Internal.HandshakeState
    Crypto.Noise.Types
  ghc-options:      -rtsopts -with-rtsopts=-N -Wall -fwarn-tabs

  if flag(llvm)
    ghc-options: -fllvm

--------------------------------------------------------------------------------
-- TESTS

test-suite test-cacophony
  type:             exitcode-stdio-1.0
  main-is:          Tests.hs
  ghc-options:      -Wall -fwarn-tabs
  hs-source-dirs:   tests
  default-language: Haskell2010

  build-depends:
    base >=4.8 && <5,
    bytestring,
    cacophony,
    mtl,
    QuickCheck,
    tasty,
    tasty-quickcheck

  other-modules:
    CipherState,
    Handshake,
    HandshakeStates,
    Imports,
    Instances,
    SymmetricState

test-suite hlint
  type:             exitcode-stdio-1.0
  main-is:          hlint.hs
  ghc-options:      -Wall -fwarn-tabs
  hs-source-dirs:   tests
  default-language: Haskell2010

  if !flag(hlint)
    buildable: False
  else
    build-depends:
      base  >=4.8 && <5,
      hlint

test-suite doctests
  type:             exitcode-stdio-1.0
  main-is:          doctests.hs
  ghc-options:      -threaded -Wall -fwarn-tabs
  hs-source-dirs:   tests
  default-language: Haskell2010

  if !flag(doctest)
    buildable: False
  else
    build-depends:
      base,
      filepath,
      directory,
      doctest

--------------------------------------------------------------------------------
-- BENCHMARKS

benchmark bench
  type:             exitcode-stdio-1.0
  main-is:          bench.hs
  ghc-options:      -Wall -fwarn-tabs
  hs-source-dirs:   benchmarks
  default-language: Haskell2010

  build-depends:
    base >=4.8 && <5
