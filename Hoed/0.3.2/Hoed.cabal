name:                Hoed
version:             0.3.2
synopsis:            Lightweight algorithmic debugging.
description:
    Hoed is a tracer and debugger for the programming language Haskell. You can trace a program by annotating functions in suspected modules and linking your program against standard profiling libraries.
    .
    To locate a defect with Hoed you annotate suspected functions and compile as usual. Then you run your program, information about the annotated functions is collected. Finally you connect to a debugging session using a webbrowser.
    .
    Hoed comes in two flavours: Hoed.Pure and Hoed.Stk. Hoed.Pure is recommended over Hoed.Stk: to debug your program with Hoed.Pure you can optimize your program and do not need to enable profiling. Hoed.Stk is Hoed as presented on PLDI 2015 and possibly has benefits over Hoed.Pure for debugging concurrent/parallel programs.
homepage:            http://maartenfaddegon.nl
license:             BSD3
license-file:        LICENSE
author:              Maarten Faddegon
maintainer:          hoed@maartenfaddegon.nl
copyright:           (c) 2000 Andy Gill, (c) 2010 University of Kansas, (c) 2013-2015 Maarten Faddegon
category:            Debug, Trace
build-type:          Simple
cabal-version:       >=1.10
extra-source-files:  changelog, README.md, configure.Demo, configure.Generic, configure.Profiling, configure.Prop, configure.Pure, configure.Stk, run, test.Generic, test.Pure, test.Stk
data-files:          img/*.png, img/*.gif

flag buildExamples
  description: Build example executables.
  default: False

flag validatePure
  description: Build test cases to validate Hoed-pure.
  default: False

flag validateStk
  description: Build test cases to validate Hoed-stk.
  default: False

flag validateGeneric
  description: Build test cases to validate deriving Observable for Generic types.
  default: False

flag validateProp
  description: Build test cases to validate deriving judgements with properties.
  default: False

Source-repository head
    type:               git
    location:           git://github.com/MaartenFaddegon/Hoed.git

library
  exposed-modules:     Debug.Hoed.Stk
                       , Debug.Hoed.Pure
  other-modules:       Debug.Hoed.Stk.Observe
                       , Debug.Hoed.Stk.Render
                       , Debug.Hoed.Stk.DemoGUI
                       , Debug.Hoed.Pure.Observe
                       , Debug.Hoed.Pure.EventForest
                       , Debug.Hoed.Pure.CompTree
                       , Debug.Hoed.Pure.Render
                       , Debug.Hoed.Pure.DemoGUI
                       , Debug.Hoed.Pure.Prop
                       , Paths_Hoed
  build-depends:       base >= 4 && <5
                       , template-haskell
                       , array, containers
                       , process
                       , threepenny-gui == 0.6.*
                       , filepath
                       , libgraph == 1.9
                       , RBTree == 0.0.5
                       , regex-posix
                       , mtl
                       , directory
                       , FPretty
  default-language:    Haskell2010
  -- Enable to get some extra warnings.
  --   ghc-options:         -fwarn-unused-binds

  -- Enable to create a file .Hoed/Transcript that lists all steps taken to construct a
  -- computation tree from the list of events.
  --   cpp-options:         -DTRANSCRIPT

---------------------------------------------------------------------------
--
-- A list of example-programs that bind to a debugging session after the
-- program terminates. After running 'cabal build' these are available to 
-- experiment with through 'sh run'.
--
---------------------------------------------------------------------------

Executable hoed-examples-FPretty_indents_too_much
  if flag(buildExamples)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
                         , containers
                         , deepseq
                         , array
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/FPretty
  default-language:    Haskell2010

Executable hoed-examples-FPretty_indents_too_much__CC
  if flag(buildExamples)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
                         , containers
                         , deepseq
                         , array
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/FPretty__CC
  default-language:    Haskell2010

-- Executable hoed-examples-FPretty_indents_too_much__with_properties
--   if flag(buildExamples)
--     build-depends:       base >= 4 && < 5
--                          , Hoed
--                          , threepenny-gui
--                          , filepath
--                          , containers
--                          , deepseq
--                          , array
--                          , QuickCheck
--                          , mtl
--   else
--     buildable: False
--   main-is:             Main.hs
--   hs-source-dirs:      examples/FPretty__with_properties
--   default-language:    Haskell2010

Executable hoed-examples-Insertion_Sort_elements_disappear
  if flag(buildExamples)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
  else
    buildable: False
  main-is:             Insertion_Sort_elements_disappear.hs
  hs-source-dirs:      examples
  default-language:    Haskell2010

Executable hoed-examples-XMonad_changing_focus_duplicates_windows
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/XMonad_changing_focus_duplicates_windows
  default-language:    Haskell2010

Executable hoed-examples-XMonad_changing_focus_duplicates_windows__CC
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/XMonad_changing_focus_duplicates_windows__CC
  default-language:    Haskell2010

Executable hoed-examples-XMonad_changing_focus_duplicates_windows__with_properties
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/XMonad_changing_focus_duplicates_windows__using_properties
  default-language:    Haskell2010

Executable hoed-examples-SummerSchool_compiler_does_not_terminate
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory,
                       array
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/afp02Exercises/Compiler/
  default-language:    Haskell2010

Executable hoed-examples-SummerSchool_compiler_does_not_terminate__with_properties
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory,
                       array,QuickCheck
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/afp02Exercises/Compiler__with_properties/
  default-language:    Haskell2010

Executable hoed-examples-CNF_unsound_de_Morgan__with_properties
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/CNF_unsound_demorgan__with_properties/
  default-language:    Haskell2010

Executable hoed-examples-Digraph_not_data_invariant__with_properties
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, lazysmallcheck
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      examples/Digraph_not_data_invariant__with_properties/
  default-language:    Haskell2010

Executable hoed-examples-Simple_higher-order_function
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory,
                       array
  else
    buildable: False
  main-is:             SimpleHO.hs
  hs-source-dirs:      examples/
  default-language:    Haskell2010

-- MF TODO: add source to repository and re-enable this again
-- Executable hoed-examples-Exception
--   if flag(buildExamples)
--     build-depends:     base >= 4 && < 5, Hoed, 
--                        X11>=1.5 && < 1.7, mtl, unix,
--                        utf8-string,
--                        extensible-exceptions, random,
--                        containers, filepath, process, directory,
--                        array
--   else
--     buildable: False
--   main-is:             Exception.hs
--   hs-source-dirs:      examples/
--   default-language:    Haskell2010

Executable hoed-examples-Parity_test
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory,
                       array
  else
    buildable: False
  main-is:             Parity.hs
  hs-source-dirs:      examples/
  default-language:    Haskell2010

Executable hoed-examples-Expression_simplifier
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory,
                       array
  else
    buildable: False
  main-is:             Main1.hs
  hs-source-dirs:      examples/ExpressionSimplifier
  default-language:    Haskell2010

Executable hoed-examples-Expression_simplifier__with_properties
  if flag(buildExamples)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory,
                       array
  else
    buildable: False
  main-is:             Main2.hs
  hs-source-dirs:      examples/ExpressionSimplifier
  default-language:    Haskell2010



--      
--      
--      Executable hoed-examples-Foldl
--        if flag(buildExamples)
--          build-depends:       base >= 4.8 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             Foldl.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-HeadOnEmpty1
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             HeadOnEmpty.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-HeadOnEmpty2
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             HeadOnEmpty2.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      -- Executable hoed-examples-IOException
--      --   build-depends:       base >= 4 && < 5
--      --                        , Hoed
--      --                        , threepenny-gui
--      --                        , filepath
--      --                        , hood
--      --   main-is:             IOException.hs
--      --   hs-source-dirs:      examples
--      --   default-language:    Haskell2010
--      --   ghc-options:         -O0
--      
--      Executable hoed-examples-IndirectRecursion
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             IndirectRecursion.lhs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-Pretty
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , array
--        else
--          buildable: False
--        main-is:             Pretty.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-Example1
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             Example1.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      -- Executable hoed-examples-Example2
--      --   build-depends:       base >= 4 && < 5
--      --                        , Hoed
--      --                        , threepenny-gui
--      --                        , filepath
--      --   main-is:             Example2.hs
--      --   hs-source-dirs:      examples
--      --   default-language:    Haskell2010
--      
--      Executable hoed-examples-Example3
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             Example3.lhs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-Example4
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             Example4.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-Insort1
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             Insort.lhs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      Executable hoed-examples-DoublingServer1
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , network
--        else
--          buildable: False
--        main-is:             DoublingServer.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-DoublingServer2
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , network
--        else
--          buildable: False
--        main-is:             DoublingServer2.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-DoublingServer3
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , network
--        else
--          buildable: False
--        main-is:             DoublingServer3.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-DoublingServer4
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , network
--        else
--          buildable: False
--        main-is:             DoublingServer4.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-DoublingServer5
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , network
--        else
--          buildable: False
--        main-is:             DoublingServer5.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-Hashmap
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , array
--        else
--          buildable: False
--        main-is:             Hashmap.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-Responsibility
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--                               , array
--        else
--          buildable: False
--        main-is:             Responsibility.lhs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-TightRope1
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             TightRope.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-TightRope2
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             TightRope2.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-TightRope3
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             TightRope3.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0
--      
--      Executable hoed-examples-AskName
--        if flag(buildExamples)
--          build-depends:       base >= 4 && < 5
--                               , Hoed
--                               , threepenny-gui
--                               , filepath
--        else
--          buildable: False
--        main-is:             AskName.hs
--        hs-source-dirs:      examples
--        default-language:    Haskell2010
--        ghc-options:         -O0

---------------------------------------------------------------------------
--
-- A set of tests that instead of binding to a debugging session write the
-- resulting computation graph to file; with the test.* scripts these are
-- validated against references.
--
---------------------------------------------------------------------------

Executable hoed-tests-Prop-t0
  if flag(validateProp)
    build-depends:     base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      tests/Prop/t0
  default-language:    Haskell2010


Executable hoed-tests-Prop-t1
  if flag(validateProp)
    build-depends:     base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      tests/Prop/t1
  default-language:    Haskell2010

Executable hoed-tests-Prop-t2
  if flag(validateProp)
    build-depends:     base >= 4 && < 5, Hoed, lazysmallcheck
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      tests/Prop/t2
  default-language:    Haskell2010

Executable hoed-tests-Prop-t3
  if flag(validateProp)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      tests/Prop/t3
  default-language:    Haskell2010

Executable hoed-tests-Prop-t4
  if flag(validateProp)
    build-depends:     base >= 4 && < 5, Hoed, 
                       X11>=1.5 && < 1.7, mtl, unix,
                       utf8-string,
                       extensible-exceptions, random,
                       containers, filepath, process, directory
  else
    buildable: False
  main-is:             Main.hs
  hs-source-dirs:      tests/Prop/t4
  default-language:    Haskell2010

-- Executable hoed-tests-Prop-t5
--   if flag(validateProp)
--     build-depends:     base >= 4 && < 5, Hoed, 
--                        X11>=1.5 && < 1.7, mtl, unix,
--                        utf8-string >= 0.3 && < 0.4,
--                        extensible-exceptions, random,
--                        containers, filepath, process, directory
--   else
--     buildable: False
--   main-is:             Main.hs
--   hs-source-dirs:      tests/Prop/t5
--   default-language:    Haskell2010

---------------------------------------------------------------------------

Executable hoed-tests-Generic-r0
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             r0.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

Executable hoed-tests-Generic-t0
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             t0.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

Executable hoed-tests-Generic-r1
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             r1.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

Executable hoed-tests-Generic-t1
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             t1.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

Executable hoed-tests-Generic-r2
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             r2.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

Executable hoed-tests-Generic-t2
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             t2.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

Executable hoed-tests-Generic-r3
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             r3.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

Executable hoed-tests-Generic-t3
  if flag(validateGeneric)
    build-depends:       base >= 4 && < 5
                         , Hoed
  else
    buildable: False
  main-is:             t3.hs
  hs-source-dirs:      tests/Generic
  default-language:    Haskell2010

---------------------------------------------------------------------------

Executable hoed-tests-Pure-t1
  if flag(validatePure)
    build-depends:       base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             t1.hs
  hs-source-dirs:      tests/Pure
  default-language:    Haskell2010

Executable hoed-tests-Pure-t2
  if flag(validatePure)
    build-depends:       base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             t2.hs
  hs-source-dirs:      tests/Pure
  default-language:    Haskell2010

Executable hoed-tests-Pure-t3
  if flag(validatePure)
    build-depends:       base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             t3.hs
  hs-source-dirs:      tests/Pure
  default-language:    Haskell2010

Executable hoed-tests-Pure-t4
  if flag(validatePure)
    build-depends:       base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             t4.hs
  hs-source-dirs:      tests/Pure
  default-language:    Haskell2010

Executable hoed-tests-Pure-t5
  if flag(validatePure)
    build-depends:       base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             t5.hs
  hs-source-dirs:      tests/Pure
  default-language:    Haskell2010

Executable hoed-tests-Pure-t6
  if flag(validatePure)
    build-depends:       base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             t6.hs
  hs-source-dirs:      tests/Pure
  default-language:    Haskell2010

Executable hoed-tests-Pure-t7
  if flag(validatePure)
    build-depends:       base >= 4 && < 5, Hoed
  else
    buildable: False
  main-is:             t7.hs
  hs-source-dirs:      tests/Pure
  default-language:    Haskell2010

---------------------------------------------------------------------------

Executable hoed-tests-Stk-DoublingServer
  if flag(validateStk)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
                         , network
  else
    buildable: False
  main-is:             DoublingServer.hs
  hs-source-dirs:      tests/Stk
  default-language:    Haskell2010
  ghc-options:         -O0

Executable hoed-tests-Stk-Insort2
  if flag(validateStk)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
  else
    buildable: False
  main-is:             Insort2.hs
  hs-source-dirs:      tests/Stk
  default-language:    Haskell2010
  ghc-options:         -O0

Executable hoed-tests-Stk-Example1
  if flag(validateStk)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
  else
    buildable: False
  main-is:             Example1.hs
  hs-source-dirs:      tests/Stk
  default-language:    Haskell2010
  ghc-options:         -O0

Executable hoed-tests-Stk-Example3
  if flag(validateStk)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
  else
    buildable: False
  main-is:             Example3.lhs
  hs-source-dirs:      tests/Stk
  default-language:    Haskell2010
  ghc-options:         -O0

Executable hoed-tests-Stk-Example4
  if flag(validateStk)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
  else
    buildable: False
  main-is:             Example4.hs
  hs-source-dirs:      tests/Stk
  default-language:    Haskell2010
  ghc-options:         -O0

Executable hoed-tests-Stk-IndirectRecursion
  if flag(validateStk)
    build-depends:       base >= 4 && < 5
                         , Hoed
                         , threepenny-gui
                         , filepath
  else
    buildable: False
  main-is:             IndirectRecursion.lhs
  hs-source-dirs:      tests/Stk
  default-language:    Haskell2010
  ghc-options:         -O0
