cabal-version: 1.12

-- This file has been generated from package.yaml by hpack version 0.31.2.
--
-- see: https://github.com/sol/hpack
--
-- hash: bb746d0a792e8c143d590f5127622e58947f2ebf3f393bca193a0d0a34446f72

name:           mpi-hs
version:        0.5.2.0
synopsis:       MPI bindings for Haskell
description:    MPI (the [Message Passing Interface](https://www.mpi-forum.org)) is
                widely used standard for distributed-memory programming on HPC (High
                Performance Computing) systems. MPI allows exchanging data
                (_messages_) between programs running in parallel. There are several
                high-quality open source MPI implementations (e.g. MPICH, MVAPICH,
                OpenMPI) as well as a variety of closed-source implementations.
                These libraries can typically make use of high-bandwidth low-latency
                communication hardware such as InfiniBand.
                .
                This library @mpi-hs@ provides Haskell bindings for MPI. It is based
                on ideas taken from
                [haskell-mpi](https://github.com/bjpop/haskell-mpi),
                [Boost.MPI](https://www.boost.org/doc/libs/1_64_0/doc/html/mpi.html),
                and [MPI for Python](https://mpi4py.readthedocs.io/en/stable/).
                .
                @mpi-hs@ provides two API levels: A low-level API gives rather
                direct access to the MPI API, apart from certain "reasonable"
                mappings from C to Haskell (e.g. output arguments that are in C
                stored to a pointer are in Haskell regular return values). A
                high-level API simplifies exchanging arbitrary values that can be
                serialized.
                .
                Note that the automated builds on
                [Hackage](http://hackage.haskell.org) will currently always fail
                since no MPI library is present there. However, builds on
                [Stackage](https://www.stackage.org) should succeed -- if not, there
                is an error in this package.
category:       Distributed Computing
homepage:       https://github.com/eschnett/mpi-hs#readme
bug-reports:    https://github.com/eschnett/mpi-hs/issues
author:         Erik Schnetter <schnetter@gmail.com>
maintainer:     Erik Schnetter <schnetter@gmail.com>
license:        Apache-2.0
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    LICENSE
    README.md
    package.yaml
    stack.yaml
    c/include/mpihs.h
    c/src/mpihs.c

source-repository head
  type: git
  location: https://github.com/eschnett/mpi-hs

flag mpich-macports
  description: Use MPICH on MacPorts
  manual: True
  default: False

flag mpich-ubuntu
  description: Use MPICH on Ubuntu
  manual: True
  default: False

flag openmpi-debian
  description: Use OpenMPI on Debian
  manual: True
  default: True

flag openmpi-macports
  description: Use OpenMPI on MacPorts
  manual: True
  default: True

flag openmpi-ubuntu
  description: Use OpenMPI on Ubuntu
  manual: True
  default: True

library
  exposed-modules:
      Control.Distributed.MPI
      Control.Distributed.MPI.Binary
      Control.Distributed.MPI.Serialize
      Control.Distributed.MPI.Storable
      Control.Distributed.MPI.Store
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      lib
  ghc-options: -Wall
  include-dirs:
      c/include
  c-sources:
      c/src/mpihs.c
  build-tools:
      c2hs
  build-depends:
      base >=4 && <5
    , binary
    , bytestring
    , cereal
    , monad-loops
    , store
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010

executable example
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      src
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , binary
    , mpi-hs
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010

test-suite mpi-test
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      tests/mpi
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , monad-loops
    , mpi-hs
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010

test-suite mpi-test-binary
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      tests/binary
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , mpi-hs
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010

test-suite mpi-test-serialize
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      tests/serialize
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , mpi-hs
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010

test-suite mpi-test-storable
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      tests/storable
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , mpi-hs
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010

test-suite mpi-test-store
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      tests/store
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , mpi-hs
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010

benchmark mpi-hs-benchmarks
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      bench
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , criterion
    , mpi-hs
  if flag(openmpi-debian)
    include-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/include
    extra-lib-dirs:
        /usr/lib/x86_64-linux-gnu/openmpi/lib
    extra-libraries:
        mpi
  if flag(openmpi-macports)
    include-dirs:
        /opt/local/include/openmpi-mp
    extra-lib-dirs:
        /opt/local/lib/openmpi-mp
    extra-libraries:
        mpi
  if flag(openmpi-ubuntu)
    include-dirs:
        /usr/lib/openmpi/include
    extra-lib-dirs:
        /usr/lib/openmpi/lib
    extra-libraries:
        mpi
  if flag(mpich-macports)
    include-dirs:
        /opt/local/include/mpich-gcc9
    extra-lib-dirs:
        /opt/local/lib/mpich-gcc9
    extra-libraries:
        mpi
  if flag(mpich-ubuntu)
    include-dirs:
        /usr/lib/mpich/include
    extra-lib-dirs:
        /usr/lib/mpich/lib
    extra-libraries:
        mpich
  default-language: Haskell2010
