-- This file has been generated from package.yaml by hpack version 0.28.2.
--
-- see: https://github.com/sol/hpack
--
-- hash: 761b31486948877bf0190f33f0275f4bd5e03c3bc4b5ee323afbe6fe1324f05e

name:           mpi-hs
version:        0.3.1.0
synopsis:       MPI bindings for Haskell
description:    MPI (the [Message Passing Interface](https://www.mpi-forum.org)) is
                widely used standard for distributed-memory programming on HPC (High
                Performance Computing) systems. MPI allows exchanging data
                (_messages_) between programs running in parallel. There are several
                high-quality open source MPI implementations (e.g. MPICH, MVAPICH,
                OpenMPI) as well as a variety of closed-source implementations.
                These libraries can typically make use of high-bandwidth low-latency
                communication hardware such as InfiniBand.
                .
                This library @mpi-hs@ provides Haskell bindings for MPI. It is based
                on ideas taken from
                [haskell-mpi](https://github.com/bjpop/haskell-mpi),
                [Boost.MPI](https://www.boost.org/doc/libs/1_64_0/doc/html/mpi.html),
                and [MPI for Python](https://mpi4py.readthedocs.io/en/stable/).
                .
                @mpi-hs@ provides two API levels: A low-level API gives rather
                direct access to the MPI API, apart from certain "reasonable"
                mappings from C to Haskell (e.g. output arguments that are in C
                stored to a pointer are in Haskell regular return values). A
                high-level API simplifies exchanging arbitrary values that can be
                serialized.
                .
                Note that the automated builds on
                [Hackage](http://hackage.haskell.org) will currently always fail
                since no MPI library is present there. However, builds on
                [Stackage](https://www.stackage.org) should succeed -- if not, there
                is an error in this package.
category:       Distributed Computing
homepage:       https://github.com/eschnett/mpi-hs#readme
bug-reports:    https://github.com/eschnett/mpi-hs/issues
author:         Erik Schnetter <schnetter@gmail.com>
maintainer:     Erik Schnetter <schnetter@gmail.com>
license:        Apache-2.0
license-file:   LICENSE
build-type:     Simple
cabal-version:  >= 1.10
extra-source-files:
    c/include/mpihs.h
    c/src/mpihs.c
    LICENSE
    package.yaml
    README.md
    stack.yaml

source-repository head
  type: git
  location: https://github.com/eschnett/mpi-hs

library
  exposed-modules:
      Control.Distributed.MPI
      Control.Distributed.MPI.Simple
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      lib
  ghc-options: -Wall
  include-dirs:
      c/include
  c-sources:
      c/src/mpihs.c
  extra-libraries:
      mpi
  build-depends:
      base >=4 && <5
    , bytestring
    , monad-loops
    , store
  build-tools:
      c2hs
  default-language: Haskell2010

executable example
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      src
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , mpi-hs
  default-language: Haskell2010

test-suite mpi-simple-tests
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      test/simple
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , monad-loops
    , mpi-hs
  default-language: Haskell2010

test-suite mpi-tests
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      test/mpi
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , monad-loops
    , mpi-hs
  default-language: Haskell2010

benchmark mpi-hs-benchmarks
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      Paths_mpi_hs
  hs-source-dirs:
      bench
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , criterion
    , mpi-hs
  default-language: Haskell2010
