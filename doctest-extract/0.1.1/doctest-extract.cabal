Cabal-Version:  2.2
Name:           doctest-extract
Version:        0.1.1
License:        BSD-3-Clause
License-File:   LICENSE
Author:         Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:     Henning Thielemann <haskell@henning-thielemann.de>
Homepage:       https://hub.darcs.net/thielema/doctest-extract/
Category:       Testing
Synopsis:       Alternative doctest implementation that extracts comments to modules
Description:
  @doctest-extract@ lets you write test examples and QuickCheck properties
  in Haddock comments and extracts them to test modules.
  It means that the user sees your tests in the documentation
  and knows that the examples and properties are machine-tested,
  or at least, she can run the tests herself.
  .
  I found the barrier to write tests much lower
  when I do not need to write new test modules
  but just add some lines to the Haddock comments.
  I do not need to think of test names or filling test data structures.
  The test identifier is the module name and the line number and
  if a test fails I can easily jump to the failing code.
  .
  .
  Differences to the original GHCi-based implementation of @doctest@:
  .
  Pros:
  .
  * Package versions for tests are consistent with tested library
  .
  * Tests run much faster, especially QuickCheck property tests
  .
  * No dependency on GHCi or GHC-as-library
  .
  * The tested package need not be ready for compilation.
    Our simple parser requires only clearly recognizable Haskell comments.
  .
  * QuickCheck properties do not cause confusing type error messages
    when actually only identifiers are missing.
  .
  * You can inspect extracted modules
  .
  * @doctest@ collects tests from the transitive hull of imports
    of the specified modules.
    This might help you to keep the list of modules short.
    @doctest-extract@ only processes the specified modules
    and thus allows you to focus on a module for development of tests.
  .
  * With option @--verbose@ test source path and line number are formatted
    such that Emacs allows you to click and jump to the test definition.
  .
  * Report success only of real tests.
    @doctest@ reports successful imports and
    definition of helper types and functions as successful tests.
    This makes it hard to monitor the number of real tests,
    e.g. whether some tests have been dropped by accident.
  .
  Cons:
  .
  * Cannot test for output of IO functions
    or error messages from partial functions.
  .
  * All free variables in QuickCheck properties
    must be all-quantified using lambda.
    (Could be even seen as an advantage for the reader of your doctests.)
  .
  * No support for a single-line 'let' (as in a 'do'-block) as an example.
  .
  * The Test module does not automatically import modules
    that the tested module imports.
    Thus, you usually have to add a setup section with required imports.
  .
  * You need tools additional to @Cabal@, e.g. @make@ and a @Makefile@,
    in order generate test modules.
  .
  Known Problems:
  .
  * In Literal Haskell files
    only @\\begin{code} ... \\end{code}@ blocks are scanned,
    but not bird style code blocks.
  .
  See packages @utility-ht@, @apportionment@ or @pathtype@ for packages
  with working setups of @doctest-extract@.
  .
  Alternatives: @cabal-docspec@, @cabal-doctest@
Tested-With:    GHC==7.4.2, GHC==8.6.5
Build-Type:     Simple

Source-Repository this
  Tag:         0.1.1
  Type:        darcs
  Location:    https://hub.darcs.net/thielema/doctest-extract/

Source-Repository head
  Type:        darcs
  Location:    https://hub.darcs.net/thielema/doctest-extract/

Executable doctest-extract-0.1
  Build-Depends:
    doctest-lib >=0.0 && <0.2,
    optparse-applicative >=0.11 && <0.19,
    pathtype >=0.8 && <0.9,
    transformers >=0.5.6 && <0.7,
    non-empty >=0.3.3 && <0.4,
    semigroups >=0.18.5 && <0.21,
    utility-ht >=0.0.16 && <0.1,
    base >=4.5 && <5

  GHC-Options:      -Wall
  Hs-Source-Dirs:   src
  Default-Language: Haskell98
  Main-Is: Main.hs
  Other-Modules:
    Format
    Parse
    Option
    ModuleName
