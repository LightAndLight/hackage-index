-- This file has been generated from package.yaml by hpack version 0.28.2.
--
-- see: https://github.com/sol/hpack
--
-- hash: 4f06465ab3fa36c72dff4e9197bc984c95dc787f823d64bf28f26b1a73a33a54

name:           freer-simple
version:        1.2.0.0
synopsis:       Implementation of a friendly effect system for Haskell.
description:    An implementation of an effect system for Haskell (a fork of
                <http://hackage.haskell.org/package/freer-effects freer-effects>), which is
                based on the work of Oleg Kiselyov et al.:
                .
                  * <http://okmij.org/ftp/Haskell/extensible/more.pdf Freer Monads, More Extensible Effects>
                  * <http://okmij.org/ftp/Haskell/zseq.pdf Reflection without Remorse>
                  * <http://okmij.org/ftp/Haskell/extensible/exteff.pdf Extensible Effects>
                .
                The key features are:
                .
                  * An efficient effect system for Haskell - as a library!
                  * Reimplementations of several common Haskell monad transformers as effects.
                  * Core components for defining your own Effects.
category:       Control
homepage:       https://github.com/lexi-lambda/freer-simple#readme
bug-reports:    https://github.com/lexi-lambda/freer-simple/issues
author:         Allele Dev, Ixcom Core Team, Alexis King, and other contributors
maintainer:     Alexis King <lexi.lambda@gmail.com>
copyright:      (c) 2016 Allele Dev; 2017 Ixperta Solutions s.r.o.; 2017 Alexis King
license:        BSD3
license-file:   LICENSE
build-type:     Simple
cabal-version:  >= 1.10
extra-source-files:
    CHANGELOG.md
    README.md

source-repository head
  type: git
  location: https://github.com/lexi-lambda/freer-simple

library
  exposed-modules:
      Control.Monad.Freer
      Control.Monad.Freer.Coroutine
      Control.Monad.Freer.Error
      Control.Monad.Freer.Fresh
      Control.Monad.Freer.Internal
      Control.Monad.Freer.NonDet
      Control.Monad.Freer.Reader
      Control.Monad.Freer.State
      Control.Monad.Freer.TH
      Control.Monad.Freer.Trace
      Control.Monad.Freer.Writer
      Data.FTCQueue
      Data.OpenUnion
      Data.OpenUnion.Internal
  other-modules:
      Paths_freer_simple
  hs-source-dirs:
      src
  default-extensions: ConstraintKinds DataKinds DeriveFunctor FlexibleContexts FlexibleInstances FunctionalDependencies GADTs LambdaCase MultiParamTypeClasses RankNTypes ScopedTypeVariables TypeApplications TypeOperators
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints
  build-depends:
      base >=4.9 && <5
    , natural-transformation >=0.2
    , template-haskell >=2.11 && <2.15
    , transformers-base
  default-language: Haskell2010

executable freer-examples
  main-is: Main.hs
  other-modules:
      Capitalize
      Console
      Coroutine
      Fresh
      Trace
      Paths_freer_simple
  hs-source-dirs:
      examples/src
  default-extensions: ConstraintKinds DataKinds DeriveFunctor FlexibleContexts FlexibleInstances FunctionalDependencies GADTs LambdaCase MultiParamTypeClasses RankNTypes ScopedTypeVariables TypeApplications TypeOperators
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints
  build-depends:
      base >=4.9 && <5
    , freer-simple
  default-language: Haskell2010

test-suite unit
  type: exitcode-stdio-1.0
  main-is: Tests.hs
  other-modules:
      Tests.Coroutine
      Tests.Exception
      Tests.Fresh
      Tests.Loop
      Tests.NonDet
      Tests.Reader
      Tests.State
      Tests.TH
      Paths_freer_simple
  hs-source-dirs:
      tests
  default-extensions: ConstraintKinds DataKinds DeriveFunctor FlexibleContexts FlexibleInstances FunctionalDependencies GADTs LambdaCase MultiParamTypeClasses RankNTypes ScopedTypeVariables TypeApplications TypeOperators
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints
  build-depends:
      QuickCheck
    , base >=4.9 && <5
    , freer-simple
    , tasty
    , tasty-hunit
    , tasty-quickcheck
  default-language: Haskell2010

benchmark core
  type: exitcode-stdio-1.0
  main-is: Core.hs
  other-modules:
      Paths_freer_simple
  hs-source-dirs:
      bench
  default-extensions: ConstraintKinds DataKinds DeriveFunctor FlexibleContexts FlexibleInstances FunctionalDependencies GADTs LambdaCase MultiParamTypeClasses RankNTypes ScopedTypeVariables TypeApplications TypeOperators
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -O2
  build-depends:
      base >=4.9 && <5
    , criterion
    , extensible-effects
    , free
    , freer-simple
    , mtl
  default-language: Haskell2010
