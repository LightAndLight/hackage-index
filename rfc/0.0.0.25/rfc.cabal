name:                rfc
version:             0.0.0.25
synopsis:            Robert Fischer's Common library
description:         An enhanced Prelude and various utilities for Aeson, Servant, PSQL, and Redis that Robert Fischer uses.
homepage:            https://github.com/RobertFischer/rfc#README.md
license:             BSD3
license-file:        LICENSE
author:              Robert Fischer
maintainer:          smokejumperit+rfc@gmail.com
copyright:           (c)2018 Robert Fischer
category:            Library
build-type:          Simple
extra-source-files:  README.md
cabal-version:       >=1.10


Flag Development
  Description: Turn on errors for warnings
  Default:     False
  Manual:      True

library
  hs-source-dirs:      src
  build-depends:       base >= 4.7 && < 5
  default-language:    Haskell2010
  default-extensions:  RebindableSyntax
                     , MonadFailDesugaring
                     , MultiParamTypeClasses
  ghc-options:         -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -fno-warn-orphans
  ghc-options:         -fno-max-relevant-binds -fprint-potential-instances -Wno-missing-monadfail-instances
  ghc-options:         -fno-warn-name-shadowing -fno-warn-tabs -Wnoncanonical-monad-instances -Wall-missed-specialisations
  ghc-options:         -Wnoncanonical-monadfail-instances -Wimplicit-prelude -Wmissing-exported-signatures -Widentities
  if flag(Development)
    ghc-options:       -Werror -dcore-lint -dcmm-lint
  build-depends:       base >= 4.7 && < 5
                     , bytestring >= 0.10.8.1
                     , classy-prelude >= 1.4
                     , uuid-types
                     , containers
                     , unordered-containers
                     , text-conversions >= 0.3.0
                     , data-default
                     , lens >= 4.16
                     , http-types
                     , http-api-data >= 0.3.7.1
                     , time-units
                     , text
                     , lifted-async >= 0.9.3.3
                     , unliftio >= 0.2.4.0
                     , unliftio-core >= 0.1.1.0
                     , monad-control >= 1.0.2.2
                     , natural-transformation >= 0.4
                     , network-uri
                     , attoparsec
                     , resource-pool
                     , time
                     , aeson-diff >= 1.1.0.4
                     , mtl
                     , miso >= 0.14.0.0
                     , url
                     , mono-traversable >= 1.0.1.1
  if impl(ghcjs)
    build-depends:     aeson
                     , bifunctors
                       -- Servant 0.13 publishes non-GHCJS-compatible Aeson version requirement.
                     , servant >= 0.12.1 && < 0.13
  else
    build-depends:     servant-server >= 0.13
                     , servant >= 0.13
                     , servant-docs >= 0.11.2
                     , servant-blaze >= 0.8
                     , blaze-html
                     , wai >= 3.2.1.1
                     , aeson >= 1.2.3.0
                     , wai-extra
                     , wai-cors
                     , wai-cli >= 0.1.1
                     , envy >= 1.5.0.0
                     , postgresql-typed >= 0.5.2
                     , hedis >= 0.10.0
                     , simple-logger
                     , temporary
                     , http-client
                     , http-client-tls
                     , wreq >= 0.5.2.0
                     , servant-swagger >= 1.1.5
                     , swagger2 >= 2.2
                     , markdown
                     , servant-client >= 0.13
                     , binary
                     , network
                     , warp >= 3.2.18
                     , mtl >= 2.2.2
  exposed-modules:     RFC.Prelude
                     , RFC.String
                     , RFC.Concurrent
                     , RFC.Throttle
                     , RFC.JSON
                     , RFC.API
                     , RFC.HTTP.Types
                     , RFC.Data.LatLng
                     , RFC.Data.IdAnd
                     , RFC.Data.ListMoveDirection
                     , RFC.Data.UUID
                     , RFC.Miso.String
                     -- , RFC.Miso.Component
                     -- , RFC.Miso.Routing
                     -- , RFC.Miso.MisoApp
  if impl(ghcjs)
    cpp-options:       -DGHCJS -DCLIENT -DGHCJS_BROWSER -DBROWSER
    exposed-modules:   RFC.GHCJS
                     , RFC.GHCJS.XHR
                     , RFC.GHCJS.Inject
                     , RFC.GHCJS.Cookie
  else
    exposed-modules: RFC.Log
                     , RFC.Psql
                     , RFC.Redis
                     , RFC.Wai
                     , RFC.Env
                     , RFC.HTTP.Client
                     , RFC.Servant
                     , RFC.Servant.ApiDoc
                     -- , RFC.Client.Coinhive


source-repository head
  type:     git
  location: https://github.com/RobertFischer/rfc
