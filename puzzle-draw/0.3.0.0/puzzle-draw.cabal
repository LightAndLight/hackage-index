cabal-version: >=1.8
name: puzzle-draw
version: 0.3.0.0
license: MIT
license-file: LICENSE
maintainer: rob@vllmrt.net
author: Robert Vollmert
synopsis: Creating graphics for pencil puzzles.
description:
    puzzle-draw is a library and tool for drawing pencil
    puzzles using Diagrams. It aims to provide a utility
    layer on top of Diagrams to help with drawing arbitrary
    puzzles, and to support several specific puzzles types
    In addition, it includes functionality for parsing
    puzzle data from a YAML file format.
category: Graphics
build-type: Simple
data-files:
    data/fonts/*.svg
extra-source-files:
    README.md
    CHANGES.md

source-repository head
    type: git
    location: http://github.com/robx/puzzle-draw.git

library
    exposed-modules:
        Data.Grid
        Data.GridShape
        Data.Util
        Data.Elements
        Data.Pyramid
        Data.Sudoku
        Data.Compose
        Data.PuzzleTypes
        Data.Code
        Data.CmdLine
        Parse.Util
        Parse.Parsec
        Parse.Puzzle
        Parse.PuzzleTypes
        Parse.Code
        Draw.Style
        Draw.Grid
        Draw.Lib
        Draw.Elements
        Draw.Widths
        Draw.Pyramid
        Draw.PuzzleGrids
        Draw.PuzzleTypes
        Draw.Draw
        Draw.Code
        Draw.CmdLine
    hs-source-dirs: src
    other-modules:
        Paths_puzzle_draw
    ghc-options: -Wall
    build-depends:
        base >=4.8 && <5,
        diagrams-lib >=1.4.2.3 && <1.5,
        parsec ==3.1.*,
        yaml >=0.8.4 && <0.9,
        aeson >=0.7 && <1.4,
        unordered-containers ==0.2.*,
        containers ==0.5.*,
        hashable ==1.2.*,
        text >=1.1 && <1.3,
        SVGFonts ==1.7.*,
        vector-space >=0.8 && <0.14,
        mtl >=2.1 && <2.3,
        optparse-applicative >=0.13 && <0.15,
        filepath >=1.3 && <1.5,
        linear >=1.20.8 && <1.21,
        diagrams-rasterific >=1.4.1 && <1.5

executable drawpuzzle
    main-is: drawpuzzle.hs
    hs-source-dirs: src/tools
    ghc-options: -Wall
    build-depends:
        base >=4.8 && <5,
        puzzle-draw -any,
        diagrams-lib >=1.4.2.3 && <1.5,
        yaml >=0.8.4 && <0.9,
        optparse-applicative >=0.12 && <0.15,
        aeson >=0.7 && <1.4,
        filepath >=1.3 && <1.5,
        diagrams-rasterific >=1.4.1 && <1.5

executable checkpuzzle
    main-is: checkpuzzle.hs
    hs-source-dirs: src/tools
    ghc-options: -Wall
    build-depends:
        base >=4.8 && <5,
        puzzle-draw -any,
        yaml >=0.8.4 && <0.9,
        containers ==0.5.*,
        optparse-applicative >=0.12 && <0.15,
        aeson >=0.7 && <1.4,
        filepath >=1.3 && <1.5

test-suite test
    type: exitcode-stdio-1.0
    main-is: tests.hs
    hs-source-dirs: tests
    other-modules:
        Data
        Util
        Data.GridSpec
        Data.GridShapeSpec
        Draw.GridSpec
        Parse.PuzzleTypesSpec
        Parse.UtilSpec
    ghc-options: -Wall
    build-depends:
        base >=4.8 && <5,
        tasty >=0.8 && <1.2,
        tasty-hspec >=1.1.5 && <1.2,
        tasty-hunit >=0.8 && <0.11,
        hspec >=2.5.4 && <2.6,
        yaml >=0.8.4 && <0.9,
        text >=1.1 && <1.3,
        deepseq >=1.3 && <1.5,
        containers ==0.5.*,
        blaze-svg ==0.3.*,
        diagrams-lib >=1.4.2.3 && <1.5,
        bytestring ==0.10.*,
        puzzle-draw -any
