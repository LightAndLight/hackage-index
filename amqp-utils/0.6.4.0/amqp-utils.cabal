-- SPDX-FileCopyrightText: 2022 Frank Doepper
--
-- SPDX-License-Identifier: GPL-3.0-only

name:                amqp-utils

version:             0.6.4.0

synopsis:            AMQP toolset for the command line

description:         AMQP tools consisting of:
  AMQP consumer which can
  create a temporary queue and attach it to an exchange, or
  attach to an existing queue;
  display header and body info;
  save message bodies to files;
  call a callback script.
  AMQP publisher with file, line-by-line and
  hotfolder capabilities.
  AMQP rpc client and server.

license:             GPL-3

license-file:        LICENSES/GPL-3.0-only.txt

author:              Frank Doepper

maintainer:          fd@taz.de

category:            Network

build-type:          Simple

extra-source-files:  ChangeLog.md, README.md

cabal-version:       >=1.10

Tested-With: GHC ==7.10.2 || ==8.0.2 || ==8.2.2 || ==8.4.4 || ==8.6.5 || >=8.8.4

executable konsum
  main-is:             konsum.hs
  build-depends:       base >= 4.6 && <5,
                       containers,
                       text,
                       crypton-connection,
                       data-default-class,
                       time,
                       process,
                       directory,
                       bytestring,
                       utf8-string,
                       filepath-bytestring,
                       crypton-x509-system,
                       network > 2.6,
                       tls >= 1.7.0,
                       amqp >= 0.22.2,
                       unix >= 2.8

  ghc-options:         -threaded -Wall

  default-language:    Haskell2010

  other-modules:       Network.AMQP.Utils.Options,
                       Network.AMQP.Utils.Helpers,
                       Network.AMQP.Utils.Connection,
                       Paths_amqp_utils

executable agitprop
  main-is:             agitprop.hs
  build-depends:       base >= 4.6 && <5,
                       containers,
                       text,
                       crypton-connection,
                       data-default-class,
                       time,
                       process,
                       directory,
                       filepath,
                       bytestring,
                       utf8-string,
                       rawfilepath,
                       filepath-bytestring,
                       crypton-x509-system,
                       network > 2.6,
                       tls >= 1.7.0,
                       amqp >= 0.22.2,
                       unix >= 2.8,
                       magic
  if os(linux)
    build-depends:     hinotify >= 0.3.10

  ghc-options:         -threaded -Wall

  default-language:    Haskell2010

  other-modules:       Network.AMQP.Utils.Options,
                       Network.AMQP.Utils.Helpers,
                       Network.AMQP.Utils.Connection,
                       Paths_amqp_utils

executable plane
  main-is:             plane.hs
  build-depends:       base >=4.6 && <5,
                       containers,
                       text,
                       crypton-connection,
                       data-default-class,
                       time,
                       process,
                       directory,
                       bytestring,
                       utf8-string,
                       filepath-bytestring,
                       crypton-x509-system,
                       network > 2.6,
                       tls >= 1.7.0,
                       amqp >=0.22.2,
                       unix >= 2.8

  ghc-options:         -threaded -Wall

  default-language:    Haskell2010

  other-modules:       Network.AMQP.Utils.Options,
                       Network.AMQP.Utils.Helpers,
                       Network.AMQP.Utils.Connection,
                       Paths_amqp_utils

executable arbeite
  main-is:             arbeite.hs
  build-depends:       base >= 4.6 && <5,
                       containers,
                       text,
                       crypton-connection,
                       data-default-class,
                       time,
                       process,
                       directory,
                       bytestring,
                       utf8-string,
                       filepath-bytestring,
                       crypton-x509-system,
                       network > 2.6,
                       tls >= 1.7.0,
                       amqp >= 0.22.2,
                       unix >= 2.8

  ghc-options:         -threaded -Wall

  default-language:    Haskell2010

  other-modules:       Network.AMQP.Utils.Options,
                       Network.AMQP.Utils.Helpers,
                       Network.AMQP.Utils.Connection,
                       Paths_amqp_utils

source-repository head
  type:                git
  location:            git://woffs.de/git/fd/haskell-amqp-utils.git
