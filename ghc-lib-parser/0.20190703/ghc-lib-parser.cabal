cabal-version: >=1.22
build-type: Simple
name: ghc-lib-parser
version: 0.20190703
license: BSD3
license-file: LICENSE
category: Development
author: The GHC Team and Digital Asset
maintainer: Digital Asset
synopsis: The GHC API, decoupled from GHC versions
description: A package equivalent to the @ghc@ package, but which can be loaded on many compiler versions.
homepage: https://github.com/digital-asset/ghc-lib
bug-reports: https://github.com/digital-asset/ghc-lib/issues
data-dir: ghc-lib/stage1/lib
data-files:
    settings
    llvm-targets
    llvm-passes
    platformConstants
extra-source-files:
    ghc-lib/generated/ghcautoconf.h
    ghc-lib/generated/ghcplatform.h
    ghc-lib/generated/ghcversion.h
    ghc-lib/generated/DerivedConstants.h
    ghc-lib/generated/GHCConstantsHaskellExports.hs
    ghc-lib/generated/GHCConstantsHaskellType.hs
    ghc-lib/generated/GHCConstantsHaskellWrappers.hs
    ghc-lib/stage1/compiler/build/ghc_boot_platform.h
    ghc-lib/stage1/compiler/build/primop-can-fail.hs-incl
    ghc-lib/stage1/compiler/build/primop-code-size.hs-incl
    ghc-lib/stage1/compiler/build/primop-commutable.hs-incl
    ghc-lib/stage1/compiler/build/primop-data-decl.hs-incl
    ghc-lib/stage1/compiler/build/primop-fixity.hs-incl
    ghc-lib/stage1/compiler/build/primop-has-side-effects.hs-incl
    ghc-lib/stage1/compiler/build/primop-list.hs-incl
    ghc-lib/stage1/compiler/build/primop-out-of-line.hs-incl
    ghc-lib/stage1/compiler/build/primop-primop-info.hs-incl
    ghc-lib/stage1/compiler/build/primop-strictness.hs-incl
    ghc-lib/stage1/compiler/build/primop-tag.hs-incl
    ghc-lib/stage1/compiler/build/primop-vector-tycons.hs-incl
    ghc-lib/stage1/compiler/build/primop-vector-tys-exports.hs-incl
    ghc-lib/stage1/compiler/build/primop-vector-tys.hs-incl
    ghc-lib/stage1/compiler/build/primop-vector-uniques.hs-incl
    ghc-lib/stage0/compiler/build/Fingerprint.hs
    ghc-lib/stage1/compiler/build/Config.hs
    ghc-lib/stage0/compiler/build/Parser.hs
    ghc-lib/stage0/compiler/build/Lexer.hs
    includes/*.h
    includes/CodeGen.Platform.hs
    includes/rts/*.h
    includes/rts/storage/*.h
    includes/rts/prof/*.h
    compiler/nativeGen/*.h
    compiler/utils/*.h
    compiler/*.h
tested-with: GHC==8.6.3, GHC==8.4.3
source-repository head
    type: git
    location: git@github.com:digital-asset/ghc-lib.git

library
    default-language:   Haskell2010
    default-extensions: NoImplicitPrelude
    include-dirs:
        ghc-lib/generated
        ghc-lib/stage0/compiler/build
        ghc-lib/stage1/compiler/build
        compiler
        compiler/utils
    ghc-options: -fobject-code -package=ghc-boot-th -optc-DTHREADED_RTS
    cc-options: -DTHREADED_RTS
    cpp-options: -DSTAGE=2 -DTHREADED_RTS -DHAVE_INTERPRETER -DGHC_IN_GHCI
    if !os(windows)
        build-depends: unix
    else
        build-depends: Win32
    build-depends:
        ghc-prim > 0.2 && < 0.6,
        base >= 4.11 && < 4.14,
        containers >= 0.5 && < 0.7,
        bytestring >= 0.9 && < 0.11,
        binary == 0.8.*,
        filepath >= 1 && < 1.5,
        directory >= 1 && < 1.4,
        array >= 0.1 && < 0.6,
        deepseq >= 1.4 && < 1.5,
        pretty == 1.1.*,
        time >= 1.4 && < 1.10,
        transformers == 0.5.*,
        process >= 1 && < 1.7,
        hpc == 0.6.*
    build-tools: alex >= 3.1, happy >= 1.19.4
    other-extensions:
        BangPatterns
        CPP
        DataKinds
        DefaultSignatures
        DeriveDataTypeable
        DeriveFoldable
        DeriveFunctor
        DeriveGeneric
        DeriveTraversable
        DisambiguateRecordFields
        ExistentialQuantification
        ExplicitForAll
        FlexibleContexts
        FlexibleInstances
        GADTs
        GeneralizedNewtypeDeriving
        InstanceSigs
        MagicHash
        MultiParamTypeClasses
        NamedFieldPuns
        NondecreasingIndentation
        RankNTypes
        RecordWildCards
        RoleAnnotations
        ScopedTypeVariables
        StandaloneDeriving
        Trustworthy
        TupleSections
        TypeFamilies
        TypeSynonymInstances
        UnboxedTuples
        UndecidableInstances
    c-sources:
        compiler/cbits/genSym.c
        compiler/parser/cutils.c
    hs-source-dirs:
        ghc-lib/stage0/libraries/ghc-heap/build
        ghc-lib/stage0/libraries/ghci/build
        ghc-lib/stage0/compiler/build
        ghc-lib/stage1/compiler/build
        libraries/template-haskell
        libraries/ghc-boot-th
        compiler/basicTypes
        compiler/specialise
        libraries/ghc-heap
        libraries/ghc-boot
        compiler/nativeGen
        compiler/profiling
        compiler/simplCore
        compiler/typecheck
        compiler/backpack
        compiler/simplStg
        compiler/coreSyn
        compiler/deSugar
        compiler/prelude
        compiler/parser
        libraries/ghci
        compiler/hsSyn
        compiler/iface
        compiler/types
        compiler/utils
        compiler/ghci
        compiler/main
        compiler/cmm
        compiler
    autogen-modules:
        Lexer
        Parser
    exposed-modules:
        Annotations
        ApiAnnotation
        Avail
        Bag
        BasicTypes
        BinFingerprint
        Binary
        BkpSyn
        BooleanFormula
        BufWrite
        ByteCodeTypes
        Class
        CliOption
        CmdLineParser
        CmmType
        CoAxiom
        Coercion
        ConLike
        Config
        Constants
        CoreArity
        CoreFVs
        CoreMap
        CoreMonad
        CoreOpt
        CoreSeq
        CoreStats
        CoreSubst
        CoreSyn
        CoreTidy
        CoreUnfold
        CoreUtils
        CostCentre
        CostCentreState
        Ctype
        DataCon
        Demand
        Digraph
        DriverPhases
        DynFlags
        Encoding
        EnumSet
        ErrUtils
        Exception
        FV
        FamInstEnv
        FastFunctions
        FastMutInt
        FastString
        FastStringEnv
        FieldLabel
        FileCleanup
        FileSettings
        Fingerprint
        FiniteMap
        ForeignCall
        GHC.BaseDir
        GHC.Exts.Heap
        GHC.Exts.Heap.ClosureTypes
        GHC.Exts.Heap.Closures
        GHC.Exts.Heap.Constants
        GHC.Exts.Heap.InfoTable
        GHC.Exts.Heap.InfoTable.Types
        GHC.Exts.Heap.InfoTableProf
        GHC.Exts.Heap.Utils
        GHC.ForeignSrcLang
        GHC.ForeignSrcLang.Type
        GHC.LanguageExtensions
        GHC.LanguageExtensions.Type
        GHC.Lexeme
        GHC.PackageDb
        GHC.Platform
        GHC.Serialized
        GHCi.BreakArray
        GHCi.FFI
        GHCi.Message
        GHCi.RemoteTypes
        GHCi.TH.Binary
        GhcMonad
        GhcNameVersion
        GhcPrelude
        HaddockUtils
        HeaderInfo
        Hooks
        HsBinds
        HsDecls
        HsDoc
        HsExpr
        HsExtension
        HsImpExp
        HsInstances
        HsLit
        HsPat
        HsSyn
        HsTypes
        HsUtils
        HscTypes
        IOEnv
        Id
        IdInfo
        IfaceSyn
        IfaceType
        InstEnv
        InteractiveEvalTypes
        Json
        Kind
        KnownUniques
        Language.Haskell.TH
        Language.Haskell.TH.LanguageExtensions
        Language.Haskell.TH.Lib
        Language.Haskell.TH.Lib.Internal
        Language.Haskell.TH.Lib.Map
        Language.Haskell.TH.Ppr
        Language.Haskell.TH.PprLib
        Language.Haskell.TH.Syntax
        Lexeme
        Lexer
        LinkerTypes
        ListSetOps
        Literal
        Maybes
        MkCore
        MkId
        Module
        MonadUtils
        Name
        NameCache
        NameEnv
        NameSet
        OccName
        OccurAnal
        OptCoercion
        OrdList
        Outputable
        PackageConfig
        Packages
        Pair
        Panic
        Parser
        PatSyn
        PipelineMonad
        PlaceHolder
        PlainPanic
        PlatformConstants
        Plugins
        PmExpr
        PprColour
        PprCore
        PrelNames
        PrelRules
        Pretty
        PrimOp
        RdrHsSyn
        RdrName
        RepType
        Rules
        Settings
        SizedSeq
        SrcLoc
        StringBuffer
        SysTools.BaseDir
        SysTools.Terminal
        TcEvidence
        TcHoleFitTypes
        TcRnTypes
        TcType
        ToIface
        ToolSettings
        TrieMap
        TyCoRep
        TyCon
        Type
        TysPrim
        TysWiredIn
        Unify
        UniqDFM
        UniqDSet
        UniqFM
        UniqSet
        UniqSupply
        Unique
        Util
        Var
        VarEnv
        VarSet
