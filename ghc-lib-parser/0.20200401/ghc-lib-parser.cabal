cabal-version: >=1.22
build-type: Simple
name: ghc-lib-parser
version: 0.20200401
license: BSD3
license-file: LICENSE
category: Development
author: The GHC Team and Digital Asset
maintainer: Digital Asset
synopsis: The GHC API, decoupled from GHC versions
description: A package equivalent to the @ghc@ package, but which can be loaded on many compiler versions.
homepage: https://github.com/digital-asset/ghc-lib
bug-reports: https://github.com/digital-asset/ghc-lib/issues
data-dir: ghc-lib/stage0/lib
data-files:
    settings
    llvm-targets
    llvm-passes
    platformConstants
extra-source-files:
    ghc-lib/stage0/lib/ghcautoconf.h
    ghc-lib/stage0/lib/ghcplatform.h
    ghc-lib/stage0/lib/ghcversion.h
    ghc-lib/stage0/lib/DerivedConstants.h
    ghc-lib/stage0/lib/GHCConstantsHaskellExports.hs
    ghc-lib/stage0/lib/GHCConstantsHaskellType.hs
    ghc-lib/stage0/lib/GHCConstantsHaskellWrappers.hs
    ghc-lib/stage0/compiler/build/primop-can-fail.hs-incl
    ghc-lib/stage0/compiler/build/primop-code-size.hs-incl
    ghc-lib/stage0/compiler/build/primop-commutable.hs-incl
    ghc-lib/stage0/compiler/build/primop-data-decl.hs-incl
    ghc-lib/stage0/compiler/build/primop-fixity.hs-incl
    ghc-lib/stage0/compiler/build/primop-has-side-effects.hs-incl
    ghc-lib/stage0/compiler/build/primop-list.hs-incl
    ghc-lib/stage0/compiler/build/primop-out-of-line.hs-incl
    ghc-lib/stage0/compiler/build/primop-primop-info.hs-incl
    ghc-lib/stage0/compiler/build/primop-strictness.hs-incl
    ghc-lib/stage0/compiler/build/primop-tag.hs-incl
    ghc-lib/stage0/compiler/build/primop-vector-tycons.hs-incl
    ghc-lib/stage0/compiler/build/primop-vector-tys-exports.hs-incl
    ghc-lib/stage0/compiler/build/primop-vector-tys.hs-incl
    ghc-lib/stage0/compiler/build/primop-vector-uniques.hs-incl
    ghc-lib/stage0/compiler/build/Config.hs
    ghc-lib/stage0/libraries/ghc-boot/build/GHC/Version.hs
    ghc-lib/stage0/compiler/build/Parser.hs
    ghc-lib/stage0/compiler/build/Lexer.hs
    includes/ghcconfig.h
    includes/MachDeps.h
    includes/stg/MachRegs.h
    includes/CodeGen.Platform.hs
    compiler/HsVersions.h
    compiler/Unique.h
tested-with: GHC==8.8.2, GHC==8.6.5, GHC==8.4.4
source-repository head
    type: git
    location: git@github.com:digital-asset/ghc-lib.git

library
    default-language:   Haskell2010
    default-extensions: NoImplicitPrelude
    exposed: False
    include-dirs:
        includes
        ghc-lib/stage0/lib
        ghc-lib/stage0/compiler/build
        compiler
    ghc-options: -fobject-code -package=ghc-boot-th -optc-DTHREADED_RTS
    cc-options: -DTHREADED_RTS
    cpp-options:  -DTHREADED_RTS  -DGHC_IN_GHCI
    if !os(windows)
        build-depends: unix
    else
        build-depends: Win32
    build-depends:
        ghc-prim > 0.2 && < 0.6,
        base >= 4.11 && < 4.14,
        containers >= 0.5 && < 0.7,
        bytestring >= 0.9 && < 0.11,
        binary == 0.8.*,
        filepath >= 1 && < 1.5,
        directory >= 1 && < 1.4,
        array >= 0.1 && < 0.6,
        deepseq >= 1.4 && < 1.5,
        pretty == 1.1.*,
        time >= 1.4 && < 1.10,
        transformers == 0.5.*,
        process >= 1 && < 1.7,
        hpc == 0.6.*
    build-tools: alex >= 3.1, happy >= 1.19.4
    other-extensions:
        BangPatterns
        CPP
        DataKinds
        DefaultSignatures
        DeriveDataTypeable
        DeriveFoldable
        DeriveFunctor
        DeriveGeneric
        DeriveTraversable
        DisambiguateRecordFields
        ExistentialQuantification
        ExplicitForAll
        FlexibleContexts
        FlexibleInstances
        GADTs
        GeneralizedNewtypeDeriving
        InstanceSigs
        MagicHash
        MultiParamTypeClasses
        NamedFieldPuns
        NondecreasingIndentation
        RankNTypes
        RecordWildCards
        RoleAnnotations
        ScopedTypeVariables
        StandaloneDeriving
        Trustworthy
        TupleSections
        TypeFamilies
        TypeSynonymInstances
        UnboxedTuples
        UndecidableInstances
    c-sources:
        libraries/ghc-heap/cbits/HeapPrim.cmm
        compiler/cbits/genSym.c
        compiler/cbits/cutils.c
    hs-source-dirs:
        ghc-lib/stage0/libraries/ghc-boot/build
        ghc-lib/stage0/compiler/build
        libraries/template-haskell
        libraries/ghc-boot-th
        compiler/typecheck
        libraries/ghc-boot
        libraries/ghc-heap
        compiler/prelude
        compiler/parser
        compiler/iface
        compiler/utils
        libraries/ghci
        compiler/main
        compiler/.
        compiler
    autogen-modules:
        Lexer
        Parser
    exposed-modules:
        ApiAnnotation
        Bag
        BinFingerprint
        Binary
        BooleanFormula
        BufWrite
        CliOption
        Config
        Constants
        Constraint
        Ctype
        Digraph
        Encoding
        EnumSet
        ErrUtils
        Exception
        FV
        FastFunctions
        FastMutInt
        FastString
        FastStringEnv
        FileCleanup
        FileSettings
        Fingerprint
        FiniteMap
        GHC.BaseDir
        GHC.ByteCode.Types
        GHC.Cmm
        GHC.Cmm.BlockId
        GHC.Cmm.CLabel
        GHC.Cmm.Dataflow.Block
        GHC.Cmm.Dataflow.Collections
        GHC.Cmm.Dataflow.Graph
        GHC.Cmm.Dataflow.Label
        GHC.Cmm.Expr
        GHC.Cmm.MachOp
        GHC.Cmm.Node
        GHC.Cmm.Switch
        GHC.Cmm.Type
        GHC.Core
        GHC.Core.Arity
        GHC.Core.Class
        GHC.Core.Coercion
        GHC.Core.Coercion.Axiom
        GHC.Core.Coercion.Opt
        GHC.Core.ConLike
        GHC.Core.DataCon
        GHC.Core.FVs
        GHC.Core.FamInstEnv
        GHC.Core.InstEnv
        GHC.Core.Make
        GHC.Core.Map
        GHC.Core.Op.ConstantFold
        GHC.Core.Op.Monad
        GHC.Core.Op.OccurAnal
        GHC.Core.Op.Tidy
        GHC.Core.PatSyn
        GHC.Core.Ppr
        GHC.Core.Predicate
        GHC.Core.Rules
        GHC.Core.Seq
        GHC.Core.SimpleOpt
        GHC.Core.Stats
        GHC.Core.Subst
        GHC.Core.TyCo.FVs
        GHC.Core.TyCo.Ppr
        GHC.Core.TyCo.Rep
        GHC.Core.TyCo.Subst
        GHC.Core.TyCo.Tidy
        GHC.Core.TyCon
        GHC.Core.Type
        GHC.Core.Unfold
        GHC.Core.Unify
        GHC.Core.Utils
        GHC.CoreToIface
        GHC.Driver.Backpack.Syntax
        GHC.Driver.CmdLine
        GHC.Driver.Flags
        GHC.Driver.Hooks
        GHC.Driver.Monad
        GHC.Driver.Packages
        GHC.Driver.Phases
        GHC.Driver.Pipeline.Monad
        GHC.Driver.Plugins
        GHC.Driver.Session
        GHC.Driver.Types
        GHC.Driver.Ways
        GHC.Exts.Heap
        GHC.Exts.Heap.ClosureTypes
        GHC.Exts.Heap.Closures
        GHC.Exts.Heap.Constants
        GHC.Exts.Heap.InfoTable
        GHC.Exts.Heap.InfoTable.Types
        GHC.Exts.Heap.InfoTableProf
        GHC.Exts.Heap.Utils
        GHC.ForeignSrcLang
        GHC.ForeignSrcLang.Type
        GHC.Hs
        GHC.Hs.Binds
        GHC.Hs.Decls
        GHC.Hs.Doc
        GHC.Hs.Dump
        GHC.Hs.Expr
        GHC.Hs.Extension
        GHC.Hs.ImpExp
        GHC.Hs.Instances
        GHC.Hs.Lit
        GHC.Hs.Pat
        GHC.Hs.Types
        GHC.Hs.Utils
        GHC.HsToCore.PmCheck.Types
        GHC.Iface.Syntax
        GHC.Iface.Type
        GHC.LanguageExtensions
        GHC.LanguageExtensions.Type
        GHC.Lexeme
        GHC.PackageDb
        GHC.Platform
        GHC.Platform.ARM
        GHC.Platform.ARM64
        GHC.Platform.NoRegs
        GHC.Platform.PPC
        GHC.Platform.Reg
        GHC.Platform.Reg.Class
        GHC.Platform.Regs
        GHC.Platform.S390X
        GHC.Platform.SPARC
        GHC.Platform.X86
        GHC.Platform.X86_64
        GHC.Runtime.Eval.Types
        GHC.Runtime.Heap.Layout
        GHC.Runtime.Interpreter.Types
        GHC.Runtime.Linker.Types
        GHC.Serialized
        GHC.Stg.Syntax
        GHC.Types.Annotations
        GHC.Types.Avail
        GHC.Types.Basic
        GHC.Types.CostCentre
        GHC.Types.CostCentre.State
        GHC.Types.Cpr
        GHC.Types.Demand
        GHC.Types.FieldLabel
        GHC.Types.ForeignCall
        GHC.Types.Id
        GHC.Types.Id.Info
        GHC.Types.Id.Make
        GHC.Types.Literal
        GHC.Types.Module
        GHC.Types.Name
        GHC.Types.Name.Cache
        GHC.Types.Name.Env
        GHC.Types.Name.Occurrence
        GHC.Types.Name.Reader
        GHC.Types.Name.Set
        GHC.Types.RepType
        GHC.Types.SrcLoc
        GHC.Types.Unique
        GHC.Types.Unique.DFM
        GHC.Types.Unique.DSet
        GHC.Types.Unique.FM
        GHC.Types.Unique.Set
        GHC.Types.Unique.Supply
        GHC.Types.Var
        GHC.Types.Var.Env
        GHC.Types.Var.Set
        GHC.UniqueSubdir
        GHC.Utils.Lexeme
        GHC.Version
        GHCi.BreakArray
        GHCi.FFI
        GHCi.Message
        GHCi.RemoteTypes
        GHCi.TH.Binary
        GhcNameVersion
        GhcPrelude
        HaddockUtils
        HeaderInfo
        IOEnv
        Json
        KnownUniques
        Language.Haskell.TH
        Language.Haskell.TH.LanguageExtensions
        Language.Haskell.TH.Lib
        Language.Haskell.TH.Lib.Internal
        Language.Haskell.TH.Lib.Map
        Language.Haskell.TH.Ppr
        Language.Haskell.TH.PprLib
        Language.Haskell.TH.Syntax
        Lexer
        ListSetOps
        Maybes
        MonadUtils
        OrdList
        Outputable
        Pair
        Panic
        Parser
        PlainPanic
        PlatformConstants
        PprColour
        PrelNames
        Pretty
        PrimOp
        RdrHsSyn
        Settings
        SizedSeq
        Stream
        StringBuffer
        SysTools.BaseDir
        SysTools.Terminal
        TcEvidence
        TcHoleFitTypes
        TcOrigin
        TcRnTypes
        TcType
        ToolSettings
        TrieMap
        TysPrim
        TysWiredIn
        UnitInfo
        Util
