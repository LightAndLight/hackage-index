name:                red-black-tree
version:             0.1.0.0
synopsis: Red Black Trees implemented in Haskell
description:

  [Red Black Tree](https://en.wikipedia.org/wiki/Red%E2%80%93black_tree) data
  structure implemented in Haskell.

  The goal of this project is to provide an efficient generic structure that can
  insert and find elements in O(log(n)) time.

homepage:            https://github.com/GAumala/red-black-tree
license:             BSD3
license-file:        LICENSE
author:              Gabriel Aumala
maintainer:          gabriel@criptext.com
copyright:           2017 Gabriel Aumala
category:            Data Structure
build-type:          Simple
extra-source-files:  README.md
cabal-version:       >=1.10

library
  hs-source-dirs:      src
  exposed-modules:     Data.RedBlackTree.Internal
                     , Data.RedBlackTree.TreeFamily
                     , Data.RedBlackTree.InsertionAlgorithm
                     , Data.RedBlackTree.BinaryTree
                     , Data.RedBlackTree
  build-depends:       base >= 4.7 && < 5
  default-language:    Haskell2010

test-suite red-black-tree-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  other-modules:       Data.RedBlackTree.BinaryTreeSpec
                     , Data.RedBlackTree.InternalSpec
                     , Data.RedBlackTree.InsertionAlgorithmSpec
                     , Data.RedBlackTree.RedBlackTreeAssertions
                     , Data.RedBlackTree.RedBlackTreeAssertionsSpec
                     , Data.RedBlackTreeSpec
                     , Data.TestUtils
  main-is:             Spec.hs
  build-depends:       base
                     , red-black-tree
                     , hspec
                     , QuickCheck
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N
  default-language:    Haskell2010

source-repository head
  type:     git
  location: https://github.com/GAumala/red-black-tree
