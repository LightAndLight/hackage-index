name:           ADPfusion
version:        0.4.1.0
author:         Christian Hoener zu Siederdissen, 2011-2015
copyright:      Christian Hoener zu Siederdissen, 2011-2015
homepage:       https://github.com/choener/ADPfusion
bug-reports:    https://github.com/choener/ADPfusion/issues
maintainer:     choener@bioinf.uni-leipzig.de
category:       Algorithms, Data Structures, Bioinformatics, Formal Languages
license:        BSD3
license-file:   LICENSE
build-type:     Simple
stability:      experimental
cabal-version:  >= 1.10.0
tested-with:    GHC == 7.8.4, GHC == 7.10.1
synopsis:       Efficient, high-level dynamic programming.
description:
                <http://www.bioinf.uni-leipzig.de/Software/gADP/ generalized Algebraic Dynamic Programming>
                .
                ADPfusion combines stream-fusion (using the stream interface
                provided by the vector library) and type-level programming to
                provide highly efficient dynamic programming combinators.
                .
                ADPfusion allows writing dynamic programs for single- and
                multi-tape problems. Inputs can be sequences, or sets. New
                input types can be defined, without having to rewrite this
                library thanks to the open-world assumption of ADPfusion.
                .
                The library provides the machinery for Outside and Ensemble
                algorithms as well. Ensemble algorithms combine Inside and
                Outside calculations.
                .
                Starting with version 0.4.1 we support writing multiple
                context-free grammars (interleaved syntactic variables). Such
                grammars have applications in bioinformatics and linguistics.
                .
                The homepage provides a number of tutorial-style examples, with
                linear and context-free grammars over sequence and set inputs.
                .
                The formal background for generalized algebraic dynamic
                progrmaming and ADPfusion is described in a number of papers.
                These can be found on the gADP homepage and in the README.
                .



Extra-Source-Files:
  README.md
  changelog.md



flag examples
  description:  build the examples
  default:      False
  manual:       True

flag debug
  description:  dump intermediate Core files
  default:      False
  manual:       True



library
-- ghc-prim: for reallyUnsafePtrEquality#
  build-depends: base               >= 4.7      && < 4.9
               , bits               >= 0.4      && < 0.5
               , containers
               , ghc-prim
               , mmorph             >= 1.0      && < 1.1
               , monad-primitive    >= 0.1      && < 0.2
               , mtl                >= 2.0      && < 2.3
               , OrderedBits        >= 0.0.0.1  && < 0.0.1
               , primitive          >= 0.5.4    && < 0.7
               , PrimitiveArray     >= 0.6.1    && < 0.6.2
               , QuickCheck         >= 2.7      && < 2.9
               , strict             >= 0.3      && < 0.4
               , template-haskell   >= 2.0      && < 3.0
               , th-orphans         >= 0.12     && < 0.13
               , transformers       >= 0.3      && < 0.5
               , tuple              >= 0.3      && < 0.4
               , vector             >= 0.10     && < 0.11

  exposed-modules:
    ADP.Fusion
    ADP.Fusion.Apply
    ADP.Fusion.Base
    ADP.Fusion.Base.Classes
    ADP.Fusion.Base.Multi
    ADP.Fusion.Base.Point
    ADP.Fusion.Base.Set
    ADP.Fusion.Base.Subword
    ADP.Fusion.QuickCheck.Common
    ADP.Fusion.QuickCheck.Point
    ADP.Fusion.QuickCheck.Set
    ADP.Fusion.QuickCheck.Subword
    ADP.Fusion.SynVar
    ADP.Fusion.SynVar.Array
    ADP.Fusion.SynVar.Array.Point
    ADP.Fusion.SynVar.Array.Set
    ADP.Fusion.SynVar.Array.Subword
    ADP.Fusion.SynVar.Array.TermSymbol
    ADP.Fusion.SynVar.Array.Type
    ADP.Fusion.SynVar.Axiom
    ADP.Fusion.SynVar.Backtrack
    ADP.Fusion.SynVar.Fill
    ADP.Fusion.SynVar.Indices
    ADP.Fusion.SynVar.Recursive
    ADP.Fusion.SynVar.Recursive.Point
    ADP.Fusion.SynVar.Recursive.Subword
    ADP.Fusion.SynVar.Recursive.Type
    ADP.Fusion.SynVar.Split
    ADP.Fusion.SynVar.Split.Subword
    ADP.Fusion.SynVar.Split.Type
    ADP.Fusion.Term
    ADP.Fusion.Term.Chr
    ADP.Fusion.Term.Chr.Point
    ADP.Fusion.Term.Chr.Subword
    ADP.Fusion.Term.Chr.Type
    ADP.Fusion.Term.Deletion
    ADP.Fusion.Term.Deletion.Point
    ADP.Fusion.Term.Deletion.Subword
    ADP.Fusion.Term.Deletion.Type
    ADP.Fusion.Term.Edge
    ADP.Fusion.Term.Edge.Set
    ADP.Fusion.Term.Edge.Type
    ADP.Fusion.Term.Epsilon
    ADP.Fusion.Term.Epsilon.Point
    ADP.Fusion.Term.Epsilon.Subword
    ADP.Fusion.Term.Epsilon.Type
    ADP.Fusion.Term.PeekIndex
    ADP.Fusion.Term.PeekIndex.Subword
    ADP.Fusion.Term.PeekIndex.Type
    ADP.Fusion.Term.Strng
    ADP.Fusion.Term.Strng.Point
    ADP.Fusion.Term.Strng.Subword
    ADP.Fusion.Term.Strng.Type
    ADP.Fusion.TH
    ADP.Fusion.TH.Backtrack
    ADP.Fusion.TH.Common

  default-extensions: BangPatterns
                    , DataKinds
                    , DefaultSignatures
                    , FlexibleContexts
                    , FlexibleInstances
                    , GADTs
                    , KindSignatures
                    , MultiParamTypeClasses
                    , RankNTypes
                    , RecordWildCards
                    , ScopedTypeVariables
                    , StandaloneDeriving
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
                    , TypeSynonymInstances
                    , UndecidableInstances

  default-language:
    Haskell2010
  ghc-options:
    -O2 -funbox-strict-fields



-- Very simple two-sequence alignment.

executable NeedlemanWunsch

  if flag(examples)
    buildable:
      True
    build-depends:  base
                 ,  ADPfusion
                 ,  PrimitiveArray
                 ,  template-haskell
                 ,  vector
  else
    buildable:
      False
  hs-source-dirs:
    src
  main-is:
    NeedlemanWunsch.hs
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , FlexibleContexts
                    , FlexibleInstances
                    , MultiParamTypeClasses
                    , RecordWildCards
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
  ghc-options:
    -O2
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all



-- Basic RNA secondary structure folding

executable Nussinov

  if flag(examples)
    buildable:
      True
    build-depends:  base
                 ,  ADPfusion
                 ,  PrimitiveArray
                 ,  template-haskell
                 ,  vector
  else
    buildable:
      False
  hs-source-dirs:
    src
  main-is:
    Nussinov.hs
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , FlexibleContexts
                    , FlexibleInstances
                    , MultiParamTypeClasses
                    , RecordWildCards
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
                    , UndecidableInstances
  ghc-options:
    -O2
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all



-- Basic RNA secondary structure folding with partition function calculations

executable PartNussinov

  if flag(examples)
    buildable:
      True
    build-depends:  base
                 ,  ADPfusion
                 ,  log-domain        == 0.10.*
                 ,  PrimitiveArray
                 ,  template-haskell
                 ,  vector
  else
    buildable:
      False
  hs-source-dirs:
    src
  main-is:
    PartNussinov.hs
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , FlexibleContexts
                    , FlexibleInstances
                    , MultiParamTypeClasses
                    , RecordWildCards
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
  ghc-options:
    -O2
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all



executable Durbin
  if flag(examples)
    buildable:
      True
    build-depends:  base
                 ,  ADPfusion
                 ,  PrimitiveArray
                 ,  template-haskell
                 ,  vector
  else
    buildable:
      False
  hs-source-dirs:
    src
  main-is:
    Durbin.hs
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , FlexibleContexts
                    , FlexibleInstances
                    , MultiParamTypeClasses
                    , RecordWildCards
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
  ghc-options:
    -O2
    -fcpr-off
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all



executable Pseudoknot
  if flag(examples)
    buildable:
      True
    build-depends:  base
                 ,  ADPfusion
                 ,  PrimitiveArray
                 ,  template-haskell
                 ,  vector
  else
    buildable:
      False
  hs-source-dirs:
    src
  main-is:
    Pseudoknot.hs
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , DataKinds
                    , FlexibleContexts
                    , FlexibleInstances
                    , MultiParamTypeClasses
                    , RecordWildCards
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
  ghc-options:
    -O2
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all



executable OverlappingPalindromes
  if flag(examples)
    buildable:
      True
    build-depends:  base
                 ,  ADPfusion
                 ,  PrimitiveArray
                 ,  template-haskell
                 ,  vector
  else
    buildable:
      False
  hs-source-dirs:
    src
  main-is:
    OverlappingPalindromes.hs
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , FlexibleContexts
                    , FlexibleInstances
                    , MultiParamTypeClasses
                    , RecordWildCards
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
  ghc-options:
    -O2
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all



executable SplitTests
  if flag(examples)
    buildable:
      True
    build-depends:  base
                 ,  ADPfusion
                 ,  PrimitiveArray
                 ,  template-haskell
                 ,  vector
  else
    buildable:
      False
  hs-source-dirs:
    src
  main-is:
    SplitTests.hs
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , FlexibleContexts
                    , FlexibleInstances
                    , MultiParamTypeClasses
                    , RecordWildCards
                    , TemplateHaskell
                    , TypeFamilies
                    , TypeOperators
  ghc-options:
    -O2
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all



test-suite properties
  type:
    exitcode-stdio-1.0
  main-is:
    properties.hs
  ghc-options:
    -threaded -rtsopts -with-rtsopts=-N
  hs-source-dirs:
    tests
  default-language:
    Haskell2010
  default-extensions: TemplateHaskell
  build-depends: base
               , ADPfusion
               , QuickCheck
               , test-framework               >= 0.8  && < 0.9
               , test-framework-quickcheck2   >= 0.3  && < 0.4
               , test-framework-th            >= 0.2  && < 0.3



benchmark performance
  type:
    exitcode-stdio-1.0
  main-is:
    performance.hs
  ghc-options:
    -rtsopts -with-rtsopts=-N -with-rtsopts=-T
    -O2
    -funbox-strict-fields
    -funfolding-use-threshold1000
    -funfolding-keeness-factor1000
  if flag(debug)
    ghc-options:
      -ddump-to-file
      -ddump-simpl
      -dsuppress-all
  hs-source-dirs:
    tests
  default-language:
    Haskell2010
  default-extensions: BangPatterns
                    , FlexibleContexts
                    , TemplateHaskell
                    , RecordWildCards
                    , TypeFamilies
                    , TypeOperators
                    , StandaloneDeriving
                    , DeriveGeneric
  build-depends: base
               , ADPfusion
               , BenchmarkHistory   >= 0.0.0  && < 0.0.1
               , PrimitiveArray
               , vector



source-repository head
  type: git
  location: git://github.com/choener/ADPfusion

