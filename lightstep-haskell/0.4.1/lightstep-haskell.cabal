cabal-version: 2.4

name:           lightstep-haskell
version:        0.4.1
synopsis:       LightStep OpenTracing client library
description:    LightStep OpenTracing client library. Uses GRPC transport via proto-lens.
category:       Tools
homepage:       https://github.com/ethercrow/lightstep-haskell#readme
bug-reports:    https://github.com/ethercrow/lightstep-haskell/issues
maintainer:     Dmitry Ivanov <ethercrow@gmail.com>
license:        Apache-2.0
license-file:   LICENSE
build-type:     Simple

source-repository head
  type: git
  location: https://github.com/ethercrow/lightstep-haskell

common options
  default-extensions:
    BangPatterns
    BlockArguments
    DataKinds
    FlexibleInstances
    LambdaCase
    MultiParamTypeClasses
    MultiWayIf
    NamedFieldPuns
    NumericUnderscores
    RecordWildCards
    ScopedTypeVariables
    TupleSections
    TypeApplications
    ViewPatterns
  ghc-options:
    -Wall
    -Wcompat
    -Widentities
    -Wincomplete-record-updates
    -Wincomplete-uni-patterns
    -Wpartial-fields
    -Wredundant-constraints
    -fhide-source-paths
    -ferror-spans
    -freverse-errors
  default-language: Haskell2010

library
  import: options
  exposed-modules:
      LightStep.LowLevel
      LightStep.HighLevel.IO
      LightStep.Internal.Debug
      LightStep.Propagation
      Network.Wai.Middleware.LightStep
  other-modules:
      Paths_lightstep_haskell
      Proto.Collector
      Proto.Collector_Fields
      Proto.Google.Api.Annotations
      Proto.Google.Api.Http
      Proto.Google.Protobuf.Timestamp
      Proto.Google.Protobuf.Timestamp_Fields
  autogen-modules:
      Paths_lightstep_haskell
  hs-source-dirs:
      src
      gen
  build-depends:
      base >=4.12 && <5
    , async
    , bytestring
    , chronos
    , containers
    , http-types
    , http2-client >= 0.9.0.0
    , http2-client-grpc >= 0.8.0.0
    , http2-grpc-proto-lens >= 0.1.0.0
    , lens
    , mtl
    , proto-lens >= 0.5.1.0
    , proto-lens-protobuf-types >= 0.5.0.0
    , proto-lens-runtime >= 0.5.0.0
    , safe-exceptions
    , stm
    , text
    , transformers
    , unordered-containers
    , wai

executable lightstep-haskell-example
  import: options
  ghc-options:
    -threaded
    -rtsopts
    -with-rtsopts=-N
  main-is: Main.hs
  hs-source-dirs:
      examples/readme
  build-depends:
      base >=4.9 && <5
    , async
    , http2-client
    , lightstep-haskell
    , text

executable lightstep-haskell-wai-example
  import: options
  ghc-options:
    -threaded
    -rtsopts
    -with-rtsopts=-N
  main-is: Main.hs
  hs-source-dirs:
      examples/wai
  build-depends:
      base >=4.9 && <5
    , lightstep-haskell
    , http-types
    , text
    , wai
    , warp

executable lightstep-haskell-req-example
  import: options
  ghc-options:
    -threaded
    -rtsopts
    -with-rtsopts=-N
  main-is: Main.hs
  hs-source-dirs:
      examples/req
  build-depends:
      base >=4.9 && <5
    , lightstep-haskell
    , http-types
    , text
    , req >= 3.0.0
