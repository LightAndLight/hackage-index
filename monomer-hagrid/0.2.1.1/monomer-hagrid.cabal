cabal-version:      2.0
name:               monomer-hagrid
version:            0.2.1.1
license:            MIT
license-file:       LICENSE
maintainer:         garethdanielsmith@gmail.com
homepage:           https://github.com/Dretch/monomer-hagrid#readme
bug-reports:        https://github.com/Dretch/monomer-hagrid/issues
synopsis:           A datagrid widget for the Monomer library.
description:
    A configurable Monomer widget that shows a scrollable grid of data.

category:           GUI
build-type:         Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
    type:     git
    location: https://github.com/Dretch/monomer-hagrid

flag examples
    default: False
    manual:  True

library
    exposed-modules:    Monomer.Hagrid
    hs-source-dirs:     src
    other-modules:      Paths_monomer_hagrid
    autogen-modules:    Paths_monomer_hagrid
    default-language:   Haskell2010
    default-extensions:
        DisambiguateRecordFields DuplicateRecordFields FlexibleContexts
        OverloadedRecordDot OverloadedStrings

    ghc-options:
        -Wall -Wcompat -Wincomplete-record-updates
        -Wincomplete-uni-patterns -Wredundant-constraints

    build-depends:
        base >=4.7 && <5,
        containers <0.7,
        data-default-class <0.2,
        ilist <0.5,
        lens <5.2,
        monomer >=1.5.1.0 && <1.6,
        text <1.3

executable example-basic
    main-is:            Main.hs
    hs-source-dirs:     example-basic
    other-modules:      Paths_monomer_hagrid
    autogen-modules:    Paths_monomer_hagrid
    default-language:   Haskell2010
    default-extensions:
        DisambiguateRecordFields DuplicateRecordFields FlexibleContexts
        OverloadedRecordDot OverloadedStrings

    ghc-options:
        -Wall -Wcompat -Wincomplete-record-updates
        -Wincomplete-uni-patterns -Wredundant-constraints -threaded
        -rtsopts -with-rtsopts=-N

    build-depends:
        base >=4.7 && <5,
        containers <0.7,
        data-default-class <0.2,
        ilist <0.5,
        lens <5.2,
        monomer >=1.5.1.0 && <1.6,
        monomer-hagrid,
        text <1.3,
        time <1.12

    if flag(examples)

    else
        buildable: False

executable example-big-grid
    main-is:            Main.hs
    hs-source-dirs:     example-big-grid
    other-modules:      Paths_monomer_hagrid
    autogen-modules:    Paths_monomer_hagrid
    default-language:   Haskell2010
    default-extensions:
        DisambiguateRecordFields DuplicateRecordFields FlexibleContexts
        OverloadedRecordDot OverloadedStrings

    ghc-options:
        -Wall -Wcompat -Wincomplete-record-updates
        -Wincomplete-uni-patterns -Wredundant-constraints -threaded
        -rtsopts -with-rtsopts=-N

    build-depends:
        base >=4.7 && <5,
        containers <0.7,
        data-default-class <0.2,
        ilist <0.5,
        lens <5.2,
        monomer >=1.5.1.0 && <1.6,
        monomer-hagrid,
        text <1.3

    if flag(examples)

    else
        buildable: False

executable example-resizing-cells
    main-is:            Main.hs
    hs-source-dirs:     example-resizing-cells
    other-modules:      Paths_monomer_hagrid
    autogen-modules:    Paths_monomer_hagrid
    default-language:   Haskell2010
    default-extensions:
        DisambiguateRecordFields DuplicateRecordFields FlexibleContexts
        OverloadedRecordDot OverloadedStrings

    ghc-options:
        -Wall -Wcompat -Wincomplete-record-updates
        -Wincomplete-uni-patterns -Wredundant-constraints -threaded
        -rtsopts -with-rtsopts=-N

    build-depends:
        base >=4.7 && <5,
        containers <0.7,
        data-default-class <0.2,
        ilist <0.5,
        lens <5.2,
        monomer >=1.5.1.0 && <1.6,
        monomer-hagrid,
        random <1.3,
        text <1.3

    if flag(examples)

    else
        buildable: False

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    build-tool-depends: hspec-discover:hspec-discover >=2 && <3
    hs-source-dirs:     test
    other-modules:
        Monomer.HagridSpec
        Monomer.TestUtil
        Paths_monomer_hagrid

    autogen-modules:    Paths_monomer_hagrid
    default-language:   Haskell2010
    default-extensions:
        DisambiguateRecordFields DuplicateRecordFields FlexibleContexts
        OverloadedRecordDot OverloadedStrings

    ghc-options:
        -Wall -Wcompat -Wincomplete-record-updates
        -Wincomplete-uni-patterns -Wredundant-constraints -threaded
        -rtsopts -with-rtsopts=-N

    build-depends:
        base >=4.7 && <5,
        bytestring <0.12,
        containers <0.7,
        data-default <0.8,
        data-default-class <0.2,
        hspec >=2 && <3,
        ilist <0.5,
        lens <5.2,
        monomer >=1.5.1.0 && <1.6,
        monomer-hagrid,
        mtl <2.3,
        stm <2.6,
        text <1.3
