name:                alfred-margaret
version:             1.1.2.0
synopsis:            Fast Aho-Corasick string searching
description:         An efficient implementation of the Aho-Corasick
                     string searching algorithm.
homepage:            https://github.com/channable/alfred-margaret
license:             BSD3
license-file:        LICENSE
author:              The Alfred-Margaret authors
maintainer:          Ruud van Asseldonk <ruud@channable.com>, Fabian Thorand <fabian@channable.com>
copyright:           2020 Channable
category:            Data, Text
build-type:          Simple
extra-source-files:  README.md
cabal-version:       >=1.10
tested-with:
                     -- Stackage LTS 13.10.
                     GHC == 8.6.3
                     -- Stackage LTS 16.18.
                   , GHC == 8.8.4
                     -- Stackage LTS 18.27
                   , GHC == 8.10.7

source-repository head
  type:     git
  location: https://github.com/channable/alfred-margaret

Flag aeson
    Description: Enable aeson support
    Manual: False
    Default: True

-- Compile this package with LLVM, rather than with the default code generator.
-- LLVM produces about 20% faster code.
Flag llvm
  Description: Compile with LLVM
  Manual: True
  -- Defaulting to false makes the package buildable by Hackage,
  -- allowing the documentation to be generated for us.
  Default: False

library
  hs-source-dirs:      src
  exposed-modules:     Data.Text.AhoCorasick.Automaton
                     , Data.Text.AhoCorasick.Replacer
                     , Data.Text.AhoCorasick.Searcher
                     , Data.Text.AhoCorasick.Splitter
                     , Data.Text.BoyerMoore.Automaton
                     , Data.Text.BoyerMoore.Replacer
                     , Data.Text.BoyerMoore.Searcher
                     , Data.Text.CaseSensitivity
                     , Data.Text.Utf16
                     , Data.Text.Utf8
                     , Data.Text.Utf8.AhoCorasick.Automaton
                     , Data.Text.Utf8.AhoCorasick.Replacer
                     , Data.Text.Utf8.AhoCorasick.Searcher
                     , Data.Text.Utf8.AhoCorasick.Splitter
                     , Data.Text.Utf8.BoyerMoore.Automaton
                     , Data.Text.Utf8.BoyerMoore.Replacer
                     , Data.Text.Utf8.BoyerMoore.Searcher
                     , Data.TypedByteArray
  build-depends:
      base             >= 4.7 && < 5
    , containers       >= 0.6 && < 0.7
    , deepseq          >= 1.4 && < 1.5
    , hashable         >= 1.2.7 && < 1.4
    , primitive        >= 0.6.4 && < 0.8
    , text             >= 1.2.3 && < 1.3
    , unordered-containers >= 0.2.9 && < 0.3
    , vector           >= 0.12 && < 0.13
    , bytestring       >= 0.10.12 && < 1
  ghc-options:         -Wall -Wincomplete-record-updates -Wincomplete-uni-patterns -O2
  default-language:    Haskell2010
  if flag(aeson) {
  build-depends:       aeson >= 1.4.2 && < 3
  cpp-options:         -DHAS_AESON
  }
  if flag(llvm) {
  ghc-options:         -fllvm -optlo=-O3 -optlo=-tailcallelim
  }

test-suite test-suite
  type:                exitcode-stdio-1.0
  main-is:             Main.hs
  other-modules:       Data.Text.AhoCorasickSpec
                     , Data.Text.BoyerMooreSpec
                     , Data.Text.Utf8.AhoCorasickSpec
                     , Data.Text.Utf8.BoyerMooreSpec
                     , Data.Text.Utf8.Utf8Spec
                     , Data.Text.Orphans
  hs-source-dirs:      tests
  ghc-options:         -Wall -Wincomplete-record-updates -Wno-orphans
  build-depends:       base >= 4.7 && < 5
                     , QuickCheck
                     , alfred-margaret
                     , deepseq
                     , hspec
                     , hspec-expectations
                     , primitive
                     , quickcheck-instances
                     , text
  default-language:    Haskell2010

benchmark uvector-vs-tba
  type:                exitcode-stdio-1.0
  main-is:             Main.hs
  hs-source-dirs:      bench/uvector-vs-tba
  ghc-options:         -Wall -Wincomplete-record-updates -Wno-orphans
  build-depends:       base >= 4.7 && < 5
                     , alfred-margaret
                     , vector
                     , deepseq
                     , criterion
  default-language:    Haskell2010

executable dump-automaton
  main-is: Main.hs
  hs-source-dirs:
    app/dump-automaton
  build-depends:   base
                 , alfred-margaret
  default-language: Haskell2010
