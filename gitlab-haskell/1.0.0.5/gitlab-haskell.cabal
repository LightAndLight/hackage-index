cabal-version: 2.4
name:           gitlab-haskell
category:       Git
version:        1.0.0.5
synopsis:       A Haskell library for the GitLab web API
description:
            This library lifts the GitLab REST API into Haskell: <https://docs.gitlab.com/ee/api/>
            .
            The library also features a reactive event driven API for writing Gitlab file hook applications.
homepage:       https://gitlab.com/robstewart57/gitlab-haskell
bug-reports:    https://gitlab.com/robstewart57/gitlab-haskell/issues
author:         Rob Stewart
maintainer:     robstewart57@gmail.com
copyright:      2024 Rob Stewart, Heriot-Watt University
license:        BSD-3-Clause
license-file:   LICENSE
data-files:     data/system-hooks/*.json
build-type:     Simple
extra-source-files:
    README.md
  , data/api/repository-files/update-existing-file-in-repository.json
  , data/api/repository-files/create-new-file-in-repository.json
  , data/api/repository-files/get-file-from-repository.json
  , data/api/repository-files/get-file-blame-from-repository.json
  , data/api/notes/single-snippet-note.json
  , data/api/notes/single-merge-request-note.json
  , data/api/notes/project-issue-notes.json
  , data/api/groups/search-for-group.json
  , data/api/groups/details-of-group-with-projects-false.json
  , data/api/groups/list-group-subgroups.json
  , data/api/groups/list-groups-with-stats.json
  , data/api/groups/list-groups.json
  , data/api/groups/list-group-projects.json
  , data/api/groups/list-group-descendant-groups.json
  , data/api/groups/details-of-group.json
  , data/api/groups/update-group.json
  , data/api/groups/list-group-shared-projects.json
  , data/api/events/list-current-authenticated-users-events.json
  , data/api/events/get-user-contributions-events.json
  , data/api/events/list-projects-visible-events.json
  , data/api/pipelines/cancel-pipeline-jobs.json
  , data/api/version/version.json
  , data/api/commits/get-comments-of-commit.json
  , data/api/commits/get-diff-of-commit.json
  , data/api/commits/create-commit-multiple-files-commits.json
  , data/api/commits/cherry-pick-commit.json
  , data/api/commits/list-repository-commits.json
  , data/api/commits/revert-commit.json
  , data/api/users/current-user.json
  , data/api/users/set-user-status.json
  , data/api/users/user-status.json
  , data/api/users/ssh-keys.json
  , data/api/users/gpg-keys.json
  , data/api/users/user-counts.json
  , data/api/users/add-ssh-key.json
  , data/api/users/list-users.json
  , data/api/users/ssh-key.json
  , data/api/users/follow-user.json
  , data/api/users/list-user.json
  , data/api/users/followers.json
  , data/api/users/user-preferences.json
  , data/api/users/emails.json
  , data/api/tags/create-new-tag.json
  , data/api/tags/list-project-repository-tags.json
  , data/api/tags/single-repository-tag.json
  , data/api/repositories/merge-base.json
  , data/api/repositories/list-repository-tree.json
  , data/api/repositories/contributors.json
  , data/api/branches/list-repository-branches.json
  , data/api/branches/get-single-repository-branch.json
  , data/api/branches/create-repository-branch.json
  , data/api/members/add-member-group-project.json
  , data/api/members/list-all-memembers-group-or-project-including-inherited-invited.json
  , data/api/members/member-group-or-project-including-inherited-invited.json
  , data/api/members/member-group-or-project.json
  , data/api/members/billable-members-of-group.json
  , data/api/members/removed-override-flag-member-group.json
  , data/api/members/edit-member-group-project.json
  , data/api/members/list-pending-members.json
  , data/api/members/override-flag-member-group.json
  , data/api/members/list-all-memembers-group-or-project.json
  , data/api/members/membership-billable-member-of-group.json
  , data/api/projects/list-forks-of-project.json
  , data/api/projects/archive-project.json
  , data/api/projects/transfer-project-new-namespace.json
  , data/api/projects/list-project-groups.json
  , data/api/projects/get-project-users.json
  , data/api/projects/starrers-of-project.json
  , data/api/projects/unarchive-project.json
  , data/api/projects/get-single-project.json
  , data/api/projects/star-project.json
  , data/api/projects/get-path-to-repository-storage.json
  , data/api/projects/list-projects-starred-by-user.json
  , data/api/projects/upload-project-avatar.json
  , data/api/projects/list-all-projects.json
  , data/api/projects/list-user-projects.json
  , data/api/projects/unstar-project.json
  , data/api/boards/update-board.json
  , data/api/boards/list-project.json
  , data/api/boards/create-board.json
  , data/api/issues/get-time-tracking-stats.json
  , data/api/issues/edit-issue.json
  , data/api/issues/single-issue.json
  , data/api/issues/reset-time-estimate-for-issue.json
  , data/api/issues/single-project-issue.json
  , data/api/issues/reset-time-spent-for-issue.json
  , data/api/issues/unsubscribe-from-issue.json
  , data/api/issues/clone-issue.json
  , data/api/issues/promote-issue-to-epic.json
  , data/api/issues/merge-requests-related-to-issue.json
  , data/api/issues/subscribe-to-issue.json
  , data/api/issues/set-time-estimate-for-issue.json
  , data/api/issues/merge-requests-close-issue-on-merge.json
  , data/api/issues/create-todo-item.json
  , data/api/issues/list-group-issues.json
  , data/api/issues/move-issue.json
  , data/api/issues/new-issue.json
  , data/api/issues/list-issues.json
  , data/api/issues/add-time-spent-for-issue.json
  , data/api/issues/list-project-issues.json
  , data/api/issues/participants-on-issues.json
  , data/api/discussions/list-project-discussion-items.json
  , data/api/discussions/list-project-merge-request-discussion-items.json
  , data/api/discussions/list-project-snippet-discussion-items.json
  , data/api/discussions/list-group-epic-discussion-items.json
  , data/api/discussions/list-project-commit-discussion-items.json
  , data/api/jobs/pipeline-bridges.json
  , data/api/jobs/erase-job.json
  , data/api/jobs/retry-job.json
  , data/api/jobs/job-tokens-job.json
  , data/api/jobs/cancel-job.json
  , data/api/jobs/project-jobs.json
  , data/api/jobs/pipeline-jobs.json
  , data/api/jobs/play-job.json
  , data/api/jobs/single-job.json
  , data/api/todos/mark-todo-item-done.json
  , data/api/todos/todo-items.json
  , data/api/merge-requests/list-group-merge-requests.json
  , data/api/merge-requests/single-merge-request-commits.json
  , data/api/merge-requests/reset-time-merge-request.json
  , data/api/merge-requests/time-tracking-stats.json
  , data/api/merge-requests/cancel-merge-request-when-pipeline-succeeds.json
  , data/api/merge-requests/single-merge-request-participants.json
  , data/api/merge-requests/list-merge-requests.json
  , data/api/merge-requests/get-merge-request-diff-versions.json
  , data/api/merge-requests/add-spent-time-merge-request.json
  , data/api/merge-requests/update-merge-request.json
  , data/api/merge-requests/create-merge-request-pipeline.json
  , data/api/merge-requests/single-merge-request-changes.json
  , data/api/merge-requests/list-project-merge-requests.json
  , data/api/merge-requests/reset-time-estimate-merge-request.json
  , data/api/merge-requests/subscribe-merge-request.json
  , data/api/merge-requests/rebase-merge-request.json
  , data/api/merge-requests/create-todo-item.json
  , data/api/merge-requests/time-estimate-merge-request.json
  , data/api/merge-requests/list-merge-request-pipelines.json
  , data/api/merge-requests/single-merge-request.json
  , data/api/merge-requests/merge-to-default-merge-path.json
  , data/api/merge-requests/single-merge-request-diff-version.json
  , data/api/merge-requests/comments-on-merge-requests.json
  , data/api/merge-requests/accept-merge-request.json
  , data/api/merge-requests/unsubscribe-merge-request.json
  , data/api/merge-requests/create-merge-request.json
 
source-repository head
  type: git
  location: https://gitlab.com/robstewart57/gitlab-haskell

library
  exposed-modules:
                  GitLab
                , GitLab.Types
                , GitLab.API.Groups
                , GitLab.API.Members
                , GitLab.API.Commits
                , GitLab.API.Projects
                , GitLab.API.Users
                , GitLab.API.Issues
                , GitLab.API.Pipelines
                , GitLab.API.Branches
                , GitLab.API.Jobs
                , GitLab.API.Repositories
                , GitLab.API.MergeRequests
                , GitLab.API.RepositoryFiles
                , GitLab.API.Tags
                , GitLab.API.Todos
                , GitLab.API.Version
                , GitLab.API.Notes
                , GitLab.API.Discussions
                , GitLab.API.Boards
                , GitLab.SystemHooks.Types
                , GitLab.SystemHooks.GitLabSystemHooks
                , GitLab.SystemHooks.Rules
  other-modules:
                  GitLab.WebRequests.GitLabWebCalls
  hs-source-dirs:
                 src
  build-depends:
                base >=4.7 && <5
              , http-conduit
              , crypton-connection
              , aeson >= 2.0.0.0
              , bytestring
              , text
              , http-client
              , http-types
              , transformers
              , time >= 1.9
              , temporary
              , unix
              , mtl
  default-language: Haskell2010
  ghc-options: -Wall
               -- the following two for the stan static analysis tool
               -- -fwrite-ide-info
               -- -hiedir=.hie

test-suite test-gitlab-haskell
  type:          exitcode-stdio-1.0
  main-is:       Tests.hs
  other-modules: SystemHookTests
               , API.BoardsTests
               , API.BranchesTests
               , API.CommitsTests
               , API.DiscussionsTests
               , API.EventsTests
               , API.GroupsTests
               , API.IssuesTests
               , API.JobsTests
               , API.MembersTests
               , API.MergeRequestsTests
               , API.NotesTests
               , API.PipelinesTests
               , API.ProjectsTests
               , API.RepositoriesTests
               , API.RepositoryFilesTests
               , API.TagsTests
               , API.TodosTests
               , API.UsersTests
               , API.VersionTests
               , API.Common
  hs-source-dirs: tests/
  default-language: Haskell2010
  build-depends: base >= 4.8.0.0 && < 6
               , tasty
               , tasty-hunit
               , gitlab-haskell
               , bytestring
               , aeson >= 2.0.0.0
               , tree-diff
               , ansi-wl-pprint
               , vector
               , unordered-containers
               , text
  ghc-options: -Wall
