cabal-version: 3.0
name: symantic-base
maintainer: mailto:~julm/symantic-base@todo.code.sourcephile.fr
bug-reports: https://todo.code.sourcephile.fr/~julm/symantic-base
homepage: https://git.code.sourcephile.fr/~julm/symantic-base
author: Julien Moutinho <julm+symantic-base@sourcephile.fr>
copyright: Julien Moutinho <julm+symantic-base@sourcephile.fr>
license: AGPL-3.0-or-later
license-file: LICENSES/AGPL-3.0-or-later.txt
-- PVP:  +-+------- breaking API changes
--       | | +----- non-breaking API additions
--       | | | +--- code changes with no API change
version: 0.4.0.20211106
stability: experimental
category: Data Structures
synopsis: Basic symantic combinators for Embedded Domain-Specific Languages (EDSL)
description:
  This is a work-in-progress collection of basic tagless-final combinators,
  along with some advanced utilities to exploit them.

  * @Symantic.Classes@
    gathers commonly used tagless-final combinators
    (the syntax part of symantics).
  * @Symantic.Data@ interprets combinators as data constructors
    enabling to pattern-match on combinators while keeping their extensibility.
  * @Symantic.Derive@
     to give a default value to combinators which avoids boilerplate code
    when implementing combinators for an interpreter is factorizable.
  * @Symantic.SharingObserver@
    interprets combinators to observe @let@ definitions at the host language level (Haskell),
    effectively turning infinite values into finite ones,
    which is useful for example to inspect and optimize recursive grammars.
    Inspired by Andy Gill's [Type-safe observable sharing in Haskell](https://doi.org/10.1145/1596638.1596653).
    For an example, see [symantic-parser](https://hackage.haskell.org/package/symantic-parser).
  * @Symantic.Reify@
    enables the lifting to any interpreter
    of any Haskell functions taking as arguments
    only polymorphic types (possibly constrained)
    or functions using such types.
    Inspired by Oleg Kiselyov's [TDPE.hs](http://okmij.org/ftp/tagless-final/course/TDPE.hs).
  * @Symantic.Viewer@
    interprets combinators as human-readable text.
  * @Symantic.ADT@
    enables to derive reciprocal functions between
    algebraic data type constructors and @Either@s of tuples.
  * @Symantic.CurryN@
    gathers utilities for currying or uncurrying tuples
    of size greater or equal to two.
  * @Symantic.Fixity@
    gathers utilities for parsing or viewing
    infix, prefix and postfix combinators.
build-type: Simple
tested-with: GHC==8.10.4
extra-doc-files:
  ChangeLog.md
extra-source-files:
  cabal.project
  .envrc
  flake.lock
  flake.nix
  Makefile
extra-tmp-files:

source-repository head
  type: git
  location: https://git.code.sourcephile.fr/~julm/symantic-base

library
  hs-source-dirs: src
  exposed-modules:
    Symantic
    Symantic.ADT
    Symantic.Classes
    Symantic.CurryN
    Symantic.Data
    Symantic.Derive
    Symantic.Fixity
    Symantic.Optimize
    Symantic.Reify
    Symantic.SharingObserver
    Symantic.Viewer
  default-language: Haskell2010
  default-extensions:
    DefaultSignatures
    FlexibleContexts
    FlexibleInstances
    GeneralizedNewtypeDeriving
    LambdaCase
    MultiParamTypeClasses
    NamedFieldPuns
    NoImplicitPrelude
    RecordWildCards
    ScopedTypeVariables
    TupleSections
    TypeApplications
    TypeFamilies
    TypeOperators
  ghc-options:
    -Wall
    -Wincomplete-uni-patterns
    -Wincomplete-record-updates
    -Wpartial-fields
    -fprint-potential-instances
  build-depends:
    base >= 4.10 && < 5,
    containers,
    hashable,
    template-haskell,
    transformers,
    unordered-containers
