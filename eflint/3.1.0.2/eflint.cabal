cabal-version:       >=1.10

name:                eflint 
version:             3.1.0.2
synopsis:            Simulation interpreter for FLINT policy descriptions
description:

    Software systems that share potentially sensitive data are subjected to laws, regulations, policies and/or contracts. The monitoring, control and enforcement processes applied to these systems are currently to a large extent manual, which we rather automate by embedding the processes as dedicated and adaptable software services in order to improve efficiency and effectiveness. This approach requires such regulatory services to be closely aligned with a formal description of the relevant norms.
    .
    eFLINT is a domain-specific language developed for formalizing norms from a variety of sources. The theoretical foundations of the language are found in transition systems and in Hohfeld’s framework of legal fundamental conceptions. The language can be used to formalize norms from a large variety of sources. The resulting specifications are executable and support several forms of reasoning such as automatic case assessment, manual exploration and simulation. Moreover, the specifications can be used to develop regulatory services for several types of monitoring, control and enforcement. The language is evaluated through a case study formalizing articles 6(1)(a) and 16 of the General Data Protection Regulation (GDPR).
    .
    Related papers:
    .
    *eFLINT: a Domain-Specific Language for Executable Norm Specifications. Proceedings of GPCE '20. L. Thomas van Binsbergen, Lu-Chi Liu, Robert van Doesburg, and Tom van Engers. <https://doi.org/10.1145/3425898.3426958>. 
    *Dynamic generation of access control policies from social policies. Proceedings of The 11th International Conference on Current and Future Trends of Information and Communication Technologies in Healthcare (ICTH 2021). Procedia Computer Science 198C (2022) pp. 140-147. L. Thomas van Binsbergen, Milen G. Kebede, Joshua Baugh, Tom van Engers, Dannis G. van Vuurden. 
    .
    Preprints available at <https://ltvanbinsbergen.nl>

bug-reports:         https://gitlab.com/eflint/haskell-implementation
homepage:            http://cci-research.nl
license:             BSD3
license-file:        LICENSE
author:              L. Thomas van Binsbergen
maintainer:          ltvanbinsbergen@acm.org
copyright:           Copyright (C) 2019-2022 L. Thomas van Binsbergen
category:            Language
build-type:          Simple
extra-source-files:  CHANGELOG.md

executable eflint-server
  main-is:        Server.hs
  other-modules: Language.EFLINT.State
                 Language.EFLINT.Spec
                 Language.EFLINT.Parse
                 Language.EFLINT.Print
                 Language.EFLINT.StaticEval
                 Language.EFLINT.Eval
                 Language.EFLINT.Saturation
                 Language.EFLINT.Binders
                 Language.EFLINT.JSON
                 Language.EFLINT.Explorer
                 Language.EFLINT.Interpreter
                 Language.EFLINT.Util
                 Language.EFLINT.Options
  build-depends:       base >=4.9 && < 5 
                      , containers >=0.5 && <0.7
                      , hxt >= 9.3.1.16
                      , time >= 1.8.0.2
                      , gll >= 0.4.1.0
                      , regex-applicative >= 0.3.3
                      , aeson >= 1.4.6.0
                      , bytestring >= 0.10.8.2
                      , network >= 3.1
                      , text >= 1.2.4.0
                      , filepath >= 1.4.2
                      , directory >= 1.3.6
                      , exploring-interpreters >= 1.4.0.0
                      , fgl >= 5.7 
                      , mtl >= 2.2 
  hs-source-dirs:      src
  default-language:    Haskell2010
  ghc-options:         -fwarn-incomplete-patterns -fwarn-unused-imports

executable eflint-repl
  main-is:       REPL.hs
  other-modules: Language.EFLINT.State
                 Language.EFLINT.Spec
                 Language.EFLINT.Parse
                 Language.EFLINT.Print
                 Language.EFLINT.StaticEval
                 Language.EFLINT.Eval
                 Language.EFLINT.Saturation
                 Language.EFLINT.Binders
                 Language.EFLINT.JSON,
                 Language.EFLINT.Explorer
                 Language.EFLINT.Interpreter
                 Language.EFLINT.Util
                 Language.EFLINT.Options
  build-depends:       base >=4.9 && < 5 
                      , containers >=0.5 && <0.7
                      , hxt >= 9.3.1.16
                      , time >= 1.8.0.2
                      , gll >= 0.4.1.0
                      , regex-applicative >= 0.3.3
                      , aeson >= 1.4.6.0
                      , bytestring >= 0.10.8.2
                      , network >= 3.1
                      , filepath >= 1.4.2
                      , directory >= 1.3.6
                      , mtl >= 2.2
                      , haskeline >= 0.8.1
                      , transformers >= 0.5.6
                      , exploring-interpreters >= 1.4.0.0
                      , text
  hs-source-dirs:      src
  default-language:    Haskell2010
  ghc-options:         -fwarn-incomplete-patterns -fwarn-unused-imports

library
  exposed-modules : Language.EFLINT.State
                  , Language.EFLINT.Spec
                  , Language.EFLINT.Parse
                  , Language.EFLINT.Print
                  , Language.EFLINT.Interpreter
                  , Language.EFLINT.Explorer
                  , Language.EFLINT.Saturation
  other-modules:
                 Language.EFLINT.StaticEval
                 Language.EFLINT.Eval
                 Language.EFLINT.Binders
                 Language.EFLINT.JSON
                 Language.EFLINT.Util
                 Language.EFLINT.Options
  build-depends:       base >=4.9 && < 5 
                      , containers >=0.5 && <0.7
                      , hxt >= 9.3.1.16
                      , time >= 1.8.0.2
                      , gll >= 0.4.1.0
                      , regex-applicative >= 0.3.3
                      , aeson >= 1.4.6.0
                      , bytestring >= 0.10.8.2
                      , network >= 3.1
                      , filepath >= 1.4.2
                      , directory >= 1.3.6
                      , exploring-interpreters >= 1.4.0.0 
                      , fgl >= 5.7 
                      , mtl >= 2.2 
  hs-source-dirs:      src
  default-language:    Haskell2010
  ghc-options:         -fwarn-incomplete-patterns -fwarn-unused-imports
