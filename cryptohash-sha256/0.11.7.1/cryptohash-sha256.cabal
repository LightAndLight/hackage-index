name:                cryptohash-sha256
version:             0.11.7.1
description:
    A practical incremental and one-pass, pure API to the
    <https://en.wikipedia.org/wiki/SHA-2 SHA-256 hash algorithm>
    with performance close to the fastest implementations available in other languages.
    .
    The implementation is made in C with a haskell FFI wrapper that hides the C implementation.
    .
    NOTE: This package has been forked off @cryptohash-0.11.7@ because the @cryptohash@ has been
    deprecated and so this package continues to satisfy the need for a lightweight package
    providing the SHA256 hash algorithm without any dependencies on packages other than
    @base@ and @bytestring@.
    .
    Consequently, this package can be used as a drop-in replacement for @cryptohash@'s
    "Crypto.Hash.SHA256" module, though with a clearly smaller footprint.

license:             BSD3
license-file:        LICENSE
copyright:           Vincent Hanquez, Herbert Valerio Riedel
maintainer:          Herbert Valerio Riedel <hvr@gnu.org>
homepage:            https://github.com/hvr/cryptohash-sha256
bug-reports:         https://github.com/hvr/cryptohash-sha256/issues
synopsis:            Fast, pure and practical SHA-256 implementation
category:            Data, Cryptography
build-type:          Simple
cabal-version:       >=1.10
tested-with:         GHC == 7.4.2
                   , GHC == 7.6.3
                   , GHC == 7.8.4
                   , GHC == 7.10.3
                   , GHC == 8.0.1

extra-source-files:  cbits/bitfn.h
                     cbits/sha256.h

source-repository head
  type:     git
  location: https://github.com/hvr/cryptohash-sha256.git

library
  default-language:  Haskell2010
  build-depends:     base             >= 4.5   && < 4.10
                   , bytestring       >= 0.9.2 && < 0.11

  exposed-modules:   Crypto.Hash.SHA256
  ghc-options:       -Wall -fno-cse -O2
  cc-options:        -O3
  c-sources:         cbits/sha256.c
  include-dirs:      cbits

test-suite test-kat
  default-language:  Haskell2010
  type:              exitcode-stdio-1.0
  hs-source-dirs:    Tests
  main-is:           KAT.hs
  build-depends:     cryptohash-sha256
                   , base
                   , bytestring
                   , tasty            == 0.11.*
                   , tasty-quickcheck == 0.8.*
                   , tasty-hunit      == 0.9.*

benchmark bench-hashes
  default-language:  Haskell2010
  type:              exitcode-stdio-1.0
  main-is:           Bench.hs
  hs-source-dirs:    Bench
  build-depends:     cryptohash-sha256
                   , base
                   , bytestring
                   , criterion        == 1.1.*

benchmark bench-api
  default-language:  Haskell2010
  type:              exitcode-stdio-1.0
  main-is:           BenchAPI.hs
  hs-source-dirs:    Bench
  build-depends:     cryptohash-sha256
                   , base
                   , bytestring
                   , criterion        == 1.1.*
