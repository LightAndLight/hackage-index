cabal-version: 1.12

name:           tinytools-vty
version:        0.1.0.4
description:    tinytools-vty is a terminal based monospace unicode diagram editing tool
homepage:       https://github.com/minimapletinytools/tinytools-vty#readme
bug-reports:    https://github.com/minimapletinytools/tinytools-vty/issues
author:         minimaple
maintainer:     minimapletinytools@gmail.com
category:       tinytools
synopsis:       tinytools-vty is a terminal based monospace unicode diagram editing tool
copyright:      2023 minimaple (Peter Lu)
license:        BSD3
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    README.md
    ChangeLog.md
    tutorial.potato


source-repository head
  type: git
  location: https://github.com/minimapletinytools/tinytools-vty


library
  exposed-modules:
      Data.String.Unicode.Lookup
      Potato.Flow.TutorialState
      Potato.Flow.Vty.Alert
      Potato.Flow.Vty.AppKbCmd
      Potato.Flow.Vty.Attrs
      Potato.Flow.Vty.Canvas
      Potato.Flow.Vty.Common
      Potato.Flow.Vty.Info
      Potato.Flow.Vty.Input
      Potato.Flow.Vty.Layer
      Potato.Flow.Vty.Left
      Potato.Flow.Vty.Main
      Potato.Flow.Vty.OpenWindow
      Potato.Flow.Vty.Params
      Potato.Flow.Vty.PotatoReader
      Potato.Flow.Vty.SaveAsWindow
      Potato.Flow.Vty.ToolOptions
      Potato.Flow.Vty.Tools
      Potato.Reflex.Vty.Helpers
      Potato.Reflex.Vty.Host
      Potato.Reflex.Vty.Widget
      Potato.Reflex.Vty.Widget.FileExplorer
      Potato.Reflex.Vty.Widget.Menu
      Potato.Reflex.Vty.Widget.Popup
      Potato.Reflex.Vty.Widget.ScrollBar
      Potato.Reflex.Vty.Widget.TextInputHelpers
      Potato.Reflex.Vty.Widget.Windows
      Reflex.Vty.Test.Monad.Host
      Reflex.Vty.Test.Monad.Host.TH
  hs-source-dirs:
      src
  default-extensions:
      ApplicativeDo
      BangPatterns
      DataKinds
      ConstraintKinds
      DeriveFoldable
      DeriveFunctor
      DeriveTraversable
      DeriveGeneric
      DeriveLift
      DeriveTraversable
      DerivingStrategies
      EmptyCase
      ExistentialQuantification
      FlexibleContexts
      FlexibleInstances
      FunctionalDependencies
      GADTs
      GeneralizedNewtypeDeriving
      InstanceSigs
      KindSignatures
      LambdaCase
      MultiParamTypeClasses
      MultiWayIf
      NamedFieldPuns
      OverloadedStrings
      PatternSynonyms
      RankNTypes
      ScopedTypeVariables
      StandaloneDeriving
      TupleSections
      TypeApplications
      TypeFamilies
      TypeFamilyDependencies
      TypeOperators
      NoImplicitPrelude
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints
  build-depends:
      aeson
    , aeson-pretty
    , ansi-terminal
    , base >=4.7 && <5
    , bimap
    , bytestring
    , clock
    , constraints-extras
    , containers
    , data-default
    , dependent-map
    , dependent-sum
    , directory
    , extra
    , file-embed
    , filepath
    , http-conduit
    , ilist
    , lens
    , mtl
    , optparse-applicative
    , primitive
    , ref-tf
    , reflex
    , reflex-potatoes
    , reflex-test-host >=0.1.2.2
    , reflex-vty >= 0.5.1
    , relude
    , semialign
    -- TODO set latest version
    , template-haskell >=2.18
    , text
    , these
    , time
    , tinytools >= 0.1.0.3
    , vty
  default-language: Haskell2010

executable tinytools
  main-is: Main.hs
  hs-source-dirs:
      app
  default-extensions:
      ApplicativeDo
      BangPatterns
      DataKinds
      ConstraintKinds
      DeriveFoldable
      DeriveFunctor
      DeriveTraversable
      DeriveGeneric
      DeriveLift
      DeriveTraversable
      DerivingStrategies
      EmptyCase
      ExistentialQuantification
      FlexibleContexts
      FlexibleInstances
      FunctionalDependencies
      GADTs
      GeneralizedNewtypeDeriving
      InstanceSigs
      KindSignatures
      LambdaCase
      MultiParamTypeClasses
      MultiWayIf
      NamedFieldPuns
      OverloadedStrings
      PatternSynonyms
      RankNTypes
      ScopedTypeVariables
      StandaloneDeriving
      TupleSections
      TypeApplications
      TypeFamilies
      TypeFamilyDependencies
      TypeOperators
      NoImplicitPrelude
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -threaded -rtsopts -with-rtsopts=-N +RTS -A128m -n2m -s -RTS
  build-depends:
      aeson
    , aeson-pretty
    , ansi-terminal
    , base >=4.7 && <5
    , bimap
    , bytestring
    , clock
    , constraints-extras
    , containers
    , data-default
    , dependent-map
    , dependent-sum
    , directory
    , extra
    , file-embed
    , filepath
    , http-conduit
    , ilist
    , lens
    , mtl
    , optparse-applicative
    , tinytools-vty
    , primitive
    , ref-tf
    , reflex-vty >= 0.5.1
    , reflex >= 0.8 && <1
    , reflex-potatoes >=0.1
    , reflex-test-host >=0.1.2.2
    , tinytools >= 0.1.0.3
    , relude
    , semialign
    , template-haskell
    , text
    , these
    , time
    , vty
  default-language: Haskell2010

executable write-term-width
  main-is: termwidth.hs
  other-modules:
      Main
  hs-source-dirs:
      app
  default-extensions:
      ApplicativeDo
      BangPatterns
      DataKinds
      ConstraintKinds
      DeriveFoldable
      DeriveFunctor
      DeriveTraversable
      DeriveGeneric
      DeriveLift
      DeriveTraversable
      DerivingStrategies
      EmptyCase
      ExistentialQuantification
      FlexibleContexts
      FlexibleInstances
      FunctionalDependencies
      GADTs
      GeneralizedNewtypeDeriving
      InstanceSigs
      KindSignatures
      LambdaCase
      MultiParamTypeClasses
      MultiWayIf
      NamedFieldPuns
      OverloadedStrings
      PatternSynonyms
      RankNTypes
      ScopedTypeVariables
      StandaloneDeriving
      TupleSections
      TypeApplications
      TypeFamilies
      TypeFamilyDependencies
      TypeOperators
      NoImplicitPrelude
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      aeson
    , aeson-pretty
    , ansi-terminal
    , base >=4.7 && <5
    , bimap
    , bytestring
    , clock
    , constraints-extras
    , containers
    , data-default
    , dependent-map
    , dependent-sum
    , directory
    , extra
    , file-embed
    , filepath
    , http-conduit
    , ilist
    , lens
    , mtl
    , optparse-applicative
    , primitive
    , ref-tf
    , reflex >= 0.8 && <1
    , reflex-potatoes >=0.1
    , reflex-test-host >=0.1.2.2
    , relude
    , semialign
    , template-haskell
    , text
    , these
    , time
    , vty
  default-language: Haskell2010

test-suite tinytools-vty-test
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      Potato.Flow.ParamsSpec
      Potato.FlowSpecTH
      Potato.Reflex.Vty.Widget.PopupSpec
      PotatoSpec
      Reflex.Vty.Test.Common
      Reflex.Vty.Test.Monad.HostSpec
      Reflex.Vty.Test.Monad.THSpec
  hs-source-dirs:
      test
  default-extensions:
      ApplicativeDo
      BangPatterns
      DataKinds
      ConstraintKinds
      DeriveFoldable
      DeriveFunctor
      DeriveTraversable
      DeriveGeneric
      DeriveLift
      DeriveTraversable
      DerivingStrategies
      EmptyCase
      ExistentialQuantification
      FlexibleContexts
      FlexibleInstances
      FunctionalDependencies
      GADTs
      GeneralizedNewtypeDeriving
      InstanceSigs
      KindSignatures
      LambdaCase
      MultiParamTypeClasses
      MultiWayIf
      NamedFieldPuns
      OverloadedStrings
      PatternSynonyms
      RankNTypes
      ScopedTypeVariables
      StandaloneDeriving
      TupleSections
      TypeApplications
      TypeFamilies
      TypeFamilyDependencies
      TypeOperators
      NoImplicitPrelude
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      HUnit
    , aeson
    , aeson-pretty
    , ansi-terminal
    , base >=4.7 && <5
    , bimap
    , bytestring
    , clock
    , constraints-extras
    , containers
    , data-default
    , dependent-map
    , dependent-sum
    , directory
    , extra
    , file-embed
    , filepath
    , hspec
    , hspec-contrib
    , http-conduit
    , ilist
    , lens
    , mtl
    , optparse-applicative
    , tinytools-vty
    , primitive
    , ref-tf
    , reflex
    , reflex-potatoes
    , reflex-test-host >=0.1.2.2
    , reflex-vty >= 0.5.1
    , relude
    , semialign
    , template-haskell
    , text
    , these
    , time
    , tinytools >= 0.1.0.3
    , vty
  default-language: Haskell2010
