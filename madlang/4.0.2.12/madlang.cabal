cabal-version: 1.18
name: madlang
version: 4.0.2.12
x-revision: 2
license: BSD3
license-file: LICENSE
copyright: Copyright: (c) 2016-2018 Vanessa McHale
maintainer: vamchale@gmail.com
author: Vanessa McHale
stability: experimental
tested-with: ghc ==8.2.2 ghc ==8.4.3 ghc ==8.0.2
synopsis: Randomized templating language DSL
description:
    Madlang is a text templating language written in Haskell,
    meant to explore computational creativity and generative
    literature.
category: Text
build-type: Custom
extra-source-files:
    test/templates/*.mad
    test/templates/err/*.mad
    demo/*.mad
    cabal.project.local
    man/madlang.1
extra-doc-files: README.md
                 CHANGELOG.md

source-repository head
    type: darcs
    location: https://hub.darcs.net/vmchale/madlang

custom-setup
    setup-depends: base -any,
                   Cabal -any,
                   cli-setup >=0.1.0.2

flag development
    description:
        Turn on '-Werror'
    default: False
    manual: True

flag library
    description:
        Don't build an executable
    default: False
    manual: True

library
    exposed-modules:
        Text.Madlibs
    hs-source-dirs: src
    other-modules:
        Text.Madlibs.Ana.ParseUtils
        Text.Madlibs.Cata.Run
        Text.Madlibs.Ana.Parse
        Text.Madlibs.Ana.Resolve
        Text.Madlibs.Internal.Types
        Text.Madlibs.Generate.TH
        Text.Madlibs.Internal.Utils
        Text.Madlibs.Cata.SemErr
        Text.Madlibs.Cata.Display
    default-language: Haskell2010
    other-extensions: DeriveAnyClass DeriveFunctor DeriveGeneric
                      DeriveLift FlexibleInstances OverloadedStrings TypeFamilies
                      TemplateHaskell
    ghc-options: -Wall
    build-depends:
        base >=4.9 && <5,
        megaparsec >=6.0 && <7.0,
        text -any,
        template-haskell -any,
        MonadRandom -any,
        composition-prelude >=1.1.0.0 && <2.0.2.0,
        directory -any,
        file-embed -any,
        random-shuffle -any,
        mtl -any,
        ansi-wl-pprint -any,
        containers -any,
        titlecase >=1.0,
        th-lift-instances -any,
        recursion >=1.0.0.0,
        binary -any
    
    if (flag(development) && impl(ghc <8.4))
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wredundant-constraints -Widentities

executable madlang
    main-is: Main.hs
    hs-source-dirs: app
    other-modules:
        Paths_madlang
        Text.Madlibs.Packaging.Fetch
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        base >=4.9,
        madlang -any,
        optparse-applicative -any,
        text -any,
        directory -any,
        megaparsec -any,
        http-client -any,
        tar -any,
        zlib -any,
        zip-archive -any,
        http-client-tls -any
    
    if flag(library)
        buildable: False
    
    if (flag(development) && impl(ghc <8.4))
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wredundant-constraints -Widentities

test-suite madlang-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    other-modules:
        Demo
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -Wall -with-rtsopts=-N
    build-depends:
        base -any,
        madlang -any,
        hspec -any,
        text -any,
        hspec-megaparsec -any
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wredundant-constraints -Widentities

benchmark madlang-bench
    type: exitcode-stdio-1.0
    main-is: Bench.hs
    hs-source-dirs: bench
    default-language: Haskell2010
    build-depends:
        base -any,
        criterion -any,
        madlang -any,
        megaparsec -any,
        text -any
    
    if impl(ghc >=8.0)
        ghc-options: -Wredundant-constraints -Widentities
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wredundant-constraints -Widentities
