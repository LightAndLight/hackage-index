name:                madlang
version:             2.4.0.1
x-revision: 2
synopsis:            Randomized templating language DSL
description:         Please see README.md
homepage:            https://github.com/vmchale/madlang#readme
license:             BSD3
license-file:        LICENSE
author:              Vanessa McHale
maintainer:          vanessa.mchale@reconfigure.io
copyright:           Copyright: (c) 2016-2017 Vanessa McHale
category:            Web
build-type:          Simple
stability:           experimental
extra-source-files:  README.md
                   , test/templates/*.mad
                   , test/templates/err/*.mad
                   , demo/*.mad
                   , default.nix
                   , release.nix
                   , stack.yaml
                   , bash/mkCompletions
cabal-version:       >=1.10

Flag llvm-fast {
  Description: Enable build with llvm backend (produces a faster executable)
  Default: False
}

Flag library {
  Description: Don't build an executable
  Default:     False
}

Flag gold {
  Description: Enable the gold linker for faster build times
  Default:     True
}

library
  hs-source-dirs:      src
  exposed-modules:     Text.Madlibs
  other-modules:       Text.Madlibs.Ana.ParseUtils
                     , Text.Madlibs.Cata.Run
                     , Text.Madlibs.Ana.Parse
                     , Text.Madlibs.Ana.Resolve
                     , Text.Madlibs.Internal.Types
                     , Text.Madlibs.Generate.TH
                     , Text.Madlibs.Internal.Utils
                     , Text.Madlibs.Cata.SemErr
                     , Text.Madlibs.Cata.Display
                     , Text.Madlibs.Exec.Main
                     , Paths_madlang
  build-depends:       base >= 4.7 && < 4.11
                     , megaparsec < 6.0.0
                     , text
                     , optparse-applicative
                     , template-haskell
                     , MonadRandom
                     , composition
                     , composition-extra
                     -- , dhall
                     , directory
                     , file-embed
                     , random-shuffle
                     , microlens
                     , mtl
                     , ansi-wl-pprint
                     , containers
  default-language:    Haskell2010
  default-extensions:  OverloadedStrings
                     , DeriveGeneric
                     , DeriveFunctor
                     , DeriveAnyClass
  if flag(gold)
    ghc-options:       -optl-fuse-ld=gold
    ld-options:        -fuse-ld=gold
  ghc-options:         -fwarn-unused-imports

executable madlang
  if flag(library)
    Buildable: False
  else
    Buildable: True
  hs-source-dirs:      app
  main-is:             Main.hs
  if flag(llvm-fast)
    ghc-options:       -fllvm -O3 -optlo-O3 -opta-march=native -opta-mtune=native
  if flag(gold)
    ghc-options:       -optl-fuse-ld=gold 
    ld-options:        -fuse-ld=gold
  ghc-options:         -threaded -rtsopts
  build-depends:       base
                     , madlang
  default-language:    Haskell2010

benchmark madlang-bench
  type:             exitcode-stdio-1.0
  hs-source-dirs:   bench
  main-is:          Bench.hs
  build-depends:    base
                  , criterion
                  , madlang
                  , megaparsec
                  , text
  if flag(llvm-fast)
    ghc-options:    -fllvm -optlo-O3 -O3 -fwarn-unused-imports
  if flag(gold)
    ghc-options:       -optl-fuse-ld=gold
    ld-options:        -fuse-ld=gold
  ghc-options:      -threaded -rtsopts -with-rtsopts=-N -O3 -fwarn-unused-imports
  default-language: Haskell2010

test-suite madlang-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Spec.hs
  other-modules:       Demo
  build-depends:       base
                     , madlang
                     , hspec
                     , megaparsec
                     , text
                     , mtl
                     , hspec-megaparsec
  if flag(gold)
    ghc-options:       -optl-fuse-ld=gold
    ld-options:        -fuse-ld=gold
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N -fwarn-unused-imports
  default-language:    Haskell2010

source-repository head
  type:     darcs
  location: https://hub.darcs.net/vmchale/madlang
