cabal-version: 2.2
name: sockets
version: 0.3.1.0
synopsis: High-level network sockets
description:
  This library provides a high-level abstraction for network sockets. It uses
  Haskell2010 (along with GADTs) without typeclasses to ensure that
  consumers of the API can only call appropriate functions on a socket.
  .
  Exceptions are tracked in the types of functions and returned to the caller
  with `Either`. The caller is free to handle these gracefully or to throw
  them. This library has another class of exceptions described as _unrecoverable_.
  This library only throws exceptions in three situations:
  .
  * The library detects that it has misused the operating system's
    sockets API. This includes getting a `sockaddr` with an unexpected
    socket family. It also includes getting an error code that should not
    be possible. For example, the abstractions provided for both datagram
    sockets and stream sockets mean that `send` system calls in either
    context should never return the error code `ENOTCONN`. Consequently,
    this error is treated as unrecoverable.
  .
  * The caller asks for a negatively-sized slice of a buffer
    (such exceptions indicate a mistake in the code consuming this API).
  .
  * A system call fails with `ENOBUFS` or `ENOMEM`. These indicate that
    the operating system is out of memory. If this happens, the
    Out Of Memory (OOM) manager is likely killing processes to
    reclaim memory, so the process that received this message may
    be killed soon. Making things even worse is that the GHC runtime
    requests pages of memory from the operating system at times that
    are effectively unpredictable to Haskell developers. (Most
    memory-managed languages have this behavior). Any attempt
    to recover from `ENOBUFS` or `ENOMEM` might cause the runtime to
    allocate memory from the operating system. According to the
    documentation for the <http://hackage.haskell.org/package/base-4.12.0.0/docs/Control-Exception-Base.html#t:AsyncException HeapOverflow>
    exception, an allocation failure at this point in time (likely given
    the recent `ENOBUFS`/`ENOMEM`) would result in immidiate
    termination of the program. So, although it is technically possible
    to recover from `ENOBUFS`/`ENOMEM`, the OOM killer and the
    GHC runtime make it impossible to do so reliably.  Consequently,
    these error codes are treated as fatal.

homepage: https://github.com/andrewthad/sockets
bug-reports: https://github.com/andrewthad/sockets/issues
license: BSD-3-Clause
license-file: LICENSE
author: Andrew Martin
maintainer: andrew.thaddeus@gmail.com
copyright: 2019 Andrew Martin
category: Network
extra-source-files: CHANGELOG.md

flag mmsg
  manual: True
  description: Use sendmmsg and recvmmsg 
  default: False

flag debug
  manual: True
  description: Print debug output 
  default: False

flag example
  manual: True
  description: Build example executables
  default: False

library
  exposed-modules:
    Socket.Datagram.IPv4.Undestined
    Socket.Datagram.IPv4.Spoof
    Socket.Stream.IPv4
  other-modules:
    Socket.Stream
    Socket.Datagram
    Socket.Datagram.IPv4.Undestined.Multiple
    Socket.Datagram.IPv4.Undestined.Internal
    Socket.Debug
    Socket.IPv4
    Socket
  build-depends:
    , base >= 4.11.1.0 && < 5
    , bytestring >= 0.10 && < 0.11
    , error-codes >= 0.1 && < 0.2
    , ip >= 1.4.1
    , posix-api >= 0.2.1
    , primitive >= 0.6.4
    , stm >= 2.4
    , text >= 1.2
  hs-source-dirs: src
  if flag(debug)
    hs-source-dirs: src-debug
  else
    hs-source-dirs: src-production
  if flag(mmsg)
    hs-source-dirs: src-mmsg
  else
    hs-source-dirs: src-no-mmsg
  default-language: Haskell2010
  ghc-options: -O2 -Wall

test-suite test
  type: exitcode-stdio-1.0
  hs-source-dirs: test
  main-is: Main.hs
  build-depends:
    , base >= 4.11.1.0 && < 5
    , sockets
    , tasty
    , tasty-hunit
    , ip >= 1.4.1
    , primitive >= 0.6.4
    , async
    , bytestring
  ghc-options: -Wall -O2 -threaded
  default-language: Haskell2010

benchmark macro
  type: exitcode-stdio-1.0
  build-depends:
    , base >= 4.11.1.0 && < 5
    , sockets
    , ip >= 1.4.1
    , primitive >= 0.6.4
    , bytestring >= 0.10.8.2
    , entropy >= 0.4.1.4
  ghc-options: -Wall -O2 -threaded -rtsopts
  default-language: Haskell2010
  hs-source-dirs: bench
  main-is: Macro.hs

executable sockets-example
  if flag(example)
    build-depends:
      , base >= 4.11.1.0 && < 5
      , sockets
      , ip >= 1.4.1
      , primitive >= 0.6.4
      , bytestring >= 0.10.8.2
      , fast-logger >= 2.4.13
  else
    buildable: False
  hs-source-dirs: example
  main-is: Main.hs
  ghc-options: -Wall -O2 -threaded
  default-language: Haskell2010
