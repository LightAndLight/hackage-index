cabal-version:      3.0
name:               horizon-spec
version:            0.6.4
synopsis:           Horizon Stable Package Set Type Definitions
description:
  This package contains the type definitions for the Horizon stable package set (https://horizon-haskell.net). This is a schema used to define package sets sourcing from hackage and git.

category:           Package Management
author:             Daniel Firth
maintainer:         dan.firth@homotopic.tech
copyright:          2022 Daniel Firth
homepage:           https://horizon-haskell.net
license:            MIT
license-file:       LICENSE
build-type:         Simple
extra-source-files:
  ChangeLog.md
  dhall/package.dhall
  README.md
  test/data/modified-overlay/input.dhall
  test/data/modified-overlay/output.golden
  test/data/modified-package-set/input.dhall
  test/data/modified-package-set/output.golden
  test/data/sample-overlay/input.dhall
  test/data/sample-overlay/output.golden
  test/data/sample-package-set/input.dhall
  test/data/sample-package-set/output.golden

source-repository head
  type:     git
  location: https://gitlab.homotopic.tech/horizon/horizon-spec

library
  exposed-modules:
    Horizon.Spec
    Horizon.Spec.Types.CabalFlag
    Horizon.Spec.Types.Compiler
    Horizon.Spec.Types.Flag
    Horizon.Spec.Types.GitSource
    Horizon.Spec.Types.HackageSource
    Horizon.Spec.Types.HaskellPackage
    Horizon.Spec.Types.HaskellSource
    Horizon.Spec.Types.HorizonExport
    Horizon.Spec.Types.LocalSource
    Horizon.Spec.Types.Modifiers
    Horizon.Spec.Types.Name
    Horizon.Spec.Types.Overlay
    Horizon.Spec.Types.OverlayExportSettings
    Horizon.Spec.Types.OverlayFile
    Horizon.Spec.Types.PackageList
    Horizon.Spec.Types.PackagesDir
    Horizon.Spec.Types.PackageSet
    Horizon.Spec.Types.PackageSetExportSettings
    Horizon.Spec.Types.PackageSetFile
    Horizon.Spec.Types.Repo
    Horizon.Spec.Types.Revision
    Horizon.Spec.Types.Subdir
    Horizon.Spec.Types.TarballSource
    Horizon.Spec.Types.Url
    Horizon.Spec.Types.Version
    Horizon.Spec.Utils

  hs-source-dirs:     src
  default-extensions:
    DataKinds
    DeriveGeneric
    DerivingStrategies
    DuplicateRecordFields
    GADTs
    GeneralizedNewtypeDeriving
    StandaloneKindSignatures

  ghc-options:
    -Weverything -Wno-all-missed-specialisations -Wno-implicit-prelude
    -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module
    -Wno-safe -Wno-unsafe

  build-depends:
    , base                 >=4.7 && <5
    , containers
    , dhall
    , path
    , path-dhall-instance
    , template-haskell
    , text
    , th-lift

  default-language:   Haskell2010

executable horizon-spec-tests
  main-is:            Spec.hs
  hs-source-dirs:     test
  ghc-options:
    -Weverything -Wno-all-missed-specialisations -Wno-implicit-prelude
    -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module
    -Wno-safe -Wno-unsafe

  default-extensions: TypeApplications
  build-depends:
    , base           >=4.7 && <5
    , dhall
    , horizon-spec
    , prettyprinter
    , sydtest
    , text

  default-language:   Haskell2010
