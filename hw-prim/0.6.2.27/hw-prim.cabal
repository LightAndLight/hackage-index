cabal-version:  2.2

name:           hw-prim
version:        0.6.2.27
synopsis:       Primitive functions and data types
description:    Primitive functions and data types.
category:       Data
stability:      Experimental
homepage:       http://github.com/haskell-works/hw-prim#readme
bug-reports:    https://github.com/haskell-works/hw-prim/issues
author:         John Ky
maintainer:     newhoggy@gmail.com
copyright:      2016-2019 John Ky
license:        BSD-3-Clause
license-file:   LICENSE
tested-with:    GHC == 8.6.5, GHC == 8.4.4, GHC == 8.2.2
build-type:     Simple
extra-source-files:
    README.md

source-repository head
  type: git
  location: https://github.com/haskell-works/hw-prim

flag bounds-checking-enabled
  description: Enable bmi2 instruction set
  manual: False
  default: False

common base                 { build-depends: base                 >= 4          && < 5      }

common QuickCheck           { build-depends: QuickCheck           >= 2.10       && < 2.14   }
common bytestring           { build-depends: bytestring           >= 0.9        && < 0.11   }
common criterion            { build-depends: criterion            >= 1.2        && < 1.6    }
common directory            { build-depends: directory            >= 1.2        && < 1.4    }
common exceptions           { build-depends: exceptions           >= 0.8        && < 0.11   }
common ghc-prim             { build-depends: ghc-prim                                       }
common hedgehog             { build-depends: hedgehog             >= 0.5        && < 1.1    }
common hspec                { build-depends: hspec                >= 2.4        && < 2.8    }
common hw-hspec-hedgehog    { build-depends: hw-hspec-hedgehog    >= 0.1        && < 0.2    }
common mmap                 { build-depends: mmap                 >= 0.5        && < 0.6    }
common semigroups           { build-depends: semigroups           >= 0.8.4      && < 0.20   }
common transformers         { build-depends: transformers         >= 0.4        && < 0.6    }
common vector               { build-depends: vector               >= 0.12       && < 0.13   }

common common
  default-language:   Haskell2010
  ghc-options:        -Wall -O2 -msse4.2
  if flag(bounds-checking-enabled)
    cpp-options: -DBOUNDS_CHECKING_ENABLED

library
  import:   base
          , common
          , bytestring
          , ghc-prim
          , mmap
          , semigroups
          , transformers
          , vector
  exposed-modules:
    HaskellWorks.Data.AtIndex
    HaskellWorks.Data.Branchless
    HaskellWorks.Data.ByteString
    HaskellWorks.Data.ByteString.Builder
    HaskellWorks.Data.ByteString.Lazy
    HaskellWorks.Data.Char
    HaskellWorks.Data.Char.IsChar
    HaskellWorks.Data.Concat
    HaskellWorks.Data.Cons
    HaskellWorks.Data.Container
    HaskellWorks.Data.Decode
    HaskellWorks.Data.Drop
    HaskellWorks.Data.Empty
    HaskellWorks.Data.Filter
    HaskellWorks.Data.Foldable
    HaskellWorks.Data.FromByteString
    HaskellWorks.Data.FromForeignRegion
    HaskellWorks.Data.Generate
    HaskellWorks.Data.Head
    HaskellWorks.Data.Length
    HaskellWorks.Data.Naive
    HaskellWorks.Data.Null
    HaskellWorks.Data.Ops
    HaskellWorks.Data.Positioning
    HaskellWorks.Data.Product
    HaskellWorks.Data.Search
    HaskellWorks.Data.Sign
    HaskellWorks.Data.Snoc
    HaskellWorks.Data.Take
    HaskellWorks.Data.TreeCursor
    HaskellWorks.Data.Uncons
    HaskellWorks.Data.Unsign
    HaskellWorks.Data.Unsnoc
    HaskellWorks.Data.Vector.AsVector64
    HaskellWorks.Data.Vector.AsVector64ns
    HaskellWorks.Data.Vector.AsVector64s
    HaskellWorks.Data.Vector.AsVector8
    HaskellWorks.Data.Vector.AsVector8ns
    HaskellWorks.Data.Vector.AsVector8s
    HaskellWorks.Data.Vector.BoxedVectorLike
    HaskellWorks.Data.Vector.Storable
    HaskellWorks.Data.Vector.StorableVectorLike
    HaskellWorks.Data.Word
  other-modules:      Paths_hw_prim
  autogen-modules:    Paths_hw_prim
  hs-source-dirs:     src
  other-extensions:   AllowAmbiguousTypes

test-suite hw-prim-test
  import:   base
          , common
          , QuickCheck
          , bytestring
          , directory
          , exceptions
          , hedgehog
          , hspec
          , hw-hspec-hedgehog
          , mmap
          , semigroups
          , transformers
          , vector
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
    HaskellWorks.Data.ByteStringSpec
    HaskellWorks.Data.FoldableSpec
    HaskellWorks.Data.FromByteStringSpec
    HaskellWorks.Data.FromForeignRegionSpec
    HaskellWorks.Data.SearchSpec
    HaskellWorks.Data.Vector.AsVector64nsSpec
    HaskellWorks.Data.Vector.AsVector64sSpec
    HaskellWorks.Data.Vector.AsVector8nsSpec
    HaskellWorks.Data.Vector.AsVector8sSpec
    HaskellWorks.Data.Vector.StorableSpec
    Paths_hw_prim
  autogen-modules:    Paths_hw_prim
  build-depends:      hw-prim
  hs-source-dirs:     test
  other-extensions:   AllowAmbiguousTypes
  ghc-options:        -threaded -rtsopts -with-rtsopts=-N
  build-tool-depends: hspec-discover:hspec-discover

benchmark bench
  import:   base
          , common
          , bytestring
          , criterion
          , mmap
          , semigroups
          , transformers
          , vector
  type:               exitcode-stdio-1.0
  main-is:            Main.hs
  other-modules:      Paths_hw_prim
  hs-source-dirs:     bench
  build-depends:      hw-prim
  other-extensions:   AllowAmbiguousTypes
  autogen-modules:    Paths_hw_prim
