cabal-version:      2.4

-- Initial package description 'balkon.cabal' generated by
-- 'cabal init'. For further documentation, see:
--   http://haskell.org/cabal/users-guide/
--
-- The name of the package.
name:               balkon

-- The package version.
-- See the Haskell package versioning policy (PVP) for standards
-- guiding when and how versions should be incremented.
-- https://pvp.haskell.org
-- PVP summary:     +-+------- breaking API changes
--                  | | +----- non-breaking API additions
--                  | | | +--- code changes with no API change
version:            1.0.0.0

stability:          experimental

-- A short (one-line) description of the package.
synopsis:           Text layout engine built on top of HarfBuzz.

-- A longer description of the package.
description:
    .
    Given an input text and formatting options, Balkón produces an inline
    layout with defined glyph positions and box coordinates, all within a
    containing unit called a paragraph. See "Data.Text.ParagraphLayout.Rich".
    .
    Internally, HarfBuzz is used to shape individual runs of text, each of
    which fits within one line and has a constant script, direction, language,
    and formatting. Balkón abstracts this so that you can provide text with any
    mix of these attributes and a desired line width for line breaking.
    .
    Additionally, Balkón can be used for breaking an inline layout into pages.
    See "Data.Text.ParagraphLayout".

-- URL for the project homepage or repository.
homepage:           https://argonaut-constellation.org/

-- A URL where users can report bugs.
bug-reports:        https://todo.argonaut-constellation.org/~jaro/balkon

-- The license under which the package is released.
license:            GPL-3.0-only

-- The file containing the license text.
license-file:       LICENSE

-- The package author(s).
author:             Jaro

-- An email address to which users can send suggestions, bug reports,
-- and patches.
maintainer:         jaro@argonaut-constellation.org

-- A copyright notice.
-- copyright:

category:           Text

-- Extra doc files to be distributed with the package, such as a CHANGELOG
-- or a README.
extra-doc-files:
    CHANGELOG.md,
    README.md,
    TESTING.md

-- Extra source files to be distributed with the package, such as examples,
-- or a tutorial module.
extra-source-files:
    cabal.project,
    .golden/**/*.fontInfo,
    .golden/**/*.golden,
    assets/fonts/plex/IBMPlexSansArabic-Regular.ttf,
    assets/fonts/plex/OFL.txt,
    assets/fonts/sarala/OFL.txt,
    assets/fonts/sarala/Sarala-Regular.ttf,
    assets/fonts/ubuntu/LICENCE.txt,
    assets/fonts/ubuntu/TRADEMARKS.txt,
    assets/fonts/ubuntu/Ubuntu-R.ttf,
    assets/fonts/ubuntu/copyright.txt

source-repository head
    type:             git
    location:         https://git.argonaut-constellation.org/~jaro/balkon
    branch:           main

common language
    -- Base language which the package is written in.
    default-language: Haskell2010
    -- LANGUAGE extensions used by modules in this package.
    -- other-extensions:

common warnings
    ghc-options:      -Wall

library balkon-internal
    import:           language, warnings

    -- Directories containing source files of the internal library.
    hs-source-dirs:   src

    -- Modules exported to tests and to the public part of the library.
    exposed-modules:
        Data.Text.ParagraphLayout.Internal.AncestorBox,
        Data.Text.ParagraphLayout.Internal.ApplyBoxes,
        Data.Text.ParagraphLayout.Internal.BiDiReorder,
        Data.Text.ParagraphLayout.Internal.BoxOptions,
        Data.Text.ParagraphLayout.Internal.Break,
        Data.Text.ParagraphLayout.Internal.Fragment,
        Data.Text.ParagraphLayout.Internal.LineHeight,
        Data.Text.ParagraphLayout.Internal.LinePagination,
        Data.Text.ParagraphLayout.Internal.Paginable,
        Data.Text.ParagraphLayout.Internal.ParagraphConstruction,
        Data.Text.ParagraphLayout.Internal.ParagraphOptions,
        Data.Text.ParagraphLayout.Internal.Plain,
        Data.Text.ParagraphLayout.Internal.Plain.Paragraph,
        Data.Text.ParagraphLayout.Internal.Plain.ParagraphLayout,
        Data.Text.ParagraphLayout.Internal.Rect,
        Data.Text.ParagraphLayout.Internal.ResolvedBox,
        Data.Text.ParagraphLayout.Internal.ResolvedSpan,
        Data.Text.ParagraphLayout.Internal.Rich,
        Data.Text.ParagraphLayout.Internal.Rich.Paragraph,
        Data.Text.ParagraphLayout.Internal.Rich.ParagraphLayout,
        Data.Text.ParagraphLayout.Internal.Run,
        Data.Text.ParagraphLayout.Internal.Span,
        Data.Text.ParagraphLayout.Internal.TextContainer,
        Data.Text.ParagraphLayout.Internal.TextOptions,
        Data.Text.ParagraphLayout.Internal.Tree,
        Data.Text.ParagraphLayout.Internal.WithSpan,
        Data.Text.ParagraphLayout.Internal.Zipper

    -- Modules used purely internally and not in any tests.
    other-modules:
        Data.Text.ParagraphLayout.Internal.Layout,
        Data.Text.ParagraphLayout.Internal.ParagraphExtents,
        Data.Text.ParagraphLayout.Internal.ParagraphLine,
        Data.Text.ParagraphLayout.Internal.ProtoFragment,
        Data.Text.ParagraphLayout.Internal.ProtoLine,
        Data.Text.ParagraphLayout.Internal.Script

    build-depends:
        base >=4.12 && < 4.16,
        harfbuzz-pure >=1.0.3.2 && <1.1,
        text >=2.0.2 && <3,
        text-icu >=0.8.0.2 && <0.9,
        unicode-data-scripts >=0.2.0.1 && < 0.3

library
    import:           language, warnings

    -- Directories containing source files.
    hs-source-dirs:   lib

    -- Modules exported by the library.
    exposed-modules:
        Data.Text.ParagraphLayout,
        Data.Text.ParagraphLayout.ParagraphConstruction,
        Data.Text.ParagraphLayout.Plain,
        Data.Text.ParagraphLayout.Rect,
        Data.Text.ParagraphLayout.Rich

    -- Other library packages from which modules are imported.
    build-depends:
        base >=4.12 && <4.16,
        balkon-internal

test-suite balkon-test
    import:           language, warnings

    -- The interface type and version of the test suite.
    type:             exitcode-stdio-1.0

    -- Directories containing source files.
    hs-source-dirs:   test

    -- The entrypoint to the test suite.
    main-is:          Spec.hs

    other-modules:
        Data.Text.ParagraphLayout.FontLoader,
        Data.Text.ParagraphLayout.Internal.ApplyBoxesSpec,
        Data.Text.ParagraphLayout.Internal.BiDiReorderSpec,
        Data.Text.ParagraphLayout.Internal.BreakSpec,
        Data.Text.ParagraphLayout.Internal.LinePaginationSpec,
        Data.Text.ParagraphLayout.Internal.RunSpec,
        Data.Text.ParagraphLayout.Internal.TextContainerSpec,
        Data.Text.ParagraphLayout.Internal.TreeSpec,
        Data.Text.ParagraphLayout.Internal.ZipperSpec,
        Data.Text.ParagraphLayout.Plain.ParagraphData,
        Data.Text.ParagraphLayout.PlainSpec,
        Data.Text.ParagraphLayout.PrettyShow,
        Data.Text.ParagraphLayout.PrettyShow.Golden,
        Data.Text.ParagraphLayout.RectSpec,
        Data.Text.ParagraphLayout.Rich.ParagraphData,
        Data.Text.ParagraphLayout.RichSpec,
        Data.Text.ParagraphLayout.SpanData

    -- Test dependencies.
    build-depends:
        base,
        balkon,
        balkon-internal,
        bytestring >=0.11.4 && < 0.12,
        filepath >=1.4.2.1 && < 1.5,
        harfbuzz-pure,
        hspec >=2.10.9 && <2.11,
        hspec-discover >=2.10.9 && <2.11,
        hspec-golden >=0.2.0.1 && <0.3,
        text,
        text-icu

    build-tool-depends:
        hspec-discover:hspec-discover
