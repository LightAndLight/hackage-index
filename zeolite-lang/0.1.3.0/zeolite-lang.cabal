cabal-version:       2.2

name:                zeolite-lang
version:             0.1.3.0
synopsis:            Zeolite is a statically-typed, general-purpose programming language.

description:
  Zeolite is an experimental general-purpose programming language. See
  <https://github.com/ta0kira/zeolite Zeolite on GitHub> for more details.
  .
  The installation process is still a bit rough, and therefore must be done in a
  few stages:
  .
  * Ensure that you have a C++ compiler such as @clang++@ or @g++@ installed,
    and an archiver such as @ar@ installed, all callable from a shell.
  * Install the binaries using @cabal@. After this step, the compiler itself is
    installed, but it cannot actually create executables from source code.
    .
    @
    cabal install zeolite-lang
    @
    .
  * Execute the setup binary that gets installed by @cabal@. This will give you
    a series of prompts to verify the binaries above. It will then
    automatically build the supporting libraries.
    .
    @
    zeolite-setup
    @
    .
  * (Optional) Once the setup above is completed, you should run the
    integration tests to ensure that code can be compiled and run. These can
    take quite a while to complete. Please create an
    <https://github.com/ta0kira/zeolite/issues issue on GitHub> if you encounter
    any errors.
    .
    @
    ZEOLITE_PATH=$(zeolite --get-path)
    zeolite -p "$ZEOLITE_PATH" -t tests lib\/file lib\/util
    @
  .
  The <https://github.com/ta0kira/zeolite/tree/master/example code examples> are
  located in @$ZEOLITE_PATH/example@. You should not normally need to use
  @$ZEOLITE_PATH@ outside of running included tests and examples.

homepage:            https://github.com/ta0kira/zeolite
license:             Apache-2.0
license-file:        LICENSE
author:              Kevin P. Barry
maintainer:          Kevin P. Barry <ta0kira@gmail.com>
copyright:           (c) Kevin P. Barry 2019-2020
category:            Compiler
build-type:          Simple

tested-with:         GHC == 8.10.1,
                     GHC == 8.8.3,
                     GHC == 8.6.5,
                     GHC == 8.4.4,
                     GHC == 8.2.2,
                     GHC == 8.0.2

extra-source-files:  ChangeLog.md
extra-source-files:  src/Test/testfiles/*.0rt,
                     src/Test/testfiles/*.0rx

data-files:          base/.zeolite-module,
                     base/*.0rp,
                     base/*.cpp,
                     base/*.hpp,
                     base/.zeolite-module,
                     base/*.0rp,
                     base/*.cpp,
                     capture-thread/include/*.h,
                     capture-thread/src/*.cc,
                     example/hello/README.md,
                     example/hello/*.0rx,
                     example/regex/README.md,
                     example/regex/*.0rp,
                     example/regex/*.0rt,
                     example/regex/*.0rx,
                     example/tree/README.md,
                     example/tree/*.0rp,
                     example/tree/*.0rt,
                     example/tree/*.0rx,
                     lib/file/.zeolite-module,
                     lib/file/*.0rp,
                     lib/file/*.0rt,
                     lib/file/*.cpp,
                     lib/util/.zeolite-module,
                     lib/util/*.0rp,
                     lib/util/*.0rt,
                     lib/util/*.0rx,
                     lib/util/*.cpp,
                     tests/.zeolite-module,
                     tests/*.0rt,
                     tests/multiple-defs/README.md,
                     tests/multiple-defs/.zeolite-module,
                     tests/multiple-defs/*.0rp,
                     tests/multiple-defs/*.0rx,
                     tests/visibility/.zeolite-module,
                     tests/visibility/*.0rp,
                     tests/visibility/*.0rx,
                     tests/visibility/internal/.zeolite-module,
                     tests/visibility/internal/*.0rp,
                     tests/visibility/internal/*.0rx,
                     tests/visibility2/.zeolite-module,
                     tests/visibility2/*.0rp,
                     tests/visibility2/*.0rx,
                     tests/visibility2/internal/.zeolite-module,
                     tests/visibility2/internal/*.0rp,
                     tests/visibility2/internal/*.0rx


common defaults
  default-language:    Haskell2010
  ghc-options:         -Wall
  if impl(ghc >= 8)
    ghc-options: -Wno-orphans -Wno-unused-top-binds


library zeolite-internal
  import:              defaults

  exposed-modules:     Base.CompileError,
                       Base.Mergeable,
                       Cli.CompileMetadata,
                       Cli.CompileOptions,
                       Cli.Compiler,
                       Cli.ParseCompileOptions,
                       Cli.TestRunner,
                       Compilation.CompileInfo,
                       Compilation.CompilerState,
                       Compilation.ProcedureContext,
                       Compilation.ScopeContext,
                       CompilerCxx.Category,
                       CompilerCxx.CategoryContext,
                       CompilerCxx.Code,
                       CompilerCxx.Naming,
                       CompilerCxx.Procedure,
                       Config.LoadConfig,
                       Config.Paths,
                       Config.Programs,
                       Parser.Common,
                       Parser.DefinedCategory,
                       Parser.IntegrationTest,
                       Parser.Procedure,
                       Parser.SourceFile,
                       Parser.TypeCategory,
                       Parser.TypeInstance,
                       Test.Common,
                       Test.DefinedCategory,
                       Test.IntegrationTest,
                       Test.Parser,
                       Test.Procedure,
                       Test.TypeCategory,
                       Test.TypeInstance,
                       Types.Builtin,
                       Types.DefinedCategory,
                       Types.Function,
                       Types.GeneralType,
                       Types.IntegrationTest,
                       Types.Positional,
                       Types.Procedure,
                       Types.TypeCategory,
                       Types.TypeInstance,
                       Types.Variance

  other-modules:       Paths_zeolite_lang

  autogen-modules:     Paths_zeolite_lang

  other-extensions:    CPP,
                       ExistentialQuantification,
                       FlexibleContexts,
                       FlexibleInstances,
                       FunctionalDependencies,
                       MultiParamTypeClasses,
                       Safe,
                       ScopedTypeVariables

  build-depends:       base >= 4.8 && < 4.15,
                       containers >= 0.3 && < 0.7,
                       directory >= 1.2.3 && < 1.4,
                       filepath >= 1.0 && < 1.5,
                       hashable >= 1.0 && < 1.4,
                       mtl >= 1.0 && < 2.3,
                       parsec >= 3.0 && < 3.2,
                       regex-tdfa >= 1.0 && < 1.4,
                       transformers >= 0.1 && < 0.6,
                       unix >= 2.6 && <= 2.8

  hs-source-dirs:      src


executable zeolite
  import:              defaults

  main-is:             bin/zeolite.hs

  build-depends:       base,
                       containers,
                       directory,
                       filepath,
                       unix,
                       zeolite-internal


executable zeolite-setup
  import:              defaults

  main-is:             bin/zeolite-setup.hs

  build-depends:       base,
                       directory,
                       filepath,
                       zeolite-internal


test-suite zeolite-test
  import:              defaults

  type:                exitcode-stdio-1.0

  main-is:             bin/unit-tests.hs

  build-depends:       base,
                       directory,
                       filepath,
                       zeolite-internal
