cabal-version:   1.18
name:            faktory
version:         1.0.3.1
license:         MIT
license-file:    LICENSE
copyright:       2018 Freckle Education
maintainer:      engineering@freckle.com
author:          Freckle Engineering
homepage:        https://github.com/frontrowed/faktory_worker_haskell#readme
bug-reports:     https://github.com/frontrowed/faktory_worker_haskell/issues
synopsis:        Faktory Worker for Haskell
description:
    Haskell client and worker process for the Faktory background job server.
    .
    == Architecture overview
    .
    @
    |                        +--------------------+
    |                        |                    |
    |                        |     Faktory        |
    |                        |     Server         |
    |         +---------->>>>|                    +>>>>--------+
    |         |              |                    |            |
    |         |              |                    |            |
    |         |              +--------------------+            |
    | +-----------------+                            +-------------------+
    | |                 |                            |                   |
    | |    Client       |                            |     Worker        |
    | |    pushes       |                            |     pulls         |
    | |     jobs        |                            |      jobs         |
    | |                 |                            |                   |
    | |                 |                            |                   |
    | +-----------------+                            +-------------------+
    @
    .
    * `Client` - an API any process can use to push jobs to the Faktory server.
    * `Worker` - a process that pulls jobs from Faktory and executes them.
    * `Server` - the Faktory daemon which stores background jobs in queues to be processed by Workers.
    .
    This package contains only the `Client` and `Worker`.

category:        Network
build-type:      Simple
extra-doc-files:
    CHANGELOG.md
    README.lhs

source-repository head
    type:     git
    location: https://github.com/frontrowed/faktory_worker_haskell

library
    exposed-modules:
        Faktory.Client
        Faktory.Connection
        Faktory.Ent.Batch
        Faktory.Ent.Tracking
        Faktory.Job
        Faktory.Job.Custom
        Faktory.JobOptions
        Faktory.JobState
        Faktory.Prelude
        Faktory.Producer
        Faktory.Protocol
        Faktory.Settings
        Faktory.Worker

    hs-source-dirs:     library
    other-modules:      Paths_faktory
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missed-specialisations -Wno-all-missed-specialisations
        -Wno-unsafe -Wno-safe -Wno-missing-local-signatures
        -Wno-monomorphism-restriction -Wno-missing-import-lists

    build-depends:
        aeson >=1.5.5.1,
        aeson-casing >=0.2.0.0,
        base >=4.13 && <5,
        bytestring >=0.10.12.0,
        connection >=0.3.1,
        cryptonite >=0.27,
        errors >=2.3.0,
        megaparsec >=9.0.1,
        memory >=0.15.0,
        mtl >=2.2.2,
        network >=3.1.1.1,
        random >=1.1,
        safe-exceptions >=0.1.7.1,
        scanner >=0.3.1,
        semigroups >=0.19.1,
        text >1.2,
        time >=1.9.3,
        unix >=2.7.2.2,
        unordered-containers >=0.2.13.0

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

executable faktory-example-consumer
    main-is:            Main.hs
    hs-source-dirs:     examples/consumer
    other-modules:      Paths_faktory
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missed-specialisations -Wno-all-missed-specialisations
        -Wno-unsafe -Wno-safe -Wno-missing-local-signatures
        -Wno-monomorphism-restriction -Wno-missing-import-lists

    build-depends:
        aeson >=1.5.5.1,
        base >=4.13 && <5,
        faktory -any,
        safe-exceptions >=0.1.7.1

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

executable faktory-example-producer
    main-is:            Main.hs
    hs-source-dirs:     examples/producer
    other-modules:      Paths_faktory
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missed-specialisations -Wno-all-missed-specialisations
        -Wno-unsafe -Wno-safe -Wno-missing-local-signatures
        -Wno-monomorphism-restriction -Wno-missing-import-lists

    build-depends:
        aeson >=1.5.5.1,
        base >=4.13 && <5,
        faktory -any,
        safe-exceptions >=0.1.7.1

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

test-suite hspec
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    hs-source-dirs:     tests
    other-modules:
        Faktory.ConnectionSpec
        Faktory.Ent.BatchSpec
        Faktory.Ent.TrackingSpec
        Faktory.JobOptionsSpec
        Faktory.Test
        FaktorySpec
        Paths_faktory

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missed-specialisations -Wno-all-missed-specialisations
        -Wno-unsafe -Wno-safe -Wno-missing-local-signatures
        -Wno-monomorphism-restriction -Wno-missing-import-lists -rtsopts

    build-depends:
        aeson >=1.5.5.1,
        async >=2.2.2,
        base >=4.13 && <5,
        faktory -any,
        hspec >=2.7.8,
        time >=1.9.3

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

test-suite readme
    type:               exitcode-stdio-1.0
    main-is:            README.lhs
    hs-source-dirs:     ./
    other-modules:      Paths_faktory
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missed-specialisations -Wno-all-missed-specialisations
        -Wno-unsafe -Wno-safe -Wno-missing-local-signatures
        -Wno-monomorphism-restriction -Wno-missing-import-lists -pgmL
        markdown-unlit

    build-depends:
        aeson >=1.5.5.1,
        base >=4.13 && <5,
        faktory -any,
        markdown-unlit >=0.5.1

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module
