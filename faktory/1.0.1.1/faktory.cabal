cabal-version: 1.18

-- This file has been generated from package.yaml by hpack version 0.31.2.
--
-- see: https://github.com/sol/hpack
--
-- hash: fe59297ff90aa80416c1575f9e03019036b581f05a2ebccc851f13748e618b5e

name:           faktory
version:        1.0.1.1
synopsis:       Faktory Worker for Haskell
description:    Haskell client and worker process for the Faktory background job server.
                .
                == Architecture overview
                .
                @
                |                        +--------------------+
                |                        |                    |
                |                        |     Faktory        |
                |                        |     Server         |
                |         +---------->>>>|                    +>>>>--------+
                |         |              |                    |            |
                |         |              |                    |            |
                |         |              +--------------------+            |
                | +-----------------+                            +-------------------+
                | |                 |                            |                   |
                | |    Client       |                            |     Worker        |
                | |    pushes       |                            |     pulls         |
                | |     jobs        |                            |      jobs         |
                | |                 |                            |                   |
                | |                 |                            |                   |
                | +-----------------+                            +-------------------+
                @
                .
                * `Client` - an API any process can use to push jobs to the Faktory server.
                * `Worker` - a process that pulls jobs from Faktory and executes them.
                * `Server` - the Faktory daemon which stores background jobs in queues to be processed by Workers.
                .
                This package contains only the `Client` and `Worker`.
category:       Network
homepage:       https://github.com/frontrowed/faktory_worker_haskell#readme
bug-reports:    https://github.com/frontrowed/faktory_worker_haskell/issues
author:         Freckle Engineering
maintainer:     engineering@freckle.com
copyright:      2018 Freckle Education
license:        MIT
license-file:   LICENSE
build-type:     Simple
extra-doc-files:
    CHANGELOG.md
    README.lhs

source-repository head
  type: git
  location: https://github.com/frontrowed/faktory_worker_haskell

library
  exposed-modules:
      Faktory.Client
      Faktory.Connection
      Faktory.Job
      Faktory.Prelude
      Faktory.Protocol
      Faktory.Settings
      Faktory.Worker
  other-modules:
      Paths_faktory
  hs-source-dirs:
      library
  default-extensions: BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving TypeApplications TypeFamilies
  ghc-options: -Weverything -Wno-missing-exported-signatures -Wno-missed-specialisations -Wno-all-missed-specialisations -Wno-unsafe -Wno-safe -Wno-missing-local-signatures -Wno-monomorphism-restriction -Wno-missing-import-lists
  build-depends:
      aeson >=1.3 && <2
    , aeson-casing >=0.1 && <1
    , base >=4.11 && <5
    , bytestring >=0.1 && <1
    , connection >=0.2 && <1
    , cryptonite >=0.2 && <1
    , megaparsec >=7 && <8.1
    , memory >=0.1 && <1
    , network >=2.6 && <3.2
    , random >=1.1 && <2
    , safe-exceptions >=0.1 && <1
    , scanner >=0.2 && <1
    , text >=1.2 && <2
    , time >=1.8 && <2
    , unix >=2.7 && <3
  default-language: Haskell2010

executable faktory-example-consumer
  main-is: Main.hs
  other-modules:
      Paths_faktory
  hs-source-dirs:
      examples/consumer
  default-extensions: BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving TypeApplications TypeFamilies
  ghc-options: -Weverything -Wno-missing-exported-signatures -Wno-missed-specialisations -Wno-all-missed-specialisations -Wno-unsafe -Wno-safe -Wno-missing-local-signatures -Wno-monomorphism-restriction -Wno-missing-import-lists
  build-depends:
      aeson
    , base >=4.11 && <5
    , faktory
    , safe-exceptions
  default-language: Haskell2010

executable faktory-example-producer
  main-is: Main.hs
  other-modules:
      Paths_faktory
  hs-source-dirs:
      examples/producer
  default-extensions: BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving TypeApplications TypeFamilies
  ghc-options: -Weverything -Wno-missing-exported-signatures -Wno-missed-specialisations -Wno-all-missed-specialisations -Wno-unsafe -Wno-safe -Wno-missing-local-signatures -Wno-monomorphism-restriction -Wno-missing-import-lists
  build-depends:
      aeson
    , base >=4.11 && <5
    , faktory
    , safe-exceptions
  default-language: Haskell2010

test-suite hspec
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      Faktory.ConnectionSpec
      FaktorySpec
      Paths_faktory
  hs-source-dirs:
      tests
  default-extensions: BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving TypeApplications TypeFamilies
  ghc-options: -Weverything -Wno-missing-exported-signatures -Wno-missed-specialisations -Wno-all-missed-specialisations -Wno-unsafe -Wno-safe -Wno-missing-local-signatures -Wno-monomorphism-restriction -Wno-missing-import-lists -rtsopts
  build-depends:
      base >=4.11 && <5
    , faktory
    , hspec
  default-language: Haskell2010

test-suite readme
  type: exitcode-stdio-1.0
  main-is: README.lhs
  other-modules:
      Paths_faktory
  hs-source-dirs:
      ./.
  default-extensions: BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving TypeApplications TypeFamilies
  ghc-options: -Weverything -Wno-missing-exported-signatures -Wno-missed-specialisations -Wno-all-missed-specialisations -Wno-unsafe -Wno-safe -Wno-missing-local-signatures -Wno-monomorphism-restriction -Wno-missing-import-lists -pgmL markdown-unlit
  build-depends:
      aeson
    , base
    , faktory
    , markdown-unlit
  default-language: Haskell2010
