name: capataz
version: 0.0.0.2
cabal-version: >=1.10
build-type: Simple
license: MIT
license-file: LICENSE
copyright: © 2018 Roman Gonzalez
maintainer: capataz@roman-gonzalez.info
stability: alpha (experimental)
homepage: https://github.com/roman/Haskell-capataz#readme
bug-reports: https://github.com/roman/Haskell-capataz/issues
synopsis: OTP-like supervision trees in Haskell
description:
    `capataz` enhances the reliability of your concurrent applications by offering
    supervision of green threads that run in your application.
    .
    Advantages over standard library:
    .
    * Links related long-living processes together under a common capataz
    supervisor, with restart/shutdown order
    .
    * Set restart strategies (Permanent, Transient, Temporary) on `IO`
    sub-routines on a granular level
    .
    * Set restart strategies on a pool of long-living worker threads (AllForOne,
    OneForOne)
    .
    * Complete telemetry on the sub-routine lifecycle of your application (start,
    error, restarts, shutdown)
category: Control, Concurrency
author: Roman Gonzalez
tested-with: GHC ==8.0.1 GHC ==8.0.2 GHC ==8.2.1
extra-source-files:
    CHANGELOG.md
    README.md

source-repository head
    type: git
    location: https://github.com/roman/Haskell-capataz

library
    exposed-modules:
        Control.Concurrent.Capataz
    build-depends:
        async >=2.1.1.1 && <2.2,
        base >=4.10.1.0 && <4.11,
        bytestring >=0.10.8.2 && <0.11,
        data-default >=0.7.1.1 && <0.8,
        protolude ==0.2.*,
        safe-exceptions >=0.1.6.0 && <0.2,
        stm >=2.4.4.1 && <2.5,
        teardown >=0.3.0.0 && <0.4,
        text >=1.2.2.2 && <1.3,
        time >=1.8.0.2 && <1.9,
        unordered-containers >=0.2.8.0 && <0.3,
        uuid >=1.3.13 && <1.4,
        vector >=0.12.0.1 && <0.13
    default-language: Haskell2010
    hs-source-dirs: src
    other-modules:
        Control.Concurrent.Internal.Capataz.Core
        Control.Concurrent.Internal.Capataz.Restart
        Control.Concurrent.Internal.Capataz.Types
        Control.Concurrent.Internal.Capataz.Util
        Control.Concurrent.Internal.Capataz.Worker
        Paths_capataz
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates

test-suite  capataz-test
    type: exitcode-stdio-1.0
    main-is: Main.hs
    build-depends:
        async >=2.1.1.1 && <2.2,
        base >=4.10.1.0 && <4.11,
        bytestring >=0.10.8.2 && <0.11,
        capataz -any,
        data-default >=0.7.1.1 && <0.8,
        pretty-show >=1.6.16 && <1.7,
        protolude ==0.2.*,
        safe-exceptions >=0.1.6.0 && <0.2,
        stm >=2.4.4.1 && <2.5,
        tasty >=1.0.0.1 && <1.1,
        tasty-hunit >=0.10.0.1 && <0.11,
        tasty-rerun >=1.1.8 && <1.2,
        tasty-smallcheck >=0.8.1 && <0.9,
        teardown >=0.3.0.0 && <0.4,
        text >=1.2.2.2 && <1.3,
        time >=1.8.0.2 && <1.9,
        unordered-containers >=0.2.8.0 && <0.3,
        uuid >=1.3.13 && <1.4,
        vector >=0.12.0.1 && <0.13
    default-language: Haskell2010
    hs-source-dirs: test/testsuite
    other-modules:
        Control.Concurrent.CapatazTest
        Paths_capataz
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates -threaded -rtsopts -with-rtsopts=-N
