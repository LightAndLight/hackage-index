Name:                manifolds
Version:             0.1.6.2
x-revision: 1
Category:            Math
Synopsis:            Coordinate-free hypersurfaces
Description:         Manifolds, a generalisation of the notion of &#x201c;smooth curves&#x201d; or surfaces,
                     are topological spaces /locally homeomorphic to a vector space/. This gives
                     rise to what is actually the most natural / mathematically elegant way of dealing
                     with them: calculations can be carried out locally, in connection with Riemannian
                     products etc., in a vector space, the tangent space / tangent bundle.
                     .
                     However, this does not trivially translate to non-local operations. Common
                     ways to carry those out include using a single affine map to cover (almost) all of the manifold
                     (in general not possible homeomorphically, which leads to both topological and geometrical
                     problems), to embed the manifold into a larger-dimensional vector space (which tends
                     to distract from the manifold's own properties and is often not friendly to computations)
                     or approximating the manifold by some kind of finite simplicial mesh (which intrinsically
                     introduces non-differentiability issues and leads to the question of what precision
                     is required).
                     .
                     This library tries to mitigate these problems by using Haskell's
                     functional nature to keep the representation close to the mathematical ideal of
                     local linearity with homeomorphic coordinate transforms, and, where it is
                     necessary to recede to the less elegant alternatives, exploiting lazy evaluation
                     etc. to optimise the compromises that have to be made.
License:             GPL-3
License-file:        COPYING
Author:              Justus Sagemüller
Homepage:            https://github.com/leftaroundabout/manifolds
Maintainer:          (@) sagemueller $ geo.uni-koeln.de
Build-Type:          Simple
Cabal-Version:       >=1.10
Extra-Doc-Files:     images/examples/*.png

Source-Repository head
    type: git
    location: git://github.com/leftaroundabout/manifolds.git

Library
  Build-Depends:     base>=4.5 && < 6
                     , transformers
                     , vector-space>=0.8
                     , MemoTrie
                     , vector
                     , hmatrix >= 0.16 && < 0.18
                     , containers
                     , comonad
                     , semigroups
                     , void
                     , tagged
                     , deepseq
                     , constrained-categories >= 0.2 && < 0.2.5
  other-extensions:  FlexibleInstances
                     , TypeFamilies
                     , FlexibleContexts
                     , GADTs
                     , RankNTypes
                     , ConstraintKinds
                     , PatternGuards
                     , ScopedTypeVariables
                     , RecordWildCards
                     , TupleSections
  ghc-options:       -O2
  Exposed-modules:   Data.Manifold
                     Data.Manifold.PseudoAffine
                     Data.Manifold.TreeCover
                     Data.SimplicialComplex
                     Data.LinearMap.HerMetric
                     Data.Function.Differentiable
                     Data.Manifold.Types
                     Data.Manifold.Griddable
                     Data.Manifold.Riemannian
  Other-modules:   Data.List.FastNub
                   Data.Manifold.Types.Primitive
                   Data.Manifold.Cone
                   Data.CoNat
                   Data.Embedding
                   Data.LinearMap.Category
                   Data.VectorSpace.FiniteDimensional
                   Util.Associate
                   Util.LtdShow
  default-language: Haskell2010
