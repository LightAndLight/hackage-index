-- This file is part of Factory.
--
-- Factory is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Factory is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Factory.  If not, see <http://www.gnu.org/licenses/>.

Name:		factory
Version:	0.2.1.2
Cabal-version:	>= 1.10
Copyright:	(C) 2011-2015 Dr. Alistair Ward
License:	GPL
License-file:	LICENSE
Author:		Dr. Alistair Ward
Stability:	stable
Synopsis:	Rational arithmetic in an irrational world.
Build-type:	Simple
Description:	A library of number-theory functions, for; factorials, square-roots, Pi and primes.
Category:	Math, Number Theory
Tested-with:	GHC == 7.4, GHC == 7.6, GHC == 7.8, GHC == 7.10
Homepage:	http://functionalley.eu/Factory/factory.html
Maintainer:	mailto <colon> factory <at> functionalley <dot> eu
Bug-reports:	mailto <colon> factory <at> functionalley <dot> eu

-- None of these files are needed at run-time.
Extra-source-files:
    changelog.markdown
    copyright
    README.markdown

-- Enable using: 'cabal configure -f llvm'.
flag llvm
    Description:	Whether the 'llvm' compiler-backend has been installed and is required for code-generation.
    Manual:		True
    Default:		False

flag threaded
    Description:	Enable parallelized code.
    Manual:		True
    Default:		True

Library
    Default-language:	Haskell2010
    GHC-options:	-Wall -O2 -fno-warn-tabs
    Hs-source-dirs:	src-lib

    Exposed-modules:
        Factory.Data.Exponential
        Factory.Data.Interval
        Factory.Data.MonicPolynomial
        Factory.Data.Monomial
        Factory.Data.Polynomial
        Factory.Data.PrimeFactors
        Factory.Data.PrimeWheel
        Factory.Data.QuotientRing
        Factory.Data.Ring
        Factory.Math.ArithmeticGeometricMean
        Factory.Math.DivideAndConquer
        Factory.Math.Factorial
        Factory.Math.Fibonacci
        Factory.Math.Hyperoperation
        Factory.Math.Implementations.Factorial
        Factory.Math.Implementations.Pi.AGM.Algorithm
        Factory.Math.Implementations.Pi.AGM.BrentSalamin
        Factory.Math.Implementations.Pi.BBP.Algorithm
        Factory.Math.Implementations.Pi.BBP.Base65536
        Factory.Math.Implementations.Pi.BBP.Bellard
        Factory.Math.Implementations.Pi.BBP.Implementation
        Factory.Math.Implementations.Pi.BBP.Series
        Factory.Math.Implementations.Pi.Borwein.Algorithm
        Factory.Math.Implementations.Pi.Borwein.Borwein1993
        Factory.Math.Implementations.Pi.Borwein.Implementation
        Factory.Math.Implementations.Pi.Borwein.Series
        Factory.Math.Implementations.Pi.Ramanujan.Algorithm
        Factory.Math.Implementations.Pi.Ramanujan.Chudnovsky
        Factory.Math.Implementations.Pi.Ramanujan.Classic
        Factory.Math.Implementations.Pi.Ramanujan.Implementation
        Factory.Math.Implementations.Pi.Ramanujan.Series
        Factory.Math.Implementations.Pi.Spigot.Algorithm
        Factory.Math.Implementations.Pi.Spigot.Gosper
        Factory.Math.Implementations.Pi.Spigot.RabinowitzWagon
        Factory.Math.Implementations.Pi.Spigot.Series
        Factory.Math.Implementations.Pi.Spigot.Spigot
        Factory.Math.Implementations.Primality
        Factory.Math.Implementations.PrimeFactorisation
        Factory.Math.Implementations.Primes.Algorithm
        Factory.Math.Implementations.Primes.SieveOfAtkin
        Factory.Math.Implementations.Primes.SieveOfEratosthenes
        Factory.Math.Implementations.Primes.TrialDivision
        Factory.Math.Implementations.Primes.TurnersSieve
        Factory.Math.Implementations.SquareRoot
        Factory.Math.MultiplicativeOrder
        Factory.Math.PerfectPower
        Factory.Math.Pi
        Factory.Math.Power
        Factory.Math.Precision
        Factory.Math.Primality
        Factory.Math.PrimeFactorisation
        Factory.Math.Primes
        Factory.Math.Probability
        Factory.Math.Radix
        Factory.Math.SquareRoot
        Factory.Math.Statistics
        Factory.Math.Summation

    Build-depends:
        array,
        base >= 4.3 && < 5,
        deepseq >= 1.1,
        containers,
        parallel >= 3.0,
        primes >= 0.1,
        random,
        toolshed >= 0.16

    if impl(ghc >= 7.4.1)
        GHC-prof-options:	-prof -fprof-auto -fprof-cafs
    else
        GHC-prof-options:	-prof -auto-all -caf-all

    if impl(ghc >= 7.0) && flag(llvm)
        GHC-options:	-fllvm

Executable factory
    Default-language:	Haskell2010
    GHC-options:	-Wall -O2 -fno-warn-tabs
    Hs-source-dirs:	src-exe
    Main-is:		Main.hs
    GHC-prof-options:	-prof -auto-all -caf-all

-- Unexposed modules must be referenced for 'cabal sdist'.
    Other-modules:
        Factory.Test.CommandOptions
        Factory.Test.Performance.Factorial
        Factory.Test.Performance.Hyperoperation
        Factory.Test.Performance.Pi
        Factory.Test.Performance.Primality
        Factory.Test.Performance.PrimeFactorisation
        Factory.Test.Performance.Primes
        Factory.Test.Performance.SquareRoot
        Factory.Test.Performance.Statistics

    Build-depends:
        array,
        base >= 4.3 && < 5,
        Cabal >= 1.10,
        containers,
        deepseq >= 1.1,
        factory,
        random,
        toolshed >= 0.16

    if flag(threaded)
        GHC-options:	-threaded

    if impl(ghc >= 7.0)
        GHC-options:	-rtsopts

        if flag(llvm)
            GHC-options:	-fllvm

Test-Suite quickCheck
    Default-language:	Haskell2010
    GHC-options:	-Wall -fno-warn-tabs
    Hs-source-dirs:	src-test
    Main-is:		Main.hs
    Type:		exitcode-stdio-1.0

-- Required for 'cabal sdist'.
    Other-modules:
        Factory.Test.QuickCheck.ArithmeticGeometricMean
        Factory.Test.QuickCheck.Factorial
        Factory.Test.QuickCheck.Hyperoperation
        Factory.Test.QuickCheck.Interval
        Factory.Test.QuickCheck.MonicPolynomial
        Factory.Test.QuickCheck.PerfectPower
        Factory.Test.QuickCheck.Pi
        Factory.Test.QuickCheck.Polynomial
        Factory.Test.QuickCheck.Power
        Factory.Test.QuickCheck.Primality
        Factory.Test.QuickCheck.PrimeFactorisation
        Factory.Test.QuickCheck.Primes
        Factory.Test.QuickCheck.Probability
        Factory.Test.QuickCheck.Radix
        Factory.Test.QuickCheck.SquareRoot
        Factory.Test.QuickCheck.Statistics
        Factory.Test.QuickCheck.Summation

    Build-depends:
        array,
        base >= 4.3 && < 5,
        containers,
        deepseq >= 1.1,
        factory,
        primes >= 0.1,
        QuickCheck >= 2.2,
        random,
        toolshed >= 0.16
