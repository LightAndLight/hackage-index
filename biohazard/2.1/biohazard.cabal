Cabal-version:       2.0
Name:                biohazard
Version:             2.1
Synopsis:            bioinformatics support library
Description:         This is a collection of modules I separated from
                     various bioinformatics tools.
Category:            Bioinformatics

Homepage:            https://bitbucket.org/ustenzel/biohazard
License:             BSD3
License-File:        LICENSE
Extra-Source-Files:  CHANGELOG.md

Author:              Udo Stenzel
Maintainer:          u.stenzel@web.de
Copyright:           (C) 2010-2019 Udo Stenzel

Build-type:          Custom
Tested-with:         GHC == 7.10.3, GHC == 8.0.2, GHC == 8.2.2, GHC == 8.4.3, GHC == 8.6.1, GHC == 8.8.1

source-repository head
  type:     git
  location: https://bitbucket.org/ustenzel/biohazard.git

custom-setup
  setup-depends:       base                     >= 4.8 && < 4.14,
                       Cabal                    >= 2.0 && < 3.1

Library
  Exposed-modules:     Bio.Adna,
                       Bio.Align,
                       Bio.Bam,
                       Bio.Bam.Evan,
                       Bio.Bam.Fastq,
                       Bio.Bam.Filter,
                       Bio.Bam.Header,
                       Bio.Bam.Index,
                       Bio.Bam.Pileup,
                       Bio.Bam.Reader
                       Bio.Bam.Rec,
                       Bio.Bam.Regions,
                       Bio.Bam.Rmdup,
                       Bio.Bam.Trim,
                       Bio.Bam.Writer,
                       Bio.Base,
                       Bio.Prelude,
                       Bio.Streaming,
                       Bio.Streaming.Bgzf,
                       Bio.Streaming.Bytes,
                       Bio.Streaming.Furrow,
                       Bio.Streaming.Parse,
                       Bio.Streaming.Vector,
                       Bio.TwoBit,
                       Bio.Util.MMap,
                       Bio.Util.Nub,
                       Bio.Util.Numeric,
                       Bio.Util.Storable,
                       Bio.Util.Text,
                       Control.Monad.Log

  Other-modules:       Paths_biohazard, GitVersion
  Autogen-modules:     Paths_biohazard, GitVersion

  Build-depends:       attoparsec               >= 0.13 && < 0.14,
                       base                     >= 4.8 && < 4.14,
                       base-prelude             >= 1.2 && < 1.2.1,
                       bytestring               >= 0.10.6 && < 0.11,
                       containers               >= 0.5 && < 0.7,
                       directory                >= 1.2.2 && < 1.4,
                       exceptions               >= 0.6.1 && < 0.11,
                       hashable                 >= 1.2.3.2 && < 1.4,
                       monad-control            == 1.0.*,
                       optparse-applicative     >= 0.13 && < 0.16,
                       primitive                >= 0.6.1 && < 0.8,
                       stm                      >= 2.4 && < 2.6,
                       streaming                >= 0.1.4.2 && < 0.3,
                       text                     >= 1.2.0.2 && < 1.3,
                       transformers             >= 0.4.2 && < 0.6,
                       transformers-base        == 0.4.*,
                       unix                     >= 2.7.1 && < 2.8,
                       unordered-containers     >= 0.2.5.1 && < 0.3,
                       vector                   >= 0.11 && < 0.13,
                       vector-algorithms        >= 0.8 && < 0.9,
                       zlib                     == 0.6.*

  if !impl(ghc >= 8.0)
    build-depends: semigroups >= 0.18 && < 0.20

  Ghc-options:         -Wall
  if impl(ghc >= 8.0)
    Ghc-options:       -Wincomplete-uni-patterns -Wredundant-constraints
                       -Wcompat -Wincomplete-record-updates -Widentities
  if impl(ghc >= 8.4)
    Ghc-options:       -Wmissing-export-lists -Wpartial-fields

  Default-Language:    Haskell2010

  Default-Extensions:  BangPatterns,
                       DeriveDataTypeable,
                       DeriveGeneric,
                       FlexibleContexts,
                       FlexibleInstances,
                       GeneralizedNewtypeDeriving,
                       LambdaCase,
                       MultiParamTypeClasses,
                       NoImplicitPrelude,
                       OverloadedStrings,
                       RecordWildCards,
                       TypeSynonymInstances

  Other-Extensions:    CPP,
                       ExistentialQuantification,
                       ForeignFunctionInterface,
                       Rank2Types,
                       StandaloneDeriving,
                       TypeFamilies,
                       UndecidableInstances

  Build-tool-depends:  hsc2hs:hsc2hs
  Hs-source-dirs:      .
  Include-dirs:        cbits
  Install-includes:    myers_align.h
  C-sources:           cbits/loops.c,
                       cbits/myers_align.c,
                       cbits/trim.c
                       cbits/util.c
                       cbits/zlib.c

-- :vim:tw=132:
