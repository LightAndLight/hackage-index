Name:           llvm-extra
Version:        0.8.0.1
License:        BSD3
License-File:   LICENSE
Author:         Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:     Henning Thielemann <haskell@henning-thielemann.de>
Homepage:       https://wiki.haskell.org/LLVM
Category:       Compilers/Interpreters, Code Generation
Synopsis:       Utility functions for the llvm interface
Description:
  The Low-Level Virtual-Machine is a compiler back-end with optimizer.
  You may also call it a high-level portable assembler.
  This package provides various utility functions
  for the Haskell interface to LLVM, for example:
  .
  * arithmetic operations with more general types
    but better type inference than the @llvm@ interface
    in "LLVM.Extra.Arithmetic",
  .
  * a type class for loading and storing sets of values with one command (macro)
    in "LLVM.Extra.Memory",
  .
  * support instance declarations of LLVM classes
    in "LLVM.Extra.Class",
  .
  * handling of termination by a custom monad on top of @CodeGenFunction@
    in "LLVM.Extra.MaybeContinuation"
  .
  * various kinds of loops (while) and condition structures (if-then-else)
    in "LLVM.Extra.Control"
  .
  * more functional loop construction using "LLVM.Extra.Iterator"
  .
  * complex Haskell values mapped to LLVM values in "LLVM.Extra.Multi.Value"
  .
  * automatic adaption to target specific extensions,
    currently used for access of vector operations
    that are specific to an SSE level on x86 processors
    in "LLVM.Extra.Extension"
    (On x86 architectures we depend on the cpuid package
     that is needed for automatic detection of available features.)
  .
  * advanced vector operations
    such as sum of all vector elements, cumulative sum,
    floor, non-negative fraction, absolute value
    in "LLVM.Extra.Vector"
  .
  * type classes for handling scalar and vector operations
    in a uniform way
    in "LLVM.Extra.ScalarOrVector"
  .
  * a Makefile and a description
    of how to run LLVM code from within GHCi.
Stability:      Experimental
Tested-With:    GHC==7.0.4, GHC==7.4.2, GHC==7.8.2
Tested-With:    GHC==8.4.3, GHC==8.6.1
Cabal-Version:  1.14
Build-Type:     Simple
Extra-Source-Files:
  Makefile
  Problems.txt
  x86/cpuid/LLVM/Extra/ExtensionCheck/X86.hs
  x86/none/LLVM/Extra/ExtensionCheck/X86.hs

Flag buildExamples
  description: Build example executables
  default:     False

Flag buildTools
  description: Build intrinsic translator
  default:     False

Flag cpuid
  description: Use CPUID for host feature detection if available on the architecture
  default:     True

Source-Repository this
  Tag:         0.8.0.1
  Type:        darcs
  Location:    http://code.haskell.org/~thielema/llvm-extra/

Source-Repository head
  Type:        darcs
  Location:    http://code.haskell.org/~thielema/llvm-extra/

Library
  Build-Depends:
    -- llvm must be imported with restrictive version bounds,
    -- because we import implicitly and unqualified
    llvm-tf >=3.1.1 && <3.2,
    tfp >=1.0 && <1.1,
    non-empty >=0.2.1 && <0.4,
    containers >=0.1 && <0.7,
    bool8 >=0.0 && <0.1,
    bifunctors >=5.4 && <6,
    transformers >=0.1.1 && <0.6,
    utility-ht >=0.0.11 && <0.1

  Build-Depends:
    base >=3 && <5

  If (arch(i386) || arch(x86_64)) && flag(cpuid)
    Build-Depends:
      unsafe >=0.0 && <0.1,
      cpuid >=0.2.3 && <0.3
    Hs-Source-Dirs: x86/cpuid
  Else
    -- Instead of calling the cpuid instruction directly
    -- we may ask LLVM's Subtarget detection.
    -- This would also enable cross compilation.
    -- However in LLVM-2.6 this is only available in the C++ interface.
    Hs-Source-Dirs: x86/none

  Default-Language: Haskell98
  GHC-Options: -Wall
  Hs-source-dirs: src
  Exposed-Modules:
    LLVM.Extra.Arithmetic
    LLVM.Extra.Monad
    LLVM.Extra.Memory
    LLVM.Extra.Maybe
    LLVM.Extra.MaybeContinuation
    LLVM.Extra.Either
    LLVM.Extra.Class
    LLVM.Extra.Control
    LLVM.Extra.Extension
    LLVM.Extra.Extension.X86
    LLVM.Extra.ExtensionCheck.X86
    LLVM.Extra.Array
    LLVM.Extra.Scalar
    LLVM.Extra.Vector
    LLVM.Extra.ScalarOrVector
    LLVM.Extra.Iterator
    LLVM.Extra.Multi.Iterator
    LLVM.Extra.Multi.Value
    LLVM.Extra.Multi.Value.Memory
    LLVM.Extra.Multi.Value.Vector
    LLVM.Extra.Multi.Vector
    LLVM.Extra.Multi.Vector.Memory
    LLVM.Extra.Multi.Vector.Instance
    LLVM.Extra.Multi.Class
  Other-Modules:
    LLVM.Extra.ArithmeticPrivate
    LLVM.Extra.MaybePrivate
    LLVM.Extra.EitherPrivate
    LLVM.Extra.MemoryPrivate
    LLVM.Extra.Extension.X86Auto
    LLVM.Extra.Multi.Value.Private

Executable tone-llvm
  If flag(buildExamples)
    Build-Depends:
      llvm-extra,
      llvm-tf,
      tfp,
      non-empty,
      containers >=0.1 && <0.7,
      transformers,
      utility-ht >=0.0.1 && <0.1,
      base >=3 && <5
  Else
    Buildable: False
  Default-Language: Haskell98
  GHC-Options: -Wall
  Main-Is: src/Array.hs

Executable prepare-intrinsics
  If flag(buildTools)
    Build-Depends:
      parsec >=2.1 && <3.2,
      containers >=0.1 && <0.7,
      transformers,
      utility-ht >=0.0.1 && <0.1,
      base >=3 && <5
  Else
    Buildable: False
  Default-Language: Haskell98
  GHC-Options: -Wall
  Main-Is: src/PrepareIntrinsics.hs
