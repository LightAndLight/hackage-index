name:                 bitcoin-payment-channel
version:              0.3.0.0
synopsis:             Library for working with Bitcoin payment channels
description:
    A Bitcoin payment channel allows secure and instant transfer of bitcoins from one
     party to another. Payments are created and verified in less than a millisecond
     (plus network latency), and cannot be double spent, as the receiver of funds is defined
     during channel setup.
    When the channel is closed, the settlement transaction transfers the appropriate value to each
     party, thus paying the Bitcoin transaction fee only once, regardless of the number of payments
     made over the channel.
    The channel setup procedure is trustless, because the funding party - after the chosen
     expiration date - is able to reclaim the bitcoins used to fund the channel,
     in case the receiving party goes missing.
    This library implements a type of payment channel where channel setup
     is safe from transaction ID malleability, and value transfer is uni-directional (one party
     sends and the other party receives) (CHECKLOCKTIMEVERIFY-style).

author:         Rune K. Svendsen <runesvend@gmail.com>
maintainer:     Rune K. Svendsen <runesvend@gmail.com>
license:        PublicDomain
stability:      experimental
homepage:       https://github.com/runeksvendsen/bitcoin-payment-channel
bug-reports:    https://github.com/runeksvendsen/bitcoin-payment-channel/issues
category:       Bitcoin, Finance, Network
build-type:     Simple
cabal-version:  >=1.10

source-repository head
    type:     git
    location: git://github.com/runeksvendsen/bitcoin-payment-channel.git

library
  exposed-modules:      Data.Bitcoin.PaymentChannel,
                        Data.Bitcoin.PaymentChannel.Types,
                        Data.Bitcoin.PaymentChannel.Util,
                        -- Exposed for testing purposes
                        Data.Bitcoin.PaymentChannel.Internal.State

  other-modules:        Data.Bitcoin.PaymentChannel.Internal.Error
                        Data.Bitcoin.PaymentChannel.Internal.Payment,
                        Data.Bitcoin.PaymentChannel.Internal.Refund,
                        Data.Bitcoin.PaymentChannel.Internal.Bitcoin.Amount,
                        Data.Bitcoin.PaymentChannel.Internal.Bitcoin.Script
                        Data.Bitcoin.PaymentChannel.Internal.Bitcoin.Util,
                        Data.Bitcoin.PaymentChannel.Internal.Bitcoin.LockTime,
                        Data.Bitcoin.PaymentChannel.Internal.Settlement,
                        Data.Bitcoin.PaymentChannel.Internal.Types,
                        Data.Bitcoin.PaymentChannel.Internal.Serialization,
                        Data.Bitcoin.PaymentChannel.Internal.Util

  ghc-options:          -W

  build-depends:        base                >= 4.7    && < 5,
                        haskoin-core        >= 0.4.0  && < 0.5.0,
                        base16-bytestring   >= 0.1.0  && < 0.2.0,
                        base64-bytestring   >= 1.0.0  && < 1.1.0,
                        binary              >= 0.7.0  && < 0.9.0,
                        bytestring          >= 0.10.0 && < 0.11.0,
                        cereal              >= 0.5.0  && < 0.6.0,
                        errors              >= 2.0.0  && < 2.2.0,
                        hexstring           >= 0.11.0 && < 0.12.0,
                        text                >= 1.2.0  && < 1.3.0,
                        time                >= 1.5.0  && < 1.7.0,
                        aeson               >= 0.11.0 && < 0.12.0,
                        scientific          >= 0.3.0  && < 0.4.0

  hs-source-dirs:       src

  default-language:     Haskell2010

executable Test
  main-is:              Main.hs

  ghc-options:          -W

  build-depends:        base >= 4.7 && < 5,
                        haskoin-core >= 0.3.0 && < 1.0.0,
                        bytestring, base16-bytestring, base64-bytestring,
                        base58string, hexstring, text,
                        time,
                        cereal, binary, aeson,

                        QuickCheck,
                        bitcoin-payment-channel

  hs-source-dirs:       test

  default-language:     Haskell2010
