name:                   overloaded-records
version:                0.3.0.0
x-revision: 1
synopsis:               Overloaded Records based on current GHC proposal.
description:
  Implementation of /Overloaded Record Fields/ based on current GHC proposal.
  It is built on top of functionality that is included in GHC 8.0.1, but it
  works on older GHC versions as well. Most importantly, this library provides
  Template Haskell functions for automatic deriving of instancess for
  @HasField@ and @SetField@ type classes. With these instances overloaded
  fields can be used directly as getters and lenses.
  .
  See README for usage examples.
  .
  This implementation is highly experimental and may change rapidly.
  .
  More about the current status of OverloadedRecordFields language extension
  can be found on:
  <https://ghc.haskell.org/trac/ghc/wiki/Records/OverloadedRecordFields GHC Wiki: OverloadedRecordFields>.

homepage:               https://github.com/trskop/overloaded-records
bug-reports:            https://github.com/trskop/overloaded-records/issues
license:                BSD3
license-file:           LICENSE
author:                 Peter Trško
maintainer:             peter.trsko@gmail.com
copyright:              (c) 2016, Peter Trško
category:               Data
build-type:             Simple
cabal-version:          >=1.10

extra-source-files:
    ChangeLog.md
  , README.md

flag pedantic
  description:          Pass additional warning flags to GHC.
  default:              False
  manual:               True

library
  hs-source-dirs:       src
  exposed-modules:
      Data.OverloadedLabels
    , Data.OverloadedLabels.TH
    , Data.OverloadedRecords
    , Data.OverloadedRecords.TH
  -- other-modules:

  default-language:     Haskell2010
  other-extensions:
      CPP
    , DataKinds
    , DeriveDataTypeable
    , DeriveGeneric
    , ExplicitForAll
    , FlexibleContexts
    , FlexibleInstances
    , FunctionalDependencies
    , KindSignatures
    , LambdaCase
    , MagicHash
    , MultiParamTypeClasses
    , NoImplicitPrelude
    , RecordWildCards
    , TemplateHaskell
    , TupleSections
    , TypeFamilies
    , UndecidableInstances

  build-depends:
      base >=4.7 && <5
    , data-default-class ==0.0.* || ==0.1.*
    , template-haskell >=2.9 && <2.12

  if impl(ghc >=8.0)
    cpp-options:
      -DHAVE_MONAD_FAIL
      -DHAVE_OVERLOADED_LABELS

  ghc-options:          -Wall
  if flag(pedantic)
    ghc-options:
      -fwarn-tabs
      -fwarn-implicit-prelude
--    -Werror

test-suite unit-tests
  hs-source-dirs:       src, test
  type:                 exitcode-stdio-1.0
  main-is:              unit-tests.hs
  other-modules:
      Data.OverloadedLabels
    , Data.OverloadedLabels.TH
    , Data.OverloadedRecords
    , Data.OverloadedRecords.TH
    , TestCase
    , TestCase.Data.OverloadedRecords

  default-language:     Haskell2010
  other-extensions:
      CPP
    , DataKinds
    , DeriveDataTypeable
    , DeriveGeneric
    , ExplicitForAll
    , FlexibleContexts
    , FlexibleInstances
    , FunctionalDependencies
    , KindSignatures
    , LambdaCase
    , MagicHash
    , MultiParamTypeClasses
    , NoImplicitPrelude
    , RecordWildCards
    , TemplateHaskell
    , TupleSections
    , TypeFamilies
    , UndecidableInstances

  build-depends:
      base >=4.7 && <5
    , data-default-class ==0.0.*
    , template-haskell >=2.9 && <2.12

    -- {{{ Test-suite dependencies --------------------------------------------
    , HUnit >=1.2 && <2
      -- ^ Same constraints as test-framework-hunit
      -- ^ Same constraints as test-framework-quickcheck2==0.3.0.3.
    , test-framework >=0.8 && <1
      -- ^ Same constraint as test-framework-skip, other packages that
      -- depend on it have less restrictive bounds.
    , test-framework-hunit >=0.2.6 && <1
      -- ^ Lower versions have more restrictive bounds on test-framework.
    -- }}} Test-suite dependencies --------------------------------------------

  if impl(ghc >=8.0)
    cpp-options:
      -DHAVE_MONAD_FAIL
      -DHAVE_OVERLOADED_LABELS

  ghc-options:          -Wall
  if flag(pedantic)
    ghc-options:
      -fwarn-tabs
      -fwarn-implicit-prelude
--    -Werror

source-repository head
  type:                 git
  location:             git://github.com/trskop/overloaded-records.git

source-repository this
  type:                 git
  location:             git://github.com/trskop/overloaded-records.git
  tag:                  0.3.0.0
