cabal-version:  2.2
name:           hasklepias
version:        0.17.1
description:    Please see the README on GitHub at <https://github.com/novisci/asclepias#readme>
homepage:       https://github.com/novisci/asclepias/#readme
bug-reports:    https://github.com/novisci/asclepias/issues
author:         Bradley Saul
maintainer:     bsaul@novisci.com
copyright:      NoviSci, Inc
category:       Data Science
synopsis:       embedded DSL for defining epidemiologic cohorts
license:        BSD-3-Clause
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
  type: git
  location: https://github.com/novisci/asclepias

library
  exposed-modules:
      EventData
      EventData.Core
      EventData.Arbitrary
      EventData.Aeson
      EventData.Context
      EventData.Context.Arbitrary
      EventData.Context.Domain
      EventData.Context.Domain.Demographics
      EventData.Context.Domain.Enrollment
      EventData.Accessors
      EventData.Predicates
      Features
      Features.Attributes
      Features.Compose
      Features.Featureable
      Features.Output
      Features.Featureset
      Cohort
      Cohort.AssessmentIntervals
      Cohort.Core
      Cohort.Input
      Cohort.Output
      Cohort.Criteria
      Cohort.Index
      Hasklepias
      Hasklepias.FeatureEvents
      Hasklepias.MakeApp
      Hasklepias.Misc
      Hasklepias.Reexports
      Hasklepias.ReexportsUnsafe
      Hasklepias.Templates
      Hasklepias.Templates.Features
      Hasklepias.Templates.Tests
      Hasklepias.Templates.TestUtilities
      Hasklepias.Templates.Features.Enrollment
      Hasklepias.Templates.Features.NsatisfyP
      Stype
      Stype.Aeson
      Stype.Numeric
      Stype.Numeric.Count
      Stype.Numeric.Continuous
      Stype.Numeric.Censored
      Stype.Categorical
      Stype.Categorical.Binary
      Stype.Categorical.Nominal
  other-modules:
      Paths_hasklepias
  autogen-modules:
      Paths_hasklepias 
  hs-source-dirs:
      src
  build-depends:
      aeson >=1.4.0.0 && <2
    , base >=4.14 && <4.15
    , bytestring == 0.10.12.0
    , cmdargs == 0.10.21
    , containers == 0.6.5.1
    , contravariant >= 1.4
    , co-log == 0.4.0.1
    , flow == 1.0.22
    , ghc-prim == 0.6.1
    , interval-algebra == 0.10.2
    , lens == 5.0.1
    , lens-aeson == 1.1.1
    , mtl == 2.2.2
    , nonempty-containers == 0.3.4.1
    , safe >= 0.3
    , semiring-simple == 1.0.0.1
    , tasty  == 1.4.1
    , tasty-hunit == 0.10.0.3
    , text == 1.2.4.1
    , time >= 1.11
    , tuple == 0.3.0.2
    , QuickCheck
    , unordered-containers == 0.2.14.0
    , vector == 0.12.2.0
    , witherable == 0.4.1
  default-language: Haskell2010

test-suite hasklepias-test
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      EventDataSpec
      EventData.AesonSpec
      EventData.AccessorsSpec
      EventData.ContextSpec
      EventData.Context.DomainSpec
      EventData.Context.Domain.DemographicsSpec
      FeaturesSpec
      Features.OutputSpec
      Features.FeaturesetSpec
      Cohort.InputSpec
      Cohort.CoreSpec
      Cohort.AssessmentIntervalsSpec
      Cohort.CriteriaSpec
      Hasklepias.FeatureEventsSpec
      Stype.AesonSpec
      Stype.Numeric.CensoredSpec
      Paths_hasklepias
  autogen-modules:
      Paths_hasklepias 
  hs-source-dirs:
      test
  ghc-options: -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      aeson >=1.4.0.0 && <2
    , base >=4.7 && <5
    , bytestring >=0.10
    , containers >=0.6.0
    , flow == 1.0.22
    , hasklepias
    , hspec
    , interval-algebra == 0.10.2
    , lens == 5.0.1
    , text >=1.2.3
    , time >=1.11
    , unordered-containers >=0.2.10
    , vector >=0.12
    , QuickCheck
  default-language: Haskell2010

test-suite templates
  type: exitcode-stdio-1.0
  main-is: Main.hs
  hs-source-dirs:
      template-test
  build-depends:
      hasklepias
    , base >=4.14 && <4.15
    , tasty  == 1.4.1
    , tasty-hunit == 0.10.0.3
    , tasty-hspec == 1.2
  default-language: Haskell2010

test-suite examples
  type: exitcode-stdio-1.0
  main-is: Main.hs
  other-modules:
      ExampleEvents
      ExampleFeatures1
      ExampleFeatures2
      ExampleFeatures3
      ExampleFeatures4
      ExampleCohort1
  hs-source-dirs:
      examples
  build-depends:
      hasklepias
    , hspec
    , base >=4.14 && <4.15
    , tasty  == 1.4.1
    , tasty-hunit == 0.10.0.3
    , tasty-hspec == 1.2
  default-language: Haskell2010

executable exampleApp
  -- type: exitcode-stdio-1.0
  main-is: Main.hs
  hs-source-dirs:
      exampleApp
  build-depends:
      hasklepias
  default-language: Haskell2010