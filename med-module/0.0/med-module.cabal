Name:                med-module
Version:             0.0
Synopsis:            Parse song module files from Amiga MED and OctaMED
Description:
  MED (Music EDitor) and its successor OctaMED
  were popular music trackers on the Amiga:
  <https://en.wikipedia.org/wiki/OctaMED>.
  This is a library for parsing the binary module data.
  .
  With the Cabal flag @-fbuildExamples@ you can build two example programs:
  .
  * @unmed2@:
    Loads song module files and show their content on standard output.
  .
  * @animed@:
    Create a PostScript document showing how a song is played.
    Also creates an FFmpeg cue file for the @concat@ input type.
    This way you can create videos with MED music
    plus precisely timed animations of the played tracks.
    See the included @Makefile@ for the command calls
    for creation of complete music videos from MMD files.
License:             GPL-3
License-File:        COPYING
Author:              Claude Heiland-Allen, Henning Thielemann
Maintainer:          haskell@henning-thielemann.de
Category:            Sound
Build-Type:          Simple
Extra-Source-Files:  README.md, Makefile, src/Sound/MED/modules.sh
Cabal-Version:       >=1.10

Source-Repository this
  Tag:         0.0
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/med-module

Source-Repository head
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/med-module

Flag buildExamples
  description: Build example executables
  default:     False

Library
  Exposed-Modules:
    Sound.MED.Basic.Amiga
    Sound.MED.Basic.Human
    Sound.MED.Raw.BlockCmdPageTable
    Sound.MED.Raw.BlockInfo
    Sound.MED.Raw.CmdPageData
    Sound.MED.Raw.HybridInstr
    Sound.MED.Raw.InstrExt
    Sound.MED.Raw.InstrHdr
    Sound.MED.Raw.MMD0
    Sound.MED.Raw.MMD0Block
    Sound.MED.Raw.MMD0NoteData
    Sound.MED.Raw.MMD0Sample
    Sound.MED.Raw.MMD0Song
    Sound.MED.Raw.MMD0exp
    Sound.MED.Raw.MMD1
    Sound.MED.Raw.MMD1Block
    Sound.MED.Raw.MMD1NoteData
    Sound.MED.Raw.MMD2
    Sound.MED.Raw.MMD2Song
    Sound.MED.Raw.MMD3
    Sound.MED.Raw.MMDARexx
    Sound.MED.Raw.MMDARexxTrigCmd
    Sound.MED.Raw.MMDDump
    Sound.MED.Raw.MMDDumpData
    Sound.MED.Raw.MMDInfo
    Sound.MED.Raw.MMDInstrInfo
    Sound.MED.Raw.MMDMIDICmd3x
    Sound.MED.Raw.NotationInfo
    Sound.MED.Raw.PlaySeq
    Sound.MED.Raw.SampleInstr
    Sound.MED.Raw.SynthInstr
    Sound.MED.Raw.SynthWF
    Sound.MED.Generic
    Sound.MED.Generic.Block
    Sound.MED.Generic.Instrument
    Sound.MED.Generic.PlaySeq
    Sound.MED.Generic.Tempo
  Other-Modules:
    Sound.MED.Basic.Pretty

  Build-Depends:
    storable-endian >=0.2.6 && <0.3,
    utility-ht >=0.0.12 && <0.1,
    base >=4.5 && <5
  Hs-Source-Dirs:      src
  Default-Language:    Haskell2010
  GHC-Options:         -Wall -fwarn-incomplete-uni-patterns -fwarn-tabs

Executable unmed2
  Main-Is: example/Unmed.hs
  If flag(buildExamples)
    Build-Depends:
      med-module,
      base >=4.5 && <5
  Else
    Buildable: False
  Default-Language:    Haskell2010
  GHC-Options:
    -Wall -fwarn-incomplete-uni-patterns
    -fwarn-tabs -fwarn-missing-import-lists

Executable animed
  Main-Is: Animate.hs
  Hs-Source-Dirs: example
  Other-Modules: Animate.Option
  If flag(buildExamples)
    Build-Depends:
      med-module,
      hps >=0.15 && <0.16,
      optparse-applicative >=0.11 && <0.15,
      filepath >=1.3 && <1.5,
      array >=0.4 && <0.6,
      transformers >=0.3 && <0.6,
      non-empty >=0.2 && <0.4,
      utility-ht >=0.0.10 && <0.1,
      base >=4.5 && <5
  Else
    Buildable: False
  Default-Language:    Haskell2010
  GHC-Options:
    -Wall -fwarn-incomplete-uni-patterns
    -fwarn-tabs -fwarn-missing-import-lists
