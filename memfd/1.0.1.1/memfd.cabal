cabal-version: 3.0

name: memfd
version: 1.0.1.1
synopsis: Open temporary anonymous Linux file handles
category: Filesystem, Linux

description:

    "memfd" (__mem__ory __f__ile __d__escriptor) lets us open pseudo-"files" that
    are not actually stored in the "real" file system. This feature is only
    available on Linux.

    Such a file is described as an "anonymous file". It behaves like a regular file,
    and so can be modified, truncated, memory-mapped, and so on. However, unlike a
    regular file, it lives in RAM and has a volatile backing storage. Once all
    references to the file are dropped, it is automatically released.

copyright: 2022 Mission Valley Software LLC
license: Apache-2.0
license-file: license.txt

author: Chris Martin
maintainer: Chris Martin, Julie Moronuki

homepage: https://github.com/typeclasses/memfd
bug-reports: https://github.com/typeclasses/memfd/issues

extra-source-files: *.md

source-repository head
    type: git
    location: git://github.com/typeclasses/memfd.git

library
    default-language: Haskell2010
    default-extensions: NoImplicitPrelude
    ghc-options: -Wall -Wmissing-deriving-strategies
    build-depends: base ^>= 4.14 || ^>= 4.15 || ^>= 4.16 || ^>= 4.17
    build-depends: transformers ^>= 0.5.6 || ^>= 0.6
    exposed-modules: Memfd
    other-modules:
        Create
        CreateFlags
        CreateFlagsType
        CreateOptionsType
        CreateOptionsFlags
        DefaultCreateOptions
        FileSystemType
        HugeTLBOptionsType
        HugeTLBSizeType
        NameType
        OnExecType
        SealingType
        SupportedHugeTLBSizes
