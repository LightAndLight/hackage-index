name: stack
version: 1.3.0
cabal-version: >=1.10
build-type: Custom
license: BSD3
license-file: LICENSE
maintainer: manny@fpcomplete.com
homepage: http://haskellstack.org
synopsis: The Haskell Tool Stack
description:
    Please see the README.md for usage information, and
    the wiki on Github for more details.  Also, note that
    the API for the library is not currently stable, and may
    change significantly, even between minor releases. It is
    currently only intended for use by the executable.
category: Development
author: Commercial Haskell SIG
extra-source-files:
    CONTRIBUTING.md
    ChangeLog.md
    README.md
    doc/*.md
    src/setup-shim/StackSetupShim.hs
    test/package-dump/ghc-7.8.txt
    test/package-dump/ghc-7.8.4-osx.txt
    test/package-dump/ghc-7.10.txt
    stack.yaml

source-repository head
    type: git
    location: https://github.com/commercialhaskell/stack.git

flag integration-tests
    description:
        Run the integration test suite
    default: False
    manual: True

flag disable-git-info
    description:
        Disable compile-time inclusion of current git info in stack
    default: False
    manual: True

flag static
    description:
        Pass -static/-pthread to ghc when linking the stack binary.
    default: False
    manual: True

flag hide-dependency-versions
    description:
        Hides dependency versions from "stack --version", used only by building with stack.yaml
    default: False
    manual: True

library
    
    if os(windows)
        build-depends:
            Win32 >=2.3.1.0 && <2.4
        cpp-options: -DWINDOWS
    else
        build-depends:
            unix >=2.7.0.1 && <2.8,
            pid1 ==0.1.*
    exposed-modules:
        Control.Concurrent.Execute
        Data.Aeson.Extended
        Data.Attoparsec.Args
        Data.Attoparsec.Combinators
        Data.Attoparsec.Interpreter
        Data.IORef.RunOnce
        Data.Maybe.Extra
        Data.Monoid.Extra
        Data.Store.VersionTagged
        Data.Text.Extra
        Distribution.Version.Extra
        Network.HTTP.Download
        Network.HTTP.Download.Verified
        Options.Applicative.Args
        Options.Applicative.Builder.Extra
        Options.Applicative.Complicated
        Path.Extra
        Path.Find
        Paths_stack
        Stack.Build
        Stack.Build.Cache
        Stack.Build.ConstructPlan
        Stack.Build.Execute
        Stack.Build.Haddock
        Stack.Build.Installed
        Stack.Build.Source
        Stack.Build.Target
        Stack.BuildPlan
        Stack.Clean
        Stack.Config
        Stack.Config.Build
        Stack.Config.Urls
        Stack.Config.Docker
        Stack.Config.Nix
        Stack.ConfigCmd
        Stack.Constants
        Stack.Coverage
        Stack.Docker
        Stack.Docker.GlobalDB
        Stack.Dot
        Stack.Exec
        Stack.Fetch
        Stack.FileWatch
        Stack.GhcPkg
        Stack.Ghci
        Stack.Ghci.Script
        Stack.Hoogle
        Stack.IDE
        Stack.Image
        Stack.Init
        Stack.New
        Stack.Nix
        Stack.Options.BenchParser
        Stack.Options.BuildMonoidParser
        Stack.Options.BuildParser
        Stack.Options.CleanParser
        Stack.Options.ConfigParser
        Stack.Options.DockerParser
        Stack.Options.DotParser
        Stack.Options.ExecParser
        Stack.Options.GhcBuildParser
        Stack.Options.GhciParser
        Stack.Options.GhcVariantParser
        Stack.Options.GlobalParser
        Stack.Options.HaddockParser
        Stack.Options.HpcReportParser
        Stack.Options.LogLevelParser
        Stack.Options.NewParser
        Stack.Options.NixParser
        Stack.Options.PackageParser
        Stack.Options.ResolverParser
        Stack.Options.SolverParser
        Stack.Options.TestParser
        Stack.Options.Utils
        Stack.Package
        Stack.PackageDump
        Stack.PackageIndex
        Stack.Path
        Stack.PrettyPrint
        Stack.Runners
        Stack.SDist
        Stack.Setup
        Stack.Setup.Installed
        Stack.SetupCmd
        Stack.Sig
        Stack.Sig.GPG
        Stack.Sig.Sign
        Stack.Solver
        Stack.Types.Build
        Stack.Types.BuildPlan
        Stack.Types.CompilerBuild
        Stack.Types.Urls
        Stack.Types.Compiler
        Stack.Types.Config
        Stack.Types.Config.Build
        Stack.Types.Docker
        Stack.Types.FlagName
        Stack.Types.GhcPkgId
        Stack.Types.Image
        Stack.Types.Internal
        Stack.Types.Nix
        Stack.Types.Package
        Stack.Types.PackageDump
        Stack.Types.PackageIdentifier
        Stack.Types.PackageIndex
        Stack.Types.PackageName
        Stack.Types.Resolver
        Stack.Types.Sig
        Stack.Types.StackT
        Stack.Types.TemplateName
        Stack.Types.Version
        Stack.Upgrade
        Stack.Upload
        Text.PrettyPrint.Leijen.Extended
        System.Process.Log
        System.Process.PagerEditor
        System.Process.Read
        System.Process.Run
    build-depends:
        Cabal >=1.18.1.5 && <1.25,
        aeson ==1.0.*,
        ansi-terminal >=0.6.2.3 && <0.7,
        async >=2.0.2 && <2.2,
        attoparsec >=0.12.1.5 && <0.14,
        base >=4.7 && <5,
        base-compat >=0.6 && <0.10,
        base16-bytestring >=0.1.1.6 && <0.2,
        base64-bytestring >=1.0.0.1 && <1.1,
        binary >=0.7 && <0.9,
        binary-tagged >=0.1.1 && <0.2,
        blaze-builder >=0.4.0.2 && <0.5,
        byteable >=0.1.1 && <0.2,
        bytestring >=0.10.4.0 && <0.11,
        clock >=0.7.2 && <0.8,
        conduit >=1.2.8 && <1.3,
        conduit-extra >=1.1.7.1 && <1.2,
        containers >=0.5.5.1 && <0.6,
        cryptohash >=0.11.6 && <0.12,
        cryptohash-conduit >=0.1.1 && <0.2,
        directory >=1.2.1.0 && <1.3,
        either >=4.4.1.1 && <4.5,
        errors >=2.1.2 && <2.2,
        exceptions >=0.8.0.2 && <0.9,
        extra >=1.4.10 && <1.6,
        fast-logger >=2.3.1 && <2.5,
        filelock >=0.1.0.1 && <0.2,
        filepath >=1.3.0.2 && <1.5,
        fsnotify >=0.2.1 && <0.3,
        generic-deriving >=1.10.5 && <1.12,
        hashable >=1.2.3.2 && <1.3,
        hit >=0.6.3 && <0.7,
        hpc >=0.6.0.2 && <0.7,
        http-client >=0.5.3.3 && <0.6,
        http-client-tls >=0.3.3 && <0.4,
        http-conduit >=2.2.3 && <2.3,
        http-types >=0.8.6 && <0.10,
        lifted-async >=0.9.0 && <0.10,
        lifted-base >=0.2.3.8 && <0.3,
        microlens >=0.3.0.0 && <0.5,
        monad-control >=1.0.1.0 && <1.1,
        monad-logger >=0.3.13.1 && <0.4,
        monad-unlift >=0.2.0 && <0.3,
        mtl >=2.1.3.1 && <2.3,
        open-browser >=0.2.1 && <0.3,
        optparse-applicative ==0.13.*,
        path >=0.5.8 && <0.6,
        path-io >=1.1.0 && <2.0.0,
        persistent >=2.1.2 && <2.7,
        persistent-sqlite >=2.1.4 && <2.5.0.1 || >2.5.0.1 && <2.7,
        persistent-template >=2.1.1 && <2.6,
        pretty >=1.1.1.1 && <1.2,
        process >=1.2.1.0 && <1.5,
        regex-applicative-text >=0.1.0.1 && <0.2,
        resourcet >=1.1.4.1 && <1.2,
        retry >=0.6 && <0.8,
        safe ==0.3.*,
        safe-exceptions >=0.1.4.0 && <0.2,
        semigroups >=0.5 && <0.19,
        split >=0.2.3.1 && <0.3,
        stm >=2.4.4 && <2.5,
        streaming-commons >=0.1.10.0 && <0.2,
        tar >=0.5.0.3 && <0.6,
        template-haskell >=2.9.0.0 && <2.12,
        temporary >=1.2.0.3 && <1.3,
        text >=1.2.0.4 && <1.3,
        text-binary >=0.2.1.1 && <0.3,
        text-metrics >=0.1 && <0.3,
        time >=1.4.2 && <1.7,
        tls >=1.3.8 && <1.4,
        transformers >=0.3.0.0 && <0.6,
        transformers-base >=0.4.4 && <0.5,
        unicode-transforms >=0.1 && <0.3,
        unix-compat >=0.4.1.4 && <0.5,
        unordered-containers >=0.2.5.1 && <0.3,
        vector >=0.10.12.3 && <0.12,
        vector-binary-instances >=0.2.3.2 && <0.3,
        yaml >=0.8.20 && <0.9,
        zlib >=0.5.4.2 && <0.7,
        deepseq ==1.4.*,
        hastache >=0.6.1 && <0.7,
        project-template ==0.2.*,
        zip-archive >=0.2.3.7 && <0.4,
        hpack >=0.14.0 && <0.16,
        store >=0.2.1.0 && <0.4,
        annotated-wl-pprint >=0.7.0 && <0.8,
        file-embed >=0.0.10 && <0.1
    default-language: Haskell2010
    hs-source-dirs: src/
    ghc-options: -Wall -fwarn-tabs -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates -fwarn-identities

executable stack
    
    if flag(static)
        ld-options: -static -pthread
    
    if os(windows)
        build-depends:
            Win32 >=2.3.1.0 && <2.4
        cpp-options: -DWINDOWS
    
    if !flag(disable-git-info)
        build-depends:
            gitrev >=1.1 && <1.3,
            optparse-simple >=0.0.3 && <0.1
        cpp-options: -DUSE_GIT_INFO
    
    if flag(hide-dependency-versions)
        cpp-options: -DHIDE_DEP_VERSIONS
    main-is: Main.hs
    build-depends:
        Cabal >=1.18.1.5 && <1.25,
        base >=4.7 && <5,
        bytestring >=0.10.4.0 && <0.11,
        containers >=0.5.5.1 && <0.6,
        directory >=1.2.1.0 && <1.3,
        either >=4.4.1.1 && <4.5,
        filelock >=0.1.0.1 && <0.2,
        filepath >=1.3.0.2 && <1.5,
        hpack >=0.14.0 && <0.16,
        http-client >=0.5.3.3 && <0.6,
        lifted-base >=0.2.3.8 && <0.3,
        microlens >=0.3.0.0 && <0.5,
        monad-control >=1.0.1.0 && <1.1,
        monad-logger >=0.3.13.1 && <0.4,
        mtl >=2.1.3.1 && <2.3,
        optparse-applicative ==0.13.*,
        path >=0.5.9 && <0.6,
        path-io >=1.1.0 && <2.0.0,
        stack >=1.3.0 && <1.4,
        text >=1.2.0.4 && <1.3,
        transformers >=0.3.0.0 && <0.6
    default-language: Haskell2010
    hs-source-dirs: src/main
    other-modules:
        Paths_stack
    ghc-options: -threaded -Wall -fwarn-tabs -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates

test-suite stack-test
    
    if os(windows)
        cpp-options: -DWINDOWS
    type: exitcode-stdio-1.0
    main-is: Test.hs
    build-depends:
        Cabal >=1.18.1.5 && <1.25,
        QuickCheck >=2.8.2 && <2.10,
        attoparsec >=0.13.1.0 && <0.14,
        base >=4.7 && <5,
        conduit >=1.2.8 && <1.3,
        conduit-extra >=1.1.14 && <1.2,
        containers >=0.5.5.1 && <0.6,
        cryptohash >=0.11.9 && <0.12,
        directory >=1.2.1.0 && <1.3,
        exceptions >=0.8.3 && <0.9,
        filepath >=1.4.0.0 && <1.5,
        hspec >=2.2 && <2.4,
        http-client-tls >=0.3.3 && <0.4,
        http-conduit >=2.2.3 && <2.3,
        monad-logger >=0.3.20.1 && <0.4,
        neat-interpolation ==0.3.*,
        path >=0.5.7 && <0.6,
        path-io >=1.1.0 && <2.0.0,
        resourcet >=1.1.8.1 && <1.2,
        retry >=0.6 && <0.8,
        stack >=1.3.0 && <1.4,
        temporary >=1.2.0.4 && <1.3,
        text >=1.2.2.1 && <1.3,
        transformers >=0.3.0.0 && <0.6,
        mono-traversable >=0.10.2 && <0.11,
        th-reify-many >=0.1.6 && <0.2,
        smallcheck >=1.1.1 && <1.2,
        bytestring >=0.10.6.0 && <0.11,
        store >=0.2.1.0 && <0.4,
        vector >=0.11.0.0 && <0.12,
        template-haskell >=2.10.0.0 && <2.11,
        yaml >=0.8.20 && <0.9
    default-language: Haskell2010
    hs-source-dirs: src/test
    other-modules:
        Spec
        Stack.BuildPlanSpec
        Stack.Build.ExecuteSpec
        Stack.Build.TargetSpec
        Stack.ConfigSpec
        Stack.DotSpec
        Stack.GhciSpec
        Stack.Ghci.ScriptSpec
        Stack.Ghci.PortableFakePaths
        Stack.PackageDumpSpec
        Stack.ArgsSpec
        Stack.NixSpec
        Stack.StoreSpec
        Network.HTTP.Download.VerifiedSpec
        Stack.SolverSpec
        Stack.Untar.UntarSpec
    ghc-options: -threaded -Wall -fwarn-tabs -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
test-suite stack-integration-test
    
    if !flag(integration-tests)
        buildable: False
    type: exitcode-stdio-1.0
    main-is: IntegrationSpec.hs
    build-depends:
        async >=2.1.0 && <2.2,
        base >=4.7 && <5,
        bytestring >=0.10.4.0 && <0.11,
        conduit >=1.2.8 && <1.3,
        conduit-extra >=1.1.14 && <1.2,
        containers >=0.5.5.1 && <0.6,
        directory >=1.2.1.0 && <1.3,
        filepath >=1.3.0.2 && <1.5,
        hspec >=2.2 && <2.4,
        process >=1.2.0.0 && <1.5,
        resourcet >=1.1.8.1 && <1.2,
        temporary >=1.2.0.4 && <1.3,
        text >=1.2.2.1 && <1.3,
        transformers >=0.3.0.0 && <0.6,
        unix-compat >=0.4.1.4 && <0.5
    default-language: Haskell2010
    hs-source-dirs: test/integration test/integration/lib
    other-modules:
        StackTest
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall -fwarn-tabs -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
