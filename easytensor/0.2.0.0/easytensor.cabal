name: easytensor
version: 0.2.0.0
cabal-version: >=1.20
build-type: Simple
license: MIT
license-file: LICENSE
copyright: (c) Artem Chirkin
maintainer: chirkin@arch.ethz.ch
homepage: https://github.com/achirkin/easytensor#readme
synopsis: Initial project template from stack
description:
    Pure haskell vector/matrix/tensor library.
    Features dimensionality type-checking for all operations.
    Generic n-dimensional versions are implemented using low-level prim ops.
    Allows ad-hoc replacement with fixed low-dimensionality vectors and matrices without changing user interface.
category: Math, Geometry
author: Artem Chirkin
extra-source-files:
    src/Numeric/Array/Family/Array.h

source-repository head
    type: git
    location: https://github.com/achirkin/easytensor.git
    subdir: easytensor


flag unsafeindices
    description:
        Disable bound checks when accessing elements of a tensor/matrix/vector.
    default: False


library

    if flag(unsafeindices)
        cpp-options: -DUNSAFE_INDICES
    exposed-modules:
        Numeric.Commons
        Numeric.DataFrame
        Numeric.DataFrame.IO
        Numeric.DataFrame.ST
        Numeric.Matrix
        Numeric.Vector
        Numeric.Scalar
    build-depends:
        base >=4.9 && <5,
        ghc-prim >=0.5,
        dimensions -any
    default-language: Haskell2010
    hs-source-dirs: src
    other-modules:
        Numeric.Array.Family.ArrayF
        Numeric.Array.Family.ArrayD
        Numeric.Array.Family.ArrayI
        Numeric.Array.Family.ArrayI8
        Numeric.Array.Family.ArrayI16
        Numeric.Array.Family.ArrayI32
        Numeric.Array.Family.ArrayI64
        Numeric.Array.Family.ArrayW
        Numeric.Array.Family.ArrayW8
        Numeric.Array.Family.ArrayW16
        Numeric.Array.Family.ArrayW32
        Numeric.Array.Family.ArrayW64
        Numeric.Array.Family.FloatX2
        Numeric.Array.Family.FloatX3
        Numeric.Array.Family.FloatX4
        Numeric.Array.Family
        Numeric.Array.ElementWise
        Numeric.Array
        Numeric.DataFrame.SubSpace
        Numeric.DataFrame.Contraction
        Numeric.DataFrame.Mutable
        Numeric.DataFrame.Type
        Numeric.DataFrame.Inference
        Numeric.DataFrame.Shape
        Numeric.Matrix.Type
    ghc-options: -Wall -fwarn-tabs -O2

test-suite et-test

    type: detailed-0.9
    test-module: Spec
    other-modules:
        Numeric.DataFrame.Arbitraries
        Numeric.DataFrame.SubSpaceTest
        Numeric.DataFrame.BasicTest
    build-depends:
        base -any,
        Cabal >=1.20,
        QuickCheck -any,
        easytensor -any,
        dimensions -any
    default-language: Haskell2010
    hs-source-dirs: test
    ghc-options: -Wall -fwarn-tabs -O2



benchmark et-bench-misc

    type: exitcode-stdio-1.0
    main-is: misc.hs
    build-depends:
        base -any,
        easytensor -any,
        dimensions -any
    default-language: Haskell2010
    hs-source-dirs: bench
    ghc-options: -Wall -fwarn-tabs -O2



benchmark et-bench-spfolds

    type: exitcode-stdio-1.0
    main-is: subspacefolds.hs
    build-depends:
        base -any,
        easytensor -any,
        dimensions -any,
        time -any
    default-language: Haskell2010
    hs-source-dirs: bench
    ghc-options: -Wall -fwarn-tabs -O2
