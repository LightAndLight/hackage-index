-- Cabal file for the Conjure program generation library.
--
-- Copyright (C) 2021 Rudy Matela
-- Distributed under the 3-Clause BSD licence (see the file LICENSE).
name:                code-conjure
version:             0.5.2
synopsis:            synthesize Haskell functions out of partial definitions
description:
  Conjure is a tool that synthesizes Haskell functions out of partial definitions.

homepage:            https://github.com/rudymatela/conjure#readme
license:             BSD3
license-file:        LICENSE
author:              Rudy Matela
maintainer:          Rudy Matela <rudy@matela.com.br>
category:            Haskell
build-type:          Simple
cabal-version:       1.18

extra-doc-files: README.md
               , TODO.md
               , changelog.md
               , doc/conjure.svg
extra-source-files: .gitignore
                  , .github/workflows/build.yml
                  , Makefile
                  , bench/bench
                  , bench/*.hs
                  , bench/*.out
                  , eg/*.hs
                  , eg/*.out
                  , proto/*.hs
                  , proto/*.out
                  , mk/All.hs
                  , mk/Toplibs.hs
                  , mk/depend.mk
                  , mk/ghcdeps
                  , mk/haddock-i
                  , mk/haskell.mk
                  , mk/install-on
                  , stack.yaml
                  , bench/runtime/zero/bench/*.runtime
                  , bench/runtime/zero/eg/*.runtime
                  , bench/runtime/zero/proto/*.runtime
                  , bench/runtime/zero/versions
                  , bench/versions
                  , bench/time
                  , test/sdist
tested-with: GHC==9.0
           , GHC==8.10
           , GHC==8.8
           , GHC==8.6
           , GHC==8.4
           , GHC==8.2
           , GHC==8.0
           , GHC==7.10
           , GHC==7.8

source-repository head
  type:            git
  location:        https://github.com/rudymatela/conjure

source-repository this
  type:            git
  location:        https://github.com/rudymatela/conjure
  tag:             v0.5.2

library
  exposed-modules: Conjure
                 , Conjure.Conjurable
                 , Conjure.Conjurable.Derive
                 , Conjure.Engine
                 , Conjure.Expr
                 , Conjure.Prim
                 , Conjure.Utils
                 , Conjure.Defn
  other-extensions: TemplateHaskell, CPP
  build-depends: base >= 4 && < 5
               , leancheck >= 0.9.10
               , template-haskell
               , speculate >= 0.4.14
               , express >= 1.0.8
  hs-source-dirs:      src
  default-language:    Haskell2010

test-suite expr
  type:                exitcode-stdio-1.0
  main-is:             expr.hs
  other-modules:       Test, Test.ListableExpr
  hs-source-dirs:      test
  build-depends:       base >= 4 && < 5, leancheck, express, speculate, code-conjure
  default-language:    Haskell2010

test-suite defn
  type:                exitcode-stdio-1.0
  main-is:             defn.hs
  other-modules:       Test, Test.ListableExpr
  hs-source-dirs:      test
  build-depends:       base >= 4 && < 5, leancheck, express, speculate, code-conjure
  default-language:    Haskell2010

test-suite conjurable
  type:                exitcode-stdio-1.0
  main-is:             conjurable.hs
  other-modules:       Test, Test.ListableExpr
  hs-source-dirs:      test
  build-depends:       base >= 4 && < 5, leancheck, express, speculate, code-conjure
  default-language:    Haskell2010

test-suite utils
  type:                exitcode-stdio-1.0
  main-is:             utils.hs
  other-modules:       Test, Test.ListableExpr
  hs-source-dirs:      test
  build-depends:       base >= 4 && < 5, leancheck, express, speculate, code-conjure
  default-language:    Haskell2010

test-suite derive
  type:                exitcode-stdio-1.0
  main-is:             derive.hs
  other-modules:       Test, Test.ListableExpr
  hs-source-dirs:      test
  build-depends:       base >= 4 && < 5, leancheck, express, speculate, code-conjure
  default-language:    Haskell2010
