cabal-version:      1.18
name:               aws-sns-verify
version:            0.0.0.0
x-revision: 1
license:            MIT
license-file:       LICENSE
copyright:          2022 Freckle By Renaissance
maintainer:         engineering@freckle.com
author:             Freckle
homepage:           https://github.com/freckle/aws-sns-verify#readme
bug-reports:        https://github.com/freckle/aws-sns-verify/issues
synopsis: Parse and verify AWS SNS messages
description:
    Please see the README on GitHub at <https://github.com/freckle/aws-sns-verify#readme>

category:
    Network, Web, AWS, Amazon, Cloud, Cryptography, Distributed-Computing

build-type:         Simple
extra-source-files:
    tests/cert.pem
    tests/key.pem

extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/freckle/aws-sns-verify

library
    exposed-modules:
        Amazon.SNS.Verify
        Amazon.SNS.Verify.Payload
        Amazon.SNS.Verify.Validate

    hs-source-dirs:     library
    other-modules:      Amazon.SNS.Verify.Prelude
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missing-import-lists -Wno-missing-local-signatures
        -Wno-monomorphism-restriction -Wno-unsafe -Wno-safe
        -Wno-all-missed-specialisations

    build-depends:
        aeson >=1.3.1.1,
        base >=4.7 && <5,
        bytestring >=0.10.8.2,
        errors >=2.3.0,
        http-conduit >=2.3.2,
        memory >=0.14.18,
        pem >=0.2.4,
        text >=1.2.3.1,
        x509 >=1.7.5,
        x509-validation >=1.6.11

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=9.2.2)
        ghc-options: -Wno-missing-kind-signatures

test-suite aws-sns-verify-test
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     tests
    other-modules:
        Amazon.SNS.Verify.TestPrelude
        Amazon.SNS.Verify.ValidateSpec
        Amazon.SNS.VerifySpec
        Paths_aws_sns_verify

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missing-import-lists -Wno-missing-local-signatures
        -Wno-monomorphism-restriction -Wno-unsafe -Wno-safe
        -Wno-all-missed-specialisations -threaded -rtsopts -with-rtsopts=-N

    build-depends:
        aeson-qq >=0.8.2,
        async >=2.2.1,
        aws-sns-verify -any,
        base >=4.7 && <5,
        hspec >=2.5.5,
        http-types >=0.12.2,
        text >=1.2.3.1,
        wai >=3.2.1.2,
        warp >=3.2.25,
        x509-validation >=1.6.11

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=9.2.2)
        ghc-options: -Wno-missing-kind-signatures
