cabal-version:      1.12
name:               hspec-junit-formatter
version:            1.0.3.0
license:            MIT
license-file:       LICENSE
copyright:          2021 Renaissance Learning Inc
maintainer:         engineering@freckle.com
author:             Freckle R&D
homepage:           https://github.com/freckle/hspec-junit-formatter#readme
bug-reports:        https://github.com/freckle/hspec-junit-formatter/issues
synopsis:           A JUnit XML runner/formatter for hspec
description:
    Allows hspec tests to write JUnit XML output for parsing in various tools.

category:           Testing
build-type:         Simple
extra-source-files:
    README.md
    CHANGELOG.md
    tests/golden.xml
    tests/golden-prefixed.xml

source-repository head
    type:     git
    location: https://github.com/freckle/hspec-junit-formatter

library
    exposed-modules:
        Test.Hspec.Core.Runner.Ext
        Test.HSpec.JUnit
        Test.Hspec.JUnit
        Test.Hspec.JUnit.Config
        Test.Hspec.JUnit.Config.Env
        Test.HSpec.JUnit.Render
        Test.Hspec.JUnit.Render
        Test.HSpec.JUnit.Schema
        Test.Hspec.JUnit.Schema

    hs-source-dirs:     library
    other-modules:      Paths_hspec_junit_formatter
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        base >=4.14.1.0 && <5,
        conduit >=1.3.4.1,
        containers >=0.6.2.1,
        directory >=1.3.6.0,
        exceptions >=0.10.4,
        filepath >=1.4.2.1,
        hspec-core >=2.8.1,
        text >=1.2.4.1,
        time >=1.9.3,
        xml-conduit >=1.9.1.1,
        xml-types >=0.3.8

test-suite readme
    type:               exitcode-stdio-1.0
    main-is:            README.lhs
    other-modules:      Paths_hspec_junit_formatter
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:        -pgmL markdown-unlit
    build-depends:
        base >=4.14.1.0 && <5,
        hspec >=2.8.1,
        hspec-core >=2.8.1,
        hspec-junit-formatter -any,
        markdown-unlit >=0.5.1

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     tests
    other-modules:
        ExampleSpec
        Paths_hspec_junit_formatter

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:        -threaded -rtsopts -O0 -with-rtsopts=-N
    build-depends:
        base >=4.14.1.0 && <5,
        containers >=0.6.2.1,
        filepath >=1.4.2.1,
        hspec >=2.8.1,
        hspec-junit-formatter -any,
        temporary >=1.3,
        text >=1.2.4.1,
        xml-conduit >=1.9.1.1
