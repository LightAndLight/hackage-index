name:               hjsonschema
version:            1.7.1
synopsis:           JSON Schema library
homepage:           https://github.com/seagreen/hjsonschema
license:            MIT
license-file:       MIT-LICENSE.txt
author:             Ian Grant Jeffries
maintainer:         ian@housejeffries.com
category:           Data
build-type:         Simple
cabal-version:      >=1.10
-- Rerun multi-ghc-travis (executable make-travis-yml-2) after changing:
Tested-With:        GHC == 7.10.3, GHC == 8.0.2, GHC == 8.2.1
extra-source-files:
  changelog.md
  JSON-Schema-Test-Suite/remotes/*.json
  JSON-Schema-Test-Suite/remotes/folder/*.json
  JSON-Schema-Test-Suite/tests/draft4/*.json
  JSON-Schema-Test-Suite/tests/draft4/optional/*.json
  README.md
  src/draft4.json
  test/Local/*.json
  test/supplement/*.json
  examples/json/*.json

library
  hs-source-dirs:
    src
  default-language: Haskell2010
  default-extensions:
    DeriveFunctor
    DeriveGeneric
    GeneralizedNewtypeDeriving
    NoImplicitPrelude
    OverloadedStrings
    ScopedTypeVariables
    TupleSections
  if impl(ghc >= 8)
    default-extensions: StrictData
  ghc-options:
    -Wall
  exposed-modules:
      JSONSchema.Draft4
    , JSONSchema.Draft4.Failure
    , JSONSchema.Draft4.Schema
    , JSONSchema.Draft4.Spec
    , JSONSchema.Fetch
    , JSONSchema.Types
    , JSONSchema.Validator.Draft4
    , JSONSchema.Validator.Draft4.Any
    , JSONSchema.Validator.Draft4.Array
    , JSONSchema.Validator.Draft4.Number
    , JSONSchema.Validator.Draft4.Object
    , JSONSchema.Validator.Draft4.String
    , JSONSchema.Validator.Reference
    , JSONSchema.Validator.Types
    , JSONSchema.Validator.Utils
  other-modules:
      JSONSchema.Validator.Draft4.Object.Properties
    , Import
  build-depends:
      base                 >= 4.7    && < 4.11
    -- 0.11 is for `.:!`:
    , aeson                >= 0.11   && < 1.3
    , bytestring           >= 0.10   && < 0.11
    , containers           >= 0.5    && < 0.6
    , file-embed           >= 0.0.8  && < 0.1
    , filepath             >= 1.3    && < 1.5
    , hashable             >= 1.2    && < 1.3
    , hjsonpointer         >= 1.1    && < 1.4
    -- 0.4.30 is for parseUrlThrow:
    , http-client          >= 0.4.30 && < 0.6
    , http-types           >= 0.8    && < 0.11
    , pcre-heavy           >= 1.0    && < 1.1
    , profunctors          >= 5.0    && < 5.3
    , protolude            >= 0.1.10 && < 1.2
    , QuickCheck           >= 2.8    && < 2.11
    , scientific           >= 0.3    && < 0.4
    , semigroups           >= 0.18   && < 0.19
    , unordered-containers >= 0.2    && < 0.3
    , text                 >= 1.1    && < 1.3
    , vector               >= 0.10   && < 1.0

test-suite local
  hs-source-dirs:
    test
    examples
  main-is: Local.hs
  type: exitcode-stdio-1.0
  default-language: Haskell2010
  ghc-options:
    -Wall
    -fno-warn-orphans
  default-extensions:
    DeriveGeneric
    NoImplicitPrelude
    OverloadedStrings
    ScopedTypeVariables
  other-modules:
      Local.Failure
    , Local.Reference
    , Local.Validation
    , Shared
    -- from ./examples:
    , AlternateSchema
    , Simple
    , TwoStep
  build-depends:
      base
    , aeson
    , bytestring
    , filepath
    , hjsonpointer
    , hjsonschema
    , profunctors
    , protolude
    , semigroups
    , text
    , QuickCheck
    , unordered-containers
    , vector

    -- directory-1.2.5 required for `listDirectory`:
    , directory            >= 1.2.5 && < 2.0
    , hspec                >= 2.2 && < 3.0

test-suite remote
  hs-source-dirs:
    test
    examples
  main-is: Remote.hs
  type: exitcode-stdio-1.0
  default-language: Haskell2010
  ghc-options:
    -Wall
    -fno-warn-orphans
  default-extensions:
    DeriveGeneric
    NoImplicitPrelude
    OverloadedStrings
    ScopedTypeVariables
  other-modules:
      Shared
    -- from ./examples:
    , AlternateSchema
  build-depends:
      base
    , aeson
    , async
    , bytestring
    , filepath
    , hjsonpointer
    , hjsonschema
    , profunctors
    , protolude
    , semigroups
    , text
    , unordered-containers
    , vector

    , directory
    , hspec
    , wai-app-static
    , warp
