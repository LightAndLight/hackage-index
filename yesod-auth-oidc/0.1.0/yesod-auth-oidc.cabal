cabal-version: 2.2
name: yesod-auth-oidc
version: 0.1.0
build-type: Simple
category: Web, Yesod
extra-source-files: README.md
license: BSD-3-Clause
license-file: LICENSE
author: Supercede Technology Ltd
maintainer: Supercede Technology Ltd <support@supercede.com>
homepage: https://github.com/SupercedeTech/yesod-auth-oidc
synopsis: A yesod-auth plugin for multi-tenant SSO via OpenID Connect
description:
  A yesod-auth plugin for multi-tenant SSO via OpenID Connect, using
  Authorization Code flow (AKA server flow).

  Please see the README.md file for more documentation.

tested-with: GHC == 8.10.4

source-repository head
  type: git
  location: git@github.com:SupercedeTech/yesod-auth-oidc.git

common common-options
  default-language: Haskell2010
  default-extensions: NoImplicitPrelude
  hs-source-dirs: src
  ghc-options:
    -Wall -Wincomplete-uni-patterns
    -Wincomplete-record-updates -Widentities -Wredundant-constraints
    -Wcpp-undef -Wimplicit-prelude -fwarn-tabs
  build-depends:
      base                              >=4.9.1.0 && <5,
      aeson                             >= 1.5.6 && < 1.6,
      text                              >= 1.2.4 && < 1.3,
      time                              >= 1.9.3 && < 1.10,
      unordered-containers              >= 0.2.13 && < 0.3,
      base64-bytestring                 >= 1.1.0 && < 1.2,
      classy-prelude-yesod              >= 1.5.0 && < 1.6,

      cryptonite                        >= 0.28 && < 0.29,

      http-client                       >= 0.6.4 && < 0.7,
      jose-jwt                          >= 0.9.2 && < 0.10,

      oidc-client                       >= 0.6.0 && < 0.7,

      shakespeare                       >= 2.0.25 && < 2.1,

      yesod-core                        >= 1.6.19 && < 1.7,
      yesod-form                        >= 1.6.7 && < 1.7,
      yesod-auth                        >= 1.6.10 && < 1.7

library
  import: common-options
  hs-source-dirs: src
  exposed-modules:
    Yesod.Auth.OIDC

Common test-properties
  Default-Language:     Haskell2010
  Hs-Source-Dirs:       src, test
  Ghc-Options:          -Wall
  Default-Extensions:   NoImplicitPrelude
  Build-Depends:
    base,
    aeson,
    bytestring                        >= 0.10.10 && < 0.11,
    containers                        >= 0.6.2 && < 0.7,
    text,
    time,
    unordered-containers,
    base64-bytestring,
    classy-prelude-yesod,
    classy-prelude                    >= 1.5.0 && < 1.6,
    directory                         >= 1.3.6 && < 1.4,
    http-conduit                      >= 2.3.8 && < 2.4,
    http-client,
    http-types                        >= 0.12.3 && < 0.13,
    memory                            >= 0.15.0 && < 0.16,
    cryptonite,
    persistent                        >= 2.11.0 && <= 2.13.2,
    blaze-html                        >= 0.9.1 && < 0.10,
    fast-logger                       >= 3.0.5 && < 3.1,
    monad-logger                      >= 0.3.36 && < 0.4,
    resource-pool                     >= 0.2.3 && < 0.3,
    yesod                             >= 1.6.1 && < 1.7,
    shakespeare,
    wai-extra                         >= 3.1.6 && < 3.2,
    warp                              >= 3.3.15 && < 3.4,
    yesod-core,
    yesod-form,
    email-validate                    >= 2.3.2 && < 2.4,
    yesod-persistent                  >= 1.6.0 && < 1.7,
    wai-app-static                    >= 3.1.7 && < 3.2,
    jose-jwt,
    oidc-client,
    yesod-auth,
    broch                             >= 0.1 && < 0.2,
    postgresql-simple                 >= 0.6.4 && < 0.7,
    reroute                           >= 0.6.0 && < 0.7,
    sqlite-simple                     >= 0.4.18 && < 0.5,
    hspec                             >= 2.7.10 && < 2.8,
    lens                              >= 4.19.2 && < 4.20,
    lens-regex-pcre                   >= 1.1.0 && < 1.2,
    persistent-sqlite                 >= 2.11.1 && <= 2.13,
    yesod-test                        >= 1.6.12 && < 1.7

  Other-Modules:
    ExampleApp
    ExampleProvider
    ExampleProviderOpts
    TestImport
    Yesod.Auth.OIDC
    Yesod.Auth.OIDCSpec

Test-Suite spec
  Import:               test-properties
  Type:                 exitcode-stdio-1.0
  Main-Is:              Spec.hs
  Build-Tool-Depends:   hspec-discover:hspec-discover

Executable yesod-auth-oidc-test
  Import:               test-properties
  Main-Is:              Spec.hs
