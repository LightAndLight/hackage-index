Cabal-Version:    2.2
Name:             explicit-exception
Version:          0.2
x-revision: 2
License:          BSD-3-Clause
License-File:     LICENSE
Author:           Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:       Henning Thielemann <haskell@henning-thielemann.de>
Homepage:         http://wiki.haskell.org/Exception
Category:         Control
Stability:        Experimental
Synopsis:         Exceptions which are explicit in the type signature.
Description:
   Synchronous and Asynchronous exceptions which are explicit in the type signature.
   The first ones are very similar to 'Either' and 'Control.Monad.Error.ErrorT'.
   The second ones are used for 'System.IO.readFile' and 'System.IO.hGetContents'.
   This package is a proposal for improved exception handling in Haskell.
   It strictly separates between handling of
   exceptional situations (file not found, invalid user input,
   see <http://wiki.haskell.org/Exception>) and
   (programming) errors (division by zero, index out of range,
   see <http://wiki.haskell.org/Error>).
   Handling of the first one is called \"exception handling\",
   whereas handling of errors is better known as \"debugging\".
   .
   For applications see the packages @midi@, @spreadsheet@, @http-monad@.
   .
   Although I'm not happy with the identifier style of the Monad Transformer Library
   (partially intended for unqualified use)
   I have tried to adopt it for this library,
   in order to let Haskell programmers get accustomed easily to it.
   .
   See also: @unexceptionalio@
Tested-With:       GHC==7.4.2, GHC==7.6.3, GHC==7.8.4
Tested-With:       GHC==8.0.2, GHC==8.4.1, GHC==9.6.0
Build-Type:        Simple

Source-Repository head
  type:     darcs
  location: https://hub.darcs.net/thielema/explicit-exception/

Source-Repository this
  type:     darcs
  location: https://hub.darcs.net/thielema/explicit-exception/
  tag:      0.2

Flag buildTests
  description: Build executables that demonstrate some space leaks
  default:     False


Library
  Build-Depends:
    transformers >=0.2 && <0.7,
    semigroups >=0.1 && <1.0,
    deepseq >=1.1 && <1.6
  If impl(jhc)
    Build-Depends:
      applicative >=1.0 && <1.1,
      base >= 1 && <2
  Else
    Build-Depends: base >= 2 && <5

  GHC-Options:      -Wall
  Hs-Source-Dirs:   src
  Default-Language: Haskell98
  Exposed-Modules:
    Control.Monad.Exception.Asynchronous
    Control.Monad.Exception.Asynchronous.Lazy
    Control.Monad.Exception.Asynchronous.Strict
    Control.Monad.Exception.Synchronous
  If !impl(jhc)
    Other-Modules:
      Control.Monad.Exception.Warning
      Control.Monad.Exception.Label
      Control.Monad.Label
      System.IO.Straight
      System.IO.Exception.File
      System.IO.Exception.BinaryFile
      System.IO.Exception.TextFile
--    System.IO.Exception.Std
--    Debug.Error

Executable ee-tar
  If flag(buildTests)
    Build-Depends:
      explicit-exception,
      bytestring >=0.9.0.1 && <0.13,
      tar >=0.4 && <0.6,
      base
  Else
    Buildable: False
  GHC-Options: -Wall
  Hs-Source-Dirs: spaceleak
  Default-Language: Haskell98
  Main-Is: Tar.hs

Executable ee-test
  If flag(buildTests)
    Build-Depends:
      explicit-exception,
      bytestring >=0.9.0.1 && <0.13,
      base
  Else
    Buildable: False
  GHC-Options: -Wall
  Hs-Source-Dirs: spaceleak
  Default-Language: Haskell98
  Main-Is: Example.hs

Executable ee-unzip
  If flag(buildTests)
    Build-Depends:
      base
  Else
    Buildable: False
  GHC-Options: -Wall
  Hs-Source-Dirs: spaceleak
  Default-Language: Haskell98
  Main-Is: Unzip.hs

Executable ee-writer
  If flag(buildTests)
    Build-Depends:
      transformers,
      base
  Else
    Buildable: False
  GHC-Options: -Wall
  Hs-Source-Dirs: spaceleak
  Default-Language: Haskell98
  Main-Is: Writer.hs
