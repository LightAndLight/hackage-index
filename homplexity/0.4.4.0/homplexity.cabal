name:                homplexity
version:             0.4.4.0
synopsis:            Haskell code quality tool
description:         Homplexity aims to measure code complexity,
                     warning about fragments that might have higher defect probability
                     due to bad coding style on-the-large:
                     .
                       * too large functions
                     .
                       * too deeply nested conditions
                     .
                       * too few comments

homepage:            https://github.com/mgajda/homplexity
license:             BSD3
license-file:        LICENSE
author:              Michal J. Gajda
maintainer:          mjgajda@gmail.com
copyright:           Copyright by Michal J. Gajda '2015-'2018
category:            Language, Tools
build-type:          Simple
extra-source-files:  README.md changelog.md
cabal-version:       >=1.10
bug-reports:         https://github.com/mgajda/homplexity/issues
tested-with:         GHC==7.8.4
                   , GHC==7.10.3
                   , GHC==8.0.2
                   , GHC==8.2.2
                   , GHC==8.4.4
                   , GHC==8.6.2

source-repository head
  type:     git
  location: https://github.com/mgajda/homplexity.git

Library
  exposed-modules:
    Language.Haskell.Homplexity.Assessment
    Language.Haskell.Homplexity.CodeFragment
    Language.Haskell.Homplexity.Comments
    Language.Haskell.Homplexity.Cyclomatic
    Language.Haskell.Homplexity.Message
    Language.Haskell.Homplexity.Metric
    Language.Haskell.Homplexity.Parse
    Language.Haskell.Homplexity.TypeComplexity
    Language.Haskell.Homplexity.SrcSlice
  Hs-source-dirs:   .
  Other-Modules:
    Paths_homplexity
  build-depends:       base
                      ,haskell-src-exts
                      ,directory
                      ,filepath
                      ,hflags
                      ,uniplate
                      ,deepseq
                      ,containers
                      ,template-haskell
                      ,cpphs
  default-language:    Haskell2010

executable homplexity
  main-is:             Homplexity.hs
  other-extensions:    FlexibleContexts,
                       FlexibleInstances,
                       UndecidableInstances,
                       OverlappingInstances,
                       IncoherentInstances,
                       TypeSynonymInstances,
                       DeriveDataTypeable,
                       MultiParamTypeClasses,
                       RecordWildCards,
                       StandaloneDeriving,
                       ScopedTypeVariables,
                       TemplateHaskell,
                       ViewPatterns,
                       BangPatterns,
                       GeneralizedNewtypeDeriving,
                       TypeFamilies
  build-depends:       base             >=4.5  && <4.13,
                       haskell-src-exts >=1.18 && <1.21,
                       directory        >=1.1  && <1.4,
                       filepath         >=1.2  && <1.5,
                       hflags           >=0.3  && <0.5,
                       uniplate         >=1.4  && <1.7,
                       deepseq          >=1.3  && <1.7,
                       containers       >=0.3  && <0.7,
                       template-haskell >=2.6  && <2.14,
                       cpphs            >=1.5  && <1.21,
                       homplexity
  build-tools:         happy            >= 1.19.0
  default-language:    Haskell2010

test-suite Comments
  main-is:          tests/Comments.hs
  other-modules:    Language.Haskell.Homplexity.CodeFragment
                    Language.Haskell.Homplexity.Comments
                    Language.Haskell.Homplexity.SrcSlice
  type:             exitcode-stdio-1.0
  build-depends:    base             >=4.5  && <4.12,
                    haskell-src-exts >=1.18 && <1.21,
                    uniplate         >=1.4  && <1.7
  default-language: Haskell2010

