cabal-version:          2.2

name:                   hw-uri
version:                0.1.1.4
synopsis:               Supports IO on URIs
description:            Supports IO on URIs.
homepage:               https://github.com/haskell-works/hw-uri
license:                BSD-3-Clause
license-file:           LICENSE
author:                 John Ky
maintainer:             newhoggy@gmail.com
copyright:              John Ky 2019, David Turnbull 2019
category:               Data
extra-source-files:     CHANGELOG.md, README.md

source-repository head
  type: git
  location: https://github.com/haskell-works/hw-uri

common base                           { build-depends: base                           >= 4.7        && < 5      }

common aeson                          { build-depends: aeson                          >= 1.4.4.0    && < 1.5    }
common amazonka                       { build-depends: amazonka                       >= 1.6.1      && < 1.7    }
common amazonka-core                  { build-depends: amazonka-core                  >= 1.6.1      && < 1.7    }
common amazonka-s3                    { build-depends: amazonka-s3                    >= 1.6.1      && < 1.7    }
common antiope-core                   { build-depends: antiope-core                   >= 7.3.3      && < 8      }
common antiope-s3                     { build-depends: antiope-s3                     >= 7.3.3      && < 8      }
common bytestring                     { build-depends: bytestring                     >= 0.10.8.2   && < 0.11   }
common directory                      { build-depends: directory                      >= 1.3.3.0    && < 1.4    }
common exceptions                     { build-depends: exceptions                     >= 0.10.1     && < 0.11   }
common filepath                       { build-depends: filepath                       >= 1.3        && < 1.5    }
common generic-lens                   { build-depends: generic-lens                   >= 1.1.0.0    && < 1.3    }
common hedgehog                       { build-depends: hedgehog                       >= 0.5        && < 1.1    }
common hspec                          { build-depends: hspec                          >= 2.4        && < 3      }
common http-client                    { build-depends: http-client                    >= 0.5.14     && < 0.7    }
common http-types                     { build-depends: http-types                     >= 0.12.3     && < 0.13   }
common hw-hspec-hedgehog              { build-depends: hw-hspec-hedgehog              >= 0.1.0.4    && < 0.2    }
common lens                           { build-depends: lens                           >= 4.17       && < 5      }
common mtl                            { build-depends: mtl                            >= 2.2.2      && < 2.3    }
common resourcet                      { build-depends: resourcet                      >= 1.2.2      && < 1.3    }
common text                           { build-depends: text                           >= 1.2.3.1    && < 1.3    }

common config
  default-language:     Haskell2010
  ghc-options:          -Wall -fwarn-tabs -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates

library
  import:   base, config
          , aeson
          , amazonka
          , amazonka-core
          , amazonka-s3
          , antiope-core
          , antiope-s3
          , bytestring
          , directory
          , exceptions
          , filepath
          , generic-lens
          , http-client
          , http-types
          , lens
          , mtl
          , resourcet
          , text
  other-modules:        Paths_hw_uri
  autogen-modules:      Paths_hw_uri
  hs-source-dirs:       src
  exposed-modules:      HaskellWorks.Data.Uri.AWS.Env
                        HaskellWorks.Data.Uri.Internal.List
                        HaskellWorks.Data.Uri.Internal.Text
                        HaskellWorks.Data.Uri.IO.Console
                        HaskellWorks.Data.Uri.IO.Error
                        HaskellWorks.Data.Uri.IO.File
                        HaskellWorks.Data.Uri.IO.Lazy
                        HaskellWorks.Data.Uri.IO.Static
                        HaskellWorks.Data.Uri.Location
                        HaskellWorks.Data.Uri.Show
                        HaskellWorks.Data.Uri.Text
                        HaskellWorks.Data.Uri.UriError

test-suite hw-uri-test
  import:   base, config
          , antiope-core
          , antiope-s3
          , bytestring
          , aeson
          , filepath
          , hedgehog
          , hspec
          , http-types
          , hw-hspec-hedgehog
          , lens
          , text

  type:                 exitcode-stdio-1.0
  main-is:              Spec.hs
  build-depends:        hw-uri
  hs-source-dirs:       test
  ghc-options:          -threaded -rtsopts -with-rtsopts=-N
  build-tool-depends:   hspec-discover:hspec-discover
  other-modules:        HaskellWorks.Data.Uri.Gen
                        HaskellWorks.Data.Uri.AwsSpec
                        HaskellWorks.Data.Uri.LocationSpec
