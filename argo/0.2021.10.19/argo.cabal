cabal-version: 2.2

name: argo
version: 0.2021.10.19

build-type: Simple
category: JSON
description: Argo parses and renders JSON.
extra-source-files: CHANGELOG.md README.md
license-file: LICENSE.txt
license: MIT
maintainer: Taylor Fausak
synopsis: Parse and render JSON.

source-repository head
    location: https://github.com/tfausak/argo
    type: git

flag pedantic
    default: False
    description: Enables @-Werror@, which turns warnings into errors.
    manual: True

common library
    build-depends:
        , array >= 0.5.4 && < 0.6
        , base >= 4.14.0 && < 4.16
        , bytestring >= 0.10.12 && < 0.11
        , containers >= 0.6.4 && < 0.7
        , deepseq >= 1.4.4 && < 1.5
        , template-haskell >= 2.16.0 && < 2.18
        , text >= 1.2.4 && < 1.3
    default-language: Haskell2010
    ghc-options:
        -Weverything
        -Wno-all-missed-specialisations
        -Wno-implicit-prelude
        -Wno-missing-deriving-strategies
        -Wno-missing-export-lists
        -Wno-missing-exported-signatures
        -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module
        -Wno-safe
        -Wno-unsafe

    if flag(pedantic)
        ghc-options: -Werror

common executable
    import: library

    build-depends: argo
    ghc-options:
        -rtsopts
        -threaded
        -Wno-unused-packages

library
    import: library

    exposed-modules:
        Argo
        Argo.Class.FromValue
        Argo.Class.ToValue
        Argo.Decode
        Argo.Decoder
        Argo.Encode
        Argo.Literal
        Argo.Pattern
        Argo.QuasiQuoter
        Argo.Result
        Argo.Type
        Argo.Type.Array
        Argo.Type.Boolean
        Argo.Type.Null
        Argo.Type.Number
        Argo.Type.Object
        Argo.Type.Pair
        Argo.Type.String
        Argo.Type.Value
    hs-source-dirs: source/library

executable argo
    import: executable

    hs-source-dirs: source/executable
    main-is: Main.hs

test-suite argo-test-suite
    import: executable

    build-depends:
        , tasty >= 1.4.2 && < 1.5
        , tasty-hunit >= 0.10.0 && < 0.11
        , tasty-quickcheck >= 0.10.1 && < 0.11
    hs-source-dirs: source/test-suite
    main-is: Main.hs
    type: exitcode-stdio-1.0

benchmark argo-benchmark
    import: executable

    build-depends:
        , tasty-bench >= 0.2.5 && < 0.4
    hs-source-dirs: source/benchmark
    main-is: Main.hs
    type: exitcode-stdio-1.0
