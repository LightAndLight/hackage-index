cabal-version: 2.2

name: argo
version: 0.2021.11.2

build-type: Simple
category: JSON
description: Argo parses and renders JSON.
extra-source-files: CHANGELOG.md README.md
license-file: LICENSE.txt
license: MIT
maintainer: Taylor Fausak
synopsis: Parse and render JSON.

source-repository head
    location: https://github.com/tfausak/argo
    type: git

flag pedantic
    default: False
    description: Enables @-Werror@, which turns warnings into errors.
    manual: True

common library
    build-depends:
        , base >= 4.14.0 && < 4.17
        , bytestring >= 0.10.12 && < 0.12
        , containers >= 0.6.4 && < 0.7
        , deepseq >= 1.4.4 && < 1.5
        , template-haskell >= 2.16.0 && < 2.19
        , text >= 1.2.4 && < 1.3
        , transformers >= 0.5.6 && < 0.6
    default-language: Haskell2010
    ghc-options:
        -Weverything
        -Wno-all-missed-specialisations
        -Wno-implicit-prelude
        -Wno-missed-specialisations
        -Wno-missing-deriving-strategies
        -Wno-missing-export-lists
        -Wno-missing-exported-signatures
        -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module
        -Wno-safe
        -Wno-unsafe

    if flag(pedantic)
        ghc-options: -Werror

    if impl(ghc >= 9.2)
        ghc-options:
            -Wno-implicit-lift
            -Wno-missing-kind-signatures

common executable
    import: library

    build-depends: argo
    ghc-options:
        -rtsopts
        -threaded
        -Wno-unused-packages

library
    import: library

    autogen-modules: Paths_argo
    exposed-modules:
        Argo
        Argo.Class.FromValue
        Argo.Class.ToValue
        Argo.Codec
        Argo.Decode
        Argo.Decoder
        Argo.Encode
        Argo.Encoder
        Argo.Json.Array
        Argo.Json.Boolean
        Argo.Json.Member
        Argo.Json.Name
        Argo.Json.Null
        Argo.Json.Number
        Argo.Json.Object
        Argo.Json.String
        Argo.Json.Value
        Argo.Literal
        Argo.Main
        Argo.Pattern
        Argo.QuasiQuoter
        Argo.Result
    hs-source-dirs: source/library
    other-modules:
        Argo.Vendor.ByteString
        Argo.Vendor.Builder
        Argo.Vendor.DeepSeq
        Argo.Vendor.TemplateHaskell
        Argo.Vendor.Text
        Argo.Vendor.Transformers
        Paths_argo

executable argo
    import: executable

    hs-source-dirs: source/executable
    main-is: Main.hs

test-suite argo-test-suite
    import: executable

    build-depends:
        , tasty >= 1.4.2 && < 1.5
        , tasty-hunit >= 0.10.0 && < 0.11
        , tasty-quickcheck >= 0.10.1 && < 0.11
    hs-source-dirs: source/test-suite
    main-is: Main.hs
    type: exitcode-stdio-1.0

benchmark argo-benchmark
    import: executable

    build-depends:
        , tasty-bench >= 0.2.5 && < 0.4
    hs-source-dirs: source/benchmark
    main-is: Main.hs
    type: exitcode-stdio-1.0
