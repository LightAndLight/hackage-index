Cabal-version:  2.2
Name:           PrimitiveArray
Version:        0.9.1.0
License:        BSD-3-Clause
License-file:   LICENSE
Maintainer:     choener@bioinf.uni-leipzig.de
author:         Christian Hoener zu Siederdissen, 2011-2019
copyright:      Christian Hoener zu Siederdissen, 2011-2019
homepage:       https://github.com/choener/PrimitiveArray
bug-reports:    https://github.com/choener/PrimitiveArray/issues
Stability:      Experimental
Category:       Data
Build-type:     Simple
tested-with:    GHC == 8.4.4
Synopsis:       Efficient multidimensional arrays
Description:
                <http://www.bioinf.uni-leipzig.de/Software/gADP/ generalized Algebraic Dynamic Programming>
                .
                This library provides efficient multidimensional arrays. Import
                @Data.PrimitiveArray@ for indices, lenses, and arrays.
                .
                For
                <http://www.bioinf.uni-leipzig.de/Software/gADP/ generalized ADP>
                users, the library also provides the machinary to
                fill tables in the correct order required by usual CYK-style
                parsers, or regular grammars (used e.g. in alignment
                algorithms). This means that unless your grammar requires a
                strange order in which parsing is to be performed, it will
                mostly "just work".
                .
                In general operations do not perform bounds-checking or other
                sanity-checking and are aimed towards efficiency as much as
                possible. Users (like @ADPfusion@) should perform their own
                bounds-checking, outside of code that performs "loop-like"
                operations.



extra-source-files:
  README.md
  changelog.md



flag debug
  description:  Enable bounds checking and various other debug operations at the cost of a significant performance penalty.
  default:      False
  manual:       True

flag debugoutput
  description:  Enable debug output, which spams the screen full of index information
  default:      False
  manual:       True



common deps
  build-depends: base                     >= 4.7      &&  < 5.0
               , aeson                    >= 0.8
               , binary                   >= 0.7
               , bits                     >= 0.4
               , cereal                   >= 0.4
               , cereal-vector            >= 0.2
               , containers
               , deepseq                  >= 1.3
               , hashable                 >= 1.2
               , lens                     >= 4.0
               , log-domain               >= 0.10
               , mtl                      >= 2.0
               , primitive                >= 0.5.4
               , QuickCheck               >= 2.7
               , smallcheck               >= 1.1
               , tasty                    >= 0.11
               , tasty-quickcheck         >= 0.8
               , tasty-smallcheck         >= 0.8
               , tasty-th                 >= 0.1
               , text                     >= 1.0
               , vector                   >= 0.11
               , vector-binary-instances  >= 0.2
               , vector-th-unbox          >= 0.2
               --
               , DPutils                  == 0.1.0.*
               , OrderedBits              == 0.0.1.*
  default-extensions: BangPatterns
                    , CPP
                    , DataKinds
                    , DefaultSignatures
                    , DeriveDataTypeable
                    , DeriveGeneric
                    , FlexibleContexts
                    , FlexibleInstances
                    , FunctionalDependencies
                    , GADTs
                    , GeneralizedNewtypeDeriving
                    , MultiParamTypeClasses
                    , PolyKinds
                    , RankNTypes
                    , RecordWildCards
                    , ScopedTypeVariables
                    , StandaloneDeriving
                    , TemplateHaskell
                    , TypeApplications
                    , TypeFamilies
                    , TypeOperators
                    , UndecidableInstances
                    , UnicodeSyntax
  default-language:
    Haskell2010
  ghc-options:
    -O2
    -funbox-strict-fields
  if flag(debug)
    cpp-options: -DADPFUSION_CHECKS
    ghc-options: -fno-ignore-asserts -O0
  if flag(debugoutput)
    cpp-options: -DADPFUSION_DEBUGOUTPUT



Library
  import:
    deps
  Exposed-modules:
    Data.PrimitiveArray
    Data.PrimitiveArray.Checked
    Data.PrimitiveArray.Class
    Data.PrimitiveArray.Dense
--    Data.PrimitiveArray.FillTables
    Data.PrimitiveArray.Index
    Data.PrimitiveArray.Index.BitSet0
    Data.PrimitiveArray.Index.BitSet1
    Data.PrimitiveArray.Index.BitSetClasses
--    Data.PrimitiveArray.Index.BS0
--    Data.PrimitiveArray.Index.BS2
    Data.PrimitiveArray.Index.Class
--    Data.PrimitiveArray.Index.EdgeBoundary
    Data.PrimitiveArray.Index.Int
    Data.PrimitiveArray.Index.IOC
    Data.PrimitiveArray.Index.PhantomInt
    Data.PrimitiveArray.Index.Point
--    Data.PrimitiveArray.Index.Set
    Data.PrimitiveArray.Index.Subword
--    Data.PrimitiveArray.Index.TernarySet
    Data.PrimitiveArray.Index.Unit
    Data.PrimitiveArray.ScoreMatrix



test-suite properties
  import:
    deps
  type:
    exitcode-stdio-1.0
  main-is:
    properties.hs
  other-modules:
    QuickCheck
    SmallCheck
    Common
  ghc-options:
    -threaded -rtsopts -with-rtsopts=-N
  hs-source-dirs:
    tests
  build-depends: PrimitiveArray



source-repository head
  type: git
  location: git://github.com/choener/PrimitiveArray

