Name:          curry-frontend
Version:       1.0.2
Cabal-Version: >= 1.10
Synopsis:      Compile the functional logic language Curry to several
               intermediate formats
Description:   The Curry front end consists of the executable program
               "curry-frontend".
               It is used by various backends to compile Curry programs to
               an intermediate representation.
               The code is a stripped-down version of an early version of
               the Muenster Curry Compiler
               (<http://danae.uni-muenster.de/curry/>)
               which has been extended to produce different intermediate
               representations.
               For further information, please check
               <http://curry-language.org>
Category:      Language
License:       BSD3
License-File:  LICENSE
Author:        Wolfgang Lux, Martin Engelke, Bernd Brassel, Holger Siegel,
               Bjoern Peemoeller, Finn Teegen
Maintainer:    fte@informatik.uni-kiel.de
Homepage:      http://curry-language.org
Build-Type:    Simple
Stability:     experimental

Extra-Source-Files: LIESMICH CHANGELOG.md

Data-Dir:   data
Data-Files: currysource.css

source-repository head
  type:     git
  location: https://git.ps.informatik.uni-kiel.de/curry/curry-frontend.git

Flag network-uri
   description: Get Network.URI from the network-uri package
   default: True

Library
  hs-source-dirs:   src
  default-language:  Haskell2010
  Build-Depends:
      base == 4.*
    , containers
    , curry-base == 1.1.0
    , directory
    , extra >= 1.4.6
    , filepath
    , mtl
    , pretty
    , process
    , set-extra
    , transformers
  if flag(network-uri)
    build-depends: network-uri >= 2.6
  else
    build-depends: network < 2.6
  Exposed-Modules:
      Base.AnnotExpr
    , Base.CurryKinds
    , Base.CurryTypes
    , Base.Expr
    , Base.KindSubst
    , Base.Kinds
    , Base.Messages
    , Base.NestEnv
    , Base.PrettyKinds
    , Base.PrettyTypes
    , Base.SCC
    , Base.Subst
    , Base.TopEnv
    , Base.TypeExpansion
    , Base.TypeSubst
    , Base.Types
    , Base.Typing
    , Base.Utils
    , Checks
    , Checks.DeriveCheck
    , Checks.ExportCheck
    , Checks.ExtensionCheck
    , Checks.ImportSyntaxCheck
    , Checks.InstanceCheck
    , Checks.InterfaceCheck
    , Checks.InterfaceSyntaxCheck
    , Checks.KindCheck
    , Checks.PrecCheck
    , Checks.SyntaxCheck
    , Checks.TypeCheck
    , Checks.TypeSyntaxCheck
    , Checks.WarnCheck
    , CompilerEnv
    , CompilerOpts
    , CondCompile
    , CurryBuilder
    , CurryDeps
    , Env.Class
    , Env.Instance
    , Env.Interface
    , Env.ModuleAlias
    , Env.OpPrec
    , Env.Type
    , Env.TypeConstructor
    , Env.Value
    , Exports
    , Files.CymakePath
    , Generators
    , Generators.GenAbstractCurry
    , Generators.GenFlatCurry
    , Generators.GenTypedFlatCurry
    , Generators.GenTypeAnnotatedFlatCurry
    , Html.CurryHtml
    , Html.SyntaxColoring
    , IL
    , IL.Pretty
    , IL.ShowModule
    , IL.Type
    , IL.Typing
    , Imports
    , Interfaces
    , Modules
    , TokenStream
    , Transformations
    , Transformations.CaseCompletion
    , Transformations.CurryToIL
    , Transformations.Derive
    , Transformations.Desugar
    , Transformations.Dictionary
    , Transformations.Lift
    , Transformations.Newtypes
    , Transformations.Qual
    , Transformations.Simplify
  Other-Modules:
    Paths_curry_frontend
  ghc-options:       -Wall -fno-warn-orphans

Executable curry-frontend
  hs-source-dirs: src
  Main-is:        cymake.hs
  default-language:  Haskell2010
  Build-Depends:
      base == 4.*
    , containers
    , curry-base == 1.1.0
    , curry-frontend
    , directory
    , extra >= 1.4.6
    , filepath
    , mtl
    , pretty
    , process
    , set-extra
    , transformers
  if flag(network-uri)
    build-depends: network-uri >= 2.6
  else
    build-depends: network < 2.6
  ghc-options: -Wall -fno-warn-orphans

Test-Suite test-frontend
  type:           detailed-0.9
  hs-source-dirs: test
  default-language:  Haskell2010
  test-module:    TestFrontend
  build-depends:  base == 4.*, Cabal >= 1.20, curry-base == 1.1.0
    , curry-frontend, filepath
