name:                wai-request-spec
version:             0.10.2.4
synopsis:            Declarative request parsing
copyright:           Allele Dev 2016
license:             BSD3
license-file:        LICENSE
author:              Allele Dev
maintainer:          allele.dev@gmail.com
homepage:            https://gitlab.com/queertypes/wai-request-spec
bug-reports:         https://gitlab.com/queertypes/wai-request-spec/issues
category:            Network, Wai
build-type:          Simple
cabal-version:       >=1.18
tested-with:         GHC==7.8.4
                   , GHC==7.10.2
description:
    WAI Request Spec is a declarative validation layer for HTTP
    requests. It aims to make error-handling for malformed requests as
    easy as taking the happy path.
    .
    A brief summary of the core features:
    .
    * Can specify headers and query params as input sources
    .
    * Support for parsing ints, floats, text, bytes (with encodings), and bools
    .
    * A parser combinator interface for chaining together request requirements
    .
    * Support for Alternatives
    .
    * Support for optional parameters
    .
    * Convenient and informative default error messages that let service
      consumers know what went wrong
    .
    It is built on WAI, so it is compatible with several Haskell
    web frameworks. All you need is the ability to access the Request
    object, and WAI Request Spec takes care of the rest!

extra-source-files:
    README.md
    changelog.md
    CODE_OF_CONDUCT.md

source-repository head
    type: git
    location: git clone https://gitlab.com/queertypes/wai-request-spec.git

library
  exposed-modules: Network.Wai.RequestSpec
                 , Network.Wai.RequestSpec.Class
                 , Network.Wai.RequestSpec.Combinators
                 , Network.Wai.RequestSpec.Error
                 , Network.Wai.RequestSpec.Parser
                 , Network.Wai.RequestSpec.Internal.Combinators
                 , Network.Wai.RequestSpec.Internal.Env
                 , Network.Wai.RequestSpec.Internal.Env.Types
                 , Network.Wai.RequestSpec.Internal.Parser
  other-modules: Network.Wai.RequestSpec.Internal.Combinators.Params
  build-depends:       base >=4.7 && <5
                     , bytestring
                     , case-insensitive
                     , containers
                     , http-types
                     , text
                     , wai
  hs-source-dirs:      src
  ghc-options:         -Wall
  default-language:    Haskell2010

benchmark benchmarks
  type: exitcode-stdio-1.0
  hs-source-dirs: benchmarks
  ghc-options: -O2 -Wall -rtsopts
  main-is: Benchmarks.hs
  build-depends:
                base
              , bytestring
              , criterion
              , http-types
              , text
              , wai
              , wai-request-spec
  default-language:    Haskell2010
