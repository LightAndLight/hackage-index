cabal-version: 1.18
name: fast-arithmetic
version: 0.3.3.2
x-revision: 2
license: BSD3
license-file: LICENSE
copyright: Copyright: (c) 2018 Vanessa McHale
maintainer: vamchale@gmail.com
author: Vanessa McHale
homepage: https://github.com/vmchale/fast-arithmetic#readme
synopsis: Fast functions on integers.
description:
    Fast functions for number theory and combinatorics with a high level of safety guaranteed by [ATS](http://www.ats-lang.org/).
category: Numerics, Math, Algorithms, Number Theory, Combinatorics, FFI, ATS
build-type: Custom
extra-source-files:
    ats-src/*.dats
    atspkg.dhall
    dist-newstyle/lib/empty
extra-doc-files: README.md

custom-setup
    setup-depends: base -any,
                   Cabal -any,
                   ats-pkg >= 2.8.0.8

source-repository head
    type: git
    location: git@github.com:vmchale/fast-arithmetic.git

flag development
    description:
        Enable `-Werror` and don't clean ATS libraries between builds.
    default: False
    manual: True

library
    exposed-modules:
        Numeric.Pure
        Numeric.Integer
        Numeric.NumberTheory
        Numeric.Combinatorics
    hs-source-dirs: src
    other-modules:
        Numeric.Common
    default-language: Haskell2010
    extra-libraries:
        numbertheory
    extra-lib-dirs: dist-newstyle/lib
    ghc-options: -Wall -optc-mtune=native -optc-flto -optc-O3
    build-depends:
        base >=5 && <5,
        composition-prelude -any,
        gmpint -any
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wcompat

test-suite fast-arithmetic-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base -any,
        fast-arithmetic -any,
        hspec -any,
        QuickCheck -any,
        arithmoi -any,
        combinat -any
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wcompat

benchmark fast-arithmetic-bench
    type: exitcode-stdio-1.0
    main-is: Bench.hs
    hs-source-dirs: bench
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        base -any,
        fast-arithmetic -any,
        criterion -any,
        arithmoi -any,
        combinat -any
    
    if flag(development)
        ghc-options: -Werror
    
    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wcompat
