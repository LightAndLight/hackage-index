cabal-version: 1.18
name: fast-arithmetic
version: 0.6.2.3
license: BSD3
license-file: LICENSE
copyright: Copyright: (c) 2018 Vanessa McHale
maintainer: vamchale@gmail.com
author: Vanessa McHale
tested-with: ghc ==7.0.4 ghc ==7.2.2 ghc ==7.4.2 ghc ==7.6.3
             ghc ==7.8.4 ghc ==7.10.3 ghc ==8.0.2 ghc ==8.2.2 ghc ==8.4.3
             ghc ==8.6.1
bug-reports: https://github.com/vmchale/hs-ats/issues
synopsis: Fast functions on integers.
description:
    Fast functions for number theory and combinatorics with a high level of safety guaranteed by [ATS](http://www.ats-lang.org/).
category: Numerics, Math, Algorithms, Number Theory, Combinatorics, FFI, ATS
build-type: Simple
extra-source-files:
    atspkg.dhall
    pkg.dhall
    lib.dhall
    source.dhall
    bench.dhall
    ats-src/*.dats
    ats-src/*.sats
    .atspkg/contrib/ats-includes-0.3.11/ccomp/runtime/*.h
    .atspkg/contrib/ats-includes-0.3.11/ccomp/runtime/*.c
    .atspkg/contrib/ats-includes-0.3.11/prelude/CATS/*.cats
    .atspkg/contrib/ats-includes-0.3.11/libats/libc/CATS/*.cats
    .atspkg/contrib/ats-includes-0.3.11/libats/libc/CATS/sys/*.cats
    .atspkg/contrib/atscntrb-hx-intinf/*.hats
    .atspkg/contrib/atscntrb-hx-libgmp/CATS/*.cats
extra-doc-files: README.md
                 CHANGELOG.md

source-repository head
    type: git
    location: git@github.com:vmchale/hs-ats.git

flag no-integer-gmp
    description:
        Disable functions relying on GMP
    default: False

flag development
    description:
        Enable `-Werror`.
    default: False
    manual: True

library
    exposed-modules:
        Numeric.NumberTheory
    c-sources:
        cbits/number-theory.c
        cbits/numerics.c
    hs-source-dirs: src
    other-modules:
        Numeric.Common
    default-language: Haskell2010
    include-dirs: .atspkg/contrib/ats-includes-0.3.11/ccomp/runtime
                  .atspkg/contrib/ats-includes-0.3.11/ .atspkg/contrib
    ghc-options: -Wall
    build-depends:
        base >=4.3 && <5,
        composition-prelude >=1.2.0.0

    if impl(ghc >=8.0)
        cc-options: -mtune=native -flto -O3

    if os(windows)
        buildable: False

    if !flag(no-integer-gmp)
        exposed-modules:
            Numeric.Combinatorics

    if !flag(no-integer-gmp)
        c-sources:
            cbits/combinatorics.c

    if !flag(no-integer-gmp)
        build-depends:
            gmpint -any

    if os(windows)
        buildable: False

    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wcompat

    if flag(development)
        ghc-options: -Werror

test-suite fast-arithmetic-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base -any,
        fast-arithmetic -any,
        hspec -any,
        QuickCheck -any,
        arithmoi >=0.6.0.0

    if impl(ghc >=8.4)
        build-depends:
            combinat-compat -any
    else
        build-depends:
            combinat -any

    if flag(development)
        ghc-options: -Werror

    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wcompat

benchmark fast-arithmetic-bench
    type: exitcode-stdio-1.0
    main-is: Bench.hs
    hs-source-dirs: bench
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        base -any,
        fast-arithmetic -any,
        criterion -any,
        arithmoi >=0.6.0.0

    if impl(ghc >=8.4)
        build-depends:
            combinat-compat -any
    else
        build-depends:
            combinat -any

    if flag(development)
        ghc-options: -Werror

    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
                     -Wcompat
