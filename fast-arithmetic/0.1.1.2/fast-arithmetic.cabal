name:                fast-arithmetic
version:             0.1.1.2
x-revision: 1
synopsis:            Fast number-theoretic functions.
description:         Fast number-theoretic code with a high level of safety guaranteed by ATS.
homepage:            https://github.com/vmchale/fast-arithmetic#readme
license:             BSD3
license-file:        LICENSE
author:              Vanessa McHale
maintainer:          vamchale@gmail.com
copyright:           Copyright: (c) 2018 Vanessa McHale
category:            Numerics, Math, Algorithms, Number Theory, Combinatorics
build-type:          Custom
extra-source-files:  ats-deps/prelude/ATS2-Postiats-include-0.3.8/ccomp/runtime/pats_ccomp_config.h
                   , ats-src/*.dats
                   , cabal.project.local
                   , shake.hs
                   , ats-deps/contrib/atscntrb-libgmp/CATS/gmp.cats
extra-doc-files:     README.md
cabal-version:       >= 1.18

Flag development {
  Description: Enable `-Werror` and don't clean ATS libraries between builds.
  manual: True
  default: False
}

custom-setup
  -- ghc-options:     -threaded -rtsopts -with-rtsopts=-N
  setup-depends:   base
                 , Cabal >= 2.0
                 , http-client
                 , http-client-tls
                 , tar
                 , zlib
                 , directory
                 , parallel-io

library
  c-sources:           cbits/numerics.c
                     , cbits/number-theory.c
                     , cbits/combinatorics.c
  include-dirs:        ats-deps/
                     , ats-deps/contrib
                     , ats-deps/prelude/ATS2-Postiats-include-0.3.8/ccomp/runtime
                     , ats-deps/prelude/ATS2-Postiats-include-0.3.8
  hs-source-dirs:      src
  exposed-modules:     Numeric.Pure
                     , Numeric.Integer
                     , Numeric.NumberTheory
                     , Numeric.Combinatorics
                     , Data.GMP
  other-modules:       Numeric.Common
  build-depends:       base >= 4.7 && < 5
                     , composition-prelude < 1.2.0.0
                     , recursion-schemes
  default-language:    Haskell2010
  if flag(development)
    if impl(ghc >= 8.0)
      ghc-options:       -Werror
  if impl(ghc >= 8.0)
    ghc-options:       -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat
  ghc-options:         -Wall -optc-mtune=native -optc-flto -optc-O3

test-suite fast-arithmetic-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Spec.hs
  build-depends:       base
                     , fast-arithmetic
                     , hspec
  if flag(development)
    ghc-options: -Werror
  if impl(ghc >= 8.0)
    ghc-options:       -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat 
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N -Wall
  default-language:    Haskell2010

benchmark fast-arithmetic-bench
  type:                exitcode-stdio-1.0
  hs-source-dirs:      bench
  main-is:             Bench.hs
  build-depends:       base
                     , fast-arithmetic
                     , criterion
  if flag(development)
    ghc-options: -Werror
  if impl(ghc >= 8.0)
    ghc-options:       -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat
  ghc-options:         -Wall
  default-language:    Haskell2010

source-repository head
  type:     git
  location: https://github.com/vmchale/fast-arithmetic
