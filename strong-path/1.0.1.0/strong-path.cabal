cabal-version:      1.12
name:               strong-path
version:            1.0.1.0
license:            MIT
license-file:       LICENSE
copyright:          2020 Martin Sosic
maintainer:         sosic.martin@gmail.com
author:             Martin Sosic
homepage:           https://github.com/wasp-lang/strong-path#readme
bug-reports:        https://github.com/wasp-lang/strong-path/issues
synopsis:           Strongly typed paths in Haskell.
description:
    Replacement for a FilePath that enables you to handle filepaths in your code in a type-safe manner. You can specify at type level if they are relative, absolute, file, directory, posix, windows, and even to which file or directory they point to or are relative to.

category:           System, Filesystem, FilePath
build-type:         Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
    type:     git
    location: https://github.com/wasp-lang/strong-path

library
    exposed-modules:
        StrongPath
        StrongPath.FilePath
        StrongPath.Internal
        StrongPath.Operations
        StrongPath.Path
        StrongPath.TH
        StrongPath.Types

    hs-source-dirs:   src
    other-modules:    Paths_strong_path
    default-language: Haskell2010
    ghc-options:      -Wall
    build-depends:
        base >=4.7 && <5,
        exceptions >=0.10.4 && <0.11,
        filepath >=1.4.2.1 && <1.5,
        path >=0.9.0 && <0.10,
        template-haskell >=2.17.0.0 && <2.18

test-suite strong-path-test
    type:             exitcode-stdio-1.0
    main-is:          TastyDiscoverDriver.hs
    hs-source-dirs:   test
    other-modules:
        PathTest
        StrongPath.FilePathTest
        StrongPath.InternalTest
        StrongPath.PathTest
        StrongPath.THTest
        StrongPathTest
        Test.Utils
        Paths_strong_path

    default-language: Haskell2010
    ghc-options:      -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        base >=4.7 && <5,
        filepath >=1.4.2.1 && <1.5,
        hspec >=2.8.2 && <2.9,
        path >=0.9.0 && <0.10,
        strong-path -any,
        tasty >=1.4.1 && <1.5,
        tasty-discover >=4.2.2 && <4.3,
        tasty-hspec ==1.2.*,
        tasty-quickcheck >=0.10.1.2 && <0.11
