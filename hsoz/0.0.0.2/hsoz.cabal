name:                hsoz
version:             0.0.0.2
synopsis:            Iron, Hawk, Oz: Web auth protocols
description:

  <<images/iron.png>> &#x20;__&#x20;
  <<images/hawk.png>> &#x20;__&#x20;
  <<images/oz.png>>
  .

  __hsoz__ is a Haskell implementation of the Iron, Hawk, and Oz web
  authentication protocols. These protocols originate from the OAuth2
  standardisation process, but are designed to be simpler to implement
  for the common case of web applications.

  .

  This module is based on the Javascript code and documentation by
  Eran Hammer and others. A fair amount of Hammer's descriptive text
  has been incorporated into this documentation, as well as the cool
  logos.

  .
  == Introduction
  .
  In the words of their principal designer:
  .
  __Iron__ is a cryptographic utility for sealing a JSON object using
  symmetric key encryption with message integrity verification. Or in
  other words, it lets you encrypt an object, send it around (in
  cookies, authentication credentials, etc.), then receive it back and
  decrypt it. The algorithm ensures that the message was not tampered
  with, and also provides a simple mechanism for password rotation.
  .
  __Hawk__ is an HTTP authentication scheme using a message
  authentication code (MAC) algorithm to provide partial HTTP request
  cryptographic verification.
  .
  __Oz__ is a web authorization protocol based on industry best
  practices. Oz combines the Hawk authentication protocol with the
  Iron encryption protocol to provide a simple to use and secure
  solution for granting and authenticating third-party access to an
  API on behalf of a user or an application.
  .
  == Usage
  .
  The top-level "Network.Iron", "Network.Hawk", "Network.Oz" modules
  contain further instructions on their usage. There are also some
  example server and client programs within the
  <https://github.com/rvl/hsoz project git repository>.


homepage:            https://github.com/rvl/hsoz#readme
license:             BSD3
author:              Rodney Lorrimar
maintainer:          dev@rodney.id.au
copyright:           2016 Rodney Lorrimar
category:            Web, Authentication
build-type:          Simple
extra-doc-files:     images/*.png
cabal-version:       >=1.10

library
  hs-source-dirs:      src
  exposed-modules:     Network.Iron
                     , Network.Hawk
                     , Network.Hawk.Types
                     , Network.Hawk.Server
                     , Network.Hawk.Server.Types
                     , Network.Hawk.Middleware
                     , Network.Hawk.Client
                     , Network.Hawk.Client.Types
                     , Network.Hawk.URI
                     , Network.Oz
                     , Network.Oz.Application
                     , Network.Oz.Client
                     , Network.Oz.Server
                     , Network.Oz.Ticket
                     , Network.Oz.Types
  other-modules:       Network.Iron.Util
                     , Network.Hawk.Algo
                     , Network.Hawk.Common
                     , Network.Hawk.Util
                     , Network.Hawk.Client.HeaderParser
                     , Network.Oz.JSON
                     , Network.Oz.Internal.Types
                     , Network.Oz.Boom
  build-depends:       base >= 4.7 && < 5
                     , aeson
                     , attoparsec
                     , base16-bytestring
                     , base64-bytestring
                     , byteable
                     , bytestring
                     , case-insensitive
                     , containers
                     , cryptonite
                     , data-default
                     , either
                     , errors
                     , http-client
                     , http-types
                     , lens
                     , memory
                     , mtl
                     , network
                     , scientific
                     , scotty
                     , securemem
                     , text
                     , time
                     , transformers
                     , uri-bytestring
                     , vault
                     , wai
                     , warp
  default-language:    Haskell2010
  default-extensions:  OverloadedStrings

executable hsoz-example
  hs-source-dirs:      example
  main-is:             Main.hs
  other-modules:       Common
                     , HawkServer
                     , HawkClient
                     , BewitServer
                     , BewitClient
                     , OzServer
                     , OzClient
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N
  build-depends:       hsoz
                     , aeson
                     , base
                     , bytestring
                     , case-insensitive
                     , containers
                     , cryptonite
                     , data-default
                     , http-client
                     , http-conduit
                     , http-types
                     , lens
                     , lucid
                     , scotty
                     , text
                     , transformers
                     , uri-bytestring
                     , wai
                     , warp
                     , wreq
  default-language:    Haskell2010
  default-extensions:  OverloadedStrings
  if impl(ghcjs)
    -- this is just because of wreq pulling in unbuildable deps
    buildable:         False

test-suite hsoz-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Main.hs
  other-modules:       Network.Iron.Tests
                     , Network.Hawk.Tests
                     , Network.Oz.Tests
  build-depends:       base
                     , hsoz
                     , QuickCheck
                     , HUnit
                     , tasty
                     , tasty-hunit
                     , tasty-quickcheck
                     , tasty-golden
                     , text
                     , bytestring
                     , aeson
                     , time
                     , data-default
                     , wai
                     , http-client
                     , http-types
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N
  default-language:    Haskell2010
  default-extensions:  OverloadedStrings

source-repository head
  type:     git
  location: https://github.com/rvl/hsoz
