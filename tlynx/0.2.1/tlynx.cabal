cabal-version: 1.12
name: tlynx
version: 0.2.1
license: GPL-3
license-file: LICENSE
copyright: Dominik Schrempf (2020)
maintainer: dominik.schrempf@gmail.com
author: Dominik Schrempf
homepage: https://github.com/dschrempf/elynx#readme
bug-reports: https://github.com/dschrempf/elynx/issues
synopsis: Handle phylogenetic trees
description:
    Examine, compare, and simulate phylogenetic trees in a reproducible way. Please see the README on GitHub at <https://github.com/dschrempf/elynx>.
category: Bioinformatics
build-type: Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
    type: git
    location: https://github.com/dschrempf/elynx

library
    exposed-modules:
        TLynx.Coalesce.Coalesce
        TLynx.Coalesce.Options
        TLynx.Compare.Compare
        TLynx.Compare.Options
        TLynx.Connect.Connect
        TLynx.Connect.Options
        TLynx.Distance.Distance
        TLynx.Distance.Options
        TLynx.Examine.Examine
        TLynx.Examine.Options
        TLynx.Options
        TLynx.Parsers
        TLynx.Shuffle.Options
        TLynx.Shuffle.Shuffle
        TLynx.Simulate.Options
        TLynx.Simulate.Simulate
        TLynx.TLynx
    hs-source-dirs: src
    other-modules:
        Paths_tlynx
    default-language: Haskell2010
    ghc-options: -Wall -fllvm
    build-depends:
        aeson >=1.4.7.1 && <1.5,
        array >=0.5.4.0 && <0.6,
        base >=4.13.0.0 && <4.14,
        bytestring >=0.10.10.0 && <0.11,
        comonad >=5.0.6 && <5.1,
        containers >=0.6.2.1 && <0.7,
        elynx-tools >=0.2.1 && <0.3,
        elynx-tree >=0.2.1 && <0.3,
        gnuplot >=0.5.6.1 && <0.6,
        lifted-async >=0.10.0.6 && <0.11,
        megaparsec >=8.0.0 && <8.1,
        monad-logger >=0.3.32 && <0.4,
        mwc-random >=0.14.0.0 && <0.15,
        optparse-applicative >=0.15.1.0 && <0.16,
        parallel >=3.2.2.0 && <3.3,
        primitive >=0.7.0.1 && <0.8,
        scientific >=0.3.6.2 && <0.4,
        statistics >=0.15.2.0 && <0.16,
        text >=1.2.4.0 && <1.3,
        transformers >=0.5.6.2 && <0.6,
        vector >=0.12.1.2 && <0.13

executable tlynx
    main-is: Main.hs
    hs-source-dirs: app
    other-modules:
        Paths_tlynx
    default-language: Haskell2010
    ghc-options: -Wall -fllvm -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        aeson >=1.4.7.1 && <1.5,
        array >=0.5.4.0 && <0.6,
        base >=4.13.0.0 && <4.14,
        bytestring >=0.10.10.0 && <0.11,
        comonad >=5.0.6 && <5.1,
        containers >=0.6.2.1 && <0.7,
        elynx-tools >=0.2.1 && <0.3,
        gnuplot >=0.5.6.1 && <0.6,
        lifted-async >=0.10.0.6 && <0.11,
        megaparsec >=8.0.0 && <8.1,
        monad-logger >=0.3.32 && <0.4,
        mwc-random >=0.14.0.0 && <0.15,
        optparse-applicative >=0.15.1.0 && <0.16,
        parallel >=3.2.2.0 && <3.3,
        primitive >=0.7.0.1 && <0.8,
        scientific >=0.3.6.2 && <0.4,
        statistics >=0.15.2.0 && <0.16,
        text >=1.2.4.0 && <1.3,
        tlynx -any,
        transformers >=0.5.6.2 && <0.6,
        vector >=0.12.1.2 && <0.13
