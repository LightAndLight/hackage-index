cabal-version: >=1.22
name: sbp
version: 2.4.7
license: LGPL-3
copyright: Copyright (C) 2015-2018 Swift Navigation, Inc.
maintainer: Swift Navigation <dev@swiftnav.com>
author: Swift Navigation Inc.
homepage: https://github.com/swift-nav/libsbp
synopsis: SwiftNav's SBP Library
description:
    Haskell bindings for Swift Navigation Binary Protocol (SBP), a fast,
    simple, and minimal binary protocol for communicating with Swift
    devices. It is the native binary protocol used by the Piksi GPS
    receiver to transmit solutions, observations, status and debugging
    messages, as well as receive messages from the host operating
    system, such as differential corrections and the almanac.
category: Network
build-type: Simple
extra-source-files:
    README.md

source-repository head
    type: git
    location: git@github.com:swift-nav/libsbp.git

library
    exposed-modules:
        SwiftNav.CRC16
        SwiftNav.SBP
        SwiftNav.SBP.Acquisition
        SwiftNav.SBP.Bootload
        SwiftNav.SBP.ExtEvents
        SwiftNav.SBP.FileIo
        SwiftNav.SBP.Flash
        SwiftNav.SBP.Gnss
        SwiftNav.SBP.Imu
        SwiftNav.SBP.Linux
        SwiftNav.SBP.Logging
        SwiftNav.SBP.Mag
        SwiftNav.SBP.Navigation
        SwiftNav.SBP.Ndb
        SwiftNav.SBP.Observation
        SwiftNav.SBP.Orientation
        SwiftNav.SBP.Piksi
        SwiftNav.SBP.Sbas
        SwiftNav.SBP.Settings
        SwiftNav.SBP.Ssr
        SwiftNav.SBP.System
        SwiftNav.SBP.Tracking
        SwiftNav.SBP.User
        SwiftNav.SBP.Vehicle
        SwiftNav.SBP.Types
    hs-source-dirs: src
    other-modules:
        SwiftNav.SBP.Msg
        SwiftNav.SBP.TH
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        aeson >=1.2.4.0,
        array >=0.5.2.0,
        base >=4.8 && <5,
        base64-bytestring >=1.0.0.1,
        basic-prelude >=0.7.0,
        binary >=0.8.5.1,
        bytestring >=0.10.8.2,
        data-binary-ieee754 >=0.4.4,
        lens >=4.15.4,
        lens-aeson >=1.0.2,
        monad-loops >=0.4.3,
        template-haskell >=2.12.0.0,
        text >=1.2.2.2

executable sbp2json
    main-is: SBP2JSON.hs
    hs-source-dirs: main
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        aeson >=1.2.4.0,
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        binary-conduit >=1.2.5,
        bytestring >=0.10.8.2,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        resourcet >=1.1.11,
        sbp -any

executable json2sbp
    main-is: JSON2SBP.hs
    hs-source-dirs: main
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        aeson >=1.2.4.0,
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        binary-conduit >=1.2.5,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        resourcet >=1.1.11,
        sbp -any

executable json2json
    main-is: JSON2JSON.hs
    hs-source-dirs: main
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        aeson >=1.2.4.0,
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        bytestring >=0.10.8.2,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        resourcet >=1.1.11,
        sbp -any,
        time >=1.8.0.2

executable sbp2yaml
    main-is: SBP2YAML.hs
    hs-source-dirs: main
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        binary-conduit >=1.2.5,
        bytestring >=0.10.8.2,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        resourcet >=1.1.11,
        sbp -any,
        yaml >=0.8.28

test-suite test
    type: exitcode-stdio-1.0
    main-is: Test.hs
    hs-source-dirs: test
    other-modules:
        Test.SwiftNav.CRC16
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        sbp -any,
        tasty >=0.11.3,
        tasty-hunit >=0.9.2
