name: sbp
version: 2.2.11
cabal-version: >=1.22
build-type: Simple
license: LGPL-3
copyright: Copyright (C) 2015 Swift Navigation, Inc.
maintainer: Mark Fine <dev@swiftnav.com>
homepage: https://github.com/swift-nav/libsbp
synopsis: SwiftNav's SBP Library
description:
    Haskell bindings for Swift Navigation Binary Protocol (SBP), a fast,
    simple, and minimal binary protocol for communicating with Swift
    devices. It is the native binary protocol used by the Piksi GPS
    receiver to transmit solutions, observations, status and debugging
    messages, as well as receive messages from the host operating
    system, such as differential corrections and the almanac.
category: Network
author: Swift Navigation Inc.
extra-source-files:
    README.md

source-repository head
    type: git
    location: git@github.com:swift-nav/libsbp.git

library
    exposed-modules:
        SwiftNav.CRC16
        SwiftNav.SBP
    build-depends:
        aeson >=0.11.3.0,
        array >=0.5.1.0,
        base >=4.8 && <5,
        base64-bytestring >=1.0.0.1,
        basic-prelude >=0.5.2,
        binary >=0.7.5.0,
        bytestring >=0.10.6.0,
        data-binary-ieee754 >=0.4.4,
        lens >=4.13,
        lens-aeson >=1.0.0.5,
        monad-loops >=0.4.3,
        template-haskell >=2.10.0.0,
        text >=1.2.2.2,
        unordered-containers >=0.2.8.0
    default-language: Haskell2010
    hs-source-dirs: src
    other-modules:
        SwiftNav.SBP.Msg
        SwiftNav.SBP.TH
        SwiftNav.SBP.Types
        SwiftNav.SBP.Acquisition
        SwiftNav.SBP.Bootload
        SwiftNav.SBP.ExtEvents
        SwiftNav.SBP.FileIo
        SwiftNav.SBP.Flash
        SwiftNav.SBP.Gnss
        SwiftNav.SBP.Imu
        SwiftNav.SBP.Logging
        SwiftNav.SBP.Navigation
        SwiftNav.SBP.Ndb
        SwiftNav.SBP.Observation
        SwiftNav.SBP.Piksi
        SwiftNav.SBP.Settings
        SwiftNav.SBP.System
        SwiftNav.SBP.Tracking
        SwiftNav.SBP.User
    ghc-options: -Wall

executable sbp2json
    main-is: SBP2JSON.hs
    build-depends:
        aeson >=0.11.3.0,
        base >=4.8.2.0,
        basic-prelude >=0.5.2,
        binary-conduit >=1.2.4.1,
        bytestring >=0.10.6.0,
        conduit >=1.2.10,
        conduit-extra >=1.1.16,
        resourcet >=1.1.9,
        sbp
    default-language: Haskell2010
    hs-source-dirs: main
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall

executable json2sbp
    main-is: JSON2SBP.hs
    build-depends:
        aeson >=0.11.3.0,
        base >=4.8.2.0,
        basic-prelude >=0.5.2,
        binary-conduit >=1.2.4.1,
        conduit >=1.2.10,
        conduit-extra >=1.1.16,
        resourcet >=1.1.9,
        sbp
    default-language: Haskell2010
    hs-source-dirs: main
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall

executable sbp2yaml
    main-is: SBP2YAML.hs
    build-depends:
        base >=4.8.2.0,
        basic-prelude >=0.5.2,
        binary-conduit >=1.2.4.1,
        bytestring >=0.10.6.0,
        conduit >=1.2.10,
        conduit-extra >=1.1.16,
        resourcet >=1.1.9,
        sbp,
        yaml >=0.8.23
    default-language: Haskell2010
    hs-source-dirs: main
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall

test-suite test
    type: exitcode-stdio-1.0
    main-is: Test.hs
    build-depends:
        aeson >=0.11.3.0,
        base >=4.8.2.0,
        base64-bytestring >=1.0.0.1,
        basic-prelude >=0.5.2,
        bytestring >=0.10.6.0,
        QuickCheck >=2.8.2,
        sbp,
        tasty >=0.11.2.1,
        tasty-hunit >=0.9.2,
        tasty-quickcheck >=0.8.4
    default-language: Haskell2010
    hs-source-dirs: test
    other-modules:
        Test.SwiftNav.CRC16
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
