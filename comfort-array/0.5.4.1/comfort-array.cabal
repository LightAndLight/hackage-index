Name:             comfort-array
Version:          0.5.4.1
x-revision: 1
License:          BSD3
License-File:     LICENSE
Author:           Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:       Henning Thielemann <haskell@henning-thielemann.de>
Homepage:         https://hub.darcs.net/thielema/comfort-array/
Category:         Data Structures
Synopsis:         Arrays where the index type is a function of the shape type
Description:
  Arrays from the basic @array@ package are already very powerful
  compared with arrays in other languages.
  They may have any number of dimensions, are type safe
  and defined in a uniform way using the Ix class
  with free choice of the lower bounds (0, 1, or whatever you like).
  .
  This package goes one step further:
  The shape and the index type are different,
  but the index type is a type function of the shape type.
  This offers much more flexibility and type safety.
  .
  Some examples are:
  .
  * @Range@:
    Allow dynamic choice of lower and upper array bounds
    such as in the 'Array's from the @array@ package.
    You can combine it with other shapes in other dimensions.
    It allows you to describe the bounds of each dimension individually.
  .
  * @Shifted@:
    Describe array bounds by start index and length.
    It is sometimes more natural to use these parameters.
    E.g. a non-negative index type like 'Word' cannot represent @-1@
    and thus cannot encode an empty range starting with index @0@.
  .
  * @ZeroBased, OneBased@:
    Arrays with fixed lower bound, either 0 or 1, respectively.
  .
  * @Cyclic@:
    Indices with wrap-around semantics.
  .
  * @Zero, ()@:
    Arrays with fixed size 0 or 1, respectively.
  .
  * @Enumeration@:
    Arrays with indices like 'LT', 'EQ', 'GT' and a shape of fixed size.
  .
  * @NestedTuple@:
    Arrays with shapes that are compatible to nested tuples
    like @(a,(a,a))@ and indices like @fst@ and @fst.snd@.
  .
  * @(::+)@:
    The Append type constructor allows to respresent block arrays,
    e.g. block matrices.
    It also allows to represent non-empty arrays via @()::+sh@.
  .
  * @Set@: Use an arbitrary ordered set as index set.
  .
  * @Map@: Concatenate a set of shapes.
  .
  * @Triangular@:
    A 2D array with the shape of a lower or upper triangular matrix.
  .
  * @Simplex@:
    Simplices of any dimension, where the dimension is encoded in the type.
    An index is a tuple of monotonic ordered sub-indices.
  .
  * @Square@: A 2D array where both dimensions always have equal size.
  .
  * @Cube@: A 3D array where all three dimensions always have equal size.
  .
  * @Tagged@: Statically distinguish shapes and indices that are isomorphic.
  .
  With our @Array@ type you can perform
  .
  * Fast Linear Algebra using the packages @comfort-blas@ and @lapack@.
    The @lapack@ package defines even more fancy shapes
    like tall rectangular matrices, triangular matrices and banded matrices.
  .
  * Fast Fourier Transforms using the package @comfort-fftw@
  .
  * Fast Linear Programming using the packages @comfort-glpk@ and @coinor-clp@
  .
  * Efficient Array Processing via LLVM Just-In-Time code generation
    using the package @knead@.
  .
  See also @comfort-graph@ for a Graph data structure,
  with non-Int node identifiers and flexible edge types.

Tested-With:      GHC==7.4.2, GHC==7.8.4
Tested-With:      GHC==8.2.2, GHC==8.6.5, GHC==8.10.4
Tested-With:      GHC==9.4.5, GHC==9.6.2
Cabal-Version:    1.14
Build-Type:       Simple
Extra-Source-Files:
  Changes.md
  test-module.list

Source-Repository this
  Tag:         0.5.4.1
  Type:        darcs
  Location:    https://hub.darcs.net/thielema/comfort-array/

Source-Repository head
  Type:        darcs
  Location:    https://hub.darcs.net/thielema/comfort-array/

Flag setIndex
  Description: Use efficient Set indexing from containers>=0.5.4
  Default:     True

Library
  Build-Depends:
    storablevector >=0.2 && <0.3,
    -- 0.7.1 required for arrayFromList, arrayFromListN
    primitive >=0.7.1 && <0.10,
    guarded-allocation >=0.0.1 && <0.1,
    storable-record >=0.0.1 && <0.1,
    tagged >=0.7 && <0.9,
    deepseq >=1.3 && <1.6,
    QuickCheck >=2 && <3,
    semigroups >=0.18.3 && <1.0,
    containers >=0.4 && <0.8,
    -- transformers-compat required for Functor.Classes in GHC-7.8.4
    transformers-compat >=0.6.6 && <0.8,
    transformers >=0.3 && <0.7,
    non-empty >=0.3.2 && <0.4,
    utility-ht >=0.0.10 && <0.1,
    prelude-compat >=0.0 && <0.1,
    base >=4.5 && <5

  If flag(setIndex)
    Build-Depends: containers >=0.5.4
    Hs-Source-Dirs: set/0.5.4
  Else
    Hs-Source-Dirs: set/0.4.0

  GHC-Options:      -Wall
  Default-Language: Haskell98
  Hs-Source-Dirs:   src
  Exposed-Modules:
    Data.Array.Comfort.Shape
    Data.Array.Comfort.Shape.Test
    Data.Array.Comfort.Storable
    Data.Array.Comfort.Storable.Unchecked
    Data.Array.Comfort.Storable.Unchecked.Monadic
    Data.Array.Comfort.Storable.Private
    Data.Array.Comfort.Storable.Mutable
    Data.Array.Comfort.Storable.Mutable.Unchecked
    Data.Array.Comfort.Storable.Mutable.Private
    Data.Array.Comfort.Boxed
    Data.Array.Comfort.Boxed.Unchecked
    Data.Array.Comfort.Container
  Other-Modules:
    Data.Array.Comfort.Shape.Tuple
    Data.Array.Comfort.Shape.Set
    Data.Array.Comfort.Shape.Utility
    Data.Array.Comfort.Boxed.Strict.Unchecked
    Data.Array.Comfort.Storable.Memory
    Data.Array.Comfort.Check

Test-Suite comfort-array-test
  Type: exitcode-stdio-1.0
  Build-Depends:
    comfort-array,
    doctest-exitcode-stdio >=0.0 && <0.1,
    doctest-lib >=0.1 && <0.1.2,
    ChasingBottoms >=1.2.2 && <1.4,
    tagged,
    containers,
    QuickCheck,
    utility-ht >=0.0.13,
    deepseq,
    base

  GHC-Options:      -Wall
  Default-Language: Haskell98
  Hs-Source-Dirs:   test
  Main-Is:          Main.hs
  Other-Modules:
    DocTest.Data.Array.Comfort.Shape
    DocTest.Data.Array.Comfort.Storable.Unchecked
    DocTest.Data.Array.Comfort.Storable
    DocTest.Data.Array.Comfort.Boxed.Unchecked
    DocTest.Data.Array.Comfort.Boxed
    DocTest.Main
    Test.Shape
    Test.Utility
