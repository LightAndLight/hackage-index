cabal-version: >=1.10
name: pier-core
version: 0.2.0.0
license: BSD3
license-file: LICENSE
maintainer: judah.jacobson@gmail.com
homepage: https://github.com/judah/pier#readme
bug-reports: https://github.com/judah/pier/issues
synopsis: A library for writing forwards-declared build systems in haskell.
description:
    A library for writing build systems in Haskell, built on top of
    <http://shakebuild.com shake>.
    .
    Pier provides a generic approach to building and caching file outputs.
    It enables build actions to be written in a "forwards" style, which
    generally leads to simpler logic than backwards-defined build systems
    such as make or (normal) Shake, where each step of the build logic must
    be written as a new build rule.
    .
    For more details, see "Pier.Core.Artifact".
category: Development
build-type: Simple

source-repository head
    type: git
    location: https://github.com/judah/pier

library
    exposed-modules:
        Pier.Core.Artifact
        Pier.Core.Download
        Pier.Core.Persistent
        Pier.Core.Run
    hs-source-dirs: src
    other-modules:
        Pier.Core.Directory
        Pier.Core.HashableSet
    default-language: Haskell2010
    default-extensions: BangPatterns DeriveGeneric FlexibleContexts
                        LambdaCase MultiWayIf NondecreasingIndentation ScopedTypeVariables
                        StandaloneDeriving TupleSections TypeFamilies TypeSynonymInstances
    build-depends:
        Cabal >=2.2.0.0 && <2.3,
        base >=4.11.0 && <4.12,
        base64-bytestring >=1.0.0.1 && <1.1,
        binary >=0.8.5.1 && <0.9,
        bytestring >=0.10.8.2 && <0.11,
        containers >=0.5.11.0 && <0.6,
        cryptohash-sha256 >=0.11.101.0 && <0.12,
        directory >=1.3.1 && <1.4,
        hashable >=1.2.7.0 && <1.3,
        http-client >=0.5.13.1 && <0.6,
        http-client-tls >=0.3.5.3 && <0.4,
        http-types >=0.12.1 && <0.13,
        process >=1.6.3.0 && <1.7,
        shake >=0.16.4 && <0.17,
        temporary ==1.3.*,
        text >=1.2.3.0 && <1.3,
        unix >=2.7.2.2 && <2.8
