name:     rapid-term
version:  0.1.0
category: Development
synopsis: External terminal support for rapid

maintainer:   Ertugrul Söylemez <esz@posteo.de>
author:       Ertugrul Söylemez <esz@posteo.de>
copyright:    Copyright 2016 Ertugrul Söylemez
homepage:     https://github.com/esoeylemez/rapid-term
bug-reports:  https://github.com/esoeylemez/rapid-term/issues
license:      BSD3
license-file: LICENSE

description:  When developing interactive command line applications in
    an editor like Emacs GHCi typically has no access to an actual
    terminal.  This is good enough for applications that only read lines
    from stdin and print diagnostics to stdout, but as soon as terminal
    functionality is needed, the application has to be tested elsewhere.
    .
    This package provides functionality that, when used together with
    the <https://hackage.haskell.org/package/rapid rapid library>, can
    open a persistent terminal that the application can access directly,
    such that terminal applications can be tested with the main GHCi
    instance.
    .
    You can find a tutorial in the documentation of the @Rapid.Term@
    module.

build-type:         Simple
cabal-version:      >= 1.10
extra-source-files: CHANGELOG.md README.md

source-repository head
    type:     git
    location: https://github.com/esoeylemez/rapid-term.git

flag Devel
    default: False
    description: Development dependencies
    manual: True


library
    build-depends:
        ansi-terminal >= 0.6 && < 1,
        base >= 4.8 && < 5,
        clock >= 0.7 && < 1,
        kan-extensions >= 5.0 && < 6,
        process >= 1.4 && < 2,
        unix >= 2.7 && < 3
    if flag(devel)
        build-depends: rapid
    default-language: Haskell2010
    ghc-options: -W -fdefer-typed-holes
    hs-source-dirs: src
    exposed-modules:
        Rapid.Term
