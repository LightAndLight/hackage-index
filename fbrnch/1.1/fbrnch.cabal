cabal-version:       2.0
name:                fbrnch
version:             1.1
x-revision: 1
synopsis:            Build and create Fedora package repos and branches
description:
            fbrnch (fedora branch) is a convenient packaging tool for
            Fedora Packagers, with integration for Bugzilla, Koji, and Bodhi.
            .
            Features include:
            .
            - merging and building a package across release branches
            .
            - automated parallel builds of sets of packages in dependency order
            .
            - creating, updating and listing one's package reviews
            .
            - requesting repos for new approved packages and branch requests
            .
            - import srpms from package reviews
            .
            - progressive copr builds
            .
            and many more commands.
homepage:            https://github.com/juhp/fbrnch
bug-reports:         https://github.com/juhp/fbrnch/issues
license:             GPL-2
license-file:        LICENSE
author:              Jens Petersen <petersen@redhat.com>
maintainer:          Jens Petersen <petersen@fedoraproject.org>
copyright:           2019-2022 Jens Petersen
category:            Distribution
build-type:          Simple
extra-doc-files:     CHANGELOG.md
                     README.md
tested-with:         GHC == 7.10.3, GHC == 8.0.2, GHC == 8.2.2, GHC == 8.4.4,
                     GHC == 8.6.5,  GHC == 8.8.4, GHC == 8.10.7, GHC == 9.0.2

source-repository head
  type:                git
  location:            https://github.com/juhp/fbrnch.git

executable fbrnch
  main-is:             Main.hs
  autogen-modules:     Paths_fbrnch
  other-modules:       Bodhi
                       Branches
                       Bugzilla
                       Cmd.Bugs
                       Cmd.Build
                       Cmd.Bump
                       Cmd.Clone
                       Cmd.Commit
                       Cmd.Copr
                       Cmd.Diff
                       Cmd.FTBFS
                       Cmd.Import
                       Cmd.Install
                       Cmd.ListBranches
                       Cmd.ListPackages
                       Cmd.Local
                       Cmd.Log
                       Cmd.Merge
                       Cmd.Mock
                       Cmd.Override
                       Cmd.Parallel
                       Cmd.PkgReview
                       Cmd.Prep
                       Cmd.PullPush
                       Cmd.Repoquery
                       Cmd.RequestBranch
                       Cmd.RequestRepo
                       Cmd.Reviews
                       Cmd.Scratch
                       Cmd.SideTags
                       Cmd.Sort
                       Cmd.Status
                       Cmd.Switch
                       Cmd.Update
                       Cmd.WaitRepo
                       Common
                       Common.System
                       Common.Text
                       Git
                       InterleaveOutput
                       Koji
                       Krb
                       ListReviews
                       Package
                       Pagure
                       Paths_fbrnch
                       Prompt
                       Types

  hs-source-dirs:      src
  default-language:    Haskell2010

  build-depends:       aeson,
                       async,
                       -- pretty-terminal needs 4.9:
                       base >= 4.9 && < 5,
                       bodhi,
                       bugzilla-redhat >= 1.0,
                       bytestring,
                       config-ini,
                       copr-api,
                       directory >= 1.2.3,
                       email-validate,
                       extra,
                       fedora-dists ^>= 2.0,
                       filepath,
                       -- haskeline,
                       http-conduit,
                       http-directory >= 0.1.5,
                       http-query,
                       koji,
                       network-uri,
                       -- (for simple_cmd_args < 0.1.7):
                       optparse-applicative,
                       pagure >= 0.1,
                       pretty-terminal,
                       process,
                       -- regex-tdfa,
                       rpmbuild-order >= 0.4.5,
                       rpm-nvr >= 0.1.1,
                       simple-cmd >= 0.2.0,
                       simple-cmd-args >= 0.1.6,
                       text,
                       time,
                       typed-process >= 0.2.4.0,
                       utf8-string,
                       unix,
                       xdg-basedir

  if impl(ghc<8.3)
       build-depends: semigroups
  if impl(ghc<8.4)
       build-depends: http-common < 0.8.3.4

  ghc-options:         -threaded
                       -Wall
                       -Wcompat
                       -Widentities
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options:       -Wredundant-constraints
  if impl(ghc >= 8.2)
    ghc-options:       -fhide-source-paths
  if impl(ghc >= 8.4)
    ghc-options:       -Wmissing-export-lists
                       -Wpartial-fields
  if impl(ghc >= 8.10)
    ghc-options:       -Wunused-packages
