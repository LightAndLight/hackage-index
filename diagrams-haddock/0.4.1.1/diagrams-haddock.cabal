name:                diagrams-haddock
version:             0.4.1.1
x-revision: 1
synopsis:            Preprocessor for embedding diagrams in Haddock documentation
description:         diagrams-haddock is a tool for compiling embedded inline
                     diagrams code in Haddock documentation, for an
                     easy way to spice up your documentation with
                     diagrams.  Just create some diagrams code using
                     special markup, run diagrams-haddock, and ensure
                     the resulting image files are installed along
                     with your documentation.  For complete
                     documentation and examples, see
                     <https://github.com/diagrams/diagrams-haddock/blob/master/README.md>.
                     .
                     For a good example of a package making use of
                     diagrams-haddock, see the diagrams-contrib
                     package
                     (<http://hackage.haskell.org/package/diagrams%2Dcontrib>).
homepage:            https://diagrams.github.io/
license:             BSD3
license-file:        LICENSE
author:              Brent Yorgey
maintainer:          diagrams-discuss@googlegroups.com
bug-reports:         https://github.com/diagrams/diagrams-haddock/issues
category:            Graphics
build-type:          Simple
cabal-version:       1.18
extra-source-files:  README.md, CHANGES.md, diagrams/*.svg
extra-doc-files:     diagrams/*.svg
tested-with:         GHC ==8.4.4 || ==8.6.5 || ==8.8.4 || ==8.10.4 || ==9.0.1 || ==9.2.1

Source-repository head
  type: git
  location: git://github.com/diagrams/diagrams-haddock.git

library
  exposed-modules:     Diagrams.Haddock
  other-modules:       Paths_diagrams_haddock
  build-depends:       base >= 4.4 && < 4.17,
                       filepath,
                       directory,
                       mtl >= 2.0 && < 2.3,
                       containers >= 0.4 && < 0.7,
                       split >= 0.2 && < 0.3,
                       bytestring >= 0.9 && < 0.12,
                       strict >= 0.3 && < 0.5,
                       parsec >= 3,
                       haskell-src-exts >= 1.18 && < 1.24,
                       svg-builder >= 0.1 && < 0.2,
                       diagrams-builder >= 0.5 && < 0.9,
                       diagrams-lib >= 1.4 && < 1.5,
                       diagrams-svg >= 1.4 && < 1.5,
                       lens >= 3.8 && < 5.2,
                       linear >= 1.10 && < 1.22,
                       cpphs >= 1.15,
                       unliftio >= 0.2.12 && < 0.3,
                       uniplate >= 1.6 && < 1.7,
                       text >= 0.11 && < 1.3,
                       base64-bytestring >= 1 && < 1.3,
                       ansi-terminal >= 0.5 && < 0.12
  hs-source-dirs:      src
  other-extensions:    TemplateHaskell
  default-language:    Haskell2010

Executable diagrams-haddock
  main-is:             diagrams-haddock.hs
  other-modules:       Paths_diagrams_haddock
  build-depends:       base,
                       directory,
                       filepath,
                       diagrams-haddock,
                       cmdargs >= 0.8 && < 0.11,
                       Cabal >= 2.2 && < 3.7,
                       cpphs >= 1.15
  hs-source-dirs:      tools
  default-language:    Haskell2010

Test-suite diagrams-haddock-tests
  type:                exitcode-stdio-1.0
  main-is:             Tests.hs
  build-depends:       base,
                       containers >= 0.4 && < 0.7,
                       QuickCheck >= 2.4 && < 2.15,
                       tasty >= 0.8 && < 1.5,
                       tasty-quickcheck >= 0.8 && < 0.11,
                       parsec >= 3,
                       lens >= 3.8 && < 5.2,
                       haskell-src-exts >= 1.18 && < 1.24,
                       diagrams-haddock
  hs-source-dirs:      test
  default-language:    Haskell2010
