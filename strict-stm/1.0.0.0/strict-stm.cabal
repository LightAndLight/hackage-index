cabal-version:       3.0
name:                strict-stm
version:             1.0.0.0
synopsis:            Strict STM interface polymorphic over stm implementation.
description:
  Strict STM interface provided on top of
  [io-classes](https://hackage.haskell.org/package/io-classes) and thus
  compatible with [stm](https://hackage.haskell.org/package/stm)
  & [io-sim](https://hackage.haskell.org/package/io-sim).
license:             Apache-2.0
license-files:
  LICENSE
  NOTICE
copyright:           2019-2023 Input Output Global Inc (IOG)
author:              Alexander Vieth, Duncan Coutts, Marcin Szamotulski, Thomas Winant
maintainer:          Duncan Coutts dunca@well-typed.com, Marcin Szamotulski coot@coot.me
category:            Control
build-type:          Simple
extra-source-files:  CHANGELOG.md
                     README.md
tested-with:         GHC == { 8.10, 9.2, 9.4 }

source-repository head
  type:     git
  location: https://github.com/input-output-hk/io-sim
  subdir:   strict-stm

flag checktvarinvariant
  Description: Enable runtime invariant checks on StrictT(M)Var
  Manual: True
  Default: False

flag asserts
  description: Enable assertions
  manual:      False
  default:     False

library
  hs-source-dirs:      src

  exposed-modules:     Control.Concurrent.Class.MonadSTM.Strict
                       Control.Concurrent.Class.MonadSTM.Strict.TArray
                       Control.Concurrent.Class.MonadSTM.Strict.TBQueue
                       Control.Concurrent.Class.MonadSTM.Strict.TChan
                       Control.Concurrent.Class.MonadSTM.Strict.TMVar
                       Control.Concurrent.Class.MonadSTM.Strict.TQueue
                       Control.Concurrent.Class.MonadSTM.Strict.TVar
  reexported-modules:  Control.Concurrent.Class.MonadSTM.TSem as Control.Concurrent.Class.MonadSTM.Strict.TSem
  default-language:    Haskell2010
  default-extensions:  BangPatterns,
                       CPP,
                       ExplicitNamespaces,
                       FlexibleContexts,
                       FlexibleInstances,
                       GADTs,
                       NamedFieldPuns,
                       TypeOperators
  build-depends:       base        >= 4.9 && <4.18,
                       array,
                       stm         >= 2.5 && <2.6,

                       io-classes  ^>= 1.0
  ghc-options:         -Wall
                       -Wno-unticked-promoted-constructors
                       -Wcompat
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
                       -Wpartial-fields
                       -Widentities

  if flag(asserts)
    ghc-options: -fno-ignore-asserts

  if flag(checktvarinvariant)
    cpp-options: -DCHECK_TVAR_INVARIAN
