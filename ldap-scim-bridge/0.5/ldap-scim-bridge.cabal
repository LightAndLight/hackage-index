cabal-version:       2.4
name:                ldap-scim-bridge
version:             0.5
synopsis:            See README for synopsis
description:         See README for description
homepage:            https://github.com/fisx/ldap-scim-bridge
bug-reports:         https://github.com/fisx/ldap-scim-bridge/issues
license:             AGPL-3.0-or-later
license-file:        LICENSE
author:              Matthias Fischmann
maintainer:          Matthias Fischmann <mf@zerobuzz.net>
copyright:           (c) 2021 wire.com
category:            System
build-type:          Simple
extra-doc-files:     README.md
                     CHANGELOG.md
extra-source-files:  examples/wire-server/run.sh
                     examples/wire-server/conf1.yaml
                     examples/wire-server/conf2.yaml
                     examples/wire-server/runlog
tested-with:         GHC == 8.8.3

source-repository head
  type:                git
  location:            https://github.com/fisx/ldap-scim-bridge.git

common common-options
  build-depends:       base >=4.13 && <4.15
                     , relude
                     , hscim >=0.3.6 && <0.4
                     , ldap-client >=0.4.1 && <0.5
                     , network >=3.1.1.1 && <3.2
                     , text >=1.2.4.1 && <1.3
                     , yaml >=0.11.5.0 && <0.12
                     , aeson >=1.4.7.1 && <1.5
                     , aeson-pretty >=0.8.8 && <0.9
                     , containers >=0.6.2.1 && <0.7
                     , bytestring >=0.10.12 && <0.11
                     , email-validate >=2.3.2.13 && <2.4
                     , string-conversions >=0.4.0.1 && <0.5
                     , servant-client >=0.18.3 && <0.19
                     , unordered-containers >= 0.2.14.0 && <0.3
                     , servant-client-core >=0.18.3 && <0.19
                     , servant >=0.18.3 && <0.19
                     , http-types >=0.12.3 && <0.13
                     , string-conversions >=0.4.0.1 && <0.5
                     , http-client >=0.7.8 && <0.8
                     , http-client-tls >=0.3.5 && <0.4
                     , tinylog >=0.15 && <0.16

  mixins:              base hiding (Prelude)
                     , relude (Relude as Prelude)

  ghc-options:         -Wall
                       -Wcompat
                       -Widentities
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options:       -Wredundant-constraints
  if impl(ghc >= 8.2)
    ghc-options:       -fhide-source-paths
  if impl(ghc >= 8.4)
    ghc-options:       -Wmissing-export-lists
                       -Wpartial-fields
  if impl(ghc >= 8.8)
    ghc-options:       -Wmissing-deriving-strategies

  default-language:    Haskell2010
  default-extensions:  AllowAmbiguousTypes
                       BangPatterns
                       ConstraintKinds
                       DataKinds
                       DefaultSignatures
                       DeriveFunctor
                       DeriveGeneric
                       DeriveLift
                       DeriveTraversable
                       DerivingStrategies
                       EmptyCase
                       FlexibleContexts
                       FlexibleInstances
                       FunctionalDependencies
                       GADTs
                       InstanceSigs
                       KindSignatures
                       LambdaCase
                       MultiParamTypeClasses
                       MultiWayIf
                       NamedFieldPuns
                       OverloadedStrings
                       PackageImports
                       PatternSynonyms
                       PolyKinds
                       QuasiQuotes
                       RankNTypes
                       RecordWildCards
                       ScopedTypeVariables
                       StandaloneDeriving
                       TemplateHaskell
                       TupleSections
                       TypeApplications
                       TypeFamilies
                       TypeFamilyDependencies
                       TypeOperators
                       UndecidableInstances
                       ViewPatterns

library
  import:              common-options
  hs-source-dirs:      src
  exposed-modules:     LdapScimBridge

executable ldap-scim-bridge
  import:              common-options
  hs-source-dirs:      app
  main-is:             Main.hs
  build-depends:       ldap-scim-bridge
  ghc-options:         -threaded
                       -rtsopts
                       -with-rtsopts=-N
