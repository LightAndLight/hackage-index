cabal-version:      1.18
name:               bz2
version:            1.0.0.1
x-revision: 2
license:            BSD3
license-file:       LICENSE
copyright:          Copyright: (c) 2020 Vanessa McHale
maintainer:         vamchale@gmail.com
author:             Vanessa McHale
bug-reports:        https://hub.darcs.net/vmchale/bz2/issues
synopsis:           Bindings to libbz2
description:        High-level bindings to libbz2 using c2hs and ByteString
category:           Compression
build-type:         Simple
data-files:
    test/data/sample1.ref
    test/data/sample2.ref
    test/data/sample3.ref
    test/data/sample1.bz2
    test/data/sample2.bz2
    test/data/sample3.bz2

extra-source-files:
    cbits/bzlib_private.h
    Makefile

extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     darcs
    location: https://hub.darcs.net/vmchale/bz2

flag cross
    description: Set this flag if cross-compiling
    default:     False
    manual:      True

library
    exposed-modules:  Codec.Compression.BZip
    cc-options:       -O3
    c-sources:
        cbits/bzlib.c
        cbits/randtable.c
        cbits/crctable.c
        cbits/compress.c
        cbits/blocksort.c
        cbits/huffman.c
        cbits/decompress.c

    hs-source-dirs:   src
    other-modules:
        Codec.Compression.BZip.Foreign.Common
        Codec.Compression.BZip.Foreign.Compress
        Codec.Compression.BZip.Foreign.Decompress
        Codec.Compression.BZip.Common
        Codec.Compression.BZip.Unpack
        Codec.Compression.BZip.Pack

    default-language: Haskell2010
    other-extensions: DeriveDataTypeable TupleSections
    include-dirs:     cbits
    install-includes: cbits/bzlib.h
    ghc-options:      -Wall
    build-depends:
        base >=4.7 && <5,
        bytestring -any

    if !flag(cross)
        build-tool-depends: c2hs:c2hs >=0.26.1

    if impl(ghc >=8.0)
        ghc-options:
            -Wincomplete-uni-patterns -Wincomplete-record-updates
            -Wredundant-constraints

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

test-suite bz2-test
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   test
    default-language: Haskell2010
    ghc-options:      -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base -any,
        bz2 -any,
        bytestring -any,
        filepath >=1.4.0.0,
        tasty -any,
        tasty-golden -any,
        tasty-hunit -any,
        deepseq -any,
        directory -any

    if impl(ghc >=8.0)
        ghc-options:
            -Wincomplete-uni-patterns -Wincomplete-record-updates
            -Wredundant-constraints -Widentities

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.2)
        ghc-options: -Wcpp-undef

benchmark bz2-bench
    type:             exitcode-stdio-1.0
    main-is:          Bench.hs
    hs-source-dirs:   bench
    default-language: Haskell2010
    ghc-options:      -threaded -rtsopts -with-rtsopts=-N -O3 -Wall
    build-depends:
        base -any,
        bz2 -any,
        criterion -any,
        filepath -any,
        temporary -any,
        bytestring -any

    if impl(ghc >=8.0)
        ghc-options:
            -Wincomplete-uni-patterns -Wincomplete-record-updates
            -Wredundant-constraints -Widentities

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.2)
        ghc-options: -Wcpp-undef

benchmark bz2-mem
    type:             exitcode-stdio-1.0
    main-is:          Mem.hs
    hs-source-dirs:   mem
    default-language: Haskell2010
    ghc-options:      -Wall
    build-depends:
        base -any,
        bz2 -any,
        filepath -any,
        temporary -any,
        bytestring -any

    if impl(ghc >=8.0)
        ghc-options: -Wincomplete-uni-patterns -Wincomplete-record-updates
