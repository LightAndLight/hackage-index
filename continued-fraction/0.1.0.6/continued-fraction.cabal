name:                continued-fraction
version:             0.1.0.6
synopsis:            Types and functions for working with continued fractions in Haskell
description:         This package provides facilities for working with both continued fractions
                     and rational approximants. It uses lists internally, so it may not be suitable
                     if you need large convergents.
homepage:            https://hub.darcs.net/vmchale/continued-fraction#readme
license:             BSD3
license-file:        LICENSE
author:              Vanessa McHale
maintainer:          vamchale@gmail.com
copyright:           Copyright: (c) 2018 Vanessa McHale
category:            Math
build-type:          Simple
extra-doc-files:     README.md
extra-source-files:  stack.yaml
                   , cabal.project.local
cabal-version:       >=1.18

Flag development {
  Description: Enable `-Werror`
  manual: True
  default: False
}

library
  hs-source-dirs:      src
  exposed-modules:     Num.ContinuedFraction
  build-depends:       base >= 4.9 && < 5
                     , recursion-schemes >= 5.0
  default-language:    Haskell2010
  if flag(development)
    ghc-options: -Werror
  ghc-options:         -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat

test-suite continued-fractions-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Spec.hs
  build-depends:       base
                     , continued-fraction
                     , hspec
  if flag(development)
    ghc-options: -Werror
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat
  default-language:    Haskell2010

benchmark continued-fractions-bench
  type:                exitcode-stdio-1.0
  hs-source-dirs:      bench
  main-is:             Bench.hs
  build-depends:       base
                     , continued-fraction
                     , criterion
  if flag(development)
    ghc-options: -Werror
  ghc-options:         -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat
  -- -O2 
  default-language:    Haskell2010

source-repository head
  type:     darcs
  location: https://hub.darcs.net/vmchale/continued-fraction
