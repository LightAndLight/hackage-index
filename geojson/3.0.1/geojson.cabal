name:                   geojson
version:                3.0.1
x-revision: 2
license:                BSD3
license-file:           LICENCE
author:                 Dom De Re
maintainer:             Andrew Newman
copyright:              Copyright (C) 2013-2018
synopsis:               A thin GeoJSON Layer above the aeson library
category:               Data
description:            A thin GeoJSON Layer above the aeson library.
                        .
                        It currently conforms to version 1.0 of the GeoJSON spec which can be found here:
                        .
                        <http://geojson.org/geojson-spec.html>

homepage:               https://github.com/indicatrix/hs-geojson
bug-reports:            https://github.com/indicatrix/hs-geojson/issues
cabal-version:          >= 1.10
build-type:             Simple
extra-source-files:     README.md,
                        CHANGELOG.md

source-repository       head
    type:               git
    location:           https://github.com/indicatrix/hs-geojson.git

source-repository       this
    type:               git
    location:           https://github.com/indicatrix/hs-geojson.git
    tag:                3.0.1

library
    build-depends: aeson <1.6

    hs-source-dirs:     src
    build-depends:      base < 5 &&     >= 4.9
                    ,   aeson           >= 0.8
                    ,   containers      >= 0.5.10.1
                    ,   deepseq         >= 1.4
                    ,   lens            >= 4.11
                    ,   semigroups      >= 0.16
                    ,   text            >= 1.2
                    ,   scientific      >= 0.2.0
                    ,   transformers    >= 0.3
                    ,   validation      >= 1
                    ,   vector          >= 0.10
    exposed-modules:    Data.Geospatial
                    ,   Data.LinearRing
                    ,   Data.LineString
                    ,   Data.SeqHelper
                    ,   Data.Geospatial.Internal.BasicTypes
                    ,   Data.Geospatial.Internal.CRS
                    ,   Data.Geospatial.Internal.GeoFeature
                    ,   Data.Geospatial.Internal.GeoFeatureCollection
                    ,   Data.Geospatial.Internal.Geometry
                    ,   Data.Geospatial.Internal.Geometry.GeoMultiLine
                    ,   Data.Geospatial.Internal.Geometry.GeoMultiPoint
                    ,   Data.Geospatial.Internal.Geometry.GeoMultiPolygon
                    ,   Data.Geospatial.Internal.Geometry.GeoLine
                    ,   Data.Geospatial.Internal.Geometry.GeoPoint
                    ,   Data.Geospatial.Internal.Geometry.GeoPolygon
                    ,   Data.Geospatial.Internal.Geometry.Aeson
                    ,   Data.Geospatial.Internal.GeoPosition
    default-language:   Haskell2010
    ghc-options:        -Wall

test-suite              geojson-test
    hs-source-dirs:     test
    main-is:            Main.hs
    type:               exitcode-stdio-1.0
    build-depends:      base < 5 &&     >= 4.9
                    ,   aeson           >= 0.8
                    ,   bytestring      >= 0.10
                    ,   containers      >= 0.5.10.1
                    ,   geojson
                    ,   tasty
                    ,   tasty-hspec
                    ,   tasty-quickcheck
                    ,   text            >= 1.2
                    ,   validation      >= 1
                    ,   vector          >= 0.10
    other-modules:      Arbitrary
                    ,   Fixture
                    ,   Data.LinearRingTests
                    ,   Data.LineStringTests
                    ,   Data.Geospatial.Internal.CRSTests
                    ,   Data.Geospatial.Internal.GeoFeatureCollectionTests
                    ,   Data.Geospatial.Internal.GeoFeatureTests
                    ,   Data.Geospatial.Internal.GeometryTests
    default-language:   Haskell2010
    ghc-options:        -Wall
                        -threaded

test-suite              geojson-hlint
    hs-source-dirs:     hlint
    main-is:            Main.hs
    type:               exitcode-stdio-1.0
    build-depends:      base
                    ,   hlint
    default-language:   Haskell2010
    ghc-options:        -Wall
                        -threaded
