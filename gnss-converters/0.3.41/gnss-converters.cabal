cabal-version: >=1.22
name: gnss-converters
version: 0.3.41
license: BSD3
copyright: Copyright (C) 2016, 2017 Swift Navigation, Inc.
maintainer: Swift Navigation <dev@swiftnav.com>
author: Mark Fine <mark@swiftnav.com>
homepage: http://github.com/swift-nav/gnss-converters
synopsis: GNSS Converters.
description:
    Haskell bindings for GNSS converters.
category: Network
build-type: Simple

source-repository head
    type: git
    location: https://github.com/swift-nav/gnss-converters

library
    exposed-modules:
        Data.RTCM3.Replay
        Data.RTCM3.SBP
        Data.RTCM3.SBP.Biases
        Data.RTCM3.SBP.Ephemerides
        Data.RTCM3.SBP.Logging
        Data.RTCM3.SBP.MSM
        Data.RTCM3.SBP.Observations
        Data.RTCM3.SBP.Positions
        Data.RTCM3.SBP.SSR
        Data.RTCM3.SBP.Time
        Data.RTCM3.SBP.Types
        SwiftNav.SBP.RTCM3
    hs-source-dirs: src
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        base >=4.8 && <5,
        basic-prelude >=0.7.0,
        conduit >=1.2.13.1,
        exceptions >=0.8.3,
        extra >=1.6.4,
        lens >=4.15.4,
        monad-control >=1.0.2.3,
        mtl >=2.2.2,
        resourcet >=1.1.11,
        rtcm >=0.2.17,
        sbp >=2.3.16,
        time >=1.8.0.2,
        transformers-base >=0.4.4,
        vector >=0.12.0.1

executable sbp2rtcm3
    main-is: SBP2RTCM3.hs
    hs-source-dirs: main
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        binary-conduit >=1.2.5,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        gnss-converters -any

executable rtcm32sbp
    main-is: RTCM32SBP.hs
    hs-source-dirs: main
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        binary-conduit >=1.2.5,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        gnss-converters -any

executable rtcm32rtcm3
    main-is: RTCM32RTCM3.hs
    hs-source-dirs: main
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        binary-conduit >=1.2.5,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        gnss-converters -any

test-suite test
    type: exitcode-stdio-1.0
    main-is: Test.hs
    hs-source-dirs: test
    other-modules:
        Test.Data.RTCM3.SBP
        Test.Data.RTCM3.SBP.Time
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
    build-depends:
        aeson >=1.2.4.0,
        aeson-pretty >=0.8.5,
        base >=4.10.1.0,
        basic-prelude >=0.7.0,
        binary-conduit >=1.2.5,
        bytestring >=0.10.8.2,
        conduit >=1.2.13.1,
        conduit-extra >=1.2.3.2,
        gnss-converters -any,
        lens >=4.15.4,
        sbp >=2.3.16,
        tasty >=0.11.3,
        tasty-golden >=2.3.1.2,
        tasty-hunit >=0.9.2,
        time >=1.8.0.2
