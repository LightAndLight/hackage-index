cabal-version:      1.18
name:               cfn-flip
version:            0.1.0.2
license:            MIT
license-file:       LICENSE
copyright:          2021 Renaissance Learning Inc
maintainer:         freckle-engineering@renaissance.com
author:             Freckle R&D
homepage:           https://github.com/freckle/cfn-flip#readme
bug-reports:        https://github.com/freckle/cfn-flip/issues
synopsis:           Haskell implementation of aws/cfn-flip
description:
    Functions to flip between CloudFormation Yaml and JSON syntaxes

category:           Tools
build-type:         Simple
extra-source-files:
    test/examples/clean.json
    test/examples/get-att.json
    test/examples/import_value.json
    test/examples/test.json
    test/examples/test_json_data.json
    test/examples/test_json_data_long_line.json
    test/examples/test_json_def_string_with_sub.json
    test/examples/test_json_state_machine.json
    test/examples/test_long.json
    test/examples/test_lorem.json
    test/examples/test_multibyte.json
    test/examples/test_multiline.json
    test/examples/test_user_data.json
    test/examples/test_yaml_def_string_with_sub.json
    test/examples/test_yaml_long_line.json
    test/examples/test_yaml_state_machine.json
    test/examples/clean.yaml
    test/examples/get-att.yaml
    test/examples/import_value.yaml
    test/examples/test.yaml
    test/examples/test_json_data.yaml
    test/examples/test_lorem.yaml
    test/examples/test_multibyte.yaml
    test/examples/test_multiline.yaml
    test/examples/test_user_data.yaml
    test/examples/test_yaml_def_string_with_sub.yaml
    test/examples/test_yaml_long_line.yaml
    test/examples/test_yaml_state_machine.yaml

extra-doc-files:
    CHANGELOG.md
    README.md

source-repository head
    type:     git
    location: https://github.com/freckle/cfn-flip

library
    exposed-modules:
        CfnFlip
        CfnFlip.Aeson
        CfnFlip.Conduit
        CfnFlip.IntrinsicFunction
        CfnFlip.JsonToYaml
        CfnFlip.Libyaml
        CfnFlip.Prelude
        CfnFlip.Yaml
        CfnFlip.YamlToJson

    hs-source-dirs:     src
    other-modules:      Paths_cfn_flip
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missing-import-lists -Wno-unsafe

    build-depends:
        aeson >=1.4.6.0,
        base >=4.11 && <10,
        bytestring >=0.10.8.2,
        conduit >=1.3.1.2,
        libyaml >=0.1.2,
        text >=1.2.3.1,
        unliftio >=0.2.12,
        unliftio-core >=0.1.2.0,
        yaml >=0.11.2.0

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        default-extensions: DerivingVia
        ghc-options:
            -fwrite-ide-info -Wno-missing-safe-haskell-mode
            -Wno-prepositive-qualified-module

test-suite doctest
    type:               exitcode-stdio-1.0
    main-is:            DocTest.hs
    other-modules:      Paths_cfn_flip
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missing-import-lists -Wno-unsafe

    build-depends:
        aeson >=1.4.6.0,
        base >=4.11 && <10,
        doctest >=0.16.2,
        yaml >=0.11.2.0

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        default-extensions: DerivingVia
        ghc-options:
            -fwrite-ide-info -Wno-missing-safe-haskell-mode
            -Wno-prepositive-qualified-module

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    hs-source-dirs:     test
    other-modules:
        CfnFlipSpec
        Paths_cfn_flip

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -Weverything -Wno-missing-exported-signatures
        -Wno-missing-import-lists -Wno-unsafe

    build-depends:
        Glob >=0.10.0,
        base >=4.11 && <10,
        cfn-flip -any,
        filepath >=1.4.2.1,
        hspec >=2.8.1,
        libyaml >=0.1.2

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        default-extensions: DerivingVia
        ghc-options:
            -fwrite-ide-info -Wno-missing-safe-haskell-mode
            -Wno-prepositive-qualified-module
