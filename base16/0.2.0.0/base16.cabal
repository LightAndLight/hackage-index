cabal-version:      2.0
name:               base16
version:            0.2.0.0
synopsis:           RFC 4648-compliant Base16 encodings/decodings
description:
  RFC 4648-compliant Base16 encodings and decodings.
  This library provides performant encoding and decoding primitives, as well as support for textual values.

homepage:           https://github.com/emilypi/base16
bug-reports:        https://github.com/emilypi/base16/issues
license:            BSD3
license-file:       LICENSE
author:             Emily Pillmore
maintainer:         emilypi@cohomolo.gy
copyright:          (c) 2020 Emily Pillmore
category:           Data
build-type:         Simple
extra-source-files:
  CHANGELOG.md
  README.md

tested-with:
  GHC ==8.2.2 || ==8.4.3 || ==8.4.4 || ==8.6.3 || ==8.6.5 || ==8.8.1

source-repository head
  type:     git
  location: https://github.com/emilypi/base16.git

library
  exposed-modules:
    Data.ByteString.Base16
    Data.ByteString.Lazy.Base16
    Data.Text.Encoding.Base16
    Data.Text.Encoding.Base16.Error
    Data.Text.Lazy.Encoding.Base16

  other-modules:
    Data.ByteString.Base16.Internal.Head
    Data.ByteString.Base16.Internal.Tables
    Data.ByteString.Base16.Internal.Utils
    Data.ByteString.Base16.Internal.W16.Loop
    Data.ByteString.Base16.Internal.W32.Loop
    Data.ByteString.Base16.Internal.W64.Loop

  build-depends:
      base        >=4.10 && <5
    , bytestring  ^>=0.10
    , text        ^>=1.2

  hs-source-dirs:   src
  default-language: Haskell2010
  ghc-options:      -Wall

test-suite tasty
  default-language: Haskell2010
  type:             exitcode-stdio-1.0
  hs-source-dirs:   test
  main-is:          Base16Tests.hs
  build-depends:
      base               >=4.10 && <5
    , base16
    , base16-bytestring
    , bytestring
    , memory
    , random-bytestring
    , tasty
    , tasty-hunit
    , text

benchmark bench
  default-language: Haskell2010
  type:             exitcode-stdio-1.0
  hs-source-dirs:   benchmarks
  main-is:          Base16Bench.hs
  build-depends:
      base               >=4.10 && <5
    , base16
    , base16-bytestring
    , bytestring
    , criterion
    , deepseq
    , memory
    , random-bytestring
    , text
