cabal-version:              2.4
name:                       Z-Data
version:                    0.1.4.0
synopsis:                   Array, vector and text
description:                This package provides array, slice and text operations
license:                    BSD-3-Clause
license-file:               LICENSE
author:                     Dong Han, Tao He
maintainer:                 winterland1989@gmail.com
copyright:                  (c) Dong Han, 2017-2020
                            (c) Tao He, 2017-2020
category:                   Data
build-type:                 Simple
homepage:                   https://github.com/haskell-Z/z-data
bug-reports:                https://github.com/haskell-Z/z-data/issues

extra-source-files:         ChangeLog.md
                            README.md
                            LICENSE

                            include/bytes.h
                            include/dtoa.h
                            include/text.h
                            cbits/bytes.c
                            cbits/dtoa.c
                            cbits/text.c

                            -- utf8rewind C sources
                            third_party/utf8rewind/include/utf8rewind/utf8rewind.h
                            third_party/utf8rewind/source/unicodedatabase.c
                            third_party/utf8rewind/source/unicodedatabase.h
                            third_party/utf8rewind/source/internal/base.h
                            third_party/utf8rewind/source/internal/casemapping.c
                            third_party/utf8rewind/source/internal/casemapping.h
                            third_party/utf8rewind/source/internal/codepoint.c
                            third_party/utf8rewind/source/internal/codepoint.h
                            third_party/utf8rewind/source/internal/composition.c
                            third_party/utf8rewind/source/internal/composition.h
                            third_party/utf8rewind/source/internal/database.c
                            third_party/utf8rewind/source/internal/database.h
                            third_party/utf8rewind/source/internal/decomposition.c
                            third_party/utf8rewind/source/internal/decomposition.h
                            third_party/utf8rewind/source/internal/seeking.c
                            third_party/utf8rewind/source/internal/seeking.h
                            third_party/utf8rewind/source/internal/streaming.c
                            third_party/utf8rewind/source/internal/streaming.h
                            third_party/utf8rewind/source/utf8rewind.c

                            -- simd utf8 code
                            third_party/fastvalidate-utf-8/include/simdasciicheck.h
                            third_party/fastvalidate-utf-8/include/simdutf8check.h

source-repository head
    type:     git
    location: git://github.com/haskell-Z/z-data.git

flag integer-simple
    description:
        Use the [simple integer library](http://hackage.haskell.org/package/integer-simple)
        instead of [integer-gmp](http://hackage.haskell.org/package/integer-gmp)
    default: False
    manual: False

library
    exposed-modules:        Z.Data.Array
                            Z.Data.Array.Cast
                            Z.Data.Array.Checked
                            Z.Data.Array.QQ
                            Z.Data.Array.UnalignedAccess
                            Z.Data.Array.UnliftedArray
                            Z.Data.CBytes
                            Z.Data.Vector
                            Z.Data.Vector.Base
                            Z.Data.Vector.Extra
                            Z.Data.Vector.Search
                            Z.Data.Vector.Sort
                            Z.Data.Vector.QQ
                            Z.Data.Vector.FlatIntMap
                            Z.Data.Vector.FlatIntSet
                            Z.Data.Vector.FlatMap
                            Z.Data.Vector.FlatSet

                            Z.Data.Text
                            Z.Data.Text.Base
                            Z.Data.Text.Extra
                            Z.Data.Text.Search
                            Z.Data.Text.UTF8Codec
                            Z.Data.Text.UTF8Rewind
                            Z.Data.Text.Builder

                            Z.Data.Builder
                            Z.Data.Builder.Base
                            Z.Data.Builder.Numeric
                            Z.Data.Builder.Numeric.DigitTable

                            Z.Data.Generics.Utils

                            Z.Data.Parser
                            Z.Data.Parser.Base
                            Z.Data.Parser.Numeric

                            Z.Data.PrimRef
                            Z.Data.PrimRef.PrimSTRef
                            Z.Data.PrimRef.PrimIORef

                            Z.Data.JSON
                            Z.Data.JSON.Base
                            Z.Data.JSON.Builder
                            Z.Data.JSON.Value

                            Z.Foreign

    build-depends:          base                    >= 4.12 && <5.0
                          , ghc-prim                >= 0.5.3 && < 0.6.2
                          , primitive               >= 0.7.1 && < 0.7.2
                          , scientific              == 0.3.*
                          , hashable                == 1.3.*
                          , case-insensitive        == 1.2.*
                          , deepseq                 >= 1.4 && < 1.5
                          , QuickCheck              >= 2.10
                          , template-haskell        >= 2.14.0
                          , unordered-containers    == 0.2.*
                          , tagged                  == 0.8.*

    if flag(integer-simple)
        cpp-options:        -DINTEGER_SIMPLE
        build-depends:      integer-simple >= 0.1 && < 0.5
    else
        cpp-options:        -DINTEGER_GMP
        build-depends:      integer-gmp >= 0.2 && < 1.1


    include-dirs:           include
                            third_party/utf8rewind/include/utf8rewind
                            third_party/utf8rewind/source/internal
                            third_party/utf8rewind/source
                            third_party/fastvalidate-utf-8/include
    includes:               bytes.h
                            dtoa.h
                            text.h
                            utf8rewind.h
                            simdasciicheck.h
                            simdutf8check.h
    install-includes:       bytes.h
                            dtoa.h
                            text.h
                            utf8rewind.h
                            simdasciicheck.h
                            simdutf8check.h
    c-sources:              cbits/bytes.c
                            cbits/dtoa.c
                            cbits/text.c
                            third_party/utf8rewind/source/unicodedatabase.c
                            third_party/utf8rewind/source/internal/casemapping.c
                            third_party/utf8rewind/source/internal/codepoint.c
                            third_party/utf8rewind/source/internal/composition.c
                            third_party/utf8rewind/source/internal/database.c
                            third_party/utf8rewind/source/internal/decomposition.c
                            third_party/utf8rewind/source/internal/seeking.c
                            third_party/utf8rewind/source/internal/streaming.c
                            third_party/utf8rewind/source/utf8rewind.c

    default-language:       Haskell2010
    build-tool-depends:     hsc2hs:hsc2hs, hspec-discover:hspec-discover
    cc-options:             -march=native
    ghc-options:            -Wall 
                            -Wno-unticked-promoted-constructors
                            -Wno-incomplete-patterns
                            -- ploymorphic pattern synonym messed exhaustiveness checker

test-suite Z-Data-Test
    type:                   exitcode-stdio-1.0
    hs-source-dirs:         test/
    main-is:                Spec.hs

    ghc-options:            -threaded
    default-language:       Haskell2010

    other-modules:          Z.Data.CBytesSpec
                            Z.Data.Builder.NumericSpec
                            Z.Data.JSON.BaseSpec
                            Z.Data.JSON.ValueSpec
                            Z.Data.Parser.BaseSpec
                            Z.Data.Parser.NumericSpec
                            Z.Data.Array.UnalignedAccessSpec
                            Z.Data.Text.BaseSpec
                            Z.Data.Text.BuilderSpec
                            Z.Data.Text.ExtraSpec
                            Z.Data.Text.SearchSpec
                            Z.Data.Vector.BaseSpec
                            Z.Data.Vector.ExtraSpec
                            Z.Data.Vector.SearchSpec
                            Z.Data.Vector.SortSpec
                            Z.Data.Vector.FlatMapSpec
                            Z.Data.Vector.FlatSetSpec
    build-depends:          Z-Data
                          , base
                          , hspec                   >= 2.5.4
                          , hashable
                          , HUnit
                          , QuickCheck              >= 2.10
                          , quickcheck-instances
                          , word8
                          , scientific
                          , primitive

    if flag(integer-simple)
        cpp-options:        -DINTEGER_SIMPLE
        build-depends:      integer-simple          >= 0.1 && < 0.5
    else
        cpp-options:        -DINTEGER_GMP
        build-depends:      integer-gmp             >= 0.2 && < 1.1

