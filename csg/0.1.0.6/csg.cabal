cabal-version: 1.12
name: csg
version: 0.1.0.6
license: BSD3
license-file: LICENSE
maintainer: dima@dzhus.org
author: Dmitry Dzhus
homepage: https://github.com/dzhus/csg#readme
bug-reports: https://github.com/dzhus/csg/issues
synopsis: Analytical CSG (Constructive Solid Geometry) library
category: Graphics
build-type: Simple
extra-source-files:
    CHANGELOG.md
    README.md
    examples/cube.geo
    examples/reentry.geo

source-repository head
    type: git
    location: https://github.com/dzhus/csg

flag triples
    description:
        Use triples of Doubles to represent vectors (slower with vector library arrays)
    default: False
    manual: True

library
    exposed-modules:
        Data.CSG
        Data.CSG.Parser
    hs-source-dirs: src
    other-modules:
        Paths_csg
    default-language: Haskell2010
    ghc-options: -Wall -Wcompat -O2
    build-depends:
        QuickCheck <2.13,
        attoparsec <0.14,
        base <5,
        bytestring <0.11,
        containers <0.7,
        simple-vec3 ==0.4.*,
        strict <0.4,
        transformers <0.6
    
    if flag(triples)
        cpp-options: -DWITH_TRIPLES

executable csg-raycaster
    main-is: raycaster.hs
    hs-source-dirs: exe
    other-modules:
        Paths_csg
    default-language: Haskell2010
    ghc-options: -Wall -Wcompat -O2 -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        QuickCheck <2.13,
        base <5,
        csg -any,
        gloss <1.14,
        gloss-raster <1.14,
        simple-vec3 ==0.4.*,
        strict <0.4,
        system-filepath <0.5,
        turtle <1.6

test-suite csg-doctests
    type: exitcode-stdio-1.0
    main-is: doctest-driver.hs
    hs-source-dirs: tests
    other-modules:
        Main
        Paths_csg
    default-language: Haskell2010
    ghc-options: -Wall -Wcompat -O2 -threaded
    build-depends:
        base <5,
        doctest <0.17,
        doctest-driver-gen <0.4,
        simple-vec3 ==0.4.*

test-suite csg-tests
    type: exitcode-stdio-1.0
    main-is: Main.hs
    hs-source-dirs: tests
    other-modules:
        Paths_csg
    default-language: Haskell2010
    ghc-options: -Wall -Wcompat -O2
    build-depends:
        base <5,
        bytestring <0.11,
        csg -any,
        simple-vec3 ==0.4.*,
        tasty <1.3,
        tasty-hunit <0.11,
        tasty-quickcheck <0.11

benchmark csg-benchmark
    type: exitcode-stdio-1.0
    main-is: benchmark/Benchmark.hs
    other-modules:
        Paths_csg
    default-language: Haskell2010
    ghc-options: -Wall -Wcompat -O2
    build-depends:
        base <5,
        criterion <1.6,
        csg -any,
        simple-vec3 ==0.4.*,
        strict <0.4,
        vector <0.13
