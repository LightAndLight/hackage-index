cabal-version: 2.2

name: json-feed
version: 2.0.0.3

synopsis: JSON Feed
description:
  This is an implementation of the JSON Feed spec in Haskell. The spec is
  available at <https://jsonfeed.org>. JSON Feed is similar to the Atom and RSS
  feed formats, but it is serialized as JSON rather than XML.

build-type: Simple
category: Web
extra-source-files:
  CHANGELOG.markdown
  README.markdown
  feeds/allenpike.com.json
  feeds/daringfireball.net.json
  feeds/flyingmeat.com.json
  feeds/hypercritical.co.json
  feeds/inessential.com.json
  feeds/jsonfeed.org.json
  feeds/manton.org.json
  feeds/maybepizza.com.json
  feeds/shapeof.com.json
  feeds/therecord.co.json
  feeds/timetable.manton.org.json
license-file: LICENSE.markdown
license: MIT
maintainer: Taylor Fausak

source-repository head
  location: https://github.com/tfausak/json-feed
  type: git

flag pedantic
  default: False
  description: Enables @-Werror@, which turns warnings into errors.
  manual: True

common library
  build-depends:
    base >= 4.13.0 && < 4.17
    , aeson >= 1.4.7 && < 1.6 || >= 2.0.0 && < 2.2
    , bytestring >= 0.10.10 && < 0.12
    , mime-types >= 0.1.0 && < 0.2
    , network-uri >= 2.6.3 && < 2.7
    , tagsoup >= 0.14.8 && < 0.15
    , text >= 1.2.4 && < 1.3
    , time >= 1.9.3 && < 1.13
  default-language: Haskell2010
  ghc-options:
    -Weverything
    -Wno-all-missed-specialisations
    -Wno-implicit-prelude
    -Wno-missed-specialisations
    -Wno-missing-deriving-strategies
    -Wno-missing-exported-signatures
    -Wno-unsafe

  if flag(pedantic)
    ghc-options: -Werror

  if impl(ghc >= 8.10)
    ghc-options:
      -Wno-missing-safe-haskell-mode
      -Wno-prepositive-qualified-module

  if impl(ghc >= 9.2)
    ghc-options:
      -Wno-missing-kind-signatures

common executable
  import: library

  build-depends: json-feed
  ghc-options:
    -rtsopts
    -threaded
    -Wno-unused-packages

library
  import: library

  exposed-modules: JsonFeed
  hs-source-dirs: source/library

test-suite test
  import: executable

  build-depends:
    , filepath >= 1.4.2 && < 1.5
    , hspec >= 2.7.6 && < 2.11
  hs-source-dirs: source/test-suite
  main-is: Main.hs
  type: exitcode-stdio-1.0
