name: hgis
version: 0.1.1.2
cabal-version: >=1.10
build-type: Simple
license: BSD3
license-file: LICENSE
copyright: Copyright: (c) 2016 Vanessa McHale
maintainer: tmchale@wisc.edu
homepage: https://github.com/vmchale/hgis#readme
synopsis: Package and command-line for GIS with Haskell
description:
    Package containting functions to make graphs, read
    shapefiles, and compute areas/perimeters of
    geographic features.
category: Web
author: Vanessa McHale
extra-source-files:
    README.md
    stack.yaml
    docs/manual.tex

source-repository head
    type: git
    location: https://github.com/vmchale/hgis

flag llvm-fast
    description:
        Enable build with llvm backend
    default: False

library
    exposed-modules:
        GIS.Exe.Opt
        GIS.Exe.OptCairo
        GIS.Math.Spherical
        GIS.Graphics.PlotPNG
        GIS.Graphics.PlotSVG
        GIS.Hylo
        GIS.Math.Projections
    build-depends:
        base >=4.7 && <5,
        optparse-applicative >=0.13.1.0,
        Chart >=1.8.2,
        Chart-cairo >=1.8.2,
        Chart-diagrams >=1.8.2,
        lens >=4.15.1,
        composition >=1.0.2.1,
        ansi-wl-pprint >=0.6.7.3,
        transformers >=0.5.2.0,
        directory >=1.3.0.0,
        colour >=2.3.3,
        data-default >=0.7.1.1,
        binary >=0.8.3.0,
        bytestring >=0.10.8.1,
        data-binary-ieee754 >=0.4.4,
        filepath >=1.4.1.1,
        monad-loops >=0.4.3
    pkgconfig-depends: cairo >=0.13.3.1
    default-language: Haskell2010
    default-extensions: DeriveGeneric OverloadedStrings
    hs-source-dirs: src src/depends/readshp
    other-modules:
        GIS.Math.Utils
        GIS.Utils
        GIS.Graphics.Plot
        GIS.Graphics.Types
        GIS.Types
        GIS.Exe.Parser
        Geometry.Shapefile
        Geometry.Shapefile.MergeShpDbf
        Geometry.Shapefile.ReadDbf
        Geometry.Shapefile.ReadShp
        Geometry.Shapefile.Types
        Geometry.Shapefile.Internal

executable hgis
    
    if flag(llvm-fast)
        ghc-options: -threaded -rtsopts -with-rtsopts=-N -fllvm -optlo-O3 -O3
    else
        ghc-options: -threaded -rtsopts -with-rtsopts=-N
    main-is: MainPng.hs
    build-depends:
        base >=4.9.1.0,
        hgis >=0.1.1.2
    default-language: Haskell2010
    hs-source-dirs: app

test-suite hgis-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    build-depends:
        base >=4.9.1.0,
        hgis >=0.1.1.2,
        hspec >=2.4.2
    default-language: Haskell2010
    hs-source-dirs: test
    ghc-options: -threaded -rtsopts -with-rtsopts=-N
