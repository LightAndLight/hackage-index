name:            attoparsec
version:         0.10.0.0
x-revision: 2
license:         BSD3
license-file:    LICENSE
category:        Text, Parsing
author:          Bryan O'Sullivan <bos@serpentine.com>
maintainer:      Bryan O'Sullivan <bos@serpentine.com>
stability:       experimental
tested-with:     GHC == 6.12.3, GHC == 7.0.3, GHC == 7.2.1
synopsis:        Fast combinator parsing for bytestrings
cabal-version:   >= 1.8
homepage:        https://github.com/bos/attoparsec
bug-reports:     https://github.com/bos/attoparsec/issues
build-type:      Simple
description:
    A fast parser combinator library, aimed particularly at dealing
    efficiently with network protocols and complicated text/binary
    file formats.
extra-source-files:
    README.markdown
    benchmarks/Makefile
    benchmarks/Tiny.hs
    benchmarks/med.txt.bz2
    tests/Makefile
    tests/QC.hs
    tests/TestFastSet.hs
    examples/Makefile
    examples/Parsec_RFC2616.hs
    examples/RFC2616.hs
    examples/TestRFC2616.hs
    examples/rfc2616.c

Flag developer
  Description: Whether to build the library in development mode
  Default: False

library
  build-depends: bytestring <0.11

  build-depends: array,
                 base >= 3 && < 4.11,
                 bytestring <0.11,
                 containers,
                 deepseq >= 1.1,
                 text >= 0.11.1.5

  extensions:      CPP
  exposed-modules: Data.Attoparsec
                   Data.Attoparsec.ByteString
                   Data.Attoparsec.ByteString.Char8
                   Data.Attoparsec.ByteString.Lazy
                   Data.Attoparsec.Char8
                   Data.Attoparsec.Combinator
                   Data.Attoparsec.Lazy
                   Data.Attoparsec.Number
                   Data.Attoparsec.Text
                   Data.Attoparsec.Text.Lazy
                   Data.Attoparsec.Types
                   Data.Attoparsec.Zepto
  other-modules:   Data.Attoparsec.ByteString.FastSet
                   Data.Attoparsec.ByteString.Internal
                   Data.Attoparsec.Internal.Types
                   Data.Attoparsec.Text.FastSet
                   Data.Attoparsec.Text.Internal
  ghc-options:     -Wall

  if flag(developer)
    ghc-prof-options: -auto-all

test-suite tests
  type:           exitcode-stdio-1.0
  hs-source-dirs: tests
  main-is:        QC.hs

  ghc-options:
    -Wall -threaded -rtsopts

  build-depends:
    attoparsec,
    base >= 4 && < 4.11,
    bytestring <0.11,
    QuickCheck >= 2.4,
    test-framework >= 0.4,
    test-framework-quickcheck2 >= 0.2,
    text

source-repository head
  type:     git
  location: https://github.com/bos/attoparsec

source-repository head
  type:     mercurial
  location: https://bitbucket.org/bos/attoparsec
