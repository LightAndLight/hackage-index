cabal-version:      2.4
name:               mmzk-typeid
version:            0.3.1.0

synopsis:           A TypeID implementation for Haskell
description:
  'TypeID' is a type-safe, K-sortable, globally unique identifier inspired by Stripe IDs.
  .
  The specification is available at https://github.com/jetpack-io/typeid.
  .
  This library supports generating and parsing speç-conforming 'TypeID's, with the following additional features:
  .
    - Batch generating 'TypeID's with the same UUIDv7 timestamp
  .
    - Encode prefixes at type-level for better type-safety
  .
  It also serves as a (temporary) UUIDv7 implementation in Haskell, since there are no official ones yet.
  .
  Please enable the following extensions if working with 'KindID':
  .
  > {-# LANGUAGE DataKinds #-}
  > {-# LANGUAGE PolyKinds #-}
  > {-# LANGUAGE TypeApplications #-}
  > {-# LANGUAGE TypeFamilies #-}
  .
  While the following is not required, it is quite convenient to have
  .
  > {-# LANGUAGE OverloadedStrings #-}
  .
  For a quick "how-to-use" guide, please refer to the README.md file at https://github.com/MMZK1526/mmzk-typeid#readme.

homepage:           https://github.com/MMZK1526/mmzk-typeid
bug-reports:        https://github.com/MMZK1526/mmzk-typeid/issues
license:            MIT
author:             Yitang Chen <mmzk1526@ic.ac.uk>
maintainer:         Yitang Chen <mmzk1526@ic.ac.uk>
category:           Data, UUID, TypeID
tested-with:
    GHC == 9.2.8
    GHC == 9.6.2
extra-source-files:
    CHANGELOG.md
    LICENSE
    test/invalid.json
    test/valid.json


library
    exposed-modules:
        Data.KindID,
        Data.KindID.Class,
        Data.KindID.Unsafe,
        Data.TypeID,
        Data.TypeID.Class,
        Data.TypeID.Error,
        Data.TypeID.Unsafe,
        Data.UUID.V7
    other-modules:
        Data.KindID.Internal,
        Data.TypeID.Internal
    default-extensions:
        BlockArguments
        ConstraintKinds
        DataKinds
        InstanceSigs
        MultiWayIf
        OverloadedStrings
        PolyKinds
        ScopedTypeVariables
        TupleSections
        TypeApplications
        TypeFamilies
        TypeOperators
    build-depends:
        base >=4.16 && <5,
        aeson >=2.1 && <3,
        array ^>=0.5,
        binary >=0.8.5 && <0.9,
        bytestring ^>= 0.11,
        entropy ^>=0.4,
        hashable ^>=1.4,
        text ^>=2.0,
        time >=1.11 && <1.13,
        uuid-types ^>=1.0,
    hs-source-dirs:   src
    default-language: Haskell2010


test-suite test
    main-is:          Spec.hs
    type:             exitcode-stdio-1.0
    other-modules:
        Data.KindID,
        Data.KindID.Class,
        Data.KindID.Internal,
        Data.KindID.Unsafe,
        Data.TypeID,
        Data.TypeID.Class,
        Data.TypeID.Error,
        Data.TypeID.Internal,
        Data.TypeID.Unsafe,
        Data.UUID.V7
    default-extensions:
        BlockArguments
        ConstraintKinds
        DataKinds
        InstanceSigs
        MultiWayIf
        OverloadedStrings
        PolyKinds
        ScopedTypeVariables
        TupleSections
        TypeApplications
        TypeFamilies
        TypeOperators
    build-depends:
        base,
        aeson,
        array,
        binary,
        bytestring,
        containers ^>=0.6,
        entropy,
        hashable,
        hspec ^>=2.11,
        text,
        time,
        uuid-types,
    hs-source-dirs:
        src
        test
    default-language: Haskell2010
