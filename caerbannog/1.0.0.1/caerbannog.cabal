cabal-version: 2.2

name: caerbannog
version: 1.0.0.1

synopsis: That rabbit's got a vicious streak a mile wide!
description:
  Caerbannog is a drop in replacement for the @binary-bits@ package. Unlike
  @binary-bits@ Caerbannog works with GHC 8.10, 8.8, and 8.6.
  .
  The original package description of @binary-bits@ at version 0.5 was: "Bit
  parsing\/writing on top of binary. Provides functions to read and write bits
  to and from 8\/16\/32\/64 words."
  .
  Caerbannog allows you to do bit level gets and puts on top of the interface
  provided by the @binary@ package. This can be useful for parsing or
  generating binary formats that are not byte aligned.
  .
  There are many names for these types of actions: get and put, read and write,
  parse and generate, deserialize and serialize, decode and encode. Caerbannog
  doesn't care which word you use. It'll work anyway.

author: Lennart Kolmodin
build-type: Simple
category: Data, Parsing
extra-source-files: CHANGELOG.markdown README.markdown
license-file: LICENSE.markdown
license: BSD-3-Clause
maintainer: Taylor Fausak

source-repository head
  location: https://github.com/tfausak/caerbannog
  type: git

flag pedantic
  default: False
  description: Enables @-Werror@, which turns warnings into errors.
  manual: True

common library
  build-depends:
    , base >= 4.13.0 && < 4.18
    , binary >= 0.8.7 && < 0.9
    , bytestring >= 0.10.10 && < 0.12
  default-language: Haskell98
  ghc-options:
    -Wall

  if flag(pedantic)
    ghc-options: -Werror

common executable
  import: library

  build-depends: caerbannog
  ghc-options:
    -rtsopts
    -threaded
    -Wno-unused-packages

library
  import: library

  exposed-modules:
    Data.Binary.Bits
    Data.Binary.Bits.Get
    Data.Binary.Bits.Put
  hs-source-dirs: source/library

test-suite test
  import: executable

  build-depends:
    , hspec >= 2.7.6 && < 2.11
    , QuickCheck >= 2.13.2 && < 2.15
    , random >= 1.1 && < 1.3
  hs-source-dirs: source/test-suite
  main-is: Main.hs
  type: exitcode-stdio-1.0
