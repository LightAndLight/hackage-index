name:                lentil
version:             1.5.3.2
x-revision: 2
synopsis:            frugal issue tracker
description:         minumum effort, cohesive issue tracker based on
                     ubiquitous @TODO@s and @FIXME@s conventions.
                     Check homepage for manual, tutorial, binaries,
                     examples.
homepage:            http://www.ariis.it/static/articles/lentil/page.html
license:             GPL-3
license-file:        LICENSE
author:              Francesco Ariis <fa-ml@ariis.it> et al.
                     (check authors.txt)
maintainer:          Francesco Ariis <fa-ml@ariis.it>
copyright:           © 2015-2021 Francesco Ariis et al. (check authors.txt)
category:            Development, Project Management
stability:           Stable
build-type:          Simple
tested-with:         GHC==8.4.1, GHC==8.6.1, GHC==8.8.3
extra-source-files:  stack.yaml,
                     test/test-files/lang-comm/clang.c,
                     test/test-files/lang-comm/forth.fs,
                     test/test-files/lang-comm/haskell.hs,
                     test/test-files/lang-comm/javascript.js,
                     test/test-files/lang-comm/out.blocks,
                     test/test-files/lang-comm/pascal.pas,
                     test/test-files/lang-comm/python.py,
                     test/test-files/lang-comm/ruby.rb,
                     test/test-files/lang-comm/perl.pl,
                     test/test-files/lang-comm/shell.sh,
                     test/test-files/lang-comm/nix.nix,
                     test/test-files/lang-comm/xml.xml,
                     test/test-files/lang-comm/erlang.erl,
                     test/test-files/lang-comm/ocaml.ml,
                     test/test-files/lang-comm/rust.rs,
                     test/test-files/lang-comm/r.r,
                     test/test-files/lang-comm/standard-ml.sml,
                     test/test-files/lang-comm/rst.rst,
                     test/test-files/lang-comm/org-mode.org,
                     test/test-files/lang-comm/yaml.yml,
                     test/test-files/lang-comm/text.txt,
                     test/test-files/specific/contiguous.c,
                     test/test-files/specific/cont-custom.c,
                     test/test-files/specific/xyz.xyz,
                     test/test-files/specific/custom-fwords.c,
                     test/test-files/test-proj/base-case/fold-a/foo1.hs,
                     test/test-files/test-proj/base-case/fold-b/foo2.hs,
                     test/test-files/test-proj/base-case/fold-c/foo3.hs,
                     test/test-files/test-proj/base-case/fold-c/sub-fold/foo4.hs,
                     test/test-files/test-proj/base-case/fold-c/sub-fold/foo5.hs,
                     test/test-files/test-proj/dot-folders/foo1.hs,
                     test/test-files/test-proj/dot-folders/.alfa/foo1.hs,
                     test/test-files/test-proj/dot-folders/_beta/foo1.hs,
                     test/test-files/test-proj/upper/prova1.cpp,
                     test/test-files/test-proj/upper/prova2.cPp,
                     test/test-files/test-proj/upper/prova3.Cpp,
                     test/test-files/test-proj/upper/prova.F
extra-doc-files:     README, changes.txt, doc/usr/page.rst, doc/usr/test.zip,
                     issues.txt, authors.txt
cabal-version:       1.24

flag developer
  description: developer mode - warnings let compilation fail
  manual: True
  default: False


executable lentil
  main-is:             Main.hs
  build-depends:       base >= 4.9 && < 5.0,
                       ansi-wl-pprint==0.6.*,
                       csv==0.1.*,
                       deepseq==1.4.*,
                       dlist >= 0.8 && < 1.1,
                       directory    >= 1.2 && < 1.4,
                       filemanip==0.3.*,
                       filepath==1.4.*,
                       megaparsec >= 8.0 && < 9.3,
                       mtl == 2.2.*,
                       natural-sort==0.1.*,
                       optparse-applicative >= 0.16 && < 0.18,
                       regex-tdfa >= 1.2 && < 1.4,
                       semigroups >= 0.18 && < 0.21,
                       text >=1.2 && <2.1,
                       terminal-progress-bar==0.4.*
  other-modules:       Lentil.Types, Lentil.Args, Lentil.File, Lentil.Print,
                       Lentil.Query, Lentil.Export, Lentil.Parse.Source,
                       Lentil.Parse.Issue Lentil.Parse.Syntaxes,
                       Lentil.Parse.Run, Lentil.Helpers
  hs-source-dirs:      src
  default-language:    Haskell2010
  if flag(developer)
      GHC-Options: -Wall -Werror
  else
      GHC-Options: -Wall


test-suite test
  default-language:    Haskell2010
  if flag(developer)
     ghc-options:      -Wall -Werror
  else
     ghc-options:      -Wall
  HS-Source-Dirs:      test, src
  main-is:             Tests.hs
  build-depends:       base >= 4.9 && < 5.0,
                       ansi-wl-pprint==0.6.*,
                       csv==0.1.*,
                       directory    >= 1.2 && < 1.4,
                       deepseq==1.4.*,
                       dlist >= 0.8 && < 1.1,
                       filemanip==0.3.*,
                       filepath==1.4.*,
                       megaparsec >= 8.0 && < 9.3,
                       mtl == 2.2.*,
                       natural-sort==0.1.*,
                       optparse-applicative >= 0.16 && < 0.18,
                       regex-tdfa >= 1.2 && < 1.4,
                       semigroups >= 0.18 && < 0.21,
                       text >=1.2 && <2.1,
                       terminal-progress-bar==0.4.*
                       -- same as above, plus hspec
                       , hspec      >= 2.3 && < 2.10
  other-modules:       Lentil.Types, Lentil.Args, Lentil.File, Lentil.Print,
                       Lentil.Query, Lentil.Export, Lentil.Parse.Source,
                       Lentil.Parse.Issue, Lentil.Parse.Syntaxes,
                       Lentil.Parse.Run, Lentil.HelpersSpec
                       Lentil.ArgsSpec, Lentil.ExportSpec, Lentil.FileSpec,
                       Lentil.PrintSpec, Lentil.Parse.SourceSpec,
                       Lentil.Parse.IssueSpec, Lentil.QuerySpec,
                       Lentil.TypeSpec, Lentil.Parse.RunSpec,
                       Lentil.Helpers
  type:                exitcode-stdio-1.0

source-repository head
    type:     darcs
    location: http://www.ariis.it/link/repos/lentil/
