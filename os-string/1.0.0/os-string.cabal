cabal-version:      2.2
name:               os-string
version:            1.0.0

-- NOTE: Don't forget to update ./changelog.md
license:            BSD-3-Clause
license-file:       LICENSE
author:             Julian Ospald <hasufell@posteo.de>
maintainer:         Julian Ospald <hasufell@posteo.de>
copyright:          Julain Ospald 2021-2023
bug-reports:        https://github.com/haskell/os-string/issues
homepage:
  https://github.com/haskell/os-string/blob/master/README.md

category:           System
build-type:         Simple
synopsis:           Library for manipulating Operating system strings.
tested-with:
  GHC ==8.0.2
   || ==8.2.2
   || ==8.4.4
   || ==8.6.5
   || ==8.8.4
   || ==8.10.7
   || ==9.0.2
   || ==9.2.3

description:
  This package provides functionality for manipulating @OsString@ values, and is shipped with <https://www.haskell.org/ghc/ GHC>.
  .
  Version 1.0.0 (unlike the following versions) is a dummy package that prevents module name clashes
  between os-string and filepath if used together.
  .
  If you simply want the newer os-string package, make sure to depend on the newer versions:
  .
  > library
  >   build-depends:
  >     os-string >= 2.0.0
  .
  If you want to default to the older filepath provided OsString modules, but allow users to switch to the newer os-string,
  you can provide a flag like so:
  .
  > flag os-string
  >   description: Use the new os-string package
  >   default: False
  >   manual: False
  >
  > library
  >   -- ...
  >   if flag(os-string)
  >     build-depends: os-string >= 2.0.0
  >   else
  >     build-depends: filepath < 1.5, os-string < 2.0.0

extra-doc-files:
  changelog.md
  README.md

source-repository head
  type:     git
  location: https://github.com/haskell/os-string

library
  default-language: Haskell2010
  build-depends:
      filepath          <1.5

