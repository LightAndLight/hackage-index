cabal-version:      1.12
name:               bcp47
version:            0.2.0.3
license:            MIT
license-file:       LICENSE
copyright:          2019 Freckle Education
maintainer:         engineering@freckle.com
author:             Evan Rutledge Borden
homepage:           https://github.com/freckle/bcp47#readme
bug-reports:        https://github.com/freckle/bcp47/issues
synopsis:           Language tags as specified by BCP 47
description:
    /Language tags for use in cases where it is desirable to indicate the/
    /language used in an information object./
    .
    / - /<https://tools.ietf.org/html/bcp47>
    .
    This package exposes a language tag data type 'BCP47' and a 'Trie' data
    structure for collecting and querying information that varies based on
    language tag.
    .
    > import Data.BCP47 (en, enGB, sw)
    > import Data.BCP47.Trie (Trie, fromList, lookup)
    >
    > color :: Trie Text
    > color = fromList [(en, "color"), (sw, "rangi")]
    >
    > main = do
    >   print $ match en color -- Just "color"
    >   print $ match enGB color -- Nothing
    >   print $ lookup enGB color -- Just "color"

category:           Data, Data Structures
build-type:         Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
    type:     git
    location: https://github.com/freckle/bcp47

library
    exposed-modules:
        Data.BCP47
        Data.BCP47.Internal.Arbitrary
        Data.BCP47.Internal.Extension
        Data.BCP47.Internal.Language
        Data.BCP47.Internal.LanguageExtension
        Data.BCP47.Internal.Parser
        Data.BCP47.Internal.PrivateUse
        Data.BCP47.Internal.Region
        Data.BCP47.Internal.Script
        Data.BCP47.Internal.Subtags
        Data.BCP47.Internal.Variant
        Data.BCP47.Trie
        Data.BCP47.Trie.Internal

    hs-source-dirs:   library
    other-modules:    Paths_bcp47
    default-language: Haskell2010
    build-depends:
        QuickCheck >=2.14.2 && <2.15,
        aeson >=1.5.5.1 && <1.6,
        base >=4.7 && <5,
        containers >=0.6.2.1 && <0.7,
        country >=0.2.1 && <0.3,
        generic-arbitrary >=0.1.0 && <0.2,
        iso639 >=0.1.0.3 && <0.2,
        megaparsec >=9.0.1 && <10.0,
        text >=1.2.4.1 && <1.3

test-suite doctest
    type:             exitcode-stdio-1.0
    main-is:          Main.hs
    hs-source-dirs:   doctest
    other-modules:    Paths_bcp47
    default-language: Haskell2010
    build-depends:
        base >=4.7 && <5,
        doctest >=0.16.3 && <0.19

test-suite spec
    type:             exitcode-stdio-1.0
    main-is:          Main.hs
    hs-source-dirs:   tests
    other-modules:
        Data.BCP47.TrieSpec
        Data.BCP47Spec
        Paths_bcp47

    default-language: Haskell2010
    build-depends:
        QuickCheck >=2.14.2 && <2.15,
        aeson >=1.5.5.1 && <1.6,
        base >=4.7 && <5,
        bcp47 -any,
        containers >=0.6.2.1 && <0.7,
        country >=0.2.1 && <0.3,
        hspec >=2.7.8 && <2.8,
        iso639 >=0.1.0.3 && <0.2,
        text >=1.2.4.1 && <1.3
