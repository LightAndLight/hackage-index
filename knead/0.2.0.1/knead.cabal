Name:             knead
Version:          0.2.0.1
License:          BSD3
License-File:     LICENSE
Author:           Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:       Henning Thielemann <haskell@henning-thielemann.de>
Homepage:         http://hub.darcs.net/thielema/knead/
Category:         Data Structures
Synopsis:         Repa array processing using LLVM JIT
Description:
  This library processes arrays like @Repa@ and @Accelerate@,
  but it uses the just-in-time compiler of @LLVM@
  for generating the machine code.
  That is, you get very efficient vectorised code
  that can be run without a GPU.
  You do not need to care about inlining and strictness annotations,
  because the LLVM code is by default inlined and strict.
  The package is the basis for an LLVM backend for the @Accelerate@ framework.
  .
  Highlights:
  .
  * Very flexible index handling,
    even more flexible than the one of 'Data.Array'.
    It is much more expressive and type-safe than that of @repa@ and @array@.
  .
  * Extensible element types, e.g. complex numbers.
    (Maybe this is also possible with accelerate, e.g. with RGB type.)
  .
  * Every compilable program also runs.
    In contrast to that, @accelerate@ may accept a program
    that cannot be run by a particular backend, like @accelerate-cuda@.
  .
  Known deficiencies:
  .
  * The functions do not check array bounds.
    (Of course, we can think about temporary bound checking
    for debugging purposes.)
  .
  * The package does not try to distribute work across multiple processors.
    It is certainly simpler, more efficient and more reliable
    if you do that at a higher level.
  .
  The name of the package is inspired by the visualization of typical operations
  like reshaping, collapsing a dimension and extruding another one.
Tested-With:      GHC==7.4.2, GHC==7.8.4, GHC==8.0.1
Cabal-Version:    >=1.6
Build-Type:       Simple

Source-Repository this
  Tag:         0.2.0.1
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/knead/

Source-Repository head
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/knead/

Library
  Build-Depends:
    llvm-extra >=0.6 && <0.8,
    llvm-tf >=3.1 && <3.2,
    storable-tuple >=0.0 && <0.1,
    storable-record >=0.0.3 && <0.1,
    transformers >=0.3 && <0.6,
    utility-ht >=0.0.1 && <0.1,
    base >=4 && <5

  GHC-Options:      -Wall
  Hs-Source-Dirs:   src
  Exposed-Modules:
    Data.Array.Knead.Index.Linear.Int
    Data.Array.Knead.Index.Linear
    Data.Array.Knead.Index.Nested.Shape
    Data.Array.Knead.Expression
    Data.Array.Knead.Parameter
    Data.Array.Knead.Simple.Symbolic
    Data.Array.Knead.Simple.ShapeDependent
    Data.Array.Knead.Simple.Physical
    Data.Array.Knead.Simple.Slice
    Data.Array.Knead.Simple.Fold
    Data.Array.Knead.Parameterized.Symbolic
    Data.Array.Knead.Parameterized.Physical
    Data.Array.Knead.Parameterized.Slice
  Other-Modules:
    Data.Array.Knead.Simple.Private
    Data.Array.Knead.Parameterized.Private
    Data.Array.Knead.Code
