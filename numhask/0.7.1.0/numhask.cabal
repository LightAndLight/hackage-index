cabal-version: 2.4
name: numhask
version: 0.7.1.0
synopsis:
  A numeric class hierarchy.
description:
    This package provides numeric classes alternate to the prelude specified in [haskell98](https://www.haskell.org/onlinereport/standard-prelude.html).
    .
    The numeric class constellation looks somewhat like:
    .
    ![nh](docs/other/nh.svg)
    .
    == Usage
    .
    >>> {-# LANGUAGE NegativeLiterals #-}
    >>> {-# LANGUAGE RebindableSyntax #-}
    >>> import NumHask.Prelude
    .
    See "NumHask" for a detailed overview.

category:
  mathematics
homepage:
  https://github.com/tonyday567/numhask#readme
bug-reports:
  https://github.com/tonyday567/numhask/issues
author:
  Tony Day
maintainer:
  tonyday567@gmail.com
copyright:
  Tony Day
license:
  BSD-3-Clause
license-file:
  LICENSE
build-type:
  Simple
tested-with:
  GHC ==8.8.4
   || ==8.10.2

extra-doc-files:
  other/*.svg

extra-source-files:
  readme.md
  ChangeLog.md

source-repository head
  type:
    git
  location:
    https://github.com/tonyday567/numhask
  subdir:
    numhask
library
  hs-source-dirs:
    src
  ghc-options:
    -Wall
    -Wcompat
    -Wincomplete-record-updates
    -Wincomplete-uni-patterns
    -Wredundant-constraints
    -fwrite-ide-info
    -hiedir=.hie
  build-depends:
    base >=4.7 && <5,
    protolude >=0.3 && <0.4,
    bifunctors >= 5.5 && < 5.6,
    mmorph >= 1.1 && < 1.2,
    random >= 1.2 && < 1.3,
    transformers >= 0.5 && < 0.6,
    text >= 1.2 && < 1.3,
  exposed-modules:
    NumHask
    NumHask.Algebra.Additive
    NumHask.Algebra.Field
    NumHask.Algebra.Group
    NumHask.Algebra.Lattice
    NumHask.Algebra.Module
    NumHask.Algebra.Multiplicative
    NumHask.Algebra.Ring
    NumHask.Analysis.Metric
    NumHask.Data.Complex
    NumHask.Data.Integral
    NumHask.Data.LogField
    NumHask.Data.Positive
    NumHask.Data.Rational
    NumHask.Data.Wrapped
    NumHask.Exception
    NumHask.Prelude
  other-modules:
  default-language: Haskell2010

test-suite test
  type: exitcode-stdio-1.0
  main-is: test.hs
  hs-source-dirs:
    test
  build-depends:
    QuickCheck >= 2.13 && < 2.15,
    base >=4.7 && <5,
    doctest >= 0.16 && < 0.18,
    numhask,
  default-language: Haskell2010
  ghc-options:
    -Wall
    -Wcompat
    -Wincomplete-record-updates
    -Wincomplete-uni-patterns
    -Wredundant-constraints
