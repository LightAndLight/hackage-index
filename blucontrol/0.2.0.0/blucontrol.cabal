name:                blucontrol
version:             0.2.0.0
synopsis:            Configurable blue light filter
description:         This application is a blue light filter, with the main focus on
                     configurability.
                     .
                     Configuration is done in Haskell in the style of
                     <https://hackage.haskell.org/package/xmonad xmonad>.
                     .
                     Blucontrol makes use of monad transformers and allows monadic calculation of
                     gamma values and recoloring.
                     The user chooses, what will be captured in the monadic state.
                     .
                     To get started with configuring read "Blucontrol".
license:             BSD3
license-file:        LICENSE
author:              Felix Springer
maintainer:          felixspringer149@gmail.com
homepage:            https://github.com/jumper149/blucontrol
bug-reports:         https://github.com/jumper149/blucontrol/issues
category:            System
build-type:          Simple
extra-source-files:  CHANGELOG.md
cabal-version:       >= 1.10

library
  exposed-modules:     Blucontrol
                       Blucontrol.Control
                       Blucontrol.Control.Concat
                       Blucontrol.Control.Count
                       Blucontrol.Control.Print
                       Blucontrol.Control.Wait
                       Blucontrol.Gamma
                       Blucontrol.Gamma.Const
                       Blucontrol.Gamma.Linear
                       Blucontrol.Main
                       Blucontrol.Main.CLI
                       Blucontrol.Main.Control
                       Blucontrol.Recolor
                       Blucontrol.Recolor.Print
                       Blucontrol.Recolor.X
                       Blucontrol.RGB
                       Blucontrol.RGB.Temperature
  other-modules:       Blucontrol.Recolor.X.Internal
                       Paths_blucontrol
  build-depends:       base              >= 4.11    && < 5
                     , containers        >= 0.6.2.1 && < 0.7
                     , directory         >= 1.3.6.0 && < 1.4
                     , data-default      >= 0.7.1   && < 0.7.2
                     , deepseq           >= 1.4.4.0 && < 1.5
                     , filepath          >= 1.4.2.1 && < 1.5
                     , finite-typelits   >= 0.1.4.0 && < 0.1.5
                     , lifted-base       >= 0.2.3.2 && < 0.2.4
                     , monad-control     >= 1.0.2.0 && < 1.1
                     , mtl               >= 2.2.2   && < 2.3
                     , process           >= 1.6.6.0 && < 1.7
                     , text              >= 1.2.0.0 && < 1.3
                     , time              >= 1.9.3   && < 1.10
                     , transformers      >= 0.5.6.2 && < 0.5.7
                     , transformers-base >= 0.4.5.2 && < 0.5
                     , unix              >= 2.7.2.2 && < 2.8
                     , X11               >= 1.9     && < 1.10
  hs-source-dirs:      src
  includes:            XrandrGamma.h
  extra-libraries:     X11, Xrandr
  include-dirs:        include
  c-sources:           include/XrandrGamma.c
                       include/XrandrGamma.h
  default-language:    Haskell2010
  default-extensions:  ConstraintKinds
                       DataKinds
                       DeriveGeneric
                       FlexibleContexts
                       FlexibleInstances
                       FunctionalDependencies
                       GeneralizedNewtypeDeriving
                       MultiParamTypeClasses
                       OverloadedStrings
                       RankNTypes
                       StandaloneDeriving
                       TypeApplications
                       TypeFamilies
  ghc-options:         -Wall

executable blucontrol
  main-is:             Main.hs
  build-depends:       base
                     , blucontrol
  default-language:    Haskell2010
  ghc-options:         -Wall

test-suite test
  main-is:             Blucontrol.hs
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  other-modules:       Blucontrol.Test.Gamma.Linear
                       Blucontrol.Test.RGB
  build-depends:       base
                     , blucontrol
                     , data-default
                     , deepseq
                     , hspec        >= 2.6.0   && < 2.8
                     , mtl
                     , QuickCheck   >= 2.13.2  && < 2.15
                     , time
  default-language:    Haskell2010
  default-extensions:  ConstraintKinds
                       DataKinds
                       DeriveGeneric
                       FlexibleContexts
                       FlexibleInstances
                       FunctionalDependencies
                       GeneralizedNewtypeDeriving
                       MultiParamTypeClasses
                       OverloadedStrings
                       RankNTypes
                       StandaloneDeriving
                       TypeApplications
                       TypeFamilies
  ghc-options:         -Wall
