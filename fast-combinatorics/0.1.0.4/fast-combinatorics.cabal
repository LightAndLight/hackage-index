name:                fast-combinatorics
version:             0.1.0.4
synopsis:            Fast combinatorics.
description:         Fast combinatorics code with a high level of safety guaranteed by writing it in ATS.
homepage:            https://github.com//fast-combinatorics#readme
license:             BSD3
license-file:        LICENSE
author:              Vanessa McHale
maintainer:          vamchale@gmail.com
copyright:           Copyright: (c) 2017 Vanessa McHale
category:            Numerics
build-type:          Custom
extra-source-files:  cbits/combinatorics-ffi.c
                   , cbits/numerics-ffi.c
                   , ATS2-Postiats-include-0.3.8/ccomp/runtime/pats_ccomp_config.h
extra-doc-files:     README.md
cabal-version:       >= 1.18

Flag development {
  Description: Enable `-Werror`
  manual: True
  default: False
}

custom-setup
  setup-depends:   base
                 , Cabal >= 2.0
                 , http-client
                 , http-client-tls
                 , tar
                 , zlib
                 , directory

library
  c-sources:           cbits/combinatorics-ffi.c
                     , cbits/numerics-ffi.c
  include-dirs:        ATS2-Postiats-include-0.3.8/ccomp/runtime/
                     , ATS2-Postiats-include-0.3.8/
  hs-source-dirs:      src
  exposed-modules:     Numeric.Combinatorics
                     , Numeric.Pure.Combinatorics
                     , Numeric.Integer
  build-depends:       base >= 4.7 && < 5
                     , composition-prelude >= 0.1.1.4
  default-language:    Haskell2010
  if flag(development)
    ghc-options:       -Werror
  if impl(ghc >= 8.0)
    ghc-options:       -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat 
  ghc-options:         -Wall -optc-mtune=native -optc-flto -optc-O3

test-suite fast-combinatorics-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Spec.hs
  build-depends:       base
                     , fast-combinatorics
                     , hspec
  if flag(development)
    ghc-options: -Werror
  if impl(ghc >= 8.0)
    ghc-options:       -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat 
  ghc-options:         -threaded -rtsopts -with-rtsopts=-N -Wall
  default-language:    Haskell2010

benchmark fast-combinatorics-bench
  type:                exitcode-stdio-1.0
  hs-source-dirs:      bench
  main-is:             Bench.hs
  build-depends:       base
                     , fast-combinatorics
                     , criterion
  if flag(development)
    ghc-options: -Werror
  if impl(ghc >= 8.0)
    ghc-options:       -Wincomplete-uni-patterns -Wincomplete-record-updates -Wcompat 
  ghc-options:         -Wall -optc-mtune=native -optc-flto -optc-O3
  default-language:    Haskell2010

source-repository head
  type:     git
  location: https://github.com//fast-combinatorics
