name:           pomaps
version:        0.0.0.1
x-revision: 1
synopsis:       Maps and sets of partial orders
category:       Data Structures
homepage:       https://github.com/sgraf812/pomaps#readme
bug-reports:    https://github.com/sgraf812/pomaps/issues
maintainer:     Sebastian Graf
license:        MIT
license-file:   LICENSE.md
build-type:     Simple
cabal-version:  >= 1.10

extra-source-files:
    CHANGELOG.md
    LICENSE.md
    README.md
    stack.yaml

description:
  Maps (and sets) indexed by keys satisfying <https://hackage.haskell.org/package/lattices/docs/Algebra-PartialOrd.html#t:PartialOrd PartialOrd>.
  .
  The goal is to provide asymptotically better data structures than simple association lists or lookup tables.
  Asymptotics depend on the partial order used as keys, its /width/ \(w\) specifically (the size of the biggest anti-chain).
  .
  For partial orders with great width, this package won't provide any benefit over using association lists, so benchmark for your use-case!

source-repository head
  type: git
  location: https://github.com/sgraf812/pomaps

flag use-lattices
  description: Depend on the lattices package for the PartialOrd class.
  default: True

library
  other-extensions: TypeApplications
  hs-source-dirs:
      src
  ghc-options: -Wall
  build-depends:
      base >= 4.6.0.0 && < 4.11
    -- oneShot
    , ghc-prim >= 0.4 && < 0.6
    , deepseq >= 1.1 && < 1.5
    -- We depend on the internal modules of containers, 
    -- so we have to track development real close.
    -- Data.Map.Internal is only available since 0.5.9,
    -- of which 0.5.9.2 is the first safe version
    , containers >= 0.5.9.2 && <= 0.5.10.2
  if flag(use-lattices)
    build-depends: 
    -- We need PartialOrd instances for ()
      lattices >= 1.7 && < 2
  exposed-modules:
      Data.POMap.Internal
      Data.POMap.Lazy
      Data.POMap.Strict
      Data.POSet
      Data.POSet.Internal
  if !flag(use-lattices)
    hs-source-dirs:
      lattices
    exposed-modules:
      Algebra.PartialOrd
  default-language: Haskell2010

test-suite unittests
  type: exitcode-stdio-1.0
  main-is: Main.hs
  hs-source-dirs:
      tests
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , containers >= 0.5.9.2
    , pomaps
    , tasty
    , tasty-hspec
    , tasty-quickcheck
    , ChasingBottoms
  if flag(use-lattices)
    build-depends: 
      lattices < 2
  other-modules:
      Data.POMap.Arbitrary
      Data.POMap.Divisibility
      Data.POMap.Properties
      Data.POMap.Strictness
  default-language: Haskell2010

test-suite doctests
  type: exitcode-stdio-1.0
  main-is: doctest-driver.hs
  hs-source-dirs:     
      tests
  ghc-options: -threaded
  build-depends:      
      base >4 && <5
    , doctest
    , Glob
  default-language:   Haskell2010

benchmark pomaps-benchmarks
  type: exitcode-stdio-1.0
  main-is: Main.hs
  hs-source-dirs:
      bench
  ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
  build-depends:
      base
    , pomaps
    , criterion
    , deepseq
    , random
    , vector
  default-language: Haskell2010


