cabal-version:      1.18
name:               lzlib
version:            0.3.0.2
license:            BSD3
license-file:       LICENSE
copyright:          Copyright: (c) 2019 Vanessa McHale
maintainer:         vamchale@gmail.com
author:             Vanessa McHale
synopsis:           lzlib bindings
description:
    Lzlib bindings via [c2hs](http://hackage.haskell.org/package/c2hs).
    Includes a bundled copy of lzlib

category:           Codec, Compression
build-type:         Simple
extra-source-files:
    stack.yaml
    cbits/cbuffer.c
    cbits/decoder.c
    cbits/decoder.h
    cbits/encoder.c
    cbits/encoder.h
    cbits/encoder_base.c
    cbits/encoder_base.h
    cbits/fast_encoder.c
    cbits/fast_encoder.h
    cbits/lzip.h
    cbits/lzlib.h

extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/vmchale/lzlib

flag cross
    description: Set this flag if cross-compiling
    default:     False
    manual:      True

library
    exposed-modules:
        Codec.Lzip
        Codec.Lzip.Raw

    cc-options:       -O3
    c-sources:        cbits/lzlib.c
    hs-source-dirs:   src
    default-language: Haskell2010
    other-extensions: BangPatterns
    include-dirs:     cbits
    install-includes: cbits/lzlib.h
    ghc-options:
        -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
        -Wredundant-constraints

    build-depends:
        base >=4.9 && <5,
        bytestring -any

    if !flag(cross)
        build-tool-depends: c2hs:c2hs >=0.26.1

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

test-suite lzlib-test
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   test
    default-language: Haskell2010
    ghc-options:
        -threaded -rtsopts -with-rtsopts=-N -Wall -Wincomplete-uni-patterns
        -Wincomplete-record-updates -Wredundant-constraints

    build-depends:
        base -any,
        lzlib -any,
        hspec -any,
        bytestring -any,
        directory -any

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

benchmark lzlib-bench
    type:             exitcode-stdio-1.0
    main-is:          Bench.hs
    hs-source-dirs:   bench
    default-language: Haskell2010
    ghc-options:
        -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
        -Wredundant-constraints

    build-depends:
        base -any,
        lzlib -any,
        criterion -any,
        bytestring -any,
        temporary -any,
        filepath -any

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists
