name:               consumers
version:            2.2.0.0
synopsis:           Concurrent PostgreSQL data consumers

description:        Library for setting up concurrent consumers of data
                    stored inside PostgreSQL database in a simple,
                    declarative manner.

homepage:           https://github.com/scrive/consumers
license:            BSD3
license-file:       LICENSE
extra-source-files: CHANGELOG.md, README.md
author:             Scrive AB
maintainer:         Andrzej Rybczak <andrzej@rybczak.net>,
                    Jonathan Jouty <jonathan@scrive.com>,
                    Mikhail Glushenkov <mikhail@scrive.com>
copyright:          Scrive AB
category:           Concurrency, Database
build-type:         Simple
cabal-version:      >=1.10
tested-with:        GHC == 8.0.2, GHC == 8.2.2, GHC == 8.4.4, GHC == 8.6.2

Source-repository head
  Type:             git
  Location:         https://github.com/scrive/consumers.git

library
  exposed-modules:  Database.PostgreSQL.Consumers,
                    Database.PostgreSQL.Consumers.Config,
                    Database.PostgreSQL.Consumers.Consumer,
                    Database.PostgreSQL.Consumers.Components,
                    Database.PostgreSQL.Consumers.Utils

  build-depends:    base >4 && <5,
                    containers,
                    exceptions,
                    extra,
                    hpqtypes >= 1.6 && < 1.7,
                    lifted-base,
                    lifted-threads,
                    log-base >= 0.7,
                    monad-control,
                    monad-time,
                    mtl,
                    stm,
                    time,
                    transformers-base

  hs-source-dirs:   src

  ghc-options:      -O2 -Wall -funbox-strict-fields

  default-language: Haskell2010
  default-extensions: DeriveDataTypeable
                    , FlexibleContexts
                    , GeneralizedNewtypeDeriving
                    , NoImplicitPrelude
                    , OverloadedStrings
                    , RankNTypes
                    , RecordWildCards
                    , ScopedTypeVariables
                    , TupleSections
                    , TypeFamilies
                    , UndecidableInstances

test-suite consumers-example
  -- Not quite a test suite, just a lazy way to disable this component
  -- by default, but have Travis build it.
  type:               exitcode-stdio-1.0
  hs-source-dirs:     example
  main-is:            Example.hs
  default-language:   Haskell2010
  ghc-options:        -Wall
  build-depends:      base,
                      consumers,
                      hpqtypes,
                      hpqtypes-extras,
                      log-base,
                      text,
                      text-show,
                      transformers

test-suite consumers-test
  type:               exitcode-stdio-1.0
  hs-source-dirs:     test
  main-is:            Test.hs
  default-language:   Haskell2010
  ghc-options:        -Wall
  build-depends:      base,
                      consumers,
                      exceptions,
                      HUnit,
                      hpqtypes,
                      hpqtypes-extras,
                      log-base,
                      monad-control,
                      monad-loops,
                      monad-time,
                      mtl,
                      stm,
                      text,
                      text-show,
                      time,
                      transformers,
                      transformers-base
