cabal-version: >=1.10
name: datasets
version: 0.4.0
license: MIT
license-file: LICENSE
maintainer: Marco Zocca <ocramz fripost org>
author: Tom Nielsen <tanielsen@gmail.com>
tested-with: ghc ==7.10.2 ghc ==7.10.3 ghc ==8.0.1 ghc ==8.4.3
homepage: https://github.com/DataHaskell/dh-core
bug-reports: https://github.com/DataHaskell/dh-core/issues
synopsis: Classical data sets for statistics and machine learning
description:
    Classical machine learning and statistics datasets from
    the UCI Machine Learning Repository and other sources.
    .
    The @datasets@ package defines two different kinds of datasets:
    .
    * small data sets which are directly (or indirectly with `file-embed`)
    embedded in the package as pure values and do not require network or IO to download
    the data set. This includes Iris, Anscombe and OldFaithful.
    .
    * other data sets which need to be fetched over the network with
    `Numeric.Datasets.getDataset` and are cached in a local temporary directory.
    .
    The @datafiles/@ directory of this package includes copies of a few famous datasets, such as Titanic, Nightingale and Michelson.
    .
    Example :
    .
    > import Numeric.Datasets (getDataset)
    > import Numeric.Datasets.Iris (iris)
    > import Numeric.Datasets.Abalone (abalone)
    >
    > main = do
    >   -- The Iris data set is embedded
    >   print (length iris)
    >   print (head iris)
    >   -- The Abalone dataset is fetched
    >   abas <- getDataset abalone
    >   print (length abas)
    >   print (head abas)
category: Statistics, Machine Learning, Data Mining, Data
build-type: Simple
extra-source-files:
    changelog.md
    datafiles/iris.data
    datafiles/michelson.json
    datafiles/nightingale.json
    datafiles/titanic2_full.tsv
    datafiles/netflix/training/mv_0000001.txt
    datafiles/netflix/test/qualifying.txt
    datafiles/netflix/movies/movie_titles.txt

source-repository head
    type: git
    location: https://github.com/DataHaskell/dh-core/datasets

library
    exposed-modules:
        Numeric.Dataloader
        Numeric.Datasets
        Numeric.Datasets.Anscombe
        Numeric.Datasets.BostonHousing
        Numeric.Datasets.CIFAR10
        Numeric.Datasets.OldFaithful
        Numeric.Datasets.Abalone
        Numeric.Datasets.Adult
        Numeric.Datasets.BreastCancerWisconsin
        Numeric.Datasets.Car
        Numeric.Datasets.Coal
        Numeric.Datasets.CO2
        Numeric.Datasets.Gapminder
        Numeric.Datasets.Iris
        Numeric.Datasets.Internal.Streaming
        Numeric.Datasets.Michelson
        Numeric.Datasets.Mushroom
        Numeric.Datasets.Nightingale
        Numeric.Datasets.Quakes
        Numeric.Datasets.States
        Numeric.Datasets.Sunspots
        Numeric.Datasets.Titanic
        Numeric.Datasets.UN
        Numeric.Datasets.Vocabulary
        Numeric.Datasets.Wine
        Numeric.Datasets.WineQuality
        Numeric.Datasets.Netflix
    hs-source-dirs: src
    other-modules:
        Streaming.Instances
    default-language: Haskell2010
    other-extensions: TemplateHaskell
    ghc-options: -Wall -fno-warn-unused-imports
    build-depends:
        base >=4.6 && <5,
        aeson >=1.4.2.0,
        attoparsec >=0.13,
        bytestring >=0.10.8.2,
        cassava >=0.5.1.0,
        deepseq >=1.4.4.0,
        directory >=1.3.3.0,
        exceptions >=0.10.0,
        file-embed >=0.0.11,
        filepath >=1.4.2.1,
        hashable >=1.2.7.0,
        JuicyPixels >=3.3.3,
        microlens >=0.4.10,
        mtl >=2.2.2,
        mwc-random >=0.14.0.0,
        parallel >=3.2.2.0,
        req >=2.0.0,
        safe-exceptions >=0.1.7.0,
        streaming >=0.2.2.0,
        streaming-attoparsec >=1.0.0,
        streaming-bytestring >=0.1.6,
        streaming-cassava >=0.1.0.1,
        streaming-commons >=0.2.1.0,
        stringsearch >=0.3.6.6,
        tar >=0.5.1.0,
        text >=1.2.3.1,
        time >=1.8.0.2,
        transformers >=0.5.5.0,
        vector >=0.12.0.2,
        safe-exceptions >=0.1.7.0,
        zlib >=0.6.2

test-suite spec
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    default-language: Haskell2010
    default-extensions: OverloadedStrings
    ghc-options: -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        base >=4.6 && <5,
        QuickCheck >=2.12.6.1,
        hspec >=2.6.0

benchmark bench
    type: exitcode-stdio-1.0
    main-is: Main.hs
    hs-source-dirs: bench
    other-modules:
        Numeric.Dataloader.Benchmark
    default-language: Haskell2010
    ghc-options: -Wall -O2 -rtsopts -threaded
    build-depends:
        base >=4.6 && <5,
        criterion >=1.5.3.0,
        datasets -any,
        deepseq >=1.4.4.0,
        directory >=1.3.3.0,
        filepath >=1.4.2.1,
        JuicyPixels >=3.3.3,
        mwc-random >=0.14.0.0,
        req >=2.0.0,
        safe-exceptions >=0.1.7.0,
        streaming >=0.2.2.0
