name:                prizm
version:             1.1.0
synopsis:            Color transformations in different color spaces
homepage:            https://github.com/ixmatus/prizm
license:             BSD3
license-file:        LICENSE
author:              Parnell Springmeyer
maintainer:          parnell+hackage@digitalmentat.com
copyright:           (c) 2013 Parnell Springmeyer
category:            Graphics
build-type:          Simple
stability:           stable
bug-reports:         https://github.com/ixmatus/prizm/issues
package-url:         http://hackage.haskell.org/package/prizm
tested-with:         GHC >= 7.8.4
cabal-version:       >=1.18.1

description:
  @prizm@ is a Haskell library for transforming colors. Specifically,
  providing functions for transforming between different color spaces
  (@CIE@ and @sRGB@), interpolating colors and adjusting the tint,
  shade, hue, or lightness or a color.
  .
  The inspiration for this library came from a desire to blend two
  colors represented in the @sRGB@ color space. My research about color
  blending and color space representation in the computer led me to
  the conclusion that the @CIE L*Ch@ color space is the most effective
  for blending because it most accurately represents how the human eye
  sees hue and therefore preserves (and blends) hue the most
  accurately.
  .
  /Colour Module/
  .
  There is a bit of overlap with the Haskell Colour Module; however,
  this library is filling a couple of needs the other doesn't
  satisfy. Primarily transformations and color mutations in the @CIE
  L*Ch@ space.
  .
  /Supported Algorithms & Functions/
  .
  All of these functions operate on color within the @CIE L*Ch@ color
  space. The percentage values may range between -100 and 100.
  .
  * @sRGB     <-> CIE XYZ @
  .
  * @CIE XYZ  <-> CIE L*ab@
  .
  * @CIE L*ab <-> CIE L*Ch@
  .
  * Color interpolation
  .
  * Tinting / Darkening
  .
  * Lightness
  .
  * Hue
  .
  * Chroma/Saturation
  .
  /Formulas, Resources, and Links/
  .
  * <http://www.brucelindbloom.com/index.html?Info.html General Color Formulas, Data, and Algorithms>
  .
  * <http://rip94550.wordpress.com/2011/07/04/color-cielab-and-tristimulus-xyz/ CIE Conversion Mathematics>
  .
  * <http://www.easyrgb.com/index.php?X=MATH&H=01 Conversion Algorithm Sources>
  .
  * <https://github.com/mikeemoo/ColorJizz-PHP/blob/master/src/MischiefCollective/ColorJizz/ColorJizz.php Good list of useful color manipulation formulas>

extra-source-files:
    LICENSE
    README.md

source-repository head
  type:     git
  location: https://github.com/ixmatus/prizm

library
  hs-source-dirs: src
  default-language: Haskell2010
  exposed-modules:
    Data.Prizm.Types
    Data.Prizm.Color
    Data.Prizm.Color.Transform
    Data.Prizm.Color.SRGB
    Data.Prizm.Color.CIE
    Data.Prizm.Color.Matrices.RGB
    Data.Prizm.Color.Matrices.XYZ
  
  ghc-options: -Wall -fno-warn-orphans

  build-depends:
                base             >= 4.5 && < 5,
                text             >= 0.11.2.3,
                convertible      >= 1.1,
                mono-traversable >= 1.0

test-suite tests
  type:           exitcode-stdio-1.0
  hs-source-dirs: tests
  main-is:        Test.hs
  default-language: Haskell2010
  other-modules:
    QC.SRGB
    QC.CIE

  ghc-options:
    -Wall -threaded -rtsopts

  build-depends:
                prizm,
                base                       >= 4,
                QuickCheck                 >= 2.5,
                convertible                >= 1.1,
                test-framework             >= 0.8,
                mono-traversable           >= 1.0,
                test-framework-quickcheck2 >= 0.3.0,
                test-framework-hunit       >= 0.3.0