cabal-version:      2.0
name:               jacinda
version:            0.2.1.0
x-revision: 1
license:            AGPL-3
license-file:       COPYING
maintainer:         vamchale@gmail.com
author:             Vanessa McHale
bug-reports:        https://github.com/vmchale/jacinda/issues
synopsis:           Functional, expression-oriented data processing language
description:
    APL meets AWK. A command-line tool for summarizing and reporting, powered by Rust's [regex](https://docs.rs/regex/regex/) library.

category:           Language, Interpreters, Text, Data
build-type:         Simple
extra-source-files:
    CHANGELOG.md
    README.md
    man/ja.1
    doc/guide.pdf
    test/examples/*.jac
    lib/*.jac

source-repository head
    type:     git
    location: https://github.com/vmchale/jacinda

flag cross
    description: Enable to ease cross-compiling
    default:     False
    manual:      True

library jacinda-lib
    exposed-modules:
        Jacinda.Parser
        Jacinda.Parser.Rewrite
        Jacinda.AST
        Jacinda.Ty
        Jacinda.Ty.Const
        Jacinda.Regex
        Jacinda.File

    hs-source-dirs:   src
    other-modules:
        Jacinda.Lexer
        Intern.Name
        Intern.Unique
        Jacinda.Rename
        Jacinda.Backend.Normalize
        Jacinda.Backend.TreeWalk
        Jacinda.Backend.Printf
        Data.List.Ext
        Data.Vector.Ext

    default-language: Haskell2010
    ghc-options:      -Wall -O2
    build-depends:
        base >=4.10.0.0 && <5,
        bytestring >=0.11.0.0,
        text,
        prettyprinter >=1.7.0,
        containers,
        array,
        mtl,
        transformers,
        regex-rure,
        microlens,
        microlens-mtl>=0.1.8.0,
        vector,
        recursion >=1.0.0.0

    if !flag(cross)
        build-tool-depends: alex:alex, happy:happy

    if impl(ghc >=8.0)
        ghc-options:
            -Wincomplete-uni-patterns -Wincomplete-record-updates
            -Wredundant-constraints -Widentities

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.2)
        ghc-options: -Wcpp-undef

    if impl(ghc >=8.10)
        ghc-options: -Wunused-packages

executable ja
    main-is:          Main.hs
    hs-source-dirs:   app
    other-modules:    Paths_jacinda
    autogen-modules:  Paths_jacinda
    default-language: Haskell2010
    ghc-options:      -Wall -rtsopts -with-rtsopts=-A200k
    build-depends:
        base,
        jacinda-lib,
        optparse-applicative,
        bytestring

    if impl(ghc >=8.0)
        ghc-options:
            -Wincomplete-uni-patterns -Wincomplete-record-updates
            -Wredundant-constraints -Widentities

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.2)
        ghc-options: -Wcpp-undef

    if impl(ghc >=8.10)
        ghc-options: -Wunused-packages

test-suite jacinda-test
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   test
    default-language: Haskell2010
    ghc-options:      -Wall -threaded -rtsopts "-with-rtsopts=-N -K1K" -Wall
    build-depends:
        base,
        jacinda-lib,
        tasty,
        tasty-hunit,
        bytestring

    if impl(ghc >=8.0)
        ghc-options:
            -Wincomplete-uni-patterns -Wincomplete-record-updates
            -Wredundant-constraints -Widentities

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.2)
        ghc-options: -Wcpp-undef

    if impl(ghc >=8.10)
        ghc-options: -Wunused-packages
