name:           aern2-mp
version:        0.1.3.1
cabal-version:  >= 1.9.2
build-type:     Simple
homepage:       https://github.com/michalkonecny/aern2
author:         Michal Konecny
maintainer:     Michal Konecny <mikkonecny@gmail.com>
copyright:      (c) 2015-2018 Michal Konecny
license:        BSD3
license-file:   LICENSE
extra-source-files:  changelog.md
stability:      experimental
category:       Math
synopsis:       Multi-precision ball (interval) arithmetic
Description:
  This package provides the following types:
  .
  * Dyadic:  variable-precision floats with exact ring operations
  .
  * MPBall: float ± error bound with field & elementary interval-like operations
  .
  The types have instances of both <https://hackage.haskell.org/package/mixed-types-num MixedTypeNumPrelude> 
  type classes as well as with traditional Prelude type classes.
  .
  There is a plan to add an Integer-only backend so that aern2-mp can
  be used without MPFR.

source-repository head
  type:     git
  location: https://github.com/mikkonecny/aern2.git
  subdir: aern2-mp

flag UseCDAR
  Description: Use an integer-only backend (work in progress, not default)
  Default:     False

library
  hs-source-dirs:  src
  build-depends:
    base == 4.*
    , integer-logarithms
    , convertible
    , regex-tdfa
    , hspec
    -- , hspec-smallcheck >= 0.3
    , QuickCheck
    , lens
    , template-haskell
    , mixed-types-num
  if flag(UseCDAR)
    cpp-options: -DUseCDAR
    build-depends:
      cdar
  else
    build-depends:
      rounded == 0.1.*
-- TODO
  ghc-options:     -Wall -fno-warn-orphans
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    DeriveGeneric,
    GeneralizedNewtypeDeriving,
    TypeFamilies,
    TypeOperators,
    ConstraintKinds,
    DefaultSignatures,
    MultiParamTypeClasses,
    FlexibleContexts,
    FlexibleInstances,
    UndecidableInstances
  if flag(UseCDAR)
    exposed-modules:
  else
    exposed-modules:
      AERN2.MP.Float.UseRounded.Type
      AERN2.MP.Float.UseRounded.RoundedAdaptor
      AERN2.MP.Float.UseRounded.Arithmetic
      AERN2.MP.Float.UseRounded.Conversions
  exposed-modules:
    AERN2.Utils.Bench
    AERN2.Normalize
    AERN2.Norm
    AERN2.MP.Precision
    AERN2.MP.Accuracy
    AERN2.MP.Enclosure
    AERN2.MP.ErrorBound
    AERN2.MP.Float.Operators
    AERN2.MP.Float.Constants
    AERN2.MP.Float.Tests
    AERN2.MP.Float
    AERN2.MP.Dyadic
    AERN2.MP.Ball.Type
    AERN2.MP.Ball.Conversions
    AERN2.MP.Ball.Comparisons
    AERN2.MP.Ball.Field
    AERN2.MP.Ball.Elementary
    AERN2.MP.Ball.PreludeOps
    AERN2.MP.Ball
    AERN2.MP.Ball.Tests
    AERN2.MP

test-suite spec
  type:
      exitcode-stdio-1.0
  ghc-options:
      -Wall
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    FlexibleContexts
  hs-source-dirs:
      test
  main-is:
      Spec.hs
  other-modules:
    AERN2.MP.BallSpec
    AERN2.MP.DyadicSpec
    AERN2.MP.FloatSpec
  build-depends:
    base == 4.*
    -- , mixed-types-num >= 0.3.1 && < 0.4
    , aern2-mp
    , hspec >= 2.1
    -- , hspec-smallcheck >= 0.3
    , QuickCheck >= 2.7
