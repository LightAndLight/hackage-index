name:           aern2-mp
version:        0.1.0.0
cabal-version:  >= 1.9.2
build-type:     Simple
homepage:       https://github.com/michalkonecny/aern2/aern2-mp
author:         Michal Konecny
maintainer:     Michal Konecny <mikkonecny@gmail.com>
copyright:      (c) 2015-2017 Michal Konecny
license:        BSD3
license-file:   LICENSE
stability:      experimental
tested-with:    GHC==7.10.3, GHC==8.0.2
category:       Math
synopsis:       Multi-precision floats via MPFR
Description:
  Arbitrary-precision dyadic numbers and interval arithmetic, via the types:
  .
  * Dyadic:  variable-precision floats with exact ring operations
  .
  * MPBall: float ± error bound with field & elementary (interval-like) operations
  .
  By default, using hmpfr when compiling with ghc >=7.10.  Older versions of ghc are no longer tested.
  .
  Set flag MPFRRounded to bind MPFR not via hmpfr but via a tweaked version of package
  rounded:
  .
  * https://github.com/michalkonecny/rounded
  .
  This backend is considerably faster than hmpfr.
  It is likely to become the default in future.
  .
  There is a plan to add an Integer-only backend so that this library can
  be used without MPFR.

source-repository head
  type:     git
  location: https://github.com/mikkonecny/aern2.git
  subdir: aern2-mp

flag MPFR
  Description: Use a MPFR backend (default); Integer backend is planned
  Default:     True

flag MPFRRounded
  Description: Use rounded (Numeric.RoundedSimple) as MPFR backend (not default)
  -- Default:     True
  Default:     False

library
  hs-source-dirs:  src
  build-depends:
    base == 4.*
    , integer-logarithms >= 1.0 && < 1.1
    , convertible >= 1.1.1.0 && < 1.2
    , regex-tdfa >= 1.2.1 && < 1.3
    , hspec >= 2.1 && < 2.5
    -- , hspec-smallcheck >= 0.3 && < 0.5
    , QuickCheck >= 2.7 && < 2.10
    , lens >= 4.13 && < 4.16
    , template-haskell
    , mixed-types-num >= 0.3.1 && < 0.4
  if flag(MPFR)
    cpp-options: -DUSEMPFR
    if impl(ghc >= 7.10)
      if flag(MPFRRounded)
        build-depends:
          rounded == 0.1.*
        cpp-options: -DMPFRRounded
      else
        build-depends:
          hmpfr == 0.4.*,
          integer-gmp >= 1.0 && < 1.1
        cpp-options: -DHMPFR
    else
      build-depends:
        haskell-mpfr == 0.1.*,
        integer-gmp < 0.6
      cpp-options: -DHaskellMPFR
  else
    cpp-options: -DUseIReal
    -- TODO
  ghc-options:     -Wall -fno-warn-orphans
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    DeriveGeneric,
    GeneralizedNewtypeDeriving,
    TypeFamilies,
    TypeOperators,
    ConstraintKinds,
    DefaultSignatures,
    MultiParamTypeClasses,
    FlexibleContexts,
    FlexibleInstances,
    UndecidableInstances
  if flag(MPFR)
    exposed-modules:
      AERN2.MP.UseMPFR.Float.Type
      AERN2.MP.UseMPFR.Float.RoundedAdaptor
      AERN2.MP.UseMPFR.Float.Arithmetic
      AERN2.MP.UseMPFR.Float.Conversions
      AERN2.MP.UseMPFR.Float.Operators
      AERN2.MP.UseMPFR.Float.Constants
      AERN2.MP.UseMPFR.Float.Tests
      AERN2.MP.UseMPFR.Float
      AERN2.MP.UseMPFR.Dyadic
      AERN2.MP.UseMPFR.ErrorBound
      AERN2.MP.UseMPFR.Ball.Type
      AERN2.MP.UseMPFR.Ball.Conversions
      AERN2.MP.UseMPFR.Ball.Comparisons
      AERN2.MP.UseMPFR.Ball.Field
      AERN2.MP.UseMPFR.Ball.Elementary
      AERN2.MP.UseMPFR.Ball.PreludeOps
      AERN2.MP.UseMPFR.Ball
  exposed-modules:
    AERN2.Utils.Bench
    AERN2.Normalize
    AERN2.Norm
    AERN2.MP.Precision
    AERN2.MP.Accuracy
    AERN2.MP.Enclosure
    AERN2.MP.Dyadic
    AERN2.MP.ErrorBound
    AERN2.MP.Ball.Tests
    AERN2.MP.Ball
    AERN2.MP

test-suite spec
  type:
      exitcode-stdio-1.0
  ghc-options:
      -Wall
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    FlexibleContexts
  hs-source-dirs:
      test
  main-is:
      Spec.hs
  other-modules:
    AERN2.MP.BallSpec
    AERN2.MP.DyadicSpec
    AERN2.MP.UseMPFR.FloatSpec
  build-depends:
    base == 4.*
    -- , mixed-types-num >= 0.3.1 && < 0.4
    , aern2-mp
    , hspec >= 2.1 && < 2.5
    -- , hspec-smallcheck >= 0.3 && < 0.5
    , QuickCheck >= 2.7 && < 2.10
