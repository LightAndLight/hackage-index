name:                wkt-geom
version:             0.0.4
synopsis:            A parser of WKT, WKB and eWKB.
description:         Well Known Text (WKT), Well Known Binary and the Postgresql extension Extended Well Know Binary (eWKB) are vector geometry representations.
                     .
                     The text or binary representations are parsed and turned into their GeoJSON representations.  The binary representations use vectors throughout
                     whereas the text representation still uses a list and then is converted to a vector representation.
homepage:            https://github.com/indicatrix/wkt-geom#readme
license:             Apache-2.0
license-file:        LICENSE
author:              Andrew Newmnan, Dominic Endrei
maintainer:          andrewfnewman@gmail.com
copyright:           2017-2018 Wkt-Geom Project
category:            Data
build-type:          Simple
extra-source-files:  README.md
cabal-version:       >=1.10

library
  hs-source-dirs:      src
  exposed-modules:     Data.Ewkb
                     , Data.Wkb
                     , Data.Wkt
                     , Data.Internal.Ewkb.Geometry                     
                     , Data.Internal.Wkb.Endian
                     , Data.Internal.Wkb.Geometry
                     , Data.Internal.Wkb.GeometryCollection
                     , Data.Internal.Wkb.Geospatial
                     , Data.Internal.Wkb.Line
                     , Data.Internal.Wkb.Point
                     , Data.Internal.Wkb.Polygon
                     , Data.Internal.Wkt.Common
                     , Data.Internal.Wkt.Line
                     , Data.Internal.Wkt.Point
                     , Data.Internal.Wkt.Polygon
                     , Data.Internal.Wkt.GeometryCollection
                     

  build-depends:       base < 5 &&      >= 4.8 
                     , binary           >= 0.8
                     , bytestring       >= 0.10
                     , containers       >= 0.5.10.1
                     , geojson          >= 3.0.3
                     , scientific       >= 0.3.6
                     , trifecta         >= 1.7
                     , utf8-string      >= 1
                     , vector           >= 0.10

  ghc-options:         -Wall
  default-language:    Haskell2010

test-suite wkt-geom-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  other-modules:       Data.Internal.Ewkb.GeometrySpec
                     , Data.Internal.Wkb.EndianSpec
                     , Data.Internal.Wkb.GeometrySpec
                     , Data.WkbGeometryCollectionSpec
                     , Data.WkbLineSpec
                     , Data.WkbPointSpec
                     , Data.WkbPolygonSpec
                     , Data.WktGeometryCollectionSpec
                     , Data.WktLineSpec
                     , Data.WktPointSpec
                     , Data.WktPolygonSpec          
                     , Data.SpecHelper  
  main-is:             Spec.hs
  build-depends:       base
                     , binary          >= 0.8
                     , ansi-wl-pprint  >= 0.6
                     , bytestring      >= 0.10
                     , containers      >= 0.5.10.1
                     , geojson         >= 3.0.3
                     , hspec
                     , lens
                     , QuickCheck
                     , scientific      >= 0.3.6
                     , trifecta        >= 1.7
                     , vector          >= 0.10            
                     , wkt-geom
  ghc-options:         -Wall
  default-language:    Haskell2010

source-repository head
  type:     git
  location: https://github.com/indicatrix/wkt-geom

source-repository this
  type:     git
  location: https://github.com/indicatrix/wkt-geom.git
  tag:      0.0.4
