Name:           classify-frog
Version:        0.2.4.1
License:        BSD3
License-File:   LICENSE
Author:         Henning Thielemann <haskell@henning-thielemann.de>
Maintainer:     Henning Thielemann <haskell@henning-thielemann.de>
Homepage:       http://code.haskell.org/~thielema/classify-frog/
Category:       Bioinformatics
Synopsis:       Classify sounds produced by Xenopus laevis
Description:
  This program is part of the Xenocall project.
  Researchers observed that the amount and duration of calls
  of the African clawed frog (Xenopus laevis) change
  when exposed to hormonal effective substances.
  The Xenocall project checks whether this effect
  can be used as an endpoint in animal tests
  instead of dissecting animals after the experiments.
  .
  An experiment for one substance
  may produce about 100 hours of audio recordings.
  It is a very tedious and error-prone work
  to examine this amount of data visually or by listening.
  The purpose of this program is to examine the recordings automatically
  and generate tables with basic statistical parameters.
  .
  Read the full report at
  <http://code.henning-thielemann.de/classify-frog-doc/report.pdf>.
Tested-With:    GHC==7.4.2, GHC==7.8.4
Cabal-Version:  >=1.6
Build-Type:     Simple

Extra-Source-Files:
  data/directory-order.txt
  data/advertisement.gp
  data/advertisement.sh
  data/advertisement-hourly.gp

Data-Files:
  model/diclo/hmm-global-supervised.csv
  model/diclo/hmm-global-unsupervised.csv
  model/diclo/hmm-growling-supervised.csv
  model/diclo/hmm-supervised.csv

Source-Repository this
  Tag:         0.2.4.1
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/classify-frog

Source-Repository head
  Type:        darcs
  Location:    http://hub.darcs.net/thielema/classify-frog

Flag buildSketch
  Description: Enable some sketch programs
  Default:     False

Flag llvm
  Description: Enable efficient signal processing using LLVM
  Default:     True

Executable classify-frog
  Main-Is: Main.hs
  Other-Modules:
    HiddenMarkovModel
    HiddenMarkovModel.Hardwired
    Label
    Class
    ClassRecord
    Durations
    Evaluation
    Measurement
    SpectralDistribution
    SignalProcessingSpecific
    SignalProcessing
    SignalProcessingMethods
    SignalProcessingOption
    Sox
    Audacity
    Audacity.TrackName
    Spreadsheet.Formula
    Spreadsheet.Row
    Spreadsheet.Palisade
    Spreadsheet.Format
    Quantile
    Time
    Fourier
    Arithmetic
    LabelTrack
    LabelChain
    LabelChainShifted
    LabelPattern
    Signal
    Rate
    Named
    Parameters
    Feature
    Option
  Hs-Source-Dirs: src

  Build-Depends:
    hmm-lapack >=0.3 && <0.4,
    lapack >=0.2 && <0.3,
    comfort-array >=0.2 && <0.3,
    text >=1.1 && <1.3,
    lazy-csv >=0.5 && <0.6,
    tagchup >=0.4 && <0.5,
    xml-basic >=0.1.1 && <0.2,
    synthesizer-core >=0.7.1 && <0.9,
    audacity >=0.0.1 && <0.1,
    soxlib >=0.0 && <0.1,
    gnuplot >=0.5 && <0.6,
    parallel >=3.2 && <3.3,
    pooled-io >=0.0.2 && <0.1,
    concurrent-split >=0.0 && <0.1,
    fft >=0.1.8 && <0.2,
    carray >=0.1.5 && <0.2,
    storablevector-carray >=0.0 && <0.1,
    -- for NFData instance
    storablevector >=0.2.11 && <0.3,
    storable-record >=0.0 && <0.1,
    array >=0.4 && <0.6,
    time >=1.5 && <1.9,
    Cabal >=1.14 && <3,
    optparse-applicative >=0.11 && <0.15,
    filemanip >=0.3.6 && <0.4,
    pathtype >=0.8 && <0.9,
    non-empty >=0.3 && <0.4,
    semigroups >=0.1 && <1.0,
    containers >=0.4 && <0.7,
    explicit-exception >=0.1.8 && <0.2,
    transformers >=0.2 && <0.6,
    bifunctors >=5 && <6,
    semigroups >=0.8.4.1 && <0.19,
    utility-ht >=0.0.12 && <0.1,
    numeric-prelude >=0.4 && <0.5,
    deepseq >=1.3 && <1.5,
    base >=4 && <5

  If flag(llvm)
    Hs-Source-Dirs: llvm/yes
    Other-Modules:
      SignalProcessingSpecificLLVM
      SignalProcessingLLVM
    Build-Depends:
      synthesizer-llvm >=0.8 && <0.9,
      llvm-extra >=0.6 && <0.8,
      llvm-tf >=3.0 && <3.2
  Else
    Hs-Source-Dirs: llvm/no

  GHC-Options: -Wall -fwarn-tabs -fwarn-incomplete-record-updates
  GHC-Options: -rtsopts -threaded
  GHC-Prof-Options: -fprof-auto -rtsopts

  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
    CPP-Options: -DNoImplicitPrelude=RebindableSyntax
    Extensions: CPP

Executable spectral-distribution
  Main-Is: SpectralDistributionTest.hs
  Other-Modules:
    SpectralDistribution
    SignalProcessing
    Rate
    Parameters
  Hs-Source-Dirs: src
  If flag(buildSketch)
    Build-Depends:
      synthesizer-core,
      storablevector >=0.2.10 && <0.3,
      utility-ht,
      numeric-prelude >=0.4 && <0.5,
      base >=4 && <5
  Else
    Buildable: False

  GHC-Options: -Wall -fwarn-tabs -fwarn-incomplete-record-updates -rtsopts

  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind
