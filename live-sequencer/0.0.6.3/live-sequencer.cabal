Name:          live-sequencer
Version:       0.0.6.3
Author:        Henning Thielemann and Johannes Waldmann
Maintainer:    Henning Thielemann <haskell@henning-thielemann.de>, Johannes Waldmann <waldmann@imn.htwk-leipzig.de>
Category:      Sound, Music, GUI
License:       GPL
License-file:  LICENSE
Cabal-Version: >= 1.10
Tested-With:   GHC==8.2.2, GHC==9.6.3
Homepage:      http://www.haskell.org/haskellwiki/Live-Sequencer
Bug-Reports:   https://hub.darcs.net/thielema/livesequencer
Synopsis:      Live coding of MIDI music
Description:
   An editor shows a textual description of music (like Haskore),
   an interpreter computes and emits a stream of MIDI events,
   and (that's the main point) the user can change the program on the fly.
   Additionally the state of the interpreter is shown
   in the form of the current reduced term
   for educational and debugging purposes.

Build-Type: Simple

Data-Files:
  data/example/Introduction.hs
  data/example/Band.hs
  data/example/BandControlled.hs
  data/example/Finite.hs
  data/example/Klingklong.hs
  data/example/Simplesong.hs
  data/example/Sweep.hs
  data/example/UD.hs
  data/example/CrossSum.hs
  data/example/Fibonacci.hs
  data/example/DeBruijn.hs
  data/example/JohnsonTrotter.hs
  data/example/GrayCode.hs
  data/example/Pattern.hs

  data/base/Bool.hs
  data/base/Chord.hs
  data/base/Controller.hs
  data/base/Drum.hs
  data/base/Enum.hs
  data/base/Function.hs
  data/base/Instrument.hs
  data/base/Integer.hs
  data/base/List.hs
  data/base/ListLive.hs
  data/base/List/Basic.hs
  data/base/List/Advanced.hs
  data/base/Maybe.hs
  data/base/Midi.hs
  data/base/Music.hs
  data/base/Pitch.hs
  data/base/Render.hs
  data/base/Tuple.hs

  data/prelude/Prelude.hs

Extra-Source-Files:
  Changes.md
  README.md
  wxc.nix
  shell.nix
  http/enable/HTTPServer.hs
  http/enable/HTTPServer/GUI.hs
  http/enable/HTTPServer/Option.hs
  http/disable/HTTPServer/GUI.hs
  http/disable/HTTPServer/Option.hs

Source-Repository head
  Type: darcs
  Location: https://hub.darcs.net/thielema/livesequencer

Source-Repository this
  Tag: 0.0.6.3
  Type: darcs
  Location: https://hub.darcs.net/thielema/livesequencer

Flag gui
  Description: Build the wxWidgets GUI for the sequencer
  Default: True

Flag mplayer
  Description: Build the remote controller to the mplayer
  Default: True

Flag httpServer
  Description: Enable access to modules via a web browser
  Default: True


Library
  Default-Language: Haskell98
  GHC-Options: -Wall
  Hs-Source-Dirs: data/base, data/example
  Build-Depends:
    -- for Render
    midi >=0.2.1 && <0.3,
    non-negative >=0.0.6 && <0.2,
    event-list >=0.0.11 && <0.2,
    base >=4.10 && <5
  Exposed-Modules:
    Render
    Bool
    Chord
    Controller
    Drum
    Enum
    Function
    Instrument
    Integer
    ListLive
    Maybe
    Midi
    Music
    Pitch
    Tuple
  Other-Modules:
    List
    List.Advanced
    List.Basic
    Pattern
    Finite
    CrossSum
    DeBruijn
    Fibonacci
    JohnsonTrotter
    GrayCode


Executable live-sequencer
  Hs-Source-Dirs: src, http/disable
  Main-is: Console.hs
  Other-Modules:
    ALSA
    Event
    InOut
    Exception
    Module
    ModuleBase
    Option
    ControllerBase
    Program
    Rewrite
    Rule
    SourceText
    Term
    TermParser
    TermFocus
    Time
    Type
    Log
    HTTPServer.Option
    Paths_live_sequencer
  Other-Modules:
    Lilypond
  Default-Language: Haskell98
  GHC-Options: -Wall -threaded
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-unused-do-bind -fwarn-missing-import-lists
  Build-Depends:
    stm-split >=0.0 && <0.1,
    concurrent-split >=0.0 && <0.1,
    transformers >=0.2.2 && <0.7,
    explicit-exception >=0.1.9 && <0.3,
    parsec >=2.1 && <3.2,
    pretty >=1.0 && <1.2,
    midi-alsa >=0.2 && <0.3,
    midi >=0.2.1 && <0.3,
    alsa-seq >=0.6 && <0.7,
    alsa-core >=0.5 && <0.6,
    data-accessor-transformers >=0.2.1 && <0.3,
    data-accessor >=0.2.1 && <0.3,
    strict >=0.3.2 && <0.6,
    utility-ht >=0.0.8 && <0.1,
    non-empty >=0.2 && <0.4,
    semigroups >=0.1 && <1.0,
    containers >=0.3 && <0.8,
    bytestring >=0.9 && <0.13,
    shell-utility >=0.0 && <0.2,
    process >=1.0 && <1.7,
    pathtype >=0.8.1 && <0.9,
    base >=4.10 && <5

Executable live-sequencer-gui
  If flag(gui)
    Build-Depends:
      wx >=0.12.1 && <0.94,
      wxcore >=0.13.2 && <0.94,
      stm-split >=0.0 && <0.1,
      stm >=2.2 && <2.6,
      concurrent-split >=0.0 && <0.1,
      transformers >=0.2.2 && <0.7,
      explicit-exception >=0.1.9 && <0.3,
      parsec >=2.1 && <3.2,
      pretty >=1.0 && <1.2,
      midi-alsa >=0.2 && <0.3,
      midi >=0.2.1 && <0.3,
      alsa-seq >=0.6 && <0.7,
      alsa-core >=0.5 && <0.6,
      data-accessor-transformers >=0.2.1 && <0.3,
      data-accessor >=0.2.1 && <0.3,
      strict >=0.3.2 && <0.6,
      non-empty >=0.2 && <0.4,
      semigroups >=0.1 && <1.0,
      utility-ht >=0.0.8 && <0.1,
      containers >=0.3 && <0.8,
      bytestring >=0.9 && <0.13,
      shell-utility >=0.0 && <0.2,
      process >=1.0 && <1.7,
      pathtype >=0.8.1 && <0.9,
      base >=4.10 && <5
  Else
    Buildable: False

  Hs-Source-Dirs: src
  Main-is: GUI.hs
  Other-Modules:
    ALSA
    Event
    InOut
    Exception
    Module
    ModuleBase
    Option
    Controller
    ControllerBase
    Program
    Rewrite
    Rule
    SourceText
    Term
    TermParser
    TermFocus
    Time
    Type
    Log
    Paths_live_sequencer
    Utility.Concurrent
    Utility.WX

  Default-Language: Haskell98
  GHC-Options: -threaded -Wall
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-tabs -fwarn-incomplete-uni-patterns

  If flag(httpServer)
    Hs-Source-Dirs: http/enable
    Other-Modules:
      HTTPServer
    Build-Depends:
      httpd-shed >=0.4 && <0.5,
      network >=2.6 && <3.2,
      network-uri >= 2.6 && <2.8,
      network-uri-flag == 0.1.*,
      cgi >=3001.1 && <3001.6,
      html >=1.0 && <1.1
  Else
    Hs-Source-Dirs: http/disable

  Other-Modules:
    HTTPServer.Option
    HTTPServer.GUI

Executable live-mplayer-control
  If flag(mplayer)
    Build-Depends:
      shell-utility,
      pathtype,
      midi-alsa >=0.2 && <0.3,
      midi >=0.2 && <0.3,
      alsa-seq >=0.6 && <0.7,
      alsa-core >=0.5 && <0.6,
      unix >=2.4 && <2.9,
      transformers >=0.2.2 && <0.7,
      base >=4.10 && <5
  Else
    Buildable: False

  Hs-Source-Dirs: src
  Main-is: MPlayer.hs

  Default-Language: Haskell98
  GHC-Options: -threaded -Wall
  If impl(ghc>=7.0)
    GHC-Options: -fwarn-tabs -fwarn-incomplete-uni-patterns
