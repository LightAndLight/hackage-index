cabal-version:       2.4
name:                morley
version:             0.1.0.5
synopsis:            Developer tools for the Michelson Language
description:
  A library to make writing smart contracts in Michelson — the smart contract
  language of the Tezos blockchain — pleasant and effective.
homepage:            https://gitlab.com/camlcase-dev/morley
license:             AGPL-3.0-or-later
license-file:        LICENSE
author:              camlCase, Serokell
maintainer:          john@camlcase.io
copyright:           2018 camlCase, 2019 Tocqueville Group
category:            Language
build-type:          Simple
bug-reports:         https://issues.serokell.io/issues/TM
extra-doc-files:     CHANGES.md
                   , CONTRIBUTING.md
                   , README.md

source-repository head
  type:     git
  location: git@gitlab.com:camlcase-dev/morley.git

library
  hs-source-dirs:      src
  default-language:    Haskell2010
  exposed-modules:     Michelson.Interpret
                     , Michelson.TypeCheck
                     , Michelson.Typed
                     , Michelson.Typed.Value
                     , Michelson.Untyped
                     , Morley.Default
                     , Morley.Lexer
                     , Morley.Macro
                     , Morley.Ext
                     , Morley.Parser
                     , Morley.Parser.Annotations
                     , Morley.Parser.Helpers
                     , Morley.Runtime
                     , Morley.Runtime.GState
                     , Morley.Runtime.TxData
                     , Morley.Test
                     , Morley.Test.Dummy
                     , Morley.Test.Gen
                     , Morley.Test.Import
                     , Morley.Test.Integrational
                     , Morley.Test.Unit
                     , Morley.Test.Util
                     , Morley.Types
                     , Tezos.Address
                     , Tezos.Core
                     , Tezos.Crypto
  other-modules:
                       Michelson.EqParam
                     , Michelson.TypeCheck.Helpers
                     , Michelson.TypeCheck.Instr
                     , Michelson.TypeCheck.Types
                     , Michelson.TypeCheck.Value
                     , Michelson.Typed.Annotation
                     , Michelson.Typed.Arith
                     , Michelson.Typed.Convert
                     , Michelson.Typed.CValue
                     , Michelson.Typed.Extract
                     , Michelson.Typed.Instr
                     , Michelson.Typed.Polymorphic
                     , Michelson.Typed.Sing
                     , Michelson.Typed.T
                     , Michelson.Untyped.Aliases
                     , Michelson.Untyped.Annotation
                     , Michelson.Untyped.Contract
                     , Michelson.Untyped.Instr
                     , Michelson.Untyped.Type
                     , Michelson.Untyped.Value

  build-depends:       aeson
                     , aeson-options
                     , aeson-pretty
                     , base-noprelude >= 4.7 && < 5
                     , base16-bytestring
                     , base58-bytestring
                     , bifunctors
                     , bytestring
                     , containers
                     , cryptonite
                     , data-default
                     , fmt
                     , formatting
                     , hspec
                     , lens
                     , megaparsec >= 7.0.0
                     , memory
                     , morley-prelude
                     , named
                     , QuickCheck
                     , text
                     , time
                     , timerep
                     -- Otherwise Hackage fails
                     , transformers-compat == 0.6.2
                     , parser-combinators >= 1.0.0
                     , directory
                     , singletons
                     , mtl
                     , vinyl
  ghc-options:        -Weverything
                      -Wno-missing-exported-signatures
                      -Wno-missing-import-lists
                      -Wno-missed-specialisations
                      -Wno-all-missed-specialisations
                      -Wno-unsafe
                      -Wno-safe
                      -Wno-missing-local-signatures
                      -Wno-monomorphism-restriction
                      -Wno-implicit-prelude
  default-extensions:
                       ApplicativeDo
                       AllowAmbiguousTypes
                       BangPatterns
                       ConstraintKinds
                       DataKinds
                       DeriveFoldable
                       DeriveFunctor
                       DeriveGeneric
                       DeriveTraversable
                       EmptyCase
                       FlexibleContexts
                       FlexibleInstances
                       GADTs
                       GeneralizedNewtypeDeriving
                       LambdaCase
                       MonadFailDesugaring
                       MultiParamTypeClasses
                       MultiWayIf
                       NegativeLiterals
                       OverloadedLabels
                       OverloadedStrings
                       PatternSynonyms
                       PolyKinds
                       RankNTypes
                       RecordWildCards
                       RecursiveDo
                       ScopedTypeVariables
                       StandaloneDeriving
                       TemplateHaskell
                       TupleSections
                       TypeApplications
                       TypeFamilies
                       TypeOperators
                       UndecidableInstances
                       ViewPatterns

executable morley
  hs-source-dirs:      app
  main-is:             Main.hs
  autogen-modules:     Paths_morley
  other-modules:       Paths_morley
  default-language:    Haskell2010
  build-depends:       base-noprelude >= 4.7 && < 5
                     , fmt
                     , megaparsec >= 7.0.0
                     , morley
                     , morley-prelude
                     , named
                     , optparse-applicative
                     , pretty-simple
                     , text
  ghc-options:        -Weverything
                      -Wno-missing-exported-signatures
                      -Wno-missing-import-lists
                      -Wno-missed-specialisations
                      -Wno-all-missed-specialisations
                      -Wno-unsafe
                      -Wno-safe
                      -Wno-missing-local-signatures
                      -Wno-monomorphism-restriction
                      -Wno-implicit-prelude
  default-extensions:
                       ApplicativeDo
                       AllowAmbiguousTypes
                       BangPatterns
                       ConstraintKinds
                       DataKinds
                       DeriveFoldable
                       DeriveFunctor
                       DeriveGeneric
                       DeriveTraversable
                       EmptyCase
                       FlexibleContexts
                       FlexibleInstances
                       GADTs
                       GeneralizedNewtypeDeriving
                       LambdaCase
                       MonadFailDesugaring
                       MultiParamTypeClasses
                       MultiWayIf
                       NegativeLiterals
                       OverloadedLabels
                       OverloadedStrings
                       PatternSynonyms
                       PolyKinds
                       RankNTypes
                       RecordWildCards
                       RecursiveDo
                       ScopedTypeVariables
                       StandaloneDeriving
                       TemplateHaskell
                       TupleSections
                       TypeApplications
                       TypeFamilies
                       TypeOperators
                       UndecidableInstances
                       ViewPatterns

test-suite morley-test
  hs-source-dirs:      test
  main-is:             Spec.hs
  default-language:    Haskell2010
  type:                exitcode-stdio-1.0
  other-modules:       Test.Arbitrary
                     , Test.CValConversion
                     , Test.Interpreter
                     , Test.Interpreter.Auction
                     , Test.Interpreter.CallSelf
                     , Test.Interpreter.Compare
                     , Test.Interpreter.Conditionals
                     , Test.Interpreter.EnvironmentSpec
                     , Test.Interpreter.StringCaller
                     , Test.Macro
                     , Test.Ext
                     , Test.Morley.Runtime
                     , Test.Parser
                     , Test.Serialization.Aeson
                     , Test.Tezos.Address
                     , Test.Tezos.Crypto
                     , Test.Typecheck
                     , Test.Util.Contracts
                     , Test.Util.QuickCheck
                     , Test.ValConversion
  build-depends:
                       aeson
                     , base-noprelude >= 4.7 && < 5
                     , containers
                     , directory
                     , filepath
                     , fmt
                     , formatting
                     , hspec
                     , hspec-golden-aeson
                     , HUnit
                     , lens
                     , megaparsec >= 7.0.0
                     , morley
                     , morley-prelude
                     , QuickCheck
                     , quickcheck-arbitrary-adt
                     , quickcheck-instances
                     , text
                     , time
                     , universum
                     , vinyl
  ghc-options:        -Weverything
                      -Wno-missing-exported-signatures
                      -Wno-missing-import-lists
                      -Wno-missed-specialisations
                      -Wno-all-missed-specialisations
                      -Wno-unsafe
                      -Wno-safe
                      -Wno-missing-local-signatures
                      -Wno-monomorphism-restriction
                      -Wno-implicit-prelude
  default-extensions:
                       ApplicativeDo
                       AllowAmbiguousTypes
                       BangPatterns
                       ConstraintKinds
                       DataKinds
                       DeriveFoldable
                       DeriveFunctor
                       DeriveGeneric
                       DeriveTraversable
                       EmptyCase
                       FlexibleContexts
                       FlexibleInstances
                       GADTs
                       GeneralizedNewtypeDeriving
                       LambdaCase
                       MonadFailDesugaring
                       MultiParamTypeClasses
                       MultiWayIf
                       NegativeLiterals
                       OverloadedLabels
                       OverloadedStrings
                       PatternSynonyms
                       PolyKinds
                       RankNTypes
                       RecordWildCards
                       RecursiveDo
                       ScopedTypeVariables
                       StandaloneDeriving
                       TemplateHaskell
                       TupleSections
                       TypeApplications
                       TypeFamilies
                       TypeOperators
                       UndecidableInstances
                       ViewPatterns
