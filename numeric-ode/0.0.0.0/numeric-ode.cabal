name:                numeric-ode
version:             0.0.0.0
synopsis:            Ode solvers
description:         Some ode solvers, e.g., Störmer-Verlet
homepage:            https://github.com/qnikst/numeric-ode
license:             BSD3
license-file:        LICENSE
author:              Alexander V Vershilov, Dominic Steinitz
maintainer:          dominic@steinitz.org
copyright:           Alexander V Vershilov, Dominic Steinitz
category:            Math
build-type:          Simple
cabal-version:       >=1.18
extra-source-files: README.md, CHANGES.md, diagrams/*.svg
extra-doc-files: diagrams/*.svg

source-repository head
  type:     git
  location: https://github.com/qnikst/numeric-ode

library
  default-language: Haskell2010
  hs-source-dirs:   src
  exposed-modules:
    Math.Integrators
    Math.Integrators.ExplicitEuler
    Math.Integrators.ImplicitEuler
    Math.Integrators.ImplicitMidpointRule
    Math.Integrators.SympleticEuler
    Math.Integrators.StormerVerlet
    Math.Integrators.StormerVerletAlt
    Math.Integrators.RK
    Math.Integrators.Implicit
    Math.Integrators.Internal
    Math.Integrators.RK.Internal
    Math.Integrators.RK.Parser
    Math.Integrators.RK.Template
    Math.Integrators.RK.Types

  ghc-options:         -Wall
  -- other-modules:
  build-depends:       base>=4 && < 5,
                       vector>=0.9 && <1.1,
                       parallel>=3.2 && <3.3,
                       parsec  == 3.1.*,
                       template-haskell,
                       linear,
                       lens,
                       primitive>=0.4 && <0.7,
                       text,
                       protolude,
                       mwc-random,
                       mwc-probability,
                       primitive,
                       ad,
                       reflection,
                       tdigest,
                       -- chart-unit,
                       numhask,
                       foldl

  other-extensions:    TypeFamilies
                       FlexibleContexts
                       BangPatterns
                       QuasiQuotes

executable Kepler
  hs-source-dirs:      src/Examples
  main-is:             KeplerProblem.hs
  ghc-options:
  build-depends:       base,
                       numeric-ode,
                       vector>=0.9 && <1.0,
                       vector-space>=0.8 && <0.11,
                       colour,
                       linear,
                       data-default-class,
                       diagrams-lib,
                       diagrams-cairo,
                       Chart,
                       Chart-cairo,
                       data-accessor,
                       diagrams-rasterific,
                       diagrams-lib,
                       JuicyPixels,
                       plots,
                       mtl

  default-language:    Haskell2010

-- executable TestChart
--   hs-source-dirs:      src/Examples
--   main-is:             TestChart.hs
--   ghc-options:
--   build-depends:       base >= 4.7 && < 5,
--                        chart-unit,
--                        protolude,
--                        foldl,
--                        text,
--                        numhask,
--                        -- for data examples
--                        mwc-random,
--                        mwc-probability,
--                        primitive,
--                        ad,
--                        reflection,
--                        tdigest,
--                        diagrams-cairo,
--                        diagrams-lib,
--                        JuicyPixels
--   default-language:    Haskell2010

-- executable TestRasterific
--   hs-source-dirs:      src/Examples
--   main-is:             TestRasterific.hs
--   ghc-options:
--   build-depends:       base >= 4.7 && < 5,
--                        chart-unit,
--                        protolude,
--                        foldl,
--                        text,
--                        numhask,
--                        -- for data examples
--                        mwc-random,
--                        mwc-probability,
--                        primitive,
--                        ad,
--                        reflection,
--                        tdigest,
--                        diagrams-rasterific,
--                        diagrams-lib,
--                        JuicyPixels
--   default-language:    Haskell2010
