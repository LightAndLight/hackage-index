name: bitvec
version: 0.2.0.0
x-revision: 1
cabal-version: >=1.10
build-type: Simple
license: PublicDomain
license-file: LICENSE
maintainer: Andrew Lelechenko <andrew.lelechenko@gmail.com>
homepage: https://github.com/Bodigrim/bitvec
synopsis: Unboxed bit vectors
description:
  Bit vectors library for Haskell.

  The current @vector@ package represents unboxed arrays of @Bool@
  allocating one byte per boolean, which might be considered wasteful.
  This library provides a newtype wrapper @Bit@ and a custom instance
  of unboxed @Vector@, which packs booleans densely.
  It is a time-memory tradeoff: 8x less memory footprint
  at the price of moderate performance penalty
  (mostly, for random writes).
category: Data, Bit Vectors
author: James Cook <mokus@deepbondi.net>,
        Andrew Lelechenko <andrew.lelechenko@gmail.com>
tested-with: GHC ==8.0.2 GHC ==8.2.2 GHC ==8.4.3 GHC ==8.6.3
extra-source-files:
  changelog.md

source-repository head
  type: git
  location: git://github.com/Bodigrim/bitvec.git

library
  exposed-modules:
    Data.Bit
    Data.Vector.Unboxed.Bit
    Data.Vector.Unboxed.Mutable.Bit
  build-depends:
    base >=4.8 && <4.17,
    primitive -any,
    vector >=0.8
  default-language: Haskell2010
  hs-source-dirs: src
  other-modules:
    Data.Bit.Internal
    Data.Vector.Unboxed.Bit.Internal
  ghc-options: -fwarn-unused-imports -fwarn-unused-binds -fwarn-type-defaults

test-suite bitvec-tests
  type: exitcode-stdio-1.0
  main-is: Main.hs
  build-depends:
    base >=4.8 && <5,
    bitvec -any,
    HUnit -any,
    primitive -any,
    vector >=0.8,
    test-framework -any,
    test-framework-hunit -any,
    test-framework-quickcheck2 -any,
    QuickCheck >=2.10,
    quickcheck-classes >=0.6.1
  default-language: Haskell2010
  hs-source-dirs: test
  other-modules:
    Support
    Tests.MVector
    Tests.SetOps
    Tests.Vector
  ghc-options: -threaded -fwarn-unused-imports -fwarn-unused-binds
