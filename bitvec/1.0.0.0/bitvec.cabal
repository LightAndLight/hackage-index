name: bitvec
version: 1.0.0.0
x-revision: 1
cabal-version: >=1.10
build-type: Simple
license: BSD3
license-file: LICENSE
copyright: 2019 Andrew Lelechenko, 2012-2016 James Cook
maintainer: Andrew Lelechenko <andrew.lelechenko@gmail.com>
homepage: https://github.com/Bodigrim/bitvec
synopsis: Unboxed bit vectors
description:
  Bit vectors library for Haskell.
  .
  The current [vector](https://hackage.haskell.org/package/vector)
  package represents unboxed arrays of 'Bool'
  allocating one byte per boolean, which might be considered wasteful.
  This library provides a newtype wrapper 'Data.Bit.Bit' and a custom instance
  of unboxed 'Data.Vector.Unboxed.Vector', which packs booleans densely.
  It is a time-memory tradeoff: 8x less memory footprint
  at the price of moderate performance penalty
  (mostly, for random writes).
  .
  === Thread safety
  * "Data.Bit" is faster, but thread-unsafe. This is because
    naive updates are not atomic operations: read the whole word from memory,
    modify a bit, write the whole word back.
  * "Data.Bit.ThreadSafe" is slower (up to 2x), but thread-safe.
  .
  === Similar packages
  .
  * [bv](https://hackage.haskell.org/package/bv)
    and [bv-little](https://hackage.haskell.org/package/bv-little)
    offer only immutable size-polymorphic bit vectors.
    @bitvec@ provides an interface to mutable vectors as well.
  .
  * [array](https://hackage.haskell.org/package/array)
    is memory-efficient for 'Bool', but lacks
    a handy 'Vector' interface and is not thread-safe.

category: Data, Bit Vectors
author: Andrew Lelechenko <andrew.lelechenko@gmail.com>,
        James Cook <mokus@deepbondi.net>

tested-with: GHC ==7.10.3 GHC ==8.0.2 GHC ==8.2.2 GHC ==8.4.4 GHC ==8.6.5 GHC ==8.8.1
extra-source-files:
  changelog.md

source-repository head
  type: git
  location: git://github.com/Bodigrim/bitvec.git

flag bmi2
  description: Enable bmi2 instruction set
  manual: False
  default: False

library
  exposed-modules:
    Data.Bit
    Data.Bit.ThreadSafe
  build-depends:
    base >=4.8 && <5,
    ghc-prim,
    primitive >=0.5,
    vector >=0.11 && <0.13
  if (flag(bmi2)) && (impl(ghc >=8.4.1))
    build-depends:
      bits-extra >=0.0.0.4 && <0.1
  if impl(ghc <8.0)
    build-depends:
      semigroups >=0.8
  default-language: Haskell2010
  hs-source-dirs: src
  other-modules:
    Data.Bit.Immutable
    Data.Bit.ImmutableTS
    Data.Bit.Internal
    Data.Bit.InternalTS
    Data.Bit.Mutable
    Data.Bit.MutableTS
    Data.Bit.Select1
    Data.Bit.Utils
  ghc-options: -O2 -Wall
  include-dirs: src
  if (flag(bmi2)) && (impl(ghc >=8.4.1))
    ghc-options: -mbmi2 -msse4.2
    cpp-options: -DBMI2_ENABLED

test-suite bitvec-tests
  type: exitcode-stdio-1.0
  main-is: Main.hs
  build-depends:
    base >=4.8 && <5,
    bitvec,
    primitive >=0.5,
    quickcheck-classes >=0.6.1,
    vector >=0.11,
    tasty,
    tasty-hunit,
    tasty-quickcheck
  if impl(ghc <8.0)
    build-depends:
      semigroups >=0.8
  default-language: Haskell2010
  hs-source-dirs: test
  other-modules:
    Support
    Tests.MVector
    Tests.MVectorTS
    Tests.SetOps
    Tests.Vector
  ghc-options: -Wall
  include-dirs: test

benchmark gauge
  build-depends:
    base,
    bitvec,
    gauge,
    vector
  type: exitcode-stdio-1.0
  main-is: Bench.hs
  default-language: Haskell2010
  hs-source-dirs: bench
  ghc-options: -O2 -Wall
