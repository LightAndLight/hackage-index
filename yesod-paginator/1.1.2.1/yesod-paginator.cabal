cabal-version: 1.12
name:          yesod-paginator
version:       1.1.2.1
license:       MIT
license-file:  LICENSE
maintainer:    Patrick Brisbin <pbrisbin@gmail.com>
author:        Patrick Brisbin <pbrisbin@gmail.com>
homepage:      http://github.com/pbrisbin/yesod-paginator
synopsis:      A pagination approach for yesod
description:
    Paginate a list showing a per-item widget and links to other pages

category:      Web, Yesod
build-type:    Simple

flag examples
    description: Build the examples
    default:     False

library
    exposed-modules:
        Yesod.Paginator
        Yesod.Paginator.Pages
        Yesod.Paginator.Paginate
        Yesod.Paginator.PaginationConfig
        Yesod.Paginator.Prelude
        Yesod.Paginator.Widgets

    hs-source-dirs:     src
    other-modules:      Paths_yesod_paginator
    default-language:   Haskell2010
    default-extensions: NoImplicitPrelude
    ghc-options:
        -Weverything -Wno-all-missed-specialisations
        -Wno-missing-import-lists -Wno-safe -Wno-unsafe

    build-depends:
        base >=4.11.1.0 && <5,
        blaze-markup >=0.8.2.2,
        path-pieces >=0.2.1,
        persistent >=2.8.2,
        safe >=0.3.17,
        text >=1.2.3.1,
        transformers >=0.5.5.0,
        uri-encode >=1.5.0.5,
        yesod-core >=1.6.9

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

executable yesod-paginator-example
    main-is:            Main.hs
    hs-source-dirs:     example
    other-modules:      Paths_yesod_paginator
    default-language:   Haskell2010
    default-extensions: NoImplicitPrelude
    ghc-options:
        -Weverything -Wno-all-missed-specialisations
        -Wno-missing-import-lists -Wno-safe -Wno-unsafe

    build-depends:
        base >=4.11.1.0 && <5,
        warp >=3.2.25,
        yesod >=1.6.0,
        yesod-paginator -any

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

    if !flag(examples)
        buildable: False

test-suite doctests
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     doctest
    other-modules:      Paths_yesod_paginator
    default-language:   Haskell2010
    default-extensions: NoImplicitPrelude
    ghc-options:
        -Weverything -Wno-all-missed-specialisations
        -Wno-missing-import-lists -Wno-safe -Wno-unsafe

    build-depends:
        base >=4.11.1.0 && <5,
        doctest >=0.16.0.1

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

test-suite test
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    hs-source-dirs:     test
    other-modules:
        SpecHelper
        Yesod.Paginator.PagesSpec
        Yesod.Paginator.WidgetsSpec
        Paths_yesod_paginator

    default-language:   Haskell2010
    default-extensions: NoImplicitPrelude
    ghc-options:
        -Weverything -Wno-all-missed-specialisations
        -Wno-missing-import-lists -Wno-safe -Wno-unsafe

    build-depends:
        QuickCheck >=2.11.3,
        base >=4.11.1.0 && <5,
        hspec >=2.5.5,
        quickcheck-classes >=0.4.13,
        yesod-core >=1.6.9,
        yesod-paginator -any,
        yesod-test >=1.6.5.1

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

    if impl(ghc >=8.8)
        ghc-options: -Wno-missing-deriving-strategies
