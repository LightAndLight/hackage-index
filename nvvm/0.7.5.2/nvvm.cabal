name:                   nvvm
version:                0.7.5.2
synopsis:               FFI bindings to NVVM
description:
  The NVVM library compiles NVVM IR (a subset of LLVM IR) into PTX code which
  can then be executed on NVIDIA GPUs. In contrast to the standard NVPTX target
  built in to the LLVM toolchain, NVVM includes a set of proprietary
  optimisations which are otherwise only available by compiling CUDA code with
  the `nvcc` compiler.
  .
  The resulting PTX code can be loaded onto the GPU and executed using the
  'cuda' package:
  .
  <https://hackage.haskell.org/package/cuda>
  .
  The NVVM library is a compiler component available a part of the CUDA toolkit:
  .
  <https://developer.nvidia.com/cuda-toolkit>
  .
  The configure step will look for your CUDA installation in the standard
  places, and if the `nvcc` compiler is found in your `PATH`, relative to that.
  .
  This package tested with version 7.5 of the CUDA toolkit.
  .

license:                BSD3
license-file:           LICENSE
homepage:               https://github.com/tmcdonell/nvvm
author:                 Trevor L. McDonell
maintainer:             Trevor L. McDonell <tmcdonell@cse.unsw.edu.au>
copyright:              [2016] Trevor L. McDonell
category:               Foreign
build-type:             Custom
cabal-version:          >= 1.22

extra-source-files:
    CHANGELOG.md
    README.md
    cbits/stubs.h

extra-tmp-files:
    nvvm.buildinfo.generated

custom-setup
  setup-depends:
      base              >= 4.6
    , Cabal             >= 1.22
    , directory         >= 1.0
    , filepath          >= 1.0
    , template-haskell

library
  default-language:     Haskell2010
  include-dirs:         .
  ghc-options:          -Wall -O2

  exposed-modules:
      Foreign.NVVM
      Foreign.NVVM.Compile
      Foreign.NVVM.Error
      Foreign.NVVM.Info

  other-modules:
      Foreign.NVVM.Internal.C2HS

  build-depends:
      base              >= 4.6 && < 5
    , bytestring
    , cuda              >= 0.7
    , template-haskell

  build-tools:
      c2hs              >= 0.21


source-repository head
  type:                 git
  location:             https://github.com/tmcdonell/nvvm

source-repository this
  type:                 git
  location:             https://github.com/tmcdonell/nvvm
  tag:                  v0.7.5.2

-- vim: nospell

