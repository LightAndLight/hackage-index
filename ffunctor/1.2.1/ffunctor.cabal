cabal-version: 2.2
name:          ffunctor
version:       1.2.1
synopsis:      FFunctor typeclass
license:       BSD-3-Clause
license-file:  LICENSE
author:        Sam Halliday
maintainer:    Sam Halliday
copyright:     2019 Sam Halliday
bug-reports:   https://gitlab.com/fommil/ffunctor/merge_requests
tested-with:   GHC ^>=8.4.4 || ^>=8.6.5
category:      Constraints
description:
  Micro library with a Higher Kinded Functor in the spirit of HFunctor,
  MFunctor and MonadTrans (which all have different kindedness).
  .
  Useful to map over the type parameter in a record of functions
  .
  * https://www.benjamin.pizza/posts/2017-12-15-functor-functors.html
  * https://discourse.haskell.org/t/local-capabilities-with-mtl/231
  * https://discourse.haskell.org/t/some-limits-of-mtl-with-records-of-functions/576
  * https://discourse.haskell.org/t/records-of-functions-and-implicit-parameters/747

source-repository head
  type:     git
  location: https://gitlab.com/fommil/ffunctor

flag transformers
  description: Compile with transformers utilities
  manual:      True
  default:     True

common deps
  build-depends:    base >=4.11 && <5
  ghc-options:      -Wall -Werror=missing-home-modules
  default-language: Haskell2010

library
  import:          deps
  hs-source-dirs:  library

  -- cabal-fmt: expand library
  exposed-modules: Data.FFunctor

  if flag(transformers)
    build-depends: transformers
    cpp-options:   -DHAVE_TRANSFORMERS

test-suite tests
  import:             deps
  hs-source-dirs:     test
  type:               exitcode-stdio-1.0
  main-is:            Driver.hs

  -- cabal-fmt: expand test -Driver
  other-modules:
    Data.FFunctor.ServantTest
    Data.FFunctor.TracingTest

  build-depends:
    , aeson             ^>=1.4.1.0
    , exceptions        ^>=0.10.1
    , ffunctor
    , generic-lens      ^>=1.1.0.0
    , http-client       ^>=0.5.12
    , mtl               ^>=2.2.2
    , servant           ^>=0.14.1
    , servant-client    ^>=0.14
    , tasty             ^>=1.2.1
    , tasty-hspec       ^>=1.1.5
    , tasty-quickcheck  ^>=0.10
    , time              ^>=1.8.0.2
    , universum         ^>=1.5.0

  build-tool-depends: tasty-discover:tasty-discover ^>=4.2.1
  ghc-options:        -threaded
