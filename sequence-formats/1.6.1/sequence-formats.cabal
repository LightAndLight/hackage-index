cabal-version:      >=1.10
name:               sequence-formats
version:            1.6.1
x-revision: 1
license:            GPL-3
license-file:       LICENSE
maintainer:         stephan.schiffels@mac.com
author:             Stephan Schiffels
homepage:           https://github.com/stschiff/sequence-formats
bug-reports:        https://github.com/stschiff/sequence-formats/issues
synopsis:
    A package with basic parsing utilities for several Bioinformatic data formats.

description:
    Contains utilities to parse and write Eigenstrat, Fasta, FreqSum, VCF, Plink and other file formats used in population genetics analyses.

category:           Bioinformatics
build-type:         Simple
extra-source-files:
    README.md
    Changelog.md
    testDat/example.bim
    testDat/example.eigenstratgeno
    testDat/example.fasta
    testDat/example.freqsum
    testDat/example.histogram.txt
    testDat/example.ind
    testDat/example.snp
    testDat/example.vcf
    testDat/example.pileup
    testDat/example.fam
    testDat/example.plink.bed
    testDat/example.plink.fam
    testDat/example.plink.bim

library
    build-depends: base <4.15

    exposed-modules:
        SequenceFormats.RareAlleleHistogram
        SequenceFormats.FreqSum
        SequenceFormats.Fasta
        SequenceFormats.VCF
        SequenceFormats.Eigenstrat
        SequenceFormats.Plink
        SequenceFormats.Utils
        SequenceFormats.Pileup

    hs-source-dirs:   src
    default-language: Haskell2010
    build-depends:
        base >=4.7 && <5,
        containers >=0.6.2.1,
        errors >=2.3.0,
        attoparsec >=0.13.2.4,
        pipes >=4.3.14,
        transformers >=0.5.6.2,
        bytestring >=0.10.12.0,
        lens-family >=2.0.0,
        pipes-bytestring >=2.1.6,
        foldl >=1.4.10,
        exceptions >=0.10.4,
        pipes-safe >=2.3.2,
        pipes-attoparsec >=0.5.1.5,
        vector >=0.12.1.2

test-suite sequenceFormatTests
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   test
    other-modules:
        SequenceFormats.EigenstratSpec
        SequenceFormats.FastaSpec
        SequenceFormats.FreqSumSpec
        SequenceFormats.RareAlleleHistogramSpec
        SequenceFormats.UtilsSpec
        SequenceFormats.PlinkSpec
        SequenceFormats.VCFSpec
        SequenceFormats.PileupSpec

    default-language: Haskell2010
    build-depends:
        base >=4.14.1.0,
        sequence-formats -any,
        foldl >=1.4.10,
        pipes >=4.3.14,
        pipes-safe >=2.3.2,
        tasty >=1.2.3,
        vector >=0.12.1.2,
        transformers >=0.5.6.2,
        tasty-hunit >=0.10.0.3,
        bytestring >=0.10.12.0,
        containers >=0.6.2.1,
        hspec >=2.7.8
