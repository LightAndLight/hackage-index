name:                WEditor
version:             0.1.0.0
synopsis:            Generic text-editor logic for use with fixed-width fonts.

description:
  This package contains interfaces and implementations of text-editing idioms
  that can be wired up to UI libraries to create a text-editor widget.
  .
  The goal is to provide:
  .
  * Dynamic line wrapping that preserves the cursor and edit positions.
  * Customizable line-wrapping policies and word-hyphenation policies.
  * Automatic management of a fixed-size viewable area.
  * Support for novel character types.

homepage:            https://github.com/ta0kira/wrapping-editor
license:             Apache-2.0
license-file:        LICENSE
author:              Kevin P. Barry
maintainer:          ta0kira@gmail.com
category:            Text
build-type:          Simple

cabal-version:       2.0
tested-with:         GHC == 8.8.3

extra-source-files:  ChangeLog.md
extra-source-files:  test/testfiles/*.txt


library
  exposed-modules:     WEditor.Base,
                       WEditor.Base.Char,
                       WEditor.Base.Editor,
                       WEditor.Base.Line,
                       WEditor.Base.Para
                       WEditor.Base.Parser,
                       WEditor.Base.Viewer,
                       WEditor.Document,
                       WEditor.LineWrap

  other-modules:       WEditor.Internal.Line,
                       WEditor.Internal.Para

  build-depends:       base,
                       WEditor-internal

  default-language:    Haskell2010


library WEditor-internal
  exposed-modules:     WEditor.Base,
                       WEditor.Base.Char,
                       WEditor.Base.Editor,
                       WEditor.Base.Line,
                       WEditor.Base.Para
                       WEditor.Base.Parser,
                       WEditor.Base.Viewer,
                       WEditor.Document,
                       WEditor.LineWrap,
                       WEditor.Internal.Line,
                       WEditor.Internal.Para

  other-extensions:    ExistentialQuantification,
                       FlexibleContexts,
                       FlexibleInstances,
                       FunctionalDependencies,
                       MultiParamTypeClasses,
                       RankNTypes,
                       Safe,
                       TypeFamilies

  build-depends:       base >= 4.0 && < 4.14

  default-language:    Haskell2010


test-suite WEditor-test
  type:                exitcode-stdio-1.0

  main-is:             test.hs

  other-modules:       Common,
                       TestDocument,
                       TestLine,
                       TestLineWrap,
                       TestPara

  hs-source-dirs:      test

  build-depends:       base,
                       directory >= 1.0,
                       filepath >= 1.0,
                       WEditor-internal

  default-language:    Haskell2010
