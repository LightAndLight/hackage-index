name: partial-semigroup
version: 0.5.1.10
synopsis: A partial binary associative operator
category: Algebra

description:
  A partial semigroup is like a semigroup, but the operator
  is partial. We represent this in Haskell as a total function
  @(<>?) :: a -> a -> Maybe a@.
  .
  The
  <https://hackage.haskell.org/package/partial-semigroup-hedgehog partial-semigroup-hedgehog>
  companion package provides support for checking the partial
  semigroup associativity axiom using the
  <https://hackage.haskell.org/package/hedgehog hedgehog>
  package.

homepage:    https://github.com/chris-martin/partial-semigroup
bug-reports: https://github.com/chris-martin/partial-semigroup/issues

author:     Chris Martin <ch.martin@gmail.com>
maintainer: Chris Martin <ch.martin@gmail.com>

license: Apache-2.0
license-file: license.txt

build-type: Simple
cabal-version: >= 1.10

tested-with:
    GHC == 8.0.2
  , GHC == 8.2.2
  , GHC == 8.4.4
  , GHC == 8.6.3
  , GHC == 8.8.1
  , GHC == 8.10.1

extra-source-files:
    changelog.md

source-repository head
  type: git
  location: https://github.com/chris-martin/partial-semigroup

flag enable-hedgehog
  default: True
  description:
    Use the `hedgehog` package for tests.
    .
    Disabling this flag disables all of the tests that use `hedgehog`.

flag enable-doctest
  default: True
  description:
    Use the `doctest` package to test the code examples in Haddock comments.

library
  ghc-options: -Wall
  default-language: Haskell2010
  -- base version 4.9 introduced the Data.Semigroup and Data.List.NonEmpty modules.
  build-depends: base >=4.9 && <4.15
  hs-source-dirs: src

  exposed-modules:
      Data.PartialSemigroup
    , Data.PartialSemigroup.Generics

test-suite docs
  type: exitcode-stdio-1.0
  ghc-options: -Wall -threaded
  default-language: Haskell2010
  hs-source-dirs: test
  main-is: docs.hs
  build-depends: base >=4.9 && <4.15

  if flag(enable-doctest)
    build-depends: doctest >=0.11 && <0.18
    cpp-options: -DDOCTEST

test-suite examples
  type: exitcode-stdio-1.0
  ghc-options: -Wall -threaded
  default-language: Haskell2010
  hs-source-dirs: test
  main-is: examples.hs
  build-depends: base >=4.9 && <4.15, partial-semigroup

  if flag(enable-hedgehog)
    build-depends: hedgehog >=0.5 && <1.1
    cpp-options: -DHEDGEHOG

test-suite properties
  type: exitcode-stdio-1.0
  ghc-options: -Wall -threaded
  default-language: Haskell2010
  hs-source-dirs: test
  main-is: properties.hs
  build-depends: base >=4.9 && <4.15, partial-semigroup

  if flag(enable-hedgehog)
    build-depends: hedgehog >=0.5 && <1.1
    cpp-options: -DHEDGEHOG

  if flag(enable-hedgehog)
    hs-source-dirs: src-hedgehog
    other-modules: Test.PartialSemigroup.Hedgehog

test-suite generics
  type: exitcode-stdio-1.0
  ghc-options: -Wall -threaded
  default-language: Haskell2010
  hs-source-dirs: test
  main-is: generics.hs
  build-depends: base >=4.9 && <4.15, partial-semigroup

  if flag(enable-hedgehog)
    build-depends: hedgehog >=0.5 && <1.1
    cpp-options: -DHEDGEHOG

  if flag(enable-hedgehog)
    hs-source-dirs: src-hedgehog
    other-modules: Test.PartialSemigroup.Hedgehog
