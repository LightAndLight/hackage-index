cabal-version:      >=1.10
name:               powerdns
version:            0.2.2
license:            BSD3
license-file:       LICENSE
copyright:          (c) 2021 Victor Nawothnig
maintainer:         Victor Nawothnig (dminuoso@icloud.com)
author:             Victor Nawothnig, Julian Jacobi

bug-reports:        https://gitlab.com/wobcom/haskell/powerdns/issues
synopsis:           PowerDNS API bindings for api/v1
description:
    PowerDNS API allows programmatic manipulation of zones and other metadata. This
    library is a thin wrapper, exposing most of the idiosyncracies directly.
    .
    Users are advised to refer to the PowerDNS documentation and its source code for
    the exact semantics.
    .
    All exposed API endpoints are provided.
    .
    > import qualified PowerDNS.Client as P
    > import           Network.HTTP.Client (newManager, defaultManagerSettings)
    > import           Servant.Client (runClientM, mkClientEnv, parseBarseUrl)
    >
    > main :: IO
    > main = do
    >   uri <- parseBaseUrl "http://localhost:8081"
    >   mgr <- newManager defaultManagerSettings
    >   env <- P.applyXApiKey "secret" <$> mkClientEnv
    >
    >   Right r <- runClientM (P.statistics "localhost" Nothing Nothing) env)
    >   traverse_ print r

category:           Network
build-type:         Simple
extra-source-files: CHANGELOG.md

library
    exposed-modules:
        PowerDNS.API
        PowerDNS.API.Version
        PowerDNS.API.Zones
        PowerDNS.API.Servers
        PowerDNS.API.Cryptokeys
        PowerDNS.API.Metadata
        PowerDNS.API.TSIGKeys
        PowerDNS.Client
        PowerDNS.Internal.Utils

    default-language: Haskell2010
    ghc-options:      -Wall -Wcompat
    build-depends:
        base >=4.12 && <4.15,
        aeson >=1.5.6 && <1.6,
        bytestring >=0.10.10 && <0.11,
        deepseq >=1.4.4 && <1.5,
        containers >=0.6.2 && <0.7,
        text >=1.2.4 && <1.3,
        time >=1.9.3 && <1.10,
        base64-bytestring >=1.1.0 && <1.2,
        case-insensitive >=1.2.1 && <1.3,
        servant >=0.18.2 && <0.19,
        servant-client >=0.18.2 && <0.19,
        servant-client-core >=0.18.2 && <0.19

test-suite powerdns-test
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   spec
    default-language: Haskell2010
    build-depends:
        base >=4.12 && <4.15,
        powerdns -any,
        tasty >=1.4.1 && <1.5,
        tasty-hunit >=0.10.0.3 && <0.11,
        servant-client >=0.18.2 && <0.19,
        servant-client-core >=0.18.2 && <0.19,
        http-client >=0.6.4.1 && <0.7
