cabal-version:      3.0
name:               powerdns
version:            0.4.2
license:            BSD-3-Clause
license-file:       LICENSE
copyright:          (c) 2021 Victor Nawothnig
maintainer:         Victor Nawothnig (dminuoso@icloud.com)
author:             Victor Nawothnig, Julian Jacobi

bug-reports:        https://gitlab.com/wobcom/haskell/powerdns/issues
synopsis:           PowerDNS API bindings for api/v1
description:
    PowerDNS API allows programmatic manipulation of zones and other metadata. This
    library is a thin wrapper, exposing most of the idiosyncracies directly.

    Users are advised to refer to the PowerDNS documentation and its source code for
    the exact semantics.

    All exposed API endpoints are provided.

    > import qualified PowerDNS.Client as P
    > import           Network.HTTP.Client (newManager, defaultManagerSettings)
    > import           Servant.Client (runClientM, mkClientEnv, parseBarseUrl)
    >
    > main :: IO
    > main = do
    >   uri <- parseBaseUrl "http://localhost:8081"
    >   mgr <- newManager defaultManagerSettings
    >   let env = P.applyXApiKey "secret" (mkClientEnv mgr uri)
    >
    >   Right r <- runClientM (P.statistics "localhost" Nothing Nothing) env)
    >   traverse_ print r

category:           Network
build-type:         Simple
extra-source-files: CHANGELOG.md

common all
    build-depends:
        base ^>= { 4.12, 4.13, 4.14, 4.15, 4.16 },
        servant ^>= { 0.18.2, 0.19 },
        servant-client ^>= { 0.18.2, 0.19 },
        servant-client-core ^>= { 0.18.2, 0.19 }
library
    import:           all
    exposed-modules:
        PowerDNS.API
        PowerDNS.API.Version
        PowerDNS.API.Zones
        PowerDNS.API.Servers
        PowerDNS.API.Cryptokeys
        PowerDNS.API.Metadata
        PowerDNS.API.TSIGKeys
        PowerDNS.Client
        PowerDNS.Internal.Utils

    default-language: Haskell2010
    ghc-options:      -Wall -Wcompat
    build-depends:
        aeson ^>= { 1.5.6, 2.0.3.0 },
        bytestring ^>= { 0.10.10, 0.11.0 },
        deepseq ^>= { 1.4.4 },
        containers ^>= { 0.6.2 },
        hashable ^>= { 1.3.0.0 },
        text ^>= { 1.2.4 },
        time ^>= { 1.9.3 },
        base64-bytestring ^>= { 1.1.0, 1.2.1.0 },
        case-insensitive ^>= { 1.2.1 }

test-suite powerdns-test
    import:           all
    type:             exitcode-stdio-1.0
    main-is:          Spec.hs
    hs-source-dirs:   spec
    default-language: Haskell2010
    build-depends:
        powerdns -any,
        tasty ^>= { 1.4.1 },
        tasty-hunit ^>= { 0.10.0.3 },
        http-client ^>= { 0.6.4.1, 0.7.11 }
