Name:                   accelerate-io
Version:                1.0.0.1
x-revision: 1
Cabal-version:          >= 1.6
Tested-with:            GHC >= 7.8
Build-type:             Simple

Synopsis:               Read and write Accelerate arrays in various formats
Description:
  This package provides efficient conversion routines between a range of array
  types and Accelerate arrays.
  .
  Refer to the main /Accelerate/ package for more information:
  <http://hackage.haskell.org/package/accelerate>

License:                BSD3
License-file:           LICENSE
Author:                 Manuel M T Chakravarty,
                        Gabriele Keller,
                        Sean Seefried,
                        Trevor L. McDonell
Maintainer:             Trevor L. McDonell <tmcdonell@cse.unsw.edu.au>
Homepage:               https://github.com/AccelerateHS/accelerate-io
Bug-reports:            https://github.com/AccelerateHS/accelerate/issues

Category:               Compilers/Interpreters, Concurrency, Data, Parallelism
Stability:              Experimental

Extra-source-files:
    README.md
    CHANGELOG.md

Flag bounds-checks
  Description:          Enable bounds checking
  Default:              True

Flag unsafe-checks
  Description:          Enable bounds checking in unsafe operations
  Default:              False

Flag internal-checks
  Description:          Enable internal consistency checks
  Default:              False

Library
  Build-depends:
          base            >= 4.7 && < 4.11
        , accelerate      >= 1.0 && < 1.2
        , array           >= 0.3
        , bmp             >= 1.2
        , bytestring      >= 0.9
        , repa            >= 3.2
        , vector          >= 0.9

  Exposed-modules:
        Data.Array.Accelerate.IO

  Other-modules:
        Data.Array.Accelerate.IO.BlockCopy
        Data.Array.Accelerate.IO.BMP
        Data.Array.Accelerate.IO.ByteString
        Data.Array.Accelerate.IO.IArray
        Data.Array.Accelerate.IO.Ptr
        Data.Array.Accelerate.IO.Repa
        Data.Array.Accelerate.IO.Vector

  ghc-options:
        -O2
        -Wall
        -funbox-strict-fields

  if flag(bounds-checks)
    cpp-options:        -DACCELERATE_BOUNDS_CHECKS

  if flag(unsafe-checks)
    cpp-options:        -DACCELERATE_UNSAFE_CHECKS

  if flag(internal-checks)
    cpp-options:        -DACCELERATE_INTERNAL_CHECKS

  -- Don't add the extensions list here. Instead, place individual LANGUAGE
  -- pragmas in the files that require a specific extension. This means the
  -- project loads in GHCi, and avoids extension clashes.
  --
  -- Extensions:

Source-repository head
  Type:                 git
  Location:             git://github.com/AccelerateHS/accelerate-io.git

Source-repository this
  Type:                 git
  Tag:                  1.0.0.1
  Location:             git://github.com/AccelerateHS/accelerate-io.git

-- vim: nospell

