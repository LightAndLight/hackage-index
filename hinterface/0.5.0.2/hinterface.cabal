name: hinterface
version: 0.5.0.2
cabal-version: >=1.22
build-type: Simple
license: BSD3
license-file: LICENSE
copyright: 2016 Timo Koepke, Sven Heyll
maintainer: timo.koepke@googlemail.com, sven.heyll@gmail.com
homepage: https://github.com/LTI2000/hinterface
synopsis: Haskell / Erlang interoperability library
description:
    A library for building nodes of a distributed Erlang system in Haskell.
    Nodes can be created and registered to an epmd, Erlang terms can be
    marshalled to/from Erlangs binary term representation and message can
    be sent to or received from processes running on a different node.
    In it's preliminary state hinterface supports of Erlangs binary terms
    and a subset of the distribution protocol.
category: Language
author: Timo Koepke, Sven Heyll
extra-source-files:
    README.md
    stack.yaml
    .travis.yml
    Setup.hs
    .gitignore

source-repository head
    type: git
    location: https://github.com/LTI2000/hinterface.git

library
    exposed-modules:
        Util.IOExtra
        Util.BufferedIOx
        Util.Socket
        Network.BufferedSocket
        Util.Binary
        Util.FloatCast
        Foreign.Erlang.NodeState
        Foreign.Erlang.NodeData
        Foreign.Erlang.Epmd
        Foreign.Erlang.Digest
        Foreign.Erlang.Handshake
        Foreign.Erlang.Term
        Foreign.Erlang.LocalNode
        Foreign.Erlang.ControlMessage
        Foreign.Erlang.Mailbox
        Foreign.Erlang.Connection
    build-depends:
        QuickCheck >=2.9.2,
        array >=0.5.1.1,
        async >=2.1.1,
        base >=4.9 && <5,
        binary >=0.8.3.0,
        bytestring >=0.10.8.1,
        containers >=0.5.7.1,
        cryptonite >=0.21,
        exceptions >=0.8.3,
        lifted-async >=0.9.1.1,
        lifted-base >=0.2.3.10,
        memory >=0.14.2,
        monad-control >=1.0.1.0,
        monad-logger >=0.3.21,
        mtl >=2.2.1,
        network >=2.6.3.1,
        random >=1.1,
        resourcet >=1.1.9,
        safe-exceptions >=0.1.5.0,
        stm >=2.4.4.1,
        text >=1.2.2.1,
        transformers >=0.5.2.0,
        transformers-base >=0.4.4,
        vector >=0.11.0.0
    default-language: Haskell2010
    default-extensions: OverloadedStrings NamedFieldPuns
                        FlexibleContexts
    hs-source-dirs: src
    ghc-options: -Wall -O2 -funbox-strict-fields

test-suite hinterface-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    build-depends:
        QuickCheck >=2.9.2,
        async >=2.1.1,
        base >=4.9 && <5,
        binary >=0.8.3.0,
        bytestring >=0.10.8.1,
        hinterface >=0.5.0.2,
        hspec >=2.4.3,
        monad-logger >=0.3.21,
        transformers >=0.5.2.0
    default-language: Haskell2010
    default-extensions: OverloadedStrings NamedFieldPuns
    hs-source-dirs: test
    other-modules:
        Foreign.Erlang.NodeDataSpec
        Foreign.Erlang.HandshakeSpec
        Foreign.Erlang.ControlMessageSpec
        Foreign.Erlang.TermSpec
    ghc-options: -Wall -threaded -rtsopts -with-rtsopts=-N
