name: rtcm
version: 0.2.11
cabal-version: >=1.22
build-type: Simple
license: BSD3
copyright: Copyright (C) 2015-2017 Swift Navigation, Inc.
maintainer: Swift Navigation <dev@swift-nav.com>
homepage: http://github.com/swift-nav/librtcm
synopsis: RTCM Library.
description:
    Haskell bindings for Radio Technical Commission For Maritime
    Services (RTCM) standard, supporting GPS, GLONASS, Galileo and other
    satellite-based position systems operation with one reference
    station or a network.
category: Network
author: Mark Fine <mark@swift-nav.com>, Joshua Gross <josh@swift-nav.com>

source-repository head
    type: git
    location: https://github.com/swift-nav/librtcm

library
    exposed-modules:
        Data.CRC24Q
        Data.RTCM3
        Data.RTCM3.Antennas
        Data.RTCM3.Ephemerides
        Data.RTCM3.Internal
        Data.RTCM3.Observations
        Data.RTCM3.SSR
        Data.RTCM3.System
        Data.RTCM3.Types
    build-depends:
        aeson >=0.11.3.0,
        array >=0.5.1.0,
        base >=4.8 && <5,
        base64-bytestring >=1.0.0.1,
        basic-prelude >=0.5.2,
        binary >=0.7.5.0,
        binary-bits >=0.5,
        bytestring >=0.10.6.0,
        lens >=4.13,
        lens-aeson >=1.0.0.5,
        template-haskell >=2.10.0.0,
        text >=1.2.2.2,
        word24 >=2.0.1
    default-language: Haskell2010
    hs-source-dirs: src
    other-modules:
        Data.RTCM3.Extras
        Data.RTCM3.TH
    ghc-options: -Wall

executable rtcm32json
    main-is: rtcm32json.hs
    build-depends:
        aeson >=0.11.3.0,
        base >=4.8.2.0,
        basic-prelude >=0.5.2,
        binary-conduit >=1.2.4.1,
        bytestring >=0.10.6.0,
        conduit >=1.2.10,
        conduit-extra >=1.1.16,
        rtcm
    default-language: Haskell2010
    hs-source-dirs: main
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall

executable json2rtcm3
    main-is: json2rtcm3.hs
    build-depends:
        aeson >=0.11.3.0,
        base >=4.8.2.0,
        basic-prelude >=0.5.2,
        binary-conduit >=1.2.4.1,
        conduit >=1.2.10,
        conduit-extra >=1.1.16,
        rtcm
    default-language: Haskell2010
    hs-source-dirs: main
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall

test-suite test
    type: exitcode-stdio-1.0
    main-is: Test.hs
    build-depends:
        base >=4.8.2.0,
        basic-prelude >=0.5.2,
        binary >=0.7.5.0,
        binary-bits >=0.5,
        bytestring >=0.10.6.0,
        lens >=4.13,
        random >=1.1,
        rtcm,
        tasty >=0.11.2.1,
        tasty-hunit >=0.9.2,
        tasty-quickcheck >=0.8.4,
        word24 >=2.0.1
    default-language: Haskell2010
    hs-source-dirs: test
    other-modules:
        Test.Data.CRC24Q
        Test.Data.RTCM3.Antennas
        Test.Data.RTCM3.Ephemerides
        Test.Data.RTCM3.Extras
        Test.Data.RTCM3.Observations
        Test.Data.RTCM3.SSR
        Test.Data.RTCM3.System
        Test.Data.RTCM3.Test
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
