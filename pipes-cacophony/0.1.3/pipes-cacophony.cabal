name:          pipes-cacophony
version:       0.1.3
synopsis:      Pipes for Noise-secured network connections.
license:       PublicDomain
license-file:  LICENSE
author:        John Galt
maintainer:    jgalt@centromere.net
homepage:      https://github.com/centromere/pipes-cacophony
bug-reports:   https://github.com/centromere/pipes-cacophony/issues
category:      Pipes, Cryptography
build-type:    Simple
cabal-version: >=1.10
tested-with:   GHC == 7.10.1, GHC == 7.10.2, GHC == 7.10.3
description:
  A set of pipes to secure network connections with the
  <https://github.com/trevp/noise/blob/master/noise.md Noise> protocol.

extra-source-files:
  .travis.yml
  README.md
  changelog.md
  LICENSE
  tests/.hlint

source-repository head
  type: git
  location: https://github.com/centromere/pipes-cacophony.git

--------------------------------------------------------------------------------
-- FLAGS

flag hlint

flag build-examples
  description: Build example executables
  default: False
  manual: True

flag llvm
  default: False
  manual: True

--------------------------------------------------------------------------------
-- LIBRARY

library
  build-depends:
    base >=4.8 && <5,
    bytestring,
    cacophony,
    pipes
  hs-source-dirs:   src
  default-language: Haskell2010
  exposed-modules:
    Pipes.Noise
  ghc-options:      -rtsopts -with-rtsopts=-N -Wall -fwarn-tabs

  if flag(llvm)
    ghc-options: -fllvm

--------------------------------------------------------------------------------
-- EXAMPLES

executable echo-server
  default-language: Haskell2010
  hs-source-dirs:   examples/echo-server
  main-is:          Main.hs

  if flag(build-examples)
    build-depends:
      aeson,
      async,
      auto-update,
      base >=4.8 && <5,
      base64-bytestring,
      bytestring,
      cacophony,
      directory,
      fast-logger,
      pipes,
      pipes-aeson,
      pipes-cacophony,
      pipes-network,
      pipes-parse,
      text,
      unix,
      unix-time
  else
    buildable: False

  other-modules:
    Handshakes

  ghc-options:      -threaded -rtsopts -with-rtsopts=-N -Wall -fwarn-tabs

executable echo-client
  default-language: Haskell2010
  hs-source-dirs:   examples/echo-client
  main-is:          Main.hs

  if flag(build-examples)
    build-depends:
      aeson,
      async,
      base >=4.8 && <5,
      base64-bytestring,
      bytestring,
      cacophony,
      directory,
      pipes,
      pipes-aeson,
      pipes-bytestring,
      pipes-cacophony,
      pipes-network,
      pipes-parse,
      text
  else
    buildable: False

  other-modules:
    Handshakes

  ghc-options:      -threaded -rtsopts -with-rtsopts=-N -Wall -fwarn-tabs

--------------------------------------------------------------------------------
-- TESTS

test-suite properties
  type:             exitcode-stdio-1.0
  main-is:          properties.hs
  ghc-options:      -Wall -fwarn-tabs
  hs-source-dirs:   tests
  default-language: Haskell2010

  build-depends:
    async,
    base >=4.8 && <5,
    bytestring,
    cacophony,
    mtl,
    pipes,
    pipes-cacophony,
    QuickCheck,
    tasty,
    tasty-quickcheck

  other-modules:
    Handshakes,
    HandshakeStates,
    Imports,
    Instances

test-suite hlint
  type:             exitcode-stdio-1.0
  main-is:          hlint.hs
  ghc-options:      -Wall -fwarn-tabs
  hs-source-dirs:   tests
  default-language: Haskell2010

  if !flag(hlint)
    buildable: False
  else
    build-depends:
      base >=4.8 && <5,
      hlint
