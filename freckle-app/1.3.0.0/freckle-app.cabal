cabal-version:      1.18
name:               freckle-app
version:            1.3.0.0
license:            MIT
license-file:       LICENSE
maintainer:         Freckle Education
homepage:           https://github.com/freckle/freckle-app#readme
bug-reports:        https://github.com/freckle/freckle-app/issues
synopsis:           Haskell application toolkit used at Freckle
description:        Please see README.md
category:           Utils
build-type:         Simple
extra-source-files: package.yaml
extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/freckle/freckle-app

flag test-git
    description: Run tests that run git commands
    manual:      True

library
    exposed-modules:
        Freckle.App
        Freckle.App.Bugsnag
        Freckle.App.Database
        Freckle.App.Datadog
        Freckle.App.Datadog.Gauge
        Freckle.App.Datadog.Rts
        Freckle.App.Env
        Freckle.App.Ghci
        Freckle.App.GlobalCache
        Freckle.App.Http
        Freckle.App.Http.Paginate
        Freckle.App.Http.Retry
        Freckle.App.Memcached
        Freckle.App.Memcached.CacheKey
        Freckle.App.Memcached.CacheTTL
        Freckle.App.Memcached.Client
        Freckle.App.Memcached.Servers
        Freckle.App.Prelude
        Freckle.App.Scientist
        Freckle.App.Test
        Freckle.App.Test.DocTest
        Freckle.App.Test.Hspec.Runner
        Freckle.App.Test.Logging
        Freckle.App.Version
        Freckle.App.Wai
        Freckle.App.Yesod
        Freckle.App.Yesod.Routes
        Network.HTTP.Link.Compat
        Yesod.Core.Lens

    hs-source-dirs:     library
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        Blammo >=1.0.0.1,
        Glob >=0.10.1,
        MonadRandom >=0.5.2,
        aeson >=1.5.2.0,
        base >=4.13.0.0 && <5,
        bugsnag >=1.0.0.0,
        bytestring >=0.10.10.1,
        case-insensitive >=1.2.1.0,
        conduit >=1.3.4,
        containers >=0.6.2.1,
        datadog >=0.2.5.0,
        dlist >=0.8.0.8,
        doctest >=0.16.3,
        ekg-core >=0.1.1.7,
        envparse >=0.4.1,
        errors >=2.3.0,
        exceptions >=0.10.4,
        filepath >=1.4.2.1,
        hashable >=1.3.0.0,
        hspec >=2.8.1,
        hspec-core >=2.8.1,
        hspec-expectations-lifted >=0.10.0,
        hspec-junit-formatter >=1.1.0.1,
        http-client >=0.6.4.1,
        http-conduit >=2.3.5,
        http-link-header >=1.0.3.1,
        http-types >=0.12.3,
        immortal >=0.3,
        lens >=4.18.1,
        load-env >=0.2.1.0,
        memcache >=0.3.0.1,
        monad-control >=1.0.2.3,
        monad-logger >=0.3.31,
        mtl >=2.2.2,
        network-uri >=2.6.3.0,
        persistent >=2.10.5.3,
        persistent-postgresql >=2.10.1.2,
        postgresql-simple >=0.6.4,
        primitive >=0.7.0.1,
        resource-pool >=0.2.3.2,
        retry >=0.8.1.0,
        safe >=0.3.19,
        scientist >=0.0.0.0,
        semigroupoids >=5.3.4,
        template-haskell >=2.15.0.0,
        text >=1.2.4.0,
        time >=1.9.3,
        transformers >=0.5.6.2,
        transformers-base >=0.4.5.2,
        typed-process >=0.2.6.0,
        unliftio >=0.2.13.1,
        unliftio-core >=0.1.2.0,
        unordered-containers >=0.2.10.0,
        vector >=0.12.1.2,
        wai >=3.2.2.1,
        wai-extra >=3.0.32,
        yaml >=0.11.5.0,
        yesod-core >=1.6.18.8

test-suite doctest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     doctest
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        base >=4.13.0.0 && <5,
        freckle-app -any

test-suite gittest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     gittest
    other-modules:
        Freckle.App.VersionSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        base >=4.13.0.0 && <5,
        directory >=1.3.6.0,
        freckle-app -any,
        hspec >=2.8.1,
        process >=1.6.9.0,
        temporary >=1.3,
        text >=1.2.4.0,
        time >=1.9.3

    if flag(test-git)

    else
        buildable: False

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     tests
    other-modules:
        Freckle.App.BugsnagSpec
        Freckle.App.HttpSpec
        Freckle.App.Memcached.ServersSpec
        Freckle.App.MemcachedSpec
        Freckle.App.WaiSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:        -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        aeson >=1.5.2.0,
        base >=4.13.0.0 && <5,
        bytestring >=0.10.10.1,
        errors >=2.3.0,
        freckle-app -any,
        hspec >=2.8.1,
        http-types >=0.12.3,
        lens >=4.18.1,
        lens-aeson >=1.1,
        memcache >=0.3.0.1,
        mtl >=2.2.2,
        postgresql-simple >=0.6.4,
        wai >=3.2.2.1,
        wai-extra >=3.0.32
