cabal-version:      1.12
name:               freckle-app
version:            1.0.0.4
license:            MIT
license-file:       LICENSE
maintainer:         Freckle Education
homepage:           https://github.com/freckle/freckle-app#readme
bug-reports:        https://github.com/freckle/freckle-app/issues
synopsis:           Haskell application toolkit used at Freckle
description:        Please see README.md
category:           Utils
build-type:         Simple
extra-source-files:
    README.md
    CHANGELOG.md
    package.yaml

source-repository head
    type:     git
    location: https://github.com/freckle/freckle-app

flag test-git
    description: Run tests that run git commands
    manual:      True

library
    exposed-modules:
        Freckle.App
        Freckle.App.Database
        Freckle.App.Datadog
        Freckle.App.Env
        Freckle.App.Env.Internal
        Freckle.App.Ghci
        Freckle.App.GlobalCache
        Freckle.App.Http
        Freckle.App.Http.Paginate
        Freckle.App.Http.Retry
        Freckle.App.Logging
        Freckle.App.RIO
        Freckle.App.Test
        Freckle.App.Test.DocTest
        Freckle.App.Test.Hspec.Runner
        Freckle.App.Version
        Freckle.App.Wai
        Freckle.App.Yesod
        Network.HTTP.Link.Compat

    hs-source-dirs:     library
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        Glob >=0.10.1,
        MonadRandom >=0.5.3,
        aeson >=1.5.6.0,
        ansi-terminal >=0.11,
        base >=4.14.1.0 && <5,
        bytestring >=0.10.12.0,
        case-insensitive >=1.2.1.0,
        conduit >=1.3.4.1,
        data-default >=0.7.1.1,
        datadog >=0.2.5.0,
        doctest >=0.17,
        errors >=2.3.0,
        exceptions >=0.10.4,
        fast-logger >=3.0.5,
        filepath >=1.4.2.1,
        hspec >=2.7.10,
        hspec-core >=2.7.10,
        hspec-expectations-lifted >=0.10.0,
        hspec-junit-formatter >=1.0.0.5,
        http-client >=0.6.4.1,
        http-conduit >=2.3.8,
        http-link-header >=1.2.0,
        http-types >=0.12.3,
        immortal >=0.3,
        iproute >=1.7.11,
        lens >=4.19.2,
        load-env >=0.2.1.0,
        monad-control >=1.0.2.3,
        monad-logger >=0.3.36,
        mtl >=2.2.2,
        network >=3.1.1.1,
        network-uri >=2.6.4.1,
        persistent >=2.13.1.1,
        persistent-postgresql >=2.13.0.3,
        postgresql-simple >=0.6.4,
        primitive >=0.7.1.0,
        process >=1.6.9.0,
        resource-pool >=0.2.3.2,
        retry >=0.8.1.2,
        rio >=0.1.20.0,
        text >=1.2.4.1,
        time >=1.9.3,
        transformers >=0.5.6.2,
        transformers-base >=0.4.5.2,
        unliftio >=0.2.18,
        unliftio-core >=0.2.0.1,
        wai >=3.2.3,
        wai-extra >=3.1.6,
        yaml >=0.11.5.0,
        yesod >=1.6.1.2,
        yesod-core >=1.6.20.2

test-suite doctest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     doctest
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        base >=4.14.1.0 && <5,
        freckle-app -any

test-suite gittest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     gittest
    other-modules:
        Freckle.App.VersionSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        base >=4.14.1.0 && <5,
        directory >=1.3.6.0,
        freckle-app -any,
        hspec >=2.7.10,
        process >=1.6.9.0,
        temporary >=1.3,
        text >=1.2.4.1,
        time >=1.9.3

    if flag(test-git)

    else
        buildable: False

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     tests
    other-modules:
        Freckle.App.Env.InternalSpec
        Freckle.App.HttpSpec
        Freckle.App.WaiSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:        -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        aeson >=1.5.6.0,
        base >=4.14.1.0 && <5,
        bytestring >=0.10.12.0,
        freckle-app -any,
        hspec >=2.7.10,
        http-types >=0.12.3,
        lens >=4.19.2,
        lens-aeson >=1.1.1,
        wai >=3.2.3,
        wai-extra >=3.1.6
