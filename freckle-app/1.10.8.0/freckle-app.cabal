cabal-version:      1.18
name:               freckle-app
version:            1.10.8.0
license:            MIT
license-file:       LICENSE
maintainer:         Freckle Education
homepage:           https://github.com/freckle/freckle-app#readme
bug-reports:        https://github.com/freckle/freckle-app/issues
synopsis:           Haskell application toolkit used at Freckle
description:        Please see README.md
category:           Utils
build-type:         Simple
extra-source-files: package.yaml
extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/freckle/freckle-app

library
    exposed-modules:
        Configuration.Dotenv.Compat
        Freckle.App
        Freckle.App.Aeson
        Freckle.App.Async
        Freckle.App.Bugsnag
        Freckle.App.Bugsnag.CallStack
        Freckle.App.Bugsnag.HttpException
        Freckle.App.Bugsnag.MetaData
        Freckle.App.Bugsnag.SqlError
        Freckle.App.Csv
        Freckle.App.Database
        Freckle.App.Database.XRay
        Freckle.App.Dotenv
        Freckle.App.Ecs
        Freckle.App.Env
        Freckle.App.Exception
        Freckle.App.Exception.MonadThrow
        Freckle.App.Exception.MonadUnliftIO
        Freckle.App.Exception.Types
        Freckle.App.Ghci
        Freckle.App.GlobalCache
        Freckle.App.Http
        Freckle.App.Http.Paginate
        Freckle.App.Http.Retry
        Freckle.App.Kafka
        Freckle.App.Kafka.Consumer
        Freckle.App.Kafka.Producer
        Freckle.App.Memcached
        Freckle.App.Memcached.CacheKey
        Freckle.App.Memcached.CacheTTL
        Freckle.App.Memcached.Client
        Freckle.App.Memcached.MD5
        Freckle.App.Memcached.Servers
        Freckle.App.OpenTelemetry
        Freckle.App.Prelude
        Freckle.App.Random
        Freckle.App.Scientist
        Freckle.App.Stats
        Freckle.App.Stats.Rts
        Freckle.App.Test
        Freckle.App.Test.DocTest
        Freckle.App.Test.Hspec.Runner
        Freckle.App.Test.Properties.JSON
        Freckle.App.Test.Properties.PathPiece
        Freckle.App.Test.Yesod
        Freckle.App.Wai
        Freckle.App.Yesod
        Freckle.App.Yesod.Routes
        Network.HTTP.Link.Compat
        Network.Wai.Middleware.Cors
        Network.Wai.Middleware.OpenTelemetry
        Network.Wai.Middleware.Stats
        Yesod.Core.Lens

    hs-source-dirs:     library
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -Weverything -Wno-all-missed-specialisations
        -Wno-missing-exported-signatures -Wno-missing-import-lists
        -Wno-missing-local-signatures -Wno-monomorphism-restriction
        -Wno-safe -Wno-unsafe

    build-depends:
        Blammo >=1.0.2.2,
        Glob >=0.10.2,
        MonadRandom >=0.5.3,
        aeson >=1.5.6.0,
        annotated-exception >=0.2.0.5,
        aws-xray-client-persistent >=0.1.0.5,
        aws-xray-client-wai >=0.1.0.2,
        base >=4.14.3.0,
        bcp47 >=0.2.0.6,
        bugsnag >=1.0.0.0,
        bytestring >=0.10.12.0,
        case-insensitive >=1.2.1.0,
        cassava >=0.5.2.0,
        conduit >=1.3.4.2,
        conduit-extra >=1.3.5,
        containers >=0.6.5.1,
        cookie >=0.4.5,
        datadog >=0.2.5.0,
        doctest >=0.17,
        dotenv >=0.8.0.7,
        ekg-core >=0.1.1.7,
        envparse >=0.5.0,
        errors >=2.3.0,
        exceptions >=0.10.4,
        extra >=1.7.9,
        filepath >=1.4.2.1,
        hashable >=1.3.0.0,
        hs-opentelemetry-api >=0.1.0.0,
        hs-opentelemetry-instrumentation-persistent >=0.1.0.0,
        hs-opentelemetry-instrumentation-wai >=0.1.0.0,
        hs-opentelemetry-propagator-datadog >=0.0.0.0,
        hs-opentelemetry-sdk >=0.0.3.6,
        hspec >=2.8.1,
        hspec-core >=2.8.1,
        hspec-expectations-lifted >=0.10.0,
        hspec-junit-formatter >=1.1.0.1,
        http-client >=0.6.4.1,
        http-conduit >=2.3.5,
        http-link-header >=1.2.1,
        http-types >=0.12.3,
        hw-kafka-client >=4.0.3 && <5.0.0,
        immortal >=0.3,
        lens >=4.19.2,
        memcache >=0.3.0.1,
        monad-control >=1.0.3.1,
        monad-logger-aeson >=0.3.0.2,
        monad-validate >=1.2.0.1,
        mtl >=2.2.2,
        network-uri >=2.6.4.1,
        nonempty-containers >=0.3.4.4,
        path-pieces >=0.2.1,
        persistent >=2.13.3.0,
        persistent-postgresql >=2.13.4.0,
        postgresql-simple >=0.6.4,
        primitive >=0.7.3.0,
        pureMD5 >=2.1.4,
        resource-pool >=0.4.0.0,
        resourcet >=1.2.4.3,
        retry >=0.8.1.0,
        safe >=0.3.19,
        scientist >=0.0.0.0,
        semigroupoids >=5.3.7,
        serialise >=0.2.4.0,
        template-haskell >=2.16.0.0,
        text >=1.2.4.1,
        time >=1.9.3,
        transformers >=0.5.6.2,
        transformers-base >=0.4.6,
        typed-process >=0.2.8.0,
        unliftio >=0.2.21.0,
        unliftio-core >=0.2.0.1,
        unordered-containers >=0.2.16.0,
        vector >=0.12.3.1,
        wai >=3.2.3,
        wai-extra >=3.1.8,
        yaml >=0.11.8.0,
        yesod-core >=1.6.21.0,
        yesod-test >=1.6.12

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

test-suite doctest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     doctest
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -Weverything -Wno-all-missed-specialisations
        -Wno-missing-exported-signatures -Wno-missing-import-lists
        -Wno-missing-local-signatures -Wno-monomorphism-restriction
        -Wno-safe -Wno-unsafe

    build-depends:
        base >=4.14.3.0 && <5,
        freckle-app

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     tests
    other-modules:
        Freckle.App.Bugsnag.MetaDataSpec
        Freckle.App.BugsnagSpec
        Freckle.App.CsvSpec
        Freckle.App.HttpSpec
        Freckle.App.Memcached.ServersSpec
        Freckle.App.MemcachedSpec
        Freckle.App.Test.Properties.JSONSpec
        Freckle.App.Test.Properties.PathPieceSpec
        Freckle.App.WaiSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -Weverything -Wno-all-missed-specialisations
        -Wno-missing-exported-signatures -Wno-missing-import-lists
        -Wno-missing-local-signatures -Wno-monomorphism-restriction
        -Wno-safe -Wno-unsafe -threaded -rtsopts -with-rtsopts=-N

    build-depends:
        Blammo >=1.0.2.2,
        QuickCheck >=2.14.2,
        aeson >=1.5.6.0,
        base >=4.14.3.0 && <5,
        bugsnag >=1.0.0.0,
        bytestring >=0.10.12.0,
        cassava >=0.5.2.0,
        conduit >=1.3.4.2,
        errors >=2.3.0,
        freckle-app,
        hspec >=2.9.3,
        http-types >=0.12.3,
        lens >=4.19.2,
        lens-aeson >=1.1.3,
        memcache >=0.3.0.1,
        monad-validate >=1.2.0.1,
        nonempty-containers >=0.3.4.4,
        postgresql-simple >=0.6.4,
        text >=1.2.4.1,
        vector >=0.12.3.1,
        wai >=3.2.3,
        wai-extra >=3.1.8

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info
