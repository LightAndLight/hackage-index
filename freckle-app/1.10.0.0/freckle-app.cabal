cabal-version:      1.18
name:               freckle-app
version:            1.10.0.0
x-revision: 1
license:            MIT
license-file:       LICENSE
maintainer:         Freckle Education
homepage:           https://github.com/freckle/freckle-app#readme
bug-reports:        https://github.com/freckle/freckle-app/issues
synopsis:           Haskell application toolkit used at Freckle
description:        Please see README.md
category:           Utils
build-type:         Simple
extra-source-files: package.yaml
extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/freckle/freckle-app

library
    build-depends: yesod-core >=1.6.9

    build-depends: yaml >=0.8.32

    build-depends: wai-extra >=3.0.29

    build-depends: wai >=3.2.1.2

    build-depends: vector >=0.12.0.2

    build-depends: unordered-containers >=0.2.10.0

    build-depends: unliftio-core >=0.1.2.0

    build-depends: unliftio >=0.2.9.0

    build-depends: typed-process >=0.2.3.0

    build-depends: transformers-base >=0.4.5.2

    build-depends: transformers >=0.5.5.0

    build-depends: time >=1.8.0.2

    build-depends: text >=1.2.3.1

    build-depends: template-haskell >=2.13.0.0

    build-depends: semigroupoids >=5.2.2

    build-depends: scientist >=0.0.0.0

    build-depends: safe >=0.3.17

    build-depends: primitive >=0.7.0.1

    build-depends: postgresql-simple >=0.6.2

    build-depends: persistent-postgresql >=2.9.0

    build-depends: persistent >=2.9.0

    build-depends: path-pieces >=0.2.1

    build-depends: network-uri >=2.6.1.0

    build-depends: mtl >=2.2.2

    build-depends: monad-control >=1.0.2.3

    build-depends: memcache >=0.3.0.1

    build-depends: lens >=4.16.1

    build-depends: immortal >=0.3

    build-depends: http-types >=0.12.2

    build-depends: http-link-header >=1.0.3.1

    build-depends: http-client >=0.5.14

    build-depends: hspec-expectations-lifted >=0.10.0

    build-depends: hashable >=1.2.7.0

    build-depends: filepath >=1.4.2

    build-depends: extra >=1.6.14

    build-depends: exceptions >=0.10.0

    build-depends: errors >=2.3.0

    build-depends: envparse >=0.5.0

    build-depends: ekg-core >=0.1.1.7

    build-depends: dotenv >=0.5.2.5

    build-depends: doctest >=0.16.0.1

    build-depends: datadog >=0.2.3.0

    build-depends: containers >=0.5.11.0

    build-depends: conduit >=1.3.1

    build-depends: case-insensitive >=1.2.0.11

    build-depends: bytestring >=0.10.8.2

    build-depends: bugsnag >=1.0.0.1

    build-depends: base >=4.14.3.0 && <5

    build-depends: aws-xray-client-wai >=0.1.0.2

    build-depends: aws-xray-client-persistent >=0.1.0.5

    build-depends: aeson >=1.5.2.0

    build-depends: MonadRandom >=0.5.1.1

    build-depends: Glob >=0.9.3

    build-depends: Blammo >=1.0.2.2

    exposed-modules:
        Configuration.Dotenv.Compat
        Freckle.App
        Freckle.App.Aeson
        Freckle.App.Async
        Freckle.App.Bugsnag
        Freckle.App.Bugsnag.MetaData
        Freckle.App.Csv
        Freckle.App.Database
        Freckle.App.Database.XRay
        Freckle.App.Dotenv
        Freckle.App.Ecs
        Freckle.App.Env
        Freckle.App.Ghci
        Freckle.App.GlobalCache
        Freckle.App.Http
        Freckle.App.Http.Paginate
        Freckle.App.Http.Retry
        Freckle.App.Kafka
        Freckle.App.Kafka.Consumer
        Freckle.App.Kafka.Producer
        Freckle.App.Memcached
        Freckle.App.Memcached.CacheKey
        Freckle.App.Memcached.CacheTTL
        Freckle.App.Memcached.Client
        Freckle.App.Memcached.Servers
        Freckle.App.OpenTelemetry
        Freckle.App.Prelude
        Freckle.App.Scientist
        Freckle.App.Stats
        Freckle.App.Stats.Rts
        Freckle.App.Test
        Freckle.App.Test.DocTest
        Freckle.App.Test.Hspec.Runner
        Freckle.App.Test.Properties.JSON
        Freckle.App.Test.Properties.PathPiece
        Freckle.App.Test.Yesod
        Freckle.App.Wai
        Freckle.App.Yesod
        Freckle.App.Yesod.Routes
        Network.HTTP.Link.Compat
        Network.Wai.Middleware.Cors
        Network.Wai.Middleware.OpenTelemetry
        Network.Wai.Middleware.Stats
        Yesod.Core.Lens

    hs-source-dirs:     library
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -Weverything -Wno-all-missed-specialisations
        -Wno-missing-exported-signatures -Wno-missing-import-lists
        -Wno-missing-local-signatures -Wno-monomorphism-restriction
        -Wno-safe -Wno-unsafe

    build-depends:
        Blammo,
        Glob,
        MonadRandom,
        aeson,
        aws-xray-client-persistent,
        aws-xray-client-wai,
        base,
        bcp47,
        bugsnag,
        bytestring,
        case-insensitive,
        cassava,
        conduit,
        conduit-extra,
        containers,
        cookie,
        datadog,
        doctest,
        dotenv,
        ekg-core,
        envparse,
        errors,
        exceptions,
        extra,
        filepath,
        hashable,
        hs-opentelemetry-api,
        hs-opentelemetry-instrumentation-persistent,
        hs-opentelemetry-instrumentation-wai,
        hs-opentelemetry-propagator-datadog,
        hs-opentelemetry-sdk,
        hspec >=2.8.1,
        hspec-core >=2.8.1,
        hspec-expectations-lifted,
        hspec-junit-formatter >=1.1.0.1,
        http-client,
        http-conduit >=2.3.5,
        http-link-header,
        http-types,
        hw-kafka-client >=4.0.3 && <5.0.0,
        immortal,
        lens,
        memcache,
        monad-control,
        monad-validate,
        mtl,
        network-uri,
        nonempty-containers,
        path-pieces,
        persistent,
        persistent-postgresql,
        postgresql-simple,
        primitive,
        resource-pool >=0.4.0.0,
        resourcet,
        retry >=0.8.1.0,
        safe,
        scientist,
        semigroupoids,
        template-haskell,
        text,
        time,
        transformers,
        transformers-base,
        typed-process,
        unliftio,
        unliftio-core,
        unordered-containers,
        vector,
        wai,
        wai-extra,
        yaml,
        yesod-core,
        yesod-test

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

test-suite doctest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     doctest
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -Weverything -Wno-all-missed-specialisations
        -Wno-missing-exported-signatures -Wno-missing-import-lists
        -Wno-missing-local-signatures -Wno-monomorphism-restriction
        -Wno-safe -Wno-unsafe

    build-depends:
        base >=4.14.3.0 && <5,
        freckle-app

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     tests
    other-modules:
        Freckle.App.Bugsnag.MetaDataSpec
        Freckle.App.BugsnagSpec
        Freckle.App.CsvSpec
        Freckle.App.HttpSpec
        Freckle.App.Memcached.ServersSpec
        Freckle.App.MemcachedSpec
        Freckle.App.Test.Properties.JSONSpec
        Freckle.App.Test.Properties.PathPieceSpec
        Freckle.App.WaiSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -Weverything -Wno-all-missed-specialisations
        -Wno-missing-exported-signatures -Wno-missing-import-lists
        -Wno-missing-local-signatures -Wno-monomorphism-restriction
        -Wno-safe -Wno-unsafe -threaded -rtsopts -with-rtsopts=-N

    build-depends:
        Blammo,
        QuickCheck,
        aeson,
        base >=4.14.3.0 && <5,
        bugsnag,
        bytestring,
        cassava,
        conduit,
        errors,
        freckle-app,
        hspec,
        http-types,
        lens,
        lens-aeson,
        memcache,
        monad-validate,
        nonempty-containers,
        postgresql-simple,
        unliftio,
        vector,
        wai,
        wai-extra

    if impl(ghc >=9.2)
        ghc-options: -Wno-missing-kind-signatures

    if impl(ghc >=8.10)
        ghc-options:
            -Wno-missing-safe-haskell-mode -Wno-prepositive-qualified-module

    if impl(ghc >=8.8)
        ghc-options: -fwrite-ide-info
