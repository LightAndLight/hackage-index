cabal-version:      1.18
name:               freckle-app
version:            1.5.1.0
license:            MIT
license-file:       LICENSE
maintainer:         Freckle Education
homepage:           https://github.com/freckle/freckle-app#readme
bug-reports:        https://github.com/freckle/freckle-app/issues
synopsis:           Haskell application toolkit used at Freckle
description:        Please see README.md
category:           Utils
build-type:         Simple
extra-source-files: package.yaml
extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/freckle/freckle-app

flag test-git
    description: Run tests that run git commands
    manual:      True

library
    exposed-modules:
        Freckle.App
        Freckle.App.Bugsnag
        Freckle.App.Database
        Freckle.App.Ecs
        Freckle.App.Env
        Freckle.App.Ghci
        Freckle.App.GlobalCache
        Freckle.App.Http
        Freckle.App.Http.Paginate
        Freckle.App.Http.Retry
        Freckle.App.Memcached
        Freckle.App.Memcached.CacheKey
        Freckle.App.Memcached.CacheTTL
        Freckle.App.Memcached.Client
        Freckle.App.Memcached.Servers
        Freckle.App.Prelude
        Freckle.App.Scientist
        Freckle.App.Stats
        Freckle.App.Stats.Gauge
        Freckle.App.Stats.Rts
        Freckle.App.Test
        Freckle.App.Test.DocTest
        Freckle.App.Test.Hspec.Runner
        Freckle.App.Test.Properties.JSON
        Freckle.App.Test.Properties.PathPiece
        Freckle.App.Version
        Freckle.App.Wai
        Freckle.App.Yesod
        Freckle.App.Yesod.Routes
        Network.HTTP.Link.Compat
        Network.Wai.Middleware.Stats
        Yesod.Core.Lens

    hs-source-dirs:     library
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        Blammo >=1.0.2.2,
        Glob >=0.9.3,
        MonadRandom >=0.5.1.1,
        aeson >=1.5.2.0,
        base >=4.11.1.0 && <5,
        bugsnag >=1.0.0.1,
        bytestring >=0.10.8.2,
        case-insensitive >=1.2.0.11,
        conduit >=1.3.1,
        containers >=0.5.11.0,
        datadog >=0.2.3.0,
        doctest >=0.16.0.1,
        ekg-core >=0.1.1.7,
        envparse >=0.5.0,
        errors >=2.3.0,
        exceptions >=0.10.0,
        extra >=1.6.14,
        filepath >=1.4.2,
        hashable >=1.2.7.0,
        hspec >=2.8.1,
        hspec-core >=2.8.1,
        hspec-expectations-lifted >=0.10.0,
        hspec-junit-formatter >=1.1.0.1,
        http-client >=0.5.14,
        http-conduit >=2.3.5,
        http-link-header >=1.0.3.1,
        http-types >=0.12.2,
        immortal >=0.3,
        lens >=4.16.1,
        load-env >=0.2.0.2,
        memcache >=0.3.0.1,
        monad-control >=1.0.2.3,
        mtl >=2.2.2,
        network-uri >=2.6.1.0,
        path-pieces >=0.2.1,
        persistent >=2.9.0,
        persistent-postgresql >=2.9.0,
        postgresql-simple >=0.6.2,
        primitive >=0.7.0.1,
        resource-pool >=0.2.3.2,
        retry >=0.8.1.0,
        safe >=0.3.17,
        scientist >=0.0.0.0,
        semigroupoids >=5.2.2,
        template-haskell >=2.13.0.0,
        text >=1.2.3.1,
        time >=1.8.0.2,
        transformers >=0.5.5.0,
        transformers-base >=0.4.5.2,
        typed-process >=0.2.3.0,
        unliftio >=0.2.9.0,
        unliftio-core >=0.1.2.0,
        unordered-containers >=0.2.10.0,
        vector >=0.12.0.2,
        wai >=3.2.1.2,
        wai-extra >=3.0.29,
        yaml >=0.8.32,
        yesod-core >=1.6.9

test-suite doctest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     doctest
    other-modules:      Paths_freckle_app
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        base >=4.11.1.0 && <5,
        freckle-app -any

test-suite gittest
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     gittest
    other-modules:
        Freckle.App.VersionSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    build-depends:
        base >=4.11.1.0 && <5,
        directory >=1.3.1.5,
        freckle-app -any,
        hspec >=2.8.1,
        process >=1.6.3.0,
        temporary >=1.3,
        text >=1.2.3.1,
        time >=1.8.0.2

    if flag(test-git)

    else
        buildable: False

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Main.hs
    hs-source-dirs:     tests
    other-modules:
        Freckle.App.BugsnagSpec
        Freckle.App.HttpSpec
        Freckle.App.Memcached.ServersSpec
        Freckle.App.MemcachedSpec
        Freckle.App.Test.Properties.JSONSpec
        Freckle.App.Test.Properties.PathPieceSpec
        Freckle.App.WaiSpec
        Spec
        Paths_freckle_app

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        FlexibleContexts FlexibleInstances GADTs GeneralizedNewtypeDeriving
        LambdaCase MultiParamTypeClasses NoImplicitPrelude
        NoMonomorphismRestriction OverloadedStrings RankNTypes
        RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:        -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        Blammo >=1.0.2.2,
        QuickCheck >=2.13.1,
        aeson >=1.5.2.0,
        base >=4.11.1.0 && <5,
        bytestring >=0.10.8.2,
        errors >=2.3.0,
        freckle-app -any,
        hspec >=2.8.1,
        http-types >=0.12.2,
        lens >=4.16.1,
        lens-aeson >=1.1,
        memcache >=0.3.0.1,
        postgresql-simple >=0.6.2,
        wai >=3.2.1.2,
        wai-extra >=3.0.29
