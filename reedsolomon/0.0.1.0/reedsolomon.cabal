Name:                reedsolomon
Version:             0.0.1.0
Synopsis:            Reed-Solomon Erasure Coding in Haskell
Description:         Please see README.md
Homepage:            http://github.com/NicolasT/reedsolomon
Bug-Reports:         http://github.com/NicolasT/reedsolomon/issues
License:             MIT
License-File:        LICENSE
Author:              Nicolas Trangez
Maintainer:          ikke@nicolast.be
Copyright:
    (c) 2015, Nicolas Trangez
    (c) 2015, Klaus Post
    (c) 2015, Backblaze
Category:            Data
Build-Type:          Custom
Extra-Source-Files:  README.md
                   , examples/README.md
                   , stack.yaml

                   , cbits/configure.ac
                   , cbits/Makefile.am
                   , cbits/m4/ax_append_compile_flags.m4
                   , cbits/m4/ax_append_flag.m4
                   , cbits/m4/ax_check_compile_flag.m4
                   , cbits/m4/ax_gcc_func_attribute.m4
                   , cbits/m4/ax_require_defined.m4

                   , cbits/reedsolomon.c
                   , cbits/reedsolomon.h
                   , cbits/reedsolomon_dispatch.c

                   , cbits/aclocal.m4
                   , cbits/ar-lib
                   , cbits/compile
                   , cbits/config.guess
                   , cbits/config.h.in
                   , cbits/config.sub
                   , cbits/configure
                   , cbits/depcomp
                   , cbits/install-sh
                   , cbits/ltmain.sh
                   , cbits/m4/libtool.m4
                   , cbits/m4/lt~obsolete.m4
                   , cbits/m4/ltoptions.m4
                   , cbits/m4/ltsugar.m4
                   , cbits/m4/ltversion.m4
                   , cbits/Makefile.in
                   , cbits/missing
Cabal-Version:       >=1.10

Flag SIMD
  Description:  Enable SIMD Galois field calculation routines
  Default:      True
  Manual:       True

Flag LLVM
  Description:  Enable LLVM compilation
  Default:      True
  Manual:       True

Library
  Hs-Source-Dirs:      src
  Exposed-Modules:     Data.ReedSolomon
                     , Data.ReedSolomon.BuildInfo
                     , Data.Vector.Storable.ByteString
  Other-Modules:       Data.ReedSolomon.Galois
                     , Data.ReedSolomon.Galois.GenTables
                     , Data.ReedSolomon.Galois.NoAsm
                     , Data.ReedSolomon.Matrix
                     , Data.Vector.Generic.Compat
                     , Data.Vector.Generic.Exceptions
                     , Data.Vector.Generic.Lifted
                     , Data.Vector.Generic.Sized
  Build-Depends:       base >= 4.7 && < 4.9
                     , vector >= 0.10 && < 0.12
                     , loop >= 0.2 && < 0.4
                     , primitive >= 0.6 && < 0.7
                     , mtl >= 2.1 && < 2.3
                     , exceptions >= 0.8 && < 0.9
                     , bytestring >= 0.10 && < 0.11
                     , profunctors >= 4.4 && < 5.2
                     , gitrev >= 1.0 && < 1.2
  Default-Language:    Haskell2010
  Ghc-Options:         -Wall -Odph
  if impl(ghc >= 7.10)
    Ghc-Options:       -g
  Ghc-Prof-Options:    -fprof-auto

  if flag(SIMD)
    Other-Modules:     Data.ReedSolomon.Galois.Amd64
    Extra-Libraries:   reedsolomon
    Cpp-Options:       -DSIMD
  if flag(LLVM)
    Ghc-Options:       -fllvm
    Cpp-Options:       -DLLVM

Executable reedsolomon-simple-encoder
  Hs-Source-Dirs:      examples
  Main-Is:             simple-encoder.lhs
  Ghc-Options:         -Wall -threaded -rtsopts -with-rtsopts=-N
  -- `Build-Depends` goes into the conditional block so Cabal calculates
  -- dependencies correctly, even though `Buildable` is set to `False`.
  -- See https://github.com/haskell/cabal/issues/1725#issuecomment-123783734
  if !os(windows)
    Build-Depends:     base
                     , bytestring
                     , vector
                     , optparse-applicative >= 0.11 && < 0.12
                     , filepath >= 1.3 && < 1.5
                     , bytestring-mmap >= 0.2 && < 0.3
                     , reedsolomon
  else
    Buildable:         False
  Default-Language:    Haskell2010

Executable reedsolomon-simple-decoder
  Hs-Source-Dirs:      examples
  Main-Is:             simple-decoder.lhs
  Ghc-Options:         -Wall -threaded -rtsopts -with-rtsopts=-N
  if !os(windows)
    Build-Depends:     base
                     , bytestring
                     , vector
                     , optparse-applicative >= 0.11 && < 0.12
                     , filepath >= 1.3 && < 1.5
                     , bytestring-mmap >= 0.2 && < 0.3
                     , reedsolomon
  else
    Buildable:         False
  Default-Language:    Haskell2010

Executable reedsolomon-simple-bench
  Hs-Source-Dirs:      examples
  Main-Is:             simple-bench.hs
  Ghc-Options:         -rtsopts -Wall -Odph
  if impl(ghc >= 7.10)
    Ghc-Options:       -g
  if flag(LLVM)
    Ghc-Options:       -fllvm
  Build-Depends:       base
                     , vector
                     , random >= 1.1 && < 1.2
                     , criterion >= 1.1 && < 1.2
                     , statistics >= 0.13 && < 0.14
                     , reedsolomon
  Default-Language:    Haskell2010

Executable reedsolomon-profiling
  Hs-Source-Dirs:      bench
  Main-Is:             profiling.hs
  Ghc-Options:         -rtsopts -Wall -Odph
  if impl(ghc >= 7.10)
    Ghc-Options:       -g
  if flag(LLVM)
    Ghc-Options:       -fllvm
  Build-Depends:       base
                     , vector
                     , deepseq >= 1.3 && < 1.5
                     , random >= 1.1 && < 1.2
                     , optparse-applicative >= 0.11 && < 0.12
                     , clock >= 0.4 && < 0.6
                     , reedsolomon
  Default-Language:    Haskell2010

Test-Suite reedsolomon-test
  Type:                exitcode-stdio-1.0
  Hs-Source-Dirs:      test
                     , src
  Main-Is:             Main.hs
  Other-Modules:       Galois
                     , Matrix
                     , ReedSolomon
                     , Vector
                     , Data.ReedSolomon
                     , Data.ReedSolomon.Galois
                     , Data.ReedSolomon.Galois.GenTables
                     , Data.ReedSolomon.Galois.NoAsm
                     , Data.ReedSolomon.Matrix
                     , Data.Vector.Generic.Compat
                     , Data.Vector.Generic.Exceptions
                     , Data.Vector.Generic.Lifted
                     , Data.Vector.Generic.Sized
                     , Data.Vector.Storable.ByteString
  Build-Depends:       base
                     , vector
                     , loop
                     , primitive
                     , mtl
                     , exceptions
                     , bytestring
                     , profunctors
                     , cpu >= 0.1 && < 0.2
                     , tasty >= 0.10 && < 0.12
                     , tasty-hunit > 0.9 && < 0.10
                     , tasty-quickcheck >= 0.8 && < 0.9
                     , tasty-ant-xml >= 1.0 && < 1.1
                     , QuickCheck >= 2.7 && < 2.9
                     , random >= 1.1 && < 1.2
                     , reedsolomon
  Default-Language:    Haskell2010
  Ghc-Options:         -threaded -rtsopts -with-rtsopts=-N -Wall -Odph
  if impl(ghc >= 7.10)
    Ghc-Options:       -g
  if flag(LLVM)
    Ghc-Options:       -fllvm
  Ghc-Prof-Options:    -fprof-auto

  if flag(SIMD)
    CPP-Options:       -DSIMD
    Other-Modules:     Data.ReedSolomon.Galois.Amd64

Benchmark reedsolomon-bench
  Type:                exitcode-stdio-1.0
  Hs-Source-Dirs:      bench
                     , src
  Main-Is:             Main.hs
  Other-Modules:       Data.ReedSolomon.Galois.GenTables
                     , Data.ReedSolomon.Galois.NoAsm
                     , Data.Vector.Generic.Sized
  Build-Depends:       base
                     , vector
                     , loop
                     , primitive
                     , cpu >= 0.1 && < 0.2
                     , criterion >= 1.1 && < 1.2
                     , reedsolomon
  Default-Language:    Haskell2010
  Ghc-Options:         -rtsopts -Wall -Odph
  if impl(ghc >= 7.10)
    Ghc-Options:       -g
  if flag(LLVM)
    Ghc-Options:       -fllvm
  Ghc-Prof-Options:    -fprof-auto

  if flag(SIMD)
    CPP-Options:       -DSIMD
    Other-Modules:     Data.ReedSolomon.Galois.Amd64

Source-Repository head
  Type:     git
  Location: https://github.com/NicolasT/reedsolomon.git
