name:         rapid
version:      0.1.5
category:     Development
synopsis:     Hot reload and reload-surviving values with GHCi
maintainer:   Markus Läll <markus.l2ll@gmail.com>
author:       Ertugrul Söylemez <esz@posteo.de>
copyright:    Copyright 2018 Ertugrul Söylemez
homepage:     https://github.com/haskell-rapid/rapid
bug-reports:  https://github.com/haskell-rapid/rapid
license:      BSD3
license-file: LICENSE

description:  This package provides a rapid prototyping suite for GHCi
    that can be used standalone or integrated into editors.  You can
    hot-reload individual running components as you make changes to
    their code.  It is designed to shorten the development cycle during
    the development of long-running programs like servers, web
    applications and interactive user interfaces.
    .
    It can also be used in the context of batch-style programs:  Keep
    resources that are expensive to create in memory and reuse them
    across module reloads instead of reloading/recomputing them after
    every code change.
    .
    Technically this package is a safe and convenient wrapper around
    <https://hackage.haskell.org/package/foreign-store foreign-store>.

build-type:         Simple
cabal-version:      >= 1.10
extra-source-files: CHANGELOG.md README.md

source-repository head
    type:     git
    location: https://github.com/haskell-rapid/rapid


library
    build-depends:
        async >= 2.1 && < 2.3,
        base >= 4.8 && < 4.18,
        containers >= 0.5 && < 0.7,
        foreign-store == 0.2.*,
        stm >= 2.4 && < 2.7
    default-language: Haskell2010
    ghc-options: -W
    exposed-modules:
        Rapid
