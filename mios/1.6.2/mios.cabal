-- This file has been generated from package.yaml by hpack version 0.28.2.
--
-- see: https://github.com/sol/hpack
--
-- hash: 5cd432e14ff114a77b46f65cd54a00907c68da1bb380706bdbeb682bf43ff46d

name:           mios
version:        1.6.2
synopsis:       A Minisat-based CDCL SAT solver in Haskell
description:    A modern and very fast SAT solver written in Haskell, using CDCL, watch literals, VSIDS, blocking-literals, phase saving, LBD, Glucose-like restart and so on. 'Mios' is an abbreviation of 'Minisat-based Implementation and Optimization Study on SAT solver'.
category:       Artificial Intelligence, Constraints
homepage:       https://github.com/shnarazk/mios
bug-reports:    https://github.com/shnarazk/mios/issues
author:         Shuji Narazaki
maintainer:     narazaki@nagasaki-u.ac.jp
copyright:      2017 Shuji Narazaki
license:        GPL-3
license-file:   LICENSE
build-type:     Simple
cabal-version:  >= 1.10
extra-source-files:
    app/sample.hs

source-repository head
  type: git
  location: https://github.com/shnarazk/mios

flag llvm
  description: Compile with llvm
  manual: True
  default: False

flag utils
  description: Build misc utilities for developer
  manual: True
  default: False

library
  exposed-modules:
      SAT.Mios
      SAT.Mios.Clause
      SAT.Mios.ClauseManager
      SAT.Mios.ClausePool
      SAT.Mios.Criteria
      SAT.Mios.Main
      SAT.Mios.OptionParser
      SAT.Mios.Solver
      SAT.Mios.Types
      SAT.Mios.Util.BoolExp
      SAT.Mios.Util.DIMACS
      SAT.Mios.Util.DIMACS.MinisatReader
      SAT.Mios.Util.DIMACS.Reader
      SAT.Mios.Util.DIMACS.Writer
      SAT.Mios.Validator
      SAT.Mios.Vec
  other-modules:
      Paths_mios
  hs-source-dirs:
      src
  default-extensions: Strict
  build-depends:
      base >=4.7 && <5
    , bytestring >=0.10
    , ghc-prim >=0.5
    , primitive >=0.6
    , vector >=0.12
  if flag(llvm)
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -optlc-O3
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -fprof-auto
  else
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -msse2
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fprof-auto
  default-language: Haskell2010

executable cnf-stat
  main-is: cnf-stat.hs
  other-modules:
      Paths_mios
  hs-source-dirs:
      utils
  default-extensions: Strict
  build-depends:
      base >=4.7 && <5
    , bytestring >=0.10
    , ghc-prim >=0.5
    , mios
    , primitive >=0.6
    , vector >=0.12
  if flag(llvm)
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -optlc-O3
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -fprof-auto
  else
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -msse2
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fprof-auto
  if flag(utils)
    buildable: True
  else
    buildable: False
  default-language: Haskell2010

executable mios162
  main-is: mios.hs
  other-modules:
      DiscordSecret
      Paths_mios
  hs-source-dirs:
      app
  default-extensions: Strict
  build-depends:
      base >=4.7 && <5
    , bytestring >=0.10
    , ghc-prim >=0.5
    , gitrev
    , mios
    , primitive >=0.6
    , vector >=0.12
  if flag(llvm)
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -optlc-O3
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -fprof-auto
  else
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -msse2
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fprof-auto
  if flag(llvm)
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -optlc-O3 -rtsopts -with-rtsopts=-M7g
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -fprof-auto -rtsopts
  else
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -msse2 -rtsopts -with-rtsopts=-M7g
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fprof-auto -rtsopts
  default-language: Haskell2010

test-suite mios-test
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      MiosSpec
      Paths_mios
  hs-source-dirs:
      test
  default-extensions: Strict
  ghc-options: -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      base >=4.7 && <5
    , bytestring >=0.10
    , ghc-prim >=0.5
    , hspec
    , mios
    , primitive >=0.6
    , vector >=0.12
  if flag(llvm)
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -optlc-O3
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fllvm -optlo-O3 -fprof-auto
  else
    ghc-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -msse2
    ghc-prof-options: -j -fwarn-missing-signatures -funbox-strict-fields -O2 -fprof-auto
  default-language: Haskell2010
