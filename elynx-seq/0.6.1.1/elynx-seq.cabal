cabal-version:  3.0
name:           elynx-seq
version:        0.6.1.1
synopsis:       Handle molecular sequences
description:    Examine, modify, and simulate molecular sequences in a reproducible way. Please see the README on GitHub at <https://github.com/dschrempf/elynx>.
category:       Bioinformatics
homepage:       https://github.com/dschrempf/elynx#readme
bug-reports:    https://github.com/dschrempf/elynx/issues
author:         Dominik Schrempf
maintainer:     dominik.schrempf@gmail.com
copyright:      Dominik Schrempf (2021)
license:        GPL-3.0-or-later
license-file:   LICENSE
build-type:     Simple

extra-source-files:
    README.md
    ChangeLog.md
    data/AminoAcid.fasta
    data/Erroneous.fasta
    data/NucleotideDifferentLength.fasta
    data/NucleotideDifferentLengthTrimmed.fasta
    data/Nucleotide.fasta
    data/NucleotideIUPAC.fasta
    data/TranslateMitochondrialVertebrateDNA.fasta
    data/TranslateMitochondrialVertebrateProtein.fasta

source-repository head
  type: git
  location: https://github.com/dschrempf/elynx

library
  exposed-modules:
      ELynx.Alphabet.Alphabet
      ELynx.Alphabet.Character
      ELynx.Alphabet.DistributionDiversity
      ELynx.Character.AminoAcid
      ELynx.Character.AminoAcidI
      ELynx.Character.AminoAcidS
      ELynx.Character.AminoAcidX
      ELynx.Character.Character
      ELynx.Character.Codon
      ELynx.Character.Nucleotide
      ELynx.Character.NucleotideI
      ELynx.Character.NucleotideX
      ELynx.Sequence.Alignment
      ELynx.Sequence.Defaults
      ELynx.Sequence.Distance
      ELynx.Sequence.Sequence
      ELynx.Sequence.Translate
      ELynx.Sequence.Export.Fasta
      ELynx.Sequence.Import.Fasta
  other-modules:
      Paths_elynx_seq
  autogen-modules:
      Paths_elynx_seq
  hs-source-dirs: src
  ghc-options: -Wall -Wunused-packages
  build-depends:
      aeson
    , attoparsec
    , base >=4.7 && <5
    , bytestring
    , containers
    , matrices
    , mwc-random
    , parallel
    , primitive
    , vector
    , vector-th-unbox
    , word8
  default-language: Haskell2010

test-suite seq-test
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      ELynx.Alphabet.DistributionDiversitySpec
      ELynx.Sequence.AlignmentSpec
      ELynx.Sequence.SequenceSpec
      ELynx.Sequence.TranslateSpec
      ELynx.Sequence.Export.FastaSpec
      ELynx.Sequence.Import.FastaSpec
      Paths_elynx_seq
  autogen-modules:
      Paths_elynx_seq
  hs-source-dirs: test
  ghc-options: -Wall -Wunused-packages
  build-depends:
      base >=4.7 && <5
    , bytestring
    , elynx-seq
    , elynx-tools
    , hspec
    , matrices
    , vector
  default-language: Haskell2010
