cabal-version: 1.12
name: elynx-seq
version: 0.2.1
license: GPL-3
license-file: LICENSE
copyright: Dominik Schrempf (2020)
maintainer: dominik.schrempf@gmail.com
author: Dominik Schrempf
homepage: https://github.com/dschrempf/elynx#readme
bug-reports: https://github.com/dschrempf/elynx/issues
synopsis: Handle molecular sequences
description:
    Examine, modify, and simulate molecular sequences in a reproducible way. Please see the README on GitHub at <https://github.com/dschrempf/elynx>.
category: Bioinformatics
build-type: Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
    type: git
    location: https://github.com/dschrempf/elynx

library
    exposed-modules:
        ELynx.Data.Alphabet.Alphabet
        ELynx.Data.Alphabet.Character
        ELynx.Data.Alphabet.DistributionDiversity
        ELynx.Data.Character.AminoAcid
        ELynx.Data.Character.AminoAcidI
        ELynx.Data.Character.AminoAcidS
        ELynx.Data.Character.AminoAcidX
        ELynx.Data.Character.Character
        ELynx.Data.Character.Codon
        ELynx.Data.Character.Nucleotide
        ELynx.Data.Character.NucleotideI
        ELynx.Data.Character.NucleotideX
        ELynx.Data.Sequence.Alignment
        ELynx.Data.Sequence.Defaults
        ELynx.Data.Sequence.Sequence
        ELynx.Data.Sequence.Translate
        ELynx.Export.Sequence.Fasta
        ELynx.Import.Sequence.Fasta
    hs-source-dirs: src
    other-modules:
        Paths_elynx_seq
    default-language: Haskell2010
    ghc-options: -Wall -fllvm
    build-depends:
        aeson >=1.4.7.1 && <1.5,
        base >=4.13.0.0 && <4.14,
        bytestring >=0.10.10.0 && <0.11,
        containers >=0.6.2.1 && <0.7,
        elynx-tools >=0.2.1 && <0.3,
        matrices >=0.5.0 && <0.6,
        megaparsec >=8.0.0 && <8.1,
        mwc-random >=0.14.0.0 && <0.15,
        parallel >=3.2.2.0 && <3.3,
        primitive >=0.7.0.1 && <0.8,
        vector >=0.12.1.2 && <0.13,
        vector-th-unbox >=0.2.1.7 && <0.3,
        word8 >=0.1.3 && <0.2

test-suite seq-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    other-modules:
        ELynx.Data.Alphabet.DistributionDiversitySpec
        ELynx.Data.Sequence.AlignmentSpec
        ELynx.Data.Sequence.SequenceSpec
        ELynx.Data.Sequence.TranslateSpec
        ELynx.Export.Sequence.FastaSpec
        ELynx.Import.Sequence.FastaSpec
        Paths_elynx_seq
    default-language: Haskell2010
    ghc-options: -Wall -fllvm -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        aeson >=1.4.7.1 && <1.5,
        base >=4.13.0.0 && <4.14,
        bytestring >=0.10.10.0 && <0.11,
        containers >=0.6.2.1 && <0.7,
        elynx-seq -any,
        elynx-tools >=0.2.1 && <0.3,
        hspec >=2.7.1 && <2.8,
        hspec-megaparsec >=2.1.0 && <2.2,
        matrices >=0.5.0 && <0.6,
        megaparsec >=8.0.0 && <8.1,
        mwc-random >=0.14.0.0 && <0.15,
        parallel >=3.2.2.0 && <3.3,
        primitive >=0.7.0.1 && <0.8,
        vector >=0.12.1.2 && <0.13,
        vector-th-unbox >=0.2.1.7 && <0.3,
        word8 >=0.1.3 && <0.2
