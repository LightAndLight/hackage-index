cabal-version: 3.0

name:     wordlist
version:  0.1.0.5
synopsis: Command-line tool to get random words
category: Text, Application

description:
    A command-line application that outputs a random list
    of words, designed to generate memorable passwords.

    == Example usage

    Download a list of words:

    > bash$ export WORD_LIST_PATH="$HOME/words"
    > bash$ wget -O "$WORD_LIST_PATH" 'https://raw.githubusercontent.com/trezor/python-mnemonic/ec21884db9f3af236732121e7ccf97435b924915/mnemonic/wordlist/english.txt'

    By default, @wordlist@ generates four words separated by spaces.

    > bash$ wordlist
    > afraid avoid sunset cactus

    The number of words and the separator are customizable.

    > bash$ wordlist -n 3 -d '/'
    > kite/wire/impact

    For full command-line documentation:

    > bash$ wordlist --help

author:     Chris Martin <ch.martin@gmail.com>
maintainer: Chris Martin <ch.martin@gmail.com>

homepage: https://github.com/chris-martin/wordlist

license: Apache-2.0
license-file: license.txt

build-type: Simple

extra-source-files: readme.md changelog.md

common base
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
        base >= 4.13 && < 4.17
      , optparse-applicative ^>= 0.16
      , text ^>= 1.2.4

library
    import: base
    hs-source-dirs: src
    exposed-modules: Wordlist.Args

executable wordlist
    import: base
    main-is: wordlist.hs
    hs-source-dirs: exe

    build-depends:
        MonadRandom ^>= 0.5.1
      , vector ^>= 0.12
      , wordlist
