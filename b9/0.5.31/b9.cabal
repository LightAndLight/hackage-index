name: b9
version: 0.5.31
cabal-version: >=1.22
build-type: Simple
license: MIT
license-file: LICENSE
copyright: 2015, 2016 Sven Heyll <svh@posteo.de>
maintainer: svh@posteo.de
homepage: https://github.com/sheyll/b9-vm-image-builder
bug-reports: https://github.com/sheyll/b9-vm-image-builder/issues
synopsis: A tool and library for building virtual machine images.
description:
    Build virtual machine images for vm-deployments; resize,
    un-partition, create from scratch or convert disk image
    files in a variety of formats; assemble and generate all
    associated files from templates and regular files.
    VM images can further be modifed through scripts, which are
    executed in LXC containers into which the vm-images as well
    as arbitrary directories from the host are mounted.
    All assembled files can also be accessed by vm build
    scripts through a special directory mounted in the build
    container, and/or can be written to directories, ISO- or
    VFAT-images.
    The ISO/VFAT images that B9 creates are compatible to
    'cloud-init's 'NoCloud' data source;
    B9 is also very well suited for compiling in a
    containerized environment. For these applications, the
    images can be marked as 'Transient' to indicate no further
    interest in the VM-image itself, and B9 will discard them
    after the build.
    B9 will never over-write source files, not even large
    vm-image files - there is no intended way to modify a
    source vm-image file 'in-place'.
    B9 operates in random build directories, which are
    discarded when the build exists.
category: Development
author: Sven Heyll <svh@posteo.de>
extra-source-files:
    README.md
    build_and_test.sh
    build_haddock.sh
    LICENSE
    TODO.org
    Setup.hs
    b9.cabal
    .gitignore
    prepare_release.sh
    stack.yaml
    stack-lts-5.14.yaml
    .travis.yml
    CONTRIBUTORS

source-repository head
    type: git
    location: git://github.com/sheyll/b9-vm-image-builder.git

library
    
    if (impl(ghc >=7.8.2) && impl(ghc <=7.10))
        ghc-options: -fno-warn-amp
    exposed-modules:
        B9
        B9.B9Monad
        B9.B9Config
        B9.Builder
        B9.ConfigUtils
        B9.ExecEnv
        B9.DiskImages
        B9.DiskImageBuilder
        B9.ShellScript
        B9.PartitionTable
        B9.MBR
        B9.LibVirtLXC
        B9.Repository
        B9.RepositoryIO
        B9.ArtifactGenerator
        B9.ArtifactGeneratorImpl
        B9.Vm
        B9.VmBuilder
        B9.Content.ErlTerms
        B9.Content.ErlangPropList
        B9.Content.YamlObject
        B9.Content.AST
        B9.Content.Generator
        B9.Content.StringTemplate
        B9.QCUtil
        B9.DSL
    build-depends:
        ConfigFile >=1.1.4,
        QuickCheck >=2.5,
        aeson >=0.11.2.1,
        async >=2.1.0,
        base >=4.8 && <5,
        binary >=0.8.3.0,
        bytestring >=0.10.8.1,
        conduit >=1.2.8,
        conduit-extra >=1.1.15,
        directory >=1.2.6.2,
        filepath >=1.4.1.0,
        hashable >=1.2.4.0,
        mtl >=2.2.1,
        time >=1.6.0.1,
        parallel >=3.2.1.0,
        parsec >=3.1.11,
        pretty-show >=1.6.12,
        pretty >=1.1.3.3,
        process >=1.4.2.0,
        random >=1.1,
        semigroups >=0.18.2,
        syb >=0.6,
        template >=0.2.0.10,
        text >=1.2.2.1,
        transformers >=0.5.2.0,
        unordered-containers >=0.2.7.1,
        vector >=0.11.0.0,
        yaml >=0.8.20,
        bifunctors >=5.4.1,
        free >=4.12.4,
        boxes >=0.1.4
    default-language: Haskell2010
    default-extensions: TupleSections GeneralizedNewtypeDeriving
                        DeriveDataTypeable DeriveGeneric RankNTypes FlexibleContexts GADTs
                        DataKinds KindSignatures TypeFamilies DeriveFunctor TemplateHaskell
                        StandaloneDeriving CPP
    hs-source-dirs: src/lib
    other-modules:
        Paths_b9
    ghc-options: -Wall -fwarn-unused-binds -fno-warn-unused-do-bind

executable b9c
    
    if (impl(ghc >=7.8.2) && impl(ghc <=7.10))
        ghc-options: -fno-warn-amp
    main-is: Main.hs
    build-depends:
        b9 >=0.5.31,
        base >=4.8 && <5,
        directory >=1.2.6.2,
        bytestring >=0.10.8.1,
        optparse-applicative >=0.12.1.0
    default-language: Haskell2010
    default-extensions: TupleSections GeneralizedNewtypeDeriving
                        DeriveDataTypeable RankNTypes FlexibleContexts GADTs DataKinds
                        KindSignatures TypeFamilies DeriveFunctor TemplateHaskell CPP
    hs-source-dirs: src/cli
    ghc-options: -threaded -with-rtsopts=-N -Wall -fwarn-unused-binds -fno-warn-unused-do-bind

test-suite spec
    
    if (impl(ghc >=7.8.2) && impl(ghc <=7.10))
        ghc-options: -fno-warn-amp -fno-warn-unused-do-bind
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    build-depends:
        base >=4.8 && <5,
        b9 >=0.5.31,
        hspec >=2.2.4,
        hspec-expectations >=0.7.2,
        QuickCheck >=2.8.2,
        aeson >=0.11.2.1,
        yaml >=0.8.20,
        vector >=0.11.0.0,
        unordered-containers >=0.2.7.1,
        bytestring >=0.10.8.1,
        text >=1.2.2.1,
        semigroups >=0.18.2
    default-language: Haskell2010
    default-extensions: TupleSections GeneralizedNewtypeDeriving
                        DeriveDataTypeable RankNTypes FlexibleContexts GADTs DataKinds
                        KindSignatures TypeFamilies DeriveFunctor TemplateHaskell CPP
    hs-source-dirs: src/tests
    other-modules:
        B9.Content.ErlTermsSpec
        B9.Content.ErlangPropListSpec
        B9.Content.YamlObjectSpec
        B9.ArtifactGeneratorImplSpec
        B9.DiskImagesSpec
    ghc-options: -Wall
