-- Package description for HGrib.

name:                hgrib
version:             0.2.0.0
synopsis:            Unofficial bindings for GRIB API
description:
    Unofficial bindings to ECMWF's GRIB API library for reading WMO
    FM-92 GRIB edition 1 and edition 2 messages.
homepage:            https://github.com/mjakob/hgrib
license:             GPL-3
license-file:        LICENSE
author:              Mattias Jakobsson
maintainer:          mjakob422@gmail.com
copyright:           (c) 2015 Mattias Jakobsson
category:            Data
stability:           unstable
bug-reports:         https://github.com/mjakob/hgrib/issues
build-type:          Simple
cabal-version:       >=1.10
extra-source-files:
    CHANGELOG.md
    README.md
    test/stage/regular_latlon_surface.grib1
    test/stage/regular_latlon_surface.grib2

source-repository head
  type:      git
  location:  https://github.com/mjakob/hgrib.git
  branch:    develop

source-repository this
  type:      git
  location:  https://github.com/mjakob/hgrib.git
  tag:       0.2.0.0

library
  default-language:  Haskell2010
  hs-source-dirs:    src
  exposed-modules:
      Data.Grib.Raw
      Data.Grib.Raw.Exception

      -- These modules need to go above any modules importing them via c2hs:
      Data.Grib.Raw.CFile
      Data.Grib.Raw.Context
      Data.Grib.Raw.Handle

      -- These modules can import any of the modules above via c2hs:
      Data.Grib.Raw.Index
      Data.Grib.Raw.Iterator
      Data.Grib.Raw.KeysIterator
      Data.Grib.Raw.Nearest
      Data.Grib.Raw.Types
      Data.Grib.Raw.Value

  other-modules:
      Data.Grib.Raw.Marshal
  build-depends:
      base >= 4.5 && < 4.9
  build-tools:
      c2hs == 0.26.*
  extra-libraries:
      grib_api
  ghc-options:
      -Wall

test-suite tests
  type:              exitcode-stdio-1.0
  default-language:  Haskell2010
  main-is:           Spec.hs
  hs-source-dirs:    test
  other-modules:
      Data.Grib.Raw.CFileSpec
      Data.Grib.Raw.ContextSpec
      Data.Grib.Raw.HandleSpec
      Data.Grib.Raw.IndexSpec
      Data.Grib.Raw.IteratorSpec
      Data.Grib.Raw.KeysIteratorSpec
      Data.Grib.Raw.NearestSpec
      Data.Grib.Raw.Test
      Data.Grib.Raw.ValueSpec
  build-depends:
      base      >= 4.5 && < 4.9
    , directory >= 1.1 && < 1.3
    , hgrib
    , hspec     == 2.1.*
  ghc-options:
      -Wall

flag build_examples
  description:  Build GRIB API example programs
  default:      False
  manual:       True

executable get
  default-language:  Haskell2010
  main-is:           get.hs
  hs-source-dirs:    examples
  build-depends:
      base >= 4.5 && < 4.9
    , hgrib
  ghc-options:
      -Wall -fno-warn-unused-do-bind
  if !flag(build_examples)
    buildable:       False
