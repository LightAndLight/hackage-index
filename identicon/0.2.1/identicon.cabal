--
-- Cabal configuration for ‘identicon’ package.
--
-- Copyright © 2016–2017 Mark Karpov <markkarpov@openmailbox.org>
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are
-- met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- * Redistributions in binary form must reproduce the above copyright
--   notice, this list of conditions and the following disclaimer in the
--   documentation and/or other materials provided with the distribution.
--
-- * Neither the name Mark Karpov nor the names of contributors may be used
--   to endorse or promote products derived from this software without
--   specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS “AS IS” AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
-- ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

name:                 identicon
version:              0.2.1
cabal-version:        >= 1.10
license:              BSD3
license-file:         LICENSE.md
author:               Mark Karpov <markkarpov@openmailbox.org>
maintainer:           Mark Karpov <markkarpov@openmailbox.org>
homepage:             https://github.com/mrkkrp/identicon
bug-reports:          https://github.com/mrkkrp/identicon/issues
category:             Graphics, Image
synopsis:             Flexible generation of identicons
build-type:           Simple
description:          Flexible generation of identicons.
extra-doc-files:      CHANGELOG.md
                    , README.md
data-files:           data-examples/*.png

source-repository head
  type:               git
  location:           https://github.com/mrkkrp/identicon.git

flag dev
  description:        Turn on development settings.
  manual:             True
  default:            False

library
  build-depends:      base             >= 4.7     && < 5.0
                    , JuicyPixels      >= 3.2.6.5 && < 4.0
                    , bytestring       >= 0.10.6  && < 0.13
  if !impl(ghc >= 8.0)
    build-depends:    semigroups       == 0.18.*
  exposed-modules:    Graphics.Identicon
                    , Graphics.Identicon.Primitive
  if flag(dev)
    ghc-options:      -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010

test-suite tests
  main-is:            Spec.hs
  hs-source-dirs:     tests
  type:               exitcode-stdio-1.0
  build-depends:      JuicyPixels      >= 3.2.6.5 && < 4.0
                    , QuickCheck       >= 2.7     && < 3.0
                    , base             >= 4.7     && < 5.0
                    , bytestring       >= 0.10.6  && < 0.13
                    , hspec            >= 2.0     && < 3.0
                    , identicon        >= 0.2.1
  if !impl(ghc >= 8.0)
    build-depends:    semigroups       == 0.18.*
  if flag(dev)
    ghc-options:      -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010

benchmark bench
  main-is:            Main.hs
  hs-source-dirs:     bench
  type:               exitcode-stdio-1.0
  build-depends:      base             >= 4.7     && < 5.0
                    , JuicyPixels      >= 3.2.6.5 && < 4.0
                    , bytestring       >= 0.10.6  && < 0.13
                    , criterion        >= 0.6.2.1 && < 1.2
                    , identicon        >= 0.2.1
                    , random           >= 1.1     && < 1.2
                    , tf-random        >= 0.4     && < 0.6
  if flag(dev)
    ghc-options:      -02 -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010
