cabal-version: 3.0
name: brick-tabular-list
version: 2.2.0.8

synopsis: Tabular list widgets for brick.

description:
    This package contains two tabular list widgets for brick.

    * Grid tabular list
    * Mixed tabular list

    A tabular list consists of cells(row columns), column headers, and row headers. Column headers and row headers are
    optional.

    It can handle a very large data set if you delete invisible rows from memory and fetch visible rows from a database
    (file). For example, SQLite database file can handle a large spreadsheet.

    == To get started

    * Read "Brick.Widgets.TabularList.Grid" or "Brick.Widgets.TabularList.Mixed".
    * Run demo programs. To learn more quickly, modify and run demo programs.

    == Lens support

    If you want to use lens, I encourage using @OverloadedLabels@ extension with generic-lens or optics-core.

    For zoom, you have to use van Laarhoven lens because brick supports zoom through microlens.

    == For Contributors

    This library tries not to exceed 120 characters per line.

homepage: https://codeberg.org/amano.kenji/brick-tabular-list
bug-reports: https://codeberg.org/amano.kenji/brick-tabular-list/issues

author: amano.kenji
maintainer: amano.kenji@proton.me
license: 0BSD
license-file: LICENSE

category: User Interface
extra-source-files:
    CHANGELOG.md

extra-doc-files: *.png

source-repository head
    type: git
    location: https://codeberg.org/amano.kenji/brick-tabular-list.git

Flag demo
    Description: Build demonstration programs
    Default: False

library
    build-depends:
        base >=4.16.3.0 && <5
      , brick >=1.5 && <2.3
      , containers >=0.6.4 && <0.7
      , generic-lens >=2.2.1 && <2.3
      , microlens >=0.4.13 && <0.5
      , optics-core >=0.4.1 && <0.5
      , vty >=5.38 && <6.2
    exposed-modules:
        Brick.Widgets.TabularList
      , Brick.Widgets.TabularList.Grid
      , Brick.Widgets.TabularList.Mixed
      , Brick.Widgets.TabularList.Types
    other-modules:
        Brick.Widgets.TabularList.Internal.Common
      , Brick.Widgets.TabularList.Internal.Lens
    hs-source-dirs: src
    default-language: Haskell2010

common demo
    if !flag(demo)
      buildable: False
    build-depends:
        base
      , brick
      , brick-tabular-list
      , containers
      , optics-core
      , vector >=0.12.3.1 && <0.14
      , vty
    hs-source-dirs: demo
    ghc-options: -threaded
    default-language: Haskell2010

executable mixed-tabular-list
    import: demo
    main-is: MixedTabularList.hs

executable grid-tabular-list
    import: demo
    main-is: GridTabularList.hs
