Name:           binary-search
Version:        0.9
x-revision: 1
Build-Type:     Simple
License:        BSD3
license-file:   LICENSE
Author:         Ross Paterson <ross@soi.city.ac.uk>, Takayuki Muranushi <muranushi@gmail.com>
Maintainer:     Takayuki Muranushi <muranushi@gmail.com>
Category:       Algorithms
Synopsis:       Binary and exponential searches
Description:
            __Introduction__
            .
            This package provides varieties of binary search functions.
            .
            These search function can search for predicates of the type
            @pred :: (Integral a, Eq b) => a -> b@, or monadic predicates
            @pred :: (Integral a, Eq b, Monad m) => a -> m b@.
            The predicates must satisfy that the domain range for any codomain value
            is continuous; that is, @∀x≦y≦z. pred x == pred z ⇒ pred y == pred x@ .
            .
            For example, we can address the problem of finding the boundary
            of an upward-closed set of integers, using a combination
            of exponential and binary searches.
            .
            Variants are provided
            for searching within bounded and unbounded intervals of
            both 'Integer' and bounded integral types.
            .
            The package was created by Ross Paterson, and extended
            by Takayuki Muranushi, to be used together with SMT solvers.
            .
            __The Module Structure__
            .
            *  "Numeric.Search.Combinator.Monadic" provides the most generic combinators. "Numeric.Search.Combinator.Pure" provides the pure version of them.
            *  "Numeric.Search" exports both pure and monadic version.
            *  "Numeric.Search.Bounded" ,  "Numeric.Search.Integer" ,  "Numeric.Search.Range" provides the various specialized searchers, which means less number of function arguments, and easier to use.
            .
            <<https://travis-ci.org/nushio3/binary-search.svg?branch=master>>

cabal-version:      >=1.8

library
  exposed-modules:  Numeric.Search
                    Numeric.Search.Bounded
                    Numeric.Search.Integer
                    Numeric.Search.Range
                    Numeric.Search.Combinator.Monadic
                    Numeric.Search.Combinator.Pure

  Ghc-Options:      -Wall

  build-depends:    base >=4.8 && < 5
                  , containers >= 0.4 && < 0.6

  other-extensions: DeriveFunctor, FlexibleContexts, FlexibleInstances, MultiParamTypeClasses, MultiWayIf, ScopedTypeVariables, TupleSections

Test-Suite doctest
  Type: exitcode-stdio-1.0
  HS-Source-Dirs: test
  Ghc-Options: -threaded -Wall
  Main-Is: doctests.hs
  Build-Depends:    base
                  , directory >= 1.1
                  , filepath >= 1.2
                  , doctest >= 0.9.3

Test-Suite spec
  Type: exitcode-stdio-1.0
  Hs-Source-Dirs: test
  Ghc-Options: -Wall
  Main-Is: Spec.hs
  Other-Modules:    PureSpec

  Build-Depends:    base >=4.5 && < 5
                  , binary-search

                  , hspec >= 1.3
                  , QuickCheck >= 2.5



Source-Repository head
  Type: git
  Location: https://github.com/nushio3/binary-search
