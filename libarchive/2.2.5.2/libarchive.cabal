cabal-version:      3.0
name:               libarchive
version:            2.2.5.2
license:            BSD-3-Clause
license-file:       LICENSE
copyright:          Copyright: (c) 2018-2020 Vanessa McHale
maintainer:         vamchale@gmail.com
author:             Vanessa McHale
tested-with:        ghc ==8.4.4 ghc ==8.6.5 ghc ==8.8.3 ghc ==8.10.1
bug-reports:        https://github.com/vmchale/libarchive/issues
synopsis:           Haskell interface to libarchive
description:
    Haskell bindings for [libarchive](https://www.libarchive.org/). Provides the ability to unpack archives, including the ability to unpack archives lazily.

category:           Codec
build-type:         Custom
extra-source-files:
    c/autoconf-darwin/config.h
    c/autoconf-linux/config.h
    c/autoconf-freebsd/config.h
    c/*.c
    c/*.h

extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/vmchale/libarchive

custom-setup
    setup-depends:
        base -any,
        Cabal -any,
        chs-cabal -any

flag cross
    description: Set this flag if cross-compiling
    default:     False
    manual:      True

flag low-memory
    description: Run low-memory version of test suite
    default:     False

flag static
    description: Use the bundled sources
    default:     False
    manual:      True

library
    exposed-modules:
        Codec.Archive
        Codec.Archive.Foreign
        Codec.Archive.Foreign.Archive
        Codec.Archive.Foreign.ArchiveEntry

    hs-source-dirs:   src
    other-modules:
        Codec.Archive.Foreign.Archive.Macros
        Codec.Archive.Foreign.ArchiveEntry.Macros
        Codec.Archive.Pack
        Codec.Archive.Pack.Lazy
        Codec.Archive.Pack.Common
        Codec.Archive.Unpack.Lazy
        Codec.Archive.Unpack
        Codec.Archive.Types
        Codec.Archive.Types.Foreign
        Codec.Archive.Permissions
        Codec.Archive.Common
        Codec.Archive.Monad

    default-language: Haskell2010
    other-extensions: DeriveGeneric DeriveAnyClass
    ghc-options:
        -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
        -Wredundant-constraints

    build-depends:
        base >=4.9 && <5,
        bytestring -any,
        composition-prelude >=2.0.5.0,
        dlist -any,
        filepath -any,
        mtl >=2.2.1,
        unix-compat >=0.1.2.1,
        deepseq >=1.4.0.0

    if !flag(cross)
        build-tool-depends: c2hs:c2hs >=0.26.1

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.10)
        ghc-options: -Wunused-packages

    if flag(static)
        cc-options:   -std=gnu99 -DHAVE_CONFIG_H
        c-sources:
            c/archive_acl.c
            c/archive_blake2sp_ref.c
            c/archive_blake2s_ref.c
            c/archive_check_magic.c
            c/archive_cmdline.c
            c/archive_cryptor.c
            c/archive_digest.c
            c/archive_disk_acl_darwin.c
            c/archive_disk_acl_freebsd.c
            c/archive_disk_acl_linux.c
            c/archive_disk_acl_sunos.c
            c/archive_entry.c
            c/archive_entry_copy_bhfi.c
            c/archive_entry_copy_stat.c
            c/archive_entry_link_resolver.c
            c/archive_entry_sparse.c
            c/archive_entry_stat.c
            c/archive_entry_strmode.c
            c/archive_entry_xattr.c
            c/archive_getdate.c
            c/archive_hmac.c
            c/archive_match.c
            c/archive_options.c
            c/archive_pack_dev.c
            c/archive_pathmatch.c
            c/archive_ppmd7.c
            c/archive_ppmd8.c
            c/archive_random.c
            c/archive_rb.c
            c/archive_read_add_passphrase.c
            c/archive_read_append_filter.c
            c/archive_read.c
            c/archive_read_data_into_fd.c
            c/archive_read_disk_entry_from_file.c
            c/archive_read_disk_posix.c
            c/archive_read_disk_set_standard_lookup.c
            c/archive_read_disk_windows.c
            c/archive_read_extract2.c
            c/archive_read_extract.c
            c/archive_read_open_fd.c
            c/archive_read_open_file.c
            c/archive_read_open_filename.c
            c/archive_read_open_memory.c
            c/archive_read_set_format.c
            c/archive_read_set_options.c
            c/archive_read_support_filter_all.c
            c/archive_read_support_filter_bzip2.c
            c/archive_read_support_filter_compress.c
            c/archive_read_support_filter_grzip.c
            c/archive_read_support_filter_gzip.c
            c/archive_read_support_filter_lrzip.c
            c/archive_read_support_filter_lz4.c
            c/archive_read_support_filter_lzop.c
            c/archive_read_support_filter_none.c
            c/archive_read_support_filter_program.c
            c/archive_read_support_filter_rpm.c
            c/archive_read_support_filter_uu.c
            c/archive_read_support_filter_xz.c
            c/archive_read_support_filter_zstd.c
            c/archive_read_support_format_7zip.c
            c/archive_read_support_format_all.c
            c/archive_read_support_format_ar.c
            c/archive_read_support_format_by_code.c
            c/archive_read_support_format_cab.c
            c/archive_read_support_format_cpio.c
            c/archive_read_support_format_empty.c
            c/archive_read_support_format_iso9660.c
            c/archive_read_support_format_lha.c
            c/archive_read_support_format_mtree.c
            c/archive_read_support_format_rar5.c
            c/archive_read_support_format_rar.c
            c/archive_read_support_format_raw.c
            c/archive_read_support_format_tar.c
            c/archive_read_support_format_warc.c
            c/archive_read_support_format_xar.c
            c/archive_read_support_format_zip.c
            c/archive_string.c
            c/archive_string_sprintf.c
            c/archive_util.c
            c/archive_version_details.c
            c/archive_virtual.c
            c/archive_windows.c
            c/archive_write_add_filter_b64encode.c
            c/archive_write_add_filter_by_name.c
            c/archive_write_add_filter_bzip2.c
            c/archive_write_add_filter.c
            c/archive_write_add_filter_compress.c
            c/archive_write_add_filter_grzip.c
            c/archive_write_add_filter_gzip.c
            c/archive_write_add_filter_lrzip.c
            c/archive_write_add_filter_lz4.c
            c/archive_write_add_filter_lzop.c
            c/archive_write_add_filter_none.c
            c/archive_write_add_filter_program.c
            c/archive_write_add_filter_uuencode.c
            c/archive_write_add_filter_xz.c
            c/archive_write_add_filter_zstd.c
            c/archive_write.c
            c/archive_write_disk_posix.c
            c/archive_write_disk_set_standard_lookup.c
            c/archive_write_disk_windows.c
            c/archive_write_open_fd.c
            c/archive_write_open_file.c
            c/archive_write_open_filename.c
            c/archive_write_open_memory.c
            c/archive_write_set_format_7zip.c
            c/archive_write_set_format_ar.c
            c/archive_write_set_format_by_name.c
            c/archive_write_set_format.c
            c/archive_write_set_format_cpio.c
            c/archive_write_set_format_cpio_newc.c
            c/archive_write_set_format_filter_by_ext.c
            c/archive_write_set_format_gnutar.c
            c/archive_write_set_format_iso9660.c
            c/archive_write_set_format_mtree.c
            c/archive_write_set_format_pax.c
            c/archive_write_set_format_raw.c
            c/archive_write_set_format_shar.c
            c/archive_write_set_format_ustar.c
            c/archive_write_set_format_v7tar.c
            c/archive_write_set_format_warc.c
            c/archive_write_set_format_xar.c
            c/archive_write_set_format_zip.c
            c/archive_write_set_options.c
            c/archive_write_set_passphrase.c
            c/filter_fork_posix.c
            c/filter_fork_windows.c
            c/xxhash.c

        include-dirs: c/

        if os(osx)
            include-dirs: c/autoconf-darwin

        else
            if os(linux)
                include-dirs: c/autoconf-linux

            else
                if os(freebsd)
                    include-dirs: c/autoconf-freebsd

                else
                    buildable:     False
                    build-depends: unbuildable <0

    else
        pkgconfig-depends: libarchive (==3.4.0 || >3.4.0) && <4.0

test-suite libarchive-test
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    build-tool-depends: cpphs:cpphs -any
    hs-source-dirs:     test
    other-modules:
        Codec.Archive.Roundtrip
        Codec.Archive.Test

    default-language:   Haskell2010
    other-extensions:   OverloadedStrings
    ghc-options:
        -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
        -Wredundant-constraints -threaded -rtsopts -with-rtsopts=-N

    build-depends:
        base -any,
        libarchive -any,
        hspec -any,
        bytestring -any,
        directory >=1.2.5.0,
        filepath -any,
        temporary -any,
        mtl >=2.2.2,
        dir-traverse -any,
        composition-prelude >=2.0.5.0,
        pathological-bytestrings -any

    if flag(low-memory)
        cpp-options: -DLOW_MEMORY

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.10)
        ghc-options: -Wunused-packages

benchmark libarchive-bench
    type:             exitcode-stdio-1.0
    main-is:          Bench.hs
    hs-source-dirs:   bench
    default-language: Haskell2010
    ghc-options:
        -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
        -Wredundant-constraints

    build-depends:
        base -any,
        libarchive -any,
        criterion -any,
        bytestring -any,
        tar -any,
        tar-conduit >=0.2.5,
        temporary -any

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.10)
        ghc-options: -Wunused-packages

benchmark mem
    type:             exitcode-stdio-1.0
    main-is:          Mem.hs
    hs-source-dirs:   mem
    default-language: Haskell2010
    ghc-options:
        -Wall -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
        -Wredundant-constraints

    build-depends:
        base -any,
        libarchive -any

    if impl(ghc >=8.4)
        ghc-options: -Wmissing-export-lists

    if impl(ghc >=8.10)
        ghc-options: -Wunused-packages
