cabal-version: 3.0

name: invert
version: 1.0.0.3
x-revision: 1
synopsis: Automatically generate a function’s inverse
category: Functions

description:
    This library deals with computing a function’s inverse.
    The function’s domain must be enumerable, and preferably
    rather small. The function’s codomain must belong to the
    @Eq@ class (even better, @Ord@ or @Hashable@).

author:     Chris Martin
maintainer: Chris Martin, Julie Moronuki

homepage:    https://github.com/typeclasses/invert
bug-reports: https://github.com/typeclasses/invert/issues

license: Apache-2.0
license-file: license.txt

extra-source-files: *.md

common base
    default-language: Haskell2010
    ghc-options: -Wall
    build-depends:
      , base ^>= 4.14 || ^>= 4.15 || ^>= 4.16 || ^>= 4.17 || ^>= 4.18
      , containers ^>= 0.6.4
      , hashable ^>= 1.3.5 || ^>= 1.4
      , unordered-containers ^>= 0.2.17
      , generic-deriving ^>= 1.14.1
      , vector ^>= 0.12.3 || ^>= 0.13

library
    import: base
    exposed-modules:
        Invert
        Invert.Reexport
    other-modules:
        Map
        Vector
    hs-source-dirs: src
    default-extensions:
        NoImplicitPrelude
        NamedFieldPuns
        ExistentialQuantification

test-suite billing-codes-example
    import: base
    type: exitcode-stdio-1.0
    main-is: billing-codes.hs
    hs-source-dirs: examples
    build-depends:
      , invert

benchmark invert-benchmark
    import: base
    default-extensions: NumericUnderscores
    type: exitcode-stdio-1.0
    hs-source-dirs: benchmarks
    main-is: bench.hs
    ghc-options: -O2
    build-depends:
      , invert
      , criterion ^>= 1.6
