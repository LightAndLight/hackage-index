cabal-version: 1.12
name: web3
version: 0.8.3.2
license: BSD3
license-file: LICENSE
copyright: (c) Alexander Krupenkin 2016
maintainer: mail@akru.me
author: Alexander Krupenkin
homepage: https://github.com/airalab/hs-web3#readme
bug-reports: https://github.com/airalab/hs-web3/issues
synopsis: Ethereum API for Haskell
description:
    Web3 is a Haskell client library for Ethereum
category: Network
build-type: Simple
extra-source-files:
    README.md
    CHANGELOG.md
    stack.yaml
    examples/token/ERC20.hs
    examples/token/ERC20.json
    examples/token/Main.hs
    test/contracts/Registry.json
    test/contracts/SimpleStorage.json
    test/contracts/ComplexStorage.json
    test/contracts/Linearization.json

source-repository head
    type: git
    location: https://github.com/airalab/hs-web3

flag compiler
    description:
        Enable Solidity compiler
    default: False
    manual: True

flag debug
    description:
        Enable debug compiler options
    default: False
    manual: True

library
    exposed-modules:
        Crypto.Ethereum
        Crypto.Ethereum.Keyfile
        Crypto.Ethereum.Signature
        Crypto.Ethereum.Utils
        Crypto.Random.HmacDrbg
        Data.ByteArray.HexString
        Data.Solidity.Abi
        Data.Solidity.Abi.Codec
        Data.Solidity.Abi.Generic
        Data.Solidity.Event
        Data.Solidity.Event.Internal
        Data.Solidity.Prim
        Data.Solidity.Prim.Address
        Data.Solidity.Prim.Bool
        Data.Solidity.Prim.Bytes
        Data.Solidity.Prim.Int
        Data.Solidity.Prim.List
        Data.Solidity.Prim.String
        Data.Solidity.Prim.Tagged
        Data.Solidity.Prim.Tuple
        Data.Solidity.Prim.Tuple.TH
        Data.String.Extra
        Language.Solidity.Abi
        Network.Ethereum.Account
        Network.Ethereum.Account.Class
        Network.Ethereum.Account.Default
        Network.Ethereum.Account.Internal
        Network.Ethereum.Account.LocalKey
        Network.Ethereum.Account.Personal
        Network.Ethereum.Account.Safe
        Network.Ethereum.Api.Eth
        Network.Ethereum.Api.Net
        Network.Ethereum.Api.Personal
        Network.Ethereum.Api.Provider
        Network.Ethereum.Api.Types
        Network.Ethereum.Api.Web3
        Network.Ethereum.Chain
        Network.Ethereum.Contract
        Network.Ethereum.Contract.Event
        Network.Ethereum.Contract.Event.Common
        Network.Ethereum.Contract.Event.MultiFilter
        Network.Ethereum.Contract.Event.SingleFilter
        Network.Ethereum.Contract.Method
        Network.Ethereum.Contract.TH
        Network.Ethereum.Ens
        Network.Ethereum.Ens.PublicResolver
        Network.Ethereum.Ens.Registry
        Network.Ethereum.Transaction
        Network.Ethereum.Unit
        Network.Ethereum.Web3
        Network.JsonRpc.TinyClient
    hs-source-dirs: src
    other-modules:
        Paths_web3
    default-language: Haskell2010
    ghc-options: -funbox-strict-fields -Wduplicate-exports
                 -Whi-shadowing -Widentities -Woverlapping-patterns
                 -Wpartial-type-signatures -Wunrecognised-pragmas -Wtyped-holes
                 -Wincomplete-patterns -Wincomplete-uni-patterns -Wmissing-fields
                 -Wmissing-methods -Wmissing-exported-signatures
                 -Wmissing-monadfail-instances -Wmissing-signatures -Wname-shadowing
                 -Wunused-binds -Wunused-top-binds -Wunused-local-binds
                 -Wunused-pattern-binds -Wunused-imports -Wunused-matches
                 -Wunused-foralls -Wtabs
    build-depends:
        OneTuple >=0.2.1 && <0.3,
        aeson >=1.1.2.0 && <1.5,
        async >=2.1.1.1 && <2.3,
        base >4.10 && <4.13,
        basement >=0.0.4 && <0.1,
        bytestring >=0.10.8.1 && <0.11,
        cereal >=0.5.4.0 && <0.6,
        cryptonite >=0.23 && <0.26,
        data-default >=0.7.1.1 && <0.8,
        exceptions >=0.8.3 && <0.11,
        generics-sop >=0.3.1.0 && <0.6,
        http-client >=0.5.7.1 && <0.7,
        http-client-tls >=0.3.5.1 && <0.4,
        machines >=0.6.3 && <0.8,
        memory >=0.14.11 && <0.15,
        microlens >=0.4.8.1 && <0.5,
        microlens-aeson >=2.2.0.2 && <2.4,
        microlens-mtl >=0.1.11.0 && <0.2,
        microlens-th >=0.4.1.1 && <0.5,
        mtl >=2.2.1 && <2.3,
        parsec >=3.1.11 && <3.2,
        relapse >=1.0.0.0 && <2.0,
        tagged >=0.8.5 && <0.9,
        template-haskell >=2.12 && <2.15,
        text >=1.2.2.2 && <1.3,
        transformers >=0.5.2.0 && <0.6,
        uuid-types >=1.0.3 && <1.1,
        vinyl >=0.5.3 && <0.12
    
    if flag(debug)
        ghc-options: -ddump-splices
    
    if flag(compiler)
        cpp-options: -DSOLIDITY_COMPILER
        c-sources:
            ./compiler/cbits/solidity_lite.cpp
        hs-source-dirs: compiler
        other-modules:
            Language.Solidity.Compiler
            Language.Solidity.Compiler.Foreign
        extra-libraries:
            solidity
        include-dirs: ./compiler/cbits
        build-depends:
            containers >=0.6.0.1 && <0.7

test-suite live
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test src
    other-modules:
        Network.Ethereum.Web3.Test.ComplexStorageSpec
        Network.Ethereum.Web3.Test.ERC20Spec
        Network.Ethereum.Web3.Test.LinearizationSpec
        Network.Ethereum.Web3.Test.LocalAccountSpec
        Network.Ethereum.Web3.Test.RegistrySpec
        Network.Ethereum.Web3.Test.SimpleStorageSpec
        Network.Ethereum.Web3.Test.Utils
        Crypto.Ethereum
        Crypto.Ethereum.Keyfile
        Crypto.Ethereum.Signature
        Crypto.Ethereum.Utils
        Crypto.Random.HmacDrbg
        Data.ByteArray.HexString
        Data.Solidity.Abi
        Data.Solidity.Abi.Codec
        Data.Solidity.Abi.Generic
        Data.Solidity.Event
        Data.Solidity.Event.Internal
        Data.Solidity.Prim
        Data.Solidity.Prim.Address
        Data.Solidity.Prim.Bool
        Data.Solidity.Prim.Bytes
        Data.Solidity.Prim.Int
        Data.Solidity.Prim.List
        Data.Solidity.Prim.String
        Data.Solidity.Prim.Tagged
        Data.Solidity.Prim.Tuple
        Data.Solidity.Prim.Tuple.TH
        Data.String.Extra
        Language.Solidity.Abi
        Network.Ethereum.Account
        Network.Ethereum.Account.Class
        Network.Ethereum.Account.Default
        Network.Ethereum.Account.Internal
        Network.Ethereum.Account.LocalKey
        Network.Ethereum.Account.Personal
        Network.Ethereum.Account.Safe
        Network.Ethereum.Api.Eth
        Network.Ethereum.Api.Net
        Network.Ethereum.Api.Personal
        Network.Ethereum.Api.Provider
        Network.Ethereum.Api.Types
        Network.Ethereum.Api.Web3
        Network.Ethereum.Chain
        Network.Ethereum.Contract
        Network.Ethereum.Contract.Event
        Network.Ethereum.Contract.Event.Common
        Network.Ethereum.Contract.Event.MultiFilter
        Network.Ethereum.Contract.Event.SingleFilter
        Network.Ethereum.Contract.Method
        Network.Ethereum.Contract.TH
        Network.Ethereum.Ens
        Network.Ethereum.Ens.PublicResolver
        Network.Ethereum.Ens.Registry
        Network.Ethereum.Transaction
        Network.Ethereum.Unit
        Network.Ethereum.Web3
        Network.JsonRpc.TinyClient
        Paths_web3
    default-language: Haskell2010
    ghc-options: -funbox-strict-fields -Wduplicate-exports
                 -Whi-shadowing -Widentities -Woverlapping-patterns
                 -Wpartial-type-signatures -Wunrecognised-pragmas -Wtyped-holes
                 -Wincomplete-patterns -Wincomplete-uni-patterns -Wmissing-fields
                 -Wmissing-methods -Wmissing-exported-signatures
                 -Wmissing-monadfail-instances -Wmissing-signatures -Wname-shadowing
                 -Wunused-binds -Wunused-top-binds -Wunused-local-binds
                 -Wunused-pattern-binds -Wunused-imports -Wunused-matches
                 -Wunused-foralls -Wtabs -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        OneTuple >=0.2.1 && <0.3,
        aeson >=1.1.2.0 && <1.5,
        async >=2.1.1.1 && <2.3,
        base >4.10 && <4.13,
        basement >=0.0.4 && <0.1,
        bytestring >=0.10.8.1 && <0.11,
        cereal >=0.5.4.0 && <0.6,
        cryptonite >=0.23 && <0.26,
        data-default >=0.7.1.1 && <0.8,
        exceptions >=0.8.3 && <0.11,
        generics-sop >=0.3.1.0 && <0.6,
        hspec >=2.4.4 && <2.8,
        hspec-contrib >=0.4.0 && <0.6,
        hspec-discover >=2.4.4 && <2.8,
        hspec-expectations >=0.8.2 && <0.9,
        http-client >=0.5.7.1 && <0.7,
        http-client-tls >=0.3.5.1 && <0.4,
        machines >=0.6.3 && <0.8,
        memory >=0.14.11 && <0.15,
        microlens >=0.4.8.1 && <0.5,
        microlens-aeson >=2.2.0.2 && <2.4,
        microlens-mtl >=0.1.11.0 && <0.2,
        microlens-th >=0.4.1.1 && <0.5,
        mtl >=2.2.1 && <2.3,
        parsec >=3.1.11 && <3.2,
        random ==1.1.*,
        relapse >=1.0.0.0 && <2.0,
        split >=0.2.3 && <0.3,
        stm >=2.4.4 && <2.6,
        tagged >=0.8.5 && <0.9,
        template-haskell >=2.12 && <2.15,
        text >=1.2.2.2 && <1.3,
        time >=1.6.0 && <1.9,
        transformers >=0.5.2.0 && <0.6,
        uuid-types >=1.0.3 && <1.1,
        vinyl >=0.5.3 && <0.12
    
    if flag(debug)
        ghc-options: -ddump-splices
    
    if flag(compiler)
        cpp-options: -DSOLIDITY_COMPILER
        c-sources:
            ./compiler/cbits/solidity_lite.cpp
        hs-source-dirs: compiler
        other-modules:
            Language.Solidity.Compiler
            Language.Solidity.Compiler.Foreign
        extra-libraries:
            solidity
        include-dirs: ./compiler/cbits
        build-depends:
            containers >=0.6.0.1 && <0.7

test-suite unit
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: unit src
    other-modules:
        Crypto.Ethereum.Test.KeyfileSpec
        Crypto.Ethereum.Test.SignatureSpec
        Crypto.Random.Test.HmacDrbgSpec
        Data.Solidity.Test.AddressSpec
        Data.Solidity.Test.EncodingSpec
        Data.Solidity.Test.IntSpec
        Language.Solidity.Test.CompilerSpec
        Network.Ethereum.Web3.Test.EventSpec
        Network.Ethereum.Web3.Test.MethodDumpSpec
        Crypto.Ethereum
        Crypto.Ethereum.Keyfile
        Crypto.Ethereum.Signature
        Crypto.Ethereum.Utils
        Crypto.Random.HmacDrbg
        Data.ByteArray.HexString
        Data.Solidity.Abi
        Data.Solidity.Abi.Codec
        Data.Solidity.Abi.Generic
        Data.Solidity.Event
        Data.Solidity.Event.Internal
        Data.Solidity.Prim
        Data.Solidity.Prim.Address
        Data.Solidity.Prim.Bool
        Data.Solidity.Prim.Bytes
        Data.Solidity.Prim.Int
        Data.Solidity.Prim.List
        Data.Solidity.Prim.String
        Data.Solidity.Prim.Tagged
        Data.Solidity.Prim.Tuple
        Data.Solidity.Prim.Tuple.TH
        Data.String.Extra
        Language.Solidity.Abi
        Network.Ethereum.Account
        Network.Ethereum.Account.Class
        Network.Ethereum.Account.Default
        Network.Ethereum.Account.Internal
        Network.Ethereum.Account.LocalKey
        Network.Ethereum.Account.Personal
        Network.Ethereum.Account.Safe
        Network.Ethereum.Api.Eth
        Network.Ethereum.Api.Net
        Network.Ethereum.Api.Personal
        Network.Ethereum.Api.Provider
        Network.Ethereum.Api.Types
        Network.Ethereum.Api.Web3
        Network.Ethereum.Chain
        Network.Ethereum.Contract
        Network.Ethereum.Contract.Event
        Network.Ethereum.Contract.Event.Common
        Network.Ethereum.Contract.Event.MultiFilter
        Network.Ethereum.Contract.Event.SingleFilter
        Network.Ethereum.Contract.Method
        Network.Ethereum.Contract.TH
        Network.Ethereum.Ens
        Network.Ethereum.Ens.PublicResolver
        Network.Ethereum.Ens.Registry
        Network.Ethereum.Transaction
        Network.Ethereum.Unit
        Network.Ethereum.Web3
        Network.JsonRpc.TinyClient
        Paths_web3
    default-language: Haskell2010
    ghc-options: -funbox-strict-fields -Wduplicate-exports
                 -Whi-shadowing -Widentities -Woverlapping-patterns
                 -Wpartial-type-signatures -Wunrecognised-pragmas -Wtyped-holes
                 -Wincomplete-patterns -Wincomplete-uni-patterns -Wmissing-fields
                 -Wmissing-methods -Wmissing-exported-signatures
                 -Wmissing-monadfail-instances -Wmissing-signatures -Wname-shadowing
                 -Wunused-binds -Wunused-top-binds -Wunused-local-binds
                 -Wunused-pattern-binds -Wunused-imports -Wunused-matches
                 -Wunused-foralls -Wtabs -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        OneTuple >=0.2.1 && <0.3,
        aeson >=1.1.2.0 && <1.5,
        async >=2.1.1.1 && <2.3,
        base >4.10 && <4.13,
        basement >=0.0.4 && <0.1,
        bytestring >=0.10.8.1 && <0.11,
        cereal >=0.5.4.0 && <0.6,
        cryptonite >=0.23 && <0.26,
        data-default >=0.7.1.1 && <0.8,
        exceptions >=0.8.3 && <0.11,
        generics-sop >=0.3.1.0 && <0.6,
        hspec >=2.4.4 && <2.8,
        hspec-contrib >=0.4.0 && <0.6,
        hspec-discover >=2.4.4 && <2.8,
        hspec-expectations >=0.8.2 && <0.9,
        http-client >=0.5.7.1 && <0.7,
        http-client-tls >=0.3.5.1 && <0.4,
        machines >=0.6.3 && <0.8,
        memory >=0.14.11 && <0.15,
        microlens >=0.4.8.1 && <0.5,
        microlens-aeson >=2.2.0.2 && <2.4,
        microlens-mtl >=0.1.11.0 && <0.2,
        microlens-th >=0.4.1.1 && <0.5,
        mtl >=2.2.1 && <2.3,
        parsec >=3.1.11 && <3.2,
        relapse >=1.0.0.0 && <2.0,
        tagged >=0.8.5 && <0.9,
        template-haskell >=2.12 && <2.15,
        text >=1.2.2.2 && <1.3,
        transformers >=0.5.2.0 && <0.6,
        uuid-types >=1.0.3 && <1.1,
        vinyl >=0.5.3 && <0.12
    
    if flag(debug)
        ghc-options: -ddump-splices
    
    if flag(compiler)
        cpp-options: -DSOLIDITY_COMPILER
        c-sources:
            ./compiler/cbits/solidity_lite.cpp
        hs-source-dirs: compiler
        other-modules:
            Language.Solidity.Compiler
            Language.Solidity.Compiler.Foreign
        extra-libraries:
            solidity
        include-dirs: ./compiler/cbits
        build-depends:
            containers >=0.6.0.1 && <0.7
