cabal-version: >=1.10
name: tropical-geometry
version: 0.0.0
license: GPL-3
license-file: LICENSE
maintainer: Fernando Zhapa
author: Fernando Zhapa
homepage: https://github.com/ferynando7/tropical-geometry#readme
bug-reports: https://github.com/ferynando7/tropical-geometry/issues
synopsis: A Tropical Geometry package for Haskell
description:
    This package includes Tropical algebra and geometry stuff such as tropical numbers, tropical matrices, and tropical polynomials. Also you can find here an algorithm to compute tropical hypersurfaces for polynomials in two variables.
category: Algebra, Geometry, Tropical Geometry
build-type: Simple
extra-source-files:
    CHANGELOG.md
    package.yaml
    README.md
    stack.yaml

source-repository head
    type: git
    location: https://github.com/ferynando7/tropical-geometry

library
    exposed-modules:
        Polynomial.Monomial
        Polynomial.Prelude
        Polynomial.Hypersurface
        Arithmetic.Numbers
        Arithmetic.Matrix
        Geometry.ConvexHull2
        Geometry.ConvexHull3
        Geometry.Polytope
        Geometry.Polyhedral
        Graphics.Drawings
        Core
    hs-source-dirs: library
    other-modules:
        Paths_tropical_geometry
    default-language: Haskell2010
    ghc-options: -Wall -freverse-errors -ferror-spans
    build-depends:
        algebra >=4.3.1 && <4.4,
        base >=4.11.1.0 && <4.12,
        containers >=0.5.11.0 && <0.6,
        gloss >=1.12.0.0 && <1.13,
        lens >=4.16.1 && <4.17,
        matrix >=0.3.6.1 && <0.4,
        numeric-prelude >=0.4.3.1 && <0.5,
        semiring-simple >=1.0.0.1 && <1.1,
        singletons >=2.4.1 && <2.5,
        sized >=0.3.0.0 && <0.4,
        type-natural >=0.8.2.0 && <0.9

executable tropical-geometry
    main-is: Main.hs
    hs-source-dirs: executable
    other-modules:
        Paths_tropical_geometry
    default-language: Haskell2010
    ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
                 -freverse-errors
    build-depends:
        tropical-geometry -any,
        base >=4.11.1.0 && <4.12

test-suite tropical-geometry-test-suite
    type: exitcode-stdio-1.0
    main-is: Main.hs
    hs-source-dirs: test-suite
    other-modules:
        Paths_tropical_geometry
        TPolynomial.TMonomial
        TPolynomial.TPrelude
        TArithmetic.TMatrix
        TArithmetic.TNumbers
        TGeometry.TConvexHull2
        TGeometry.TConvexHull3
        TGeometry.TPolytope
        TGeometry.TPolyhedral
        TPolynomial.THypersurface
    default-language: Haskell2010
    ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
                 -freverse-errors
    build-depends:
        tropical-geometry -any,
        base >=4.11.1.0 && <4.12,
        tasty >=1.1.0.4 && <1.2,
        tasty-hspec >=1.1.5.1 && <1.2,
        tasty-hunit >=0.10.0.1 && <0.11,
        containers >=0.5.11.0 && <0.6,
        hlint-test >=0.1.0.0 && <0.2

benchmark tropical-geometry-benchmarks
    type: exitcode-stdio-1.0
    main-is: Main.hs
    hs-source-dirs: benchmark
    other-modules:
        Paths_tropical_geometry
    default-language: Haskell2010
    ghc-options: -Wall -rtsopts -threaded -with-rtsopts=-N
                 -freverse-errors
    build-depends:
        tropical-geometry -any,
        base >=4.11.1.0 && <4.12,
        criterion >=1.4.1.0 && <1.5
