name:                extensible-effects

-- The package version.  See the Haskell package versioning policy (PVP)
-- for standards guiding when and how versions should be incremented.
-- http://www.haskell.org/haskellwiki/Package_versioning_policy
-- PVP summary:      +-+------- breaking API changes
--                   | | +----- non-breaking API additions
--                   | | | +--- code changes with no API change
version:             2.0.1.0

-- A short (one-line) description of the package.
synopsis:            An Alternative to Monad Transformers

-- A longer description of the package.
description:         This package introduces datatypes for typeclass-constrained effects,
                     as an alternative to monad-transformer based (datatype-constrained)
                     approach of multi-layered effects.
                     For more information, see the original paper at
                     <http://okmij.org/ftp/Haskell/extensible/exteff.pdf>.

                     Any help is appreciated!

stability:           Experimental

-- URL for the project homepage or repository.
homepage:            https://github.com/suhailshergill/extensible-effects

-- The license under which the package is released.
license:             MIT

-- The file containing the license text.
license-file:        LICENSE

-- The package author(s).
author:              Oleg Kiselyov, Amr Sabry, Cameron Swords, Ben Foppa

-- An email address to which users can send suggestions, bug reports, and
-- patches.
maintainer:          suhailshergill@gmail.com

-- A copyright notice.
-- copyright:

category:            Control, Effect

tested-with:         GHC==8.2.1, GHC==8.0.2, GHC==7.10.3, GHC==7.8.4

build-type:          Simple

-- Extra files to be distributed with the package, such as examples or a
-- README.
extra-source-files:  README.md

-- Constraint on the version of Cabal needed to build this package.
cabal-version:       >=1.10

flag lib-Werror
  default: False
  manual: True

flag force-openunion-51
  description:         Force usage of OpenUnion51.hs implementation
  default:             True
  manual:              True

library
  ghc-options:         -Wall
  -- Modules exported by the library.
  exposed-modules:     Control.Eff
                       Control.Eff.Choose
                       Control.Eff.Coroutine
                       Control.Eff.Cut
                       Control.Eff.Example
                       Control.Eff.Exception
                       Control.Eff.Fresh
                       Control.Eff.Lift
                       Control.Eff.NdetEff
                       Control.Eff.Reader.Lazy
                       Control.Eff.Reader.Strict
                       Control.Eff.State.LazyState
                       Control.Eff.State.Lazy
                       Control.Eff.State.Strict
                       Control.Eff.Writer.Lazy
                       Control.Eff.Writer.Strict
                       Control.Eff.Trace
                       Control.Eff.Operational
                       Control.Eff.Operational.Example
                       Data.OpenUnion

  -- Modules included in this library but not exported.
  other-modules:       Data.FTCQueue
  if flag(force-openunion-51)
    cpp-options:       -DFORCE_OU51

  -- LANGUAGE extensions used by modules in this package.
  other-extensions:    BangPatterns
                       , CPP
                       , DeriveDataTypeable
                       , DeriveFunctor
                       , EmptyDataDecls
                       , ExistentialQuantification
                       , FlexibleContexts
                       , FlexibleInstances
                       , FunctionalDependencies
                       , GeneralizedNewtypeDeriving
                       , KindSignatures
                       , MultiParamTypeClasses
                       , NoMonomorphismRestriction
                       , PatternGuards
                       , PolyKinds
                       , RankNTypes
                       , Safe
                       , ScopedTypeVariables
                       , TupleSections
                       , Trustworthy
                       , TypeOperators
                       , UndecidableInstances
  if impl(ghc < 7.8.1)
     other-extensions: OverlappingInstances
  if impl(ghc >= 8.2)
     ghc-options:      -Wno-simplifiable-class-constraints

  -- Other library packages from which modules are imported.
  build-depends:       base >= 4.6 && < 5
                       , type-aligned >= 0.9.3
                       -- For MonadIO instance
                       , transformers >= 0.3 && < 0.6
                       -- For MonadBase instance
                       , transformers-base == 0.4.*

  -- Directories containing source files.
  hs-source-dirs:      src

  -- Base language which the package is written in.
  default-language:    Haskell2010

  -- TODO: uncomment when https://github.com/haskell/cabal/issues/2527 is
  -- resolved
  if flag(lib-Werror)
     ghc-options: -Werror

test-suite extensible-effects-tests
  type: exitcode-stdio-1.0
  main-is: Test.hs
  hs-source-dirs: test/

  ghc-options: -Wall

  build-depends:
                base >= 4.6 && < 5
              , QuickCheck == 2.*
              , HUnit >= 1.2 && < 1.4
              , test-framework == 0.8.*
              , test-framework-hunit == 0.3.*
              , test-framework-quickcheck2 == 0.3.*
              , test-framework-th >= 0.2
              , extensible-effects
              , directory >= 1.2 && < 1.4

  default-language:    Haskell2010

source-repository head
  type: git
  location: https://github.com/suhailshergill/extensible-effects.git
