cabal-version:   1.18
name:            stackctl
version:         1.1.2.1
license:         MIT
license-file:    LICENSE
copyright:       2022 Renaissance Learning Inc
maintainer:      freckle-engineering@renaissance.com
author:          Freckle Engineering
homepage:        https://github.com/freckle/stackctl#readme
bug-reports:     https://github.com/freckle/stackctl/issues
description:     Please see <https://github.com/freckle/stackctl#readme>
build-type:      Simple
extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/freckle/stackctl

library
    exposed-modules:
        Stackctl.Action
        Stackctl.AWS
        Stackctl.AWS.CloudFormation
        Stackctl.AWS.Core
        Stackctl.AWS.EC2
        Stackctl.AWS.Lambda
        Stackctl.AWS.Orphans
        Stackctl.AWS.Scope
        Stackctl.AWS.STS
        Stackctl.CLI
        Stackctl.ColorOption
        Stackctl.Colors
        Stackctl.Commands
        Stackctl.DirectoryOption
        Stackctl.FilterOption
        Stackctl.Options
        Stackctl.ParameterOption
        Stackctl.Prelude
        Stackctl.Prompt
        Stackctl.Sort
        Stackctl.Spec.Capture
        Stackctl.Spec.Cat
        Stackctl.Spec.Changes
        Stackctl.Spec.Changes.Format
        Stackctl.Spec.Deploy
        Stackctl.Spec.Discover
        Stackctl.Spec.Generate
        Stackctl.StackSpec
        Stackctl.StackSpecPath
        Stackctl.StackSpecYaml
        Stackctl.Subcommand
        Stackctl.VerboseOption
        Stackctl.Version
        UnliftIO.Exception.Lens

    hs-source-dirs:     src
    other-modules:      Paths_stackctl
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        DerivingVia FlexibleContexts FlexibleInstances GADTs
        GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses
        NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings
        RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fwrite-ide-info -Weverything -Wno-all-missed-specialisations
        -Wno-missing-import-lists -Wno-missing-kind-signatures
        -Wno-missing-local-signatures -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module -Wno-unsafe
        -optP-Wno-nonportable-include-path

    build-depends:
        Blammo >=1.1.1.1,
        Glob,
        aeson,
        aeson-casing,
        aeson-pretty,
        amazonka,
        amazonka-cloudformation,
        amazonka-core,
        amazonka-ec2,
        amazonka-lambda,
        amazonka-sts,
        base >=4 && <5,
        bytestring,
        cfn-flip >=0.1.0.3,
        conduit,
        containers,
        errors,
        exceptions,
        extra,
        filepath,
        lens,
        lens-aeson,
        monad-logger,
        optparse-applicative,
        resourcet,
        rio,
        text,
        time,
        unliftio,
        unliftio-core,
        unordered-containers,
        uuid,
        yaml

executable stackctl
    main-is:            Main.hs
    hs-source-dirs:     app
    other-modules:      Paths_stackctl
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        DerivingVia FlexibleContexts FlexibleInstances GADTs
        GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses
        NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings
        RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fwrite-ide-info -Weverything -Wno-all-missed-specialisations
        -Wno-missing-import-lists -Wno-missing-kind-signatures
        -Wno-missing-local-signatures -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module -Wno-unsafe
        -optP-Wno-nonportable-include-path -threaded -rtsopts
        -with-rtsopts=-N

    build-depends:
        base >=4 && <5,
        stackctl

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    hs-source-dirs:     test
    other-modules:
        Stackctl.AWS.CloudFormationSpec
        Stackctl.FilterOptionSpec
        Stackctl.StackSpecSpec
        Stackctl.StackSpecYamlSpec
        Paths_stackctl

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        DerivingVia FlexibleContexts FlexibleInstances GADTs
        GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses
        NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings
        RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fwrite-ide-info -Weverything -Wno-all-missed-specialisations
        -Wno-missing-import-lists -Wno-missing-kind-signatures
        -Wno-missing-local-signatures -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module -Wno-unsafe
        -optP-Wno-nonportable-include-path

    build-depends:
        base >=4 && <5,
        hspec,
        stackctl,
        yaml
