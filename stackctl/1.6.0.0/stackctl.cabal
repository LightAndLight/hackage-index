cabal-version:   1.18
name:            stackctl
version:         1.6.0.0
license:         MIT
license-file:    LICENSE
copyright:       2022 Renaissance Learning Inc
maintainer:      freckle-engineering@renaissance.com
author:          Freckle Engineering
homepage:        https://github.com/freckle/stackctl#readme
bug-reports:     https://github.com/freckle/stackctl/issues
description:     Please see <https://github.com/freckle/stackctl#readme>
build-type:      Simple
extra-doc-files:
    README.md
    CHANGELOG.md

source-repository head
    type:     git
    location: https://github.com/freckle/stackctl

library
    exposed-modules:
        Stackctl.Action
        Stackctl.AutoSSO
        Stackctl.AWS
        Stackctl.AWS.CloudFormation
        Stackctl.AWS.Core
        Stackctl.AWS.EC2
        Stackctl.AWS.Lambda
        Stackctl.AWS.Orphans
        Stackctl.AWS.Scope
        Stackctl.AWS.STS
        Stackctl.CLI
        Stackctl.ColorOption
        Stackctl.Colors
        Stackctl.Commands
        Stackctl.Config
        Stackctl.Config.RequiredVersion
        Stackctl.DirectoryOption
        Stackctl.FilterOption
        Stackctl.OneOrListOf
        Stackctl.Options
        Stackctl.ParameterOption
        Stackctl.Prelude
        Stackctl.Prompt
        Stackctl.RemovedStack
        Stackctl.Sort
        Stackctl.Spec.Capture
        Stackctl.Spec.Cat
        Stackctl.Spec.Changes
        Stackctl.Spec.Changes.Format
        Stackctl.Spec.Deploy
        Stackctl.Spec.Discover
        Stackctl.Spec.Generate
        Stackctl.Spec.List
        Stackctl.StackDescription
        Stackctl.StackSpec
        Stackctl.StackSpecPath
        Stackctl.StackSpecYaml
        Stackctl.Subcommand
        Stackctl.TagOption
        Stackctl.VerboseOption
        Stackctl.Version

    hs-source-dirs:     src
    other-modules:      Paths_stackctl
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        DerivingVia FlexibleContexts FlexibleInstances GADTs
        GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses
        NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings
        RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -fwrite-ide-info -Weverything
        -Wno-all-missed-specialisations -Wno-missed-specialisations
        -Wno-missing-import-lists -Wno-missing-kind-signatures
        -Wno-missing-local-signatures -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module -Wno-unsafe
        -optP-Wno-nonportable-include-path

    build-depends:
        Blammo >=1.1.2.1,
        Glob >=0.10.2,
        QuickCheck >=2.14.2,
        aeson >=2.0.3.0,
        aeson-casing >=0.2.0.0,
        aeson-pretty >=0.8.9,
        amazonka >=2.0,
        amazonka-cloudformation >=2.0,
        amazonka-core >=2.0,
        amazonka-ec2 >=2.0,
        amazonka-lambda >=2.0,
        amazonka-mtl >=0.1.1.0,
        amazonka-sso >=2.0,
        amazonka-sts >=2.0,
        base >=4 && <5,
        bytestring >=0.11.3.1,
        cfn-flip >=0.1.0.3,
        conduit >=1.3.4.3,
        containers >=0.6.5.1,
        envparse >=0.5.0,
        errors >=2.3.0,
        exceptions >=0.10.4,
        extra >=1.7.12,
        filepath >=1.4.2.2,
        lens >=5.1.1,
        lens-aeson >=1.2.2,
        monad-logger >=0.3.37,
        mtl >=2.2.2,
        optparse-applicative >=0.17.0.0,
        resourcet >=1.2.6,
        rio >=0.1.22.0,
        semigroups >=0.20,
        text >=1.2.5.0,
        text-metrics >=0.3.2,
        time >=1.11.1.1,
        transformers >=0.5.6.2,
        typed-process >=0.2.10.1,
        unliftio >=0.2.25.0,
        unordered-containers >=0.2.19.1,
        uuid >=1.3.15,
        yaml >=0.11.8.0

executable stackctl
    main-is:            Main.hs
    hs-source-dirs:     app
    other-modules:      Paths_stackctl
    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        DerivingVia FlexibleContexts FlexibleInstances GADTs
        GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses
        NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings
        RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -fwrite-ide-info -Weverything
        -Wno-all-missed-specialisations -Wno-missed-specialisations
        -Wno-missing-import-lists -Wno-missing-kind-signatures
        -Wno-missing-local-signatures -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module -Wno-unsafe
        -optP-Wno-nonportable-include-path -threaded -rtsopts
        -with-rtsopts=-N

    build-depends:
        base >=4 && <5,
        stackctl

test-suite spec
    type:               exitcode-stdio-1.0
    main-is:            Spec.hs
    hs-source-dirs:     test
    other-modules:
        Stackctl.AWS.CloudFormationSpec
        Stackctl.AWS.ScopeSpec
        Stackctl.Config.RequiredVersionSpec
        Stackctl.ConfigSpec
        Stackctl.FilterOptionSpec
        Stackctl.OneOrListOfSpec
        Stackctl.Spec.Changes.FormatSpec
        Stackctl.StackDescriptionSpec
        Stackctl.StackSpecSpec
        Stackctl.StackSpecYamlSpec
        Paths_stackctl

    default-language:   Haskell2010
    default-extensions:
        BangPatterns DataKinds DeriveAnyClass DeriveFoldable DeriveFunctor
        DeriveGeneric DeriveLift DeriveTraversable DerivingStrategies
        DerivingVia FlexibleContexts FlexibleInstances GADTs
        GeneralizedNewtypeDeriving LambdaCase MultiParamTypeClasses
        NoImplicitPrelude NoMonomorphismRestriction OverloadedStrings
        RankNTypes RecordWildCards ScopedTypeVariables StandaloneDeriving
        TypeApplications TypeFamilies

    ghc-options:
        -fignore-optim-changes -fwrite-ide-info -Weverything
        -Wno-all-missed-specialisations -Wno-missed-specialisations
        -Wno-missing-import-lists -Wno-missing-kind-signatures
        -Wno-missing-local-signatures -Wno-missing-safe-haskell-mode
        -Wno-prepositive-qualified-module -Wno-unsafe
        -optP-Wno-nonportable-include-path

    build-depends:
        Glob >=0.10.2,
        QuickCheck >=2.14.2,
        aeson >=2.0.3.0,
        base >=4 && <5,
        bytestring >=0.11.3.1,
        filepath >=1.4.2.2,
        hspec >=2.9.7,
        hspec-golden >=0.2.1.0,
        mtl >=2.2.2,
        stackctl,
        yaml >=0.11.8.0
