name:                itemfield
version:             1.2.3.0
synopsis:            A brick Widget for selectable summary of many elements on a terminal
description:         

  This module provides a brick Widget that can be used with the
  brick package to handle situations where there are lots of items
  to represent to the user along with a corresponding state for each
  item.  In addition, the user should be able to use the cursor
  keys and space bar to mark one or more items (presumably so other
  code can get the list of marked elements and perform a
  state-changing operation).
  .
  * 0.1.0.0  -- initial version
  .
  * 0.2.0.0  -- more key events: Shift+Arrow to mark/unmark with move
  .
  * 0.2.0.1  -- more key events: G=toggle group, A=toggle all, !=mrk bad; add helpMsg
  .
  * 0.3.0.0  -- rename from StateFieldSelector to ItemField
  .
  * 0.3.1.0  -- add + key selector to select all successful targets
  .
  * 1.0.0.0  -- update from vty-ui to brick
  .
  * 1.1.0.0  -- add support for 's' and 'f' keys and mouse events
  .
  * 1.2.0.0  -- intermediary state is "Pending"
  .
  * 1.2.1.0  -- added compatibility for older distributions
  .
  * 1.2.2.0  -- added compatibility for GHC7.10
  .
  * 1.2.2.1  -- added stack.yaml for using itemfield with stack
  .
  * 1.2.3.0  -- added brick 0.16 compatibility

license:             BSD3
license-file:        LICENSE
author:              Kevin Quick <quick@sparq.org>
maintainer:          quick@sparq.org
copyright:           Kevin Quick, 2013-2017
category:            Graphics, Console
build-type:          Simple
cabal-version:       >=1.8

extra-source-files: compat/brick_015plus/TextUI/ItemField/BrickHelper.hs
                    compat/brick_pre015/TextUI/ItemField/BrickHelper.hs
                    compat/brick_016/TextUI/ItemField/BrickHelper.hs
                    compat/base_pre48/Compat.hs
                    compat/base48/Compat.hs

library
  hs-source-dirs:    src
  exposed-modules:     TextUI.ItemField
  other-modules:       TextUI.ItemField.Types
                     , TextUI.ItemField.Attrs
                     , TextUI.ItemField.Operations
                     , TextUI.ItemField.Layout
                     , TextUI.ItemField.BrickHelper
                     , Compat
  build-depends:       vty
                     , text < 1.3
                     , microlens < 0.5
  if flag(base48)
    build-depends:   base >= 4.8 && < 5.0
    hs-source-dirs:  compat/base48
  else
    build-depends:   base < 4.8
    hs-source-dirs:  compat/base_pre48
  if flag(brick016)
    build-depends:   brick >= 0.16
    hs-source-dirs:  compat/brick_016
  else
    if flag(brick015)
      build-depends:   brick >= 0.15 && < 0.16
      hs-source-dirs:  compat/brick_015plus
    else
      build-depends:   brick >= 0.13 && < 0.15
      hs-source-dirs:  compat/brick_pre015
  ghc-options:       -Wall -fno-warn-unused-do-bind -O3


Flag examples
     Description: Build example programs
     Default:     True

Flag brick016
     Description: true for Brick version 0.16 or later
     Default: True

Flag brick015
     Description: true for Brick version 0.15.x
     Default: True

Flag base48
     Description: base 4.8 or later
     Default: True

Flag base49
     Description: base 4.8 or later
     Default: True


executable bookcase
           if !flag(examples)
              Buildable: False
           hs-source-dirs: examples
           if flag(base48)
             build-depends:   base >= 4.8 && < 5.0
             hs-source-dirs:  compat/base48
           else
             build-depends:   base < 4.8
             hs-source-dirs:  compat/base_pre48
           if flag(brick016)
             build-depends:   brick >= 0.16
             hs-source-dirs:  compat/brick_016
           else
             if flag(brick015)
               build-depends:   brick >= 0.15 && < 0.16
               hs-source-dirs:  compat/brick_015plus
             else
               build-depends:   brick >= 0.13 && < 0.15
               hs-source-dirs:  compat/brick_pre015
           ghc-options:  -threaded -Wall -fno-warn-unused-do-bind -O3
           -- default-language: Haskell2010
           main-is: bookcase.hs
           build-depends: vty, text, microlens, microlens-th
                        , itemfield, data-default


executable workreport
           if !flag(examples)
              Buildable: False
           hs-source-dirs: examples
           if flag(base49)
             build-depends:   base >= 4.9 && < 5.0
             hs-source-dirs:  compat/base48
           else
             if flag(base48)
               build-depends:   base >= 4.8 && < 5.0, transformers
               hs-source-dirs:  compat/base48
             else
               build-depends:   base < 4.8, transformers
               hs-source-dirs:  compat/base_pre48
           if flag(brick016)
             build-depends:   brick >= 0.16
             hs-source-dirs:  compat/brick_016
           else
             if flag(brick015)
               build-depends:   brick >= 0.15 && < 0.16
               hs-source-dirs:  compat/brick_015plus
             else
               build-depends:   brick >= 0.13 && < 0.15
               hs-source-dirs:  compat/brick_pre015
           ghc-options:  -threaded -Wall -fno-warn-unused-do-bind -O3
           -- default-language: Haskell2010
           main-is: workreport.hs
           other-modules: TextUI.ItemField.BrickHelper
                        , Compat
           build-depends: vty, text, microlens, microlens-th
                        , itemfield, data-default, random


test-suite test_itemfield
           type: exitcode-stdio-1.0
           main-is: test_itemfield.hs
           hs-source-dirs: test, src
           if flag(base48)
             build-depends:   base >= 4.8 && < 5.0
             hs-source-dirs:  compat/base48
           else
             build-depends:   base < 4.8
             hs-source-dirs:  compat/base_pre48
           if flag(brick016)
             build-depends:   brick >= 0.16
             hs-source-dirs:  compat/brick_016
           else
             if flag(brick015)
               build-depends:   brick >= 0.15 && < 0.16
               hs-source-dirs:  compat/brick_015plus
             else
               build-depends:   brick >= 0.13 && < 0.15
               hs-source-dirs:  compat/brick_pre015
           ghc-options:  -threaded -O0
           other-modules: TextUI.ItemField.Types
                        , TextUI.ItemField.Attrs
                        , TextUI.ItemField.Operations
                        , TextUI.ItemField.Layout
                        , TextUI.ItemField.BrickHelper
                        , TextUI.ItemField
                        , Compat
                        , TestDataGen
           build-depends: HUnit, QuickCheck
                        , test-framework, test-framework-hunit
                        , test-framework-quickcheck2
                        , brick >= 0.13 && < 1.0
                        , vty, text, microlens, microlens-th

test-suite test_layout
           type: exitcode-stdio-1.0
           main-is: test_layout.hs
           hs-source-dirs: test, src
           if flag(base48)
             build-depends:   base >= 4.8 && < 5.0
             hs-source-dirs:  compat/base48
           else
             build-depends:   base < 4.8
             hs-source-dirs:  compat/base_pre48
           if flag(brick016)
             build-depends:   brick >= 0.16
             hs-source-dirs:  compat/brick_016
           else
             if flag(brick015)
               build-depends:   brick >= 0.15 && < 0.16
               hs-source-dirs:  compat/brick_015plus
             else
               build-depends:   brick >= 0.13 && < 0.15
               hs-source-dirs:  compat/brick_pre015
           ghc-options:  -threaded -O0
           other-modules: TextUI.ItemField.Types
                        , TextUI.ItemField.Attrs
                        , TextUI.ItemField.Operations
                        , TextUI.ItemField.Layout
                        , TextUI.ItemField.BrickHelper
                        , TextUI.ItemField
                        , Compat
                        , TestDataGen
           cpp-options: -DTEST
           build-depends: HUnit, QuickCheck
                        , test-framework, test-framework-hunit
                        , test-framework-quickcheck2
                        , brick >= 0.13 && < 1.0
                        , vty, text, microlens, microlens-th
                        , data-default

source-repository head
  type: darcs
  location: http://hub.darcs.net/kquick/itemfield
