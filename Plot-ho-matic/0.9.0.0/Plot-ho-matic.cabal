name:                Plot-ho-matic
version:             0.9.0.0
synopsis:            Real-time line plotter for generic data
license:             BSD3
license-file:        LICENSE
author:              Greg Horn
maintainer:          gregmainland@gmail.com
copyright:           Copyright (c) 2013-2015, Greg Horn
category:            Graphics
build-type:          Simple
cabal-version:       >=1.10
extra-source-files:
  .gitignore
  .travis.yml
  CHANGELOG.md
  README.md
description: {
Plot-ho-matic provides real-time plotting of time-series data with a simple interface ('addHistoryChannel').
There is also a more general interface to plot arbitrary data ('addChannel').
.
See the example on <http://www.github.com/ghorn/Plot-ho-matic> to help get started.
}

library
  hs-source-dirs:    src
  default-language:  Haskell2010
  exposed-modules:   PlotHo, SetHo
  other-modules:     PlotHo.GraphWidget,
                     PlotHo.PlotChart,
                     PlotHo.PlotTypes,
                     SetHo.LookupTree
                     SetHo.OptionsWidget
  build-depends:     base >= 4.6.0.0 && < 5
                     , containers
                     , lens
                     , data-default-class
                     , glib
                     , gtk3 >= 0.14.2
                     , time
                     , Chart >= 1.1
                     , Chart-cairo >= 1.1
                     , cairo
                     , text
                     , vector
                     , transformers
                     , generic-accessors >= 0.6.0.0

  ghc-options:      -O2 -Wall
  ghc-prof-options: -O2 -Wall -prof -fprof-auto -fprof-cafs -rtsopts


flag examples
    description:    build the examples
    default:        False

executable plot-example
  if flag(examples)
    Buildable: True
  else
    Buildable: False
  hs-source-dirs:      examples
  main-is:             PlotExample.hs
  default-language:    Haskell2010
  build-depends:       base >= 4.5 && < 5
                       , Plot-ho-matic
                       , containers

  ghc-options:         -O2 -with-rtsopts=-T
  ghc-prof-options:    -O2 -Wall -prof -fprof-auto -fprof-cafs -rtsopts

executable set-example
  if flag(examples)
    Buildable: True
  else
    Buildable: False
  hs-source-dirs:      examples
  main-is:             SetExample.hs
  default-language:    Haskell2010
  build-depends:       base >= 4.5 && < 5
                       , Plot-ho-matic
                       , generic-accessors

  ghc-options:         -O2 -with-rtsopts=-T
  ghc-prof-options:    -O2 -Wall -prof -fprof-auto -fprof-cafs -rtsopts
