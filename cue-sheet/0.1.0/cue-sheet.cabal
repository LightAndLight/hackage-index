--
-- Cabal configuration for ‘cue-sheet’ package.
--
-- Copyright © 2016 Mark Karpov <markkarpov@openmailbox.org>
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are
-- met:
--
-- * Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
-- * Redistributions in binary form must reproduce the above copyright
--   notice, this list of conditions and the following disclaimer in the
--   documentation and/or other materials provided with the distribution.
--
-- * Neither the name Mark Karpov nor the names of contributors may be used
--   to endorse or promote products derived from this software without
--   specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS “AS IS” AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
-- ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

name:                 cue-sheet
version:              0.1.0
x-revision: 1
cabal-version:        >= 1.10
license:              BSD3
license-file:         LICENSE.md
author:               Mark Karpov <markkarpov@openmailbox.org>
maintainer:           Mark Karpov <markkarpov@openmailbox.org>
homepage:             https://github.com/mrkkrp/cue-sheet
bug-reports:          https://github.com/mrkkrp/cue-sheet/issues
category:             Audio
synopsis:             Support for construction, rendering, and parsing of CUE sheets
build-type:           Simple
description:          Support for construction, rendering, and parsing of CUE sheets.
extra-doc-files:      CHANGELOG.md
                    , README.md
data-files:           cue-sheet-samples/*.cue

source-repository head
  type:               git
  location:           https://github.com/mrkkrp/cue-sheet.git

flag dev
  description:        Turn on development settings.
  manual:             True
  default:            False

library
  build-depends:      QuickCheck       >= 2.4    && < 2.10
                    , base             >= 4.8    && < 5.0
                    , bytestring       >= 0.10.8 && < 0.11
                    , containers       >= 0.5.6.2 && < 0.6
                    , data-default-class
                    , exceptions       >= 0.6    && < 0.9
                    , megaparsec       >= 5.1.1  && < 6.0
                    , mtl              >= 2.0    && < 3.0
                    , text             >= 0.2    && < 1.3
  if !impl(ghc >= 8.0)
    build-depends:    semigroups       == 0.18.*

  exposed-modules:    Text.CueSheet
                    , Text.CueSheet.Types
                    , Text.CueSheet.Parser
                    , Text.CueSheet.Render
  if flag(dev)
    ghc-options:      -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010

test-suite tests
  main-is:            Spec.hs
  other-modules:      Text.CueSheet.ParserSpec
                    , Text.CueSheet.RenderSpec
                    , Text.CueSheet.TypesSpec
  hs-source-dirs:     tests
  type:               exitcode-stdio-1.0
  build-depends:      QuickCheck       >= 2.4    && < 3.0
                    , base             >= 4.8    && < 5.0
                    , bytestring       >= 0.10.8 && < 0.11
                    , cue-sheet        >= 0.1
                    , exceptions       >= 0.6    && < 0.9
                    , hspec            >= 2.0    && < 3.0
                    , hspec-megaparsec >= 0.3    && < 0.4
                    , text             >= 0.2    && < 1.3
  if !impl(ghc >= 8.0)
    build-depends:    semigroups       == 0.18.*

  if flag(dev)
    ghc-options:      -Wall -Werror
  else
    ghc-options:      -O2 -Wall
  default-language:   Haskell2010
