-- Copyright 2016 Ertugrul Söylemez
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

name:     timelike
version:  0.2.2
category: Data, System
synopsis: Type classes for types representing time

maintainer:   Ertugrul Söylemez <esz@posteo.de>
author:       Ertugrul Söylemez <esz@posteo.de>
copyright:    Copyright 2016 Ertugrul Söylemez
homepage:     http://hub.darcs.net/esz/timelike
bug-reports:  http://hub.darcs.net/esz/timelike/issues
license:      Apache
license-file: LICENSE

description:  This library defines a set of type classes for generic
    time arithmetic.  It supports universal time types like @UTCTime@
    from the <http://hackage.haskell.org/package/time time library> as
    well as physical time types like the ones from
    <http://hackage.haskell.org/package/clock clock>.

build-type:         Simple
cabal-version:      >= 1.10
extra-source-files: CHANGELOG.md NOTICE README.md

source-repository head
    type:     darcs
    location: http://hub.darcs.net/esz/timelike


library
    build-depends:
        base                >= 4.8 && < 5,
        transformers        >= 0.4 && < 1
    default-language: Haskell2010
    ghc-options: -W -fdefer-typed-holes
    exposed-modules:
        Data.Time.Class
