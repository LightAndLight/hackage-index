name:                lol-cpp
-- The package version.  See the Haskell package versioning policy (PVP)
-- for standards guiding when and how versions should be incremented.
-- http://www.haskell.org/haskellwiki/Package_versioning_policy
-- PVP summary:      +-+------- breaking API changes
--                   | | +----- non-breaking API additions
--                   | | | +--- code changes with no API change
version:             0.0.0.1
synopsis:            A fast C++ backend for <https://hackage.haskell.org/package/lol Λ ∘ λ>.
homepage:            https://github.com/cpeikert/Lol
Bug-Reports:         https://github.com/cpeikert/Lol/issues
license:             GPL-2
license-file:        LICENSE
author:              Eric Crockett <ecrockett0@gmail.com>, Chris Peikert <cpeikert@alum.mit.edu>
maintainer:          Eric Crockett <ecrockett0@gmail.com>
copyright:           Eric Crockett, Chris Peikert
category:            Crypto
stability:           experimental
build-type:          Simple
extra-source-files:  README, CHANGES.md,
                     Crypto/Lol/Cyclotomic/Tensor/CPP/*.h,
                     Crypto/Lol/Cyclotomic/Tensor/CPP/*.cpp
cabal-version:       >= 1.10
description:
    Λ ∘ λ (Lol) is a general-purpose library for ring-based lattice cryptography.
    This package provides a C++ implementation of Lol's Tensor interface.
source-repository head
  type: git
  location: https://github.com/cpeikert/Lol

-- For information on compiling C with cabal: http://blog.ezyang.com/2010/06/setting-up-cabal-the-ffi-and-c2hs/

Flag llvm
  Description:  Compile via LLVM. This produces much better object code,
                but you need to have the LLVM compiler installed.
  -- If you enable this and get errors like "Error: can't resolve `.rodata' {.rodata section}"
  -- then GHC doesn't like your version of LLVM!
  Default:      False

Flag opt
  Description: Turn on library optimizations
  Default:     True

library
  default-language:   Haskell2010
  ghc-options: -fwarn-dodgy-imports
  cc-options: -std=c++11
  Include-dirs: Crypto/Lol/Cyclotomic/Tensor/CPP
  -- Due to #12152, the file containing the definition of `Zq::q` must be linked first,
  -- otherwise dynamic linking (`cabal repl` or `stack ghci`) results in the error:
  -- "Loading temp shared object failed: /tmp/ghc54651_0/libghc_1.so: undefined symbol _ZN2Zq1qE"
  -- For `cabal repl`, we can simply reorder the list so that the file that should be linked
  -- first comes first in the list. However `stack ghci` always links alphabetically,
  -- so we really just have to define `Zq::q` in the first file alphabetically.
  C-sources: Crypto/Lol/Cyclotomic/Tensor/CPP/common.cpp,
             Crypto/Lol/Cyclotomic/Tensor/CPP/crt.cpp,
             Crypto/Lol/Cyclotomic/Tensor/CPP/g.cpp,
             Crypto/Lol/Cyclotomic/Tensor/CPP/l.cpp,
             Crypto/Lol/Cyclotomic/Tensor/CPP/mul.cpp,
             Crypto/Lol/Cyclotomic/Tensor/CPP/norm.cpp,
             Crypto/Lol/Cyclotomic/Tensor/CPP/random.cpp,
             Crypto/Lol/Cyclotomic/Tensor/CPP/zq.cpp

  if flag(llvm)
    ghc-options: -fllvm -optlo-O3

  -- ghc optimizations
  if flag(opt)
    -- makes lift much faster!
    ghc-options: -funfolding-use-threshold1000
  exposed-modules:
    Crypto.Lol.Cyclotomic.Tensor.CPP

  other-modules:
    Crypto.Lol.Cyclotomic.Tensor.CPP.Backend
    Crypto.Lol.Cyclotomic.Tensor.CPP.Extension
    Crypto.Lol.Cyclotomic.Tensor.CPP.Instances

  build-depends:
    arithmoi >= 0.4.1.3,
    base >= 4.9 && < 5,
    bytestring,
    constraints,
    containers >= 0.5.6.2,
    crypto-api,
    data-default >= 0.3.0,
    deepseq >= 1.4.1.1,
    lol >= 0.6.0.0,
    monadcryptorandom,
    MonadRandom >= 0.2,
    mtl >= 2.2.1,
    numeric-prelude >= 0.4.2,
    protocol-buffers,
    protocol-buffers-descriptor,
    random >= 1.1,
    reflection >= 1.5.1,
    repa>=3.4,
    singletons >= 1.1.2.1,
    th-desugar >= 1.5.4,
    tagged-transformer >= 0.7,
    template-haskell  >=  2.2.0.0,
    transformers >= 0.4.2.0,
    vector>=0.11,
    vector-th-unbox >= 0.2.1.0

  other-extensions: TemplateHaskell

Benchmark bench-lol-cpp
  type:             exitcode-stdio-1.0
  default-language: Haskell2010
  main-is:          BenchCPPMain.hs
  ghc-options:      -main-is BenchCPPMain
  hs-source-dirs:   benchmarks

  ghc-options: -O2 -funfolding-creation-threshold=15000 -funfolding-use-threshold=1000

  build-depends:
    base >= 4.9 && < 5,
    DRBG,
    lol >= 0.6.0.0,
    lol-benches,
    lol-cpp

test-suite test-lol-cpp
  type:             exitcode-stdio-1.0
  default-language: Haskell2010
  main-is:          TestCPPMain.hs
  ghc-options:      -main-is TestCPPMain
  hs-source-dirs:   tests
  ghc-options:      -threaded -O2

  build-depends:
    base >= 4.9 && < 5,
    lol-cpp,
    lol-tests
