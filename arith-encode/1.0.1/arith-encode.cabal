name: arith-encode
version: 1.0.1
x-revision: 1
cabal-version: >=1.16
license: BSD3
license-file: LICENSE
copyright: Copyright (c) 2014 Eric McCorkle.  All rights reserved.
maintainer: Eric McCorkle <emc2@metricspace.net>, Andrew Lelechenko <andrew.lelechenko@gmail.com>
author: Eric McCorkle
stability: Stable
homepage: https://github.com/emc2/arith-encode
bug-reports: https://github.com/emc2/arith-encode/issues
synopsis: A practical arithmetic encoding (aka Godel numbering) library.
description:
  A library providing tools and various schemes for encoding arbitrary datatypes
  as natural numbers.  The underlying theory is that of isomorphisms with the natural
  numbers (known as Godel numbering).  The library provides functionality for defining
  multiple such encodings for a given datatype, as well as a collection of stock
  encodings and combinators which can be used to build more complex encodings.
  .
  This has various uses, among them binary serialization/deserialization and
  enumeration testing.
category: Data, Serialization, Test, Testing
build-type: Simple
tested-with: GHC==8.8.1, GHC==8.6.5, GHC==8.4.4, GHC==8.2.2, GHC==8.0.2

source-repository head
  type: git
  location: git@github.com:Bodigrim/arith-encode.git

library
  exposed-modules:
    Data.ArithEncode
    Data.ArithEncode.Basic
    Data.ArithEncode.Binary
    Data.ArithEncode.Util
  hs-source-dirs: src
  default-language: Haskell2010
  build-depends:
    base >=4.9 && <4.15,
    arithmoi >= 0.5 && <0.11,
    array,
    binary,
    containers,
    hashable,
    integer-logarithms,
    unordered-containers

test-suite UnitTest
  type: exitcode-stdio-1.0
  main-is: UnitTest.hs
  other-modules:
    Tests.Data
    Tests.Data.ArithEncode
    Tests.Data.ArithEncode.Basic
    Tests.Data.ArithEncode.Binary
    Tests.Data.ArithEncode.TestUtils
    Tests.Data.ArithEncode.Util
  hs-source-dirs: test
  default-language: Haskell2010
  build-depends:
    base >=4.4 && <5,
    arith-encode,
    binary,
    containers,
    hashable,
    HUnit-Plus,
    unordered-containers
