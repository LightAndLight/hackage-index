name:           mixed-types-num
version:        0.3.0.1
cabal-version:  >= 1.9.2
build-type:     Simple
homepage:       https://github.com/michalkonecny/mixed-types-num
author:         Michal Konecny
maintainer:     Michal Konecny <mikkonecny@gmail.com>
copyright:      (c) 2015-2017 Michal Konecny
license:        BSD3
license-file:   LICENSE
extra-source-files:  changelog.md
stability:      experimental
tested-with:    GHC==7.10.3, GHC==8.0.2, GHC==8.2.1
category:       Math
synopsis:       Alternative Prelude with numeric and logic expressions typed bottom-up
Description:
    This package provides a version of Prelude where
    unary and binary operations such as @not@, @+@, @==@
    have their result type derived from the parameter type(s)
    and thus supports mixed-type arithmetic and comparisons.
    .
    See module "MixedTypesNumPrelude" for further documentation.
    .
    /Ghci 8.0.* fails when loading this package/ due to ghc bug <https://ghc.haskell.org/trac/ghc/ticket/13385#ticket 13385>.
    This bug does not affect ghci 7.10.3 and ghci 8.2.1.

source-repository head
  type:     git
  location: https://github.com/mikkonecny/mixed-types-num

library
  hs-source-dirs:  src
  build-depends:
    base >= 4.8 && < 4.11
    , convertible >= 1.1.1.0 && < 1.2
    , template-haskell
    , hspec >= 2.1 && < 2.5
    , hspec-smallcheck >= 0.3 && < 0.5
    , smallcheck == 1.1.*
    , QuickCheck >= 2.7 && < 2.11
  ghc-options:     -Wall -fno-warn-orphans
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    TypeFamilies,
    TypeOperators,
    ConstraintKinds,
    DefaultSignatures,
    MultiParamTypeClasses,
    FlexibleContexts,
    FlexibleInstances,
    UndecidableInstances
  exposed-modules:
    Utils.TH.DeclForTypes
    Control.CollectErrors
    Numeric.CollectErrors
    Numeric.MixedTypes.PreludeHiding
    Numeric.MixedTypes.Literals
    Numeric.MixedTypes.Bool
    Numeric.MixedTypes.Eq
    Numeric.MixedTypes.Ord
    Numeric.MixedTypes.MinMaxAbs
    Numeric.MixedTypes.AddSub
    Numeric.MixedTypes.Round
    Numeric.MixedTypes.Ring
    Numeric.MixedTypes.Field
    Numeric.MixedTypes.Elementary
    Numeric.MixedTypes.Complex
    MixedTypesNumPrelude


test-suite spec
  type:
      exitcode-stdio-1.0
  ghc-options:
      -Wall
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    FlexibleContexts
  hs-source-dirs:
      test
  main-is:
    Spec.hs
  other-modules:
    Numeric.MixedTypes.AddSubSpec
    Numeric.MixedTypes.BoolSpec
    Numeric.MixedTypes.EqOrdSpec
    Numeric.MixedTypes.FieldSpec
    Numeric.MixedTypes.LiteralsSpec
    Numeric.MixedTypes.MinMaxAbsSpec
    Numeric.MixedTypes.RingSpec
    Numeric.MixedTypes.RoundSpec
  build-depends:
    base >= 4.8 && < 4.10
    , mixed-types-num
    , hspec >= 2.1 && < 2.5
    , hspec-smallcheck >= 0.3 && < 0.5
    , QuickCheck >= 2.7 && < 2.11
