name:           mixed-types-num
version:        0.4.0
cabal-version:  >= 1.9.2
build-type:     Simple
homepage:       https://github.com/michalkonecny/mixed-types-num
author:         Michal Konecny
maintainer:     Michal Konecny <mikkonecny@gmail.com>
copyright:      (c) 2015-2019 Michal Konecny
license:        BSD3
license-file:   LICENSE
extra-source-files:  changelog.md
stability:      experimental
category:       Math
synopsis:       Alternative Prelude with numeric and logic expressions typed bottom-up
Description:
    This package provides a version of Prelude where
    unary and binary operations such as @not@, @+@, @==@
    have their result type derived from the parameter type(s)
    and thus supports mixed-type arithmetic and comparisons.
    .
    Partial operations such as division, sqrt and power
    do not throw exceptions even when errors such as division by zero
    occur.  Instead, these errors are propagated bottom-up in
    a bespoke error-accumulating functor.
    .
    This library is a by-product of developing the
    <https://github.com/michalkonecny/aern2 AERN2> library for interval and exact real computation.
    Certain aspects are specifically tailored for interval or exact real arithmetics,
    including three-valued numerical comparisons
    and distinguishing potential and certain errors.
    .
    See module "MixedTypesNumPrelude" for further documentation.
    .
    /Ghci 8.0.* fails when loading this package/ due to ghc bug <https://ghc.haskell.org/trac/ghc/ticket/13385#ticket 13385>.
    This bug does not affect ghci 7.10.3 and ghci 8.2.* and above.

source-repository head
  type:     git
  location: https://github.com/mikkonecny/mixed-types-num

library
  hs-source-dirs:  src
  build-depends:
    base >= 4.8 && < 5
    -- , convertible >= 1.1.1.0
    , template-haskell
    , mtl
    , hspec >= 2.1
    , hspec-smallcheck >= 0.3
    , smallcheck == 1.1.*
    , QuickCheck >= 2.7
  ghc-options:     -Wall -fno-warn-orphans
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    TypeFamilies,
    TypeOperators,
    ConstraintKinds,
    DefaultSignatures,
    MultiParamTypeClasses,
    FlexibleContexts,
    FlexibleInstances,
    UndecidableInstances
  exposed-modules:
    Utils.TH.DeclForTypes
    Utils.Test.EnforceRange
    Data.Convertible.Base
    Data.Convertible.Utils
    Data.Convertible.Instances.Num
    Control.CollectErrors
    Numeric.CollectErrors
    Numeric.MixedTypes.PreludeHiding
    Numeric.MixedTypes.Literals
    Numeric.MixedTypes.Bool
    Numeric.MixedTypes.Eq
    Numeric.MixedTypes.Ord
    Numeric.MixedTypes.MinMaxAbs
    Numeric.MixedTypes.AddSub
    Numeric.MixedTypes.Round
    Numeric.MixedTypes.Ring
    Numeric.MixedTypes.Field
    Numeric.MixedTypes.Elementary
    Numeric.MixedTypes.Complex
    MixedTypesNumPrelude


test-suite spec
  type:
      exitcode-stdio-1.0
  ghc-options:
      -Wall
  extensions:
    RebindableSyntax,
    PostfixOperators,
    ScopedTypeVariables,
    FlexibleContexts
  hs-source-dirs:
      test
  main-is:
    Spec.hs
  other-modules:
    Numeric.MixedTypes.AddSubSpec
    Numeric.MixedTypes.BoolSpec
    Numeric.MixedTypes.EqOrdSpec
    Numeric.MixedTypes.FieldSpec
    Numeric.MixedTypes.LiteralsSpec
    Numeric.MixedTypes.MinMaxAbsSpec
    Numeric.MixedTypes.RingSpec
    Numeric.MixedTypes.RoundSpec
  build-depends:
    base >= 4.8 && < 5
    , mixed-types-num
    , hspec >= 2.1
    , hspec-smallcheck >= 0.3
    , QuickCheck >= 2.7
