name:                proxied
version:             0.1.1
synopsis:            Make functions consume Proxy instead of undefined
description:         @proxied@ is a simple library that exports a function to
                     convert constant functions to ones that take a @proxy@
                     value. This is useful for retrofiting typeclasses that
                     have functions that return a constant value for any value
                     of a particular type (but still need to consume some
                     value, since one of the parameterized types must appear
                     in a typeclass function). Often, these functions are
                     given @undefined@ as an argument, which might be
                     considered poor design.
                     .
                     @Proxy@, however, does not carry any of the
                     error-throwing risks of @undefined@, so it is much more
                     preferable to take @Proxy@ as an argument to a constant
                     function instead of @undefined@. Unfortunately, @Proxy@
                     was included in @base@ until GHC 7.8, so many of @base@'s
                     typeclasses still contain constant functions that aren't
                     amenable to passing @Proxy@. @proxied@ addresses this
                     issue by providing variants of those typeclass functions
                     that take an explicit @proxy@ value.
homepage:            https://github.com/RyanGlScott/proxied
bug-reports:         https://github.com/RyanGlScott/proxied/issues
license:             BSD3
license-file:        LICENSE
author:              Ryan Scott
maintainer:          Ryan Scott <ryan.gl.scott@gmail.com>
stability:           Provisional
copyright:           (C) 2016 Ryan Scott
category:            Data
build-type:          Simple
tested-with:         GHC == 7.0.4
                   , GHC == 7.2.2
                   , GHC == 7.4.2
                   , GHC == 7.6.3
                   , GHC == 7.8.4
                   , GHC == 7.10.3
                   , GHC == 8.0.1
extra-source-files:  CHANGELOG.md, README.md
cabal-version:       >=1.10

source-repository head
  type:                git
  location:            https://github.com/RyanGlScott/proxied

library
  exposed-modules:     Data.Proxied
  build-depends:       base             >= 4.3    && < 5
                     , generic-deriving >= 1.10.1 && < 2
                     , tagged           >= 0.4.4  && < 1
  hs-source-dirs:      src
  default-language:    Haskell2010
  ghc-options:         -Wall
