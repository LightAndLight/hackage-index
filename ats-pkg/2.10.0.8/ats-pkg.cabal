cabal-version: 1.18
name: ats-pkg
version: 2.10.0.8
x-revision: 2
license: BSD3
license-file: LICENSE
copyright: Copyright: (c) 2018 Vanessa McHale
maintainer: vamchale@gmail.com
author: Vanessa McHale
homepage: https://github.com/vmchale/atspkg#readme
bug-reports: https://github.com/vmchale/atspkg/issues
synopsis: A build tool for ATS
description:
    A collection of scripts to simplify building ATS projects.
category: Development, ATS
build-type: Custom
extra-source-files:
    man/atspkg.1
    dhall/config.dhall
    dhall/atslib.dhall
extra-doc-files: README.md
                 docs/manual.tex

source-repository head
    type: git
    location: git@github.com:vmchale/atspkg.git

custom-setup
    setup-depends: base -any,
                   Cabal >=2.2,
                   cli-setup >=0.2.0.1

flag development
    description:
        Enable `-Werror`
    default: False
    manual: True

flag no-executable
    description:
        Enable `-Werror`
    default: False

library
    exposed-modules:
        Language.ATS.Package
        Distribution.ATS
    hs-source-dirs: src
    other-modules:
        Paths_ats_pkg
        Distribution.ATS.Build
        Language.ATS.Package.Build
        Language.ATS.Package.Build.C
        Language.ATS.Package.Compiler
        Language.ATS.Package.Config
        Language.ATS.Package.Dependency
        Language.ATS.Package.Dhall
        Language.ATS.Package.Error
        Language.ATS.Package.Generic
        Language.ATS.Package.PackageSet
        Language.ATS.Package.Type
        Language.ATS.Package.Upgrade
        Language.ATS.Package.Debian
        Distribution.ATS.Version
        System.Process.Ext
        Quaalude
    default-language: Haskell2010
    ghc-options: -Wall -Wincomplete-uni-patterns
                 -Wincomplete-record-updates
    build-depends:
        base >=4.11 && <5,
        http-client -any,
        bytestring -any,
        file-embed -any,
        shake < 0.18.4,
        bzlib -any,
        Cabal >=2.2.0.0,
        lzma -any,
        tar -any,
        zlib -any,
        http-client-tls -any,
        text -any,
        directory -any,
        process -any,
        hashable -any,
        containers -any,
        parallel-io -any,
        unix -any,
        mtl -any,
        dhall >=1.11.0 && <=1.12.0,
        ansi-wl-pprint -any,
        shake-ats >=1.8.0.0 && < 1.10.1.0,
        shake-ext >=2.6.0.0,
        composition-prelude >=1.3.0.0,
        zip-archive -any,
        ansi-wl-pprint -any,
        binary -any,
        lens,
        dependency -any,
        filemanip -any
    
    if flag(development)
        ghc-options: -Werror

executable atspkg
    main-is: Main.hs
    hs-source-dirs: app
    other-modules:
        Paths_ats_pkg
    default-language: Haskell2010
    ghc-options: -threaded -rtsopts -with-rtsopts=-N -Wall
                 -Wincomplete-uni-patterns -Wincomplete-record-updates
    
    if !flag(no-executable)
        build-depends:
            base -any,
            ats-pkg -any,
            optparse-applicative -any,
            shake-ats -any,
            lens,
            temporary -any,
            directory -any,
            composition-prelude -any,
            text -any,
            shake -any
    
    if flag(no-executable)
        buildable: False
    
    if flag(development)
        ghc-options: -Werror
