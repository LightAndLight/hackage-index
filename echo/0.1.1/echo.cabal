name:                echo
version:             0.1.1
synopsis:            A cross-platform, cross-console way to handle echoing terminal input
description:         The @base@ library exposes the @hGetEcho@ and @hSetEcho@ functions
                     for querying and setting echo status, but unfortunately, neither
                     function works with MinTTY consoles on Windows. This is a serious
                     issue, since @hGetEcho@ and @hSetEcho@ are often used to disable
                     input echoing when a program prompts for a password, so many
                     programs will reveal your password as you type it on MinTTY!
                     .
                     This library provides an alternative interface which works
                     with both MinTTY and other consoles. An example is included
                     which demonstrates how one might prompt for a password using
                     this library. To build it, make sure to configure with the
                     @-fexample@ flag.
homepage:            https://github.com/RyanGlScott/echo
bug-reports:         https://github.com/RyanGlScott/echo/issues
license:             BSD3
license-file:        LICENSE
author:              Ryan Scott
maintainer:          Ryan Scott <ryan.gl.scott@gmail.com>
stability:           Provisional
copyright:           (C) 2016-2017 Ryan Scott
category:            System
build-type:          Simple
extra-source-files:  CHANGELOG.md, README.md, include/*.h
cabal-version:       >=1.10

source-repository head
  type:                git
  location:            https://github.com/RyanGlScott/echo

flag Win32-2-5
  description:         Use Win32-2.5.0.0 or later.
  default:             True

flag example
  description:         Build the bundled example program.
  default:             False

library
  exposed-modules:     System.IO.Echo
                       System.IO.Echo.Internal

  build-depends:       base >= 4.3 && < 5
                     , process
  if os(windows)
    cpp-options:       "-DWINDOWS"

    if flag(Win32-2-5)
      build-depends:   Win32 >= 2.5
    else
      build-depends:   filepath
                     , Win32 < 2.5
      build-tools:     hsc2hs
      include-dirs:    include
      includes:        windows_cconv.h, winternl_compat.h
      other-modules:   System.IO.Echo.MinTTY
      extra-libraries: ntdll

  hs-source-dirs:      src
  default-language:    Haskell2010
  ghc-options:         -Wall

executable password
  if !flag(example)
    buildable:         False

  main-is:             Password.hs
  build-depends:       base >= 4.3 && < 5
                     , echo
  hs-source-dirs:      example
  default-language:    Haskell2010
  ghc-options:         -Wall
