cabal-version: 1.12

name:           tinytools
version:        0.1.0.5
description:    tinytools is a mono-space unicode diagram editor
homepage:       https://github.com/minimapletinytools/tinytools#readme
bug-reports:    https://github.com/minimapletinytools/tinytools/issues
author:         minimaple
maintainer:     minimapletinytools@gmail.com
synopsis:       tinytools is a monospace unicode diagram editor
category:       User Interfaces
copyright:      2023 minimaple (Peter Lu)
license:        BSD3
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    README.md
    ChangeLog.md


source-repository head
  type: git
  location: https://github.com/minimapletinytools/tinytools

library
  exposed-modules:
      Potato.Data.Text.Unicode
      Potato.Data.Text.Zipper
      Potato.Data.Text.Zipper2
      Potato.Flow
      Potato.Flow.Attachments
      Potato.Flow.BroadPhase
      Potato.Flow.Cmd
      Potato.Flow.Configuration
      Potato.Flow.Controller
      Potato.Flow.Controller.Goat
      Potato.Flow.Controller.Handler
      Potato.Flow.Controller.Input
      Potato.Flow.Controller.Manipulator.Box
      Potato.Flow.Controller.Manipulator.BoxText
      Potato.Flow.Controller.Manipulator.Common
      Potato.Flow.Controller.Manipulator.Layers
      Potato.Flow.Controller.Manipulator.Line
      Potato.Flow.Controller.Manipulator.Pan
      Potato.Flow.Controller.Manipulator.Select
      Potato.Flow.Controller.Manipulator.TextArea
      Potato.Flow.Controller.Manipulator.TextInputState
      Potato.Flow.Controller.OwlLayers
      Potato.Flow.Controller.Types
      Potato.Flow.DebugHelpers
      Potato.Flow.Deprecated.Layers
      Potato.Flow.Deprecated.State
      Potato.Flow.Deprecated.TestStates
      Potato.Flow.Preview
      Potato.Flow.Llama
      Potato.Flow.Math
      Potato.Flow.Methods.LineDrawer
      Potato.Flow.Methods.LineTypes
      Potato.Flow.Methods.TextCommon
      Potato.Flow.Methods.Types
      Potato.Flow.Methods.LlamaWorks
      Potato.Flow.Owl
      Potato.Flow.OwlHelpers
      Potato.Flow.OwlItem
      Potato.Flow.OwlState
      Potato.Flow.OwlWorkspace
      Potato.Flow.Reflex
      Potato.Flow.Reflex.GoatSwitcher
      Potato.Flow.Reflex.GoatWidget
      Potato.Flow.Render
      Potato.Flow.RenderCache
      Potato.Flow.Methods.SEltMethods
      Potato.Flow.Serialization.Snake
      Potato.Flow.Serialization.SnakeWrangler
      Potato.Flow.Serialization.Versions.V1.SElts
      Potato.Flow.TestStates
      Potato.Flow.Types
  hs-source-dirs:
      src
  default-extensions:
      ApplicativeDo
      BangPatterns
      DataKinds
      ConstraintKinds
      DeriveFoldable
      DeriveFunctor
      DeriveTraversable
      DeriveGeneric
      DeriveLift
      DeriveTraversable
      DerivingStrategies
      EmptyCase
      ExistentialQuantification
      FlexibleContexts
      FlexibleInstances
      FunctionalDependencies
      GADTs
      GeneralizedNewtypeDeriving
      InstanceSigs
      KindSignatures
      LambdaCase
      MultiParamTypeClasses
      MultiWayIf
      NamedFieldPuns
      OverloadedStrings
      PatternSynonyms
      RankNTypes
      ScopedTypeVariables
      StandaloneDeriving
      TupleSections
      TypeApplications
      TypeFamilies
      TypeFamilyDependencies
      TypeOperators
      NoImplicitPrelude
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -fno-ignore-asserts
  build-depends:
      MonadRandom
    , aeson
    , aeson-pretty
    , base >=4.7 && <5
    , bimap
    , binary
    , bytestring
    , constraints-extras
    , containers
    , data-default
    , data-ordlist
    , deepseq
    , dependent-map
    , dependent-sum
    , dependent-sum-template
    , extra
    , hashable
    , ilist
    , lens
    , linear
    , listsafe
    , mtl
    , patch
    , pretty-simple
    , random-shuffle
    , ref-tf
    , reflex >= 0.9.2 && < 1
    , reflex-potatoes >=0.1
    , reflex-test-host >=0.1.2.3
    , relude
    , semialign
    , text
    , text-icu
    , these
    , vector
    , vty
    , filepath
    --, uuid
  default-language: Haskell2010

test-suite tinytools-test
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
      Potato.Data.Text.UnicodeSpec
      Potato.Data.Text.Zipper2Spec
      Potato.Data.Text.ZipperSpec
      Potato.Flow.AttachmentsSpec
      Potato.Flow.BroadPhaseSpec
      Potato.Flow.Common
      Potato.Flow.Controller.Manipulator.BoxSpec
      Potato.Flow.Controller.Manipulator.LayersSpec
      Potato.Flow.Controller.Manipulator.LineSpec
      Potato.Flow.Controller.Manipulator.PanSpec
      Potato.Flow.Controller.Manipulator.TestHelpers
      Potato.Flow.Deprecated.Controller.EverythingWidgetSpec
      Potato.Flow.Deprecated.Controller.LayersSpec
      Potato.Flow.Deprecated.Controller.Manipulator.BoxSpec
      Potato.Flow.Deprecated.Controller.Manipulator.BoxTextSpec
      Potato.Flow.Deprecated.Controller.Manipulator.LayersSpec
      Potato.Flow.Deprecated.Controller.Manipulator.SelectSpec
      Potato.Flow.Deprecated.Controller.Manipulator.TextAreaSpec
      Potato.Flow.Deprecated.Controller.ManipulatorSpec
      Potato.Flow.Deprecated.LayersSpec
      Potato.Flow.Deprecated.StateSpec
      Potato.Flow.GoatCmdSpec
      Potato.Flow.GoatTester
      Potato.Flow.LlamaSpec
      Potato.Flow.MathSpec
      Potato.Flow.Methods.LineDrawerSpec
      Potato.Flow.OwlSpec
      Potato.Flow.RenderSpec
      Potato.Flow.Methods.SEltMethodsSpec
      Paths_tinytools
  hs-source-dirs:
      test
  default-extensions:
      ApplicativeDo
      BangPatterns
      DataKinds
      ConstraintKinds
      DeriveFoldable
      DeriveFunctor
      DeriveTraversable
      DeriveGeneric
      DeriveLift
      DeriveTraversable
      DerivingStrategies
      EmptyCase
      ExistentialQuantification
      FlexibleContexts
      FlexibleInstances
      FunctionalDependencies
      GADTs
      GeneralizedNewtypeDeriving
      InstanceSigs
      KindSignatures
      LambdaCase
      MultiParamTypeClasses
      MultiWayIf
      NamedFieldPuns
      OverloadedStrings
      PatternSynonyms
      RankNTypes
      ScopedTypeVariables
      StandaloneDeriving
      TupleSections
      TypeApplications
      TypeFamilies
      TypeFamilyDependencies
      TypeOperators
      NoImplicitPrelude
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -threaded -rtsopts -with-rtsopts=-N -fno-ignore-asserts
  build-depends:
      HUnit
    , MonadRandom
    , aeson
    , base >=4.7 && <5
    , bimap
    , binary
    , bytestring
    , constraints-extras
    , containers
    , data-default
    , data-ordlist
    , deepseq
    , dependent-map
    , dependent-sum
    , dependent-sum-template
    , extra
    , hashable
    , hspec
    , hspec-contrib
    , ilist
    , lens
    , linear
    , listsafe
    , mtl
    , patch
    , pretty-simple
    , random-shuffle
    , ref-tf
    , reflex >= 0.9.2 && < 1
    , reflex-potatoes >=0.1
    , reflex-test-host >=0.1.2.3
    , relude
    , semialign
    , text
    , text-icu
    , these
    , tinytools
    , vector
    , vty
  default-language: Haskell2010
