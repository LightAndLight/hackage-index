cabal-version: 1.12
name: taskell
version: 1.8.2.0
license: BSD3
license-file: LICENSE
copyright: 2019 Mark Wales
maintainer: mark@smallhadroncollider.com
author: Mark Wales
homepage: https://github.com/smallhadroncollider/taskell#readme
bug-reports: https://github.com/smallhadroncollider/taskell/issues
synopsis: A command-line kanban board/task manager
description:
    Please see the README on GitHub at <https://github.com/smallhadroncollider/taskell#readme>
category: Command Line Tools
build-type: Simple
extra-source-files:
    README.md
    templates/api-error.txt
    templates/bindings.ini
    templates/config.ini
    templates/github-token.txt
    templates/theme.ini
    templates/trello-token.txt
    templates/usage.txt

source-repository head
    type: git
    location: https://github.com/smallhadroncollider/taskell

library
    exposed-modules:
        App
        Events.State
        IO.Config
        IO.Taskell
        Data.Taskell.Date
        Data.Taskell.Date.RelativeParser
        Data.Taskell.List
        Data.Taskell.List.Internal
        Data.Taskell.Lists
        Data.Taskell.Lists.Internal
        Data.Taskell.Seq
        Data.Taskell.Subtask
        Data.Taskell.Subtask.Internal
        Data.Taskell.Task
        Data.Taskell.Task.Internal
        Events.Actions.Types
        Events.State.History
        Events.State.Types
        Events.State.Types.Mode
        IO.Config.Markdown
        IO.Markdown.Internal
        IO.HTTP.GitHub
        IO.HTTP.Trello.List
        IO.HTTP.Trello.ChecklistItem
        IO.Keyboard
        IO.Keyboard.Parser
        IO.Keyboard.Types
        UI.Draw.Field
        Types
    hs-source-dirs: src
    other-modules:
        Config
        Events.Actions
        Events.Actions.Insert
        Events.Actions.Modal
        Events.Actions.Modal.Detail
        Events.Actions.Modal.Due
        Events.Actions.Modal.Help
        Events.Actions.Modal.MoveTo
        Events.Actions.Normal
        Events.Actions.Search
        Events.State.Modal.Detail
        Events.State.Modal.Due
        IO.Config.General
        IO.Config.GitHub
        IO.Config.Layout
        IO.Config.Parser
        IO.Config.Trello
        IO.HTTP.Aeson
        IO.HTTP.GitHub.Card
        IO.HTTP.GitHub.Column
        IO.HTTP.GitHub.Project
        IO.HTTP.Trello
        IO.HTTP.Trello.Card
        IO.Markdown
        UI.CLI
        UI.Draw
        UI.Draw.Main
        UI.Draw.Main.List
        UI.Draw.Main.Search
        UI.Draw.Main.StatusBar
        UI.Draw.Modal
        UI.Draw.Modal.Detail
        UI.Draw.Modal.Due
        UI.Draw.Modal.Help
        UI.Draw.Modal.MoveTo
        UI.Draw.Mode
        UI.Draw.Task
        UI.Draw.Types
        UI.Theme
        UI.Types
        Utility.Parser
        Paths_taskell
    default-language: Haskell2010
    default-extensions: OverloadedStrings NoImplicitPrelude
    build-depends:
        aeson >=1.4.5.0 && <1.5,
        attoparsec >=0.13.2.3 && <0.14,
        base >=4.12.0.0 && <=5,
        brick ==0.50.*,
        bytestring >=0.10.8.2 && <0.11,
        classy-prelude >=1.5.0 && <1.6,
        config-ini >=0.2.4.0 && <0.3,
        containers >=0.6.0.1 && <0.7,
        directory >=1.3.3.0 && <1.4,
        file-embed >=0.0.11 && <0.1,
        fold-debounce >=0.2.0.9 && <0.3,
        http-client >=0.6.4 && <0.7,
        http-conduit >=2.3.7.3 && <2.4,
        http-types >=0.12.3 && <0.13,
        lens >=4.17.1 && <4.18,
        mtl >=2.2.2 && <2.3,
        template-haskell >=2.14.0.0 && <2.15,
        text >=1.2.3.1 && <1.3,
        time >=1.8.0.2 && <1.9,
        tz >=0.1.3.2 && <0.2,
        vty ==5.26.*

executable taskell
    main-is: Main.hs
    hs-source-dirs: app
    other-modules:
        Paths_taskell
    default-language: Haskell2010
    default-extensions: OverloadedStrings NoImplicitPrelude
    ghc-options: -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        base >=4.12.0.0 && <4.13,
        classy-prelude >=1.5.0 && <1.6,
        taskell -any,
        tz >=0.1.3.2 && <0.2

test-suite taskell-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    other-modules:
        Data.Taskell.Date.RelativeParserTest
        Data.Taskell.DateTest
        Data.Taskell.ListNavigationTest
        Data.Taskell.ListsTest
        Data.Taskell.ListTest
        Data.Taskell.SeqTest
        Data.Taskell.SubtaskTest
        Data.Taskell.TaskTest
        Events.State.HistoryTest
        Events.StateTest
        IO.GitHubTest
        IO.Keyboard.ParserTest
        IO.Keyboard.TypesTest
        IO.KeyboardTest
        IO.MarkdownTest
        IO.TrelloTest
        UI.FieldTest
        Paths_taskell
    default-language: Haskell2010
    default-extensions: OverloadedStrings NoImplicitPrelude
    ghc-options: -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        aeson >=1.4.5.0 && <1.5,
        base >=4.12.0.0 && <4.13,
        classy-prelude >=1.5.0 && <1.6,
        containers >=0.6.0.1 && <0.7,
        file-embed >=0.0.11 && <0.1,
        lens >=4.17.1 && <4.18,
        mtl >=2.2.2 && <2.3,
        raw-strings-qq ==1.1.*,
        taskell -any,
        tasty >=1.2.3 && <1.3,
        tasty-discover >=4.2.1 && <4.3,
        tasty-expected-failure >=0.11.1.2 && <0.12,
        tasty-hunit >=0.10.0.2 && <0.11,
        text >=1.2.3.1 && <1.3,
        time >=1.8.0.2 && <1.9,
        tz >=0.1.3.2 && <0.2,
        vty ==5.26.*
