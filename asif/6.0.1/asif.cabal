cabal-version:  2.2
name:           asif
version:        6.0.1
description:    Library for creating and querying segmented feeds. Please see <https://github.com/arbor/asif#readme>
synopsis:       Library for creating and querying segmented feeds
category:       Services
homepage:       https://github.com/arbor/asif#readme
bug-reports:    https://github.com/arbor/asif/issues
author:         Arbor Networks
maintainer:     mayhem@arbor.net
copyright:      Arbor Networks
license:        MIT
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    ChangeLog.md
    README.md

source-repository head
  type: git
  location: https://github.com/arbor/asif

common common-deps
  default-language:    Haskell2010
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -msse4.2
  build-depends:
      attoparsec
    , base >=4.7 && <5
    , binary
    , bytestring
    , conduit
    , conduit-combinators
    , conduit-extra
    , containers
    , cpu
    , either
    , exceptions
    , foldl
    , generic-lens
    , hw-bits
    , hw-ip
    , lens
    , network
    , old-locale
    , profunctors
    , resourcet
    , temporary-resourcet
    , text
    , thyme
    , transformers
    , vector


library
  import: common-deps
  exposed-modules:
    Arbor.File.Format.Asif
    Arbor.File.Format.Asif.ByIndex
    Arbor.File.Format.Asif.ByteString
    Arbor.File.Format.Asif.ByteString.Builder
    Arbor.File.Format.Asif.ByteString.Lazy
    Arbor.File.Format.Asif.Data.Ip
    Arbor.File.Format.Asif.Data.Read
    Arbor.File.Format.Asif.Extract
    Arbor.File.Format.Asif.Format
    Arbor.File.Format.Asif.Format.Text
    Arbor.File.Format.Asif.Get
    Arbor.File.Format.Asif.IO
    Arbor.File.Format.Asif.Lookup
    Arbor.File.Format.Asif.Segment
    Arbor.File.Format.Asif.Type
    Arbor.File.Format.Asif.Whatever
    Arbor.File.Format.Asif.Write
  other-modules:
    Arbor.File.Format.Asif.Format.SegmentValue
    Arbor.File.Format.Asif.Format.Type
    Arbor.File.Format.Asif.List
    Arbor.File.Format.Asif.Maybe
    Arbor.File.Format.Asif.Text
    Paths_asif
  autogen-modules:
    Paths_asif
  hs-source-dirs:
    src
  default-extensions: BangPatterns FlexibleContexts FlexibleInstances GeneralizedNewtypeDeriving MultiParamTypeClasses OverloadedStrings TupleSections
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints
  default-language: Haskell2010

executable asif
  import: common-deps
  main-is: Main.hs
  other-modules:
    App.Commands
    App.Commands.Dump
    App.Commands.DumpBitmap
    App.Commands.DumpOnly
    App.Commands.EncodeFiles
    App.Commands.ExtractFiles
    App.Commands.ExtractSegments
    App.Commands.Ls
    App.Commands.Options.Type
    App.Dump
    App.IO
    Paths_asif
  autogen-modules:
    Paths_asif
  hs-source-dirs:
    app
  default-extensions: BangPatterns FlexibleContexts FlexibleInstances GeneralizedNewtypeDeriving MultiParamTypeClasses OverloadedStrings TupleSections
  ghc-options: -Wall -Wcompat -Wincomplete-record-updates -Wincomplete-uni-patterns -Wredundant-constraints -threaded -rtsopts -O2 -msse4.2
  build-depends:
      asif
    , directory
    , optparse-applicative
  default-language: Haskell2010

test-suite asif-test
  import: common-deps
  type: exitcode-stdio-1.0
  main-is: Spec.hs
  other-modules:
    Arbor.File.Format.Asif.ByteString.BuilderSpec
    Arbor.File.Format.Asif.Data.IpSpec
    Arbor.File.Format.Asif.ExtractSpec
    Arbor.File.Format.Asif.Format.SegmentValueSpec
    Arbor.File.Format.Asif.Format.TextSpec
    Arbor.File.Format.Asif.WriteSpec
    Arbor.File.Format.AsifSpec
    Arbor.TestUtils
    Gen.Feed
    TestApp
    Paths_asif
  autogen-modules:
    Paths_asif
  hs-source-dirs:
    test
  default-extensions: BangPatterns FlexibleContexts FlexibleInstances GeneralizedNewtypeDeriving MultiParamTypeClasses OverloadedStrings TupleSections
  ghc-options: -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      asif
    , hedgehog
    , hspec
    , hw-hspec-hedgehog
  default-language: Haskell2010
