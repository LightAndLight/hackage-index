name:           extensible-effects-concurrent
version:        0.10.0
description:    Please see the README on GitHub at <https://github.com/sheyll/extensible-effects-concurrent#readme>
synopsis:       Message passing concurrency as extensible-effect
homepage:       https://github.com/sheyll/extensible-effects-concurrent#readme
bug-reports:    https://github.com/sheyll/extensible-effects-concurrent/issues
author:         Sven Heyll
maintainer:     sven.heyll@gmail.com
category:       Concurrency, Control, Effect
tested-with:    GHC==8.4.3, GHC==8.4.4
copyright:      Copyright Sven Heyll
license:        BSD3
license-file:   LICENSE
build-type:     Simple
cabal-version:  2.0

extra-doc-files:
    docs/extensible-effects-concurrent-test-Loop-without-space-leaks.png
    docs/extensible-effects-concurrent-test-Loop-WITH-space-leaks.png

extra-source-files:
    stack.yaml
    stack.nightly-2018-05-29.yaml
    ChangeLog.md
    README.md
    brittany.yaml
    .travis.yml

source-repository head
  type: git
  location: https://github.com/sheyll/extensible-effects-concurrent

library
  hs-source-dirs:
      src
  build-depends:
      async >= 2.2 && <3,
      base >=4.7 && <4.12,
      data-default >= 0.7 && < 0.8,
      deepseq >= 1.4 && < 1.5,
      enclosed-exceptions >= 1.0 && < 1.1,
      filepath >= 1.4 && < 1.5,
      time >= 1.8 && < 1.9,
      mtl >= 2.2 && < 2.3,
      containers >=0.5.8 && <0.7,
      QuickCheck <2.12,
      lens >= 4.14 && < 4.18,
      parallel >= 3.2 && < 3.3,
      process >= 1.6 && < 1.7,
      monad-control >= 1.0 && < 1.1,
      extensible-effects >= 3.1 && <4,
      stm >= 2.4.5 && <2.6,
      transformers-base >= 0.4 && < 0.5
  autogen-modules: Paths_extensible_effects_concurrent
  exposed-modules:
                  Control.Eff.Loop,
                  Control.Eff.ExceptionExtra,
                  Control.Eff.Log,
                  Control.Eff.Log.Channel,
                  Control.Eff.Log.Handler,
                  Control.Eff.Log.Message,
                  Control.Eff.Concurrent,
                  Control.Eff.Concurrent.Api,
                  Control.Eff.Concurrent.Api.Client,
                  Control.Eff.Concurrent.Api.Server,
                  Control.Eff.Concurrent.Process,
                  Control.Eff.Concurrent.Process.ForkIOScheduler,
                  Control.Eff.Concurrent.Process.Interactive,
                  Control.Eff.Concurrent.Process.SingleThreadedScheduler,
                  Control.Eff.Concurrent.Api.Observer
                  Control.Eff.Concurrent.Api.Observer.Queue
  other-modules:
                Control.Eff.Concurrent.Api.Internal,
                Paths_extensible_effects_concurrent
  default-extensions:
                     BangPatterns,
                     ConstraintKinds,
                     DeriveFoldable,
                     DeriveFunctor,
                     DeriveFunctor,
                     DeriveGeneric,
                     DeriveTraversable,
                     FlexibleContexts,
                     FlexibleInstances,
                     FunctionalDependencies,
                     GADTs,
                     GeneralizedNewtypeDeriving,
                     LambdaCase,
                     MultiParamTypeClasses,
                     RankNTypes,
                     ScopedTypeVariables,
                     StandaloneDeriving,
                     TemplateHaskell,
                     TupleSections,
                     TypeApplications,
                     TypeFamilies,
                     TypeInType,
                     TypeOperators,
                     ViewPatterns
  default-language: Haskell2010
  ghc-options: -Wall -fno-full-laziness

executable extensible-effects-concurrent-example-1
  main-is: Main.hs
  hs-source-dirs: examples/example-1
  build-depends:
                base
              , data-default
              , extensible-effects-concurrent
              , extensible-effects
              , lens
  ghc-options: -Wall -threaded -fno-full-laziness -rtsopts -with-rtsopts=-N
  default-language: Haskell2010
  default-extensions:   BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators

executable extensible-effects-concurrent-example-2
  main-is: Main.hs
  hs-source-dirs: examples/example-2
  build-depends:
                base
              , data-default
              , extensible-effects-concurrent
              , extensible-effects
              , lens
  ghc-options: -Wall -threaded -fno-full-laziness -rtsopts -with-rtsopts=-N
  default-language: Haskell2010
  default-extensions:   BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators

executable extensible-effects-concurrent-example-3
  main-is: Main.hs
  hs-source-dirs: examples/example-3
  build-depends:
                base
              , data-default
              , directory
              , extensible-effects-concurrent
              , extensible-effects
              , filepath
              , lens
  ghc-options: -Wall -threaded -fno-full-laziness -rtsopts -with-rtsopts=-N
  default-language: Haskell2010
  default-extensions:   BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators

executable extensible-effects-concurrent-example-4
  main-is: Main.hs
  hs-source-dirs: examples/example-4
  build-depends:
                base
              , extensible-effects-concurrent
              , extensible-effects
              , deepseq
  ghc-options: -Wall -threaded -fno-full-laziness -rtsopts -with-rtsopts=-N
  default-language: Haskell2010
  default-extensions:   BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators

test-suite extensible-effects-concurrent-test
  type: exitcode-stdio-1.0
  main-is: Driver.hs
  hs-source-dirs: test
  other-modules:
                Common
              , Debug
              , ForkIOScheduler
              , Interactive
              , LoggingTests
              , LoopTests
              , ProcessBehaviourTestCases
              , SingleThreadedScheduler
  ghc-options: -Wall -threaded -fno-full-laziness -rtsopts -with-rtsopts=-N
  build-depends:
                base >=4.7 && <4.12
              , data-default
              , extensible-effects-concurrent
              , extensible-effects >= 3
              , tasty
              , tasty-discover
              , tasty-hunit
              , containers
              , deepseq
              , QuickCheck <2.12
              , lens
              , HUnit
              , stm
  default-language: Haskell2010
  default-extensions:   BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators
