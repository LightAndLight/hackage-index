cabal-version:  2.0
name:           extensible-effects-concurrent
version:        0.24.0
description:    Please see the README on GitHub at <https://github.com/sheyll/extensible-effects-concurrent#readme>
synopsis:       Message passing concurrency as extensible-effect
homepage:       https://github.com/sheyll/extensible-effects-concurrent#readme
bug-reports:    https://github.com/sheyll/extensible-effects-concurrent/issues
author:         Sven Heyll
maintainer:     sven.heyll@gmail.com
category:       Concurrency, Control, Effect
tested-with:    GHC==8.6.3, GHC==8.6.4
copyright:      Copyright Sven Heyll
license:        BSD3
license-file:   LICENSE
build-type:     Simple

extra-doc-files:
    docs/extensible-effects-concurrent-test-Loop-without-space-leaks.png
    docs/extensible-effects-concurrent-test-Loop-WITH-space-leaks.png

extra-source-files:
    stack.yaml
    extensible-effects-concurrent.nix
    default.nix
    shell.nix
    release.nix
    with-hoogle.nix
    ChangeLog.md
    README.md
    brittany.yaml
    .travis.yml

source-repository head
  type: git
  location: https://github.com/sheyll/extensible-effects-concurrent

library
  hs-source-dirs:
      src
  build-depends:
      async >= 2.2 && <3,
      base >= 4.12 && <5,
      bytestring >= 0.10 && < 0.11,
      data-default >= 0.7 && < 0.8,
      deepseq >= 1.4 && < 1.5,
      directory,
      hostname,
      exceptions >= 0.10 && < 0.11,
      safe-exceptions >= 0.1 && < 0.2,
      filepath >= 1.4 && < 1.5,
      time >= 1.8 && < 1.9,
      mtl >= 2.2 && < 2.3,
      containers >=0.5.8 && <0.7,
      QuickCheck,
      lens >= 4.14 && < 4.18,
      parallel >= 3.2 && < 3.3,
      process >= 1.6 && < 1.7,
      monad-control >= 1.0 && < 1.1,
      extensible-effects >= 5 && < 6,
      stm >= 2.4.5 && <2.6,
      transformers-base >= 0.4 && < 0.5,
      text >= 1.2 && < 1.3,
      network >= 2 && < 4,
      pretty-types >= 0.2.3.1 && < 0.4
  autogen-modules: Paths_extensible_effects_concurrent
  exposed-modules:
                  Control.Eff.Loop,
                  Control.Eff.ExceptionExtra,
                  Control.Eff.Log,
                  Control.Eff.Log.Examples,
                  Control.Eff.Log.Handler,
                  Control.Eff.Log.Message,
                  Control.Eff.Log.MessageRenderer,
                  Control.Eff.Log.Writer,
                  Control.Eff.LogWriter.Async,
                  Control.Eff.LogWriter.Console,
                  Control.Eff.LogWriter.Capture,
                  Control.Eff.LogWriter.DebugTrace,
                  Control.Eff.LogWriter.File,
                  Control.Eff.LogWriter.IO,
                  Control.Eff.LogWriter.UDP,
                  Control.Eff.LogWriter.UnixSocket,
                  Control.Eff.Concurrent,
                  Control.Eff.Concurrent.Protocol,
                  Control.Eff.Concurrent.Protocol.Client,
                  Control.Eff.Concurrent.Protocol.Server,
                  Control.Eff.Concurrent.Protocol.Request,
                  Control.Eff.Concurrent.Protocol.Supervisor,
                  Control.Eff.Concurrent.Process,
                  Control.Eff.Concurrent.Process.Timer,
                  Control.Eff.Concurrent.Process.ForkIOScheduler,
                  Control.Eff.Concurrent.Process.Interactive,
                  Control.Eff.Concurrent.Process.SingleThreadedScheduler,
                  Control.Eff.Concurrent.Protocol.Observer
                  Control.Eff.Concurrent.Protocol.Observer.Queue
  other-modules:
                Control.Eff.Concurrent.Protocol.Supervisor.InternalState,
                Paths_extensible_effects_concurrent
  default-extensions:
                     AllowAmbiguousTypes,
                     BangPatterns,
                     ConstraintKinds,
                     DefaultSignatures,
                     DeriveFoldable,
                     DeriveFunctor,
                     DeriveFunctor,
                     DeriveGeneric,
                     DeriveTraversable,
                     FlexibleContexts,
                     FlexibleInstances,
                     FunctionalDependencies,
                     GADTs,
                     GeneralizedNewtypeDeriving,
                     LambdaCase,
                     OverloadedStrings,
                     MultiParamTypeClasses,
                     RankNTypes,
                     ScopedTypeVariables,
                     StandaloneDeriving,
                     TemplateHaskell,
                     TupleSections,
                     TypeApplications,
                     TypeFamilies,
                     TypeInType,
                     TypeOperators,
                     ViewPatterns
  other-extensions:  ImplicitParams,
                     UndecidableInstances
  default-language: Haskell2010
  ghc-options: -Wall -fno-full-laziness

executable extensible-effects-concurrent-example-1
  main-is: Main.hs
  hs-source-dirs: examples/example-1
  build-depends:
                base
              , data-default
              , extensible-effects-concurrent
              , extensible-effects
              , lens
              , text
              , deepseq
              , pretty-types >= 0.2.3.1 && < 0.4
  ghc-options: -Wall -threaded -fno-full-laziness
  default-language: Haskell2010
  default-extensions:   AllowAmbiguousTypes
                      , BangPatterns
                      , DataKinds
                      , DeriveGeneric
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , LambdaCase
                      , OverloadedStrings
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators

executable extensible-effects-concurrent-example-2
  main-is: Main.hs
  hs-source-dirs: examples/example-2
  build-depends:
                base
              , data-default
              , extensible-effects-concurrent
              , extensible-effects
              , lens
              , text
              , deepseq
              , pretty-types >= 0.2.3.1 && < 0.4
  ghc-options: -Wall -threaded -fno-full-laziness
  default-language: Haskell2010
  default-extensions:   AllowAmbiguousTypes
                      , BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , LambdaCase
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators
                      , OverloadedStrings

executable extensible-effects-concurrent-example-3
  main-is: Main.hs
  hs-source-dirs: examples/example-3
  build-depends:
                base
              , data-default
              , directory
              , extensible-effects-concurrent
              , extensible-effects
              , filepath
              , lens
              , text
              , pretty-types >= 0.2.3.1 && < 0.4
  ghc-options: -Wall -threaded -fno-full-laziness
  default-language: Haskell2010
  default-extensions:   AllowAmbiguousTypes
                      , BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , LambdaCase
                      , OverloadedStrings
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators

executable extensible-effects-concurrent-example-4
  main-is: Main.hs
  hs-source-dirs: examples/example-4
  build-depends:
                base
              , extensible-effects-concurrent
              , extensible-effects
              , deepseq
              , text
              , deepseq
              , pretty-types >= 0.2.3.1 && < 0.4
  ghc-options: -Wall -threaded -fno-full-laziness
  default-language: Haskell2010
  default-extensions:   AllowAmbiguousTypes
                      , BangPatterns
                      , DataKinds
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , LambdaCase
                      , OverloadedStrings
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators

test-suite extensible-effects-concurrent-test
  type: exitcode-stdio-1.0
  main-is: Driver.hs
  hs-source-dirs: test
  other-modules:
                Common
              , Debug
              , ForkIOScheduler
              , GenServerTests
              , Interactive
              , LoggingTests
              , LogMessageIdeaTest
              , LoopTests
              , ProcessBehaviourTestCases
              , SupervisorTests
              , SingleThreadedScheduler
  ghc-options: -Wall -threaded -fno-full-laziness
  build-depends:
                async
              , base
              , data-default
              , deepseq
              , extensible-effects-concurrent
              , extensible-effects
              , tasty
              , tasty-discover
              , tasty-hunit
              , text
              , containers
              , QuickCheck
              , lens
              , HUnit
              , stm
              , text
              , time
              , filepath
              , hostname
              , pretty-types >= 0.2.3.1 && < 0.4
  default-language: Haskell2010
  default-extensions:   AllowAmbiguousTypes
                      , BangPatterns
                      , DataKinds
                      , DeriveGeneric
                      , FlexibleContexts
                      , FlexibleInstances
                      , FunctionalDependencies
                      , GADTs
                      , GeneralizedNewtypeDeriving
                      , LambdaCase
                      , OverloadedStrings
                      , RankNTypes
                      , ScopedTypeVariables
                      , StandaloneDeriving
                      , TemplateHaskell
                      , TypeApplications
                      , TypeFamilies
                      , TypeOperators
