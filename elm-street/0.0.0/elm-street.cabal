cabal-version:       2.0
name:                elm-street
version:             0.0.0
x-revision: 1
synopsis:            Crossing the road between Haskell and Elm
description:
    `Elm-street` allows you to generate automatically derived from Haskell types
    definitions of Elm data types, JSON encoders and decoders. This helps to avoid
    writing and maintaining huge chunk of boilerplate code when developing full-stack
    applications.
homepage:            https://github.com/Holmusk/elm-street
bug-reports:         https://github.com/Holmusk/elm-street/issues
license:             MPL-2.0
license-file:        LICENSE
author:              Holmusk
maintainer:          tech@holmusk.com
copyright:           2019 Holmusk
category:            Language, Compiler, Elm
build-type:          Simple
extra-doc-files:     README.md
                   , CHANGELOG.md
tested-with:         GHC == 8.4.4, GHC == 8.6.3

source-repository head
  type:                git
  location:            https://github.com/Holmusk/elm-street.git

library
  hs-source-dirs:      src
  exposed-modules:     Elm
                         Elm.Aeson
                         Elm.Ast
                         Elm.Generate
                         Elm.Generic
                         Elm.Print

  build-depends:       base >= 4.11.1.0 && < 4.13
                     , aeson >= 1.3
                     , directory ^>= 1.3
                     , filepath ^>= 1.4
                     , prettyprinter ^>= 1.2.1
                     , text ^>= 1.2
                     , time

  ghc-options:         -Wall
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
                       -Wcompat
                       -Widentities
                       -Wredundant-constraints
                       -fhide-source-paths
                       -Wmissing-export-lists
                       -Wpartial-fields

  default-language:    Haskell2010
  default-extensions:  ConstraintKinds
                       DeriveGeneric
                       GeneralizedNewtypeDeriving
                       InstanceSigs
                       KindSignatures
                       LambdaCase
                       OverloadedStrings
                       RecordWildCards
                       ScopedTypeVariables
                       StandaloneDeriving
                       TupleSections
                       TypeApplications
                       ViewPatterns

library types
  hs-source-dirs:      types
  exposed-modules:     Types

  build-depends:       base >= 4.11 && < 4.13
                     , aeson
                     , elm-street
                     , text
                     , time

  ghc-options:         -Wall
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
                       -Wcompat
                       -Widentities
                       -Wredundant-constraints
                       -fhide-source-paths
                       -Wmissing-export-lists
                       -Wpartial-fields

  default-language:    Haskell2010
  default-extensions:  ConstraintKinds
                       DeriveGeneric
                       GeneralizedNewtypeDeriving
                       InstanceSigs
                       KindSignatures
                       LambdaCase
                       OverloadedStrings
                       RecordWildCards
                       ScopedTypeVariables
                       StandaloneDeriving
                       TupleSections
                       TypeApplications
                       ViewPatterns

executable generate-elm
  hs-source-dirs:      generate-elm
  main-is:             Main.hs

  build-depends:       base
                     , elm-street
                     , types

  ghc-options:         -Wall
                       -threaded
                       -rtsopts
                       -with-rtsopts=-N
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
                       -Wcompat
                       -Widentities
                       -Wredundant-constraints
                       -fhide-source-paths

  default-language:    Haskell2010

executable run-backend
  hs-source-dirs:      backend
  main-is:             Main.hs
  other-modules:       Api

  build-depends:       base
                     , servant >= 0.14 && < 0.16
                     , servant-server >= 0.14 && < 0.16
                     , types
                     , wai ^>= 3.2
                     , warp ^>= 3.2

  ghc-options:         -Wall
                       -threaded
                       -rtsopts
                       -with-rtsopts=-N
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
                       -Wcompat
                       -Widentities
                       -Wredundant-constraints
                       -fhide-source-paths

  default-language:    Haskell2010
  default-extensions:  ConstraintKinds
                       DeriveGeneric
                       GeneralizedNewtypeDeriving
                       InstanceSigs
                       KindSignatures
                       LambdaCase
                       OverloadedStrings
                       RecordWildCards
                       ScopedTypeVariables
                       StandaloneDeriving
                       TupleSections
                       TypeApplications
                       TypeOperators
                       ViewPatterns

test-suite elm-street-test
  type:                exitcode-stdio-1.0
  hs-source-dirs:      test
  main-is:             Spec.hs

  build-depends:       base >= 4.11.1.0 && < 4.13
                     , elm-street


  ghc-options:         -Wall
                       -threaded
                       -rtsopts
                       -with-rtsopts=-N
                       -Wincomplete-uni-patterns
                       -Wincomplete-record-updates
                       -Wcompat
                       -Widentities
                       -Wredundant-constraints
                       -fhide-source-paths
                       -Wmissing-export-lists
                       -Wpartial-fields

  default-language:    Haskell2010
  default-extensions:  ConstraintKinds
                       DeriveGeneric
                       GeneralizedNewtypeDeriving
                       InstanceSigs
                       KindSignatures
                       LambdaCase
                       OverloadedStrings
                       RecordWildCards
                       ScopedTypeVariables
                       StandaloneDeriving
                       TupleSections
                       TypeApplications
                       ViewPatterns
