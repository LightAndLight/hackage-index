cabal-version: 1.12
name: elynx-markov
version: 0.2.2
license: GPL-3
license-file: LICENSE
copyright: Dominik Schrempf (2020)
maintainer: dominik.schrempf@gmail.com
author: Dominik Schrempf
homepage: https://github.com/dschrempf/elynx#readme
bug-reports: https://github.com/dschrempf/elynx/issues
synopsis: Simulate molecular sequences along trees
description:
    Examine, modify, and simulate molecular sequences in a reproducible way. Please see the README on GitHub at <https://github.com/dschrempf/elynx>.
category: Bioinformatics
build-type: Simple
extra-source-files:
    README.md
    ChangeLog.md

source-repository head
    type: git
    location: https://github.com/dschrempf/elynx

library
    exposed-modules:
        ELynx.Data.MarkovProcess.AminoAcid
        ELynx.Data.MarkovProcess.CXXModels
        ELynx.Data.MarkovProcess.CXXModelsData
        ELynx.Data.MarkovProcess.GammaRateHeterogeneity
        ELynx.Data.MarkovProcess.MixtureModel
        ELynx.Data.MarkovProcess.Nucleotide
        ELynx.Data.MarkovProcess.PhyloModel
        ELynx.Data.MarkovProcess.RateMatrix
        ELynx.Data.MarkovProcess.SubstitutionModel
        ELynx.Import.MarkovProcess.EDMModelPhylobayes
        ELynx.Import.MarkovProcess.SiteprofilesPhylobayes
        ELynx.Simulate.MarkovProcess
        ELynx.Simulate.MarkovProcessAlongTree
    hs-source-dirs: src
    other-modules:
        Paths_elynx_markov
    default-language: Haskell2010
    ghc-options: -Wall -fllvm
    build-depends:
        base >=4.13.0.0 && <4.14,
        bytestring >=0.10.10.0 && <0.11,
        containers >=0.6.2.1 && <0.7,
        elynx-seq >=0.2.1 && <0.3,
        elynx-tools >=0.2.2 && <0.3,
        elynx-tree >=0.2.2 && <0.3,
        hmatrix >=0.20.0.0 && <0.21,
        integration >=0.2.1 && <0.3,
        math-functions >=0.3.3.0 && <0.4,
        megaparsec >=8.0.0 && <8.1,
        mwc-random >=0.14.0.0 && <0.15,
        parallel >=3.2.2.0 && <3.3,
        primitive >=0.7.0.1 && <0.8,
        statistics >=0.15.2.0 && <0.16,
        vector >=0.12.1.2 && <0.13

test-suite markov-test
    type: exitcode-stdio-1.0
    main-is: Spec.hs
    hs-source-dirs: test
    other-modules:
        ELynx.Data.MarkovProcess.AminoAcidSpec
        ELynx.Data.MarkovProcess.NucleotideSpec
        ELynx.Data.MarkovProcess.RateMatrixSpec
        ELynx.Import.MarkovProcess.EDMModelPhylobayesSpec
        ELynx.Import.MarkovProcess.SiteprofilesPhylobayesSpec
        ELynx.Simulate.MarkovProcessAlongTreeSpec
        Paths_elynx_markov
    default-language: Haskell2010
    ghc-options: -Wall -fllvm -threaded -rtsopts -with-rtsopts=-N
    build-depends:
        base >=4.13.0.0 && <4.14,
        bytestring >=0.10.10.0 && <0.11,
        containers >=0.6.2.1 && <0.7,
        elynx-markov -any,
        elynx-tools >=0.2.2 && <0.3,
        elynx-tree >=0.2.2 && <0.3,
        hmatrix >=0.20.0.0 && <0.21,
        hspec >=2.7.1 && <2.8,
        hspec-megaparsec >=2.1.0 && <2.2,
        integration >=0.2.1 && <0.3,
        math-functions >=0.3.3.0 && <0.4,
        megaparsec >=8.0.0 && <8.1,
        mwc-random >=0.14.0.0 && <0.15,
        parallel >=3.2.2.0 && <3.3,
        primitive >=0.7.0.1 && <0.8,
        statistics >=0.15.2.0 && <0.16,
        vector >=0.12.1.2 && <0.13
