-- Copyright 2016 Ertugrul Söylemez
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

name:     acme-default
version:  0.1.0
category: ACME
synopsis: A class for types with a distinguished aesthetically pleasing value

maintainer:   Ertugrul Söylemez <esz@posteo.de>
author:       Ertugrul Söylemez <esz@posteo.de>
copyright:    Copyright 2016 Ertugrul Söylemez
homepage:     http://hub.darcs.net/esz/acme-default
bug-reports:  http://hub.darcs.net/esz/acme-default/issues
license:      Apache
license-file: LICENSE

description:  This package defines a type class for types with certain
    distinguished values that someone considers to be aesthetically
    pleasing.  Such a value is commonly referred to as a /default/
    value.
    .
    This package exists to introduce artistic variety regarding the
    aesthetics of Haskell's base types, but is otherwise identical in
    purpose to
    <https://hackage.haskell.org/package/data-default data-default>.

build-type:         Simple
cabal-version:      >= 1.10
extra-source-files: NOTICE README.md

source-repository head
    type:     darcs
    location: http://hub.darcs.net/esz/acme-default


library
    build-depends:
        base >= 4.8 && < 5
    default-language: Haskell2010
    ghc-options: -W -fdefer-typed-holes
    exposed-modules:
        Data.Default
