name:            tar
version:         0.4.3.0
x-revision: 2
license:         BSD3
license-file:    LICENSE
author:          Duncan Coutts <duncan@community.haskell.org>
                 Bjorn Bringert <bjorn@bringert.net>
maintainer:      Duncan Coutts <duncan@community.haskell.org>
bug-reports:     https://github.com/haskell/tar/issues
copyright:       2007 Bjorn Bringert <bjorn@bringert.net>
                 2008-2016 Duncan Coutts <duncan@community.haskell.org>
category:        Codec
synopsis:        Reading, writing and manipulating ".tar" archive files.
description:     This library is for working with \"@.tar@\" archive files. It
                 can read and write a range of common variations of archive
                 format including V7, USTAR, POSIX and GNU formats. It provides
                 support for packing and unpacking portable archives. This
                 makes it suitable for distribution but not backup because
                 details like file ownership and exact permissions are not
                 preserved.
build-type:      Simple
cabal-version:   >=1.8
extra-source-files: changelog.md
tested-with:     GHC ==7.4.2, GHC ==7.6.3, GHC ==7.8.4, GHC ==7.10.2

source-repository head
  type: git
  location: https://github.com/haskell/tar.git

flag old-time

library
  build-depends: base >= 4 && < 4.8,
                 bytestring >= 0.10,
                 filepath,
                 directory,
                 array,
                 containers >= 0.4.2,
                 deepseq >= 1.1 && < 1.4
  if flag(old-time)
    build-depends: directory < 1.2, old-time
  else
    build-depends: directory >= 1.2, time

  exposed-modules:
    Codec.Archive.Tar
    Codec.Archive.Tar.Entry
    Codec.Archive.Tar.Check
    Codec.Archive.Tar.Index

  other-modules:
    Codec.Archive.Tar.Types
    Codec.Archive.Tar.Read
    Codec.Archive.Tar.Write
    Codec.Archive.Tar.Pack
    Codec.Archive.Tar.Unpack
    Codec.Archive.Tar.Index.StringTable
    Codec.Archive.Tar.Index.IntTrie

  other-extensions:
    CPP, BangPatterns,
    DeriveDataTypeable, ScopedTypeVariables

  ghc-options: -Wall -fno-warn-unused-imports

test-suite properties
  type:          exitcode-stdio-1.0
  build-depends: base,
                 bytestring,
                 filepath, directory,
                 array,
                 containers,
                 deepseq,
                 old-time, time,
                 bytestring-handle,
                 QuickCheck == 2.*,
                 tasty            >= 0.10 && <0.12,
                 tasty-quickcheck == 0.8.*

  hs-source-dirs: . test

  main-is: test/Properties.hs
  cpp-options: -DTESTS

  other-modules:
    Codec.Archive.Tar.Index
    Codec.Archive.Tar.Index.StringTable
    Codec.Archive.Tar.Index.IntTrie

  other-extensions:
    CPP, BangPatterns,
    DeriveDataTypeable, ScopedTypeVariables

  ghc-options: -fno-ignore-asserts

benchmark bench
  type:          exitcode-stdio-1.0
  hs-source-dirs: . bench
  main-is:       bench/Main.hs
  build-depends: base,
                 bytestring,
                 filepath, directory,
                 array,
                 containers,
                 deepseq,
                 old-time, time,
                 criterion >= 1.0

