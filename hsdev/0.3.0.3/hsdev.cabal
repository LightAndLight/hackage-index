name:                hsdev
version:             0.3.0.3
x-revision: 1
synopsis:            Haskell development library
description:
  Haskell development library and tool with support of autocompletion, symbol info, go to declaration, find references, hayoo search etc.
homepage:            https://github.com/mvoidex/hsdev
license:             BSD3
license-file:        LICENSE
author:              Alexandr `Voidex` Ruchkin
maintainer:          voidex@live.com
-- copyright:
category:            Development
build-type:          Simple
cabal-version:       >=1.8
extra-source-files:
  tests/test-package/*.hs
  tests/test-package/test-package.cabal
  tests/data/base.sql
  tests/data/ModuleTwo.modified.hs
  data/hsdev.sql

source-repository head
  type: git
  location: git://github.com/mvoidex/hsdev.git

flag docs
  description: build with haddock/hdocs to support scanning docs
  default: True

library
  hs-source-dirs: src
  ghc-options: -Wall -fno-warn-tabs

  if !flag(docs)
    cpp-options: -DNODOCS

  exposed-modules:
    Control.Apply.Util
    Control.Concurrent.FiniteChan
    Control.Concurrent.Worker
    Control.Concurrent.Util
    Data.Deps
    Data.Help
    Data.Lisp
    Data.Maybe.JustIf
    Data.LookupTable
    HsDev
    HsDev.Client.Commands
    HsDev.Database.SQLite
    HsDev.Database.SQLite.Instances
    HsDev.Database.SQLite.Schema
    HsDev.Database.SQLite.Schema.TH
    HsDev.Database.SQLite.Select
    HsDev.Database.SQLite.Transaction
    HsDev.Database.Update
    HsDev.Database.Update.Types
    HsDev.Display
    HsDev.Error
    HsDev.Inspect
    HsDev.Inspect.Order
    HsDev.PackageDb
    HsDev.PackageDb.Types
    HsDev.Project
    HsDev.Project.Compat
    HsDev.Project.Types
    HsDev.Scan
    HsDev.Scan.Browse
    HsDev.Server.Base
    HsDev.Server.Commands
    HsDev.Server.Message
    HsDev.Server.Message.Lisp
    HsDev.Server.Types
    HsDev.Sandbox
    HsDev.Stack
    HsDev.Symbols
    HsDev.Symbols.Name
    HsDev.Symbols.Class
    HsDev.Symbols.HaskellNames
    HsDev.Symbols.Location
    HsDev.Symbols.Documented
    HsDev.Symbols.Resolve
    HsDev.Symbols.Parsed
    HsDev.Symbols.Types
    HsDev.Tools.AutoFix
    HsDev.Tools.Base
    HsDev.Tools.Cabal
    HsDev.Tools.ClearImports
    HsDev.Tools.Ghc.Check
    HsDev.Tools.Ghc.Compat
    HsDev.Tools.Ghc.MGhc
    HsDev.Tools.Ghc.Prelude
    HsDev.Tools.Ghc.Session
    HsDev.Tools.Ghc.Types
    HsDev.Tools.Ghc.Worker
    HsDev.Tools.Hayoo
    HsDev.Tools.HDocs
    HsDev.Tools.HLint
    HsDev.Tools.Refact
    HsDev.Tools.Tabs
    HsDev.Tools.Types
    HsDev.Types
    HsDev.Util
    HsDev.Version
    HsDev.Watcher
    HsDev.Watcher.Types
    System.Directory.Paths
    System.Directory.Watcher

  if os(windows)
    build-depends:
      Win32 >= 2.3.0
    exposed-modules:
      System.Win32.PowerShell
      System.Win32.FileMapping.NamePool
      System.Win32.FileMapping.Memory
  else
    build-depends:
      unix >= 2.7.0

  if impl(ghc >= 8.2) && impl(ghc < 8.3)
    if flag(docs)
      build-depends:
        haddock-api >= 2.18.0 && < 2.19.0

    build-depends:
      ghc == 8.2.*,
      ghc-boot,
      directory >= 1.2.6.0
  if impl(ghc >= 8.0) && impl(ghc < 8.2)
    if flag(docs)
      build-depends:
        haddock-api >= 2.17.0 && < 2.18.0

    build-depends:
      ghc >= 8.0.0 && < 8.1.0,
      ghc-boot,
      directory >= 1.2.6.0
  if impl(ghc < 8.0)
    if flag(docs)
      build-depends:
        haddock-api >= 2.16.0 && < 2.17.0

    build-depends:
      ghc >= 7.10.0 && < 7.11.0,
      bin-package-db,
      directory == 1.2.2.*

  if flag(docs)
    build-depends:
      hdocs >= 0.5.1

  build-depends:
    base >= 4.7 && < 4.11,
    aeson >= 0.7.0,
    aeson-pretty >= 0.7.0,
    array >= 0.5.0,
    async >= 2.0,
    attoparsec >= 0.11.0,
    bytestring >= 0.10.0,
    Cabal >= 1.22.0,
    containers >= 0.5,
    cpphs >= 1.19.0,
    data-default >= 0.5.0,
    deepseq >= 1.4.0,
    exceptions >= 0.6.0,
    filepath >= 1.4.0,
    fsnotify >= 0.2.1 && < 0.3,
    ghc-paths >= 0.1.0,
    ghc-syb-utils >= 0.2.3,
    haskell-names >= 0.9.0 && < 0.10.0,
    haskell-src-exts >= 1.18.0 && < 1.21.0,
    hformat >= 0.3.0,
    hlint >= 1.9.13 && < 2.1,
    HTTP >= 4000.2.0,
    lens >= 4.8,
    lifted-base >= 0.2,
    monad-control >= 1.0,
    monad-loops >= 0.4,
    mmorph >= 1.0 && < 1.2,
    mtl >= 2.2.0,
    network >= 2.6.0,
    optparse-applicative >= 0.11,
    process >= 1.2.0,
    regex-pcre-builtin >= 0.94,
    scientific >= 0.3,
    simple-log == 0.9.*,
    stm == 2.4.*,
    direct-sqlite >= 2.3.21,
    sqlite-simple,
    syb >= 0.5.1,
    template-haskell,
    text >= 1.2.0,
    text-region >= 0.3,
    time >= 1.5.0,
    transformers >= 0.4.0,
    transformers-base >= 0.4.0,
    uniplate >= 1.6.0,
    unordered-containers >= 0.2.0,
    vector >= 0.10.0

executable hsdev
  main-is: hsdev.hs
  hs-source-dirs: tools
  ghc-options: -threaded -Wall -fno-warn-tabs "-with-rtsopts=-N4"

  build-depends:
    base >= 4.7 && < 4.11,
    hsdev,
    aeson >= 0.7.0,
    aeson-pretty >= 0.7.0,
    bytestring >= 0.10.0,
    containers >= 0.5,
    deepseq >= 1.4.0,
    directory,
    exceptions >= 0.6.0,
    filepath >= 1.4.0,
    monad-loops >= 0.4.0,
    mtl >= 2.2.0,
    network >= 2.6.0,
    optparse-applicative >= 0.11,
    process >= 1.2.0,
    text >= 1.2.0,
    transformers >= 0.4.0,
    unordered-containers >= 0.2.0

test-suite test
  main-is: Test.hs
  hs-source-dirs: tests
  ghc-options: -threaded -Wall -fno-warn-tabs
  type: exitcode-stdio-1.0
  build-depends:
    base >= 4.7 && < 4.11,
    hsdev,
    aeson >= 0.7.0,
    aeson-lens >= 0.5.0,
    async >= 2.0,
    data-default >= 0.5.0,
    deepseq >= 1.4.0,
    directory,
    filepath >= 1.4.0,
    containers >= 0.5.0,
    hformat == 0.3.*,
    hspec,
    lens >= 4.8,
    mtl >= 2.2.0,
    text >= 1.2.0
