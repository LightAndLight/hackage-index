-- This file has been generated from package.yaml by hpack version 0.21.2.
--
-- see: https://github.com/sol/hpack
--
-- hash: 55af04fb959faaa66f7f850ad4ce3d6ebb14a32c54b20fb5450b1fe5a51ef64a

name:                   mellon-web
version:                0.8.0.5
synopsis:               A REST web service for Mellon controllers
description:            The @mellon-web@ package wraps a @mellon-core@ controller in a REST
                        web service, making it possible to control physical access devices
                        from an HTTP client. The package includes both a WAI application
                        server, and native Haskell client bindings for the service.
                        .
                        Like the @mellon-core@ controller interface, the @mellon-web@ REST API
                        is quite simple. There are only 3 methods:
                        .
                        * @GET /time@ returns the system time on the server. This is made
                        available for diagnostic purposes, primarily to ensure the server
                        has an accurate clock.
                        .
                        * @GET /state@ returns the controller's current state (either @Locked@
                        or @Unlocked date@ where @date@ is the UTC time at which the
                        controller will automatically lock again).
                        .
                        * @PUT /state@ sets the controller's current state. Use this method to
                        lock and unlock the controller.
                        .
                        For detailed documentation, the server includes a self-documenting
                        Swagger spec.
                        .
                        Note that the @mellon-web@ server does not provide an authentication
                        mechanism! You should proxy it behind a secure, authenticating HTTPS
                        server such as Nginx.
category:               Web
stability:              experimental
homepage:               https://github.com/quixoftic/mellon#readme
bug-reports:            https://github.com/quixoftic/mellon/issues
author:                 Drew Hess <dhess-src@quixoftic.com>
maintainer:             Drew Hess <dhess-src@quixoftic.com>
copyright:              Copyright (c) 2018, Quixoftic, LLC
license:                BSD3
license-file:           LICENSE
tested-with:            GHC==8.0.2 GHC==8.2.2
build-type:             Simple
cabal-version:          >= 1.10

extra-source-files:
    changelog.md
    package.yaml
    README.md

data-files:
    swagger.json

source-repository head
  type: git
  location: https://github.com/quixoftic/mellon

flag client-unlock-example
  description: Build the unlock client example
  manual: True
  default: True

flag gpio-example
  description: Build the GPIO server example
  manual: True
  default: True

flag mock-example
  description: Build the mock server example
  manual: True
  default: True

flag test-doctests
  description: Build doctests
  manual: True
  default: True

flag test-hlint
  description: Build hlint test
  manual: True
  default: False

library
  hs-source-dirs:
      src
  default-extensions: NoImplicitPrelude
  other-extensions: DataKinds DeriveDataTypeable DeriveGeneric MultiParamTypeClasses OverloadedStrings OverloadedLists ScopedTypeVariables TypeOperators
  build-depends:
      aeson >=0.11 && <1.3
    , aeson-pretty ==0.8.*
    , base >=4.8 && <5
    , bytestring ==0.10.*
    , http-client >=0.4 && <0.6
    , http-types ==0.9.*
    , lens >=4 && <5
    , lucid ==2.9.*
    , mellon-core
    , protolude ==0.2.*
    , servant ==0.11.*
    , servant-client ==0.11.*
    , servant-docs >=0.10 && <0.12
    , servant-lucid ==0.7.*
    , servant-server ==0.11.*
    , servant-swagger ==1.1.*
    , servant-swagger-ui ==0.2.*
    , swagger2 >=2.1 && <2.3
    , text ==1.2.*
    , time >=1.5 && <2
    , transformers >=0.4.2 && <0.6
    , wai ==3.2.*
    , warp ==3.2.*
  if impl(ghc >= 8.0)
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  else
    ghc-options: -Wall -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options: -Wcompat -Wnoncanonical-monad-instances -Wnoncanonical-monadfail-instances
  else
    build-depends:
        fail ==4.9.*
      , semigroups ==0.18.*
  exposed-modules:
      Mellon.Web.Client
      Mellon.Web.Server
      Mellon.Web.Server.API
      Mellon.Web.Server.SwaggerAPI
  other-modules:
      Paths_mellon_web
  default-language: Haskell2010

executable gpio-mellon-server
  main-is: GpioServer.hs
  hs-source-dirs:
      examples
  default-extensions: NoImplicitPrelude
  other-extensions: OverloadedStrings
  ghc-options: -threaded -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  else
    ghc-options: -Wall -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options: -Wcompat -Wnoncanonical-monad-instances -Wnoncanonical-monadfail-instances -fno-warn-redundant-constraints
  if !(flag(gpio-example))
    buildable: False
  else
    build-depends:
        base
      , exceptions >=0.8.0 && <1
      , hpio >=0.8 && <1
      , mellon-core
      , mellon-gpio
      , mellon-web
      , mtl >=2.1.3 && <2.3
      , network
      , optparse-applicative >=0.11.0 && <0.15
      , protolude
      , time
      , transformers
      , warp
  default-language: Haskell2010

executable mellon-schedule-unlock
  main-is: ScheduleUnlock.hs
  hs-source-dirs:
      examples
  default-extensions: NoImplicitPrelude
  other-extensions: LambdaCase OverloadedStrings
  ghc-options: -threaded -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  else
    ghc-options: -Wall -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options: -Wcompat -Wnoncanonical-monad-instances -Wnoncanonical-monadfail-instances -fno-warn-redundant-constraints
  if !(flag(client-unlock-example))
    buildable: False
  else
    build-depends:
        base
      , bytestring
      , exceptions
      , http-client
      , http-client-tls ==0.3.*
      , http-types
      , mellon-core
      , mellon-web
      , mtl
      , network
      , optparse-applicative
      , protolude
      , servant-client
      , time
      , transformers
  default-language: Haskell2010

executable mock-mellon-server
  main-is: MockServer.hs
  hs-source-dirs:
      examples
  default-extensions: NoImplicitPrelude
  other-extensions: OverloadedStrings
  ghc-options: -threaded -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  else
    ghc-options: -Wall -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if impl(ghc >= 8.0)
    ghc-options: -Wcompat -Wnoncanonical-monad-instances -Wnoncanonical-monadfail-instances -fno-warn-redundant-constraints
  if !(flag(mock-example))
    buildable: False
  else
    build-depends:
        base
      , mellon-core
      , mellon-web
      , protolude
      , warp
  default-language: Haskell2010

test-suite doctest
  type: exitcode-stdio-1.0
  main-is: doctest.hs
  hs-source-dirs:
      test
  default-extensions: NoImplicitPrelude
  ghc-options: -threaded
  if impl(ghc >= 8.0)
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  else
    ghc-options: -Wall -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if !(flag(test-doctests))
    buildable: False
  else
    build-depends:
        base
      , doctest >=0.11 && <0.14
      , protolude
  default-language: Haskell2010

test-suite hlint
  type: exitcode-stdio-1.0
  main-is: hlint.hs
  hs-source-dirs:
      test
  default-extensions: NoImplicitPrelude
  ghc-options: -w -threaded -rtsopts -with-rtsopts=-N
  if impl(ghc >= 8.0)
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  else
    ghc-options: -Wall -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  if !(flag(test-hlint))
    buildable: False
  else
    build-depends:
        base
      , hlint ==2.0.*
      , protolude
  default-language: Haskell2010

test-suite spec
  type: exitcode-stdio-1.0
  main-is: Main.hs
  hs-source-dirs:
      test
  default-extensions: NoImplicitPrelude
  other-extensions: OverloadedStrings ScopedTypeVariables
  ghc-options: -w -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      QuickCheck >=2.8 && <2.12
    , aeson
    , aeson-pretty
    , base
    , bytestring
    , hspec >=2.2 && <2.5
    , hspec-wai >=0.6.6 && <0.10
    , http-client
    , http-types
    , lens
    , lucid
    , mellon-core
    , mellon-web
    , network ==2.6.*
    , protolude
    , quickcheck-instances ==0.3.*
    , servant
    , servant-client
    , servant-docs
    , servant-lucid
    , servant-server
    , servant-swagger
    , servant-swagger-ui
    , swagger2
    , text
    , time
    , transformers
    , wai
    , wai-extra ==3.0.*
    , warp
  if impl(ghc >= 8.0)
    ghc-options: -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates
  else
    ghc-options: -Wall -fwarn-incomplete-uni-patterns -fwarn-incomplete-record-updates
  other-modules:
      Mellon.Web.ClientSpec
      Mellon.Web.ServerSpec
      Mellon.Web.SwaggerAPISpec
      Spec
      Paths_mellon_web
  default-language: Haskell2010
