name:                 cimple
version:              0.0.6
synopsis:             Simple C-like programming language
homepage:             https://toktok.github.io/
license:              GPL-3
license-file:         LICENSE
author:               Iphigenia Df <iphydf@gmail.com>
maintainer:           Iphigenia Df <iphydf@gmail.com>
copyright:            Copyright (c) 2016-2022, Iphigenia Df
category:             Data
stability:            Experimental
cabal-version:        >= 1.10
build-type:           Simple
description:
  Parser and AST for Cimple, a simple C-like programming language.

source-repository head
  type:             git
  location:         https://github.com/TokTok/hs-cimple

library
  default-language: Haskell2010
  hs-source-dirs:
      src
  ghc-options:
      -Wall
  build-tools: alex, happy
  exposed-modules:
      Language.Cimple
    , Language.Cimple.Diagnostics
    , Language.Cimple.IO
    , Language.Cimple.Pretty
    , Language.Cimple.Program
  other-modules:
      Language.Cimple.AST
    , Language.Cimple.Flatten
    , Language.Cimple.Graph
    , Language.Cimple.Lexer
    , Language.Cimple.Parser
    , Language.Cimple.SemCheck.Includes
    , Language.Cimple.Tokens
    , Language.Cimple.TranslationUnit
    , Language.Cimple.TraverseAst
    , Language.Cimple.TreeParser
  build-depends:
      base < 5
    , aeson
    , ansi-wl-pprint
    , array
    , bytestring
    , containers
    , data-fix
    , filepath
    , groom
    , mtl
    , recursion-schemes
    , text
    , transformers-compat

executable cimplefmt
  default-language: Haskell2010
  hs-source-dirs:
      tools
  ghc-options:
      -Wall
  main-is: cimplefmt.hs
  build-depends:
      base < 5
    , cimple

executable dump-ast
  default-language: Haskell2010
  hs-source-dirs:
      tools
  ghc-options:
      -Wall
  main-is: dump-ast.hs
  build-depends:
      base < 5
    , bytestring
    , cimple
    , groom
    , text

executable dump-tokens
  default-language: Haskell2010
  hs-source-dirs:
      tools
  ghc-options:
      -Wall
  main-is: dump-tokens.hs
  build-depends:
      base < 5
    , bytestring
    , cimple
    , groom
    , text

executable include-graph
  default-language: Haskell2010
  hs-source-dirs:
      tools
  ghc-options:
      -Wall
  main-is: include-graph.hs
  build-depends:
      base < 5
    , cimple
    , groom

test-suite testsuite
  type: exitcode-stdio-1.0
  default-language: Haskell2010
  hs-source-dirs: test
  main-is: testsuite.hs
  other-modules:
      Language.CimpleSpec
    , Language.Cimple.PrettySpec
  ghc-options:
      -Wall
      -fno-warn-unused-imports
  build-tool-depends:
      hspec-discover:hspec-discover
  build-depends:
      base < 5
    , ansi-wl-pprint
    , cimple
    , data-fix
    , hspec
    , text
    , transformers-compat
